#!/bin/bash

source ./pdfbox_global.sh
RevList=${1:-"$subjectloc/revisionList"}
ROOTDIR=${2:-"$subjectloc/source_revisions"}

while read N;
do
	echo "==================="
	echo "DynAliasRunning r$N ......"
	echo "==================="
	pushd . 1>/dev/null 2>&1
	cd $ROOTDIR/$N/
	sh $subjectloc/DynAliasRun.sh $N
	popd . 1>/dev/null 2>&1
done < ${RevList}

echo "All revisions are finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

