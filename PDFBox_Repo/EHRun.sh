#!/bin/bash
source ../../pdfbox_global.sh
if [ $# -lt 1 ];then
	echo "Usage: $0 revision"
	exit 1
fi

Rev=$1

INDIR=$subjectloc/source_revisions/$Rev/EHInstrumented

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$subjectloc/libs/xercesImpl-fixed:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/Deam/bin:$ROOT/workspace/Sensa/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/InstrReporters/bin:$INDIR"

for i in $subjectloc/libs/*.jar;
do
	MAINCP=$MAINCP:$i
done

OUTDIR=$subjectloc/source_revisions/$Rev/EHoutdyn
mkdir -p $OUTDIR

FAILLIST=$subjectloc/source_revisions/$Rev/errorTestName.txt
PASSLIST=$subjectloc/source_revisions/$Rev/normalTestName.txt
> $FAILLIST
> $PASSLIST

function RunAllInOne()
{
	java -Xmx4000m -ea -cp $MAINCP  $DRIVERCLASS 
}

function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	cat $subjectloc/inputs/testinputs.txt | dos2unix | \
	while read testname;
	do
		let i=i+1

		echo "Run Test #$i" # [" $testname "] ....."
		#java -noverify -Xmx4000m -ea -cp $MAINCP  $DRIVERCLASS $testname 1> $OUTDIR/$i.out 2> $OUTDIR/$i.err
		java -Xmx4000m -ea -cp $MAINCP  $DRIVERCLASS $testname 1> $OUTDIR/$i.out 2> $OUTDIR/$i.err
		if [ -s $OUTDIR/$i.err ];then
			echo "$testname" >> $FAILLIST
		else
			echo "$testname" >> $PASSLIST
		fi
	done
}

starttime=`date +%s%N | cut -b1-13`
#RunAllInOne
RunOneByOne
stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for r$Rev elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0

# hcai vim :set ts=4 tw=4 tws=4

