#!/bin/bash

source ./pdfbox_global.sh

for N in ${SEEDS};
do
	echo "==================="
	echo "compiling r$N ......"
	echo "==================="
	pushd . 1>/dev/null 2>&1
	cd $subjectloc/source_revisions/$N/
	sh $subjectloc/compile.sh $N
	popd . 1>/dev/null 2>&1
done

echo "All revisions are finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

