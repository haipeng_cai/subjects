#!/bin/bash

#sh ./checkoutAvailables.sh 924515 30

#sh ./generateRevisionCompileFileList.sh

#sh ./compileRevisions.sh
#sh ./run_uninstrRevisions.sh

sh ./runRepoDiff.sh

sh ./EAInstrRevisions.sh
sh ./EARunRevisions.sh

sh ./EHInstrRevisions.sh
sh ./EHRunRevisions.sh

#sh ./RepoRunAnalysis.sh 924515 926512 1>repoAnalysis.out 2>repoAnalysis.err
python ./RepoEASAccuracy.py 1>logres.EASAccuracy 2>&1 

# hcai vim :set ts=4 tw=4 tws=4

