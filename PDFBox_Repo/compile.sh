#!/bin/sh
if [ $# -lt 1 ];then
	echo "Usage: $0 Rev"
	exit 1
fi

Rev=$1

source ../../pdfbox_global.sh

mkdir -p bin

MAINCP=".:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Sensa/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin"

for i in $subjectloc/libs/*.jar;
do
	MAINCP=$MAINCP:$i
done

#-source 1.4 -target 1.4
#-target 1.5 
javac -g:source -Xlint:none -cp ${MAINCP} -d bin @compilefiles.lst  #1>err.compile 2>&1

mkdir -p src/test/resources
cp -fr $subjectloc/source_revisions/$Rev/pdfbox/src/test/resources/* src/test/resources/
cp -fr $subjectloc/source_revisions/$Rev/pdfbox/src/main/resources/* ./ 

cp -fr $subjectloc/source_revisions/$Rev/pdfbox/src/test/resources/org/apache/pdfbox/pdmodel/test_pagelabels.pdf bin/org/apache/pdfbox/pdmodel/
cp -fr $subjectloc/source_revisions/$Rev/pdfbox/src/test/resources/org/apache/pdfbox/encryption/* bin/org/apache/pdfbox/encryption/

cp -fr $subjectloc/inputs/genko_oc_shiryo1.pdf src/test/resources/pdfparser/

mkdir -p target/test-output/rendering/
mkdir -p src/test/resources/output

echo "Compilation for $Rev all done." 
exit 0

# hcai vim :set ts=4 tw=4 tws=4
