#!/bin/bash

srcfn=${1:-"-"}

grep -a -E "valid versus|Totally|between revisions" -C1 $srcfn

#	grep -a -E "between revision|valid versus" logres.EASVsDiver > diverresult_final_aux.txt
#	vim diverresult_final_aux.txt
#	grep -a -E "between revision|valid versus" diverresult_final_aux.txt
#	grep -a -E "between revision" diverresult_final_aux.txt | wc -l
#	ll
#	grep -a -E "between revision" diverresult_final_aux.txt
#	grep -a -E "between revision" diverresult_final_aux.txt | awk '{print $(NF-2),$(NF-1),$NF}'
#	man paste
#	paste logres.EASVsDiver
#	paste diverresult_final_aux.txt | less
#	paste diverresult_final_aux.txt diverresult_final_aux.txt
#	grep -a -E "between revision" diverresult_final_aux.txt | awk '{print $(NF-3),$(NF-2),$(NF-1)}'
#	grep -a -E "between revision" diverresult_final_aux.txt | awk '{print $(NF-3),$(NF-2),$(NF-1)}' > diverresult_final_aux2.txt
#	wc -l diverresult_final_aux2.txt
#	wc -l diverresult_final.txt
#	paste diverresult_final_aux2.txt diverresult_final.txt

cat diverresult_final_aux2.txt | while read line; do a=`echo $line | awk '{print $1}'`; b=`echo $line | awk '{print $3}'`; cat diffs/diff_$a-$b | wc -l; done
