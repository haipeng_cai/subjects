#!/bin/bash

source ./pdfbox_global.sh
RevList=${1:-"$subjectloc/revisionList"}
BASEDIR=`pwd`/source_revisions

while read N;
do
	fnls=$BASEDIR/${N}/compilefiles.lst
	#fnls=./r${N}-files.lst
	> $fnls
	for subdir in fontbox jempbox pdfbox;
	do
		find $BASEDIR/$N/$subdir -name "*.java" 1>> $fnls
	done
	find $BASEDIR/td/ -name "*.java" 1>> $fnls

	cp $subjectloc/inputs/Type1CharStringTest.java $BASEDIR/$N/fontbox/src/test/java/org/apache/fontbox/cff/Type1CharStringTest.java
	cp $subjectloc/inputs/Type1FontUtilTest.java $BASEDIR/$N/fontbox/src/test/java/org/apache/fontbox/cff/Type1FontUtilTest.java
done < ${RevList}

exit 0

# hcai vim :set ts=4 tw=4 tws=4

