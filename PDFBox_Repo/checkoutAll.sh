#!/bin/bash

source ./pdfbox_global.sh

for N in ${SEEDS};
do
	echo "==================="
	echo "checking out r$N ......"
	echo "==================="
	svn checkout -r$N http://svn.apache.org/repos/asf/pdfbox/trunk/ $subjectloc/source_revisions/$N
done

echo "All revisions are finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

