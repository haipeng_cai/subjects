#!/bin/bash

source ./pdfbox_global.sh
RevList=${1:-"$subjectloc/revisionList"}

while read N;
do
	~/bin/cloc-1.60.pl $subjectloc/source_revisions/$N/fontbox \
		$subjectloc/source_revisions/$N/jempbox \
		$subjectloc/source_revisions/$N/pdfbox | grep Java
done < ${RevList}

echo "All revisions are finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

