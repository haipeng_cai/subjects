#!/bin/bash

if [ $# -lt 2 ];then
	echo "Usage: $0 start_rev #revs [repository url] [target directory]"
	exit 1
fi
source ./ant_global.sh

SRev=$1
NRev=$2
url=${3:-"http://svn.apache.org/repos/asf/ant/core/trunk/"}
tgtdir=${4:-"$subjectloc/source_revisions"}

## get all existing revision numbers
#svn log -q -l 10 -r268130:668130 http://svn.apache.org/repos/asf/ant/core/trunk/
svn log -q "$url" | tac | awk '{if(NF>=7)print $1}' > /tmp/allRevs
AllRevs=()
j=0
while read rev;
do
	AllRevs[$j]=${rev:1}
	((j++))
done < /tmp/allRevs
T=${#AllRevs[@]}
echo "There are totally $T revisions in current commit history of $url."

if [ $NRev -gt $T ];then
	echo "$T (the maximal available) instead of $NRev revisions are to be retrieved."
	NRev=$T
fi

for ((k=0;k<$T;++k))
do
	if [ ${AllRevs[$k]} -eq $SRev ];then
		SRev=$k # from now on SRev becomes an index to the global revision list instead of a revision number itself, so are NextRev and CurRev.
		break
	fi
done
#echo "starting from revision index $SRev."
#exit 0
rm -f /tmp/allRevs

fnrev=`pwd`/revisionList
> ${fnrev} 
mkdir -p $tgtdir

starttime=`date +%s%N | cut -b1-13`

## look up for a revision before retrieving it
svn ls -r${AllRevs[$SRev]} "$url" 1>/dev/null
if [ $? -ne 0 ];then
	echo "there is no revision ${AllRevs[$SRev]} found in $url, bailing out now."
	exit 1
fi

echo -n "checking out the starting revision ${AllRevs[$SRev]} ...... "
## check out the starting revision first
svn checkout -r${AllRevs[$SRev]} "$url" $tgtdir/${AllRevs[$SRev]} 1>/dev/null
if [ $? -ne 0 ];then
	echo "failed in checking out revision ${AllRevs[$SRev]} from $url, bailing out now."
	exit 1
fi
echo "${AllRevs[$SRev]}" >> ${fnrev}
echo " Done."

## check out the next revisions with those having not contained source changes skipped
i=1
NextRev=$SRev
while ((i<=NRev))
do
	echo "==================================="
	echo "retrieving revision No. $i ......"
	echo "==================================="
	CurRev=$NextRev

	## find the next source-code--change revision
	ndiff=0
	while ((ndiff<1));
	do
		((NextRev=NextRev+1))
		## look up for a revision before retrieving it
		svn ls -r${AllRevs[$NextRev]} "$url" 1>/dev/null
		if [ $? -ne 0 ];then
			echo "there is no revision ${AllRevs[$NextRev]} found in $url, giving up."
			echo "only $((i+0)) code-change revisions have been checked out and can be found at : $tgtdir"
			exit 1
		fi
		ndiff=`svn diff -r${AllRevs[$CurRev]}:${AllRevs[$NextRev]} "$url" | grep "Index:" | grep -a -c ".java"`
:<<'COMMENT'
		if [ $? -ne 0 ];then
			echo "failed in differencing revision $NextRev against $CurRev in repository $url, giving up now."
			exit 1
		fi
COMMENT
	done
	echo "one more pair of consecutive code-change revisions involving source change found: ${AllRevs[$CurRev]} -> ${AllRevs[$NextRev]}"

	## check out the source-code--change revision just found
	echo -n "checking out revision ${AllRevs[$NextRev]} ...... "
	svn checkout -r${AllRevs[$NextRev]} "$url" $tgtdir/${AllRevs[$NextRev]} 1>/dev/null
	if [ $? -ne 0 ];then
		echo "failed in checking out revision ${AllRevs[$NextRev]} from $url, stopping now."
		break
	fi
	echo "${AllRevs[$NextRev]}" >> ${fnrev}
	echo " Done."

	((i++))
done

## done all check-outs
echo "all $((i+0)) code-change revisions have been checked out and can be found at : $tgtdir"

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0

# hcai vim :set ts=4 tw=4 tws=4

