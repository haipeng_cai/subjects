#!/bin/bash

mkdir -p timecosts
grep -a -E "Normal RunTime for" timing.uninstrRun | awk '{print $(NF-1)}' > timecosts/uninstrRun 
grep -a -E "elapsed" timing.runprof  | awk '{print $(NF-1)}' > timecosts/runprof
grep -a -E "elapsed" timing.instprof  | awk '{print $(NF-1)}' > timecosts/instprof
grep -a -E "elapsed" timing.earun  | awk '{print $(NF-1)}' > timecosts/earun
grep -a -E "elapsed" timing.DiverRun  | awk '{print $(NF-1)}' > timecosts/diverrun
grep -a -E "elapsed" timing.DiverBranchRun  | awk '{print $(NF-1)}' > timecosts/diverBranchRun
grep -a -E "elapsed" timing.AliasRun  | awk '{print $(NF-1)}' > timecosts/AliasRun
grep -a -E "elapsed" timing.AliasInsRun  | awk '{print $(NF-1)}' > timecosts/AliasInsRun

grep "Soot has run for" source_revisions/*/out-EAInstr/instr.out | awk '{printf("%d\n",($(NF-3))*60+($(NF-1)));}' > timecosts/eainstr
grep "Soot has run for" source_revisions/*/out-DiverInstr/instr.out | awk '{printf("%d\n",($(NF-3))*60+($(NF-1)));}' > timecosts/diverinstr
grep "Soot has run for" source_revisions/*/out-DiverBranchInstr/instr.out | awk '{printf("%d\n",($(NF-3))*60+($(NF-1)));}' > timecosts/branchinstr
grep "Soot has run for" source_revisions/*/out-DynAliasInstr/instr.out | awk '{printf("%d\n",($(NF-3))*60+($(NF-1)));}' > timecosts/aliasinstr

