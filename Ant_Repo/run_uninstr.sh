#!/bin/bash
if [ $# -lt 1 ];then
	echo "Usage: $0 Rev"
	exit 1
fi

Rev=$1

source ./ant_global.sh

if [ ! -d $subjectloc/source_revisions/$Rev ];then
	echo "Rev $Rev not found."
	exit -1
fi

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$subjectloc/source_revisions/$Rev/bin/"

for i in $subjectloc/libs/*.jar;
do
	MAINCP=$MAINCP:$i
done

OUTDIR=$subjectloc/source_revisions/$Rev/Runout-uninstr
mkdir -p $OUTDIR

FAILLIST=$subjectloc/source_revisions/$Rev/failedTestName.txt
PASSLIST=$subjectloc/source_revisions/$Rev/passedTestName.txt
> $FAILLIST
> $PASSLIST

function RunAllInOne()
{
	java -Xmx4000m -Dbuild.tests=$subjectloc/source_revisions/$Rev/bin -ea -cp $MAINCP  $DRIVERCLASS 
	#java -Xmx4000m -ea -cp $MAINCP org.junit.runner.JUnitCore opennlp.uima.AnnotatorsInitializationTest
}

function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	cat $subjectloc/inputs/testinputs.txt | dos2unix | \
	while read testname;
	do
		let i=i+1

		echo "Run Test #$i" # [" $testname "] ....."
		java -Xmx4000m -Dbuild.tests=$subjectloc/source_revisions/$Rev/bin -ea -cp $MAINCP  \
				$DRIVERCLASS $testname 1>$OUTDIR/$i.out 2>$OUTDIR/$i.err
		if [ -s $OUTDIR/$i.err ];then
			echo "$testname" >> $FAILLIST
		else
			echo "$testname" >> $PASSLIST
		fi
	done
}

starttime=`date +%s%N | cut -b1-13`
pushd . 1>/dev/null 2>&1
cd $subjectloc/source_revisions/$Rev/
RunAllInOne
#RunOneByOne
popd . 1>/dev/null 2>&1
stoptime=`date +%s%N | cut -b1-13`
echo "Normal RunTime for r${Rev} elapsed: " `expr $stoptime - $starttime` milliseconds
exit 0

# hcai vim :set ts=4 tw=4 tws=4
