#!/bin/bash

srcfn=${1:-"-"}

grep -v -E "Done ALL with|valid versus|Running Diver|current at" $srcfn | \
	awk '{
			if(NF>=11) {
				for (i=7; i>=0; i--) printf("%s\t", $(NF-i)); 
				printf("\n");
			}
		}'
	#awk '{if(NF>12) print $0}'

