#!/bin/bash

source ./ant_global.sh
RevList=${1:-"$subjectloc/revisionList"}

while read N;
do
	~/bin/cloc-1.60.pl $subjectloc/source_revisions/$N/src/main \
		$subjectloc/source_revisions/$N/src/testcases | grep Java
done < ${RevList}

echo "All revisions are finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

