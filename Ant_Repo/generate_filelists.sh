#!/bin/bash

source ./ant_global.sh
BASEDIR=`pwd`/source_revisions
for N in $SEEDS;
do
	fnls=$BASEDIR/${N}/compilefiles.lst
	> $fnls

	for subdir in src/main src/testcases;
	do
		find $BASEDIR/$N/$subdir -name "*.java"  | \
			grep -v -E "FTP.java|NetRexxC.java|AntStarTeamCheckOut.java|XalanLiaison.java|XslpLiaison.java|XalanLiaisonTest.java|XslpLiaisonTest.java" | \
			grep -v -E "Script.java|EjbcHelper.java|DDCreatorHelper.java|Javah.java" | grep -v -E "optional\/ide" 1>> $fnls
	done

	find $BASEDIR/td/ -name "*.java" 1>> $fnls

	while read fn;
	do
		replace "assert(" "assertTrue(" -- $fn
	done < $fnls

	replace "((TestCase) t).name()" "((TestCase) t).getName()" -- \
		/home/hcai/SVNRepos/star-lab/trunk/Subjects/Ant_Repo/source_revisions/$N/src/main/org/apache/tools/ant/taskdefs/optional/junit/PlainJUnitResultFormatter.java
	replace "((TestCase) t).name()" "((TestCase) t).getName()" -- \
		/home/hcai/SVNRepos/star-lab/trunk/Subjects/Ant_Repo/source_revisions/$N/src/main/org/apache/tools/ant/taskdefs/optional/junit/XMLJUnitResultFormatter.java

	chmod a+w /home/hcai/SVNRepos/star-lab/trunk/Subjects/Ant_Repo/source_revisions/$N/src -R

	# determinize
	sh ./makeReplaceFileList.sh $N
	sh ./replaceTimingCalls.sh $subjectloc/source_revisions/$N/replaceFileList
done

exit 0

# hcai vim :set ts=4 tw=4 tws=4

