#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 base_revision next_revision [query] [number of tests]"
	exit 1
fi

RevBase=$1
RevNext=$2
#query=${3:-"<org.apache.tools.ant.Project: void addTaskDefinition(java.lang.String,java.lang.Class)>"}
query=${3:-" "}
NT=${4:-183}

source ./ant_global.sh

TraceLenFile=$subjectloc/source_revisions/$RevBase/EAInstrumented/traceLengths.txt

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$subjectloc/libs/xercesImpl-fixed:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Sensa/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin"
for i in $subjectloc/libs/*.jar;
do
	MAINCP=$MAINCP:$i
done

cp $subjectloc/inputs/testinputs.txt $subjectloc/source_revisions/

starttime=`date +%s%N | cut -b1-13`

cd "$subjectloc/source_revisions/$RevBase"
java -Xmx60g -ea -cp ${MAINCP} deam.RepoRunAnalysis \
	"$subjectloc/source_revisions/" \
	"$RevBase" \
	"$RevNext" \
	$DRIVERCLASS \
	"$query" \
	"$TraceLenFile" \
	$NT \
	10
cd "$subjectloc"

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for $RevBase vs. $RevNext elapsed: " `expr $stoptime - $starttime` milliseconds
exit 0

# hcai vim :set ts=4 tw=4 tws=4
