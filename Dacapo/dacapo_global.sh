#!/bin/bash

VERSION=v0
SEEDS="1 2 3 4 5 6 7"
ROOT=/home/hcai
DRIVERCLASS=org.dacapo.harness.Fop
subjectloc=$ROOT/SVNRepos/star-lab/trunk/Subjects/Dacapo/
C=(22034 13134 4028 32102 34315 31128 21486) # after insert the __link invocations

JAVA=$ROOT/tools/jdk160/bin/java
# hcai vim :set ts=4 tw=4 tws=4
