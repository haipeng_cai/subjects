#!/bin/bash
if [ $# -lt 1 ];then
	echo "Usage: $0 bm [scale]"
	exit 1
fi

bm=$1
scale=${2:-"default"}

source ./dacapo_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$subjectloc/jar/fop.jar"

#for i in $subjectloc/jar/*.jar;
for i in "avalon-framework-4.2.0.jar" "batik-all-1.7.jar" "commons-io-1.3.1.jar" "commons-logging-1.0.4.jar" "serializer-2.7.0.jar" "xmlgraphics-commons-1.3.1.jar"; 
do
	MAINCP=$MAINCP:$subjectloc/jar/$i
done

OUTDIR=Runout-uninstr
#mkdir -p $OUTDIR

args="-q $subjectloc/dat/fop/test.fo -ps test.ps"
if [ "$scale"="small" ];
then
	args="-q $subjectloc/dat/fop/readme.fo -pdf readme.pdf"
fi

function RunAllInOne()
{
	#java -Xmx4000m -ea -cp $MAINCP  $DRIVERCLASS "$args"
	#java -Xmx4000m -ea -cp $MAINCP -jar $subjectloc/dacapo-9.12-bach.jar $DRIVERCLASS "$args"
	#java -Xmx4000m -ea -cp $MAINCP -jar $subjectloc/dacapo-9.12-bach.jar
	#java -Xmx4000m -ea -cp $MAINCP -jar $subjectloc/dacapo-9.12-bach.jar fop
	#java -Xmx4000m -ea -cp $MAINCP -jar $subjectloc/dacapo-9.12-bach.jar fop "$args"
	#java -Xmx4000m -ea -cp $MAINCP -jar $subjectloc/dacapo-9.12-bach.jar fop "--size $scale"

	#java -Xmx4000m -ea -cp $MAINCP -jar $subjectloc/dacapo-9.12-bach.jar -size $scale fop 
	#java -Xmx4000m -ea -cp $MAINCP -jar $subjectloc/dacapo-9.12-bach.jar fop "--help"

	if [ $bm = "help" ]
	then
		java -Xmx4000m -ea -cp $MAINCP -jar $subjectloc/dacapo-9.12-bach.jar
	else
		java -Xmx4000m -ea -cp $MAINCP -jar $subjectloc/dacapo-9.12-bach.jar -size $scale $bm
	fi
}

starttime=`date +%s%N | cut -b1-13`
RunAllInOne
stoptime=`date +%s%N | cut -b1-13`
echo "Normal RunTime elapsed: " `expr $stoptime - $starttime` milliseconds
exit 0

# hcai vim :set ts=4 tw=4 tws=4

