#!/bin/bash
source ./jmeter-det_global.sh
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

INDIR=$subjectloc/tracerInstrumented-$ver$seed

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/Tracer/bin:$ROOT/tools/java_cup.jar:$INDIR"

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

starttime=`date +%s%N | cut -b1-13`

	#"-fullseq" 
java -Xmx10000m -ea -cp ${MAINCP} trace.Runner \
	"$subjectloc" \
	${ver}${seed} \
	$DRIVERCLASS \
	79 \
	-caching \
	15

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

#cp $INDIR/{varIds.out,staticCDInfo.dat,staticDDInfo.dat} $subjectloc/tracerOutdyn-${ver}${seed}/
cp $INDIR/{varIds.out,staticCDInfo.dat,stmtids.out} $subjectloc/tracerOutdyn-${ver}${seed}/

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

