#!/bin/bash
if [ $# -lt 1 ];then
	echo "Usage: $0 bm"
	exit 1
fi
source ./dacapo_global.sh

bm=$1

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Tracer/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$subjectloc/$bm/:$subjectloc"

mkdir -p out-TracerInstr

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Tracer/bin:$subjectloc/$bm/:$subjectloc

#for i in $subjectloc/lib/*.jar;
#do
#	SOOTCP=$SOOTCP:$i
#done
for i in "avalon-framework-4.2.0.jar" "batik-all-1.7.jar" "commons-io-1.3.1.jar" "commons-logging-1.0.4.jar" "serializer-2.7.0.jar" "xmlgraphics-commons-1.3.1.jar"; 
do
	SOOTCP=$SOOTCP:$subjectloc/jar/$i
done

OUTDIR=$subjectloc/tracerInstrumented-$bm/
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-debug \
	#-dumpJimple \
	#-wrapTryCatch \
   	#-duaverbose \
	#-dumpFunctionList \
	#-notracingusedval \
$JAVA -Xmx16g -ea -cp ${MAINCP} profile.EventInstrumenter \
	-w -cp ${SOOTCP} \
	-p cg verbose:false,implicit-entry:false -p cg.spark verbose:false,on-fly-cg:true,rta:true \
	-f c -d "$OUTDIR" -brinstr:off -duainstr:off \
	-defblacktypes "org.apache.xpath.objects.XObject,org.apache.xpath.objects.XNodeSet,org.apache.jorphan.collections.Data" \
	-useblacktypes "org.apache.xpath.objects.XObject,org.apache.xpath.objects.XNodeSet,org.apache.jorphan.collections.Data" \
	-wrapTryCatch \
	-dumpJimple \
	-notracingval \
	-slicectxinsens \
	-allowphantom \
	-staticCDInfo \
	-main-class ${DRIVERCLASS} -entry:${DRIVERCLASS} \
	-process-dir $subjectloc/$bm  \
	1>out-TracerInstr/instr-${bm}.out 2>out-TracerInstr/instr-${bm}.err
stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for ${bm} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Copying data to prepare for running the instrumented subject..."

echo "Running finished."
exit 0

# hcai vim :set ts=4 tw=4 tws=4

