#!/bin/bash
if [ $# -lt 1 ];then
	echo "Usage: $0 VersionSeed"
	exit 1
fi

verDir=$1
mkdir -p bin/$verDir

MAINCP=.:/afs/nd.edu/user36/hcai/workspace/InstrReporters/bin:libs/bc-jce-jdk13-114.jar:libs/commons-logging-api.jar:libs/commons-logging.jar:libs/junit3.8.1.jar:libs/log4j-1.2.8.jar:libs/style-apachexml.jar:libs/stylebook-1.0-b3_xalan-2.jar:libs/xalan.jar:libs/xercesImpl-fixed:libs/xml-apis.jar:libs/xmlParserAPIs.jar:libs/xercesImpl-fixed

#-source 1.4 -target 1.4
sed s'/\\/\//g' ${verDir}files.lst > /tmp/${verDir}files.lst
$ROOT/tools/jdk160/bin/javac -g:source -source 1.4 -cp ${MAINCP} -d bin/$verDir @/tmp/${verDir}files.lst  #1>err 2>&1

function compileOneByOne()
{
	> ${verDir}-compile.out
	cat ${verDir}files.lst | dos2unix | sed 's/\\/\//g' | while read fn;
	do
		echo "compiling $fn ....."
		#javac -g:source -source 1.4 -cp ${MAINCP} -d bin/$verDir $fn 1>/dev/null 2>&1
		javac -g:source -source 1.4 -cp ${MAINCP} -d bin/$verDir $fn 1>>compile.out 2>&1
		if [ $? -ne 0 ];
		then
			echo "Failed to compile $fn, bail out."
			exit 1
		fi
	done
}

if [ -d bin/${verDir}/profile ];
then
	rm -rf bin/${verDir}/profile
fi

echo "Compilation all done."
mkdir -p bin/$verDir/org/apache/xml/security/resource
#cp -fr resource/$verDir/* bin/$verDir/org/apache/xml/security/resource/
cp -fr resource/v1/* bin/$verDir/org/apache/xml/security/resource/
exit 0

