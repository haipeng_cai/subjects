#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3
source ./xmlsec_global.sh

INDIR=$subjectloc/execHistInstrumented-$ver-$seed-$change

#MAINCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/workspace/ProbSli/bin:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$ROOT/workspace/Sensa/bin:$ROOT/workspace/TestAdequacy/bin:$INDIR

MAINCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:/afs/nd.edu/user36/hcai/tools/polyglot-1.3.5/lib/polyglot.jar:/afs/nd.edu/user36/hcai/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:/afs/nd.edu/user36/hcai/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/DUAForensics:/afs/nd.edu/user36/hcai/workspace/Sensa/bin:/afs/nd.edu/user36/hcai/tools/java_cup.jar:/afs/nd.edu/user36/hcai/workspace/ProbSli/bin:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/LocalsBox:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/InstrReporters:/afs/nd.edu/user36/hcai/workspace/ProbSli/bin:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$INDIR

OUTDIR=$subjectloc/IndependentExechist/outdyn-${ver}${seed}-$change
mkdir -p $OUTDIR

i=0
cat inputs/testinputs.txt | while read test;
do
	let i=i+1
	echo "Test No. $i: $test "
	java -Xmx10000m -ea -cp ${MAINCP} \
		org.apache.xml.security.test.AllTestsSelect $test \
		1> $OUTDIR/$i.out \
		2> $OUTDIR/$i.err
done

exit 0

