#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

source ./xmlsec_global.sh
INDIR=$subjectloc/DynwsliInstrumented-$ver-$seed-$change

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin"

mkdir -p out-instrInstrumented

#$ROOT/tools/j2re1.4.2_18/lib/rt.jar:
SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/ProbSli/bin:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$INDIR:$ROOT/workspace/Sensa/bin"

OUTDIR=$subjectloc/InstrInstrumented-$ver-$seed-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`

java -Xmx1600m -ea -cp ${MAINCP} Sensa.SensaInst \
	-w -cp $SOOTCP -p cg verbose:true,implicit-entry:false \
	-p cg.spark verbose:true,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
   	-duaverbose \
	-slicectxinsens \
	-allowphantom \
	-exechist \
	-start:$change \
	-main-class org.apache.xml.security.test.AllTestsSelect_uninstr \
	-entry:org.apache.xml.security.test.AllTestsSelect_uninstr \
	-process-dir $INDIR \
	1>out-instrInstrumented/instr-$change-${ver}${seed}.out 2>out-instrInstrumented/instr-$change-${ver}${seed}.err

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for $change-${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

#cp -fr resource $OUTDIR/org/apache/xml/security

echo "Copying data to prepare for running the instrumented subject..."
mkdir -p $OUTDIR/org/apache/xml/security/resource
mkdir -p $OUTDIR/org/apache/xml/security/test/resource
cp -fr resource/$ver/* $OUTDIR/org/apache/xml/security/resource/
#cp -fr resource/td$ver/* $OUTDIR/org/apache/xml/security/test/resource/
cp -fr resource/test/* $OUTDIR/org/apache/xml/security/test/resource/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

