#!/bin/bash

for N in 2 3 5 14 16 17 20;
do
	echo "Now Running Uninstrumented v1 s$N ..."
	sh run_uninstr.sh v1 s$N-orig
done

echo "Normal Running now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

