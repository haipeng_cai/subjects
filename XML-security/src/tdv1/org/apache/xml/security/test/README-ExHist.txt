======= NOTICE =========
Workaround versions of the Test Driver classes in this directory are duplicated in order to, as a makeshift for now,
adapt to different needs. With ${Driver} in {AllTests, AllTestsSelect, AllTestsSorted}:

1. ${Driver}.java files are those that are inserted a cheating block of code that will never be executed to enable
Soot/DUAForensics to retrieve dependencies necessary for analysis applications like backward slicing.

2. ${Driver}_ExHist.java files are duplicated from the ${Driver}.java ones but with the cheating block removed to bypass
a current flaw in ExecHistoryInstrumenter that would cause runtime exceptions like JavaVerifyError or so with the
cheating code added. USE these drivers if execution history instrumentation is needed for Xml-security. 

