#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 sensaRankingFile actualRankingFile FwdSliceFile"
	exit 1
fi

fnSensaRank=$1
fnActualRank=$2
fnFwdslices=$3

source ./xmlsec_global.sh

cat $fnActualRank | gawk 'BEGIN {
					# read the FwdSliceFile into an array
					while ( getline < "'$fnFwdslices'" )
					{
						id = $0
						if ( id ~ /\n$/ )
						{
							id = substr(id,0,length(id)-1)
						}
						FwdSlices[id] = 1
					}

					# read the sensaRankingFile into an array
					FS=" : "
					while ( getline < "'$fnSensaRank'" )
					{
						split($0, a)
						SensaRank[a[1]] = 1
					}
				} 

				{
					split($0, b)
					if ( ! b[1] in SensaRank )
					{
						if ( b[1] in FwdSlices )
						{
							printf("%s : 0.0\n", b[1]);
						}
					}
				}'

exit 0

# hcai vim :set ts=4 tw=4 tws=4

