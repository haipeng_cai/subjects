#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3
source ./xmlsec_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin"

#INDIR=$subjectloc/instrumented-$ver-$seed-$change
#INDIR=$subjectloc/bin/tdv1:$subjectloc/bin/${ver}${seed}
#INDIR=$subjectloc/execHistInstrumented-$ver-$seed-$change
#INDIR=$subjectloc/MutEAInstrumented-$ver$seed
INDIR=$subjectloc/tracerInstrumented-$ver$seed

#$ROOT/tools/j2re1.4.2_18/lib/rt.jar:
SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/ProbSli/bin:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$ROOT/workspace/Sensa/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin:$INDIR:$ROOT/workspace/Dyndep/bin


	#org.apache.xml.security.c14n.Canonicalizer \
	#org.apache.xml.security.test.AllTestsSelect \
	#org.apache.xml.security.test.external.org.apache.xalan.XPathAPI.AttributeAncestorOrSelf \
	#org.apache.xml.security.test.interop.IAIKTest \
	#org.apache.xml.security.test.AllTestsSelect \
java -Xmx1600m -ea -cp ${MAINCP} soot.Main -f J -cp ${SOOTCP} \
	org.apache.xml.security.utils.CachedXPathFuncHereAPI \
	-d `pwd`


exit 0


# hcai vim :set ts=4 tw=4 tws=4

