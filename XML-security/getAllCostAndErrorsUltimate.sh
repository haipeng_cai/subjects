python ./tabulateCostAndError.py  \
	ultimateResults/costEffectiveness  \
	ultimateWslicingResults/costEffectiveness \
	ultimateResults/IdealCostEffectiveness  \
	ultimateResults/errorMetrics  \
	ultimateWslicingResults/errorMetrics \
	ultimateResults/IdealErrorMetrics 

# hcai vim :set ts=4 tw=4 tws=4

