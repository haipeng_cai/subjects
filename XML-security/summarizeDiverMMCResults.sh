#!/bin/bash
title=${1:-"1"}
resdir=${2:-"MMC_results"}
allsubjects="Schedule1 Nano XML-security JMeter-det Ant-det Jaba ArgoUML"
root=/home/hcai/SVNRepos/star-lab/trunk/Subjects/
for subject in ${allsubjects};
do
	if [ $title -eq 1 ];
	then
		echo -e "\t================================"
		echo -e "\t===== for Subject $subject ====="
		echo -e "\t================================"
	fi
	for s in 2 3 4 5 6 7 8 9 10;
	do
		if [ $title -eq 1 ];
		then
			echo -e "\t----- query group size=$s -----"
		fi
		#ls -l $root/$subject/MMC_results/result_s_$s
		fn=$root/$subject/$resdir/result_s_$s
		tail -n100 $fn | awk '{if ($1=="average" || $1=="stdev") print $0}'
	done
done
