#!/bin/bash

for N in 2 3 5 14 16 17 20;
do
	echo "Now instrumenting s$N ..."
	sh DeamRun.sh v1 s$N 
	echo "Now instrumenting s$N-orig ..."
	sh DeamRun.sh v1 s$N-orig 
done

exit 0


# hcai vim :set ts=4 tw=4 tws=4

