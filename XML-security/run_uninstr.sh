#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi
ver=$1
seed=$2
source ./xmlsec_global.sh

MAINCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:/afs/nd.edu/user36/hcai/tools/polyglot-1.3.5/lib/polyglot.jar:/afs/nd.edu/user36/hcai/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:/afs/nd.edu/user36/hcai/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/DUAForensics:/afs/nd.edu/user36/hcai/workspace/Sensa/bin:/afs/nd.edu/user36/hcai/tools/java_cup.jar:/afs/nd.edu/user36/hcai/workspace/ProbSli/bin:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/LocalsBox:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/InstrReporters:/afs/nd.edu/user36/hcai/workspace/ProbSli/bin:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$subjectloc/bin/td$ver:$subjectloc/bin/${ver}${seed}

OUTDIR=Runout-${ver}${seed}-uninstr
mkdir -p $OUTDIR

function RunAllInOne()
{
	# to run all test in one
	java -Xmx1600m -ea -cp $MAINCP org.apache.xml.security.test.AllTests #1> $OUTDIR/1.out 2> $OUTDIR/1.err
}

function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	#cat $subjectloc/test_names.txt | dos2unix | \
	cat inputs/testinputs.txt | \
	while read testname;
	do
		let i=i+1

		echo "Run Test #$i.....$testname"
		java -Xmx4000m -ea -cp $MAINCP org.apache.xml.security.test.AllTestsSelect \
	   	$testname 1> $OUTDIR/$i.out 2> $OUTDIR/$i.err
	done
}

starttime=`date +%s%N | cut -b1-13`
#RunOneByOne
RunAllInOne
stoptime=`date +%s%N | cut -b1-13`
echo "Normal RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0


