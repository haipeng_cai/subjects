#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi
ver=$1
seed=$2
source ./xmlsec_global.sh

MAINCP=".:/afs/nd.edu/user36/hcai/tools/polyglot-1.3.5/lib/polyglot.jar:/afs/nd.edu/user36/hcai/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:/afs/nd.edu/user36/hcai/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/DUAForensics:/afs/nd.edu/user36/hcai/workspace/Sensa/bin:/afs/nd.edu/user36/hcai/tools/java_cup.jar:/afs/nd.edu/user36/hcai/workspace/ProbSli/bin:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/LocalsBox:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/InstrReporters:/afs/nd.edu/user36/hcai/workspace/ProbSli/bin:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$subjectloc/bin/$ver$seed:$subjectloc/bin/td$ver"

#cp -fr resource $subjectloc/bin/td$ver/org/apache/xml/security/
java -Xmx1600m -ea -cp $MAINCP org.apache.xml.security.test.AllTests #1> $OUTDIR/1.out 2> $OUTDIR/1.err

