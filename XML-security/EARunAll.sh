#!/bin/bash
for N in 2 3 5 14 16 17 20;
do
	echo "Now Running instrumented s$N ..."
	sh EARun.sh v1 s$N

done

echo "Running phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

