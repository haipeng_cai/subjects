#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi
source ./xmlsec_global.sh

ver=$1
seed=$2

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Tracer/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

mkdir -p out-TracerInstr

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Tracer/bin:$subjectloc/libs/xercesImpl-fixed:$subjectloc/bin/${ver}${seed}:$subjectloc/bin/td${ver}

for i in $subjectloc/libs/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/tracerInstrumented-$ver$seed
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-debug \
	#-dumpJimple \
	#-wrapTryCatch \
   	#-duaverbose \
	#-dumpFunctionList \
	#-defblacktypes org.apache.xml.security.utils.CachedXPathFuncHereAPI
	#blacktypes "org.apache.xpath.objects.XObject,org.apache.xpath.objects.XNodeSet,org.apache.jorphan.collections.Data,org.w3c.dom.Element,org.w3c.dom.Document,org.w3c.dom.Node" \
	#blacktypes "org.apache.xpath.objects.XObject,org.apache.xpath.objects.XNodeSet,org.apache.jorphan.collections.Data,org.apache.xml.security.transforms.Transforms,org.apache.xml.security.signature.XMLSignatureInput,java.io.ByteArrayInputStream,java.lang.String,org.w3c.dom.Element,org.w3c.dom.Document,org.w3c.dom.Node,boolean" \
	#-notracingusedval \
$JAVA -Xmx14g -ea -cp ${MAINCP} profile.EventInstrumenter \
	-w -cp ${SOOTCP} \
	-p cg verbose:false,implicit-entry:false -p cg.spark verbose:false,on-fly-cg:true,rta:true \
	-f c -d "$OUTDIR" -brinstr:off -duainstr:off \
	-defblacktypes "org.apache.xpath.objects.XObject,org.apache.xpath.objects.XNodeSet,org.apache.jorphan.collections.Data" \
	-useblacktypes "org.apache.xml.security.signature.XMLSignatureInput,org.apache.xpath.objects.XObject,org.apache.xpath.objects.XNodeSet,org.apache.jorphan.collections.Data" \
	-wrapTryCatch \
	-slicectxinsens \
	-allowphantom \
	-notracingval \
	-staticCDInfo \
	-main-class ${DRIVERCLASS} -entry:${DRIVERCLASS} \
	-process-dir $subjectloc/bin/${ver}${seed}  \
	-process-dir $subjectloc/bin/td${ver} \
	1>out-TracerInstr/instr-${ver}${seed}.out 2>out-TracerInstr/instr-${ver}${seed}.err
stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Copying data to prepare for running the instrumented subject..."
mkdir -p $OUTDIR/org/apache/xml/security/resource
mkdir -p $OUTDIR/org/apache/xml/security/test/resource
cp -fr resource/$ver/* $OUTDIR/org/apache/xml/security/resource/
cp -fr resource/test/* $OUTDIR/org/apache/xml/security/test/resource/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

