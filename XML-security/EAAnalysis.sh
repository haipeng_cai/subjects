#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [number of traces]"
	exit 1
fi

ver=$1
seed=$2
NT=${3:-"92"}
#query=${4:-"main"}
query=${4:-"<org.apache.xml.security.signature.XMLSignatureException: void <init>(java.lang.String,java.lang.Object[])>"}
#query=${4:-"<org.apache.xml.security.test.signature.XMLSignatureInputTest: java.util.Set getNodeSet1()>"}
query=${4:-"<org.apache.xml.security.test.c14n.implementations.Canonicalizer20010315Test: void <init>(java.lang.String)>"}
query=${4:-"<org.apache.xml.security.signature.XMLSignatureInput: java.util.Set getNodeSet()>"}
query=${4:-"<org.apache.xml.security.c14n.Canonicalizer: java.lang.String getXPATH_C14N_WITH_COMMENTS_SINGLE_NODE()>"}

source ./xmlsec_global.sh

INDIR=$subjectloc/EAoutdyn-${ver}${seed}

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$INDIR"

starttime=`date +%s%N | cut -b1-13`
	#"main" \
	#"ExclusiveC14NInterop" \
	#"AllTestsSelect::sortTestSuite" \
	#-debug
java -Xmx2800m -ea -cp ${MAINCP} EAS.EAAnalysis \
	"$query" \
	"$INDIR" \
	$NT 

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

