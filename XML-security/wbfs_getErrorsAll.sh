#!/bin/bash
# these are the real change locations to instrument at
C=(5620 5659 6159 4286 4946 4164 13932)  # after improved

RESDIR=`pwd`/errorMetrics
mkdir -p $RESDIR

for algo in rand inc observed;
do
	echo " -------- For the $algo Strategy ------- "
	i=0
	for N in 2 3 5 14 16 17 20;
	do
		echo -n "Now calculating errors for v1 s$N at ${C[$i]} ... "
		sh ./wbfs_getErrors.sh ${C[$i]} v1 s$N $algo > $RESDIR/Error-v1s$N-${C[$i]}-$algo

		let i=i+1
		echo " done."
	done
done

echo "Ranking phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

