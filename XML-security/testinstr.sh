#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2
source ./xmlsec_global.sh

SOOTLIBDIR=/mnt/filedisk/work/duaf/Soot/Soot-libs-2.3.0
SOOTLIBS=$SOOTLIBDIR/jasminclasses-2.3.0.jar:$SOOTLIBDIR/java_cup-1.3.5.jar:$SOOTLIBDIR/jedd-runtime.jar:$SOOTLIBDIR/paddle-0.2.jar:$SOOTLIBDIR/polyglot-1.3.5.jar:$SOOTLIBDIR/sootclasses-2.3.0.jar

MAINCP=".:/usr/java/jdk1.6.0_45/jre/lib/rt.jar:$SOOTLIBS:/mnt/filedisk/work/duaf/Soot/DUAForensics/bin:/mnt/filedisk/work/duaf/Soot/TestAdequacy/bin:/mnt/filedisk/work/duaf/Soot/Sensa/bin:/mnt/filedisk/work/duaf/Soot/ProbSli/bin"

SOOTCP=".:/usr/java/jdk1.6.0_45/jre/lib/rt.jar:/mnt/filedisk/work/duaf/Soot/LocalsBox/bin:/mnt/filedisk/work/duaf/Soot/InstrReporters/bin:/mnt/filedisk/work/duaf/Soot/DUAForensics/bin:/mnt/filedisk/work/duaf/Soot/ProbSli/bin:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$subjectloc/bin/${ver}${seed}:$subjectloc/bin/td$ver:/mnt/filedisk/work/duaf/Soot/Sensa/bin:/mnt/filedisk/work/duaf/Soot/TestAdequacy/bin"

OUTDIR=$subjectloc/test-instrumented-$ver-$seed
mkdir -p $OUTDIR

mkdir -p out-test-instr

starttime=`date +%s%N | cut -b1-13`

java -Xmx20480m -ea -cp ${MAINCP} InstrumentationTester \
    -w -cp $SOOTCP -p cg verbose:true,implicit-entry:false \
    -p cg.spark verbose:true,on-fly-cg:true,rta:true -f c \
    -d $OUTDIR \
    -brinstr:off -duainstr:off \
    -duaverbose \
    -slicectxinsens \
    -allowphantom \
    -paramdefuses \
    -keeprepbrs \
    -main-class org.apache.xml.security.test.AllTestsSelect \
    -entry:org.apache.xml.security.test.AllTestsSelect \
    -process-dir $subjectloc/bin/${ver}${seed} \
    -process-dir $subjectloc/bin/td${ver} \
    1>out-test-instr/instr-${ver}${seed}.out 2>out-test-instr/instr-${ver}${seed}.err

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds >> overhead.instrumentation.txt

#cp -fr resource $OUTDIR/org/apache/xml/security

echo "Copying data to prepare for running the instrumented subject..."
mkdir -p $OUTDIR/org/apache/xml/security/resource
mkdir -p $OUTDIR/org/apache/xml/security/test/resource
cp -fr resource/$ver/* $OUTDIR/org/apache/xml/security/resource/
#cp -fr resource/td$ver/* $OUTDIR/org/apache/xml/security/test/resource/
cp -fr resource/test/* $OUTDIR/org/apache/xml/security/test/resource/

echo "Running finished."
exit 0

# hcai vim :set ts=4 tw=4 tws=4

