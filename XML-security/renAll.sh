#!/bin/bash

ls -dl wsliceExechist_outdyn-v1* | \
	while read a;
	do 
		echo ${a}
		mkdir -p ResultWslicing/${a#*_}
		mv ./${a}/*-observed/*  ResultWslicing/${a#*_}/
	done

# hcai vim :set ts=4 tw=4 tws=4

