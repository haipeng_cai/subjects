#!/usr/bin/env python
import os
import sys
import string
import subprocess
import random
import time

# structure of the Accuracy table: 
# {change:{query:[[EA Impact set],EAAnalysisTime, [Diver Impact set], DiverAnalysisTime]}}
g_dataTable= dict()
# structure of the Result table: 
# {change:{query:[[EA],[Diver],[EA-Diver],[Diver-EA],Impact set size Diver/EA, EATime, DiverTime, analysis cost Diver/EA]}}
g_resultTable= dict()

# executables
g_DiverBin = os.getcwd()+"/DiverAnalysis.sh"
g_EABin = os.getcwd()+"/EAAnalysis.sh"

# data source locations
g_VerPrefix="v1"
g_Seeds=[2,3,5,14,16,17,20]

# optional inputs
#g_fnQueryFile= os.getcwd()+"/functionList.out"
g_fnQueryFile="" 
g_nTest=92

# number of queries as samples
g_nQueries=-1

def Usage():
	print >> sys.stdout, "%s [versionId] [seedId,..] [file of queries] [number of traces] " % sys.argv[0]
	return

def ParseCommandLine():
	global g_VerPrefix
	global g_Seeds
	global g_fnQueryFile
	global g_nTest
	argc = len(sys.argv)
	if argc >= 3:
		g_VerPrefix = sys.argv[1]
		g_Seeds=list()
		for s in string.split(sys.argv[2],','):
			g_Seeds.append( s )
		if argc >= 4:
			g_fnQueryFile = sys.argv[3]
			if not os.path.exists( g_fnQueryFile ):
				raise IOError, "the file of queries [%s] does not exist, bailed out now." % g_fnQueryFile
		if argc >= 5:
			g_nTest = int(sys.argv[4])
	else:
		g_Seeds = ['s'+str(v) for v in g_Seeds]
	
	'''check existence of binaries'''
	global g_DiverBin, g_EABin
	for binary in (g_DiverBin, g_EABin):
		if not os.path.exists( binary ):
			raise IOError, "required executable [%s] does not exist, bailed out now." % binary 
	print "command line: %s %s %s %s %d" % (sys.argv[0], g_VerPrefix, g_Seeds, g_fnQueryFile, g_nTest)

def collectData():
	global g_dataTable
	global g_fnQueryFile
	global g_nTest
	global g_nQueries

	bUsingSingleQueryList=True
	AllQueries=list()
	if len(g_fnQueryFile) >= 3:
		'''open the file of queries, namely input function list, fetch each one as a single query at a time'''
		qfh = file(g_fnQueryFile, "r")
		if None == qfh:
			raise Exception, "Failed to open query file named %s\n" % g_fnQueryFile 
		AllQueries = qfh.readlines()
		qfh.close()
	else:
		bUsingSingleQueryList=False

	for seed in g_Seeds:
		change = "%s%s" % (g_VerPrefix, seed)
		if bUsingSingleQueryList != True:
			g_fnQueryFile= "%s/EAInstrumented-%s/functionList.out" % (os.getcwd(), change)
			if not os.path.exists( g_fnQueryFile ):
				print >> sys.stderr,  "the file of queries [%s] does not exist, bailed out now." % g_fnQueryFile
				continue
			'''open the file of queries, namely input function list, fetch each one as a single query at a time'''
			qfh = file(g_fnQueryFile, "r")
			if None == qfh:
				print >> sys.stderr, "%s skipped because: failed to open query file named %s\n" % (change, g_fnQueryFile)
				continue
			AllQueries = qfh.readlines()
			qfh.close()

		bRandomSampling=True
		
		if g_nQueries<1:
			g_nQueries=len(AllQueries)
			bRandomSampling=False
		else:
			if len(AllQueries) < g_nQueries:
				g_nQueries = len(AllQueries)

		## query = qfh.readline()
		## while query:
		for k in range(0, g_nQueries):
			qidx=k
			if bRandomSampling:
				qidx=int(random.random()*(len(AllQueries)-1))
			query = AllQueries[ qidx ]
			query = query.lstrip().rstrip(' \r\n')
			''' get EAS result '''
			efh=None
			cmd=None
			try:
				efh = file ('/tmp/x', 'w')
				cmd = [str(g_EABin), g_VerPrefix, seed, str(g_nTest), query]
				rcEA = subprocess.check_call( cmd, stdout=efh, stderr=subprocess.STDOUT )
			except subprocess.CalledProcessError,e:
				print >> sys.stderr, "call %s failed, skipped [ret code=%d]." % \
					( str(cmd).strip(','), e.returncode )
				## query = qfh.readline()
				continue
			except Exception,e:
				print >> sys.stderr, "Unexpected error when calling %s, halt now with error=%s" % \
					( str(cmd).strip(','), e )
				sys.exit(-1)
			efh.flush()
			efh.close()

			''' get Diver result '''
			dfh=None
			try:
				dfh = file ('/tmp/y', 'w')
				cmd = [str(g_DiverBin), g_VerPrefix, seed, str(g_nTest), query]
				rcDiver = subprocess.check_call( cmd, stdout=dfh, stderr=subprocess.STDOUT )
			except subprocess.CalledProcessError,e:
				print >> sys.stderr, "call %s failed, skipped [ret code=%d]." % \
					( str(cmd).strip(','), e.returncode )
				## query = qfh.readline()
				continue
			except Exception,e:
				print >> sys.stderr, "Unexpected error when calling %s, halt now with error=%s" % \
					( str(cmd).strip(','), e )
				sys.exit(-1)
			dfh.flush()
			dfh.close()

			''' now read query results from EAS and Diver respectively, format-dependent text parsing'''
			EAImpactSet=set()
			EATime=0
			esz=0
			efh = file('/tmp/x','r')
			efh.seek(0,0)
			bstart = False
			curline = efh.readline()
			while curline:
				if bstart==False:
					bstart = (-1!=curline.find("Change Impact Set"))
					if bstart==True:
						esz = int( string.split( curline )[-1] )
					curline = efh.readline()
					continue
				if curline.find("RunTime for")==-1:
					EAImpactSet.add( curline.lstrip().rstrip(' \r\n') )
				else:
					flds = string.split( curline )
					assert len(flds)>=3
					EATime = int( flds[len(flds)-2] )
					break
				curline = efh.readline()
			#print "%d : %d" % (len(EAImpactSet), esz)
			assert len(EAImpactSet)==esz

			if esz < 1:
				print >> sys.stderr, "EAS got an uncovered query: %s, skipped." % (query)
				continue
				
			DiverImpactSet=set()
			DiverTime=0
			dsz=0
			dfh = file('/tmp/y','r')
			dfh.seek(0,0)
			bstart = False
			curline = dfh.readline()
			if curline!=None and -1!=curline.find("Invalid queries"):
				print >> sys.stderr, "Diver got an Invalid query: %s, skipped." % (query)
				## query = qfh.readline()
				continue
			while curline:

				if bstart==False:
					bstart = (-1!=curline.find("Change Impact Set of All Changes"))
					if bstart==True:
						dsz = int( string.split( curline )[-1] )
					curline = dfh.readline()
					continue
				if curline.find("RunTime for")==-1:
					DiverImpactSet.add( curline.lstrip().rstrip(' \r\n') )
				else:
					flds = string.split( curline )
					assert len(flds)>=3
					DiverTime = int( flds[len(flds)-2] )
					break
				curline = dfh.readline()
			#print "%d : %d" % (len(DiverImpactSet), dsz)
			assert len(DiverImpactSet)==dsz

			print "Done with query [%s]." % (query)

			''' save results to the global data table '''
			if change not in g_dataTable.keys():
				g_dataTable[change] = dict()
			if query not in g_dataTable[change].keys():
				g_dataTable[change][query] = list()

			g_dataTable[change][query].append( EAImpactSet )
			g_dataTable[change][query].append( EATime )
			g_dataTable[change][query].append( DiverImpactSet )
			if DiverTime < 1:
				print >> sys.stderr, "ERROR: %s with query %s by Diver costs 0!" % \
						(change, query)
				sys.exit(-2)
			g_dataTable[change][query].append( DiverTime )

			# remove temporary files
			os.remove("/tmp/x")
			os.remove("/tmp/y")

			## query = qfh.readline()

		print "Done with %s-%s." % (g_VerPrefix, seed)
		## qfh.seek(0,0)
	# // all seeds
	## qfh.close()

# result Data structure:
# {change:{query:[[EA],[Diver],[EA-Diver],[Diver-EA],Impact set size Diver/EA, EATime, DiverTime, analysis cost Diver/EA]}}
def computeResults():
	global g_dataTable
	global g_resultTable
	for chg in g_dataTable.keys():
		if chg not in g_resultTable.keys():
			g_resultTable[chg] = dict()
		for query in g_dataTable[chg].keys():
			if query not in g_resultTable[chg].keys():
				g_resultTable[chg][query] = list()
				
			EAIS = g_dataTable[chg][query][0]
			EATime = g_dataTable[chg][query][1]
			DiverIS = g_dataTable[chg][query][2]
			DiverTime = g_dataTable[chg][query][3]
			EDDiff = EAIS - DiverIS
			DEDiff = DiverIS - EAIS
			if len(EAIS)<1:
				DESizeRatio=-1
			else:
				DESizeRatio = float( "%.4f" % (len(DiverIS)*1.0/len(EAIS)) )
			if DiverTime<=0:
				EDTimeRatio=-1
			else:
				EDTimeRatio = float( "%.4f" % (EATime*1.0/DiverTime) )

			g_resultTable[chg][query]= [ EAIS, DiverIS, EDDiff, DEDiff, DESizeRatio, EATime, DiverTime, EDTimeRatio ]

def dumpData():
	global g_dataTable
	global g_nTest

	for chg in g_dataTable.keys():
		print "===== chang=%s [from %d tests] ==== " % (chg, g_nTest)
		for query in g_dataTable[chg].keys():
			print "EAS impact set of [%s] costing %d milliseconds " % (query, g_dataTable[chg][query][1])
			for m in g_dataTable[chg][query][0]:
				print "\t%s" % m
			print "Diver impact set of [%s] costing %d milliseconds " % (query, g_dataTable[chg][query][3])
			for m in g_dataTable[chg][query][2]:
				print "\t%s" % m

def dumpResults():
	global g_resultTable

	# table title
	print >> sys.stdout, "change\tquery\tEA\tDiver\tEA-Diver\tDiver-EA\tSize:Diver/EA\tEATime\tDiverTime\tTime: EA/Diver"
	print
	AllSumSize=[0,0]
	AllSumTime=[0,0]
	for chg in g_resultTable.keys():
		SumSizePerChg=[0,0]
		SumTimePerChg=[0,0]
		for query in g_resultTable[chg].keys():
			print >> sys.stdout, chg, "\t", query, "\t", \
					len( g_resultTable[chg][query][0]), "\t", len (g_resultTable[chg][query][1]), "\t", \
					len( g_resultTable[chg][query][2]), "\t", len (g_resultTable[chg][query][3]), "\t", \
					str(g_resultTable[chg][query][4]*100)+"%", "\t", \
					g_resultTable[chg][query][5], "\t", g_resultTable[chg][query][6], "\t", \
					str(g_resultTable[chg][query][7]*100)+"%"

			AllSumSize[0] += len(g_resultTable[chg][query][0])
			AllSumSize[1] += len(g_resultTable[chg][query][1])
			SumSizePerChg[0] += len(g_resultTable[chg][query][0])
			SumSizePerChg[1] += len(g_resultTable[chg][query][1])

			AllSumTime[0] += g_resultTable[chg][query][5]
			AllSumTime[1] += g_resultTable[chg][query][6]
			SumTimePerChg[0] += g_resultTable[chg][query][5]
			SumTimePerChg[1] += g_resultTable[chg][query][6]

		sizeRatioPerChg=None
		if SumSizePerChg[0]<1:
			sizeRatioPerChg=-1
		else:
			sizeRatioPerChg=SumSizePerChg[1]*1.0/SumSizePerChg[0]
		print >> sys.stdout, "%s Size: Diver/EA is %f%%" % \
				(chg, sizeRatioPerChg*100)
		timeRatioPerChg=None
		if SumTimePerChg[1]<1:
			timeRatioPerChg=-1
		else:
			timeRatioPerChg=SumTimePerChg[0]*1.0/SumTimePerChg[1]
		print >> sys.stdout, "%s Time: EA/Diver is %f%%" % \
				(chg, timeRatioPerChg*100)

	if AllSumSize[0]>=1:
		print >> sys.stdout, "aggregate Size: Diver/EA is %f%%" % \
				(AllSumSize[1]*1.0/AllSumSize[0]*100)
	else:
		print >> sys.stdout, "Something might have been wrong: all EAS impact sets are empty."
	if AllSumTime[1]>=1:
		print >> sys.stdout, "aggregate Time: EA/Diver is %f%%" % \
				(AllSumTime[0]*1.0/AllSumTime[1]*100)
	else:
		print >> sys.stdout, "Something might have been wrong: all Diver analysis cost zero in total."

######################################
# the boost
if __name__ == "__main__":
	try:
		ParseCommandLine()
	except Exception,e:
		print >> sys.stderr, e
		sys.exit(1)

	#time.sleep(28800)

	collectData()
	computeResults()
	dumpData()
	dumpResults()

	sys.exit(0)

# hcai vim set ts=4 tw=100 sts=4 sw=4


