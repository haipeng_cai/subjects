#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 source1 source2"
	exit 1
fi

source1=$1
source2=$2

source ./xmlsec_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin"

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$subjectloc/libs/xercesImpl-fixed:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/Sensa/bin:$ROOT/workspace/ProbSli/bin:$subjectloc/bin/${ver}${seed}:$subjectloc/bin/td${ver}
for i in $subjectloc/libs/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

suffix=${ver}${seed}

LOGDIR=out-EHInstr
#mkdir -p $LOGDIR
logout=$LOGDIR/instr-$suffix.out
logerr=$LOGDIR/instr-$suffix.err

starttime=`date +%s%N | cut -b1-13` 

java -Xmx6600m -ea -cp ${MAINCP} deam.AlignStmt \
	$source1 \
	$source2

stoptime=`date +%s%N | cut -b1-13`
echo "AnalysisTime for $suffix elapsed: " `expr $stoptime - $starttime` milliseconds
exit 0


# hcai vim :set ts=4 tw=4 tws=4

