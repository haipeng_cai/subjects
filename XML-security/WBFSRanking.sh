#!/bin/bash
if [ $# -lt 4 ];then
	echo "Usage: $0 changeLoc version seed algo"
	exit 1
fi

change=$1
ver=$2
seed=$3
algo=$4

sensaRankFile=ultimateResults/predictedImpactRanking-$ver$seed-$change-$algo

source ./xmlsec_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin/:$ROOT/workspace/InstrReporters/bin/:$ROOT/workspace/WeightedBFS/bin"

SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/WeightedBFS/bin:$ROOT/workspace/TestAdequacy/bin/:$ROOT/workspace/Sensa/bin:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$subjectloc/bin/${ver}${seed}-orig:$subjectloc/bin/td$ver"

mkdir -p log-wbfs

OUTDIR=$subjectloc/wbfs-$ver-$seed-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`

	#-bfs \
	#-dsssp \
	#-dssspex \
	#-untied \
	#-debug \
java -Xmx6000m -ea -cp ${MAINCP} WBFSRanking \
	-w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
   	-duaverbose \
	-sensaRanking $sensaRankFile \
	-version "$ver$seed-$algo" \
	-slicectxinsens \
	-untied \
	-start:$change \
	-main-class org.apache.xml.security.test.AllTestsSelect \
	-entry:org.apache.xml.security.test.AllTestsSelect \
	-process-dir $subjectloc/bin/${ver}${seed}-orig \
	-process-dir $subjectloc/bin/td${ver} \
	1>log-wbfs/wbfs-$change-${ver}${seed}.out \
	2>log-wbfs/wbfs-$change-${ver}${seed}.err

echo "WBFS Ranking finished."

stoptime=`date +%s%N | cut -b1-13`
echo "WBFS Ranking Time for $change-${ver}${seed}-$algo elapsed: " `expr $stoptime - $starttime` milliseconds
exit 0


# hcai vim :set ts=4 tw=4 tws=4

