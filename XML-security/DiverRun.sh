#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2
source ./xmlsec_global.sh

INDIR=$subjectloc/DiverInstrumented-$ver$seed

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$INDIR"

OUTDIR=Diveroutdyn-${ver}${seed}
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`

java -Xmx2800m -ea -cp ${MAINCP} Diver.DiverRun \
	org.apache.xml.security.test.AllTestsSelect \
	"$subjectloc" \
	"$INDIR" \
	${ver}${seed} \
	$OUTDIR 

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

