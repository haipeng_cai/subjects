#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 stmtList rankfile"
	exit 1
fi

stmtsfn=$1
rankfn=$2

while read stid;
do
	cnt=`grep -a -c ${stid} $rankfn`
	if [ $cnt -lt 1 ];then
		echo "$stid" not found in the given ranking, skipped.
		continue
	fi
	cat $rankfn | awk '{ if ($1=='$stid') printf("%s : %.1f\n", $1, $3); }'
done < $stmtsfn;

exit 0


# hcai vim :set ts=4 tw=4 tws=4

