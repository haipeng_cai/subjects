#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 srcdir dstfile"
	exit 1
fi
srcdir=$1
dstfile=$2

source ./xmlsec_global.sh
CURVEFILE=curveEffectiveness-${ver}${seed}-$change

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/TestAdequacy/bin:$ROOT/tools/java_cup.jar"

#C=(5614 5655 6159 4282 4942 4170 13932) # after corrections
C=(5620 5659 6159 4286 4946 4164 13932)  # after improved
N=(2 3 5 14 16 17 20)
for algo in inc rand observed;
do
	inputfiles=""
	for ((i=0;i<${#C[@]};i++));
	do
		inputfiles=$inputfiles" "${srcdir}/curveEffectiveness-v1s${N[$i]}-${C[$i]}-$algo
	done

	java -Xmx1600m -ea -cp $MAINCP \
		experiment.sensa.getCostOfEffectiveness \
		${inputfiles} \
		"$dstfile-$algo"
done
exit 0

# hcai vim :set ts=4 tw=4 tws=4

