#!/bin/bash
C=(5620 5659 6159 4286 4946 4164 13932)  # after improved
i=0

for algo in rand inc observed;
do
	echo "Now for the $algo strategy ---"
	i=0
	for N in 2 3 5 14 16 17 20;
	do
		echo -n "Now Ranking impact for v1 s$N at ${C[$i]} ... "
		sh ./WBFSRanking.sh ${C[$i]} v1 s$N $algo

		let i=i+1
	done
done

echo "Ranking phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

