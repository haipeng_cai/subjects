#!/bin/bash
srcfn=$1
grep -a -E "use model not found" $srcfn | awk '{print $7,$8,$9}' | sort | uniq > usemodels
grep -a -E "def model not found" $srcfn | awk '{print $7,$8,$9}' | sort | uniq > defmodels
grep -a -E "p2 seed model not found" $srcfn | awk '{print $8,$9,$10}' | sort | uniq > seedmodels
grep -a -E "p2 transfer model not found" $srcfn | awk '{print $8,$9,$10}' | sort | uniq > transfermodels

# hcai vim :set ts=4 tw=4 tws=4

