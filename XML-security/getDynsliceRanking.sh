#!/bin/bash
#C=(5620 5659 6159 4286 4946 4164 13932)  # after improved
C=(5621 5660 6160 4287 4947 4165 13933)  # after worked around in Init::init()
i=0
for N in 2 3 5 14 16 17 20;
do
	echo "Now dynslice instrumenting s$N at ${C[$i]} ..."
	sh DynwsliInstr.sh ${C[$i]} v1 s$N

	let i=i+1
done

i=0
for N in 2 3 5 14 16 17 20;
do
	echo "Now Running instrumented s$N at ${C[$i]} ..."
	sh DynwsliRun.sh ${C[$i]} v1 s$N

	let i=i+1
done


i=0
for N in 2 3 5 14 16 17 20;
do
	echo "Now Ranking instrumented s$N at ${C[$i]} ..."
	sh DynwsliRank.sh ${C[$i]} v1 s$N

	let i=i+1
done

echo "Running phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

