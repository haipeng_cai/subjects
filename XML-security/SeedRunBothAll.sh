#!/bin/bash
source ./xmlsec_global.sh
if [ $# -lt 1 ];then
	echo "Usage: $0 querylist_file number_Of_traces"
	exit 1
fi
fnlist=$1
NT=${2:-92}

source ./xmlsec_global.sh

log=seeded_log_xmlsec_accuracyResults
res=seeded_result_xmlsec_accuracyResults

>$log
>$res

function RunOneByOne()
{
	local i=0
	cat $fnlist | dos2unix | \
	while read query;
	do
		echo "Querying $query ..."
		sh SeedRunBoth.sh v1 s${SEEDS[$i]} \""$query"\" 1>>$res 2>>$log
		let i=i+1
	done
}

starttime=`date +%s%N | cut -b1-13`
RunOneByOne
stoptime=`date +%s%N | cut -b1-13`
echo "Query Time elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0


# hcai vim :set ts=4 tw=4 tws=4

