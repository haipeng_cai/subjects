#!/bin/bash
# these dummy change locations are used for producing stmtids.out only
#C=(1000 1000 1000 1000 1000 1000 1000)

# these are the real change locations to instrument at
#C=(5614 5655 6159 4284 4942 25369 13932) # the originally planned
#C=(5614 5655 6159 4282 4942 4170 13932)   # after corrections
C=(5620 5659 6159 4286 4946 4164 13932)  # after improved
i=0
for N in 2 3 5 14 16 17 20;
do
	echo "Now Running execHistInstrumented s$N at ${C[$i]} ..."
	sh execHistRun_withoutSensaRun.sh ${C[$i]} v1 s$N 

	echo "Now Running execHistInstrumented s$N-orig at ${C[$i]} ..."
	sh execHistRun_withoutSensaRun.sh ${C[$i]} v1 s$N-orig

	let i=i+1
done

exit 0


# hcai vim :set ts=4 tw=4 tws=4

