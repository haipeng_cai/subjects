#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

source ./xmlsec_global.sh

INDIR=$subjectloc/execHistInstrumented-$ver-$seed-orig-$change
MODINDIR=$subjectloc/execHistInstrumented-$ver-$seed-$change

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/ProbSli/bin:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$ROOT/workspace/Sensa/bin:$ROOT/workspace/TestAdequacy/bin"

OUTDIR=exechist_outdyn-${ver}${seed}-$change-orig
mkdir -p $OUTDIR

MODOUTDIR=exechist_outdyn-${ver}${seed}-$change-mod
mkdir -p $MODOUTDIR

#cp $MODINDIR/org/apache/xml/security/test/AllTestsSelect.class $MODINDIR/org/apache/xml/security/test/AllTestsSelectMod.class

java -Xmx10000m -ea -cp ${MAINCP} -DOutputScope=2 -DForActualImpact=true Sensa.SensaRun \
	org.apache.xml.security.test.AllTestsSelect \
	"$subjectloc" \
	"$INDIR" \
	"$change" \
	"$OUTDIR" \
	"$MODINDIR" \
	"$MODOUTDIR"


exit 0


# hcai vim :set ts=4 tw=4 tws=4

