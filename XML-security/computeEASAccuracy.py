#!/usr/bin/env python
''' for the MCIA accuracy study data analysis '''
import os
import sys
import string
import subprocess
import random
import time
import re

# structure of the Accuracy table: 
# {change:{query:{mp:[[EA Impact set],EAAnalysisTime, [mDEA Impact set], mDEAAnalysisTime]}}}
g_dataTable= dict()
# structure of the Result table1: 
# {change:{query:{mp:[[EA],[mDEA],[EA-mDEA],[mDEA-EA],Impact set size mDEA/EA, EATime, mDEATime, analysis cost mDEA/EA]}}}
g_resultTable= dict()

# structure of the Result table2: 
# {change:{query:{mp:[[EA],[mDEA],[FP],[FN],Precision, Recall, F1, F2]}}}
g_resultTable2= dict()

# executables
g_mutRunBoth = os.getcwd() + "/MutRunBoth.sh"
g_mDEABin = os.getcwd()+"/MutAnalysis.sh"
g_EABin = os.getcwd()+"/MutEAAnalysis.sh"

# data source locations
g_VerPrefix="v1"
#g_Seeds=[2,3,5,14,16,17,20]
g_Seeds=[2]

# optional inputs
#g_fnQueryFile= os.getcwd()+"/functionList.out"
g_fnQueryFile="" 
g_nTest=92

# number of queries as samples
g_nQueries=-1

tmpFEA="/tmp/s"
tmpFEH="/tmp/t"

def Usage():
	print >> sys.stdout, "%s [versionId] [seedId,..] [file of queries] [number of traces] " % sys.argv[0]
	return

def ParseCommandLine():
	global g_VerPrefix
	global g_Seeds
	global g_fnQueryFile
	global g_nTest
	argc = len(sys.argv)
	if argc >= 3:
		g_VerPrefix = sys.argv[1]
		g_Seeds=list()
		for s in string.split(sys.argv[2],','):
			g_Seeds.append( s )
		if argc >= 4:
			g_fnQueryFile = sys.argv[3]
			if not os.path.exists( g_fnQueryFile ):
				raise IOError, "the file of queries [%s] does not exist, bailed out now." % g_fnQueryFile
		if argc >= 5:
			g_nTest = int(sys.argv[4])
	else:
		g_Seeds = ['s'+str(v) for v in g_Seeds]
	
	'''check existence of binaries'''
	global g_mDEABin, g_EABin, g_mutRunBoth
	for binary in (g_mDEABin, g_EABin, g_mutRunBoth):
		if not os.path.exists( binary ):
			raise IOError, "required executable [%s] does not exist, bailed out now." % binary 
	print "command line: %s %s %s %s %d" % (sys.argv[0], g_VerPrefix, g_Seeds, g_fnQueryFile, g_nTest)

def collectData():
	global g_dataTable
	global g_fnQueryFile
	global g_nTest
	global g_nQueries

	bUsingSingleQueryList=True
	AllQueries=list()
	if len(g_fnQueryFile) >= 3:
		'''open the file of queries, namely input function list, fetch each one as a single query at a time'''
		qfh = file(g_fnQueryFile, "r")
		if None == qfh:
			raise Exception, "Failed to open query file named %s\n" % g_fnQueryFile 
		AllQueries = qfh.readlines()
		qfh.close()
	else:
		bUsingSingleQueryList=False

	for seed in g_Seeds:
		change = "%s%s" % (g_VerPrefix, seed)
		if bUsingSingleQueryList != True:
			g_fnQueryFile= "%s/MutEHInstrumented-%s/MutPoints.out" % (os.getcwd(), change)
			if not os.path.exists( g_fnQueryFile ):
				print >> sys.stderr,  "the file of queries [%s] does not exist, bailed out now." % g_fnQueryFile
				continue
			'''open the file of queries, namely input function list, fetch each one as a single query at a time'''
			qfh = file(g_fnQueryFile, "r")
			if None == qfh:
				print >> sys.stderr, "%s skipped because: failed to open query file named %s\n" % (change, g_fnQueryFile)
				continue
			AllQueries = qfh.readlines()
			qfh.close()

		bRandomSampling=True
		
		if g_nQueries<1:
			g_nQueries=len(AllQueries)
			bRandomSampling=False
		else:
			if len(AllQueries) < g_nQueries:
				g_nQueries = len(AllQueries)

		## query = qfh.readline()
		## while query:
		for k in range(0, g_nQueries):
			qidx=k
			if bRandomSampling:
				qidx=int(random.random()*(len(AllQueries)-1))
			queryline = AllQueries[ qidx ]
			queryline = queryline.lstrip().rstrip(' \r\n')

			''' retrieve the query method and selected mutation points in it '''
			mres = re.match( r'(<.*>) ([0-9].*)', queryline )
			query = mres.group(1)
			qcontent = string.split(mres.group(2), ' ')
			assert len(qcontent)>=1 
			mutpnts = [ n for n in qcontent[0:len(qcontent)] ]

			''' run the mutated versions on MutEAS and MutEH, each characterized with a mutation point'''
			for mp in mutpnts:
				print "With query [%s] and mutation point[%s]..." % (query,mp)
				print "1. run the base version with MutEAS and MutEH ..."
				os.system("rm -rf MutEAoutdyn-%s%s" % (g_VerPrefix,seed))
				os.system("rm -rf MutEHoutdyn-%s%s" % (g_VerPrefix,seed))
				try:
					cmd = [str(g_mutRunBoth), g_VerPrefix, seed, mp, str(g_nTest)]
					rcMutBoth = subprocess.check_call( cmd, stderr=subprocess.STDOUT )
				except subprocess.CalledProcessError,e:
					print >> sys.stderr, "call %s failed, skipped [ret code=%d]." % \
						( str(cmd).strip(','), e.returncode )
					## query = qfh.readline()
					continue
				except Exception,e:
					print >> sys.stderr, "Unexpected error when calling %s, halt now with error=%s" % \
						( str(cmd).strip(','), e )
					sys.exit(-1)


				''' get EAS result '''
				print "2. get the EAS result with the current base version ..."
				efh=None
				cmd=None
				os.system("rm -f " + tmpFEA)
				try:
					efh = file (tmpFEA, 'w')
					cmd = [str(g_EABin), g_VerPrefix, seed, str(g_nTest), query]
					rcEA = subprocess.check_call( cmd, stdout=efh, stderr=subprocess.STDOUT )
				except subprocess.CalledProcessError,e:
					print >> sys.stderr, "call %s failed, skipped [ret code=%d]." % \
						( str(cmd).strip(','), e.returncode )
					## query = qfh.readline()
					continue
				except Exception,e:
					print >> sys.stderr, "Unexpected error when calling %s, halt now with error=%s" % \
						( str(cmd).strip(','), e )
					os.system("rm -f " + tmpFEA)
					sys.exit(-1)
				efh.flush()
				efh.close()

				''' get mDEA result '''
				print "3. get the mDEA result with the current base version ..."
				dfh=None
				os.system("rm -f " + tmpFEH)
				try:
					dfh = file (tmpFEH, 'w')
					cmd = [str(g_mDEABin), g_VerPrefix, seed, str(g_nTest), query]
					rcmDEA = subprocess.check_call( cmd, stdout=dfh, stderr=subprocess.STDOUT )
				except subprocess.CalledProcessError,e:
					print >> sys.stderr, "call %s failed, skipped [ret code=%d]." % \
						( str(cmd).strip(','), e.returncode )
					## query = qfh.readline()
					continue
				except Exception,e:
					print >> sys.stderr, "Unexpected error when calling %s, halt now with error=%s" % \
						( str(cmd).strip(','), e )
					os.system("rm -f " + tmpFEH)
					sys.exit(-1)
				dfh.flush()
				dfh.close()

				''' now read query results from EAS and mDEA respectively, format-dependent text parsing'''
				EAImpactSet=set()
				EATime=0
				esz=0
				efh = file(tmpFEA,'r')
				efh.seek(0,0)
				bstart = False
				curline = efh.readline()
				while curline:
					if bstart==False:
						bstart = (-1!=curline.find("Change Impact Set"))
						if bstart==True:
							esz = int( string.split( curline )[-1] )
						curline = efh.readline()
						continue
					if curline.find("RunTime for")==-1:
						EAImpactSet.add( curline.lstrip().rstrip(' \r\n') )
					else:
						flds = string.split( curline )
						assert len(flds)>=3
						EATime = int( flds[len(flds)-2] )
						break
					curline = efh.readline()
				#print "%d : %d" % (len(EAImpactSet), esz)
				assert len(EAImpactSet)==esz

				if esz < 1:
					print >> sys.stderr, "EAS got an uncovered query: %s, skipped." % (query)
					''' 
					if the current method is not covered at all, there is no need to try other 
					mutation points in it
					'''
					os.system("rm -f " + tmpFEA)
					os.system("rm -f " + tmpFEH)
					break
					#continue
					
				mDEAImpactSet=set()
				mDEATime=0
				dsz=0
				dfh = file(tmpFEH,'r')
				dfh.seek(0,0)
				bstart = False
				curline = dfh.readline()
				if curline!=None and -1!=curline.find("Invalid queries"):
					print >> sys.stderr, "mDEA got an Invalid query: %s, skipped." % (query)
					## query = qfh.readline()
					continue
				while curline:

					if bstart==False:
						bstart = (-1!=curline.find("Change Impact Set of All Changes"))
						if bstart==True:
							dsz = int( string.split( curline )[-1] )
						curline = dfh.readline()
						continue
					if curline.find("RunTime for")==-1:
						mDEAImpactSet.add( curline.lstrip().rstrip(' \r\n') )
					else:
						flds = string.split( curline )
						assert len(flds)>=3
						mDEATime = int( flds[len(flds)-2] )
						break
					curline = dfh.readline()
				#print "%d : %d" % (len(mDEAImpactSet), dsz)
				assert len(mDEAImpactSet)==dsz

				print "Done with query [%s] and mutation point[%s]." % (query,mp)

				''' save results to the global data table '''
				if change not in g_dataTable.keys():
					g_dataTable[change] = dict()
				if query not in g_dataTable[change].keys():
					g_dataTable[change][query] = dict()
				if mp not in g_dataTable[change][query].keys():
					g_dataTable[change][query][mp] = list()

				g_dataTable[change][query][mp].append( EAImpactSet )
				g_dataTable[change][query][mp].append( EATime )
				g_dataTable[change][query][mp].append( mDEAImpactSet )
				if mDEATime < 1:
					print >> sys.stderr, "ERROR: %s with query %s by mDEA costs 0!" % \
							(change, query)
					sys.exit(-2)
				g_dataTable[change][query][mp].append( mDEATime )

				# remove temporary files
				os.system("rm -f " + tmpFEA)
				os.system("rm -f " + tmpFEH)

				## query = qfh.readline()

		print "Done ALL with %s-%s." % (g_VerPrefix, seed)
		## qfh.seek(0,0)
	# // all seeds
	## qfh.close()

# result Data structure:
# {change:{query:[[EA],[mDEA],[EA-mDEA],[mDEA-EA],Impact set size mDEA/EA, EATime, mDEATime, analysis cost mDEA/EA]}}
def computeResults():
	global g_dataTable
	global g_resultTable
	for chg in g_dataTable.keys():
		if chg not in g_resultTable.keys():
			g_resultTable[chg] = dict()
		for query in g_dataTable[chg].keys():
			if query not in g_resultTable[chg].keys():
				g_resultTable[chg][query] = dict()
			for mp in g_dataTable[chg][query].keys():
				if mp not in g_resultTable[chg][query]:
					g_resultTable[chg][query][mp] = list()
				
				EAIS = g_dataTable[chg][query][mp][0]
				EATime = g_dataTable[chg][query][mp][1]
				mDEAIS = g_dataTable[chg][query][mp][2]
				mDEATime = g_dataTable[chg][query][mp][3]
				EDDiff = EAIS - mDEAIS
				DEDiff = mDEAIS - EAIS
				if len(EAIS)<1:
					DESizeRatio=-1
				else:
					DESizeRatio = float( "%.4f" % (len(mDEAIS)*1.0/len(EAIS)) )
				if mDEATime<=0:
					EDTimeRatio=-1
				else:
					EDTimeRatio = float( "%.4f" % (EATime*1.0/mDEATime) )

				g_resultTable[chg][query][mp] = [EAIS,mDEAIS,EDDiff,DEDiff,DESizeRatio,EATime,mDEATime,EDTimeRatio]

def FMeasurement(p,c,b):
	return float( (1+b**2)*(p*c)/(p*(b**2)+c) )

# result Data structure:
# {change:{query:{mp:[[EA],[mDEA],[FP],[FN],Precision, Recall, F1, F2]}}}
def computeResults2():
	global g_dataTable
	global g_resultTable2
	for chg in g_dataTable.keys():
		if chg not in g_resultTable2.keys():
			g_resultTable2[chg] = dict()
		for query in g_dataTable[chg].keys():
			if query not in g_resultTable2[chg].keys():
				g_resultTable2[chg][query] = dict()
			for mp in g_dataTable[chg][query].keys():
				if mp not in g_resultTable2[chg][query]:
					g_resultTable2[chg][query][mp] = list()
				
				EAIS = g_dataTable[chg][query][mp][0]
				EATime = g_dataTable[chg][query][mp][1]
				mDEAIS = g_dataTable[chg][query][mp][2]
				mDEATime = g_dataTable[chg][query][mp][3]
				EDDiff = EAIS - mDEAIS
				DEDiff = mDEAIS - EAIS

				Intersection=set(EAIS).intersection(set(mDEAIS))
				if len(EAIS) < 1:
					precision = 1
				else:
					precision = float( "%.4f" % (len(Intersection)*1.0/len(EAIS)) )
				if len(mDEAIS) < 1:
					recall = 1
				else:
					recall = float( "%.4f" % (len(Intersection)*1.0/len(mDEAIS)) )

				#F1 = 2*recall*precision/(recall+precision)
				F1 = FMeasurement(precision, recall, 1)
				F2 = FMeasurement(precision, recall, 2)

				g_resultTable2[chg][query][mp] = [EAIS,mDEAIS,EDDiff,DEDiff,precision,recall,F1,F2]

def dumpResults2():
	global g_resultTable2

	# table title
	print >> sys.stdout, "change\tquery\tmp\tEA\tmDEA\tFP\tFN\tprecision\trecall\tF1\tF2"
	print
	# AllSum structure: {idx:[sum]}
	AllSum={}
	resfn=file(os.getcwd()+"/summaryData.txt", 'w')
	for chg in g_resultTable2.keys():
		for query in g_resultTable2[chg].keys():
			for mp in g_resultTable2[chg][query].keys():
				print >> sys.stdout, chg, "\t", query, "\t", mp, "\t", \
						len( g_resultTable2[chg][query][mp][0]), "\t", len (g_resultTable2[chg][query][mp][1]), "\t", \
						len( g_resultTable2[chg][query][mp][2]), "\t", len (g_resultTable2[chg][query][mp][3]), "\t", \
						str(g_resultTable2[chg][query][mp][4]*100)+"%", "\t", \
						str(g_resultTable2[chg][query][mp][5]*100)+"%", "\t", \
						g_resultTable2[chg][query][mp][6], "\t", g_resultTable2[chg][query][mp][7]

				''' save to file for importing into spreadsheet '''
				print >> resfn, query, "\t", mp, "\t", \
						len( g_resultTable2[chg][query][mp][0]), "\t", len (g_resultTable2[chg][query][mp][1]), "\t", \
						len( g_resultTable2[chg][query][mp][2]), "\t", len (g_resultTable2[chg][query][mp][3]), "\t", \
						str(g_resultTable2[chg][query][mp][4]*100)+"%", "\t", \
						str(g_resultTable2[chg][query][mp][5]*100)+"%", "\t", \
						g_resultTable2[chg][query][mp][6], "\t", g_resultTable2[chg][query][mp][7]

				for i in range(0,4):
					if i not in AllSum.keys():
						AllSum[i]=list()
					AllSum[i].append( len(g_resultTable2[chg][query][mp][i]) )
				for i in range(4,8):
					if i not in AllSum.keys():
						AllSum[i]=list()
					AllSum[i].append( g_resultTable2[chg][query][mp][i] )

	resfn.flush()
	resfn.close()

	try:
		import numpy

		print >> sys.stdout, "[statistics]\tEA\tmDEA\tFP\tFN\tprecision\trecall\tF1\tF2"
		print >> sys.stdout, "average\t",
		for i in range(0,8):
			print >> sys.stdout, "\t%.4f" % ( numpy.mean(AllSum[i]) ),
		print 
		print >> sys.stdout, "median\t",
		for i in range(0,8):
			print >> sys.stdout, "\t%.4f" % ( numpy.median(AllSum[i]) ),
		print 
		print >> sys.stdout, "stdev\t",
		for i in range(0,8):
			print >> sys.stdout, "\t%.4f" % ( numpy.std(AllSum[i]) ),
		print 
	except Exception, e:
		print >> sys.stderr, "Numpy not available, statistics skipped"

def dumpData():
	global g_dataTable
	global g_nTest

	for chg in g_dataTable.keys():
		print >> sys.stdout, "===== chang=%s [from %d tests] ==== " % (chg, g_nTest)
		for query in g_dataTable[chg].keys():
			for mp in g_dataTable[chg][query]:
				print >> sys.stdout, "==== EAS impact set of [%s at location %s] ==== costing %d milliseconds " \
						% (query, mp, g_dataTable[chg][query][mp][1])
				for m in g_dataTable[chg][query][mp][0]:
					print >> sys.stdout, "\t%s" % m
				print >> sys.stdout, "==== mDEA impact set of [%s at location %s] ==== costing %d milliseconds " \
						% (query, mp, g_dataTable[chg][query][mp][3])
				for m in g_dataTable[chg][query][mp][2]:
					print >> sys.stdout, "\t%s" % m

def dumpResults():
	global g_resultTable

	# table title
	print >> sys.stdout, "change\tquery\tmp\tEA\tmDEA\tEA-mDEA\tmDEA-EA\tSize:mDEA/EA\tEATime\tmDEATime\tTime: EA/mDEA"
	print
	AllSumSize=[0,0]
	AllSumTime=[0,0]
	for chg in g_resultTable.keys():
		SumSizePerChg=[0,0]
		SumTimePerChg=[0,0]
		for query in g_resultTable[chg].keys():
			for mp in g_resultTable[chg][query].keys():
				print >> sys.stdout, chg, "\t", query, "\t", mp, "\t", \
						len( g_resultTable[chg][query][mp][0]), "\t", len (g_resultTable[chg][query][mp][1]), "\t", \
						len( g_resultTable[chg][query][mp][2]), "\t", len (g_resultTable[chg][query][mp][3]), "\t", \
						str(g_resultTable[chg][query][mp][4]*100)+"%", "\t", \
						g_resultTable[chg][query][mp][5], "\t", g_resultTable[chg][query][mp][6], "\t", \
						str(g_resultTable[chg][query][mp][7]*100)+"%"

				AllSumSize[0] += len(g_resultTable[chg][query][mp][0])
				AllSumSize[1] += len(g_resultTable[chg][query][mp][1])
				SumSizePerChg[0] += len(g_resultTable[chg][query][mp][0])
				SumSizePerChg[1] += len(g_resultTable[chg][query][mp][1])

				AllSumTime[0] += g_resultTable[chg][query][mp][5]
				AllSumTime[1] += g_resultTable[chg][query][mp][6]
				SumTimePerChg[0] += g_resultTable[chg][query][mp][5]
				SumTimePerChg[1] += g_resultTable[chg][query][mp][6]

		sizeRatioPerChg=None
		if SumSizePerChg[0]<1:
			sizeRatioPerChg=-1
		else:
			sizeRatioPerChg=SumSizePerChg[1]*1.0/SumSizePerChg[0]
		print >> sys.stdout, "%s Size: mDEA/EA is %f%%" % \
				(chg, sizeRatioPerChg*100)
		timeRatioPerChg=None
		if SumTimePerChg[1]<1:
			timeRatioPerChg=-1
		else:
			timeRatioPerChg=SumTimePerChg[0]*1.0/SumTimePerChg[1]
		print >> sys.stdout, "%s Time: EA/mDEA is %f%%" % \
				(chg, timeRatioPerChg*100)

	if AllSumSize[0]>=1:
		print >> sys.stdout, "aggregate Size: mDEA/EA is %f%%" % \
				(AllSumSize[1]*1.0/AllSumSize[0]*100)
	else:
		print >> sys.stdout, "Something might have been wrong: all EAS impact sets are empty."
	if AllSumTime[1]>=1:
		print >> sys.stdout, "aggregate Time: EA/mDEA is %f%%" % \
				(AllSumTime[0]*1.0/AllSumTime[1]*100)
	else:
		print >> sys.stdout, "Something might have been wrong: all mDEA analysis cost zero in total."

######################################
# the boost
if __name__ == "__main__":
	try:
		ParseCommandLine()
	except Exception,e:
		print >> sys.stderr, e
		sys.exit(1)

	#time.sleep(28800)

	collectData()
	#computeResults()
	computeResults2()
	dumpData()
	#dumpResults()
	dumpResults2()

	sys.exit(0)

# hcai vim set ts=4 tw=100 sts=4 sw=4


