#!/usr/bin/env python
''' for the MCIA accuracy study data analysis '''
import os
import sys
import string
import subprocess
import random
import time
import re

# executables
g_mutRunBoth = os.getcwd() + "/MutRunAnalysis.sh"

# data source locations
g_VerPrefix="v1"
#g_Seeds=[2,3,5,14,16,17,20]
g_Seeds=[1]

# optional inputs
#g_fnQueryFile= os.getcwd()+"/functionList.out"
g_fnQueryFile="" 
g_nTest=92

# number of queries as samples
g_nQueries=-1

def Usage():
	print >> sys.stdout, "%s [versionId] [seedId,..] [file of queries] [number of traces] " % sys.argv[0]
	return

def ParseCommandLine():
	global g_VerPrefix
	global g_Seeds
	global g_fnQueryFile
	global g_nTest
	argc = len(sys.argv)
	if argc >= 3:
		g_VerPrefix = sys.argv[1]
		g_Seeds=list()
		for s in string.split(sys.argv[2],','):
			g_Seeds.append( s )
		if argc >= 4:
			g_fnQueryFile = sys.argv[3]
			if not os.path.exists( g_fnQueryFile ):
				raise IOError, "the file of queries [%s] does not exist, bailed out now." % g_fnQueryFile
		if argc >= 5:
			g_nTest = int(sys.argv[4])
	else:
		g_Seeds = ['s'+str(v) for v in g_Seeds]
	
	'''check existence of binaries'''
	global g_mutRunBoth
	for binary in (g_mutRunBoth,):
		if not os.path.exists( binary ):
			raise IOError, "required executable [%s] does not exist, bailed out now." % binary 
	print "command line: %s %s %s %s %d" % (sys.argv[0], g_VerPrefix, g_Seeds, g_fnQueryFile, g_nTest)

def collectData():
	global g_dataTable
	global g_fnQueryFile
	global g_nTest
	global g_nQueries

	bUsingSingleQueryList=True
	AllQueries=list()
	if len(g_fnQueryFile) >= 3:
		'''open the file of queries, namely input function list, fetch each one as a single query at a time'''
		qfh = file(g_fnQueryFile, "r")
		if None == qfh:
			raise Exception, "Failed to open query file named %s\n" % g_fnQueryFile 
		AllQueries = qfh.readlines()
		qfh.close()
	else:
		bUsingSingleQueryList=False

	for seed in g_Seeds:
		change = "%s%s" % (g_VerPrefix, seed)
		if bUsingSingleQueryList != True:
			g_fnQueryFile= "%s/MutEHInstrumented-%s/MutPoints.out" % (os.getcwd(), change)
			if not os.path.exists( g_fnQueryFile ):
				print >> sys.stderr,  "the file of queries [%s] does not exist, bailed out now." % g_fnQueryFile
				continue
			'''open the file of queries, namely input function list, fetch each one as a single query at a time'''
			qfh = file(g_fnQueryFile, "r")
			if None == qfh:
				print >> sys.stderr, "%s skipped because: failed to open query file named %s\n" % (change, g_fnQueryFile)
				continue
			AllQueries = qfh.readlines()
			qfh.close()

		bRandomSampling=True
		
		if g_nQueries<1:
			g_nQueries=len(AllQueries)
			bRandomSampling=False
		else:
			if len(AllQueries) < g_nQueries:
				g_nQueries = len(AllQueries)

		## query = qfh.readline()
		## while query:
		for k in range(0, g_nQueries):
			qidx=k
			if bRandomSampling:
				qidx=int(random.random()*(len(AllQueries)-1))
			queryline = AllQueries[ qidx ]
			queryline = queryline.lstrip().rstrip(' \r\n')

			''' retrieve the query method and selected mutation points in it '''
			mres = re.match( r'(<.*>) ([0-9].*)', queryline )
			query = mres.group(1)
			qcontent = string.split(mres.group(2), ' ')
			assert len(qcontent)>=1 
			mutpnts = [ n for n in qcontent[0:len(qcontent)] ]

			''' run the mutated versions on MutEAS and MutEH, each characterized with a mutation point'''
			for mp in mutpnts:
				print "With query [%s] and mutation point[%s]..." % (query,mp)
				print "Running the base version with MutEAS and MutEH ..."
				os.system("rm -rf MutEAoutdyn-%s%s" % (g_VerPrefix,seed))
				os.system("rm -rf MutEHoutdyn-%s%s" % (g_VerPrefix,seed))
				try:
					cmd = [str(g_mutRunBoth), g_VerPrefix, seed, query, mp, str(g_nTest)]
					#rcMutBoth = subprocess.check_call( cmd, stderr=subprocess.STDOUT )
					rcMutBoth = subprocess.check_call( cmd )
				except subprocess.CalledProcessError,e:
					print >> sys.stderr, "call %s failed, skipped [ret code=%d]." % \
						( str(cmd).strip(','), e.returncode )
					## query = qfh.readline()
					continue
				except Exception,e:
					print >> sys.stderr, "Unexpected error when calling %s, halt now with error=%s" % \
						( str(cmd).strip(','), e )
					sys.exit(-1)

		print "Done ALL with %s-%s." % (g_VerPrefix, seed)
		## qfh.seek(0,0)
	# // all seeds
	## qfh.close()

######################################
# the boost
if __name__ == "__main__":
	try:
		ParseCommandLine()
	except Exception,e:
		print >> sys.stderr, e
		sys.exit(1)

	#time.sleep(28800)

	collectData()

	sys.exit(0)

# hcai vim set ts=4 tw=100 sts=4 sw=4


