#!/bin/bash
# these are the real change locations to instrument at
C=(5620 5659 6159 4286 4946 4164 13932)  # after improved

RESDIR=`pwd`/errorMetrics
mkdir -p $RESDIR

i=0
for N in 2 3 5 14 16 17 20;
do
	echo -n "Now calculating errors for v1 s$N at ${C[$i]} ... "
	sh ./dynwslice_getErrors.sh ${C[$i]} v0 s$N > $RESDIR/Error-v0s$N-${C[$i]}

	let i=i+1
	echo " done."
done

echo "Ranking phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

