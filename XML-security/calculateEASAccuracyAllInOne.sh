#!/bin/bash

for gsz in 3 4 5 6 7 8 9 10;
do
	python ./MMC_calculateEASAccuracy.py $gsz v1 s2-orig 1>result_xmlsec_EASAccuracy_s$gsz 2>log_xmlsec_EASAccuracy_s$gsz
done

# hcai vim :set ts=4 tw=4 tws=4

