#!/bin/bash

mkdir ./MMC_CovDynaliasresults/
for s in 2 3 4 5 6 7 8 9 10;
do
	sh MMC_EASVsDiver.sh \
		DiverResults/xmlsecStmtcovDynAlias_EASVsDiverLog \
		EAInstrumented-v1s2-orig/functionList.out \
		$s 1000 \
		1>./MMC_CovDynaliasresults/result_s_$s 2>/dev/null #./MMC_results/log_s_$s
done

echo "all finished."
exit 0

# hcai vim :set ts=4 tw=4 tws=4
