#1/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 change ver seed "
	exit 1
fi
change=$1
ver=$2
seed=$3

source ./xmlsec_global.sh

OUTDIR=deamout_$ver$seed

INDIR=$subjectloc/execHistInstrumented-$ver-$seed-${change}

#MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$LIBPATH/polyglot-1.3.5.jar:$LIBPATH/sootclasses-2.3.0.jar:$LIBPATH/jasminclasses-2.3.0.jar:$LIBPATH/DUAForensics.jar:$LIBPATH/LocalsBox.jar:$LIBPATH/InstrReporters.jar:$LIBPATH/java_cup-1.3.5.jar:$LIBPATH/ProbSli.jar:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$INDIR"

MAINCP="sootlibs/rt.jar:sootlibs/polyglot.jar:sootlibs/sootclasses-2.3.0.jar:sootlibs/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/Deam/bin:sootlibs/java_cup.jar:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/ProbSli/bin:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$ROOT/workspace/Deam/bin:$ROOT/workspace/TestAdequacy/bin:$INDIR"


mkdir -p $OUTDIR

function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	cat $subjectloc/test_names.txt | dos2unix | \
	while read testname;
	do
		let i=i+1

		echo "Run Test #$i....."
		java -Xmx4000m -ea -cp $MAINCP org.apache.xml.security.test.AllTestsSelect \
	   	$testname 1> $OUTDIR/$i.txt 2> $OUTDIR/$i.err
	done
}

starttime=`date +%s%N | cut -b1-13`
RunOneByOne
#RunAllInOne
stoptime=`date +%s%N | cut -b1-13`

exit 0



# hcai vim :set ts=4 tw=4 tws=4

