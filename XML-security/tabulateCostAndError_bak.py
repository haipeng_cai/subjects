#!/usr/bin/env python
import os
import sys
import string

# structure of the Cost table: {change:[RandCost,IncCost,ObservedCost,WslicingCost]
g_costTable= dict()
# structure of the Cost table: {change:[RandError,IncError,ObservedError,WslicingError]
g_errorTable= dict()

# data source locations
g_dirSensaResult=""
g_dirWslicingResult=""
g_dirIdealResult=""
g_dirSensaError=""
g_dirWslicingError=""
g_dirIdealError=""

# these are the real change locations to instrument at
g_VerPrefix="v1"
#g_ChgLocs=(5614,5655,6159,4282,4942,4170,13932)   # after corrections
g_ChgLocs=(5620,5659,6159,4286,4946,4164,13932)  # after improved
g_Seeds=(2,3,5,14,16,17,20)

DELIMITOR=' '

## @brief print usage
## @param none
## @retrun none
def Usage():
	print >> sys.stdout, "%s SensaResultDir WSlicingResultDir IdealResultDir [SensaErrorDir WSlicingErrorDir IdealErrorDir]\n" % \
			sys.argv[0]
	return

## @brief parse command line to get user's input for source file name and target
## @param none
## @retrun none
def ParseCommandLine():
	global g_dirWslicingResult
	global g_dirSensaResult
	global g_dirIdealResult
	argc = len(sys.argv)
	if argc >= 4:
		for i in range(1, argc):
			if not os.path.exists( sys.argv[i] ):
				raise IOError, "source dir given at [%s] does not exist, bailed out now." \
					% sys.argv[i]
		g_dirSensaResult = sys.argv[1]
		g_dirWslicingResult = sys.argv[2]
		g_dirIdealResult = sys.argv[3]
		if argc >= 7:
			global g_dirSensaError
			global g_dirWslicingError
			global g_dirIdealError
			g_dirSensaError = sys.argv[4]
			g_dirWslicingError = sys.argv[5]
			g_dirIdealError = sys.argv[6]
	else:
		Usage()
		raise Exception, "too few arguments, aborted."

''' example input cost file
costEffectiveness/averageCosts-v0s4-315-rand
../correctResultWslicing/costEffectiveness/averageCosts-v0s1-13
'''
def retrieveCosts():
	global g_costTable
	for idx in range(0, min(len(g_ChgLocs),len(g_Seeds))):
		change="%s-s%d-%d" % (g_VerPrefix,g_Seeds[idx],g_ChgLocs[idx])
		print "Processing costs %s ...... " % change,

		'''
		Retrieve Ideal ranking costs
		'''
		fnIdealCost="%s/averageCosts-%ss%d-%d" % \
					(g_dirIdealResult,g_VerPrefix,g_Seeds[idx],g_ChgLocs[idx])
		ifh = file(fnIdealCost,"r")
		if None == ifh:
			raise Exception, "Failed to open file named %s\n" % fnIdealCost 
		if change not in g_costTable.keys():
			g_costTable[change] = list()
		g_costTable[change].append( ifh.readline().lstrip().rstrip(' \r\n') )
		ifh.close()

		'''
		Retrieve all Sensa ranking costs
		'''
		for algo in ("rand","inc","observed"):
			fnSensaCost="%s/averageCosts-%ss%d-%d-%s" % \
					(g_dirSensaResult,g_VerPrefix,g_Seeds[idx],g_ChgLocs[idx],algo)

			sfh = file(fnSensaCost,"r")
			if None == sfh:
				raise Exception, "Failed to open file named %s\n" % fnSensaCost

			if change not in g_costTable.keys():
				g_costTable[change] = list()
			g_costTable[change].append( sfh.readline().lstrip().rstrip(' \r\n') )
			sfh.close()

		'''
		Retrieve wslicing ranking costs
		'''
		fnWslicingCost="%s/averageCosts-%ss%d-%d" % \
					(g_dirWslicingResult,g_VerPrefix,g_Seeds[idx],g_ChgLocs[idx])
		wfh = file(fnWslicingCost,"r")
		if None == wfh:
			raise Exception, "Failed to open file named %s\n" % fnWslicingCost 
		if change not in g_costTable.keys():
			g_costTable[change] = list()
		g_costTable[change].append( wfh.readline().lstrip().rstrip(' \r\n') )
		wfh.close()

		print "Done."

''' example input error file
errorMetrics/Error-v0s1-13-inc
../correctResultWslicing/errorMetrics/Error-v0s1-13
'''
def retrieveErrors():
	global g_errorTable
	for idx in range(0, min(len(g_ChgLocs),len(g_Seeds))):
		change="%s-s%d-%d" % (g_VerPrefix,g_Seeds[idx],g_ChgLocs[idx])
		print "Processing errors %s ...... " % change,

		'''
		Retrieve Ideal ranking errors 
		'''
		fnIdealError="%s/Error-%ss%d-%d" % \
					(g_dirIdealError,g_VerPrefix,g_Seeds[idx],g_ChgLocs[idx])
		ifh = file(fnIdealError,"r")
		if None == ifh:
			raise Exception, "Failed to open file named %s\n" % fnIdealError
		if change not in g_errorTable.keys():
			g_errorTable[change] = list()
		g_errorTable[change].append( string.split(ifh.readlines()[1],"=")[1].lstrip().rstrip(' \r\n') )
		ifh.close()

		'''
		Retrieve all Sensa ranking errors
		'''
		for algo in ("rand","inc","observed"):
			fnSensaError="%s/Error-%ss%d-%d-%s" % \
					(g_dirSensaError,g_VerPrefix,g_Seeds[idx],g_ChgLocs[idx],algo)

			sfh = file(fnSensaError,"r")
			if None == sfh:
				raise Exception, "Failed to open file named %s\n" % fnSensaError

			if change not in g_errorTable.keys():
				g_errorTable[change] = list()
			g_errorTable[change].append( string.split(sfh.readlines()[1],"=")[1].lstrip().rstrip(' \r\n') )
			sfh.close()

		'''
		Retrieve wslicing ranking errors 
		'''
		fnWslicingError="%s/Error-%ss%d-%d" % \
					(g_dirWslicingError,g_VerPrefix,g_Seeds[idx],g_ChgLocs[idx])
		wfh = file(fnWslicingError,"r")
		if None == wfh:
			raise Exception, "Failed to open file named %s\n" % fnWslicingError
		if change not in g_errorTable.keys():
			g_errorTable[change] = list()
		g_errorTable[change].append( string.split(wfh.readlines()[1],"=")[1].lstrip().rstrip(' \r\n') )
		wfh.close()

		print "Done."

def dump():
	# table title
	print >> sys.stdout, "change IdealCost RandCost IncCost ObservedCost WslicingCost",
	if len(sys.argv) >= 7:
		print >> sys.stdout, " IdealErr RandErr IncErr ObservedErr WslicingErr"
	else:
		print >> sys.stdout, ""

	# separator
	print >> sys.stdout, "-------------------------------------------------------",
	if len(sys.argv) >= 7:
		print >> sys.stdout, "-------------------------------------------------------"
	else:
		print >> sys.stdout, ""

	# content
	for idx in range(0, min(len(g_ChgLocs),len(g_Seeds))):
		change="%s-s%d-%d" % (g_VerPrefix,g_Seeds[idx],g_ChgLocs[idx])
		print >> sys.stdout, "%s%s" % (change,DELIMITOR),
		print >> sys.stdout, DELIMITOR.join(g_costTable[change]),
		if len(sys.argv) >= 7:
			#print >> sys.stdout, DELIMITOR,
			print >> sys.stdout, DELIMITOR.join(g_errorTable[change]);
		else:
			print;

######################################
# the boost
if __name__ == "__main__":
	try:
		ParseCommandLine()
	except Exception,e:
		print >> sys.stderr, e
		sys.exit(1)

	retrieveCosts()
	if len(sys.argv) >= 7:
		retrieveErrors()

	dump()
	sys.exit(0)

# hcai vim set ts=4 tw=100 sts=4 sw=4


