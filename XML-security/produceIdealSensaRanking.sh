#!/bin/bash
# these are the real change locations to instrument at
#C=(5614 5655 6159 4282 4942 4170 13932)   # after corrections
C=(5620 5659 6159 4286 4946 4164 13932)  # after improved

i=0
for N in 2 3 5 14 16 17 20;
do
	echo -n "Now processing v1 s$N at ${C[$i]} ... "
	cat actualImpactRanking-v1s$N-${C[$i]} > IdealSensaRanking-v1s$N-${C[$i]}
	sh ./addStaticSlices.sh actualImpactRanking-v1s$N-${C[$i]} \
		../instrumented-v1-s$N-orig-${C[$i]}/fwdslice.out \
		1>>IdealSensaRanking-v1s$N-${C[$i]}

	let i=i+1
	echo " done."
done

echo "got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

