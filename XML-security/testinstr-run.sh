#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi
ver=$1
seed=$2
source ./xmlsec_global.sh

SOOTLIBDIR=/mnt/filedisk/work/duaf/Soot/Soot-libs-2.3.0
SOOTLIBS=$SOOTLIBDIR/jasminclasses-2.3.0.jar:$SOOTLIBDIR/java_cup-1.3.5.jar:$SOOTLIBDIR/jedd-runtime.jar:$SOOTLIBDIR/paddle-0.2.jar:$SOOTLIBDIR/polyglot-1.3.5.jar:$SOOTLIBDIR/sootclasses-2.3.0.jar

MAINCP=".:/usr/java/jdk1.6.0_45/jre/lib/rt.jar:$SOOTLIBS:/mnt/filedisk/work/duaf/Soot/DUAForensics/bin:/mnt/filedisk/work/duaf/Soot/Sensa/bin:/mnt/filedisk/work/duaf/Soot/TestAdequacy/bin:/mnt/filedisk/work/duaf/Soot/ProbSli/bin:/mnt/filedisk/work/duaf/Soot/LocalsBox/bin:/mnt/filedisk/work/duaf/Soot/InstrReporters/bin:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$subjectloc/test-instrumented-$ver-$seed/"

OUTDIR=Runout-${ver}${seed}-testinstr
mkdir -p $OUTDIR

function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	cat $subjectloc/test_names.txt | dos2unix | \
	while read testname;
	do
		let i=i+1

		echo "Run Test #$i....."
		java -Xmx204800m -ea -cp $MAINCP org.apache.xml.security.test.AllTestsSelect \
	   	$testname \
		1> $OUTDIR/$i.out 2> $OUTDIR/$i.err

		break;
	done
}

starttime=`date +%s%N | cut -b1-13`
RunOneByOne
#RunAllInOne
stoptime=`date +%s%N | cut -b1-13`
echo "Test instrumented subjects RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0

# hcai vim :set ts=4 tw=4 tws=4

