#!/bash/bin
#sensaData=${1:-`pwd`/ultimateResults/}
sensaData=${1:-`pwd`/ResultWBFS/}
wslicingData=${2:-`pwd`/ResultWslicing/}
#dynwslicingData=${3:-`pwd`/DynWslicingResults/}
dynwslicingData=${3:-`pwd`/ResultWslicing/}

python ./tabulateCostAndError.py \
	$sensaData/IdealCostEffectiveness \
	$wslicingData/costEffectiveness \
	$dynwslicingData/costEffectiveness \
	$sensaData/costEffectiveness \
	$sensaData/IdealErrorMetrics \
	$wslicingData/errorMetrics \
	$dynwslicingData/errorMetrics \
	$sensaData/errorMetrics 

# hcai vim :set ts=4 tw=4 tws=4

