#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi
ver=$1
seed=$2

source ./xmlsec_global.sh

MAINCP=.:/afs/nd.edu/user36/hcai/workspace/InstrReporters/bin:libs/bc-jce-jdk13-114.jar:libs/commons-logging-api.jar:libs/commons-logging.jar:libs/junit3.8.1.jar:libs/log4j-1.2.8.jar:libs/style-apachexml.jar:libs/stylebook-1.0-b3_xalan-2.jar:libs/xalan.jar:libs/xercesImpl-fixed:libs/xml-apis.jar:libs/xmlParserAPIs.jar:libs/xercesImpl-fixed:/afs/nd.edu/user36/hcai/workspace/Sensa/bin/:bin/$ver$seed:src/td$ver:$ROOT/workspace/Tracer/bin

mkdir -p bin/td$ver

#-source 1.4 -target 1.4
sed s'/\\/\//g' ${ver}files-tests.lst > /tmp/${ver}files-tests.lst
$ROOT/tools/jdk160/bin/javac -g:source -source 1.4 -cp ${MAINCP} -d bin/td$ver @/tmp/${ver}files-tests.lst  #1>err 2>&1

function compileOneByOne()
{

	cat ${ver}files-tests.lst | dos2unix | sed 's/\\/\//g' | while read fn;
	do
		echo "compiling $fn ......"
		javac -g:source -source 1.4 -cp ${MAINCP} -d bin/td$ver $fn 
		#1>/dev/null 2>&1
		if [ $? -ne 0 ];
		then
			echo "Failed to compile $fn, bail out."
			exit 1
		fi
	done
}

if [ -d bin/td$ver/profile ];
then
	rm -rf bin/td$ver/profile
fi

echo "Compilation all done."
mkdir -p bin/td$ver/org/apache/xml/security/test/resource
#cp -fr resource/td$ver/* bin/$ver/org/apache/xml/security/test/resource/
cp -fr resource/test/* bin/td$ver/org/apache/xml/security/test/resource/
exit 0
