#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi
source ./xmlsec_global.sh

ver=$1
seed=$2

function prepare()
{
rm -rf out-MutEAInstr/ out-MutEHInstr/ MutEAInstrumented-v1s2-orig/ \
	MutEHInstrumented-v1s2-orig/ NonMutEHoutdyn-v1s2-orig/ MutEHoutdyn-v1s2-orig/ MutEAoutdyn-v1s2-orig/

# EH of the fixed version
sh ./MutEHInstr.sh $ver $seed
sh ./MutEHRun.sh $ver $seed

# instrument MutEAS and MutEH
sh ./MutEAInstr.sh $ver $seed
}

#prepare

# run the base versions with EAS and mDEA, get results
#python ./computeEASAccuracy.py $ver $seed

python ./calculateEASAccuracy.py $ver $seed 1>result_xmlsec_EASAccuracy 2>log_xmlsec_EASAccuracy


# hcai vim :set ts=4 tw=4 tws=4

