
								--------------------------------------
								Instructions to SensA Experimentation 
								--------------------------------------

===========================================
1. Produce SensA rankings
===========================================
NOTE: the following scripts are written for running SensA experiment on *fixed(original)* versions. You can
remove those "-orig" suffixes in the following three scripts in order to run experiment on *buggy* versions instead.

(1) Static phase: SensA instrumentation

	sh instrAll.sh

(2) Run time phase: Run SensA-instrumented subjects 

	sh runAll.sh

(3) Post processing phase: Obtain rankings

	sh ./sensaRankingForRawoutAll.sh

	* Alternatively, "sensaRankingAll.sh" can be used when the "change count map" (instead of or besides the execution history output (raw outputs), has been dumped during the runtime (do it by setting "-OutputScope" option in run.sh to 1 or 2, 1 for the count map only and 2 for both raw output and the map), thus no large disk I/O would be required in the post-processing phase.

	* Another handy script, "./startSensaExperiment.sh", has already wrapped the above three steps together, so you may want to use it for convenience. Also, "./shnohupSensaExperiment.sh" should be used to avoid the termination of SSH session by "broken pipe" if you use remote terminal.

Now, a "predictedImpactRanking-v${ver}s{seed}-changeLoc-modificationAlgo" file of ranking will be resulted for each seed per modification algorithm.

===========================================
2. Produce Actual rankings (as ground truth)
===========================================

(1) Static phase: ExecHistory instrumentation

	sh execHistInstrAll.sh

(2) Run time phase: Run execHist-instrumented subjects 

	sh execHistRun_withoutSensaRunAll.sh

	* Alternatively, "execHistRunAll.sh" can be used to save time cost by running both the original and buggy versions pair by pair using class loaders, which is, however, not recommended for now given that we have encountered troubles with class loader in other applications.

(3) Post processing phase: Obtain rankings

	sh ./actualRankingForIndependentExechistRunAll.sh

	* Alternatively, "actualRankingAll.sh" can be used when execHistRunAll.sh was used for the run time phase.

Now, a "actualImpactRanking-v${ver}${seed}-changeLoc" file of ranking will be resulted for each seed.

===========================================
3. Produce experimental statistics
===========================================
(0) Preparation:
	mkdir -p Results
	mv actualImpactRanking* predictedImpactRanking* Results/
	cp addStaticSlices*.sh getEffectiveness*.sh getErrors*.sh getIdeal*.sh produceIdealSensaRanking.sh Results/

NOTE: before using the following scripts, both SensA and Actual ranking files as mentioned above should be ready and
match. And, they should be in the SAME directory as the following scripts since these scripts read required ranking 
files ONLY from current directory. The above Preparation steps are used for ensuring this.

(1) Make complete SensA ranking: add statements in the static forward slice that are missing in the current SensA ranking
(* first, make sure the forward slice file for each seed has been ready in the directories of instrumented class files under the upper level directory, such as "../instrumented-v1-s2-orig-5620/fwdslice.out"

	sh addStaticSlicesAll.sh

Now, original ranking files are moved to "./origSensaRankings/" and complete ranking files replace corresponding
original ones.

(2) Obtain effectiveness of SensA's predictive rankings versus Actual rankings

	sh getEffectivenessAll.sh

Now, "./costEffectiveness/" contains all effectiveness results, three files (accumulated and interpolated effectiveness
curves, and average costs of effectiveness) for each seed per modification algorithm.

(3) Obtain Ideal rankings and effectiveness curves, as references

	sh produceIdealSensaRanking.sh
	sh getIdealEffectivenessAll.sh

(4) Obtain ranking error measures (Optional)
	sh getErrorsAll.sh 
	sh getIdealErrorsAll.sh

(5) Get the summary data statistic table (Optional)

	python tabulateCostAndError.py IdealResultDir WSlicingResultDir DynWslicingResultDir SensaResultDir [IdealErrorDir WSlicingErrorDir DynWslicingErrorDir SensaErrorDir]

	* without WSlicing and/or DynWslicing related results, you can simply use Ideal/SensA results to fill the argument placeholders and then in the final result report simply ignore corresponding columns. This script is useful when you need prepare asummary report in, Excel sheets, say.

	* An example of usage can be found in "getAllCostAndErrors.sh"






==========================
Issues addressed to:

hcai@nd.edu

08/01/2013
==========================
