#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

source ./xmlsec_global.sh

#$ROOT/tools/DUAForensics-bins-code/DUAForensics
MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin/:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/DynWSlicing/bin:$ROOT/workspace/TestAdequacy/bin"

#$ROOT/tools/DUAForensics-bins-code/DUAForensics
SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/ProbSli/bin:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$subjectloc/bin/${ver}${seed}:$subjectloc/bin/td$ver:$ROOT/workspace/Sensa/bin"

LOGDIR=$subjectloc/out-DynwsliRank
mkdir -p $LOGDIR

INDIR=$subjectloc/DynwsliInstrumented-$ver-$seed-$change
OUTDIR=$subjectloc/DynWslicingResults-v1s20/
mkdir -p $OUTDIR

function Rank()
{
		#-debug \
		#-testNum 92 \
	java -Xmx20000m -ea -cp ${MAINCP} sli.DynWSlicing_specialRank \
		-start:$change \
		-dynWSlice \
		-version ${ver}${seed} \
		-pathBase $subjectloc/dynwsli_outdyn-${ver}${seed}-$change \
		1>$LOGDIR/dynwslice-$change-${ver}${seed}.out 2>$LOGDIR/dynwslice-$change-${ver}${seed}.err
}

pushd . 1>/dev/null 2>&1
cd ${INDIR}
starttime=`date +%s%N | cut -b1-13`
Rank
mv dynwsliceImpactRanking-$ver$seed-$change $OUTDIR/
#mv wslice* DepDepth* $LOGDIR/
popd
stoptime=`date +%s%N | cut -b1-13`
echo "Dynamic W-Slicing Ranking Time for $change-${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0


# hcai vim :set ts=4 tw=4 tws=4

