#!/bin/bash
# these are the real change locations to instrument at
C=(5620 5659 6159 4286 4946 4164 13932)  # after improved

for algo in rand inc observed;
do
	echo " -------- For the $algo Strategy ------- "
	i=0
	for N in 2 3 5 14 16 17 20;
	do
		echo "Now calculating effectiveness for v1 s$N at ${C[$i]} ... "
		sh ./wbfs_getEffectiveness.sh ${C[$i]} v1 s$N $algo

		let i=i+1
	done
done

echo "Ranking phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

