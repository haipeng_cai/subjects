#!/bin/bash
# these are the real change locations to instrument at
#C=(5614 5615 5655 5649 6159 4282 4942 4170 4164 13932)   # after corrections
C=(5620 5659 6159 4286 4946 4164 13932)  # after improved

BACKUPDIR=`pwd`/origSensaRankings
mkdir -p $BACKUPDIR
for algo in rand inc observed;
do
	echo " -------- For the $algo Strategy ------- "
	i=0
	#for N in 2 2 3 3 5 14 16 17 17 20;
	for N in 2 3 5 14 16 17 20;
	do
		echo -n "Now processing v1 s$N at ${C[$i]} ... "
		#sh ./addStaticSlicesStrict.sh predictedImpactRanking-v1s$N-${C[$i]}-inc actualImpactRanking-v1s$N-${C[$i]} \
		mv predictedImpactRanking-v1s$N-${C[$i]}-$algo $BACKUPDIR/
		cat $BACKUPDIR/predictedImpactRanking-v1s$N-${C[$i]}-$algo 1>predictedImpactRanking-v1s$N-${C[$i]}-$algo
		sh ./addStaticSlices.sh $BACKUPDIR/predictedImpactRanking-v1s$N-${C[$i]}-$algo \
								../instrumented-v1-s$N-orig-${C[$i]}/fwdslice.out \
								1>>predictedImpactRanking-v1s$N-${C[$i]}-$algo

		let i=i+1
		echo " done."
	done
done

echo "got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

