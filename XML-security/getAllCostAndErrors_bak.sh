python ./tabulateCostAndError.py  \
	Results/costEffectiveness  \
	ResultWslicing/costEffectiveness \
	Results/IdealCostEffectiveness  \
	Results/errorMetrics  \
	ResultWslicing/errorMetrics \
	Results/IdealErrorMetrics 

# hcai vim :set ts=4 tw=4 tws=4

