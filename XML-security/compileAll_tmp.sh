#!/bin/bash
LOGDIR=`pwd`/complog
#mkdir -p $LOGDIR

for N in 3 5 14 16 17 20;
do
	echo "compiling v1s$N-orig......"
	sh compile.sh  v1s$N-orig 
	#1>$LOGDIR/v1s$N-org.log 2>&1
	if [ $? -ne 0 ];then
		echo "compiling v1s$N-orig failed, aborted."
		exit 1
	fi
done

echo "All finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

