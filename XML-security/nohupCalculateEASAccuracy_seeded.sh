#!/bin/bash
#sh -c "python calculateEASAccuracy.py v1 s1-orig 1>seed_result_xmlsec_EASAccuracy 2>seed_log_xmlsec_EASAccuracy"
>seed_result_xmlsec_EASAccuracy 
>seed_log_xmlsec_EASAccuracy
for ((i=1;i<=20;i++));
do
	echo "Repetition $i" >> seed_log_xmlsec_EASAccuracy
	python calculateEASAccuracy.py v1 s1-orig 1>>seed_result_xmlsec_EASAccuracy 2>>seed_log_xmlsec_EASAccuracy
done

exit 0
# hcai vim :set ts=4 tw=4 tws=4

