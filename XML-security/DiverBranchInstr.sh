#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./xmlsec_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

mkdir -p out-DiverBrInstr

#$ROOT/tools/j2re1.4.2_18/lib/rt.jar:
SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$subjectloc/libs/bc-jce-jdk13-114.jar:$subjectloc/libs/commons-logging-api.jar:$subjectloc/libs/commons-logging.jar:$subjectloc/libs/junit3.8.1.jar:$subjectloc/libs/log4j-1.2.8.jar:$subjectloc/libs/style-apachexml.jar:$subjectloc/libs/stylebook-1.0-b3_xalan-2.jar:$subjectloc/libs/xalan.jar:$subjectloc/libs/xercesImpl-fixed:$subjectloc/libs/xml-apis.jar:$subjectloc/libs/xmlParserAPIs.jar:$subjectloc/bin/${ver}${seed}:$ROOT/workspace/mcia/bin:$subjectloc/bin/td$ver:$ROOT/workspace/Deam/bin

OUTDIR=$subjectloc/DiverBrInstrumented-$ver$seed
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-sclinit \
	#-wrapTryCatch \
	#-debug \
	#-dumpJimple \
	#-reachability \
   	#-duaverbose \
java -Xmx1600m -ea -cp ${MAINCP} Diver.DiverBranchInst \
	-w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
	-slicectxinsens \
	-dumpJimple \
	-allowphantom \
	-main-class org.apache.xml.security.test.AllTestsSelect \
	-entry:org.apache.xml.security.test.AllTestsSelect \
	-process-dir $subjectloc/bin/${ver}${seed} \
	-process-dir $subjectloc/bin/td${ver} \
	1>out-DiverBrInstr/instr-${ver}${seed}.out 2>out-DiverBrInstr/instr-${ver}${seed}.err

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds
cp -f $OUTDIR/entitystmt.out.branch $subjectloc/

#cp -fr resource $OUTDIR/org/apache/xml/security

echo "Copying data to prepare for running the instrumented subject..."
mkdir -p $OUTDIR/org/apache/xml/security/resource
mkdir -p $OUTDIR/org/apache/xml/security/test/resource
cp -fr resource/$ver/* $OUTDIR/org/apache/xml/security/resource/
cp -fr resource/test/* $OUTDIR/org/apache/xml/security/test/resource/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

