#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [changeLoc]"
	exit 1
fi

ver=$1
seed=$2
change=${3:-"-2147483648"}

source ./ant_global.sh

MAINCP=".:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia2.5/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/Deam/bin:$ROOT/workspace/ProbSli/bin"

mkdir -p out-DeamInstr

#/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/lib/tools.jar
#SOOTCP=.:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$subjectloc/bin/${ver}${seed}:$ROOT/workspace/mcia/bin

SOOTCP=.:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$subjectloc/bin/${ver}${seed}:$ROOT/workspace/mcia/bin:$ROOT/workspace/ProbSli/bin

for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

suffix=${ver}${seed}
[ $change -ne -2147483648 ] && suffix=$change-${ver}${seed}

LOGDIR=out-DeamInstr
mkdir -p $LOGDIR
logout=$LOGDIR/instr-$suffix.out
logerr=$LOGDIR/instr-$suffix.err

OUTDIR=$subjectloc/DeamInstrumented-$ver-$seed
[ $change -ne -2147483648 ] && OUTDIR=$OUTDIR-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-allowphantom \
   	#-duaverbose \
java -Xmx1600m -ea -cp ${MAINCP} deam.DeamInst \
	-w -cp ${SOOTCP} \
	-p cg verbose:false,implicit-entry:false -p cg.spark verbose:false,on-fly-cg:true,rta:true \
	-f c -d "$OUTDIR" -brinstr:off -duainstr:off \
	-allowphantom \
	-start:$change \
	-slicectxinsens \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	1> $logout 2> $logerr
stoptime=`date +%s%N | cut -b1-13`

echo "StaticAnalysisTime for $suffix elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Instrumentation done, now copying resources required for running."
cp -fr $subjectloc/source_allseeds/${ver}${seed}/main/org/apache/tools/ant/taskdefs/defaults.properties $OUTDIR/org/apache/tools/ant/taskdefs/
cp -fr $subjectloc/source_allseeds/${ver}${seed}/main/org/apache/tools/ant/types/defaults.properties $OUTDIR/org/apache/tools/ant/types/


echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

