#!/bin/sh
if [ $# -lt 1 ];then
	echo "Usage: $0 verDir"
	exit 1
fi

verDir=$1

source ./ant_global.sh

mkdir -p bin/${verDir}

MAINCP=".:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/InstrReporters/bin:$ROOT/source_allseeds/$verDir"

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done
javac -g:source -source 1.4 -cp ${MAINCP} -d bin/$verDir @${verDir}files.lst  #1>err 2>&1

cp $subjectloc/source_allseeds/$verDir/main/org/apache/tools/ant/taskdefs/defaults.properties bin/$verDir/org/apache/tools/ant/taskdefs/
cp $subjectloc/source_allseeds/$verDir/main/org/apache/tools/ant/types/defaults.properties bin/$verDir/org/apache/tools/ant/types/

exit 0

LOGFILE=${verDir}-compile.out
#> $LOGFILE
cat ${verDir}files.lst | dos2unix | sed 's/\\/\//g' | while read fn;
do
	echo "compiling $fn ....."
	#javac -g:source -source 1.4 -cp ${MAINCP} -d bin/$verDir $fn 1>/dev/null 2>&1
	javac -g:source -source 1.4 -cp ${MAINCP} -d bin/$verDir $fn #1>/dev/null 2>&1
	#javac -cp ${MAINCP} -d bin/$verDir $fn 1>>$LOGFILE 2>&1
	if [ $? -ne 0 ];
	then
		echo "Failed to compile $fn, bail out."
		exit 1
	fi
done

echo "now copying resources required for running."
#cp -fr $subjectloc/source_allseeds/${verDir}/core/org/apache/jmeter/resources bin/${verDir}/org/apache/jmeter/
#cp -fr $subjectloc/source_allseeds/${verDir}/core/org/apache/jmeter/help.txt bin/${verDir}/org/apache/jmeter/
#cp -fr $subjectloc/source_allseeds/${verDir}/core/org/apache/jmeter/images bin/${verDir}/org/apache/jmeter/

if [ -d bin/${verDir}/profile ];
then
	rm -rf bin/${verDir}/profile
fi

echo "Compilation all done." 
exit 0

# hcai vim :set ts=4 tw=4 tws=4
