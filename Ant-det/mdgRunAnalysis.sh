#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./ant_global.sh

INDIR=$subjectloc/DiverInstrumented-$ver$seed

#$ROOT/tools/j2re1.4.2_18/lib/rt.jar
MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$INDIR:$ROOT/workspace/mcia/bin"

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

starttime=`date +%s%N | cut -b1-13`

	#"-fullseq" 
	#-stmtcov
java -Xmx12800m -ea -cp ${MAINCP} MDG.MdgRunAnalysis \
	"$subjectloc" \
	${ver}${seed} \
	$DRIVERCLASS \
	"" \
	112 

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

