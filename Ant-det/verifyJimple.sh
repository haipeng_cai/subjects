#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [changeLoc]"
	exit 1
fi

ver=$1
seed=$2
change=${3:-"-2147483648"}

source ./ant_global.sh

MAINCP=".:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia2.5/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/Deam/bin:$ROOT/workspace/ProbSli/bin"

#/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/lib/tools.jar
#SOOTCP=.:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$subjectloc/bin/${ver}${seed}:$ROOT/workspace/mcia/bin

INDIR=$subjectloc/DeamInstrumented-$ver-$seed
[ $change -ne -2147483648 ] && OUTDIR=$INDIR-$change

SOOTCP=.:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$INDIR:$ROOT/workspace/mcia/bin:$ROOT/workspace/ProbSli/bin

for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

suffix=${ver}${seed}
[ $change -ne -2147483648 ] && suffix=$change-${ver}${seed}

	#org.apache.tools.ant.IntrospectionHelper$13 \
	#org.apache.tools.ant.IntrospectionHelper$2 \
	#org.apache.tools.ant.IntrospectionHelper$3 \
	#org.apache.tools.ant.IntrospectionHelper$4 \
java -Xmx1600m -ea -cp ${MAINCP} soot.Main -f J -cp ${SOOTCP} \
	org.apache.tools.ant.IntrospectionHelper\$2 \
	-d `pwd`

exit 0


# hcai vim :set ts=4 tw=4 tws=4

