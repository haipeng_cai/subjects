#!/bin/bash

if [ $# -lt 1 ];then
	echo "too few arguments"
	exit 1
fi

src=${1:-"-"}
cat "$src" | while read fn;
do 
	#sed s'/System.currentTimeMillis()/org.apache.jmeter.samplers.SampleResult.getCurrentMillis()/g' $fn > /tmp/__m

	#sed s'/System.currentTimeMillis()/jmock.sysclock.getCurrentMillis()/g' $fn > /tmp/__m
	#sed s'/org.apache.jmeter.samplers.SampleResult.getCurrentMillis()/jmock.sysclock.getCurrentMillis()/g' $fn > /tmp/__m
	sed s'/System.currentTimeMillis()/jmock.sysclock.getCurrentMillis()/g' $fn > /tmp/__m
	mv /tmp/__m $fn
	echo "done with $fn"
done
#done <<<"$src"

exit 0


# hcai vim :set ts=4 tw=4 tws=4

