#!/bin/bash
if [ $# -lt 1 ];then
    echo "Usage: $0 seed"
    exit 1
fi
source ./bcel_global.sh

ver=$VERSION
seed=$1 #s1-orig

MAINCP=".:$rtlib:$toollib:$SOOTLIBS:$duafdir/DUAForensics/bin:$duafdir/Sensa/bin/:$duafdir/LocalsBox/bin:$duafdir/InstrReporters/bin:$duafdir/ProbSli/bin/:$duafdir/TestAdequacy/bin:$subjectloc/bin/$ver$seed"
for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

INDIR=$subjectloc/bin/$ver$seed
OUTDIR=$subjectloc/rawRun_outdyn
mkdir -p $OUTDIR
function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	cat $subjectloc/inputs/testinputs.txt | dos2unix | \
	while read testname;
	do
	    let i=i+1
	    
	    echo "Run Test #$i..... $testname"
	    java -Xmx20480m -ea -cp $MAINCP $DRIVERCLASS $testname 1> $OUTDIR/$i.out 2> $OUTDIR/$i.err
	done
}

starttime=`date +%s%N | cut -b1-13`

pushd . 1>/dev/null 2>&1
cd ${INDIR}

#create testinputs.txt
#java -Xmx20480m -ea -cp $MAINCP $DRIVERCLASS -printTestsName $subjectloc/inputs/testinputs.txt
#java -Xmx20480m -ea -cp $MAINCP $DRIVERCLASS -runTest testLineNumberCount
RunOneByOne

popd

DATE=`date "+%a %m/%d/%Y %H:%M:%S $HOSTNAME"`
#echo "mcia-accuracy study: Compile $subjectname completed. Took $exectime milliseconds $DATE" | mutt -s "mcia-accuracy study: Compile $subjectname completed." hcai@nd.edu
grep -a -E "Total runs|failureCount" * | awk '{if($NF==1)print $0}'

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0
