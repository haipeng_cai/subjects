#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./bcel_global.sh

MAINCP=".:$rtlib:$toollib:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

LOGDIR=out-dMDGInstr
suffix=${ver}${seed}
mkdir -p $LOGDIR
logout=$LOGDIR/instr-$suffix.out
logerr=$LOGDIR/instr-$suffix.err

SOOTCP=.:$rtlib:$toollib:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$subjectloc/bin/${ver}${seed}:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin

for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/dMDGInstrumented-$ver$seed
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-debug \
	#-dumpJimple \
	#-wrapTryCatch \
	#-dumpFunctionList \
	#-reachability \
	#-debug \
   	#-duaverbose \
	#-ignoreRTECD \
	#-intraCD \
	#-interCD \
	#-exInterCD \
	#-ignoreRTECD \
	#-serializeVTG \
#java -Xmx1600m -ea -cp ${MAINCP} Diver.DiverInst \
java -Xmx45000m -ea -cp ${MAINCP} MDG.DynMDGInst \
	-w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-d "$OUTDIR" -brinstr:off -duainstr:off \
	-allowphantom \
	-dumpFunctionList \
	-wrapTryCatch \
	-intraCD \
	-interCD \
	-exInterCD \
	-ignoreRTECD \
	-serializeMDG \
	-slicectxinsens \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	1> $logout 2> $logerr

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Copying data to prepare for running the instrumented subject..."
cp -fr $subjectloc/src/${ver}${seed}/src/examples/* $OUTDIR/
cp -fr $subjectloc/src/${ver}${seed}/src/main/java/* $OUTDIR/
cp -fr $subjectloc/src/${ver}${seed}/src/test/java/* $OUTDIR/
cp -fr $subjectloc/bin/${ver}${seed}/org/apache/bcel/data/* $OUTDIR/org/apache/bcel/data/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

