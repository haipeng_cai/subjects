#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./bcel_global.sh

MAINCP=".:$rtlib:$toollib:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

LOGDIR=$subjectloc/out-DynAliasInstr
mkdir -p $LOGDIR
logout=$LOGDIR/instr-$ver$seed.out
logerr=$LOGDIR/instr-$ver$seed.err

SOOTCP=.:$rtlib:$toollib:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$subjectloc/bin/${ver}${seed}:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin

for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/DynAliasInstrumented-$ver$seed
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-allowphantom \
	#-debug \
	#-dumpJimple \
	#-wrapTryCatch \
	#-dumpFunctionList \
	#-statUncaught \
	#-intraCD \
	#-interCD \
	#-exInterCD \
	#-ignoreRTECD \
   	#-duaverbose \
	#-serializeVTG \
java -Xmx14g -ea -cp ${MAINCP} Diver.DynAliasInst \
	-w -cp ${SOOTCP} \
	-p cg verbose:true,implicit-entry:false -p cg.spark verbose:true,on-fly-cg:true,rta:true \
	-f c -d "$OUTDIR" -brinstr:off -duainstr:off \
	-allowphantom \
	-dumpJimple \
	-cachingOIDs \
	-slicectxinsens \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	1> $logout 2> $logerr
stoptime=`date +%s%N | cut -b1-13`

echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Copying data to prepare for running the instrumented subject..."
cp -fr $subjectloc/src/${ver}${seed}/src/examples/* $OUTDIR/
cp -fr $subjectloc/src/${ver}${seed}/src/main/java/* $OUTDIR/
cp -fr $subjectloc/src/${ver}${seed}/src/test/java/* $OUTDIR/
cp -fr $subjectloc/bin/${ver}${seed}/org/apache/bcel/data/* $OUTDIR/org/apache/bcel/data/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

