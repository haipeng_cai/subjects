#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./bcel_global.sh

INDIR=$subjectloc/EAInstrumented-$ver$seed

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$subjectloc/libs/xercesImpl-fixed:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Sensa/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin"

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

OUTDIR=$subjectloc/EAoutdyn-${ver}${seed}
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#"-fullseq" 
	#-fullseq \
	#-fullseq
#pushd . 1>/dev/null 2>&1
#cd ${INDIR}
java -Xmx14g -ea -cp ${MAINCP} EAS.EARun \
	$DRIVERCLASS \
	"$subjectloc" \
	"$INDIR" \
	${ver}${seed} \
	$OUTDIR \

#popd

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

