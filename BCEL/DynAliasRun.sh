#!/bin/bash
if [ $# -lt 1 ];then
	echo "Usage: $0 revision"
	exit 1
fi
Rev=$1

source ./bcel_global.sh
INDIR=$subjectloc/DynAliasInstrumented-$Rev/

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$subjectloc/libs/xercesImpl-fixed:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Sensa/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin"
for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

OUTDIR=$subjectloc/DynAliasoutdyn-$Rev/
mkdir -p $OUTDIR

function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	cat $subjectloc/inputs/testinputs.txt | dos2unix | \
	while read testname;
	do
		let i=i+1

		echo "Run Test #$i" # [" $testname "] ....."
		java -Xmx4000m -ea -cp $MAINCP  $DRIVERCLASS $testname 1> $OUTDIR/test$i.out 2> $OUTDIR/test$i.err
		if [ -s $OUTDIR/$i.err ];then
			echo "$testname failed"
		else
			echo "$testname passed"
		fi
	done
}

function RunbyRunner()
{
		#"-fullseq" 
		#-ninstanceLevel
		#-instanceLevel
	java -Xmx14g -ea -cp ${MAINCP} Diver.DiverRun \
		$DRIVERCLASS \
		"$subjectloc" \
		"$INDIR" \
		"$Rev" \
		$OUTDIR \
		-cachingOIDs \
		-ninstanceLevel
}

starttime=`date +%s%N | cut -b1-13`
#RunOneByOne
RunbyRunner
stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for r$Rev elapsed: " `expr $stoptime - $starttime` milliseconds
cp $OUTDIR/*.emo $subjectloc/Diveroutdyn-$Rev/

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

