#!/bin/bash
source ./bcel_global.sh
if [ $# -lt 1 ];then
	echo "Usage: $0 revision"
	exit 1
fi

rev=$1
NT=${2:-"75"}

MAINCP=".:$rtlib:$toollib:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin"
for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

starttime=`date +%s%N | cut -b1-13`

	#"-fullseq" 
	#-stmtcov
	#-nstmtcovdynalias \
	#-ninstanceprune \
	#-stmtcov \
	#-postprune \
	#-stmtcovdynalias \
	#-instanceprune \
	#-nstmtcovdynalias \
	#-ninstanceprune 
	#-stmtcov \
	#-postprune 
	#-stmtcovdynalias \
	#-instanceprune
java -Xmx14g -ea -cp ${MAINCP} Diver.RunAnalysis \
	"$subjectloc" \
	"$rev" \
	$DRIVERCLASS \
	"" \
	$NT \
	-dynalias \
	-instanceprune 

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${rev} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

