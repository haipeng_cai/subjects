import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;

import org.apache.commons.cli.*;

import profile.BranchReporter;
import profile.DUAReporter;
import profile.ExecHistReporter;
import change.DynSliceReporter;
import junit.framework.*;


public class AllDriversMain {

	private static CommandLine cmd;
	
	private static Options options;
	private static Option printTestsNames = new Option("printTestsName", true, "if this opt is presented, then will write testsnames to the given filepath");
	private static Option runTest = new Option("runTest", true, "if this option is presented, then will run the test specified");	
	
	static void __link() { 
		BranchReporter.__link(); 
		DUAReporter.__link(); 
		DynSliceReporter.__link();
		ExecHistReporter.__link(); 
		Sensa.Modify.__link(); 
	}
	
	public static void main(String[] args) throws Exception {
		options = new Options();
		options.addOption(printTestsNames);
		options.addOption(runTest);
		
		CommandLineParser parser = new BasicParser();
		cmd = parser.parse(options, args);
		
		TestSuite suite = new TestSuite("All Bcel tests");
		initializesuite(suite);

		if(cmd.hasOption(printTestsNames.getOpt())){
			String testsnames_file = cmd.getOptionValue(printTestsNames.getOpt());
			FileWriter testsnames_writer = null;
			if( testsnames_file != null ){
				try {
					testsnames_writer = new FileWriter(new File(testsnames_file));
					int[] results = enumerateTests_recursive(suite, testsnames_writer);
					int runs = results[0];
					int errors = results[1];
					System.out.println("Total runs: " + runs + " errors: " + errors);
					testsnames_writer.close();
				} catch (IOException e1) {
					e1.printStackTrace();
					throw new RuntimeException("Cannot write testsnames");
				}
			}
		}
		
		if(cmd.hasOption(runTest.getOpt())){
			String testname = cmd.getOptionValue(runTest.getOpt());
			if(testname != null){
				int[] results = enumerateTests_recursive(suite, testname);
				int runs = results[0];
				int errors = results[1];
				if (runs != 1)
					System.out.println("Warning: No test to execute or Too many tests. TestName: " + testname);
				
				System.out.println("Total runs: " + runs + " errors: " + errors);
			}
			else{
				System.out.println("Please specify the testname that you want to run. -runTest *testname*");
			}
		}
	}


	private static int[] enumerateTests_recursive(TestSuite suite, String testname) {
		
		int runs = 0;
		int errors = 0;
				
		for(@SuppressWarnings("unchecked")
		Enumeration<Test> e = suite.tests(); e.hasMoreElements();){
			Object nextelement = e.nextElement();
			if(nextelement instanceof TestCase){
				TestCase t = (TestCase) nextelement;
				
				if(t.getName().equals("warning")){
					System.out.println("  enumerate tests error(there is no testcases in the suite): " + suite.getName() + t.getName());
					continue;
				}
				else{
					if(t.getName().equals(testname)){
						prerun(t);
						TestResult r = t.run();
						postrun(t);
						runs = runs + 1;
						int errorcount = r.errorCount();
						int failurecount = r.failureCount();
						
						if(errorcount > 0 || failurecount > 0){
							errors = errors + 1;
							printTestWithError(t, r);
						}
						break;
					}					
				}
			}
			else if(nextelement instanceof TestSuite){
				TestSuite s = (TestSuite) nextelement;
				int[] returns_1 = enumerateTests_recursive(s, testname);
				runs = runs + returns_1[0];
				errors = errors + returns_1[1];
			}
		}
				
		int[] returns = new int[2];
		returns[0] = runs;
		returns[1] = errors;
		return returns;
		
	}


	private static void printTestWithError(TestCase t, TestResult r) {
		int errorcount = r.errorCount();
		int failurecount = r.failureCount();
		int runcount = r.runCount();

		for (@SuppressWarnings("unchecked")
			Enumeration<TestFailure> ef = r.failures();ef.hasMoreElements();){
			TestFailure f = ef.nextElement();
			System.out.println(f.exceptionMessage());
		}

		System.out.println(t.toString());
		System.out.println("   Has errors or failures: " + runcount 
				+ " errorCount: " + errorcount
				+ " failureCount: " + failurecount);
	}

	private static int[] enumerateTests_recursive(TestSuite suite, FileWriter testsnames_writer) throws IOException {
		int runs = 0;
		int errors = 0;
				
		for(@SuppressWarnings("unchecked")
		Enumeration<Test> e = suite.tests(); e.hasMoreElements();){
			Object nextelement = e.nextElement();
			if(nextelement instanceof TestCase){
				TestCase t = (TestCase) nextelement;
				if(testsnames_writer != null){
					testsnames_writer.write(t.getName() + "\n");
				}
				
				if(t.getName().equals("warning")){
					System.out.println("  enumerate tests error(there is no testcases in the suite): " + suite.getName() + t.getName());
					continue;
				}
				else{
					prerun(t);
					TestResult r = t.run();
					postrun(t);
					
					runs = runs + 1;
					int errorcount = r.errorCount();
					int failurecount = r.failureCount();
					
					if(errorcount > 0 || failurecount > 0){
						errors = errors + 1;
						printTestWithError(t, r);
					}
				}
			}
			else if(nextelement instanceof TestSuite){
				TestSuite s = (TestSuite) nextelement;
				int[] returns_1 = enumerateTests_recursive(s, testsnames_writer);
				runs = runs + returns_1[0];
				errors = errors + returns_1[1];
			}
		}
				
		int[] returns = new int[2];
		returns[0] = runs;
		returns[1] = errors;
		return returns;
	}

	private static void postrun(TestCase t) {
		// add files or delete files
		return;
	}

	private static void prerun(TestCase t) {
		// add files or delete files
		return;
	}

	private static void initializesuite(TestSuite suite) {
		suite.addTestSuite(org.apache.bcel.CounterVisitorTestCase.class);
		suite.addTestSuite(org.apache.bcel.AnonymousClassTestCase.class);
		suite.addTestSuite(org.apache.bcel.util.InstructionFinderTest.class);
		suite.addTestSuite(org.apache.bcel.AnnotationGenTestCase.class);
		suite.addTestSuite(org.apache.bcel.EnclosingMethodAttributeTestCase.class);
		suite.addTestSuite(org.apache.bcel.ElementValueGenTestCase.class);
		suite.addTestSuite(org.apache.bcel.GeneratingAnnotatedClassesTestCase.class);
		suite.addTestSuite(org.apache.bcel.InstructionFinderTestCase.class);
		suite.addTestSuite(org.apache.bcel.FieldAnnotationsTestCase.class);
		suite.addTestSuite(org.apache.bcel.EnumAccessFlagTestCase.class);
		suite.addTestSuite(org.apache.bcel.AnnotationAccessFlagTestCase.class);
		suite.addTestSuite(org.apache.bcel.AnnotationDefaultAttributeTestCase.class);
		suite.addTestSuite(org.apache.bcel.PerformanceTest.class);
	}
}
