#!/bin/bash

VERSION=v5
SEEDS=(1)
DRIVERCLASS=AllDriversMain
subjectloc=/var/home/hcai/SVNRepos/star-lab/trunk/Subjects/BCEL
subjectname=BCEL
testnumber=75
ROOT=/home/hcai

duafdir=$ROOT/workspace/

#Soot Lib
sootlibdir=$ROOT/tools/Soot-libs-2.3.0
SOOTLIBS=$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar

#Java
#rtlib=/etc/alternatives/java_sdk/jre/lib/rt.jar
#rtlib=/var/home/hcai/tools/j2re1.4.2_18/lib/rt.jar
#toollib=/etc/alternatives/java_sdk/lib/tools.jar
#jcelib=/etc/alternatives/java_sdk/jre/lib/jce.jar

jdkhome=/afs/nd.edu/user36/hcai/tools/jdk160

rtlib=$jdkhome/jre/lib/rt.jar
toollib=$jdkhome/lib/tools.jar
jcelib=$jdkhome/jre/lib/jce.jar

