#sh ./EAInstr.sh v0 s1-orig > timing.EAInstr
#sh ./DiverInstr.sh v0 s1-orig > timing.DiverInstr

#sh ./EARun.sh v0 s1-orig > timing.EARun
#sh ./DiverRun.sh v0 s1-orig > timing.DiverRun

sh ./DynAliasRun.sh v5s1-orig

#sh DiverRunAnalysis.sh v5s1-orig 1>bcel_EASVsDiverResults 2>bcel_EASVsDiverLog
#sh DiverRunAnalysis.sh v5s1-orig 1>bcel_EASVsDiverStmtCovResults 2>bcel_EASVsDiverStmtCovLog
#sh DiverRunAnalysis.sh v5s1-orig 1>bcel_EASVsDiverStmtCovDynaliasMeResults 2>bcel_EASVsDiverStmtCovDynaliasMeLog
#sh DiverRunAnalysis.sh v5s1-orig 1>bcel_EASVsDiverStmtCovDynaliasMeInsResults 2>bcel_EASVsDiverStmtCovDynaliasMeInsLog
#sh DiverRunAnalysis.sh v5s1-orig 1>bcel_EASVsDiverDynaliasMeResults 2>bcel_EASVsDiverDynaliasMeLog
sh DiverRunAnalysis.sh v5s1-orig 1>bcel_EASVsDiverDynaliasMeInsResults 2>bcel_EASVsDiverDynaliasMeInsLog

# hcai vim :set ts=4 tw=4 tws=4

