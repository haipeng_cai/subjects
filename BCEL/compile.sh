#!/bin/sh
if [ $# -lt 1 ];then
	echo "Usage: $0 verDir"
	exit 1
fi

verDir=$1 #v5s1-orig

source ./bcel_global.sh

OUTDIR="bin/${verDir}"
mkdir -p $OUTDIR

MAINCP=".:$rtlib:$toollib:$SOOTLIBS:$duafdir/InstrReporters/bin:$duafdir/DUAForensics/bin:$duafdir/Sensa/bin:$duafdir/mcia/bin:$duafdir/Deam/bin:$subjectloc/src/$verDir/src/examples:$subjectloc/src/$verDir/src/main/java:$subjectloc/src/$verDir/src/test/java"

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

starttime=`date +%s%N | cut -b1-13`

#javac -g -source 1.5 -target jsr14 -cp ${MAINCP} -d $OUTDIR @${verDir}.javafilelist # 2 tests failed
#javac -source 1.5 -target jsr14 -cp ${MAINCP} -d $OUTDIR @${verDir}.javafilelist # 4 tests failed
#javac -g -cp ${MAINCP} -d $OUTDIR @${verDir}.javafilelist # 0 tests failed
#javac -g -source 1.5 -cp ${MAINCP} -d $OUTDIR @${verDir}.javafilelist # 0 tests failed
#javac -g -source jsr14 -cp ${MAINCP} -d $OUTDIR @${verDir}.javafilelist # 0 tests failed

javac -g -target jsr14 -cp ${MAINCP} -d $OUTDIR @${verDir}.javafilelist # 0 tests failed
javac -g -target jsr14 -cp ${MAINCP} -d $OUTDIR @${verDir}.datafilelist # 0 tests failed

stoptime=`date +%s%N | cut -b1-13`
exectime=`expr $stoptime - $starttime`

#copy resource files
cp -fr $subjectloc/src/$verDir/src/examples/* $OUTDIR/
cp -fr $subjectloc/src/$verDir/src/main/java/* $OUTDIR/
cp -fr $subjectloc/src/$verDir/src/test/java/* $OUTDIR/

DATE=`date "+%a %m/%d/%Y %H:%M:%S $HOSTNAME"`

#echo "mcia-accuracy study: Compile $subjectname completed. Took $exectime milliseconds $DATE" | mutt -s "mcia-accuracy study: Compile $subjectname completed." hcai@nd.edu
exit 0
