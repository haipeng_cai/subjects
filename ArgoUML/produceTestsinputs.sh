#!/bin/bash
source ./argo_global.sh
mkdir -p $subjectloc/inputs

cp test-names-all.lst $subjectloc/inputs/testinputs.txt
cp test-names-all_raw.lst $subjectloc/inputs/testinputs_raw.txt
cp test-names-coreonly.lst $subjectloc/inputs/testinputs_coreonly.txt

# hcai vim :set ts=4 tw=4 tws=4
