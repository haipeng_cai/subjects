#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

source ./argo_global.sh

ver=$1
seed=$2
change=0

MAINCP=".:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/Deam/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin"

SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin/:$ROOT/workspace/Deam/bin:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$subjectloc/bin/$ver$seed"

for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/test-instrumented-$ver-$seed
mkdir -p $OUTDIR

mkdir -p out-test-instr

starttime=`date +%s%N | cut -b1-13`

#    -duaverbose \
#    instrumentAll $change \  #for test instrumentation
    #startIdOnly $change \
java -Xmx20480m -ea -cp ${MAINCP} InstrumentationTester \
    blocklistend \
    instrumentAll $change \
    -w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
    -p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
    -d $OUTDIR \
    -brinstr:off -duainstr:off \
    -slicectxinsens \
    -allowphantom \
    -paramdefuses \
    -keeprepbrs \
    -main-class $DRIVERCLASS \
    -entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
    1>out-test-instr/instr-${ver}${seed}.out 2>out-test-instr/instr-${ver}${seed}.err 

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds >> overhead.test-instrumentation.txt

echo "Instrumentation done, now copying resources required for running."
cp -rf $subjectloc/src/${ver}${seed}/org/argouml/Images $OUTDIR/org/argouml/
cp -rf $subjectloc/src/${ver}${seed}/org/argouml/i18n/*.properties $OUTDIR/org/argouml/i18n/
cp -rf $subjectloc/src/${ver}${seed}/org/argouml/resource $OUTDIR/org/argouml/
cp -rf $subjectloc/src/${ver}${seed}/org/argouml/xml/dtd $OUTDIR/org/argouml/xml/
cp -rf $subjectloc/src/${ver}${seed}/org/argouml/{argo.ini,default.xmi,registry.xml} $OUTDIR/org/argouml/

echo "Running finished."
exit 0

# hcai vim :set ts=4 tw=4 tws=4

