#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [number of traces]"
	exit 1
fi

ver=$1
seed=$2

source ./argo_global.sh

SEADIR=$subjectloc/SEAAnalyzed-${ver}${seed}
MDGDIR=$subjectloc/MDGAnalyzed-$ver$seed
FWDDIR=$subjectloc/RDFWDAnalyzed-$ver$seed

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$INDIR"

starttime=`date +%s%N | cut -b1-13`

	#"$MDGDIR"/mdgResults.dat  \
java -Xmx10800m -ea -cp ${MAINCP} SEA.SeaMdgComp \
	"$MDGDIR"/mdgResults.dat  \
	"$FWDDIR"/sliceResults.dat 2>/dev/null

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

