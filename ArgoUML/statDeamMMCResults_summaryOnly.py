#!/usr/bin/env python
import os
import sys
import string
import numpy

g_datapoints=[]

# data source locations
g_input=""
g_suffix=""
g_scope=2

DELIMITOR=' '

## @brief print usage
## @param none
## @retrun none
def Usage():
	print >> sys.stdout, "%s [input_file|-]" % sys.argv[0]
	return

## @brief parse command line to get user's input for source file name and target
## @param none
## @retrun none
def ParseCommandLine():
	global g_datapoints
	global g_suffix
	global g_scope
	argc = len(sys.argv)
	if argc >= 2:
		#for i in range(1, argc):
		if sys.argv[1]!="-":
			if not os.path.exists( sys.argv[1] ):
				raise IOError, "source file given at [%s] does not exist, bailed out now." % sys.argv[1]
		g_input = sys.argv[1]
		if argc >= 3:
			g_suffix = sys.argv[2]
			if argc >= 4:
				g_scope = int(sys.argv[3])
	'''
	else:
		Usage()
		raise Exception, "too few arguments, aborted."
	'''

def retrieveStats():
	global g_input
	global g_suffix
	global g_datapoints
	global g_scope
	fh = None
	if len(g_input)>0 and g_input!="-":
		fh = file(g_input,'r')
	else:
		fh = sys.stdin

	if len(g_input)>0 and None == fh:
		raise Exception, "Failed to open file named %s\n" % g_input
	curline = fh.readline()
	fw=0
	while curline:
		flds=string.split(curline)
		'''
		if len(flds) < 8:
			curline = fh.readline()
			continue
		'''
		if fw==0:
			fw = len(flds)
		else:
			assert fw == len(flds)

		#print "line="+curline
		curlist=[]
		#for i in range(len(flds)-1,len(flds)-9,-1):
		#for i in range(len(flds)-1,len(flds)-9,-1):
		for i in range(fw-1,-1,-1):
			curlist.append( float( flds[i] ) )

		g_datapoints.append(curlist)

		curline = fh.readline()

	if len(g_input)>0:
		fh.close()

	'''
	sums=[0 0 0 0 0 0 0 0]
	for j in range(7,-1):
		for i in range(0, len(g_datapoints)):
			sums[7-i] += g_datapoints[i][j]
	avgs=[0 0 0 0 0 0 0 0]
	for i in range(0,8):
		avgs[i] = sums[i] / len(g_datapoints)
	'''

	tg=numpy.transpose(g_datapoints)
	'''
	NCratio=tg[0].count(0)*1.0/len(tg[0])
	prefix=""
	'''

	if g_scope > 0:
		#print g_suffix+'\tmean\t',
		if len(g_suffix)>0:
			print g_suffix+'\t',
		#for i in range(0,8):
		for i in range(0,fw):
			print >> sys.stdout, "%.4f\t" % ( numpy.mean( tg[i] ) ),
		#print 
	''' median is an optional metric
	'''
	if g_scope > 2:
		print g_suffix+'\tmedian\t',
		#for i in range(0,8):
		for i in range(0,fw):
			print >> sys.stdout, "%.4f\t" % ( numpy.median( tg[i] ) ),
		print 
	if g_scope > 1:
		print g_suffix+'\tstdev\t',
		#for i in range(0,8):
		for i in range(0,fw):
			print >> sys.stdout, "%.4f\t" % ( numpy.std( tg[i] ) ),
		print 

######################################
# the boost
if __name__ == "__main__":
	try:
		ParseCommandLine()
	except Exception,e:
		print >> sys.stderr, e
		sys.exit(1)

	retrieveStats()

	sys.exit(0)

# hcai vim set ts=4 tw=100 sts=4 sw=4


