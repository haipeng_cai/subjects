#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [changeLoc]"
	exit 1
fi

ver=$1
seed=$2
change=$3
source ./argo_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/Deam/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin"

INDIR=$subjectloc/DeamInstrumented-$ver-$seed
#INDIR=$subjectloc/bin/${ver}${seed}

#$ROOT/tools/j2re1.4.2_18/lib/rt.jar:
SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin/:$ROOT/workspace/Deam/bin:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$INDIR"

for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

	#org.apache.xml.security.c14n.Canonicalizer \
	#org.apache.xml.security.test.AllTestsSelect \
	#org.apache.xml.security.test.external.org.apache.xalan.XPathAPI.AttributeAncestorOrSelf \
	#$DRIVERCLASS \
	#org.argouml.util.CheckUMLModelHelper \
	#org.argouml.uml.ui.ActionAddMessage \
	#org.argouml.util.CheckUMLModelHelper \
	#TabConstraints"$"ConstraintModel"$"CR"$"1 \
	#org.argouml.uml.ui.TabConstraints"$"ConstraintModel"$"CR"$"1 \
	#org.argouml.uml.ui.ActionAddMessage \
	#org.argouml.uml.ui.TabConstraints"$"ConstraintModel"$"CR"$"1CRTemp \
	#org.argouml.uml.ui.TabConstraints"$"ConstraintModel"$"CR"$"CRTemp \
	#org.argouml.uml.ui.CRTemp \
	#org.argouml.util.CheckUMLModelHelper \
java -Xmx1600m -ea -cp ${MAINCP} soot.Main -f J -cp ${SOOTCP} \
	org.argouml.model.uml.foundation.core.TestCoreFactory \
	-d `pwd`

exit 0


# hcai vim :set ts=4 tw=4 tws=4

