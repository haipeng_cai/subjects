#!/bin/bash
if [ $# -lt 2 ];then
	echo "$0 ver seed"
	exit 1
fi

prefix=$1$2

function copyTools()
{
scp -r \
	~/workspace/Deam \
	~/workspace/Sensa \
	~/workspace/InstrReporters \
	~/workspace/TestAdequacy \
	~/workspace/mcia \
	~/workspace/ProbSli \
	${CRCFE}:workspace/
}

function copyData()
{
	#NonMutEHoutdyn-$prefix \
scp -r \
	MutEAInstrumented-$prefix \
	MutEHInstrumented-$prefix \
	${CRCFE}:SVNRepos/star-lab/trunk/Subjects/ArgoUML/
}

#copyTools
copyData

exit 0

# hcai vim :set ts=4 tw=4 tws=4

