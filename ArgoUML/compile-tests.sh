#! /bin/sh
if [ $# -lt 1 ];then
	echo "Usage: $0 verDir"
	exit 1
fi

verDir=$1

source ./argo_global.sh

mkdir -p bin/${verDir}

MAINCP=".:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/InstrReporters/bin:$subjectloc/bin/$verDir:$ROOT/workspace/mcia/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Sensa/bin"

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

echo "Compiling the tests..."
javac -g:source -source 1.4 -cp ${MAINCP} -d bin/$verDir @${verDir}files-tests.lst  #1>err 2>&1

echo "Copying data needed..."
cp -rf src/$verDir/org/argouml/Images bin/$verDir/org/argouml/
cp -rf src/$verDir/org/argouml/i18n/*.properties bin/$verDir/org/argouml/i18n/
cp -rf src/$verDir/org/argouml/resource bin/$verDir/org/argouml/
cp -rf src/$verDir/org/argouml/xml/dtd bin/$verDir/org/argouml/xml/
cp -rf src/$verDir/org/argouml/{argo.ini,default.xmi,registry.xml} bin/$verDir/org/argouml/

exit 0


# hcai vim :set ts=4 tw=4 tws=4

