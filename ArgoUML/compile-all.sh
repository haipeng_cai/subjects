#! /bin/sh

source ./argo_global.sh

for N in ${SEEDS}
do
	echo "Compiling ${VERSION}s$N..."
	sh compile.sh ${VERSION}s$N-orig
	
	if [ $? -ne 0 ]; then
		echo "[ERROR] Compiling ${VERSION}s$N failed!"
	fi

	echo "Compiling tests for ${VERSION}s$N..."
	sh compile-tests.sh ${VERSION}s$N-orig
	if [ $? -ne 0 ]; then
		echo "[ERROR] Compiling ${VERSION}s$N tests failed!"
	fi
done

echo "All versions compiled."
exit 0

# hcai vim :set ts=4 tw=4 tws=4

