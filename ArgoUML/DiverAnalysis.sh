#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [number of traces]"
	exit 1
fi

ver=$1
seed=$2
NT=${3:-"211"}
#query=${4:-"<org.argouml.application.helpers.ResourceLoaderWrapper: void <init>()>"}
query=${4:-"<org.argouml.kernel.Project: void addMember(ru.novosoft.uml.model_management.MModel)>"}
query=${4:-"<org.argouml.model.uml.foundation.extensionmechanisms.ExtensionMechanismsHelper: java.lang.String getMetaModelName(ru.novosoft.uml.foundation.core.MModelElement)>"}

source ./argo_global.sh

INDIR=$subjectloc/Diveroutdyn-${ver}${seed}
BINDIR=$subjectloc/DiverInstrumented-$ver$seed

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$INDIR"

starttime=`date +%s%N | cut -b1-13`
	#"main" \
	#-debug
	#-dynalias \
	#-postprune \
	#-debug
java -Xmx2800m -ea -cp ${MAINCP} Diver.DiverAnalysis \
	"$query" \
	"$INDIR" \
	"$BINDIR" \
	$NT \
	-stmtcovdynalias \
	-ninstanceprune

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4
