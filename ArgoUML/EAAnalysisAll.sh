#!/bin/bash
source ./argo_global.sh
if [ $# -lt 4 ];then
	echo "Usage: $0 version seed number_Of_traces querylist_file"
	exit 1
fi
ver=$1
seed=$2
NT=$3
fnlist=$4

function RunOneByOne()
{
	local i=0
	cat $fnlist | dos2unix | \
	while read query;
	do
		let i=i+1

		echo "Querying $query ..."
		sh EAAnalysis.sh $ver $seed $NT \""$query"\"
	done
}

starttime=`date +%s%N | cut -b1-13`
RunOneByOne
stoptime=`date +%s%N | cut -b1-13`
echo "Query Time for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0



# hcai vim :set ts=4 tw=4 tws=4

