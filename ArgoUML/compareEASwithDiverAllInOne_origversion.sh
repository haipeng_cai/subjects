#sh ./EAInstr.sh v0 s1-orig  > timing.EAInstr
#sh ./DiverInstr.sh v0 s1-orig > timing.DiverInstr

#sh ./EARun.sh v0 s1-orig  > timing.EARun
#sh ./DiverRun.sh v0 s1-orig > timing.DiverRun

#python ./compareEASwithDiver.py v0 s1-orig 1>argo_EASVsDiverResults_origversion 2>&1 

#echo "first run Diver-RT "
#cp -r DiverResult_fullCD_orgversion_ExInterCD_includeRTECD/DiverInstrumented-v0-s1-orig .

#sleep 5400 
#sh DynAliasRun.sh v0 s1-orig >> timing.dynalias
#cp DynAliasoutdyn-v0s1-orig/*.em* Diveroutdyn-v0s1-orig/

sh ./DiverRunAnalysis.sh v0 s1-orig 1>argoStmtcovDynAlias_EASVsDiverResults_origversion 2>argoStmtcovDynAlias_EASVsDiverLog

#rm -rf ./DiverInstrumented-v0-s1-orig
#echo "now run Diver-nonRT "
#cp -r DiverResult_fullCD_orgversion_ExInterCD_ignoredRTECD/DiverInstrumented-v0-s1-orig .
#sh ./DiverRunAnalysis.sh v0 s1-orig 1>argoNonRT_EASVsDiverResults_origversion 2>argoNonRT_EASVsDiverLog


# hcai vim :set ts=4 tw=4 tws=4

