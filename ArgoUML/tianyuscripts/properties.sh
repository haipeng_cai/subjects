#!/bin/sh

#----!!!! Modify these !!!!----#
JAVA_HOME=/usr/lib/jvm/java-1.6.0-openjdk.x86_64
#ARGO_ROOT_DIR=/afs/nd.edu/user1/txu2/ND/DEAM/subject/ArgoUML
#DEAM_PATH="/afs/nd.edu/user1/txu2/ND/DEAM"
ARGO_ROOT_DIR=/var/home/hcai/SVNRepos/star-lab/trunk/Subjects/ArgoUML/
DEAM_PATH="/afs/nd.edu/user1/txu2/ND/DEAM"

# Output file locations
RUN_COREGUI_OUT_DIR=${ARGO_ROOT_DIR}/build/$VER_SEED/tests/run_coregui_out
RUN_COREONLY_OUT_DIR=${ARGO_ROOT_DIR}/build/$VER_SEED/tests/run_coreonly_out
RUN_COREGUI_UNINST_OUT_DIR=${ARGO_ROOT_DIR}/build/$VER_SEED/tests/run_coregui_uninst_out
RUN_COREONLY_UNINST_OUT_DIR=${ARGO_ROOT_DIR}/build/$VER_SEED/tests/run_coreonly_uninst_out
#----!!!! Modify these !!!!----#



# Java tools locations
JAVA_TOOLS_CLASSPATH=$JAVA_HOME/lib/tools.jar

# Directory locations
ARGO_BUILD_DIR=${ARGO_ROOT_DIR}/build/$VER_SEED
ARGO_TOOLS_DIR=${ARGO_ROOT_DIR}/tools
ARGO_LIB_DIR=${ARGO_ROOT_DIR}/lib
ARGO_SRC_DIR=${ARGO_ROOT_DIR}/src/$VER_SEED
ARGO_TOOLS_LIB=${ARGO_TOOLS_DIR}/lib

# Test directory locations
ARGO_TESTS_SRC=${ARGO_ROOT_DIR}/tests
ARGO_TESTS_DIR=${ARGO_BUILD_DIR}/tests
ARGO_TESTS_CLASSES=${ARGO_TESTS_DIR}/classes
ARGO_TESTS_REPORTS=${ARGO_TESTS_DIR}/reports

# Build directory locations
ARGO_BUILD_CLASSES=${ARGO_BUILD_DIR}/classes
ARGO_BIN_DIR=${ARGO_BUILD_DIR}/bin
ARGO_DIST_DIR=${ARGO_BUILD_DIR}/dist
ARGO_JAVADOCS_DIR=${ARGO_BUILD_DIR}/javadocs

# Source directory locations
ARGO_BIN_SRC=${ARGO_SRC_DIR}/bin

# Tools directory locations
ARGO_TOOLS_LIB_DIR=${ARGO_TOOLS_DIR}/lib

# Libs location
XERCES_JAR_PATH=${ARGO_TOOLS_DIR}/ant-1.4.1/lib/xerces-1.2.3.jar
NSUML_JAR_PATH=${ARGO_LIB_DIR}/nsuml-0.4.19.jar
OCL_ARGO_JAR_PATH=${ARGO_LIB_DIR}/ocl-argo.jar
GEF_JAR_PATH=${ARGO_LIB_DIR}/gef-0.9.6.jar
ANTLRALL_JAR_PATH=${ARGO_LIB_DIR}/antlrall.jar
LOG4J_JAR_PATH=${ARGO_LIB_DIR}/log4j-1.1.3.jar
JH_JAR_PATH=${ARGO_LIB_DIR}/jh.jar

ARGO_COMPILE_CLASSPATH=${XERCES_JAR_PATH}:${NSUML_JAR_PATH}:${OCL_ARGO_JAR_PATH}:${GEF_JAR_PATH}:${ANTLRALL_JAR_PATH}:${LOG4J_JAR_PATH}:${JH_JAR_PATH}

JUNIT_JAR_PATH=${ARGO_TOOLS_DIR}/junit-3.7/junit.jar

ARGO_RUNTIME_CLASSPATH=${ARGO_COMPILE_CLASSPATH}:$JUNIT_JAR_PATH

# DEAM classpaths
DEAM_LIB_PATH="${DEAM_PATH}/lib"
DEAM_COMPILE_PATH=$DEAM_LIB_PATH/InstrReporters.jar
