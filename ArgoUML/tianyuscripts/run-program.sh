#! /bin/sh

if [ $# -lt 1 ]; then
	echo "Usage: $0 v*s*"
	exit 1
fi

VER_SEED=$1

source ./properties.sh

echo `pwd`

java -cp $ARGO_BUILD_CLASSES:$ARGO_RUNTIME_CLASSPATH org.argouml.application.Main
