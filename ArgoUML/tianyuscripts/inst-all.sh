#! /bin/sh

VER=v0

for N in 1;
do
	echo "Instrumenting ${VER}s$N..."
	sh inst.sh ${VER} s$N 10
	
	if [ $? -ne 0 ]; then
		echo "[ERROR] Instrumenting ${VER}s$N failed!"
	fi
done

echo "All versions Instrumented."
exit 0
