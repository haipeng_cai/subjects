#! /bin/sh

VER=v0

for N in 1;
do
	echo "Compiling ${VER}s$N..."
	sh compile.sh ${VER}s$N
	
	if [ $? -ne 0 ]; then
		echo "[ERROR] Compiling ${VER}s$N failed!"
	fi
done

echo "All versions compiled."
exit 0
