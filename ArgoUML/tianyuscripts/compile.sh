#! /bin/sh

if [ $# -lt 1 ]; then
	echo "Usage: $0 v*s*"
	exit 1
fi

VER_SEED=$1

######## INIT target ########
source ./properties.sh

######## PREPARE target ########
rm -rf ${ARGO_BUILD_DIR}

mkdir -p ${ARGO_BUILD_DIR}
mkdir -p ${ARGO_BUILD_DIR}/ext

mkdir -p $ARGO_BUILD_CLASSES/org/argouml
cp -rf $ARGO_SRC_DIR/org/argouml/Images $ARGO_BUILD_CLASSES/org/argouml
find $ARGO_BUILD_CLASSES/org/argouml/Images -type d -name ".svn" | xargs rm -rf

mkdir -p $ARGO_BUILD_CLASSES/org/argouml/i18n
cp -rf $(find $ARGO_SRC_DIR/org/argouml/i18n -type f -name "*.properties") $ARGO_BUILD_CLASSES/org/argouml/i18n

cp -rf $ARGO_SRC_DIR/org/argouml/resource $ARGO_BUILD_CLASSES/org/argouml
find $ARGO_BUILD_CLASSES/org/argouml/resource -type d -name ".svn"|xargs rm -rf

mkdir -p $ARGO_BUILD_CLASSES/org/argouml/xml
cp -rf $ARGO_SRC_DIR/org/argouml/xml/dtd $ARGO_BUILD_CLASSES/org/argouml/xml
find $ARGO_BUILD_CLASSES/org/argouml/xml/dtd -type d -name ".svn"|xargs rm -rf

cp $ARGO_SRC_DIR/org/argouml/argo.ini $ARGO_BUILD_CLASSES/org/argouml/argo.ini
cp $ARGO_SRC_DIR/org/argouml/default.xmi $ARGO_BUILD_CLASSES/org/argouml/default.xmi
cp $ARGO_SRC_DIR/org/argouml/registry.xml $ARGO_BUILD_CLASSES/org/argouml/registry.xml

######## CHECK_GEF_LOCALIZER target ########

######## CHECK_GEF target ########

######## CHECK_LOG4J target ########

######## CHECK_ANTLR target ########

######## UPDATE-VERSION target ########

######## CHECK-DEPENDENCIES target ########

######## COMPILE target ########
echo "Compiling the sources..."
javac -g:none -nowarn -source 1.4 -cp $ARGO_COMPILE_CLASSPATH -d $ARGO_BUILD_CLASSES @compile-file-$VER_SEED.lst

######## COMPILE-TESTS target ########
echo "Compiling the tests..."
mkdir -p $ARGO_TESTS_CLASSES
javac -g:none -nowarn -source 1.4 -cp $ARGO_BUILD_CLASSES:$JUNIT_JAR_PATH:$ARGO_COMPILE_CLASSPATH:$DEAM_COMPILE_PATH -d $ARGO_TESTS_CLASSES @compile-test-file.lst

exit 0
