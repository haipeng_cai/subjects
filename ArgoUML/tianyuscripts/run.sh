#!/bin/bash
# *** PARAMS   1=change 2=ver 3=seed
# *** EXAMPLE    1941    %2     s2-orig
#change=$1
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

DEAMPATH="/afs/nd.edu/user1/txu2/ND/DEAM"
subjectloc=${DEAMPATH}/subject/JMeter
LIBPATH="${DEAMPATH}/lib"

MAINCP=".:/afs/nd.edu/user1/txu2/ND/Java1.4/rt.jar:$LIBPATH/polyglot-1.3.5.jar:$LIBPATH/sootclasses-2.3.0.jar:$LIBPATH/jasminclasses-2.3.0.jar:$LIBPATH/DUAForensics.jar:$LIBPATH/LocalsBox.jar:$LIBPATH/InstrReporters.jar:$LIBPATH/java_cup-1.3.5.jar:$LIBPATH/ProbSli.jar:$DEAMPATH/inst_out/JMeter/$ver$seed"

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

OUTDIR="/home/txu2/run_result/JMeter/$ver$seed"
mkdir -p $OUTDIR


function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	#cat $subjectloc/test_names.txt | dos2unix | \
	cat $subjectloc/inputs/testinputs.txt | dos2unix | \
	while read testname;
	do
		let i=i+1

		echo "Run Test #$i....."
		java -Xmx4000m -ea -cp $MAINCP org.apache.jorphan.test.AllTestsSelect $testname  \
		1> $OUTDIR/$i.out 2> $OUTDIR/$i.err
	done
}

RunOneByOne

exit 0


