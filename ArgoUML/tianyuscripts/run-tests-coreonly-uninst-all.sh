#! /bin/sh

VER=v0

for N in 1;
do
	echo "Running uninstrumented core tests of ${VER}s$N..."
	sh run-tests-coreonly-uninst.sh ${VER}s$N
	
	if [ $? -ne 0 ]; then
		echo "[ERROR] Running uninstrumented core tests of ${VER}s$N failed!"
	fi
done

echo "All running finished."
exit 0
