#!/bin/bash

for N in 1 2 5 6 11 13 19; # all 7 changes selected after s9 removed (change has no impacts!) and change s11 added
do
	echo "Now Running instrumented v2 s$N ..."
	sh run.sh v2 s$N
	
	echo "Now Running instrumented v2 s$N-orig ..."
	sh run.sh v2 s$N-orig
done

echo "Normal Running now got ALL done."

exit 0

