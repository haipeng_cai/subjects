#! /bin/sh

VER=v0

for N in 1;
do
	echo "Running uninstrumented core and gui tests of ${VER}s$N..."
	sh run-tests-coregui-uninst.sh ${VER}s$N
	
	if [ $? -ne 0 ]; then
		echo "[ERROR] Running uninstrumented core and gui tests of ${VER}s$N failed!"
	fi
done

echo "All running finished."
exit 0
