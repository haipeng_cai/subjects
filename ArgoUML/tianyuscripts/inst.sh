#! /bin/sh

if [ $# -lt 3 ];then
	echo "Usage: $0 version seed changeLoc"
	exit 1
fi

CHANGE_LOC=$3
VER=$1
SEED=$2
VER_SEED=${VER}${SEED}

source ./properties.sh

SUBJECT_PATH=${DEAM_PATH}/subject/ArgoUML

MAINCP="/afs/nd.edu/user1/txu2/ND/Java1.4/rt.jar:${DEAM_PATH}/bin:$DEAM_LIB_PATH/polyglot-1.3.5.jar:$DEAM_LIB_PATH/sootclasses-2.3.0.jar:$DEAM_LIB_PATH/jasminclasses-2.3.0.jar:$DEAM_LIB_PATH/DUAForensics.jar:$DEAM_LIB_PATH/LocalsBox.jar:$DEAM_LIB_PATH/InstrReporters.jar:$DEAM_LIB_PATH/java_cup-1.3.5.jar:$DEAM_LIB_PATH/ProbSli.jar"

OUTDIR="${DEAM_PATH}/inst_out/ArgoUML/${VER_SEED}"

mkdir -p $OUTDIR

SOOTCP="/afs/nd.edu/user1/txu2/ND/Java1.4/rt.jar:$DEAM_LIB_PATH/DUAForensics.jar:$DEAM_LIB_PATH/LocalsBox.jar:$DEAM_LIB_PATH/InstrReporters.jar:$DEAM_LIB_PATH/java_cup-1.3.5.jar:$DEAM_LIB_PATH/ProbSli.jar:$DEAM_LIB_PATH/TestAdequacy.jar:${DEAM_PATH}/bin:$DEAM_LIB_PATH/polyglot-1.3.5.jar:$DEAM_LIB_PATH/jasminclasses-2.3.0.jar:$DEAM_LIB_PATH/sootclasses-2.3.0.jar:$ARGO_BUILD_CLASSES:$ARGO_TESTS_CLASSES:$ARGO_RUNTIME_CLASSPATH"



java -Xmx10240m -ea -cp ${MAINCP} deam.DeamInst \
	-w -cp $SOOTCP -p cg verbose:true,implicit-entry:false \
	-p cg.spark verbose:true,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
  	-duaverbose \
	-slicectxinsens \
	-start:$CHANGE_LOC \
	-main-class org.argouml.AllTestsSelect \
	-entry:org.argouml.AllTestsSelect \
	-process-dir $ARGO_TESTS_CLASSES \
	-process-dir $ARGO_BUILD_CLASSES \
	1>$OUTDIR/inst.out 2>$OUTDIR/inst.err

echo "Instrumenting finished."
exit 0

