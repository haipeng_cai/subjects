#! /bin/sh

if [ $# -lt 1 ]; then
	echo "Usage: $0 v*s*"
	exit 1
fi

VER_SEED=$1

source ./properties.sh

######## JUNIT-INIT target ########

######## JUNIT-GET-991015-DTD target ########
#echo "Cannot find: tests/testmodels/Model.dtd"

######## JUNIT-GET-011202-XML target ########
#echo "Cannot find: tests/testmodels/01-12-02.xml"

######## TESTS target ########

######## RUN TESTS ########
echo "Clean the run out dir: $RUN_COREONLY_UNINST_OUT_DIR"
rm -rf $RUN_COREONLY_UNINST_OUT_DIR
mkdir -p $RUN_COREONLY_UNINST_OUT_DIR

i=0
cat test-names-coreonly.lst | while read testname;
do
	let i=i+1
	echo "Run Test #$i: $testname"
	touch $RUN_COREONLY_UNINST_OUT_DIR/$i.out
	touch $RUN_COREONLY_UNINST_OUT_DIR/$i.err
	java -ea -cp $ARGO_BUILD_CLASSES:$ARGO_TESTS_CLASSES:$ARGO_RUNTIME_CLASSPATH org.argouml.AllTestsSelect_uninst $testname \
	1> $RUN_COREONLY_UNINST_OUT_DIR/$i.out 2> $RUN_COREONLY_UNINST_OUT_DIR/$i.err
done

exit 0
