// Copyright (c) 1996-99 The Regents of the University of California. All
// Rights Reserved. Permission to use, copy, modify, and distribute this
// software and its documentation without fee, and without a written
// agreement is hereby granted, provided that the above copyright notice
// and this paragraph appear in all copies.  This software program and
// documentation are copyrighted by The Regents of the University of
// California. The software program and documentation are supplied "AS
// IS", without any accompanying services from The Regents. The Regents
// does not warrant that the operation of the program will be
// uninterrupted or error-free. The end-user understands that the program
// was developed for research purposes and is advised not to rely
// exclusively on the program for any reason.  IN NO EVENT SHALL THE
// UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
// SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS,
// ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
// THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE. THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
// PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
// CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
// UPDATES, ENHANCEMENTS, OR MODIFICATIONS.


// File: ActionGoToCritique.java
// Classes: ActionGoToCritique
// Original Author: agauthie
// $Id: ActionGoToCritique.java 3025 2002-11-23 22:04:48Z kataka $

package org.argouml.ui;

import java.awt.event.ActionEvent;

import org.argouml.cognitive.ToDoItem;
import org.argouml.cognitive.ui.ToDoPane;
import org.argouml.uml.ui.UMLAction;

public class ActionGoToCritique extends UMLAction {
  ToDoItem _item = null;

  public ActionGoToCritique(ToDoItem item) {
    super(item.getHeadline(), NO_ICON);
    _item = item;
  }

  public void actionPerformed(ActionEvent ae) {
    ProjectBrowser.TheInstance.getTodoPane().selectItem(_item);
  }

  public boolean shouldBeEnabled() { return true; }

} /* end class ActionGoToCritique */
