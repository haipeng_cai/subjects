// Copyright (c) 1996-2002 The Regents of the University of California. All
// Rights Reserved. Permission to use, copy, modify, and distribute this
// software and its documentation without fee, and without a written
// agreement is hereby granted, provided that the above copyright notice
// and this paragraph appear in all copies.  This software program and
// documentation are copyrighted by The Regents of the University of
// California. The software program and documentation are supplied "AS
// IS", without any accompanying services from The Regents. The Regents
// does not warrant that the operation of the program will be
// uninterrupted or error-free. The end-user understands that the program
// was developed for research purposes and is advised not to rely
// exclusively on the program for any reason.  IN NO EVENT SHALL THE
// UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
// SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS,
// ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
// THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF
// SUCH DAMAGE. THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
// PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
// CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
// UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

package org.argouml.uml.ui;

import java.awt.event.ActionEvent;
import java.util.Iterator;

import org.argouml.kernel.Project;
import org.argouml.kernel.ProjectManager;
import org.argouml.model.uml.UmlFactory;
import org.argouml.model.uml.UmlModelEventPump;
import org.argouml.ui.ProjectBrowser;
import org.tigris.gef.presentation.Fig;

import ru.novosoft.uml.MElementListener;
import ru.novosoft.uml.foundation.core.MAttribute;
import ru.novosoft.uml.foundation.core.MClass;
import ru.novosoft.uml.foundation.core.MClassifier;

/** Action to add an attribute to a classifier.
 *  @stereotype singleton
 */
public class ActionAddAttribute extends UMLChangeAction {

    ////////////////////////////////////////////////////////////////
    // static variables

    public static ActionAddAttribute SINGLETON = new ActionAddAttribute();

   


    ////////////////////////////////////////////////////////////////
    // constructors

    public ActionAddAttribute() { super("button.add-attribute"); }


    ////////////////////////////////////////////////////////////////
    // main methods

    public void actionPerformed(ActionEvent ae) {
	ProjectBrowser pb = ProjectBrowser.TheInstance;
	Project p = ProjectManager.getManager().getCurrentProject();
	Object target = pb.getTarget();
        if (target instanceof Fig) {
            target = ((Fig)target).getOwner();
        }
	if (!(target instanceof MClassifier)) return;
	MClassifier cls = (MClassifier) target;
	MAttribute attr = UmlFactory.getFactory().getCore().buildAttribute(cls);
	pb.getNavigatorPane().addToHistory(attr);
	pb.setTarget(attr);
        Iterator it = pb.getEditorPane().findPresentationsFor(cls, p.getDiagrams()).iterator();
        while (it.hasNext()) {
            MElementListener listener = (MElementListener)it.next();
            UmlModelEventPump.getPump().removeModelEventListener(listener, attr);
            UmlModelEventPump.getPump().addModelEventListener(listener, attr);
        }
	super.actionPerformed(ae);
    }

    public boolean shouldBeEnabled() {
	ProjectBrowser pb = ProjectBrowser.TheInstance;
	Object target = pb.getDetailsTarget();
	/*
	if (target instanceof MInterface) {
		return Notation.getDefaultNotation().getName().equals("Java");
	}
	*/
	return target instanceof MClass;		
    }
} /* end class ActionAddAttribute */
