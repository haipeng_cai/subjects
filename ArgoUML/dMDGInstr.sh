#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./argo_global.sh

MAINCP=".:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin"

SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin/:$ROOT/workspace/mcia/bin:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/Deam/bin:$subjectloc/bin/$ver$seed"

for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

suffix=${ver}${seed}

LOGDIR=out-dMDGInstr
mkdir -p $LOGDIR
logout=$LOGDIR/instr-$suffix.out
logerr=$LOGDIR/instr-$suffix.err

OUTDIR=$subjectloc/dMDGInstrumented-$ver$seed
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`

	#-allowphantom \
   	#-duaverbose \
	#-wrapTryCatch \
	#-dumpJimple \
	#-dumpFunctionList \
	#-ignoreRTECD \
	#-debug \
	#-exInterCD \
java -Xmx45000m -ea -cp ${MAINCP} MDG.DynMDGInst \
	-w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
	-allowphantom \
	-dumpFunctionList \
	-wrapTryCatch \
	-intraCD \
	-interCD \
	-serializeVTG \
	-slicectxinsens \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	1> $logout 2> $logerr

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for $suffix elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Instrumentation done, now copying resources required for running."
cp -rf $subjectloc/src/${ver}${seed}/org/argouml/Images $OUTDIR/org/argouml/
cp -rf $subjectloc/src/${ver}${seed}/org/argouml/i18n/*.properties $OUTDIR/org/argouml/i18n/
cp -rf $subjectloc/src/${ver}${seed}/org/argouml/resource $OUTDIR/org/argouml/
cp -rf $subjectloc/src/${ver}${seed}/org/argouml/xml/dtd $OUTDIR/org/argouml/xml/
cp -rf $subjectloc/src/${ver}${seed}/org/argouml/{argo.ini,default.xmi,registry.xml} $OUTDIR/org/argouml/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

