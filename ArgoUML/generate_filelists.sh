#!/bin/bash

source ./argo_global.sh

BASEDIR=$subjectloc/src
for N in 1-orig;
do
	> ${VERSION}s${N}files.lst
	for subdir in " ";
	do
		find $BASEDIR/${VERSION}s$N/$subdir -name "*.java" 1>>${VERSION}s${N}files.lst
	done

	> ${VERSION}s${N}files-tests.lst
	for subdir in " ";
	do
		find $BASEDIR/td${VERSION}/$subdir -name "*.java" 1>>${VERSION}s${N}files-tests.lst
	done
done

exit 0

# hcai vim :set ts=4 tw=4 tws=4
