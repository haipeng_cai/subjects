#!/bin/bash

#for gsz in 10 9 8;
for gsz in 7 6 5 4 3 2;
do
	python ./MMC_calculateEASAccuracy.py $gsz v0 s1-orig 1>result_argo_EASAccuracy_s$gsz 2>log_argo_EASAccuracy_s$gsz
done

# hcai vim :set ts=4 tw=4 tws=4

