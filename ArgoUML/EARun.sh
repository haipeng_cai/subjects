#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [changeloc]"
	exit 1
fi

ver=$1
seed=$2
change=${3:-"-2147483648"}

source ./argo_global.sh

INDIR=$subjectloc/EAInstrumented-$ver$seed
[ $change -ne -2147483648 ] && INDIR=$INDIR-$change

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$INDIR"

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

suffix=${ver}${seed}
[ $change -ne -2147483648 ] && suffix=$change-${ver}${seed}

OUTDIR=EAoutdyn-${ver}${seed}
[ $change -ne -2147483648 ] && OUTDIR=$OUTDIR-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#"-fullseq" 
	#"-callmap" \
java -Xmx2800m -ea -cp ${MAINCP} EAS.EARun \
	$DRIVERCLASS \
	"$subjectloc" \
	"$INDIR" \
	${ver}${seed} \
	$OUTDIR 

stoptime=`date +%s%N | cut -b1-13`

echo "RunTime for $suffix elapsed: " `expr $stoptime - $starttime` milliseconds
exit 0

# hcai vim :set ts=4 tw=4 tws=4
