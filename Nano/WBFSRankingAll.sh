#!/bin/bash
source ./nano_global.sh
i=0

for algo in rand inc observed;
do
	echo "Now for the $algo strategy ---"
	i=0

	for N in ${SEEDS[@]};
	do
		echo -n Now Ranking impact for ${VERSION} s$N $algo at ${C[$i]} ... 
		sh WBFSRanking.sh ${C[$i]} ${VERSION} s$N $algo

		let i=i+1
	done
done

exit 0


# hcai vim :set ts=4 tw=4 tws=4

