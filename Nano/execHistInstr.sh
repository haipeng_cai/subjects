#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi
source ./nano_global.sh

change=$1
ver=$2
seed=$3

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin"

mkdir -p out-execHistInstr

SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin/:$ROOT/workspace/Sensa/bin:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$subjectloc/bin/$ver$seed:$subjectloc/bin/td-${ver}${seed}"

OUTDIR=$subjectloc/execHistInstrumented-$ver-$seed-$change
mkdir -p $OUTDIR

java -Xmx6000m -ea -cp ${MAINCP} sli.ProbSli \
	-w -cp $SOOTCP -p cg verbose:true,implicit-entry:false \
	-p cg.spark verbose:true,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
	-nopslice \
   	-duaverbose \
	-slicectxinsens \
	-start:$change \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	-process-dir $subjectloc/bin/td-${ver}${seed} \
	1>out-execHistInstr/execHistInstr-$change-${ver}${seed}.out 2>out-execHistInstr/execHistInstr-$change-${ver}${seed}.err

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

