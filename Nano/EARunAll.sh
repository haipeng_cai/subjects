#!/bin/bash
source ./nano_global.sh

for N in ${SEEDS[@]};
do
	echo "Now Running instrumented s$N "
	sh EARun.sh $VERSION s$N

	let i=i+1
done

echo "Running phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

