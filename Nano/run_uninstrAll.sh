#!/bin/bash
source ./nano_global.sh

for N in ${SEEDS[@]};
do
	echo "Now Running Uninstrumented $VERSION s$N-orig ..."
	sh run_uninstr.sh $VERSION s$N-orig
done

echo "Normal Running now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

