#!/bin/bash
source ./nano_global.sh

i=0
for N in ${SEEDS[@]};
do
	echo "Now DynwsliceInstrumenting s$N-orig at ${C[$i]} ..."
	sh DynwsliInstr.sh ${C[$i]} ${VERSION} s$N-orig

	let i=i+1
done

exit 0


# hcai vim :set ts=4 tw=4 tws=4

