#!/bin/bash
source ./nano_global.sh

i=0
for N in ${SEEDS[@]};
do
	echo "Now Running instrumented s$N-orig at ${C[$i]} ..."
	sh DynwsliRun.sh ${C[$i]} ${VERSION} s$N-orig

	let i=i+1
done

echo "Running phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

