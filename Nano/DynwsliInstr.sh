#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi
source ./nano_global.sh

change=$1
ver=$2
seed=$3

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin/:$ROOT/workspace/InstrReporters/bin/:$ROOT/workspace/DynWSlicing/bin"

SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/DynWSlicing/bin:$ROOT/workspace/TestAdequacy/bin/:$ROOT/workspace/Sensa/bin:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$subjectloc/bin/$ver$seed:$subjectloc/bin/td-$ver$seed"

LOGDIR=out-DynwsliInstr
mkdir -p $LOGDIR

OUTDIR=$subjectloc/DynwsliInstrumented-$ver-$seed-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-debug \
	#-allowphantom \
	#-p cg verbose:true,safe-newinstance:true \
java -Xmx8000m -ea -cp ${MAINCP} sli.DynWSlicing \
	-w -cp $SOOTCP -p cg verbose:true,implicit-entry:false \
	-p cg.spark verbose:true,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
   	-duaverbose -start:$change \
	-fdynslice \
	-slicectxinsens \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	-process-dir $subjectloc/bin/td-${ver}${seed} \
	1>$LOGDIR/dynwslice-$change-${ver}${seed}.out 2>$LOGDIR/dynwslice-$change-${ver}${seed}.err
stoptime=`date +%s%N | cut -b1-13`
echo "Dynamic W-Slicing Instrumentation Time for $change-${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0


# hcai vim :set ts=4 tw=4 tws=4

