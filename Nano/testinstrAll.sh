#!/bin/bash

#Step 1: make sure the souce codes have imported the following packages:
#        import profile.ValueAndTypeReporter;
#Step 2: added a __link method in main class
#	 static void __link() {
#	    ValueAndTypeReporter.__link();
#	 }
#Step 3: edit nano_global.sh. make sure it includes the following definitions:
#        VERSION=v1 #the version of nano source codes
#        SEEDS=(1 2 3 4 5 6 7) #the seeds that we use for experimenting nano subject
#        DRIVERCLASS=AllDriversMain #the entry class of nano experimentation
#        subjectloc=/mnt/filedisk/work/sensa-paper/data/new/workspace/Nano #the current experimentation directory
#        inputxml_prefix=$subjectloc/inputs-lnx/ #the inputs directory
#        C=(2506 2514 2688 2448 2911 2990 3095) #the change location
#        duafdir=/mnt/filedisk/work/duaf/Soot #the workspace of duaf svn solution
#        sootlibdir=/mnt/filedisk/work/duaf/Soot/Soot-libs-2.3.0 #the directory that includes the libraries that soot needs
#        rtlib=/usr/java/jdk1.6.0_45/jre/lib/rt.jar
#        jcelib=/usr/java/jdk1.6.0_45/jre/lib/jce.jar
#Step 4: make sure inputs/testinputs.txt is correct


source ./nano_global.sh

getTestIds(){
    filename=$1
    rm -fr ${filename}_
    cat $filename | dos2unix | \
	while read testname;
    do
	index=`expr match "$testname" '[A-Za-z0-9\-]*/'`
	length=`expr length $testname`
	sublength=`expr $length - $index - 4`
	substring=`expr ${testname:$index:$sublength}`
	echo $substring >> ${filename}_
    done
}

testAllseeds(){
  # local variable x and y with passed args	
  local suffix=$1
  
  local i=0
  for N in ${SEEDS[@]};
  do
      # normal run
      sh run_uninstr.sh ${VERSION} s$N$suffix
      # test instrumentation, and run instrumented program
      sh testinstr.sh ${VERSION} s$N$suffix ${C[$i]}
      sh testinstr-run.sh ${VERSION} s$N$suffix
            
      # find the tests that fails in normal run and fails in instrumented run
      find Runout-${VERSION}s$N$suffix-uninstr/*.err -size +0 > tmp1
      find Runout-${VERSION}s$N$suffix-testinstr/*.err -size +0 > tmp2
      getTestIds "tmp1"
      getTestIds "tmp2"
      # compare the failed tests in normal run and instrumented run
      diff tmp1_ tmp2_
      # remove temporary files
      rm -fr tmp1 tmp2
      rm -fr tmp1_ tmp2_
      
      # find any statement that has two different reported states
      python ./rununinstr-postprocess.py -d "Runout-${VERSION}s$N$suffix-uninstr" > rununinstr-postprocess.${VERSION}s$N$suffix
      python ./testinstr-postprocess.py -d "Runout-${VERSION}s$N$suffix-testinstr" > testinstr-postprocess.${VERSION}s$N$suffix

      python ./testinstr-compareuninstr-instr.py -s $N$suffix > compareuninstr-instr.${VERSION}s$N$suffix

      # The following steps find tests that executes change locations
      grep -rl "ValueReporter" Runout-${VERSION}s$N$suffix-testinstr/*.out > ${VERSION}s$N$suffix-tests
      
      rm -fr $subjectloc/${VERSION}s$N$suffix-tests-final
      cat $subjectloc/${VERSION}s$N$suffix-tests | dos2unix | \
	  while read testname;
      do
	  index=`expr match "$testname" '[A-Za-z0-9\-]*/'`
	  length=`expr length $testname`
	  sublength=`expr $length - $index - 4`
	  substring=`expr ${testname:$index:$sublength}`
	  echo $substring >> $subjectloc/${VERSION}s$N$suffix-tests-final
      done

      let i=i+1
  done
}

testAllseeds ""
testAllseeds "-orig"



