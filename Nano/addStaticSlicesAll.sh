#!/bin/bash
source ../nano_global.sh

i=0

BACKUPDIR=`pwd`/origSensaRankings
mkdir -p $BACKUPDIR
for algo in rand inc observed;
do
	echo " -------- For the $algo Strategy ------- "
	i=0
	for N in ${SEEDS[@]};
	do
		echo -n "Now processing ${VERSION} s$N at ${C[$i]} ... "
		mv predictedImpactRanking-${VERSION}s$N-${C[$i]}-$algo $BACKUPDIR/
		cat $BACKUPDIR/predictedImpactRanking-${VERSION}s$N-${C[$i]}-$algo 1>predictedImpactRanking-${VERSION}s$N-${C[$i]}-$algo
		sh ./addStaticSlices.sh $BACKUPDIR/predictedImpactRanking-${VERSION}s$N-${C[$i]}-$algo \
								../instrumented-${VERSION}-s$N-orig-${C[$i]}/fwdslice.out \
								1>>predictedImpactRanking-${VERSION}s$N-${C[$i]}-$algo

		let i=i+1
		echo " done."
	done
done

echo "got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

