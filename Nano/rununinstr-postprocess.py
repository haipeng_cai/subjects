import argparse

parser = argparse.ArgumentParser(description='Postprocessing for instrumentation tester.')
parser.add_argument('-d','--dir', help='specify the input directory', required=True)
args = vars(parser.parse_args())

dir =  args['dir'];

for fileid in range(1,214):
    filedir = dir + '/' + str(fileid) + ".out";
    #print 'open: ' + filedir;
    file = open(filedir);
    
    findresult = 0;
    for line in file:
        if(line.startswith("OK")):
            print 'test' + str(fileid) + ': OK';
            findresult = 1;
            break;
        if(line.startswith("FAILURES")):
            print 'test' + str(fileid) + ': FAIL';
            findresult = 1;
            break;
    if (findresult == 0):
        print 'test' + str(fileid) + ': unknown';

# hcai vim :set ts=4 tw=4 tws=4

