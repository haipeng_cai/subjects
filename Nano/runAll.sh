#!/bin/bash
source ./nano_global.sh

i=0

for algo in rand inc observed;
do
	cp sensa_${algo}.cfg sensa.cfg
	echo "Now using the $algo strategies for all tests ---"
	i=0
	for N in ${SEEDS[@]};
	do
		echo "Now Running instrumented s$N-orig at ${C[$i]} ..."
		sh run.sh ${C[$i]} $VERSION s$N-orig $algo

		let i=i+1
	done
done

echo "Running phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

