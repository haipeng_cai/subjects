#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

source ./nano_global.sh

ver=$1
seed=$2
change=$3

SOOTLIBS=$sootlibdir/jasminclasses-2.3.0.jar:$sootlibdir/java_cup-1.3.5.jar:$sootlibdir/jedd-runtime.jar:$sootlibdir/paddle-0.2.jar:$sootlibdir/polyglot-1.3.5.jar:$sootlibdir/sootclasses-2.3.0.jar

MAINCP=".:$rtlib:$SOOTLIBS:$duafdir/DUAForensics/bin:$duafdir/TestAdequacy/bin:$duafdir/Sensa/bin:$duafdir/ProbSli/bin"

SOOTCP=".:$rtlib:$jcelib:$duafdir/LocalsBox/bin/:$duafdir/InstrReporters/bin/:$duafdir/DUAForensics/bin/:$duafdir/ProbSli/bin/:$duafdir/Sensa/bin:$duafdir/TestAdequacy/bin/:$SOOTLIBS:$subjectloc/bin/$ver$seed:$subjectloc/bin/td-${ver}${seed}"


OUTDIR=$subjectloc/test-instrumented-$ver-$seed
mkdir -p $OUTDIR

mkdir -p out-test-instr

starttime=`date +%s%N | cut -b1-13`

#    -duaverbose \
#    instrumentAll $change \  #for test instrumentation
java -Xmx20480m -ea -cp ${MAINCP} InstrumentationTester \
    blocklistend \
    startIdOnly $change \
    -w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
    -p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
    -d $OUTDIR \
    -brinstr:off -duainstr:off \
    -slicectxinsens \
    -allowphantom \
    -paramdefuses \
    -keeprepbrs \
    -main-class $DRIVERCLASS \
    -entry:$DRIVERCLASS \
    -process-dir $subjectloc/bin/${ver}${seed} \
    -process-dir $subjectloc/bin/td-${ver}${seed} \
    1>out-test-instr/instr-${ver}${seed}.out 2>out-test-instr/instr-${ver}${seed}.err 

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds >> overhead.test-instrumentation.txt

echo "Running finished."
exit 0

# hcai vim :set ts=4 tw=4 tws=4

