#!/bin/bash

function copyTools()
{
scp -r \
	~/workspace/Deam \
	~/workspace/Sensa \
	~/workspace/InstrReporters \
	~/workspace/TestAdequacy \
	~/workspace/mcia \
	~/workspace/ProbSli \
	/home/hcai/workspace/
}

copyTools

exit 0
