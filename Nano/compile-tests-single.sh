#!/bin/bash
source ./nano_global.sh

MAINCP=".:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/workspace/Sensa/bin:$subjectloc/bin/${VERSION}s1:$subjectloc/src/td$VERSION"

mkdir -p bin/td$VERSION

cat ${VERSION}files-tests.lst | dos2unix | sed 's/\\/\//g' | while read fn;
do
	echo "compiling $fn ......"
	javac -g:source -source 1.4 -cp ${MAINCP} -d bin/td$VERSION $fn 1>/dev/null 2>&1
	if [ $? -ne 0 ];
	then
		echo "Failed to compile $fn, bail out."
		exit 1
	fi
done

if [ -d bin/td$VERSION/profile ];
then
	rm -rf bin/td$VERSION/profile
fi

echo "Compilation all done."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

