#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi
source ./nano_global.sh

change=$1
ver=$2
seed=$3

MAINCP=".:ROOT/tools/j2re1.4.2_18/lib/rt.jar:$ROOT/workspace/Soot-libs-2.3.0/polyglot-1.3.5.jar:$ROOT/workspace/Soot-libs-2.3.0/sootclasses-2.3.0.jar:$ROOT/workspace/Soot-libs-2.3.0/jasminclasses-2.3.0.jar:$ROOT/workspace/Soot-libs-2.3.0/java_cup-1.3.5.jar:$ROOT/workspace/DUAForensics:$ROOT/workspace/InstrReporters:$ROOT/workspace/LocalsBox:$ROOT/workspace/ProbSli/bin"

mkdir -p out-instr
                                                                                                                                               
SOOTCP=".:$ROOT/tools/j2re1.4.2_18/lib/rt.jar:$ROOT/workspace/DUAForensics:$ROOT/workspace/InstrReporters:$ROOT/workspace/LocalsBox:$ROOT/workspace/ProbSli/bin:$subjectloc/bin/${ver}${seed}:$subjectloc/bin/td-${ver}${seed}"

OUTDIR=$subjectloc/backsli-$ver$seed-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`

java -Xmx10000m -ea -cp ${MAINCP} sli.ProbBackSli \
	-w -cp $SOOTCP -p cg verbose:true,implicit-entry:false \
	-p cg.spark verbose:true,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-thinslice -wslice \
	-brinstr:off -duainstr:off \
   	-duaverbose \
	-slicectxinsens \
	-allowphantom \
	-start:$change \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	-process-dir $subjectloc/bin/td-${ver}${seed} \
	1>out-instr/instr-$change-${ver}${seed}.out 2>out-instr/instr-$change-${ver}${seed}.err

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for $change-${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

