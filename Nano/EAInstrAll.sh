#!/bin/bash
source ./nano_global.sh

for N in ${SEEDS[@]};
do
	echo "Now instrumenting s$N ..."
	sh EAInstr.sh ${VERSION} s$N

done

exit 0


# hcai vim :set ts=4 tw=4 tws=4

