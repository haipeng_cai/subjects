#!/bin/bash

VERSION=v1
SEEDS=(1 2 3 4 5 6 7)
ROOT=/home/hcai/
DRIVERCLASS=AllDriversMain
#DRIVERCLASS=JXML2SQLApp
subjectloc=$ROOT/SVNRepos/star-lab/trunk/Subjects/Nano/
inputxml_prefix=$subjectloc/inputs-lnx/
#C=(2504 2512 2686 2446 2909 2988 3093)
C=(2505 2513 2687 2447 2910 2989 3094) # after insert the __link invocations

JAVA=$ROOT/tools/jdk160/bin/java
