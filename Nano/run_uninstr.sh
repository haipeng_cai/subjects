#!/bin/bash
source ./nano_global.sh
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi
ver=$1
seed=$2

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$subjectloc/bin/td-$ver$seed:$subjectloc/bin/$ver$seed"

OUTDIR=Runout-${ver}${seed}-uninstr
mkdir -p $OUTDIR

function RunAllInOne()
{
	# to run all test in one
	java -Xmx1600m -ea -cp $MAINCP $DRIVERCLASS #1> $OUTDIR/1.out 2> $OUTDIR/1.err
}

function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	#cat $subjectloc/test_names.txt | dos2unix | \
	cat $subjectloc/inputs/testinputs.txt | dos2unix | \
	while read testname;
	do
		tag=`echo ${testname} | awk '{print $1}'`
		java -Xmx4000m -ea -cp $MAINCP $DRIVERCLASS $tag 
		return
		let i=i+1

		echo "Run Test #$i....."
		tag=`echo ${testname} | awk '{print $1}'`
		fn=`echo ${testname} | awk '{print $2}'`
		#echo "testname=$testname tag=$tag fn=$fn"
		if [ ! -s "$inputxml_prefix/$fn.xml" ];
		then
			echo target xml file "$inputxml_prefix/$fn.xml" is not found, test skipped out
			continue
		fi
		#echo java -Xmx4000m -ea -cp $MAINCP $DRIVERCLASS $tag "$inputxml_prefix/$fn.xml"
		java -Xmx4000m -ea -cp $MAINCP $DRIVERCLASS $tag "$inputxml_prefix/$fn.xml" \
			1> $OUTDIR/$i.out 2> $OUTDIR/$i.err
	done
}

starttime=`date +%s%N | cut -b1-13`
RunOneByOne
#RunAllInOne
stoptime=`date +%s%N | cut -b1-13`
echo "Normal RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0



# hcai vim :set ts=4 tw=4 tws=4

