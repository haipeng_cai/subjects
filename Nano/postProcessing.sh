#!/bin/bash

if [ ! -d origSensaRankings ];
then
	sh ./addStaticSlicesAll.sh
fi

sh ./produceIdealSensaRanking.sh
sh ./getIdealEffectivenessAll.sh
sh ./getIdealErrorsAll.sh
sh ./mergeEffectivenessStandalone.sh ./IdealCostEffectiveness mergedIdealCurve

sh ./getErrorsAll.sh
sh ./getEffectivenessAll.sh
sh ./mergeEffectivenessCurves.sh ./costEffectiveness mergedSensaCurve

echo "All done."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

