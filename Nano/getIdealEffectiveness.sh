#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

source ./nano_global.sh

change=$1
ver=$2
seed=$3

ARANKFILE=actualImpactRanking-${ver}${seed}-$change
SRANKFILE=IdealSensaRanking-${ver}${seed}-$change
ACCUMEFFECTFILE=accumulatedEffectiveness-${ver}${seed}-$change
AVGEFFECTFILE=averageCosts-${ver}${seed}-$change
CURVEFILE=curveEffectiveness-${ver}${seed}-$change

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/TestAdequacy/bin:$ROOT/tools/java_cup.jar"

COSTEFFECTDIR=`pwd`/IdealCostEffectiveness
mkdir -p $COSTEFFECTDIR

java -Xmx1600m -ea -cp $MAINCP \
	experiment.sensa.getCostOfEffectiveness \
	"$SRANKFILE" \
	"$ARANKFILE" \
	"$COSTEFFECTDIR/$ACCUMEFFECTFILE" \
	"$COSTEFFECTDIR/$AVGEFFECTFILE" \
	"$COSTEFFECTDIR/$CURVEFILE"

exit 0

# hcai vim :set ts=4 tw=4 tws=4

