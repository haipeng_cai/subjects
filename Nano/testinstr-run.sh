#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

source ./nano_global.sh

ver=$1
seed=$2

SOOTLIBS=$sootlibdir/jasminclasses-2.3.0.jar:$sootlibdir/java_cup-1.3.5.jar:$sootlibdir/jedd-runtime.jar:$sootlibdir/paddle-0.2.jar:$sootlibdir/polyglot-1.3.5.jar:$sootlibdir/sootclasses-2.3.0.jar

MAINCP=".:$rtlib:$SOOTLIBS:$duafdir/DUAForensics/bin:$duafdir/Sensa/bin:$duafdir/TestAdequacy/bin:$duafdir/ProbSli/bin:$duafdir/LocalsBox/bin:$duafdir/InstrReporters/bin:$subjectloc/test-instrumented-$ver-$seed/"

OUTDIR=Runout-${ver}${seed}-testinstr
mkdir -p $OUTDIR

function RunOneByOne()
{
    # to run a single test at a time
    local i=0
    
    cat inputs/testinputs.txt | dos2unix | while read test;
    do
	let i=i+1
	
	echo "Run Test #$i....."
	java -Xmx20480m -ea -cp $MAINCP \
		$DRIVERCLASS $test \
		1> $OUTDIR/$i.out \
		2> $OUTDIR/$i.err
    done
}

starttime=`date +%s%N | cut -b1-13`
RunOneByOne
#RunAllInOne
stoptime=`date +%s%N | cut -b1-13`
echo "Test instrumented subjects RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0

# hcai vim :set ts=4 tw=4 tws=4

