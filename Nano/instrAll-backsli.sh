#!/bin/bash
source ./nano_global.sh

i=0
for N in ${SEEDS[@]};
do
	echo "Now running PrioSlice s$N at ${C[$i]} ..."
	sh instr-backsli.sh ${C[$i]} ${VERSION} s$N

	let i=i+1
done

exit 0


# hcai vim :set ts=4 tw=4 tws=4

