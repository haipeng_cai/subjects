#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi
source ./nano_global.sh

change=$1
ver=$2
seed=$3

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin"

mkdir -p out-instr

SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/ProbSli/bin:$subjectloc/bin/${ver}${seed}:$subjectloc/bin/td-${ver}${seed}:$ROOT/workspace/Sensa/bin"

OUTDIR=$subjectloc/instrumented-$ver-$seed-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`

   	#-duaverbose \
java -Xmx1600m -ea -cp ${MAINCP} Sensa.SensaInst \
	-w -cp $SOOTCP -p cg verbose:false ,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
	-slicectxinsens \
	-allowphantom \
	-exechist \
	-start:$change \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	-process-dir $subjectloc/bin/td-${ver}${seed} \
	1>out-instr/instr-$change-${ver}${seed}.out 2>out-instr/instr-$change-${ver}${seed}.err

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for $change-${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

