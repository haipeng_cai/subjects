import argparse

parser = argparse.ArgumentParser(description='Postprocessing for instrumentation tester.')
parser.add_argument('-d','--dir', help='specify the input directory', required=True)
args = vars(parser.parse_args())

dir =  args['dir'];

for fileid in range(1,214):
    filedir = dir + '/' + str(fileid) + ".out";
    #print 'open: ' + filedir;
    file = open(filedir);
    
    i = 0;
    line1 = "";
    line2 = "";
    findresult = 0;
    errorflag = 0;
    for line in file:
        if(errorflag == 1):
            print line;
        if(line.startswith("ValueReporter:")):
            i = (i + 1)%2; 
            if(i == 1):
                line1 = line;
                errorflag = 0;
            else:
                line2 = line;
                if(line1 != line2):
                    print "error:\n" + line1 + "\n" + line2;
                    errorflag = 1;
                line1 = "";
                line2 = "";
        if(line.startswith("OK")):
            print 'test' + str(fileid) + ': OK';
            findresult = 1;
            continue;
        if(line.startswith("FAILURES")):
            print 'test' + str(fileid) + ': FAIL';
            findresult = 1;
            continue;

    if (findresult == 0):
        print 'test' + str(fileid) + ': unknown';


