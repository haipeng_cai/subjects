#!/bin/bash
source ../nano_global.sh
i=0
for N in ${SEEDS[@]};
do
	echo "Now calculating effectiveness for $VERSION s$N at ${C[$i]} ... "
	sh ./getIdealEffectiveness.sh ${C[$i]} $VERSION s$N

	let i=i+1
done

echo "Ranking phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

