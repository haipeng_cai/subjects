#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [changeloc]"
	exit 1
fi

ver=$1
seed=$2
change=${3:-"-2147483648"}

source ./nano_global.sh

suffix=${ver}${seed}
[ $change -ne -2147483648 ] && suffix=$change-${ver}${seed}

INDIR=$subjectloc/DeamInstrumented-$suffix
[ $change -ne -2147483648 ] && INDIR=$INDIR-$change

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/TestAdequacy/bin:$INDIR"

OUTDIR1=Deamoutdyn-${suffix}
[ $change -ne -2147483648 ] && OUTDIR1=$OUTDIR-$change
OUTDIR2=Deamoutdyn-${suffix}-orig
[ $change -ne -2147483648 ] && OUTDIR2=$OUTDIR-$change

starttime=`date +%s%N | cut -b1-13`

java -ea -Xmx1600m -cp ${MAINCP} deam.DeamAnalysis \
	$OUTDIR1 \
	$OUTDIR2 \
	$INDIR/stmtids.out \
	$change

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for $suffix elapsed: " `expr $stoptime - $starttime` milliseconds
exit 0

# hcai vim :set ts=4 tw=4 tws=4
