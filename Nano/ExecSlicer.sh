#!/bin/bash
source ./nano_global.sh
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [number of traces]"
	exit 1
fi

ver=$1
seed=$2
NT=${3:-"214"}

INDIR=$subjectloc/tracerOutdyn-${ver}${seed}

MAINCP=".:$ROOT/tools/j2re1.4.2_18/lib/rt.jar:$ROOT/workspace/Soot-libs-2.3.0/polyglot-1.3.5.jar:$ROOT/workspace/Soot-libs-2.3.0/sootclasses-2.3.0.jar:$ROOT/workspace/Soot-libs-2.3.0/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics:$ROOT/workspace/InstrReporters:$ROOT/workspace/LocalsBox:$ROOT/workspace/Dyndep/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$INDIR"


starttime=`date +%s%N | cut -b1-13`
	#"main" \
java -Xmx2800m -ea -cp ${MAINCP} trace.ExecSlicer \
	"$INDIR" \
	$NT \
	1>ExecSlicer/execSlicer-${ver}${seed}.out 2>ExecSlicer/execSlicer-${ver}${seed}.err

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

