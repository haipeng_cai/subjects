#!/bin/bash
source ./nano_global.sh
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

INDIR=$subjectloc/DiverInstrumented-$ver$seed

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$INDIR"

OUTDIR=Diveroutdyn-${ver}${seed}
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`

	#"-fullseq" 
java -Xmx1600m -ea -cp ${MAINCP} Diver.DiverRun \
	$DRIVERCLASS \
	"$subjectloc" \
	"$INDIR" \
	${ver}${seed} \
	$OUTDIR 

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed}-$4 elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

