#!/bin/bash
if [ $# -lt 1 ];then
	echo "Usage: $0 verDir"
	exit 1
fi
source ./nano_global.sh

verDir=$1

mkdir -p bin/${verDir}
MAINCP=".:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Tracer/bin:$subjectloc/src/$verDir"

LOGFILE=${verDir}-compile.out
#> $LOGFILE
cat ${verDir}files.lst | dos2unix | sed 's/\\/\//g' | while read fn;
do
	echo "compiling $fn ....."
	javac -g:source -source 1.4 -cp ${MAINCP} -d bin/$verDir $fn #1>/dev/null 2>&1
	#javac -cp ${MAINCP} -d bin/$verDir $fn 1>>$LOGFILE 2>&1
	if [ $? -ne 0 ];
	then
		echo "Failed to compile $fn, bail out."
		exit 1
	fi
done

if [ -d bin/${verDir}/profile ];
then
	rm -rf bin/${verDir}/profile
fi

echo "Compilation all done." 
exit 0


# hcai vim :set ts=4 tw=4 tws=4

