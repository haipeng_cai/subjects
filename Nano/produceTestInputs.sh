#!/bin/bash

source ./nano_global.sh

cat $subjectloc/test_names.txt | dos2unix | \
	awk '{printf("%s %s.xml\r\n", $1,"'$inputxml_prefix'/"$2)}' > inputs/testinputs.txt
exit 0


# hcai vim :set ts=4 tw=4 tws=4

