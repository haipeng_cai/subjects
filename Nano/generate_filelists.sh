#!/bin/bash
source ./nano_global.sh 
BASEDIR=`pwd`/src

for N in ${SEEDS[@]};
do
	> ${VERSION}s${N}files.lst
	> ${VERSION}s${N}-origfiles.lst
	find $BASEDIR/${VERSION}s$N -name "*.java" 1>${VERSION}s${N}files.lst
	find $BASEDIR/${VERSION}s$N-orig -name "*.java" 1>${VERSION}s${N}-origfiles.lst

	: '
	for subdir in jorphan core components functions protocol/ftp protocol/http protocol/jdbc protocol/java;
	do
		# we omit the PKCS12KeyStore.java code because of the lack of the IAIK package, which is not freely available
		find $BASEDIR/${VERSION}s$N/$subdir -name "*.java" | grep -v "PKCS12KeyStore" 1>>${VERSION}s${N}files.lst
		find $BASEDIR/${VERSION}s$N-orig/$subdir -name "*.java" | grep -v "PKCS12KeyStore" 1>>${VERSION}s${N}-origfiles.lst
	done
	'
done

find $BASEDIR/td${VERSION} -name "*.java" 1>${VERSION}files-tests.lst

exit 0

# hcai vim :set ts=4 tw=4 tws=4

