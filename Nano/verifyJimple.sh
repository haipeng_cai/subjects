#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [changeLoc]"
	exit 1
fi

ver=$1
seed=$2
change=${3:-"-2147483648"}

source ./nano_global.sh

MAINCP=".:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/Deam/bin:$ROOT/workspace/ProbSli/bin"

INDIR=$subjectloc/MutEHInstrumented-$ver-$seed
#INDIR=$subjectloc/MutEAInstrumented-$ver-$seed
#INDIR=$subjectloc/DeamInstrumented-$ver-$seed
[ $change -ne -2147483648 ] && OUTDIR=$INDIR-$change

#$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$INDIR:$ROOT/workspace/mcia/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/Deam/bin

suffix=${ver}${seed}
[ $change -ne -2147483648 ] && suffix=$change-${ver}${seed}

java -Xmx1600m -ea -cp ${MAINCP} soot.Main -f J -cp ${SOOTCP} \
	net.n3.nanoxml.StdXMLParser \
	-d `pwd`

exit 0


# hcai vim :set ts=4 tw=4 tws=4

