#!/bin/bash
if [ $# -lt 4 ];then
	echo "Usage: $0 version seed query change [number of tests]"
	exit 1
fi

ver=$1
seed=$2
query=${3:-"main"}
change=$4
NT=${5:-214}

source ./nano_global.sh

suffix=${ver}${seed}

INDIREA=$subjectloc/MutEAInstrumented-$suffix
INDIREH=$subjectloc/MutEHInstrumented-$suffix

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Sensa/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin"

suffix=${ver}${seed}

starttime=`date +%s%N | cut -b1-13`

java -Xmx1600m -ea -cp ${MAINCP} mut.MutRunAnalysis \
	"$subjectloc" \
	${ver}${seed} \
	$DRIVERCLASS \
	"$query" \
	$change \
	$INDIREA/traceLengths.txt \
	$INDIREH/stmtids.out \
	$NT \
	10 \
	1

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for $suffix elapsed: " `expr $stoptime - $starttime` milliseconds
exit 0

# hcai vim :set ts=4 tw=4 tws=4
