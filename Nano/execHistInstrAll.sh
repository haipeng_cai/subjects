#!/bin/bash
source ./nano_global.sh
i=0
for N in ${SEEDS[@]};
do
	echo "Now execHistInstrumenting s$N at ${C[$i]} ..."
	sh execHistInstr.sh ${C[$i]} $VERSION s$N 

	echo "Now execHistInstrumenting s$N-orig at ${C[$i]} ..."
	sh execHistInstr.sh ${C[$i]} $VERSION s$N-orig

	let i=i+1
done

exit 0


# hcai vim :set ts=4 tw=4 tws=4

