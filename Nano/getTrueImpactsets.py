#!/usr/bin/env python
import os
import sys
import string

# structure of the impact set table: {change:[impact set]}
g_impactSets= dict()

# data source locations
g_dirStmtDiff=os.getcwd()
g_dirStmtIds=os.getcwd()

# these are the real change locations to instrument at
g_VerPrefix="v1"
g_ChgLocs=(2505,2513,2687,2447,2910,2989,3094) # after insert the __link invocations
g_Seeds=[1,2,3,4,5,6,7]

DELIMITOR=')> - '

## @brief print usage
## @param none
## @retrun none
def Usage():
	print >> sys.stdout, "%s StmtDiffDir StmtIdsDir " % sys.argv[0]
	return

## @brief parse command line to get user's input for source file name and target
## @param none
## @retrun none
def ParseCommandLine():
	global g_dirStmtDiff
	global g_dirStmtIds
	argc = len(sys.argv)
	if argc >= 3:
		for i in range(1, argc):
			if not os.path.exists( sys.argv[i] ):
				raise IOError, "source dir given at [%s] does not exist, bailed out now." \
					% sys.argv[i]
		g_dirStmtDiff = sys.argv[1]
		g_dirStmtIds = sys.argv[2]
	else:
		Usage()
		raise Exception, "too few arguments, aborted."

''' example input cost file
costEffectiveness/averageCosts-v0s4-315-rand
../correctResultWslicing/costEffectiveness/averageCosts-v0s1-13
'''
def retrieveImpactsets():
	global g_impactSets
	global DELIMITOR
	for idx in range(0, min(len(g_ChgLocs),len(g_Seeds))):
		changeInId="%s-s%d-%d" % (g_VerPrefix,g_Seeds[idx],g_ChgLocs[idx])
		changeInDiff="%ss%d-%d" % (g_VerPrefix,g_Seeds[idx],g_ChgLocs[idx])
		print "Processing diff in %s w.r.t stmts in %s...... " % (changeInDiff, changeInId)

		'''
		Retrieve diffs
		'''
		fnIdDiff="%s/actualImpactRanking-%s" % (g_dirStmtDiff,changeInDiff)
		if not os.path.exists( fnIdDiff ):
			raise IOError, "source given at [%s] does not exist, bailed out now." % fnIdDiff
		fnIds="%s/execHistInstrumented-%s/stmtids.out" % (g_dirStmtIds,changeInId)
		if not os.path.exists( fnIds ):
			raise IOError, "source given at [%s] does not exist, bailed out now." % fnIds

		dfh = file(fnIdDiff,"r")
		if None == dfh:
			raise Exception, "Failed to open file named %s\n" % fnIdDiff
		AllDiffIds = set()
		curline=dfh.readline()
		while curline:
			segs = string.split(curline, " : ")
			assert len(segs)==2
			AllDiffIds.add ( abs(int(segs[0])) )
			curline=dfh.readline()
		dfh.close()

		'''
		Retrieve stmts 
		'''
		ifh = file(fnIds,"r")
		if None == ifh:
			raise Exception, "Failed to open file named %s\n" % fnIds
		AllFuncs = list()
		curline=ifh.readline()
		while curline:
			segs = string.split(curline, DELIMITOR)
			assert len(segs)>=2
			AllFuncs.append( segs[0]+")>" )
			curline=ifh.readline()
		ifh.close()


		'''
		Retrieve all functions corresponding to the diff stmts
		'''
		ImpactSet = set()
		for id in AllDiffIds:
			ImpactSet.add( AllFuncs[id] )

		g_impactSets[changeInId] = ImpactSet

		print "Done with %s [impacted: #statements= %d; #methods= %d." % \
				(changeInId, len(AllDiffIds), len(ImpactSet))

def dump():
	global g_impactSets
	for chg in g_impactSets.keys():
		''' dump the impact set '''
		ImpactSet = g_impactSets[chg]
		print >> sys.stdout, "\n\t\t ====== Impact set ground truth for %s [size = %d] ======\n" % \
				(chg, len(ImpactSet))
		for f in ImpactSet:
			print >> sys.stdout, f

######################################
# the boost
if __name__ == "__main__":
	try:
		ParseCommandLine()
	except Exception,e:
		print >> sys.stderr, e
		sys.exit(1)

	retrieveImpactsets()
	dump()

	sys.exit(0)

# hcai vim set ts=4 tw=100 sts=4 sw=4


