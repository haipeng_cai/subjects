#!/bin/bash
LOGDIR=`pwd`/complog

source ./nano_global.sh

for N in ${SEEDS[@]};
do
	echo "compiling ${VERSION}s$N......"
	sh compile.sh  ${VERSION}s$N 
	#1>$LOGDIR/${VERSION}s$N.log 2>&1 
	if [ $? -ne 0 ];then
		echo "compiling ${VERSION}s$N failed, aborted."
		exit 1
	fi

	echo "compiling ${VERSION}s$N-orig......"
	sh compile.sh  ${VERSION}s$N-orig 
	#1>$LOGDIR/${VERSION}s$N-org.log 2>&1
	if [ $? -ne 0 ];then
		echo "compiling ${VERSION}s$N-orig failed, aborted."
		exit 1
	fi

	echo "compiling td${VERSION} code dependent on ${VERSION}s$N ......"
	sh compile-tests.sh ${VERSION}s$N
	#1>$LOGDIR/test-${VERSION}s$N.log 2>&1 
	if [ $? -ne 0 ];then
		echo "compiling td${VERSION} on ${VERSION}s$N failed, aborted."
		exit 1
	fi

	echo "compiling td${VERSION} code dependent on ${VERSION}s$N-orig ......"
	sh compile-tests.sh ${VERSION}s$N-orig
	#1>$LOGDIR/test-${VERSION}s$N-orig.log 2>&1 
	if [ $? -ne 0 ];then
		echo "compiling td${VERSION} on ${VERSION}s$N-orig failed, aborted."
		exit 1
	fi
done


echo "All finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

