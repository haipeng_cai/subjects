#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./nano_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin"

#$ROOT/software/j2re1.4.2_18/lib/rt.jar
SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/Sensa/bin:$subjectloc/bin/${ver}${seed}:$subjectloc/bin/td-${ver}

suffix=${ver}${seed}

LOGDIR=out-MutEAInstr
mkdir -p $LOGDIR
logout=$LOGDIR/instr-$suffix.out
logerr=$LOGDIR/instr-$suffix.err

OUTDIR=$subjectloc/MutEAInstrumented-$suffix
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`

	#-duaverbose \
	#blacktypes "ru.novosoft.uml.foundation.data_types.MMultiplicity,java.lang.ref.WeakReference" \
java -Xmx6600m -ea -cp ${MAINCP} mut.MutInst \
	-w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
	-allowphantom \
	-wrapTryCatch \
	-dumpJimple \
	-instrEAS \
	-allApplicablePoints \
	-dumpFunctionList \
	-slicectxinsens \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	-process-dir $subjectloc/bin/td-${ver} \
	1> $logout 2> $logerr

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for $suffix elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

