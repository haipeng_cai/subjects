#!/bin/bash
source ./nano_global.sh

for algo in rand inc observed;
do
	echo "Now for the $algo strategy ---"
	i=0
	for N in ${SEEDS[@]};
	do
		echo -n "Now Ranking impact for $VERSION s$N at ${C[$i]} ... "
		sh ./sensaRankingForRawout.sh ${C[$i]} $VERSION s$N $algo 2>err.${VERSION}s$N-orig-${C[$i]}-$algo

		let i=i+1
	done
done

echo "Ranking phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

