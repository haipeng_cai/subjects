#!/bin/bash
if [ $# -lt 1 ];then
	echo "Usage: $0 verDir"
	exit 1
fi
source ./nano_global.sh

verDir=$1

if [ ! -d $subjectloc/bin/$verDir ];
then
	echo "bin/$verDir should be compiled first, bailed out."
	exit 1
fi

MAINCP=".:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Sensa/bin:$ROOT/workspace/Tracer/bin:$subjectloc/bin/$verDir:$subjectloc/src/td$VERSION"

mkdir -p bin/td-$verDir

cat ${VERSION}files-tests.lst | dos2unix | sed 's/\\/\//g' | while read fn;
do
	echo "compiling $fn ......"
	$ROOT/tools/jdk160/bin/javac -g:source -source 1.4 -cp ${MAINCP} -d bin/td-$verDir $fn #1>/dev/null 2>&1
	if [ $? -ne 0 ];
	then
		echo "Failed to compile $fn, bail out."
		exit 1
	fi
done

if [ -d bin/td-${verDir}/profile ];
then
	rm -rf bin/td-${verDir}/profile
fi

echo "Compilation all done."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

