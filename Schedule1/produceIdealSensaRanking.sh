#!/bin/bash
# these are the real change locations to instrument at
C=(13 291 318 315 322 13 327) # after correction

i=0
for N in 1 2 3 4 5 6 8;
do
	echo -n "Now processing v0 s$N at ${C[$i]} ... "
	cat actualImpactRanking-v0s$N-${C[$i]} > IdealSensaRanking-v0s$N-${C[$i]}
	sh ./addStaticSlices.sh actualImpactRanking-v0s$N-${C[$i]} \
		../instrumented-v0-s$N-orig-${C[$i]}/fwdslice.out \
		1>>IdealSensaRanking-v0s$N-${C[$i]}

	let i=i+1
	echo " done."
done

echo "got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

