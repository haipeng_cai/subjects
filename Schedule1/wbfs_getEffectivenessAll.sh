#!/bin/bash
# these are the real change locations to instrument at
C=(13 291 318 315 322 13 327) # after correction

for algo in rand inc observed;
do
	echo " -------- For the $algo Strategy ------- "
	i=0
	for N in 1 2 3 4 5 6 8;
	do
		echo "Now calculating effectiveness for v0 s$N at ${C[$i]} ... "
		sh ./getEffectiveness.sh ${C[$i]} v0 s$N $algo

		let i=i+1
	done
done

echo "Ranking phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

