#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 sensaRankingFile FwdSliceFile"
	exit 1
fi

fnSensaRank=$1
fnFwdslices=$2

source ./schedule_global.sh

cat $fnFwdslices | gawk 'BEGIN {
					# read the sensaRankingFile into an array
					FS=" : "
					srsz=0
					while ( getline < "'$fnSensaRank'" )
					{
						split($0, a)
						SensaRank[a[1]] = 1
						srsz++
						#SensaRank[srsz] = a[1]
					}
				} 

				{
					split($0, b)
					#if ( SensaRank[b[1]] == "" )
					if ( ! (b[1] in SensaRank) )
					{
						printf("%s : %d.0\n", b[1], srsz+1);
					}
				}'

exit 0

# hcai vim :set ts=4 tw=4 tws=4

