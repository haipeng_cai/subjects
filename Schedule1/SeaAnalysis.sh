#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./schedule_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

mkdir -p out-SEA
#$ROOT/software/j2re1.4.2_18/lib/rt.jar
SOOTCP=".:$ROOT/software/j2re1.4.2_18/lib/rt.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin":$subjectloc/lib:$subjectloc/fldAdptInstrumented-$ver$seed
#$subjectloc/bin/${ver}${seed}

OUTDIR=$subjectloc/SEAAnalyzed-$ver$seed
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`

	#-queryAll \
   	#-duaverbose \
java -Xmx1600m -ea -cp ${MAINCP} SEA.SeaAnalysis \
	-w -cp ${SOOTCP} \
	-p cg verbose:true,implicit-entry:false -p cg.spark verbose:true,on-fly-cg:true,rta:true \
	-f n -d "$OUTDIR" -brinstr:off -duainstr:off \
	-slicectxinsens \
	-allowphantom \
	-main-class ScheduleClass -entry:ScheduleClass \
	-process-dir $subjectloc/fldAdptInstrumented-$ver$seed \
	1>out-SEA/instr-${ver}${seed}.out 2>out-SEA/instr-${ver}${seed}.err
	#-process-dir $subjectloc/bin/${ver}${seed}  \
stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

