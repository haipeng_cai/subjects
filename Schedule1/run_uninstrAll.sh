#!/bin/bash

for N in 1 2 3 4 5 6 8;
do
	echo "Now Running Uninstrumented s$N-orig at ..."
	sh run_uninstr.sh v0 s$N-orig
done

echo "Running phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

