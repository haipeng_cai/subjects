#!/bin/bash
# these are the real change locations to instrument at
#C=(13 288 315 312 319 13 324)
C=(13 291 318 315 322 13 327) # after correction
i=0

for algo in rand inc observed;
do
	echo "Now for the $algo strategy ---"
	i=0
	for N in 1 2 3 4 5 6 8;
	do
		echo -n "Now Ranking impact for v0 s$N at ${C[$i]} ... "
		sh ./sensaRankingForRawout.sh ${C[$i]} v0 s$N $algo 2>err.v0s$N-orig-${C[$i]}-$algo

		let i=i+1
	done
done

echo "Ranking phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

