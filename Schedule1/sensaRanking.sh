#!/bin/bash
if [ $# -lt 4 ];then
	echo "Usage: $0 changeLoc version seed algo"
	exit 1
fi

change=$1
ver=$2
seed=$3
algo=$4
testtotal=${5:-2650}

source ./schedule_global.sh
INDIR=outdyn-${ver}${seed}-orig-$change
OUTFILE=predictedImpactRanking-${ver}${seed}-$change-$algo

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/TestAdequacy/bin:$ROOT/tools/java_cup.jar"

java -Xmx1600m -ea -cp $MAINCP -DLoadMap=1 \
	ExecHistProb \
	$testtotal \
	$change \
	"$INDIR/$change-$algo/test" \
	"$OUTFILE" \
	21

echo " done."
exit 0

# hcai vim :set ts=4 tw=4 tws=4

