#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./schedule_global.sh

mkdir -p bin/${ver}${seed}
javac -bootclasspath $ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar \
	-cp $subjectloc/lib/:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/tools/DUAForensics-bins-code/InstrReporters/:$ROOT/workspace/Sensa/bin/:$ROOT/workspace/Tracer/bin  \
	-g:source \
	-source 1.4 \
	-d bin/${ver}${seed} \
	src/${ver}${seed}/ScheduleClass.java

rm -rf bin/${ver}${seed}/profile
rm -rf bin/${ver}${seed}/change


# hcai vim :set ts=4 tw=4 tws=4

