#!/bin/bash

for N in 1 2 3 4 5 6 8;
do
	echo "Now Running instrumented s$N ..."
	sh DiverRun.sh v0 s$N 
done

echo "Running phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

