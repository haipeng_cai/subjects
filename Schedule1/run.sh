#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

source ./schedule_global.sh

INDIR=$subjectloc/instrumented-$ver-$seed-$change

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/Sensa/bin:$ROOT/workspace/ProbSli/bin:$ROOT/tools/java_cup.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/workspace/Sensa/bin:$subjectloc/lib:$ROOT/workspace/TestAdequacy/bin:$INDIR"


OUTDIR=$subjectloc/outdyn-${ver}${seed}-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
java -Xmx2800m -ea -cp ${MAINCP} -DOutputScope=0 -DnotRunWithNotHit=true Sensa.SensaRun \
	ScheduleClass \
	"$subjectloc" \
	"$INDIR" \
	$change \
	$OUTDIR

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for $change-${ver}${seed}-$4 elapsed: " `expr $stoptime - $starttime` milliseconds

if [ $# -ge 4 ];
then
	# save these for possible later trouble shooting
	algo=$4
	mkdir -p $OUTDIR/${algo}_ValuesUsed
	mv $subjectloc/valuetried $subjectloc/observed_value.out $OUTDIR/${algo}_ValuesUsed
fi

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

