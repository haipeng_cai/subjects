#!/bin/bash
# these are the real change locations to instrument at
C=(13 291 318 315 322 13 327) # after correction

BACKUPDIR=`pwd`/origSensaRankings
mkdir -p $BACKUPDIR
for algo in rand inc observed;
do
	echo " -------- For the $algo Strategy ------- "
	i=0
	for N in 1 2 3 4 5 6 8;
	do
		echo -n "Now processing v0 s$N at ${C[$i]} ... "
		#sh ./addStaticSlicesStrict.sh predictedImpactRanking-v0s$N-${C[$i]}-inc actualImpactRanking-v0s$N-${C[$i]} \
		mv predictedImpactRanking-v0s$N-${C[$i]}-$algo $BACKUPDIR/
		cat $BACKUPDIR/predictedImpactRanking-v0s$N-${C[$i]}-$algo 1>predictedImpactRanking-v0s$N-${C[$i]}-$algo
		sh ./addStaticSlices.sh $BACKUPDIR/predictedImpactRanking-v0s$N-${C[$i]}-$algo \
								../instrumented-v0-s$N-orig-${C[$i]}/fwdslice.out \
								1>>predictedImpactRanking-v0s$N-${C[$i]}-$algo

		let i=i+1
		echo " done."
	done
done

echo "got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

