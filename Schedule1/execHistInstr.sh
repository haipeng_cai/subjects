#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

source ./schedule_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin"


SOOTCP=".:$ROOT/software/j2re1.4.2_18/lib/rt.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/workspace/Sensa/bin:$subjectloc/bin/${ver}${seed}:$subjectloc/lib"

mkdir -p out-execHistInstr

OUTDIR=$subjectloc/execHistInstrumented-$ver-$seed-$change
mkdir -p $OUTDIR

java -Xmx1600m -ea -cp ${MAINCP} sli.ProbSli \
	-w -cp $SOOTCP -p cg verbose:true,implicit-entry:false \
	-p cg.spark verbose:true,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
   	-duaverbose -start:$change \
	-allowphantom \
	-nopslice \
	-slicectxinsens \
	-main-class ScheduleClass -entry:ScheduleClass \
	-process-dir $subjectloc/bin/${ver}${seed} \
	1>out-execHistInstr/execHistInstr-$change-${ver}${seed}.out 2>out-execHistInstr/execHistInstr-$change-${ver}${seed}.err

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

