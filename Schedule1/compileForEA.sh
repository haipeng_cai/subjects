#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./schedule_global.sh

mkdir -p bin/${ver}${seed}
javac -bootclasspath $ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar \
	-cp $subjectloc/lib/:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin/:$ROOT/workspace/Deam/bin:$ROOT/workspace/Dyndep/bin  \
	-g:source \
	-source 1.4 \
	-d bin/${ver}${seed} \
	src/${ver}${seed}/ScheduleClass.java


# hcai vim :set ts=4 tw=4 tws=4

