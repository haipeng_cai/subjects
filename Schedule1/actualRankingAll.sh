#!/bin/bash
# these are the real change locations to instrument at
#C=(13 288 315 312 319 13 324)
C=(13 291 318 315 322 13 327) # after correction
i=0

for N in 1 2 3 4 5 6 8;
do
	echo "Now Ranking actual impact for v0 s$N at ${C[$i]} ... "
	sh ./actualRanking.sh ${C[$i]} v0 s$N 2>acerr.v0s$N-${C[$i]}-mod

	let i=i+1
done

echo "Ranking phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

