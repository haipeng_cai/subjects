#!/bin/bash
# these are the real change locations to instrument at
C=(13 291 318 315 322 13 327) # after correction

RESDIR=`pwd`/errorMetrics
mkdir -p $RESDIR

echo " -------- For the $algo Strategy ------- "
i=0
for N in 1 2 3 4 5 6 8;
do
	echo -n "Now calculating errors for v0 s$N at ${C[$i]} ... "
	sh ./wslice_getErrors.sh ${C[$i]} v0 s$N > $RESDIR/Error-v0s$N-${C[$i]}

	let i=i+1
	echo " done."
done

echo "Ranking phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

