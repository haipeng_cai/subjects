#!/bin/bash

if [ $# -lt 1 ];
then
	echo "too few arguments."
	exit -1
fi

subject=$1

fnsea=${subject}SeaAccuracy.txt.sea
fnmdg=${subject}MdgAccuracy.txt

if [ ! -z $fnsea -a -z $fnmdg ];
then
	echo "source files missing."
	exit -1
fi	

cat $fnsea | awk '{if(NF==13) print $0}' | sort > /tmp/a
cat $fnmdg | awk '{if(NF==13) print $0}' | sort > /tmp/b
mz=`cat /tmp/a | wc -l`

cat /tmp/b | gawk 'BEGIN {
					# read file /tmp/b into an array
					FS="\t"
					sz=0
					while ( getline < "/tmp/a" )
					{
						n=split($0, s)
						if (n==12) {
							 #for (i=1;i<=n;i++) printf("s[%d]=%s ", i,s[i]);
							 #print "\n"
							sz++;
							blines[sz]=$0
						}
					else printf("%s --- size=%d\n", $0, n);
					}
					if ("'$mz'" != sz) {
						printf("size unmatched:%d!=%d\n!", sz, "'$mz'");
						exit
					}
					i=1
				}

				{
					split($0, m)
					split(blines[i],ss)
					# check = ss[1]==m[1] && ss[2]==m[2] && ss[3]==m[3] && ss[12]==m[12]
					check = ss[2]==m[2] && ss[11]==m[11]
					if (!check) {
						printf("%s\n%s\n\tunaliged.\n", blines[i], $0);
						exit
					}
					printf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n", m[2],ss[9],m[9],ss[10],m[10],m[11],ss[12],m[12]);
					i++
				}'


