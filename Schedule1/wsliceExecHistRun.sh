#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

source ./schedule_global.sh

INDIR=$subjectloc/wsliceExecHistInstrumented-$ver-$seed-orig-$change
MODINDIR=$subjectloc/wsliceExecHistInstrumented-$ver-$seed-$change

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/workspace/ProbSli/bin:$subjectloc/lib:$ROOT/workspace/Sensa/bin:$ROOT/workspace/TestAdequacy/bin:$INDIR:$MODINDIR"

OUTDIR=wsliceExechist_outdyn-${ver}${seed}-$change-orig
mkdir -p $OUTDIR

MODOUTDIR=wsliceExechist_outdyn-${ver}${seed}-$change-mod
mkdir -p $MODOUTDIR

java -Xmx1500m -ea -cp ${MAINCP} -DForActualImpact=true -DOutputScope=2 Sensa.SensaRun \
	ScheduleClass \
	"$subjectloc" \
	"$INDIR" \
	"$change" \
	"$OUTDIR" \
	"$MODINDIR" \
	"$MODOUTDIR"

exit 0


# hcai vim :set ts=4 tw=4 tws=4

