#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

source ./schedule_global.sh
SRANKFILE=wsliceImpactRanking-${ver}${seed}-$change
ARANKFILE=actualImpactRanking-${ver}${seed}-$change

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/TestAdequacy/bin:$ROOT/tools/java_cup.jar"

java -Xmx1600m -ea -cp $MAINCP \
	StmtProbAnalysis \
	`pwd` \
	"$SRANKFILE" \
	"$ARANKFILE" 

exit 0

# hcai vim :set ts=4 tw=4 tws=4

