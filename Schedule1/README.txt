// The following is the source code (it will decompress into ./src)
// The format is v0sN for the modified version N, and v0sN-orig for the unmodified version
// All v0sN-orig are the same, except for small differences intended to 'align' the stmtids.out
//   files to ensure that the STMT IDs given in the .txt files are the same in the original and modified
//   versions and ALSO for each one of us
src-sched1-ALIGNED.7z

// These will decompress into ./inputs and are needed to run the subject with run.bat without errors
inputs-sched1.7z

// NEXT: sample scripts to (1) compile the subject and its library, (2) analyze+instrument it,
//       (3) run the instrumented subject, and (4) analyze (post-process) the runtime information created by the instrumentation

// First, compile the library C_stdlib into ./bin/lib
// Then, use compile.bat to compile each v0sN and v0sN-orig (see compile-all.txt for the complete list)
compile-lib.bat
compile.bat
compile-all.txt

// This is to instrument. Replace sli.ProbSli with whatever is the analysis you need to use
// MAKE SURE the parameters stay the same as in this example script
// NOTE: instr-all.txt includes the STMT IDs for the location of each change in this subject
instr.bat
instr-all.txt

// This includes the STMT IDs for the location of each change
// NOTE: run-all.txt includes the STMT IDs for the location of each change in this subject
run.bat
run-all.txt

// This is for post-processing. Replace ExecHistoryDiff with whatever is the post-procesing analysis you need
// NOTE: adeq-all.txt includes the STMT IDs for the location of each change in this subject
adeq.bat
adeq-all.txt
