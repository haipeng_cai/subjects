#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 sensaRankingFile FwdSliceFile"
	exit 1
fi

fnSensaRank=$1
fnFwdslices=$2

fwdslicesz=`cat $fnFwdslices | wc -l`

cat $fnFwdslices | gawk 'BEGIN {
					# read the sensaRankingFile into an array
					FS=" : "
					srsz=0
					uniquesz=0
					while ( getline < "'$fnSensaRank'" )
					{
						split($0, a)
						if ( SensaRank[a[1]] == "" )
						{
							uniquesz++
						}
						SensaRank[a[1]] = 1
						srsz++
					}
				} 

				{
					split($0, b)
					if ( SensaRank[b[1]] == "" )
					{
						#printf("%s : %d.0\n", b[1], srsz+1);
						printf("%s : %f\n", b[1], uniquesz + ('$fwdslicesz' - uniquesz + 1)*0.5);
					}
				}'

exit 0

# hcai vim :set ts=4 tw=4 tws=4

