#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi
ver=$1
seed=$2

source ./schedule_global.sh

#INDIR="$subjectloc/DiverInstrumented-$ver-$seed:$ROOT/workspace/mcia/bin"
INDIR=$subjectloc/bin/$ver$seed

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/Sensa/bin:$ROOT/workspace/ProbSli/bin:$ROOT/tools/java_cup.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/workspace/Sensa/bin:$subjectloc/lib:$INDIR"

OUTDIR=Runout-${ver}${seed}-uninstr
mkdir -p $OUTDIR

function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	prefix="\/home\/hcai\/SVNRepos\/star-lab\/trunk\/Subjects\/Schedule1"
	sed s'/\\/\//g' $subjectloc/inputs/testinputs.txt > $subjectloc/inputs/testinputs.mod

	cat $subjectloc/inputs/testinputs.mod | dos2unix | \
	while read testname;
	do
		let i=i+1

		echo
		echo "Run Test #$i....."
		args=`echo $testname | sed s"/\.\./$prefix/"`
		echo $args
		java -Xmx4000m -ea -cp $MAINCP ScheduleClass $args \
		1> $OUTDIR/$i.out 2> $OUTDIR/$i.err
	done
}

starttime=`date +%s%N | cut -b1-13`
RunOneByOne
stoptime=`date +%s%N | cut -b1-13`
echo "Normal RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0


