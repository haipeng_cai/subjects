#!/bin/bash
source ./schedule_global.sh
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

INDIR=$subjectloc/tracerInstrumented-$ver$seed

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/Tracer/bin:$ROOT/tools/java_cup.jar:$subjectloc/lib:$INDIR"

starttime=`date +%s%N | cut -b1-13`

	#"-fullseq" 
java -Xmx9600m -ea -cp ${MAINCP} trace.Runner \
	"$subjectloc" \
	${ver}${seed} \
	$DRIVERCLASS \
	2650 \
	-caching \
	5

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds
cp $INDIR/{varIds.out,staticCDInfo.dat,staticDDInfo.dat} $subjectloc/tracerOutdyn-${ver}${seed}/

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

