#!/bin/bash

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:/afs/nd.edu/user36/hcai/tools/polyglot-1.3.5/lib/polyglot.jar:/afs/nd.edu/user36/hcai/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:/afs/nd.edu/user36/hcai/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/DUAForensics:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/LocalsBox:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/InstrReporters:/afs/nd.edu/user36/hcai/workspace/Sensa/bin:/afs/nd.edu/user36/hcai/tools/java_cup.jar:/afs/nd.edu/user36/hcai/workspace/ProbSli/bin"

SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/DUAForensics:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/LocalsBox:/afs/nd.edu/user36/hcai/tools/DUAForensics-bins-code/InstrReporters:/afs/nd.edu/user36/hcai/workspace/Sensa/bin:/afs/nd.edu/user36/hcai/workspace/ProbSli/bin:/afs/nd.edu/user36/hcai/workspace/SensaFeeder/bin:/afs/nd.edu/user36/hcai/workspace/Schedule/lib"

java -Xmx1500m -ea  -cp ${MAINCP} \
	Sensa.SensaInst b0 4 assStmt -w -cp ${SOOTCP} \
	-p cg verbose:true,implicit-entry:false -p cg.spark verbose:true,on-fly-cg:true,rta:true \
	-f c -d "/afs/nd.edu/user36/hcai/workspace/SensaFeeder/instrumented" -brinstr:off -duainstr:on \
   	-duaverbose -allowphantom -start:12 \
	-exechist \
	-main-class Dummy -entry:Dummy \
	-process-dir "/afs/nd.edu/user36/hcai/workspace/SensaFeeder/bin" 
	 #1>SensaInst.out 2>SensaInst.err 

