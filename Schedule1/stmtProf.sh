#!/bin/bash
source ./schedule_global.sh
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [number of traces]"
	exit 1
fi

ver=$1
seed=$2
NT=${3:-"2650"}

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Tracer/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Tracer/bin:$subjectloc/lib:$subjectloc/bin/${ver}${seed}

### static analysis

OUTDIR=$subjectloc/tracerInstrumented-$ver$seed-prof

function phase1() {
	mkdir -p $OUTDIR
	mkdir -p out-TracerInstr-prof

	starttime=`date +%s%N | cut -b1-13`
		#-debug \
		#-dumpJimple \
		#-wrapTryCatch \
		#-duaverbose \
		#-dumpFunctionList \
		#-debug \
	$JAVA -Xmx1600m -ea -cp ${MAINCP} profile.EventInstrumenter \
		-w -cp ${SOOTCP} \
		-p cg verbose:false,implicit-entry:false -p cg.spark verbose:false,on-fly-cg:true,rta:true \
		-f c -d "$OUTDIR" -brinstr:off -duainstr:off \
		-wrapTryCatch \
		-slicectxinsens \
		-stmtProf \
		-notracingval \
		-allowphantom \
		-staticCDInfo \
		-main-class ${DRIVERCLASS} -entry:${DRIVERCLASS} \
		-process-dir $subjectloc/bin/${ver}${seed}  \
		1>out-TracerInstr-prof/instr-${ver}${seed}.out 2>out-TracerInstr-prof/instr-${ver}${seed}.err
	stoptime=`date +%s%N | cut -b1-13`
	echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds
}

### runtime tracing
MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/Tracer/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$subjectloc/lib:$OUTDIR"

function phase2() {
	starttime=`date +%s%N | cut -b1-13`
		#"-fullseq" 
	java -Xmx1600m -ea -cp ${MAINCP} trace.Runner \
		"$subjectloc" \
		${ver}${seed}-prof \
		$DRIVERCLASS \
		$NT \
		-stmtProf \
		5

	stoptime=`date +%s%N | cut -b1-13`
	echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds
	#cp $OUTDIR/{varIds.out,staticCDInfo.dat,staticDDInfo.dat} $subjectloc/tracerOutdyn-${ver}${seed}-prof/
}

### trace analysis 

INDIR=$subjectloc/tracerOutdyn-${ver}${seed}-prof
function phase3() {
	starttime=`date +%s%N | cut -b1-13`
		#"main" \
	java -Xmx10g -ea -cp ${MAINCP} apps.StmtProf \
		"$INDIR" \
		$subjectloc/stmt.txt \
		$NT \
		#-debug

	stoptime=`date +%s%N | cut -b1-13`
	echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds
}

phase1
phase2
phase3

echo "All finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

