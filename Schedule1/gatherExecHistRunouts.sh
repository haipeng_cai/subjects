#!/bin/bash

if [ $# -lt 4 ];then
	echo "Usage: $0 changeLoc version seed destDir"
	exit 1
fi

change=$1
ver=$2
seed=$3
dstdir=$4

source ./schedule_global.sh
OUTDIR=wsliceExechist_outdyn-${ver}${seed}-$change-orig

i=1
while true;
do
	file=$subjectloc/$OUTDIR/$change-observed/test$i/run1.out
	if [ -e ${file} ];
	then
		cp ${file} $dstdir/$i.out
	else
		echo "$file is invalid or does not exist."
		break
	fi
	let i=i+1
done

exit 0


# hcai vim :set ts=4 tw=4 tws=4

