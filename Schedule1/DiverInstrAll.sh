#!/bin/bash
for N in 1 2 3 4 5 6 8;
do
	echo "Now instrumenting s$N ..."
	sh DiverInstr.sh v0 s$N
done

exit 0


# hcai vim :set ts=4 tw=4 tws=4

