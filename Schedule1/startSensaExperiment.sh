#!/bin/bash

# 1. instrumentation / static analysis
stime=`date +%s%N | cut -b1-13`
sh ./instrAll.sh 
etime=`date +%s%N | cut -b1-13`
echo "ALL STATIC ANALYSIS TIME: " `expr $etime - $stime` milliseconds

# 2. Run phase
stime=`date +%s%N | cut -b1-13`
sh ./runAll.sh 
etime=`date +%s%N | cut -b1-13`
echo "ALL RUN TIME: " `expr $etime - $stime` milliseconds

# 3. Ranking phase
stime=`date +%s%N | cut -b1-13`
sh ./sensaRankingForRawoutAll.sh 
etime=`date +%s%N | cut -b1-13`
echo "ALL RANKING TIME: " `expr $etime - $stime` milliseconds

wait
exit 0

# hcai vim :set ts=4 tw=4 tws=4

