#!/bin/bash

source ./xmlsec_global.sh
RevList=${1:-"$subjectloc/revisionList"}

while read N;
do
	echo "==================="
	echo "running r$N ......"
	echo "==================="
	sh run_uninstr.sh $N
done < ${RevList}

echo "All revisions are finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

