#!/bin/bash

source ./xmlsec_global.sh

for N in ${SEEDS};
do
	echo "==================="
	echo "running r$N ......"
	echo "==================="
	sh run_uninstr.sh $N
done

echo "All revisions are finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

