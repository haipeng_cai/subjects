#!/bin/bash

source ./xmlsec_global.sh
RevList=${1:-"$subjectloc/revisionList"}

while read N;
do
	#~/bin/cloc-1.60.pl $subjectloc/source_revisions/$N/src/org/apache/xml/security | grep Java
	~/bin/cloc-1.60.pl $subjectloc/source_revisions/$N | grep Java
done < ${RevList}

echo "All revisions are finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

