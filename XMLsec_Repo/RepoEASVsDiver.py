#!/usr/bin/env python
''' hcai: compare Diver with EAS for repository changes'''
import os
import sys
import string
import subprocess
import random
import time
import re

# executables
g_Root = os.getcwd()
g_RunBothBin = ""
g_nTest = 92
g_revListFn = ""

def Usage():
	print >> sys.stdout, "%s [root dir] [number of traces] " % sys.argv[0]
	return

def ParseCommandLine():
	global g_Root
	global g_RunBothBin
	global g_revListFn
	global g_nTest

	argc = len(sys.argv)
	if argc >= 2:
		g_Root = sys.argv[1]
		if argc >= 3:
			g_nTest = int(sys.argv[2])
	
	g_RunBothBin = g_Root + "/DiverRunAnalysis.sh"
	g_revListFn = g_Root + "/revisionList"

	'''check existence of binaries'''
	for binary in (g_RunBothBin,):
		if not os.path.exists( binary ):
			raise IOError, "required executable [%s] does not exist, bailed out now." % binary 
	print >> sys.stderr, "command line: %s %s %d" % (sys.argv[0], g_Root, g_nTest)

def getChangeList(fRev, nRev):
	global g_Root
	sfh = file( "%s/source_revisions/%s/EAInstrumented/functionList.out" % (g_Root, fRev), "r")
	if None == sfh:
		raise Exception, "Failed to open function list for revision %s\n" % fRev
	allSignatures = sfh.readlines()
	sfh.close()

	dfh = file ("%s/diffs/diff_%s-%s" % (g_Root, fRev, nRev), "r")
	if None == dfh:
		raise Exception, "Failed to open diff method list for revision %s-%s\n" % (fRev,nRev)
	allDiffMethods = dfh.readlines()
	dfh.close()

	retfn = "/tmp/diff_%s-%s" % (fRev, nRev)
	rfh = file (retfn, "w")
	if None == rfh:
		raise Exception, "Failed to create temporary file to save the finalized list of diff methods for revision %s-%s\n" % (fRev,nRev)

	for m in allDiffMethods:
		lf = 0
		mf = m
		for sig in allSignatures:
			l = len( lcs(m, sig) )
			if l > lf:
				lf = l
				mf = sig
		rfh.write(mf)
		allSignatures.remove(mf)

	rfh.flush()
	rfh.close()

	return retfn 

def lcs(a, b):
    lengths = [[0 for j in range(len(b)+1)] for i in range(len(a)+1)]
    # row 0 and column 0 are initialized to 0 already
    for i, x in enumerate(a):
        for j, y in enumerate(b):
            if x == y:
                lengths[i+1][j+1] = lengths[i][j] + 1
            else:
                lengths[i+1][j+1] = max(lengths[i+1][j], lengths[i][j+1])
    # read the substring out from the matrix
    result = ""
    x, y = len(a), len(b)
    while x != 0 and y != 0:
        if lengths[x][y] == lengths[x-1][y]:
            x -= 1
        elif lengths[x][y] == lengths[x][y-1]:
            y -= 1
        else:
            assert a[x-1] == b[y-1]
            result = a[x-1] + result
            x -= 1
            y -= 1
    return result

def collectData():
	global g_Root
	global g_RunBothBin
	global g_revListFn
	global g_nTest

	allRevs=[]
	rfh = file(g_revListFn, "r")
	if None == rfh:
		raise Exception, "Failed to open revision list named %s\n" % g_revListFn
	allRevs = rfh.readlines()
	rfh.close()

	for i in range(0, len(allRevs)-1, 1):
		fRev = allRevs[i].rstrip('\n')
		nRev = allRevs[i+1].rstrip('\n')
		print >> sys.stderr, "Finalizing changed method list between revisions %s and %s ..." % (fRev, nRev)
		#fnChg = getChangeList(fRev, nRev)
		fnChg = "%s/diffs/diff_%s-%s" % (g_Root, fRev, nRev)

		''' run DiverRunAnalysis on the revision $fRev for the changes between fRev and nRev'''
		print >> sys.stderr, "Running Diver against EAS for the changes between revisions %s and %s ..." % (fRev, nRev)
		try:
			cmd = [str(g_RunBothBin), fRev, fnChg, str(g_nTest)]
			#rcMutBoth = subprocess.check_call( cmd, stderr=subprocess.STDOUT )
			rcMutBoth = subprocess.check_call( cmd )
		except subprocess.CalledProcessError,e:
			print >> sys.stderr, "call %s failed, skipped [ret code=%d]." % \
				( str(cmd).strip(','), e.returncode )
			continue
		except Exception,e:
			print >> sys.stderr, "Unexpected error when calling %s, halt now with error=%s" % \
				( str(cmd).strip(','), e )
			sys.exit(-1)

		print >> sys.stderr,"Done ALL with revision %s vs %s." % (fRev, nRev)

######################################
# the boost
if __name__ == "__main__":
	try:
		ParseCommandLine()
	except Exception,e:
		print >> sys.stderr, e
		sys.exit(1)

	#time.sleep(28800)
	import time
	current_milli_time = lambda: int(round(time.time() * 1000))
	random.seed( current_milli_time() )

	collectData()

	sys.exit(0)

# set ts=4 tw=100 sts=4 sw=4

