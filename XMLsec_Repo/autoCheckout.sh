#!/bin/bash

if [ $# -lt 2 ];then
	echo "Usage: $0 start_rev #revs [repository url] [target directory]"
	exit 1
fi
source ./xmlsec_global.sh

SRev=$1
NRev=$2
url=${3:-"http://svn.apache.org/repos/asf/santuario/xml-security-java/trunk/"}
tgtdir=${4:-"$subjectloc/source_revisions"}

fnrev=`pwd`/revisionList
> ${fnrev} 
mkdir -p $tgtdir

starttime=`date +%s%N | cut -b1-13`

## look up for a revision before retrieving it
svn ls -r$SRev "$url" 1>/dev/null
if [ $? -ne 0 ];then
	echo "there is no revision $SRev found in $url, bailing out now."
	exit 1
fi

echo -n "checking out the starting revision $SRev ...... "
## check out the starting revision first
svn checkout -r$SRev "$url" $tgtdir/$SRev 1>/dev/null
if [ $? -ne 0 ];then
	echo "failed in checking out revision $SRev from $url, bailing out now."
	exit 1
fi
echo "$SRev" >> ${fnrev}
echo " Done."

## check out the next revisions with those having not contained source changes skipped
i=1
NextRev=$SRev
while ((i<=NRev))
do
	echo "==================================="
	echo "retrieving revision No. $i ......"
	echo "==================================="
	CurRev=$NextRev

	## find the next source-code--change revision
	ndiff=0
	while ((ndiff<1));
	do
		((NextRev=NextRev+1))
		## look up for a revision before retrieving it
		svn ls -r$NextRev "$url" 1>/dev/null
		if [ $? -ne 0 ];then
			echo "there is no revision $NextRev found in $url, giving up."
			echo "only $((i+0)) code-change revisions have been checked out and can be found at : $tgtdir"
			exit 1
		fi
		ndiff=`svn diff -r$CurRev:$NextRev "$url" | grep "Index:" | grep -a -c ".java"`
:<<'COMMENT'
		if [ $? -ne 0 ];then
			echo "failed in differencing revision $NextRev against $CurRev in repository $url, giving up now."
			exit 1
		fi
COMMENT
	done
	echo "one more pair of consecutive code-change revisions involving source change found: $CurRev -> $NextRev"

	## check out the source-code--change revision just found
	echo -n "checking out revision $NextRev ...... "
	svn checkout -r$NextRev "$url" $tgtdir/$NextRev 1>/dev/null
	if [ $? -ne 0 ];then
		echo "failed in checking out revision $NextRev from $url, stopping now."
		break
	fi
	echo "$NextRev" >> ${fnrev}
	echo " Done."

	((i++))
done

## done all check-outs
echo "all $((i+0)) code-change revisions have been checked out and can be found at : $tgtdir"

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0

# hcai vim :set ts=4 tw=4 tws=4

