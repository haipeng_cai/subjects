#!/bin/sh
if [ $# -lt 1 ];then
	echo "Usage: $0 Rev"
	exit 1
fi

Rev=$1

source ../../xmlsec_global.sh

mkdir -p bin

#$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:
MAINCP=".:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Sensa/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin:$subjectloc/libs/xercesImpl-fixed"

for i in $subjectloc/libs/*.jar;
do
	MAINCP=$MAINCP:$i
done

#~/tools/jdk160/bin/javac -g:source -Xlint:none -source 1.4 -target 1.4 -cp ${MAINCP} -d bin @compilefiles.lst  #1>err.compile 2>&1
javac -Xmaxwarns 0 -g:source -nowarn -Xlint:none -source 1.4 -cp ${MAINCP} -d bin @compilefiles.lst  #1>err.compile 2>&1

if [ -d ${Rev}/bin/profile ];
then
	rm -rf ${Rev}/bin/profile
fi

mkdir -p bin/org/apache/xml/security/resource
cp -fr $subjectloc/resource/v1/* bin/org/apache/xml/security/resource/
cp -fr src/org/apache/xml/security/resource/* bin/org/apache/xml/security/resource/

mkdir -p bin/org/apache/xml/security/test/resource
cp -fr $subjectloc/resource/test/* bin/org/apache/xml/security/test/resource/
cp -fr src_unitTests/org/apache/xml/security/test/resource/* bin/org/apache/xml/security/test/resource/

echo "Compilation for $Rev all done." 
exit 0

# hcai vim :set ts=4 tw=4 tws=4

