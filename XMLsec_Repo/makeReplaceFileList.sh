#!/bin/bash
if [ $# -lt 1 ];then
	echo "Usage: $0 Rev"
	exit 1
fi

Rev=$1

source ./xmlsec_global.sh

grep "System.currentTimeMillis()" $subjectloc/source_revisions/$Rev/src -r | grep -v svn | \
	awk '{print $1}' | sed s'/://g' | sort | uniq > $subjectloc/source_revisions/$Rev/replaceFileList


# hcai vim :set ts=4 tw=4 tws=4

