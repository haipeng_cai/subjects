#!/bin/bash
if [ $# -lt 1 ];then
	echo "Usage: $0 revision"
	exit 1
fi
Rev=$1

source ../../xmlsec_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar"

LOGDIR=$subjectloc/source_revisions/$Rev/out-DiverInstr
mkdir -p $LOGDIR
logout=$LOGDIR/instr.out
logerr=$LOGDIR/instr.err

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$subjectloc/libs/xercesImpl-fixed:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Deam/bin:$subjectloc/source_revisions/$Rev/bin

for i in $subjectloc/libs/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/source_revisions/$Rev/DiverInstrumented
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-debug \
	#-dumpJimple \
	#-wrapTryCatch \
	#-dumpFunctionList \
	#-reachability \
	#-debug \
   	#-duaverbose \
java -Xmx14g -ea -cp ${MAINCP} Diver.DiverInst \
	-w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
	-wrapTryCatch \
	-intraCD \
	-interCD \
	-ignoreRTECD \
	-exInterCD \
	-serializeVTG \
	-slicectxinsens \
	-allowphantom \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/source_revisions/$Rev/bin \
	1> $logout 2> $logerr

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for r$Rev elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Copying data to prepare for running the instrumented subject..."
mkdir -p $OUTDIR/org/apache/xml/security/resource
cp -fr $subjectloc/resource/v1/* $OUTDIR/org/apache/xml/security/resource/
cp -fr src/org/apache/xml/security/resource/* $OUTDIR/org/apache/xml/security/resource/

mkdir -p $OUTDIR/org/apache/xml/security/test/resource
cp -fr $subjectloc/resource/test/* $OUTDIR/org/apache/xml/security/test/resource/
cp -fr src_unitTests/org/apache/xml/security/test/resource/* $OUTDIR/org/apache/xml/security/test/resource/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

