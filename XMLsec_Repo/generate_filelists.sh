#!/bin/bash

source ./xmlsec_global.sh
BASEDIR=`pwd`/source_revisions
for N in $SEEDS;
do
	fnls=$BASEDIR/${N}/compilefiles.lst
	> $fnls

	for subdir in src/org/apache/xml/security;
	do
		find $BASEDIR/$N/$subdir -name "*.java"  1>> $fnls
	done

	find $BASEDIR/td/ -name "*.java" 1>> $fnls
done

exit 0

# hcai vim :set ts=4 tw=4 tws=4

