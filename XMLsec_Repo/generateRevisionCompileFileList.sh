#!/bin/bash

source ./xmlsec_global.sh
RevList=${1:-"$subjectloc/revisionList"}
BASEDIR=`pwd`/source_revisions

while read N;
do
	fnls=$BASEDIR/${N}/compilefiles.lst
	> $fnls

	for subdir in src/org/apache/xml/security;
	do
		find $BASEDIR/$N/$subdir -name "*.java"  1>> $fnls
	done

	find $BASEDIR/td/ -name "*.java" 1>> $fnls

	cp $BASEDIR/350604/src/org/apache/xml/security/c14n/implementations/CanonicalizerBase.java \
	   $BASEDIR/${N}/src/org/apache/xml/security/c14n/implementations/CanonicalizerBase.java

done < ${RevList}

exit 0

# hcai vim :set ts=4 tw=4 tws=4

