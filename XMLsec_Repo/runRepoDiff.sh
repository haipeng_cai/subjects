#!/bin/bash
source ./xmlsec_global.sh

#url=${1:-"http://svn.apache.org/repos/asf/santuario/xml-security-java/trunk/"}
url=${1:-"https://svn.apache.org/repos/asf/xml/security/trunk/"}
username=${2:-"null"}
password=${3:-"null"}
OUTDIR=${4:-"$subjectloc/source_revisions/"}
mkdir -p $OUTDIR

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/RepoMiner/bin"

for i in $ROOT/repolibs/*.jar;
do
	MAINCP=$MAINCP:$i
done

starttime=`date +%s%N | cut -b1-13`

	#-fnRevPairs $subjectloc/testPairs \
java -Xmx1600m -ea -cp ${MAINCP} RevisionDiff \
	-url ${url} \
	-name ${username} \
	-password ${password} \
	-repoPath ${OUTDIR} \
	-fnRevList $subjectloc/revisionList \
	-fnConverter $ROOT/repolibs/src2srcml \
	-outputDiff \
	1>res.repoDiff 2>log.repoDiff
	#-startRev 924515 \
	#-endRev 925352 \
	#-nRev 2 \
	#-endRev 924516 \
	#925352
	#1561561 \
	#1561562

stoptime=`date +%s%N | cut -b1-13`
echo "Time elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."
exit 0

