#!/bin/bash
scale=${1:-"default"}

source ./jython_global.sh

MAINCP="$subjectloc/build/exposed:$subjectloc/build/classes/:.:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar"

#for i in $subjectloc/extlibs/*.jar;
for i in $subjectloc/libs/*.jar;
do
	MAINCP=$MAINCP:$i
done

OUTDIR=Runout-uninstr
#mkdir -p $OUTDIR

args="$subjectloc/data/jython/sieve.py 50"
if [ ${scale} = "nop" ];
then
	args="$subjectloc/data/jython/noop.py"
else
	case ${scale} in
		"small")
			args="$subjectloc/data/jython/sieve.py 50"
			;;
		"default")
			args="$subjectloc/data/jython/pybench/pybench.py --with-gc --debug -n 1 -C 0 -w 20"
			;;
		"large")
			args="$subjectloc/data/jython/pybench/pybench.py --with-gc --debug -n 4 -C 0 -w 10"
			;;
		*)
			echo "unknown input scale; use nop instead."
			args="$subjectloc/data/jython/noop.py"
			;;
	esac
fi

function RunAllInOne()
{
	$JAVA -Xmx4000m -ea -cp $MAINCP \
		-Dpython.home="$subjectloc/data/jython" \
		-Dverbose="warning" \
		-Dcachedir="$subjectloc/cachedir" \
	  	$DRIVERCLASS ${args} #2>/dev/null
	#java -Xmx4000m -ea -cp $MAINCP -jar $subjectloc/dacapo-9.12-bach.jar $DRIVERCLASS "$args"
}

starttime=`date +%s%N | cut -b1-13`
RunAllInOne
stoptime=`date +%s%N | cut -b1-13`
echo "Normal RunTime elapsed: " `expr $stoptime - $starttime` milliseconds
exit 0

# hcai vim :set ts=4 tw=4 tws=4

