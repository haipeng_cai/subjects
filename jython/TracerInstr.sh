#!/bin/bash
source ./jython_global.sh

bm=jython

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Tracer/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$subjectloc/build/exposed:$subjectloc/build/classes"
#MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$subjectloc/build/classes:$subjectloc/lib/tracer.jar:$subjectloc/lib/mcia.jar:$subjectloc/lib/duaf.jar"

mkdir -p out-TracerInstr

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Tracer/bin:$subjectloc/build/exposed:$subjectloc/build/classes
#SOOTCP=.:/home/hcai/software/jdk160/jre/lib/rt.jar:/home/hcai/software/jdk160/jre/lib/jce.jar:$subjectloc/build/classes

for i in $subjectloc/libs/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/tracerInstrumented/
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-debug \
	#-dumpJimple \
	#-wrapTryCatch \
   	#-duaverbose \
	#-dumpFunctionList \
	#-notracingusedval \
	#-defblacktypes "org.apache.xpath.objects.XObject,org.apache.xpath.objects.XNodeSet,org.apache.jorphan.collections.Data" \
	#-useblacktypes "org.apache.xpath.objects.XObject,org.apache.xpath.objects.XNodeSet,org.apache.jorphan.collections.Data" \
	#-process-dir $subjectloc/build/exposed \
	#-debug \
	#-intraduas \
	#-localduas \
$JAVA -Xmx100g -ea -cp ${MAINCP} profile.EventInstrumenter \
	-w -cp ${SOOTCP} \
	-p cg verbose:false,implicit-entry:false -p cg.spark verbose:false,on-fly-cg:true,rta:true \
	-f c -d "$OUTDIR" -brinstr:off -duainstr:off \
	-wrapTryCatch \
	-dumpJimple \
	-notracingval \
	-slicectxinsens \
	-allowphantom \
	-staticCDInfo \
	-main-class ${DRIVERCLASS} -entry:${DRIVERCLASS} \
	-process-dir $subjectloc/build/classes  \
	1>out-TracerInstr/instr-${bm}.out 2>out-TracerInstr/instr-${bm}.err
stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for ${bm} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Copying data to prepare for running the instrumented subject..."

echo "Running finished."
exit 0

# hcai vim :set ts=4 tw=4 tws=4

