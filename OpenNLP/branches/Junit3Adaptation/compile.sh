#!/bin/sh
if [ $# -lt 1 ];then
	echo "Usage: $0 verDir"
	exit 1
fi

verDir=$1

source ./opennlp_global.sh

mkdir -p bin/${verDir}

#$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:
MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Sensa/bin"

for i in $subjectloc/libs/*.jar;
do
	MAINCP=$MAINCP:$i
done

#-source 5 -target 1.5 
javac -g:source -cp ${MAINCP} -d bin/$verDir @${verDir}files.lst  #1>err 2>&1

#cp $subjectloc/source_allseeds/$verDir/main/org/apache/tools/ant/taskdefs/defaults.properties bin/$verDir/org/apache/tools/ant/taskdefs/
#cp $subjectloc/source_allseeds/$verDir/main/org/apache/tools/ant/types/defaults.properties bin/$verDir/org/apache/tools/ant/types/

echo "Compilation all done." 
exit 0

# hcai vim :set ts=4 tw=4 tws=4
