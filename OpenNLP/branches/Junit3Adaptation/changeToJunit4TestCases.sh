#!/bin/bash

cat ./testClassSrc.txt | \
while read fn;
do
	_clsname=${fn##*/}
	clsname=${_clsname%.*}
	replace "import org.junit.Test;" "import junit.framework.TestCase;" \
		"@Test" "//@Test" "public class $clsname" "public class $clsname extends TestCase" -- $fn;
done
