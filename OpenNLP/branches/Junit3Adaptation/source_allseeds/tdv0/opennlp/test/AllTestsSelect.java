package opennlp.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Properties;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Comparator;

/*
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
*/
import junit.framework.*;

import profile.ExecHistReporter;
import change.DynSliceReporter;
import profile.BranchReporter;
import profile.DUAReporter;

public class AllTestsSelect
{
	/** Generic comparator for objects based on the result of toString. Null is the "minimum". */
	/*
	public static class StringBasedComparator implements Comparator {
		private StringBasedComparator() {}
		
		public int compare(Object o1, Object o2) {
			if (o1 == null)
				return (o2 == null)? 0 : -1; // o1 is equal to or less than o2
			if (o2 == null)
				return 1; // o1 is greater than o2
			return o1.toString().compareTo(o2.toString());
		}
	}
	*/

	static void __link() { 
		BranchReporter.__link(); DUAReporter.__link(); DynSliceReporter.__link(); ExecHistReporter.__link(); 
		/*
		EAS.Monitor.__link(); 
		mut.Modify.__link();
		Diver.EAMonitor.__link();
		*/
		Sensa.Modify.__link();
	}

	public static void main(String[] args) {
		TestSuite tsSorted = sortTestSuite((TestSuite) suite());
		TestSuite tsSelect = new TestSuite();
		for (Enumeration e = tsSorted.tests(); e.hasMoreElements(); ) {

			TestCase t = (TestCase) e.nextElement();

			// hcai: to dump the complete test case list
			// System.out.println((TestCase)t);

			// hcai: the following accommodates org.junit instead of junit4
			/*
			Test t = (Test) e.nextElement();
			// debug
			if (t instanceof TestCase) {
				System.out.println("[TEST]----" + (TestCase)t);
			}
			*/

			if (args.length == 0 || args[0].equals(t.toString())) 
				tsSelect.addTest(t);
		}
		if (tsSelect.testCount() == 0)
			throw new RuntimeException("ABORTING: NO TEST CASES TO EXECUTE");
		
		junit.textui.TestRunner.run(tsSelect);
	}

	public static Test suite() {
		// hcai: add test classes directly without relying on the system class loader in DoAllTests.suite();
		//		which is required by the mutation-based mcia study framework that has to run EAS and mDEA subjects 
		//		using app-level class-loader; this is also useful for other applications which need load classes
		//		using app-level class-loader too.
		// return DoAllTests.suite();

		TestSuite suite = new TestSuite("All OpenNLP-1.5.3 JUnit Tests");

		try
		{
			//suite.addTestSuite ( opennlp.uima.AnnotatorsInitializationTest.class.asSubclass(TestCase.class) );

			// hcai: the following accommodates org.junit instead of junit4
			/*
			suite.addTest( new JUnit4TestAdapter(opennlp.uima.AnnotatorsInitializationTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.perceptron.PerceptronPrepAttachTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.model.IndexHashTableTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.SpanTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.BeamSearchTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.ext.ExtensionLoaderTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.StringUtilTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.PlainTextByLineStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.featuregen.PreviousMapFeatureGeneratorTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.featuregen.StringPatternTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.featuregen.WindowFeatureGeneratorTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.featuregen.CachedFeatureGeneratorTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.featuregen.GeneratorFactoryTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.ListHeapTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.AbstractEventStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.eval.MeanTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.eval.CrossValidationPartitionerTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.eval.FMeasureTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.ParagraphStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.VersionTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.StringListTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.util.SequenceTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.namefind.NameSampleDataStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.namefind.NameFinderEventStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.namefind.DictionaryNameFinderEvaluatorTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.namefind.DictionaryNameFinderTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.namefind.NameSampleTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.namefind.TokenNameFinderCrossValidatorTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.namefind.RegexNameFinderTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.namefind.NameFinderMETest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.namefind.TokenNameFinderEvaluatorTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.chunker.ChunkerFactoryTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.chunker.ChunkSampleTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.chunker.ChunkSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.chunker.ChunkerEvaluatorTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.chunker.ChunkerMETest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.chunker.ChunkerDetailedFMeasureListenerTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.dictionary.DictionaryAsSetCaseInsensitiveTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.dictionary.DictionaryTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.dictionary.DictionaryAsSetCaseSensitiveTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.tokenize.TokenizerEvaluatorTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.tokenize.TokenizerMETest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.tokenize.WhitespaceTokenizerTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.tokenize.TokenizerModelTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.tokenize.TokenSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.tokenize.DictionaryDetokenizerTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.tokenize.TokenizerFactoryTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.tokenize.TokenSampleTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.tokenize.SimpleTokenizerTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.tokenize.TokSpanEventStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.tokenize.DetokenizationDictionaryTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.formats.Conll02NameSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.formats.frenchtreebank.ConstitParseSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.formats.LeipzigDoccatSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.formats.muc.DocumentSplitterStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.formats.muc.SgmlParserTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.formats.Conll03NameSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.formats.ConllXPOSSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.formats.NameFinderCensus90NameStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.formats.ad.ADParagraphStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.formats.ad.ADSentenceSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.formats.ad.ADNameSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.formats.ad.ADPOSSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.formats.ad.ADTokenSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.formats.ad.ADChunkSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.doccat.DocumentSampleTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.doccat.DocumentCategorizerMETest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.parser.ParseTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.parser.lang.en.HeadRulesTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.parser.ChunkSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.parser.ParseSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.parser.PosSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.parser.chunking.ParserTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.parser.treeinsert.ParserTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.sentdetect.SentenceDetectorFactoryTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.sentdetect.SentenceDetectorMETest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.sentdetect.DefaultEndOfSentenceScannerTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.sentdetect.SentenceSampleTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.sentdetect.SentenceDetectorEvaluatorTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.sentdetect.SDEventStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.stemmer.PorterStemmerTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.postag.POSSampleTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.postag.POSTaggerMETest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.postag.POSModelTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.postag.WordTagSampleStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.postag.POSTaggerFactoryTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.postag.POSDictionaryTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.postag.POSEvaluatorTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.postag.POSSampleEventStreamTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.cmdline.ArgumentParserTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.cmdline.TerminateToolExceptionTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.tools.cmdline.CLITest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.maxent.RealValueModelTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.maxent.ScaleDoesntMatterTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.maxent.quasinewton.QNTrainerTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.maxent.quasinewton.LogLikelihoodFunctionTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.maxent.quasinewton.LineSearchTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.maxent.MaxentPrepAttachTest.class) );
			suite.addTest( new JUnit4TestAdapter(opennlp.maxent.io.RealValueFileEventStreamTest.class) );
			*/

			// hcai: the following works only after the source code has been adapted to junit4 paradigm (namely each
			// test class must extend from the TestCase class

			suite.addTestSuite( opennlp.uima.AnnotatorsInitializationTest.class );
			suite.addTestSuite( opennlp.perceptron.PerceptronPrepAttachTest.class );
			suite.addTestSuite( opennlp.model.IndexHashTableTest.class );
			suite.addTestSuite( opennlp.tools.util.SpanTest.class );
			suite.addTestSuite( opennlp.tools.util.BeamSearchTest.class );
			suite.addTestSuite( opennlp.tools.util.ext.ExtensionLoaderTest.class );
			suite.addTestSuite( opennlp.tools.util.StringUtilTest.class );
			suite.addTestSuite( opennlp.tools.util.PlainTextByLineStreamTest.class );
			suite.addTestSuite( opennlp.tools.util.featuregen.PreviousMapFeatureGeneratorTest.class );
			suite.addTestSuite( opennlp.tools.util.featuregen.StringPatternTest.class );
			suite.addTestSuite( opennlp.tools.util.featuregen.WindowFeatureGeneratorTest.class );
			suite.addTestSuite( opennlp.tools.util.featuregen.CachedFeatureGeneratorTest.class );
			suite.addTestSuite( opennlp.tools.util.featuregen.GeneratorFactoryTest.class );
			suite.addTestSuite( opennlp.tools.util.ListHeapTest.class );
			suite.addTestSuite( opennlp.tools.util.AbstractEventStreamTest.class );
			suite.addTestSuite( opennlp.tools.util.eval.MeanTest.class );
			suite.addTestSuite( opennlp.tools.util.eval.CrossValidationPartitionerTest.class );
			suite.addTestSuite( opennlp.tools.util.eval.FMeasureTest.class );
			suite.addTestSuite( opennlp.tools.util.ParagraphStreamTest.class );
			suite.addTestSuite( opennlp.tools.util.VersionTest.class );
			suite.addTestSuite( opennlp.tools.util.StringListTest.class );
			suite.addTestSuite( opennlp.tools.util.SequenceTest.class );
			suite.addTestSuite( opennlp.tools.namefind.NameSampleDataStreamTest.class );
			suite.addTestSuite( opennlp.tools.namefind.NameFinderEventStreamTest.class );
			suite.addTestSuite( opennlp.tools.namefind.DictionaryNameFinderEvaluatorTest.class );
			suite.addTestSuite( opennlp.tools.namefind.DictionaryNameFinderTest.class );
			suite.addTestSuite( opennlp.tools.namefind.NameSampleTest.class );
			suite.addTestSuite( opennlp.tools.namefind.TokenNameFinderCrossValidatorTest.class );
			suite.addTestSuite( opennlp.tools.namefind.RegexNameFinderTest.class );
			suite.addTestSuite( opennlp.tools.namefind.NameFinderMETest.class );
			suite.addTestSuite( opennlp.tools.namefind.TokenNameFinderEvaluatorTest.class );
			suite.addTestSuite( opennlp.tools.chunker.ChunkerFactoryTest.class );
			suite.addTestSuite( opennlp.tools.chunker.ChunkSampleTest.class );
			suite.addTestSuite( opennlp.tools.chunker.ChunkSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.chunker.ChunkerEvaluatorTest.class );
			suite.addTestSuite( opennlp.tools.chunker.ChunkerMETest.class );
			suite.addTestSuite( opennlp.tools.chunker.ChunkerDetailedFMeasureListenerTest.class );
			suite.addTestSuite( opennlp.tools.dictionary.DictionaryAsSetCaseInsensitiveTest.class );
			suite.addTestSuite( opennlp.tools.dictionary.DictionaryTest.class );
			suite.addTestSuite( opennlp.tools.dictionary.DictionaryAsSetCaseSensitiveTest.class );
			suite.addTestSuite( opennlp.tools.tokenize.TokenizerEvaluatorTest.class );
			suite.addTestSuite( opennlp.tools.tokenize.TokenizerMETest.class );
			suite.addTestSuite( opennlp.tools.tokenize.WhitespaceTokenizerTest.class );
			suite.addTestSuite( opennlp.tools.tokenize.TokenizerModelTest.class );
			suite.addTestSuite( opennlp.tools.tokenize.TokenSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.tokenize.DictionaryDetokenizerTest.class );
			suite.addTestSuite( opennlp.tools.tokenize.TokenizerFactoryTest.class );
			suite.addTestSuite( opennlp.tools.tokenize.TokenSampleTest.class );
			suite.addTestSuite( opennlp.tools.tokenize.SimpleTokenizerTest.class );
			suite.addTestSuite( opennlp.tools.tokenize.TokSpanEventStreamTest.class );
			suite.addTestSuite( opennlp.tools.tokenize.DetokenizationDictionaryTest.class );
			suite.addTestSuite( opennlp.tools.formats.Conll02NameSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.formats.frenchtreebank.ConstitParseSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.formats.LeipzigDoccatSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.formats.muc.DocumentSplitterStreamTest.class );
			suite.addTestSuite( opennlp.tools.formats.muc.SgmlParserTest.class );
			suite.addTestSuite( opennlp.tools.formats.Conll03NameSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.formats.ConllXPOSSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.formats.NameFinderCensus90NameStreamTest.class );
			suite.addTestSuite( opennlp.tools.formats.ad.ADParagraphStreamTest.class );
			suite.addTestSuite( opennlp.tools.formats.ad.ADSentenceSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.formats.ad.ADNameSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.formats.ad.ADPOSSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.formats.ad.ADTokenSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.formats.ad.ADChunkSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.doccat.DocumentSampleTest.class );
			suite.addTestSuite( opennlp.tools.doccat.DocumentCategorizerMETest.class );
			suite.addTestSuite( opennlp.tools.parser.ParseTest.class );
			suite.addTestSuite( opennlp.tools.parser.lang.en.HeadRulesTest.class );
			suite.addTestSuite( opennlp.tools.parser.ChunkSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.parser.ParseSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.parser.PosSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.parser.chunking.ParserTest.class );
			suite.addTestSuite( opennlp.tools.parser.treeinsert.ParserTest.class );
			suite.addTestSuite( opennlp.tools.sentdetect.SentenceDetectorFactoryTest.class );
			suite.addTestSuite( opennlp.tools.sentdetect.SentenceDetectorMETest.class );
			suite.addTestSuite( opennlp.tools.sentdetect.DefaultEndOfSentenceScannerTest.class );
			suite.addTestSuite( opennlp.tools.sentdetect.SentenceSampleTest.class );
			suite.addTestSuite( opennlp.tools.sentdetect.SentenceDetectorEvaluatorTest.class );
			suite.addTestSuite( opennlp.tools.sentdetect.SDEventStreamTest.class );
			suite.addTestSuite( opennlp.tools.stemmer.PorterStemmerTest.class );
			suite.addTestSuite( opennlp.tools.postag.POSSampleTest.class );
			suite.addTestSuite( opennlp.tools.postag.POSTaggerMETest.class );
			suite.addTestSuite( opennlp.tools.postag.POSModelTest.class );
			suite.addTestSuite( opennlp.tools.postag.WordTagSampleStreamTest.class );
			suite.addTestSuite( opennlp.tools.postag.POSTaggerFactoryTest.class );
			suite.addTestSuite( opennlp.tools.postag.POSDictionaryTest.class );
			suite.addTestSuite( opennlp.tools.postag.POSEvaluatorTest.class );
			suite.addTestSuite( opennlp.tools.postag.POSSampleEventStreamTest.class );
			suite.addTestSuite( opennlp.tools.cmdline.ArgumentParserTest.class );
			suite.addTestSuite( opennlp.tools.cmdline.TerminateToolExceptionTest.class );
			suite.addTestSuite( opennlp.tools.cmdline.CLITest.class );
			suite.addTestSuite( opennlp.maxent.RealValueModelTest.class );
			suite.addTestSuite( opennlp.maxent.ScaleDoesntMatterTest.class );
			suite.addTestSuite( opennlp.maxent.quasinewton.QNTrainerTest.class );
			suite.addTestSuite( opennlp.maxent.quasinewton.LogLikelihoodFunctionTest.class );
			suite.addTestSuite( opennlp.maxent.quasinewton.LineSearchTest.class );
			suite.addTestSuite( opennlp.maxent.MaxentPrepAttachTest.class );
			suite.addTestSuite( opennlp.maxent.io.RealValueFileEventStreamTest.class );
		}
		catch (Exception ex)
		{
			System.err.println("error adding test :"+ex);
		}

		return suite;
	}

	// hcai: the following accommodates org.junit instead of junit4
	/*
	private static TestSuite sortTestSuite(TestSuite ts) {
		Set tests = new HashSet();
		getTestsRecursive(ts, tests);

		Collections.sort(new ArrayList(tests), StringBasedComparator.inst);

		TestSuite tsSorted = new TestSuite();
		for (Iterator it = tests.iterator(); it.hasNext();)
			tsSorted.addTest((Test) it.next());

		return tsSorted;
	}

	private static void getTestsRecursive(Test t, Set tests) {
		if (t instanceof TestCase)
			tests.add(t);
		else if (t instanceof TestSuite) {
			for (Enumeration e = ((TestSuite) t).tests(); e.hasMoreElements();)
				getTestsRecursive((Test) e.nextElement(), tests);
		}
		else if (t instanceof JUnit4TestAdapter) {
			for (Test e : ((JUnit4TestAdapter) t).getTests())
				tests.add(t);
		}
		else
			tests.add(t);
	}
	*/

	private static TestSuite sortTestSuite(TestSuite ts) {
		List tests = new ArrayList();
		getTestsRecursive(ts, tests);

		Collections.sort(tests, StringBasedComparator.inst);

		TestSuite tsSorted = new TestSuite();
		for (Iterator it = tests.iterator(); it.hasNext();)
			tsSorted.addTest((TestCase) it.next());

		return tsSorted;
	}

	private static void getTestsRecursive(Test t, List tests) {
		if (t instanceof TestCase)
			tests.add(t);
		else
			for (Enumeration e = ((TestSuite) t).tests(); e.hasMoreElements();)
				getTestsRecursive((Test) e.nextElement(), tests);
	}
}

/* hcai vim :set ts=4 tw=4 tws=4 */

