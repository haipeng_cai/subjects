
/**
 *  This is a test driver that collects all test cases from the given list of test classes that are written
 *  in the framework of Junit4 instead of Junit3.
 *  Without inputing any argument, this driver will run all test cases garnered as in a single holistic test suite, 
 *  otherwise, it will run only the single test case matching the given name provided as the input argument;
 *  
 *   This test driver is intended to serve a test driver template that supports test separation for unit tests written in the 
 *   paradigm of JUnit4
 *
 *@author     $Author: hcai $
 *@created    $Date: 2013/12/04 $
 *@version    $Revision: 1.0.0.0 $
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Properties;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Comparator;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import junit.framework.*;

public class AllTestsSelect
{
	/* the driver entry */
	public static void main(String[] args) {
		TestSuite tsSorted = sortTestSuite(createSuite());
		TestSuite tsSelect = new TestSuite();
		for (Enumeration e = tsSorted.tests(); e.hasMoreElements(); ) {
			Test t = (Test) e.nextElement();

			// hcai: to dump the complete test case list
			// System.out.println(t);

			if (args.length == 0 || args[0].equals(t.toString())) 
				tsSelect.addTest(t);
		}
		if (tsSelect.testCount() == 0)
			throw new RuntimeException("ABORTING: NO TEST CASES TO EXECUTE");
		
		junit.textui.TestRunner.run(tsSelect);
	}

	/* collect all test classes; for now the list of classes is filled manually, but instead an automatic search could replace this laborious manner */
	public static List<Class> createSuite() {

		List<Class> testClses = new ArrayList();

		try
		{
			testClses.add( opennlp.uima.AnnotatorsInitializationTest.class );
			testClses.add( opennlp.perceptron.PerceptronPrepAttachTest.class );
			testClses.add( opennlp.model.IndexHashTableTest.class );
			testClses.add( opennlp.tools.util.SpanTest.class );
			testClses.add( opennlp.tools.util.BeamSearchTest.class );
			testClses.add( opennlp.tools.util.ext.ExtensionLoaderTest.class );
			testClses.add( opennlp.tools.util.StringUtilTest.class );
			testClses.add( opennlp.tools.util.PlainTextByLineStreamTest.class );
			testClses.add( opennlp.tools.util.featuregen.PreviousMapFeatureGeneratorTest.class );
			testClses.add( opennlp.tools.util.featuregen.StringPatternTest.class );
			testClses.add( opennlp.tools.util.featuregen.WindowFeatureGeneratorTest.class );
			testClses.add( opennlp.tools.util.featuregen.CachedFeatureGeneratorTest.class );
			testClses.add( opennlp.tools.util.featuregen.GeneratorFactoryTest.class );
			testClses.add( opennlp.tools.util.ListHeapTest.class );
			testClses.add( opennlp.tools.util.AbstractEventStreamTest.class );
			testClses.add( opennlp.tools.util.eval.MeanTest.class );
			testClses.add( opennlp.tools.util.eval.CrossValidationPartitionerTest.class );
			testClses.add( opennlp.tools.util.eval.FMeasureTest.class );
			testClses.add( opennlp.tools.util.ParagraphStreamTest.class );
			testClses.add( opennlp.tools.util.VersionTest.class );
			testClses.add( opennlp.tools.util.StringListTest.class );
			testClses.add( opennlp.tools.util.SequenceTest.class );
			testClses.add( opennlp.tools.namefind.NameSampleDataStreamTest.class );
			testClses.add( opennlp.tools.namefind.NameFinderEventStreamTest.class );
			testClses.add( opennlp.tools.namefind.DictionaryNameFinderEvaluatorTest.class );
			testClses.add( opennlp.tools.namefind.DictionaryNameFinderTest.class );
			testClses.add( opennlp.tools.namefind.NameSampleTest.class );
			testClses.add( opennlp.tools.namefind.TokenNameFinderCrossValidatorTest.class );
			testClses.add( opennlp.tools.namefind.RegexNameFinderTest.class );
			testClses.add( opennlp.tools.namefind.NameFinderMETest.class );
			testClses.add( opennlp.tools.namefind.TokenNameFinderEvaluatorTest.class );
			testClses.add( opennlp.tools.chunker.ChunkerFactoryTest.class );
			testClses.add( opennlp.tools.chunker.ChunkSampleTest.class );
			testClses.add( opennlp.tools.chunker.ChunkSampleStreamTest.class );
			testClses.add( opennlp.tools.chunker.ChunkerEvaluatorTest.class );
			testClses.add( opennlp.tools.chunker.ChunkerMETest.class );
			testClses.add( opennlp.tools.chunker.ChunkerDetailedFMeasureListenerTest.class );
			testClses.add( opennlp.tools.dictionary.DictionaryAsSetCaseInsensitiveTest.class );
			testClses.add( opennlp.tools.dictionary.DictionaryTest.class );
			testClses.add( opennlp.tools.dictionary.DictionaryAsSetCaseSensitiveTest.class );
			testClses.add( opennlp.tools.tokenize.TokenizerEvaluatorTest.class );
			testClses.add( opennlp.tools.tokenize.TokenizerMETest.class );
			testClses.add( opennlp.tools.tokenize.WhitespaceTokenizerTest.class );
			testClses.add( opennlp.tools.tokenize.TokenizerModelTest.class );
			testClses.add( opennlp.tools.tokenize.TokenSampleStreamTest.class );
			testClses.add( opennlp.tools.tokenize.DictionaryDetokenizerTest.class );
			testClses.add( opennlp.tools.tokenize.TokenizerFactoryTest.class );
			testClses.add( opennlp.tools.tokenize.TokenSampleTest.class );
			testClses.add( opennlp.tools.tokenize.SimpleTokenizerTest.class );
			testClses.add( opennlp.tools.tokenize.TokSpanEventStreamTest.class );
			testClses.add( opennlp.tools.tokenize.DetokenizationDictionaryTest.class );
			testClses.add( opennlp.tools.formats.Conll02NameSampleStreamTest.class );
			testClses.add( opennlp.tools.formats.frenchtreebank.ConstitParseSampleStreamTest.class );
			testClses.add( opennlp.tools.formats.LeipzigDoccatSampleStreamTest.class );
			testClses.add( opennlp.tools.formats.muc.DocumentSplitterStreamTest.class );
			testClses.add( opennlp.tools.formats.muc.SgmlParserTest.class );
			testClses.add( opennlp.tools.formats.Conll03NameSampleStreamTest.class );
			testClses.add( opennlp.tools.formats.ConllXPOSSampleStreamTest.class );
			testClses.add( opennlp.tools.formats.NameFinderCensus90NameStreamTest.class );
			testClses.add( opennlp.tools.formats.ad.ADParagraphStreamTest.class );
			testClses.add( opennlp.tools.formats.ad.ADSentenceSampleStreamTest.class );
			testClses.add( opennlp.tools.formats.ad.ADNameSampleStreamTest.class );
			testClses.add( opennlp.tools.formats.ad.ADPOSSampleStreamTest.class );
			testClses.add( opennlp.tools.formats.ad.ADTokenSampleStreamTest.class );
			testClses.add( opennlp.tools.formats.ad.ADChunkSampleStreamTest.class );
			testClses.add( opennlp.tools.doccat.DocumentSampleTest.class );
			testClses.add( opennlp.tools.doccat.DocumentCategorizerMETest.class );
			testClses.add( opennlp.tools.parser.ParseTest.class );
			testClses.add( opennlp.tools.parser.lang.en.HeadRulesTest.class );
			testClses.add( opennlp.tools.parser.ChunkSampleStreamTest.class );
			testClses.add( opennlp.tools.parser.ParseSampleStreamTest.class );
			testClses.add( opennlp.tools.parser.PosSampleStreamTest.class );
			testClses.add( opennlp.tools.parser.chunking.ParserTest.class );
			testClses.add( opennlp.tools.parser.treeinsert.ParserTest.class );
			testClses.add( opennlp.tools.sentdetect.SentenceDetectorFactoryTest.class );
			testClses.add( opennlp.tools.sentdetect.SentenceDetectorMETest.class );
			testClses.add( opennlp.tools.sentdetect.DefaultEndOfSentenceScannerTest.class );
			testClses.add( opennlp.tools.sentdetect.SentenceSampleTest.class );
			testClses.add( opennlp.tools.sentdetect.SentenceDetectorEvaluatorTest.class );
			testClses.add( opennlp.tools.sentdetect.SDEventStreamTest.class );
			testClses.add( opennlp.tools.stemmer.PorterStemmerTest.class );
			testClses.add( opennlp.tools.postag.POSSampleTest.class );
			testClses.add( opennlp.tools.postag.POSTaggerMETest.class );
			testClses.add( opennlp.tools.postag.POSModelTest.class );
			testClses.add( opennlp.tools.postag.WordTagSampleStreamTest.class );
			testClses.add( opennlp.tools.postag.POSTaggerFactoryTest.class );
			testClses.add( opennlp.tools.postag.POSDictionaryTest.class );
			testClses.add( opennlp.tools.postag.POSEvaluatorTest.class );
			testClses.add( opennlp.tools.postag.POSSampleEventStreamTest.class );
			testClses.add( opennlp.tools.cmdline.ArgumentParserTest.class );
			testClses.add( opennlp.tools.cmdline.TerminateToolExceptionTest.class );
			testClses.add( opennlp.tools.cmdline.CLITest.class );
			testClses.add( opennlp.maxent.RealValueModelTest.class );
			testClses.add( opennlp.maxent.ScaleDoesntMatterTest.class );
			testClses.add( opennlp.maxent.quasinewton.QNTrainerTest.class );
			testClses.add( opennlp.maxent.quasinewton.LogLikelihoodFunctionTest.class );
			testClses.add( opennlp.maxent.quasinewton.LineSearchTest.class );
			testClses.add( opennlp.maxent.MaxentPrepAttachTest.class );
			testClses.add( opennlp.maxent.io.RealValueFileEventStreamTest.class );
		}
		catch (Exception ex)
		{
			System.err.println("error adding test :"+ex);
		}

		return testClses;
	}

	/* retrieve test cases from test classes following the standard convention (test prefix, for instance); and 
	 * then sort all test cases by their names
	 */ 
	private static TestSuite sortTestSuite(List<Class> testClses) {
		List<Test> allTests = new ArrayList<Test>();
		
		for (Class cls : testClses) {
			for (Method m : cls.getDeclaredMethods() ) {
				if (!isPublicTestMethod(m)) {
					// skip non-test methods
					continue;
				}
				// create a test case for each public (accessible from outside of the class then) test method
				String testName = m.getName()+"("+cls.getName()+")"; // cls.getName()+": " + m.getName();
				Test t = TestSuite.createTest(cls, testName);
				allTests.add(t);
			}
		}
		
		Collections.sort(allTests, StringBasedComparator.inst);

		TestSuite tsSorted = new TestSuite("OpenNLP 1.5.3 entire unit test suite");
		for (Iterator it = tests.iterator(); it.hasNext();) {
			tsSorted.addTest((Test) it.next());
		}

		return tsSorted;
	}
	
	private static boolean isPublicTestMethod(Method m) {
        return isTestMethod(m) && Modifier.isPublic(m.getModifiers());
    }

    private static boolean isTestMethod(Method m) {
        return m.getParameterTypes().length == 0 &&
                m.getName().startsWith("test") &&
                m.getReturnType().equals(Void.TYPE);
    }
}

/* hcai vim :set ts=4 tw=4 tws=4 */

