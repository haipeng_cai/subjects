#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./opennlp_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

mkdir -p out-EAInstr

SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$subjectloc/libs/xercesImpl-fixed:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$subjectloc/bin/${ver}${seed}"

for i in $subjectloc/libs/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/EAInstrumented-$ver$seed
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-debug \
	#-dumpJimple \
	#-wrapTryCatch \
	#-dumpFunctionList \
	#-reachability \
	#-debug \
	#-statUncaught \
	#-dumpJimple \
	#-wrapTryCatch \
	#-statUncaught \
   	#-duaverbose \
	#-src-prec java \
java -Xmx11600m -ea -cp ${MAINCP} EAS.EAInst \
	-w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-p jb preserve-source-annotations:true \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
	-wrapTryCatch \
	-dumpFunctionList \
	-statUncaught \
	-dumpJimple \
	-slicectxinsens \
	-allowphantom \
	-main-class $DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	-entry:$DRIVERCLASS \
	1>out-EAInstr/instr-${ver}${seed}.out 2>out-EAInstr/instr-${ver}${seed}.err
	#-process-dir $subjectloc/source_allseeds/${ver}${seed} \
	#-process-dir $subjectloc/source_allseeds/td${ver} \

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Copying data to prepare for running the instrumented subject..."

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

