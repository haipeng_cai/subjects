#!/bin/bash
source ./opennlp_global.sh
if [ $# -lt 1 ];then
	echo "Usage: $0 revision"
	exit 1
fi

rev=$1
NT=${2:-"265"}

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin"
for i in $subjectloc/libs/*.jar;
do
	MAINCP=$MAINCP:$i
done

starttime=`date +%s%N | cut -b1-13`

	#"-fullseq" 
	#-stmtcov
	#-nstmtcovdynalias \
	#-ninstanceprune \
	#-stmtcov \
	#-postprune \
	#-stmtcovdynalias \
	#-instanceprune \
java -Xmx14g -ea -cp ${MAINCP} Diver.RunAnalysis \
	"$subjectloc" \
	"$rev" \
	$DRIVERCLASS \
	"" \
	$NT \
	-dynalias \
	-ninstanceprune 

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${rev} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

