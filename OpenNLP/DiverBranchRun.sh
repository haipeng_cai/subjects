#!/bin/bash
if [ $# -lt 1 ];then
	echo "Usage: $0 revision"
	exit 1
fi

Rev=$1

source ./opennlp_global.sh
INDIR=$subjectloc/DiverBranchInstrumented-$Rev/

#$ROOT/tools/j2re1.4.2_18/lib/rt.jar
MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$INDIR:$ROOT/workspace/mcia/bin"

for i in $subjectloc/libs/*.jar;
do
	MAINCP=$MAINCP:$i
done

OUTDIR=$subjectloc/DiverBroutdyn-$Rev
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#"-fullseq" 
java -Xmx14g -ea -cp ${MAINCP} Diver.DiverRun \
	$DRIVERCLASS \
	"$subjectloc" \
	"$INDIR" \
	"$Rev" \
	$OUTDIR 

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for r$Rev elapsed: " `expr $stoptime - $starttime` milliseconds
cp $OUTDIR/*.out $subjectloc/Diveroutdyn-$Rev/

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

