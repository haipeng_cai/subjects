#!/bin/bash
if [ $# -lt 4 ];then
	echo "Usage: $0 singleMethodResult queryList groupsize reps"
	exit 1
fi

fnsmr=$1
fnquery=$2
grpSize=$3
reps=$4

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/TestAdequacy/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/mcia/bin"

starttime=`date +%s%N | cut -b1-13`
java -Xmx1600m -ea -cp $MAINCP \
	Diver.MultiMethodIA \
	$fnsmr \
	$fnquery \
	$grpSize \
	$reps

stoptime=`date +%s%N | cut -b1-13`
echo
echo "MMCIA AnalysisTime elapsed: " `expr $stoptime - $starttime` milliseconds
echo "Running finished."
exit 0

# hcai vim :set ts=4 tw=4 tws=4

