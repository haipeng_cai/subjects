#!/bin/bash
if [ $# -lt 1 ];then
	echo "Usage: $0 revision"
	exit 1
fi
Rev=$1

source ./opennlp_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

LOGDIR=$subjectloc/out-DynAliasInstr
mkdir -p $LOGDIR
logout=$LOGDIR/instr-$Rev.out
logerr=$LOGDIR/instr-$Rev.err

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/mcia/bin:$subjectloc/bin/$Rev

for i in $subjectloc/libs/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/DynAliasInstrumented-$Rev
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-allowphantom \
	#-debug \
	#-dumpJimple \
	#-wrapTryCatch \
	#-dumpFunctionList \
	#-statUncaught \
	#-intraCD \
	#-interCD \
	#-exInterCD \
	#-ignoreRTECD \
   	#-duaverbose \
	#-serializeVTG \
java -Xmx14g -ea -cp ${MAINCP} Diver.DynAliasInst \
	-w -cp ${SOOTCP} \
	-p cg verbose:true,implicit-entry:false -p cg.spark verbose:true,on-fly-cg:true,rta:true \
	-f c -d "$OUTDIR" -brinstr:off -duainstr:off \
	-allowphantom \
	-dumpJimple \
	-cachingOIDs \
	-slicectxinsens \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/$Rev \
	1> $logout 2> $logerr
stoptime=`date +%s%N | cut -b1-13`

echo "StaticAnalysisTime for r$Rev elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Copying data to prepare for running the instrumented subject..."

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

