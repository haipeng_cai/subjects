#!/bin/bash

source ./opennlp_global.sh
BASEDIR=source_allseeds
for N in $SEEDS;
do
	#find $BASEDIR/v2s$N -name "*.java" 1>v2s${N}files.lst
	#find $BASEDIR/v2s$N-orig -name "*.java" 1>v2s${N}-origfiles.lst

	#> $VERSIONs${N}files.lst
	> ${VERSION}s${N}-origfiles.lst
	#for subdir in opennlp-tools opennlp-uima;
	for subdir in '';
	do
		#find $BASEDIR/$VERSIONs$N/$subdir -name "*.java" 1>>$VERSIONs${N}files.lst
		find $BASEDIR/${VERSION}s$N-orig/$subdir -name "*.java" 1>>${VERSION}s${N}-origfiles.lst
	done
	find $BASEDIR/td${VERSION}/ -name "*.java" 1>>${VERSION}s${N}-origfiles.lst
done

exit 0

# hcai vim :set ts=4 tw=4 tws=4

