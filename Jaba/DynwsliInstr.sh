#!/bin/bash

if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

source ./jaba_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/DynWSlicing/bin"

SOOTCP=.:$ROOT/software/j2re1.4.2_18/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$subjectloc/bin/${ver}${seed}:$subjectloc/RegressionTestSuite/jaba/testdrivers/AllDriver/lib:$subjectloc/bin/$ver$seed:$ROOT/workspace/Sensa/bin:$ROOT/workspace/DynWSlicing/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin/

suffix=${ver}${seed}

LOGDIR=out-DynwsliInstr
mkdir -p $LOGDIR
logout=$LOGDIR/instr-$suffix.out
logerr=$LOGDIR/instr-$suffix.err

OUTDIR=$subjectloc/DynwsliInstrumented-$ver$seed-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`

	#-debug \
	#-allowphantom \
	#-eventLimit 2000 \
   	#-duaverbose \
java -Xmx8000m -ea -cp ${MAINCP} sli.DynWSlicing \
	-w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
   	-start:$change \
	-fdynslice \
	-slicectxinsens \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	1> $logout 2> $logerr

stoptime=`date +%s%N | cut -b1-13`
echo "Dynamic W-Slicing Instrumentation Time for $change-${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Instrumentation done, now copying resources required for running."

exit 0

# hcai vim :set ts=4 tw=4 tws=4

