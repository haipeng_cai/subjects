#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi
source ./jaba_global.sh

ver=$1
seed=$2

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

mkdir -p out-SEA

SOOTCP=.:$ROOT/software/j2re1.4.2_18/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$subjectloc/bin/${ver}${seed}:$subjectloc/RegressionTestSuite/jaba/testdrivers/AllDriver/lib

OUTDIR=$subjectloc/SEAAnalyzed-$ver$seed
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-debug \
	#-dumpJimple \
	#-wrapTryCatch \
	#-statUncaught \
	#-dumpFunctionList \
	#-statUncaught \
	#-ignoreRTECD \
	#-exInterCD \
	#-ignoreRTECD \
   	#-duaverbose \
	#-dumpFunctionList \
	#-queryOnICFG \
java -Xss4096m -Xmx42600m -ea -cp ${MAINCP} SEA.SeaAnalysis \
	-w -cp ${SOOTCP} \
	-p cg verbose:true,implicit-entry:false -p cg.spark verbose:true,on-fly-cg:true,rta:true \
	-f n -d "$OUTDIR" -brinstr:off -duainstr:off \
	-slicectxinsens \
	-allowphantom \
	-queryAll \
	-main-class ${DRIVERCLASS} -entry:${DRIVERCLASS} \
	-process-dir $subjectloc/bin/${ver}${seed}  \
	1>out-SEA/instr-${ver}${seed}.out 2>out-SEA/instr-${ver}${seed}.err
stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

