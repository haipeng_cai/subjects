#!/bin/bash
export base_dir=/home/jjones/research/fault_local_exp/jaba

for ((i=0; i <= 1000 ; i++))
do
echo $i
java -mx1000m -ea -cp ${base_dir}/jaba.orig/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/SymbolTableDriver/lib jaba.main.JABADriver SymbolTableDriver -l ${base_dir}/RegressionTestSuite/jaba/inputs/raja-v0.4.jrc > ${base_dir}/test/$i 2>&1
diff --brief test/$i test/0
done



