package jaba.graph.dom;

import jaba.sym.DemandDrivenAttribute;

import jaba.graph.Node;
import jaba.graph.GraphAttribute;
import jaba.graph.Graph;
import jaba.graph.NodeSortComparator;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Arrays;

/**
 * PostDominanceFrontiers - Encapsulates the dominance frontiers
 * of all the nodes in a cfg.
 * <br><br>
 * @author <a href="mailto:peterd@cc.gatech.edu">Peter Dillinger</a>
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/07/10 changed to implement PostDominanceFrontiers
 * @author Jay Lofstead 2003/07/17 removed legacy load ()
 */
public class PostDominanceFrontiersImpl implements GraphAttribute, DemandDrivenAttribute, FrontiersType, java.io.Serializable, PostDominanceFrontiers
{
	/** Map to store frontiers. */
	private Map frontiers;

	/** Map to store frontiers for control-dependence calculation.
	 *  Unlike frontiers, which contains vectors of nodes,
	 *  cdFrontiers contains vectors of edges; this enables the CDG to
	 *  have labels on the edges
	 */
	private Map cdFrontiers;

	/** 
	 * Constructor taking in a Map of the frontiers.
	 * <br><br>
	 * The Map should map Nodes to Node[]s.
	 *  <br><br>
	 * @param frontiers the <code>Map</code>
	 */
	PostDominanceFrontiersImpl (Map frontiers, Map cdFrontiers)
	{
		this.frontiers = frontiers;
		this.cdFrontiers = cdFrontiers;
	}

	//================================================================//
	//                       A C C E S S O R S                        //
	//================================================================//

	/**
	 * Returns a map of the frontiers represented in this object.
	 * <br><br>
	 * The <code>Map</code> maps <code>Node</code>s to <code>Node[]</code>s.
	 * <br><br>
	 * @return map of the frontiers
	 */
	public Map getFrontiersMap()
	{
		int hashSize = frontiers.size() > 5 ? frontiers.size() * 2 - 1 : 11;
		Map toRet = new HashMap(hashSize);
		Iterator keys = frontiers.keySet().iterator();
		while (keys.hasNext()) {
			Node node = (Node) keys.next();
			toRet.put(node, getFrontierOf(node));
		}
		return toRet;
	}

	/**
	 * Returns a map of the cd frontiers represented in this object.
	 * The cd frontiers map a node to the edge, such that the sink of
	 * the edge belongs to the postdominance frontier of the node.
	 * The cd frontiers are used for computing control dependences
	 * so that control-dependence edges in the CDG can have labels
	 * associated with them. By using regular frontiers information,
	 * that only stores the nodes in the frontiers, it is not possible
	 * to associate labels with the edges in the CDG.
	 * <br><br>
	 * The <code>Map</code> maps <code>Node</code>s to <code>Edge[]</code>s.
	 * <br><br>
	 * @return map of the frontiers
	 */
	public Map getCDFrontiersMap()
	{
		int hashSize = cdFrontiers.size() > 5 ? cdFrontiers.size() * 2 - 1 : 11;
		Map toRet = new HashMap(hashSize);
		Iterator keys = cdFrontiers.keySet().iterator();
		while (keys.hasNext()) {
			Node node = (Node) keys.next();
			toRet.put(node, cdFrontiers.get(node));
		}
		return toRet;
	}

	/**
	 * Gets all nodes in the postdominance frontier of the specified node.
	 * <br><br>
	 * @param node node whose postdominance frontier to find
	 * @return all nodes in the postdominance frontier of <code>node</code>
	 */
	public Node[] getFrontierOf(Node node)
	{
		return (Node[]) ((Node[]) frontiers.get(node)).clone();
	}

	/**
	 * Returns true iff <code>a</code> is in the postdominance frontier of
	 * <code>b</code>.
	 * <br><br>
	 * @param a node to check in frontier of <code>b</code>
	 * @param b node whose frontier to check for <code>a</code>
	 * @return true iff <code>a</code> is in the dominance frontier of
	 *         <code>b</code>
	 */
	public boolean isInFrontierOf(Node a, Node b)
	{
		Node[] frontierNodes = (Node[]) frontiers.get(b);
		for (int i = 0; i < frontierNodes.length; i++) {
			if (frontierNodes[i] == a) {
				return true;
			}
		}
		// otherwise if not found
		return false;
	}
	
	/** returns an XML format of the object */
	public String toString ()
	{
		StringBuffer toRet = new StringBuffer (1000);
		NodeSortComparator nsc = new NodeSortComparator ();

		toRet.append ("<postdominancefrontiers>");

		Node [] nodes = (Node []) frontiers.keySet ().toArray (new Node [0]);
		Arrays.sort (nodes, nsc);

		for (int n = 0; n < nodes.length; n++)
		{
			toRet.append ("<frontiernode>");

			toRet.append ("<node>" + nodes [n].getNodeNumber () + "</node>");

			toRet.append ("<frontier>");
			Node [] frontier = (Node []) frontiers.get (nodes [n]);
			Arrays.sort (frontier, nsc);
			for (int i = 0; i < frontier.length; i++)
			{
				toRet.append ("<node>" + frontier [i].getNodeNumber () + "</node>");
			}
			toRet.append ("</frontier>");

			toRet.append ("</frontiernode>");
		}

		toRet.append ("</postdominancefrontiers>");

		return toRet.toString();
	}
	
	//================================================================//
	//                   F A C T O R Y   S T U F F                    //
	//================================================================//

	/**
	 * Factory method for <code>PostDominanceFrontiers</code>s.
	 * <br><br>
	 * @param cfg the <code>Graph</code> representing the cfg this attribute
	 *            should describe
	 * @return a new <code>PostDominanceFrontiers</code> associated with the given cfg.
	 */
	public static GraphAttribute load(Graph cfg)
	{
		// calculate the dominator info
		DominanceFrontiersCalculation dfCalc =
					new DominanceFrontiersCalculation(cfg, true);

		// and give it to a new instance
		PostDominanceFrontiers newAttribute =
		    new PostDominanceFrontiersImpl (dfCalc.getFrontiers(),
					       dfCalc.getCDFrontiers());

		// store the attribute
		cfg.addAttribute(newAttribute);

		return newAttribute;
	}

}
