package jaba.graph.dom;

import jaba.sym.DemandDrivenAttribute;

import jaba.graph.Graph;
import jaba.graph.GraphAttribute;
import jaba.graph.Node;
import jaba.graph.NodeSortComparator;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Arrays;

/**
 * DominanceFrontiers - Encapsulates the dominance frontiers
 * of all the nodes in a cfg.
 *
 * @author <a href="mailto:peterd@cc.gatech.edu">Peter Dillinger</a>
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc.
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/07/10 changed to implement DominanceFrontiers
 * @author Jay Lofstead 2003/07/17 removed legacy load ()
 */
public class DominanceFrontiersImpl implements GraphAttribute, DemandDrivenAttribute, FrontiersType, java.io.Serializable, DominanceFrontiers
{
	/** Map to store frontiers. */
	private Map frontiers;
	
	/** 
	 * Constructor taking in a Map of the frontiers.
	 * <br><br>
	 * The Map should map Nodes to Node[]s.
	 * <br><br>
	 * @param frontiers the <code>Map</code>
	 */
	DominanceFrontiersImpl (Map frontiers)
	{
		this.frontiers = frontiers;
	}

	//================================================================//
	//                       A C C E S S O R S                        //
	//================================================================//

	/**
	 * Returns a map of the frontiers represented in this object.
	 * <br><br>
	 * The <code>Map</code> maps <code>Node</code>s to <code>Node[]</code>s.
	 * <br><br>
	 * @return map of the frontiers
	 */
	public Map getFrontiersMap()
	{
		int hashSize = frontiers.size() > 5 ? frontiers.size() * 2 - 1 : 11;
		Map toRet = new HashMap(hashSize);
		Iterator keys = frontiers.keySet().iterator();
		while (keys.hasNext()) {
			Node node = (Node) keys.next();
			toRet.put(node, getFrontierOf(node));
		}
		return toRet;
	}

	/**
	 * Gets all nodes in the dominance frontier of the specified node.
	 * <br><br>
	 * @param node node whose dominance frontier to find
	 * @return all nodes in the dominance frontier of <code>node</code>
	 */
	public Node[] getFrontierOf(Node node)
	{
		return (Node[]) ((Node[]) frontiers.get(node)).clone();
	}

	/**
	 * Returns true iff <code>a</code> is in the dominance frontier of
	 * <code>b</code>.
	 * <br><br>
	 * @param a node to check in frontier of <code>b</code>
	 * @param b node whose frontier to check for <code>a</code>
	 * @return true iff <code>a</code> is in the dominance frontier of
	 *         <code>b</code>
	 */
	public boolean isInFrontierOf(Node a, Node b)
	{
		Node[] frontierNodes = (Node[]) frontiers.get(b);
		for (int i = 0; i < frontierNodes.length; i++) {
			if (frontierNodes[i] == a) {
				return true;
			}
		}
		// otherwise if not found
		return false;
	}
	

	/** converts to an XML format */
	public String toString()
	{
		StringBuffer toRet = new StringBuffer ();
		NodeSortComparator nsc = new NodeSortComparator ();

		toRet.append ("<dominancefrontiers>");
		Node [] nodes = (Node []) frontiers.keySet ().toArray (new Node [0]);
		Arrays.sort (nodes, nsc);

		for (int n = 0; n < nodes.length; n++)
		{
			toRet.append ("<frontiernode>");

			toRet.append ("<node>" + nodes [n].getNodeNumber () + "</node>");

			toRet.append ("<frontier>");
			Node [] frontier = (Node []) frontiers.get (nodes [n]);
			Arrays.sort (frontier, nsc);
			for (int i = 0; i < frontier.length; i++)
			{
				toRet.append ("<node>" + frontier [i].getNodeNumber () + "</node>");
			}
			toRet.append ("</frontier>");

			toRet.append ("</frontiernode>");
		}

		toRet.append ("</dominancefrontiers>");

		return toRet.toString();
	}
	
	//================================================================//
	//                   F A C T O R Y   S T U F F                    //
	//================================================================//

	/**
	 * Factory method for <code>DominanceFrontiers</code>s.
	 * <br><br>
	 * @param cfg the <code>Graph</code> representing the cfg this attribute
	 *            should describe
	 * @return a new <code>DominanceFrontiers</code> associated with the given cfg.
	 */
	public static GraphAttribute load(Graph cfg)
	{
		// calculate the dominator info
		DominanceFrontiersCalculation dfCalc =
					new DominanceFrontiersCalculation(cfg, false);

		// and give it to a new instance
		DominanceFrontiers newAttribute =
				new DominanceFrontiersImpl (dfCalc.getFrontiers());

		// store the attribute
		cfg.addAttribute(newAttribute);

		return newAttribute;
	}
}
