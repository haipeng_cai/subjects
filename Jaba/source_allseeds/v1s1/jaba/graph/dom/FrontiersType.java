package jaba.graph.dom;

import jaba.graph.Node;

import java.util.Map;

/**
 * FrontiersType
 *
 * @author <a href="mailto:peterd@cc.gatech.edu">Peter Dillinger</a>
 * @author Jay Lofstead 2003/06/05 fixed some of the doc
 */
public interface FrontiersType
{
	/**
	 * Returns a map of the frontiers represented in this object.
	 * <br><br>
	 * The <code>Map</code> maps <code>Node</code>s to <code>Node[]</code>s.
	 *
	 * @return map of the frontiers
	 */
	public Map getFrontiersMap();

	/**
	 * Gets all nodes in the dominance frontier of the specified node.
	 * <br><br>
	 * @param node node whose dominance frontier to find
	 * @return all nodes in the dominance frontier of <code>node</code>
	 */
	public Node[] getFrontierOf(Node node);

	/**
	 * Returns true iff <code>a</code> is in the dominance frontier of
	 * <code>b</code>.
	 * <br><br>
	 * @param a node to check in frontier of <code>b</code>
	 * @param b node whose frontier to check for <code>a</code>
	 * @return true iff <code>a</code> is in the dominance frontier of
	 *         <code>b</code>
	 */
	public boolean isInFrontierOf(Node a, Node b);
}
