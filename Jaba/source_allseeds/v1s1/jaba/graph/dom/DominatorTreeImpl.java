package jaba.graph.dom;

import jaba.sym.DemandDrivenAttribute;

import jaba.graph.Tree;
import jaba.graph.TreeImpl;
import jaba.graph.GraphAttribute;
import jaba.graph.Node;
import jaba.graph.Edge;
import jaba.graph.Graph;

import java.util.Map;
import java.util.Vector;

/**
 * DominatorTree - Encapsulates dominator information about a cfg
 * using a dominator tree.
 * <br><br>
 * @author <a href="mailto:peterd@cc.gatech.edu">Peter Dillinger</a>
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 added toString
 * @author Jay Lofstead 2003/06/06 changed toString to use getStringReduced
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/16 added toStringReduced (), toStringDetailed ()
 * @author Jay Lofstead 2003/07/10 changed to implement DominatorTree
 * @author Jay Lofstead 2003/07/17 removed legacy load ()
 */
public class DominatorTreeImpl extends TreeImpl implements GraphAttribute, DemandDrivenAttribute, DominatorTree
{
	/** 
	 * Constructs a DominatorTree based on the given <code>Map</code>
	 * from nodes to their immediate dominators.
	 * <br><br>
	 * @param immediateDominatorMap <code>Map</code> from nodes to their
	 *                              immediate dominators
	 */
	DominatorTreeImpl (Map immediateDominatorMap)
	{
		super(immediateDominatorMap);
	}

	/**
	 * Creates an edge for the tree.
	 * <br><br>
	 * This method is used internally.
	 * <br><br>
	 * @param source source for the new edge
	 * @param sink sink for the new edge
	 * @return the new edge
	 */
	protected Edge createNewEdge(Node source, Node sink)
	{
		Edge newEdge = super.createNewEdge(source, sink);
		newEdge.setLabel("dominates");
		return newEdge;
	}

	//================================================================//
	//                       A C C E S S O R S                        //
	//================================================================//

	/**
	 * Gets the immediate dominator of the specified node.
	 * <br><br>
	 * @param node node whose immediate dominator to find
	 * @return the immediate dominator of the given node.
	 */
	public Node idom(Node node)
					{ return getImmediateDominatorOf(node); }

	/**
	 * Gets the immediate dominator of the specified node.
	 * <br><br>
	 * @param node node whose immediate dominator to find
	 * @return the immediate dominator of the given node.
	 */
	public Node getImmediateDominatorOf(Node node)
					{ return getParentOf(node);}

	/**
	 * Gets all the dominators of the specified node.
	 * <br><br>
	 * @param node node whose dominators to find
	 * @return all dominators of the given node.
	 */
	public Vector domVector (Node node)
	{ return getDominatorsOfVector (node); }

	/**
         * Convenience method for converting the Vector to an Array (less efficient) from <code>domVector</code>
	 * @param node node whose dominators to find
	 * @return all dominators of the given node.
	 */
	public Node[] dom (Node node)
	{ return getDominatorsOf(node); }

	/**
	 * Gets all the dominators of the specified node.
	 * <br><br>
	 * @param node node whose dominators to find
	 * @return all dominators of the given node.
	 */
	public Vector getDominatorsOfVector (Node node)
	{ return getAncestorsOfVector (node); }

	/**
         * Convenience method for converting the Vector to an Array (less efficient) from <code>getDominatorsOfVector</code>
	 * @param node node whose dominators to find
	 * @return all dominators of the given node.
	 */
	public Node[] getDominatorsOf(Node node)
	{ return getAncestorsOf(node); }

	/**
	 * Gets the strict dominators of the specified node.
	 * <br><br>
	 * The strict dominators of a node are all the dominators of that node
	 * except the node itself.
	 * <br><br>
	 * @param node node whose strict dominators to find
	 * @return all dominators of the given node.
	 */
	public Vector getStrictDominatorsOfVector (Node node)
	{ return getStrictAncestorsOfVector (node); }

	/**
	 * Convenience method for converting the Vector to an Array (less efficient) from <code>getStrictDominatorsOfVector</code>
	 */
	public Node[] getStrictDominatorsOf(Node node)
	{ return getStrictAncestorsOf(node); }

	/** Returns true iff <code>a</code> dominates <code>b</code>. */
	public boolean dominates(Node a, Node b)
	{ return isDescendantOf(a,b); }
	
	/** Returns true iff <code>a</code> strictly dominates <code>b</code>. */
	public boolean strictlyDominates(Node a, Node b)
	{ return isStrictDescendantOf(a,b); }
	
	
	//================================================================//
	//                   F A C T O R Y   S T U F F                    //
	//================================================================//

	/**
	 * Factory method for <code>DominatorTree</code>s.
	 * <br><br>
	 * @param cfg the <code>Graph</code> representing the cfg this attribute
	 *            should describe
	 * @return a new <code>DominatorTree</code> associated with the given cfg.
	 */
	public static GraphAttribute load(Graph cfg)
	{
		// calculate the dominator info
		DominatorsCalculation domCalc = new DominatorsCalculation(cfg);
		
		// and give it to a new instance
		DominatorTree newAttribute =
				new DominatorTreeImpl (domCalc.getImmediateDominatorMap());

		// store the attribute
		cfg.addAttribute(newAttribute);

		return newAttribute;
	}

	/** returns an XML representation of this object */
	public String toString ()
	{
		return "<dominatortree>" + getStringReduced () + "</dominatortree>";
	}

	/** returns the reduced representation of this object */
	public String toStringReduced ()
	{
		return "<dominatortree>" + getStringReduced () + "</dominatortree>";
	}

	/** returns the detailed representation of this object */
	public String toStringDetailed ()
	{
		return "<dominatortree>" + getStringDetailed () + "</dominatortree>";
	}
}
