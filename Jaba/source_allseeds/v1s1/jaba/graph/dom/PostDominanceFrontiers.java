package jaba.graph.dom;

import jaba.sym.DemandDrivenAttribute;

import jaba.graph.GraphAttribute;
import jaba.graph.Node;

import java.util.Map;

/**
 * PostDominanceFrontiers - Encapsulates the dominance frontiers
 * of all the nodes in a cfg.
 * <br><br>
 * @author <a href="mailto:peterd@cc.gatech.edu">Peter Dillinger</a>
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface PostDominanceFrontiers extends GraphAttribute, DemandDrivenAttribute, FrontiersType, java.io.Serializable
{
	//================================================================//
	//                       A C C E S S O R S                        //
	//================================================================//

	/**
	 * Returns a map of the frontiers represented in this object.
	 * <br><br>
	 * The <code>Map</code> maps <code>Node</code>s to <code>Node[]</code>s.
	 * <br><br>
	 * @return map of the frontiers
	 */
	public Map getFrontiersMap();

	/**
	 * Returns a map of the cd frontiers represented in this object.
	 * The cd frontiers map a node to the edge, such that the sink of
	 * the edge belongs to the postdominance frontier of the node.
	 * The cd frontiers are used for computing control dependences
	 * so that control-dependence edges in the CDG can have labels
	 * associated with them. By using regular frontiers information,
	 * that only stores the nodes in the frontiers, it is not possible
	 * to associate labels with the edges in the CDG.
	 * <br><br>
	 * The <code>Map</code> maps <code>Node</code>s to <code>Edge[]</code>s.
	 * <br><br>
	 * @return map of the frontiers
	 */
	public Map getCDFrontiersMap();

	/**
	 * Gets all nodes in the postdominance frontier of the specified node.
	 * <br><br>
	 * @param node node whose postdominance frontier to find
	 * @return all nodes in the postdominance frontier of <code>node</code>
	 */
	public Node[] getFrontierOf(Node node);

	/**
	 * Returns true iff <code>a</code> is in the postdominance frontier of
	 * <code>b</code>.
	 * <br><br>
	 * @param a node to check in frontier of <code>b</code>
	 * @param b node whose frontier to check for <code>a</code>
	 * @return true iff <code>a</code> is in the dominance frontier of
	 *         <code>b</code>
	 */
	public boolean isInFrontierOf(Node a, Node b);
	
	/** returns an XML format of the object */
	public String toString ();
}
