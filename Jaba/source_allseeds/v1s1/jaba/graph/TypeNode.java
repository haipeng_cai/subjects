package jaba.graph;

import jaba.sym.NamedReferenceType;

/**
 * A <code>TypeNode </code> is an upper level representation of an object's type
 * A <code>TypeNode</code>, is used in constructing a ClassHierarchyGraph.
 * They are used only in the backend for organizational purposes, and are never 
 * displayed in the actual graph. Both InterfaceNode and ClassNode extend TypeNode.
 * In java, if an object implements interface I, the object is considered to be of 
 * type I. If an object is of class C, the object is considered to be of type C as
 * well. Thus, In designing TypeNode, it was reasoned that InterfaceNode and ClassNode
 * should extend the common class TypeNode.
 * @author Caleb Ho -- <i>Created 5/2001 </i>
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/04 added call to getString () in toString ()
 * @author Jay Lofstead 2003/06/06 changed XML type to just node for ease of manipulation
 * @author Jay Lofstead 2003/06/25 added implements serializable to support RMI
 */
public abstract class TypeNode extends NodeImpl implements java.io.Serializable
{
    /** ??? */
    public static final int CLASS_NODE = 1;

    /** ??? */
    public static final int INTERFACE_NODE = 2;
    
    /**
     * Used to tabulate the number of TypeNodes created for a particular
     * ClassHierarchyGraph.
     */
    protected static int TOTAL_TYPE_NODES = 0;
    /** 
     * typeNodeNumber stores the value of the static variable
     * TOTAL_TYPE_NODES when this TypeNode is initially instantiated.
     * It is used to provide each node with a numeric id, and does not
     * imply ordering of the nodes, because their order is generated randomly
     */
    protected int typeNodeNumber;

    /**
     * typeOf is initialized to 0 in the constructor, indicating that this is a 
     * TypeNode. If a ClassNode is created, the ClassNode constructor initializes the
     * typeOf variable to CLASS_NODE (i.e. value of 1). The InterfaceNode constructor
     * initializes this variable to the value of INTERFACE_NODE
     */
    protected int typeOf;

    /** A reference to the Type this TypeNode represents */
    private NamedReferenceType typeReference;

    /** ??? */
    public TypeNode()
    {
	typeOf = 0;
        /*
        * each time a TypeNode is constructed, the TOTAL_TYPE_NODES variable
        * keeping track of the number of TypeNodes created is incremented.
        */
	typeNodeNumber = TOTAL_TYPE_NODES++; 
    }

    /**
     * setTypeReference is passed in a NamedReferenceType and the type
     * this TypeNode represents is then set to that value
     */
    protected void setTypeReference( NamedReferenceType reference){
	typeReference = reference;
    }

    /**
     * getTypeNodeNumber returns the typeNodeNumber, a numeric id used in keeping
     * track of different nodes
     */
    public int getTypeNodeNumber(){
	return(typeNodeNumber);
    }

    /**
     * Returns whether this node is stricly a TypeNode, or a ClassNode or an InterfaceNode
     * return value of 0 means this is a TypeNode.
     * return value of CLASS_NODE (value of 1) means this is a classNode, etc.
     */
    public int getTypeOf(){
	return typeOf;
    }

    /**
     *  the toString method for TypeNode returns the name of the Type this typeNode represents
     */
    public String toString()
	{
		return "<node>" + getString () + "<typereferencename>" + typeReference.getName () + "</typereferencename></node>";
    }
}
