/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Class;

import java.util.Vector;
import java.util.Enumeration;

/**
 * Attribute for a throw statement. The attribute stores information about
 * the type of a throw-statement expression, and the set of possible
 * exception types that can be raised at a throw statement
 * (that are determined by the type-inference algorithm).
 * <p>
 * A throw-statement expression may be one of three types:
 * <l>
 *  <li> New-instance expression - <i>throw new Exception()</i>
 *  <li> Method-call expression - <i>throw object.method()</i>
 *  <li> Variable - <i>throw e </i>
 * </l>
 * <p>
 * The type of a throw-statement expression is set during the partial CFG
 * construction. Following that the type-inference algorithm is
 * invoked, which determines a set of possible exception types, based on
 * the throw-statement expression type, and adds those exception types
 * to the throw-statement attribute. CFG construction then continues, and
 * used the inferred types to create edges in the CFG.
 * <p>
 * Throw-statement attribute extends exception attribute: the exception type
 * stored in exception attribute represents the common super-type of all
 * exceptions that can be raised. For the various throw statement expression
 * types:
 * <l>
 *  <li> New-instance expression - Type of created object.
 *  <li> Method-call expression - Return type of called method.
 *  <li> Variable - Declared type of variable.
 * </l>
 * <p>
 * Exception types stored in throw-statement attribute may include or exclude
 * the type stored in exception attribute, and may include additional types.
 *
 * @see jaba.graph.cfg.TypeInference
 * @author S. Sinha
 * @author Jay Lofstead 2003/05/29 coverted toString to generate XML
 * @author Jay Lofstead 2003/06/02 converted elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/07/10 changed to implement ThrowStatementAttribute
 */
public class ThrowStatementAttributeImpl extends ExceptionAttributeImpl implements ThrowStatementAttribute
{
    /** Throw-statement expression is a new-instance expression. */
    public static final int NEW_INSTANCE_THROW_EXPR = 1;

    /** Throw-statement expression is a method-call expression. */
    public static final int METHOD_CALL_THROW_EXPR = 2;

    /** Throw-statement expression is a variable */
    public static final int VAR_THROW_EXPR = 3;

    /**
     * Boolean indicating if the expression contains a typecast;
     * typecasted varable and method-call expressions require
     * no type inferencing
     */
    private boolean containsTypecast;

    /** Exception types that may be raised at a throw statement. */
    private Vector exceptionTypes;

    /**
     * Array copy of the corresponding vector field. The first call to
     * <code>getExceptionTypes()</code> copies the vector to the array,
     * and sets the vector to null.
     */
    private Class[] exceptionTypesArr;

    /** Throw statement expression type of a throw statement. */
    private int expressionType;

    /**
     * Creates an empty throw-statement attribute.
     */
    public ThrowStatementAttributeImpl ()
    {
        exceptionTypes = new Vector();
        expressionType = 0;
	containsTypecast = false;
    }

    /**
     * Sets the expression type for a throw-statement attribute.
     * @param type type of throw-statement expression.
     * @throws IllegalArgumentException if <code>type</code> is not one of
     *         {@link #NEW_INSTANCE_THROW_EXPR NEW_INSTANCE_THROW_EXPR},
     *         {@link #METHOD_CALL_THROW_EXPR METHOD_CALL_THROW_EXPR}, or
     *         {@link #VAR_THROW_EXPR VAR_THROW_EXPR}.
     */
    public void setExpressionType( int type ) throws IllegalArgumentException {
        if ( type != NEW_INSTANCE_THROW_EXPR &&
             type != METHOD_CALL_THROW_EXPR &&
             type != VAR_THROW_EXPR ) {
            throw new IllegalArgumentException(
                "ThrowStatementAttribute.setExpressionType(): invalid type: "+
                type );
        }
        expressionType = type;
    }

    /**
     * Returns the expression type for this throw-statement attribute.
     * @return Expression type for this throw-statement attribute.
     */
    public int getExpressionType() {
        return expressionType;
    }

    /**
     * Returns true if the expression for this throw statement contains
     * a typecase, or false otherwise.
     * @return Boolean indicating whether the expression of the throw
     * statement contains a typecast.
     */
    public boolean containsTypecast() {
        return containsTypecast;
    }

    /**
     * Sets the boolean value indicating if the expression for this throw
     * statement contains a typecast.
     * @param value Boolean value indicating if the expression for this throw
     * statement contains a typecast.
     */
    public void setContainsTypecast( boolean value ) {
        containsTypecast = value;
    }

    /**
     * Adds an exception type to the set of exception types for this
     * throw-statement attribute.
     * @param type exception type to add.
     * @throws IllegalArgumentException if <code>type</code> is
     *                                     <code>null</code>.
     */
    public void addExceptionType( Class type ) throws IllegalArgumentException {
        if ( type == null ) {
            throw new IllegalArgumentException(
                "ThrowStatementAttribute.addExceptionType(): cannot add "+
                "null type" );
        }
	if ( exceptionTypes == null ) {
	    exceptionTypes = new Vector();
	    for ( int i = 0; i < exceptionTypesArr.length; i++ ) {
		exceptionTypes.addElement( exceptionTypesArr[i] );
	    }
	    exceptionTypesArr = null;
	}
        exceptionTypes.addElement( type );
    }

    /**
     * Returns the exception types for this throw-statement attribute.
     * @return Array of exception types for this throw-statement attribute.
     */
    public Class[] getExceptionTypes() {
        if ( exceptionTypesArr == null ) {
            exceptionTypesArr = new Class[exceptionTypes.size()];
	    int i = 0;
            for (Enumeration e = exceptionTypes.elements (); e.hasMoreElements (); i++)
	    {
                exceptionTypesArr [i] = (Class) e.nextElement ();
            }
            exceptionTypes = null;
        }
        return exceptionTypesArr;
    }

    /**
     * Returns a string representation of a throw-statement attribute.
     * @return String representation of throw-statement attribute.
     */
    public String toString()
	{
		StringBuffer str = new StringBuffer (1000);

		str.append ("<throwstatementattribute>");

		str.append ("<type>").append (getExprTypeName (expressionType)).append ("</type>");
		if (getExceptionType () != null)
		{
			str.append ("<supertype>").append (getExceptionType ().getName ()).append ("</supertype>");
		}

		Class [] types = getExceptionTypes ();
		for (int i = 0; i < types.length; i++)
		{
			str.append ("<exceptiontype>").append (types [i].getName ()).append ("</exceptiontype>");
		}

		str.append ("</throwstatementattribute>");

		return str.toString ();
    }

    /**
     * Returns a string for a throw-expression type.
     * @param type throw-expression type.
     * @return String for throw-expression type.
     * @throws IllegalArgumentException if <code>type</code> is not a valid
     *                                  throw-expression type.
     */
    private String getExprTypeName( int type ) throws IllegalArgumentException {
        switch( type ) {
            case NEW_INSTANCE_THROW_EXPR: return "new-instance expr";
            case METHOD_CALL_THROW_EXPR: return "method-call expr";
            case VAR_THROW_EXPR: return "var expr";
            default: throw new IllegalArgumentException(
                "CFG.getExprTypeName(): invalid throw-expression type: "+type );
        }
    }
}
