/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Method;

/**
 * Attribute that needs to be stored with a finally-call (statement) node.
 * @author S. Sinha
 * @author Jay Lofstead 2003/05/29 changed toString to generate XML
 * @author Jay Lofstead 2003/07/10 changed to implement FinallyCallAttribute
 */
public class FinallyCallAttributeImpl extends CallAttributeImpl implements FinallyCallAttribute
{
    /** Normal finally-call node. */
    public static final int NORMAL_CALL = 1;

    /** Exceptional finally-call node. */
    public static final int EXCEPTIONAL_CALL = 2;

  /** Name of finally block. */
  private String name;

  /** Type of finally-call node -- normal or exceptional */
  private int type;

  /** Finally method that is called. */
  private Method finallyMethod;

  /**
   * Constructor
   */
  public FinallyCallAttributeImpl ()
    {
      /* initialize all fields */
      super();
      name = null;
      type = 0;
      finallyMethod = null;
    }


  /**
   * Sets name of finally block that is called at a finally-call node.
   * @param name  The name of finally block.
   */
  public void setName( String name ) 
    {
      this.name = name;
    }

  /**
   * Sets the finally method that is called.
   * @param method  The finally method being called.
   */
  public void setMethod( Method method )
    {
      finallyMethod = method;
    }

  /**
   * Returns the name of finally block that is called at a finally-call node.
   * @return The name of finally block.
   */
  public String getName()
    {
      return name;
    }

  /**
   * Returns the finally method called.
   * @return The finally method called.
   */
  public Method getMethod()
    {
      return finallyMethod;
    }

    /**
     * Returns a string representation of a finally-call attribute.
     * @return String representation of finally-call attribute.
     */
    public String toString()
	{
		return "<finallycallattribute><name>" + name + "</name></finallycallattribute>";
    }

}
