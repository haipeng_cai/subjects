/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

/**
 * Attribute that needs to be stored with a finally-entry node.
 * @author Jay Lofstead 2003/05/29 converted toString to output XML
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface FinallyEntryAttribute extends StatementNodeAttribute
{
  /**
   * Sets name of finally block that is called at a finally-call node.
   * @param name  The name of finally block.
   */
  public void setName( String name ) ;

  /**
   * Returns the of finally block that is called at a finally-call node.
   * @return The name of finally block.
   */
  public String getName();

    /**
     * Returns a string representation of a finally-entry attribute.
     * @return String representation of finally-entry attribute.
     */
    public String toString();
}
