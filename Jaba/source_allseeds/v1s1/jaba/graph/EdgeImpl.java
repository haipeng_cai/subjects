/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import java.util.Vector;
import java.util.Arrays;
import java.util.Enumeration;

import jaba.sym.DemandDrivenAttribute;

/**
 * Represents a directed edge in a generic graph.
 *
 * @author S. Sinha
 * @author Huaxing Wu -- <i> Revised 7/17/02 -- Add toString() method </i>
 * @author Huaxing Wu -- <i> Revised 9/06/02 -- Add addAttributes() method </i>
 * @author Jay Lofstead -- 2003/05/29 changed toString to generate XML.
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/04 tweaked toString to only list the node numbers rather than the entire nodes.
 * @author Jay Lofstead 2003/06/06 added getStringReduced for minimal output
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 code merge with original branch
 * @author Jay Lofstead 2003/06/30 added implements Edge
 * @author Jay Lofstead 2003/07/17 replaced factory mechanism with newer style
 */
public class EdgeImpl implements Cloneable, java.io.Serializable, Edge
{
    /** Node at the source of the edge. */
    private Node source;

    /** Node at the sink of the edge. */
    private Node sink;

    /** Label of the edge. */
    private String label;

    /** Attributes associated with the edge. */
    private Vector attributes;


    // Constructors

    /**
     * Creates a graph edge with the specified source and sink nodes.
     * @param source source node of edge.
     * @param sink sink node of edge.
     */
    public EdgeImpl ( Node source, Node sink ) {
        this.source = source;
        this.sink = sink;
        label = null;
        attributes = new Vector();
    }   

    /**
     * Creates a graph edge with the specified source and sink nodes, and
     * the specified label.
     * @param source source node of edge.
     * @param sink sink node of edge.
     * @param label label of edge.
     */
    public EdgeImpl ( Node source, Node sink, String label ) {
        this.source = source;
        this.sink = sink;
        this.label = label;
        attributes = new Vector();
    }   


    // Methods

    /**
     * Clones a graph edge.
     * @return Clone of this edge.
     */
    public Object clone() {
        Edge newEdge = null;
	try
	  {
	    newEdge = (Edge) super.clone();
	  }catch ( CloneNotSupportedException e){
	      e.printStackTrace();
	      throw new RuntimeException( e );
	  }

        return newEdge;
    }

    /**
     * Sets the label for edge.
     * @param label label of edge.
     */
    public void setLabel( String label ) {
        this.label = label;
    }

    /**
     * Adds an attribute to the set of attributes associated with an
     * edge.
     * @param attribute Attribute to add.
     * @throws IllegalArgumentException if <code>attribute</code> is
     *                                  <code>null</code>.
     */
    public void addAttribute( EdgeAttribute attribute ) {
        if ( attribute == null ) {
            throw new IllegalArgumentException( "Edge.addAttribute():"+
                " cannot add null attribute." );
        }
        attributes.addElement( attribute );
    }

     /**
      * Adds several attributes to the set of attributes associated with an
      * edge.
      * @param attArr An array of Attributes to add.
      * @throws IllegalArgumentException if <code>attribute</code> is
      *                                  <code>null</code>.
      */
    public void addAttributes(EdgeAttribute[] attArr) {
	if(attArr == null) {
            throw new IllegalArgumentException( "Edge.addAttribute():"+
                " cannot add null attribute." );
        }
	attributes.addAll(Arrays.asList(attArr));
    }

     /**
      * Adds several attributes to the set of attributes associated with an
      * edge.
      * @param attArr A vector of Attribute objects to add.
      * @throws IllegalArgumentException if <code>attribute</code> is
      *                                  <code>null</code>.
      */
    public void addAttributes (Vector attArr)
    {
	if(attArr == null)
	{
            throw new IllegalArgumentException( "Edge.addAttribute(): cannot add null attribute." );
        }

	attributes.addAll (attArr);
    }

    /** Change the source node of this edge
     * @param source node of edge.
     */
    public void setSource(Node newSource) {
	source = newSource;
    }

    /**
     * Returns the source node of an edge.
     * @return source node of edge.
     */
    public Node getSource() {
        return source;
    }

    /**
     * Returns the sink node of an edge.
     * @return sink node of edge.
     */
    public Node getSink() {
        return sink;
    }

    /**
     * Returns the label of an edge.
     * @return label of edge.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Returns a Vector of <code>EdgeAttribute</code> objects
     * associated with an edge.
     * @return Vector of attributes associated with edge.
     * @see EdgeAttribute
     */
    public Vector getAttributesVector ()
    {
	return attributes;
    }

	/** Returns an EdgeAttribute [] of the attributes for this edge */
	public EdgeAttribute [] getAttributes ()
	{
		return (EdgeAttribute []) attributes.toArray (new EdgeAttribute [attributes.size ()]);
	}

    /**
     * Returns the number of attributes associated with an edge.
     * @return Number of attributes associated with edge.
     */
    public int getAttributeCount()
    {
        return attributes.size();
    }

	/**
	 * Returns the attribute of the specified type that may be associated
	 * with an edge.
	 * @param typeStr type of edge attribute. Must be a
	 *                subtype of <code>EdgeAttribute</code>.
	 * @return Edge attribute of the specified type, or
	 *         <code>null</code> if edge has no attribute of the specified
	 *         type.
	 * @throws IllegalArgumentException if <code>type</code> is not a subtype
	 *                                  of <code>EdgeAttribute</code>
	 */
	public EdgeAttribute getAttributeOfType (String typeStr) throws IllegalArgumentException
	{
		if (typeStr.equals ("jaba.graph.MethodCallEdgeAttribute"))
		{
			typeStr = "jaba.graph.MethodCallEdgeAttributeImpl";
		}
		if (typeStr.equals ("jaba.graph.ReturnEdgeAttribute"))
		{
			typeStr = "jaba.graph.ReturnEdgeAttributeImpl";
		}
		if (typeStr.equals ("jaba.graph.ThrowEdgeAttribute"))
		{
			typeStr = "jaba.graph.ThrowEdgeAttributeImpl";
		}
		if (typeStr.equals ("jaba.graph.FinallyContextAttribute"))
		{
			typeStr = "jaba.graph.FinallyContextAttributeImpl";
		}

		Class type = null;

		try
		{
			type = Class.forName (typeStr);
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace ();
			throw new RuntimeException (e);
		}

		try
		{
			Class c = java.lang.Class.forName ("jaba.graph.EdgeAttribute");
			if (!c.isAssignableFrom (type))
			{
				throw new IllegalArgumentException ("Edge.getAttributeOfType (): "
								+ type.getName ()
								+ " is not a subtype of EdgeAttribute"
								);
			}
		}
		catch (ClassNotFoundException cnfe)
		{
			cnfe.printStackTrace ();
			throw new RuntimeException (cnfe);
		}

		for (Enumeration e = attributes.elements (); e.hasMoreElements ();)
		{
			EdgeAttribute ea = (EdgeAttribute) e.nextElement ();
			Class storedAttrClass = ea.getClass();
			if (type.isAssignableFrom (storedAttrClass))
			{
				return ea;
			}
		}

		// attribute not found... now check if it is a DemandDrivenAttribute.
		//  If so, create it and initialize it.
		try
		{
			java.lang.Class dda = java.lang.Class.forName ("jaba.sym.DemandDrivenAttribute");
			if (dda.isAssignableFrom (type))
			{
				EdgeAttribute attr = null;
				try
				{
					java.lang.Class cc [] = new java.lang.Class [1];
					cc [0] = java.lang.Class.forName ("jaba.graph.Edge");
					java.lang.reflect.Method m = type.getMethod ("load", cc);
					attr = (EdgeAttribute) m.invoke (null, new Object [] {this});
				}
				catch (NoSuchMethodException e1)
				{
					e1.printStackTrace ();
					throw new RuntimeException (e1);
				}
				catch (IllegalAccessException e2)
				{
					e2.printStackTrace ();
					throw new RuntimeException (e2);
				}
				catch (java.lang.reflect.InvocationTargetException e3)
				{
					e3.printStackTrace ();
					throw new RuntimeException (e3);
				}

				// add demand-driven attribute to the edge
				addAttribute (attr);

				return attr;
			}
		}
		catch (ClassNotFoundException cnfe)
		{
			cnfe.printStackTrace ();
			throw new RuntimeException (cnfe);
		}

		return null;
	}

    /** Return a string rpresentation of the Edge object
     * @return string representation of the Edge
     */
    public String toString()
	{
	StringBuffer buf = new StringBuffer();

	buf.append ("<edge>");
	buf.append ("<label>" + label + "</label>");
	buf.append ("<source>" + source.getNodeNumber () + "</source>");
	buf.append ("<sink>" + sink.getNodeNumber () + "</sink>");
	
	for (Enumeration e = attributes.elements (); e.hasMoreElements ();)
	{
		buf.append ("<attribute>");
		buf.append (e.nextElement ());
		buf.append ("</attribute>");
	}

	buf.append ("</edge>");

	return buf.toString();
    }

	/** returns a minimal form of the data in XML format */
	public String getStringReduced ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<edge>");
		buf.append ("<label>" + label + "</label>");
		buf.append ("<source>" + source.getNodeNumber () + "</source>");
		buf.append ("<sink>" + sink.getNodeNumber () + "</sink>");
		buf.append ("</edge>");

		return buf.toString ();
	}
}
