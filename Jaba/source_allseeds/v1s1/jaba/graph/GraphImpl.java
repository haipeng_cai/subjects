// Copyright (c) 1999, The Ohio State University

package jaba.graph;

import jaba.debug.Debug;

import jaba.sym.Method;

import jaba.main.DottyOutputSpec;

import java.util.Vector;
import java.util.HashMap;
import java.util.Iterator;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Comparator;
import java.util.Arrays;

import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;

/**
 * Represents a generic graph.
 *
 * @author S. Sinha -- <i>Created</i>
 * @author Jim Jones -- <i>Revised, Feb. 13, 1999, Added 
 *                      getNodesInDepthFirstOrder</i>
 * @author Lakshmish Ramaswamy -- <i> Revised Nov 3 2001 Added dottyop()</i>
 * @author Caleb Ho -- <i> Revised 5/2001 Gave dottyop a more intuitive name: createDottyFile() </i>
 * @author Manas Tungare -- <i>Revised 10/2001, Debugging createDottyFile() [currently throws NullPointerException</i>
 * @author Huaxing Wu -- <i>Revised 1/30/02. Fixing bugs in getNodesInDepthFirstOrder and createDottyFile() </i>
 * @author Huaxing Wu -- <i> Revised 7/18/02. Handling two new nodes in createDottyFile() </i>
 * @author Huaxing Wu -- <i> Revised 9/23/02. Remove fontname setting in
 *                      createDottyFile() because if the fontname is unfound, 
 *                      then the label will not be displayed. And this may
 *                      become a serious problem as fontname isn't platform
 *                      independent.</i>
 * @author Jay Lofstead 2003/05/22 fixed reflection access to jaba.sym.GraphAttribute and jaba.sym.graph.Graph to refer to their current locations under jaba.graph.{Graph,GraphAttribute}.
 * @author Jay Lofstead 2003/05/29 Changed toString to generate XML
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/04 removed private node number in favor of one at Node
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 made toString abstract and added getString to replace the functionality.
 * @author Jay Lofstead 2003/06/05 commented out the printStackTrace calls in getAttributeOfType to give user more control over output
 * @author Jay Lofstead 2003/06/06 changed ordering of edges in getString to be sorted by source then sink rather than random
 * @author Jay Lofstead 2003/06/06 extracted out the comparators into separate classes
 * @author Jay Lofstead 2003/06/06 added getStringReduced () for minimal output for derived graphs
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/06/16 changed reduced string to be the default and added getStringDetailed (), toStringDetailed (), toStringReduced ()
 * @author Jay Lofstead 2003/06/30 added implements Graph
 */
public abstract class GraphImpl implements Cloneable, java.io.Serializable, Graph
{
	// Constructor
	/**
	 * Initializes fields of a graph object. Is called by the constructors
	 * of concrete sub-classes.
	 */
	public GraphImpl ()
	{
		inEdges = new HashMap();
		outEdges = new HashMap();
		edges = new Vector();
		nodes = new HashSet();
		entryNodes = null;
		exitNodes = null;
		attributes = new Vector();
	}
  
	/**
	 * Adds a node to the graph.
	 * @param node  Node to be added to the graph.
	 */
	public void addNode (Node node)
	{
		if (node == null)
		{
			throw new RuntimeException ();
		}

		nodes.add( node );
	}
  
	/**
	 * Returns all nodes in this graph.
	 * @return Array of nodes.
	 */
	public Node [] getNodes ()
	{
		Node [] returnVal = new Node [nodes.size ()];
		int i = 0;

		for (Iterator nodesIter = nodes.iterator(); nodesIter.hasNext (); i++)
		{
			returnVal [i] = (Node) nodesIter.next ();
		}

		Arrays.sort (returnVal, new NodeSortComparator ());

		return returnVal;
	}

	/**
	 * Adds an attribute to this method.
	 * @param attribute  The attribute to be added to this method.
	 * @throws IllegalArgumentException if <code>attribute</code> is
	 *                                  <code>null</code>.
	 */
	public void addAttribute (GraphAttribute attribute) throws IllegalArgumentException
	{
		if (attribute == null)
		{
			throw new IllegalArgumentException ("Graph.addAttribute (): attribute is null");
		}

		attributes.add( attribute );
	}

	/**
	 * Returns an array of attributes of this method.
	 * @return All attributes defined for this method.
	 */
	public GraphAttribute [] getAttributes ()
	{
		GraphAttribute [] g = new GraphAttribute [attributes.size ()];
		int i = 0;

		for (Iterator e = attributes.iterator (); e.hasNext (); i++)
		{
			g [i] = (GraphAttribute) e.next ();
		}

		return g;
	}

	/**
	 * Returns the attribute of the specified type that may be associated
	 * with a graph.
	 * @param type type of graph attribute.
	 * @return Graph attribute of the specified type, or
	 *         <code>null</code> if graph has no attribute of the specified
	 *         type and the specified attribute is not a
	 *         <link>DemandDrivenAttribute</link>.
	 */
	public GraphAttribute getAttributeOfType (String typeStr)
	{
		if (typeStr.equals ("jaba.graph.cdg.CDG"))
		{
			typeStr = "jaba.graph.cdg.CDGImpl";
		}
		if (typeStr.equals ("jaba.graph.dom.DominanceFrontiers"))
		{
			typeStr = "jaba.graph.dom.DominanceFrontiersImpl";
		}
		if (typeStr.equals ("jaba.graph.dom.PostDominanceFrontiers"))
		{
			typeStr = "jaba.graph.dom.PostDominanceFrontiersImpl";
		}
		if (typeStr.equals ("jaba.graph.dom.DominatorTree"))
		{
			typeStr = "jaba.graph.dom.DominatorTreeImpl";
		}
		if (typeStr.equals ("jaba.graph.dom.PostDominatorTree"))
		{
			typeStr = "jaba.graph.dom.PostDominatorTreeImpl";
		}
		if (typeStr.equals ("jaba.graph.hammock.HammockGraph"))
		{
			typeStr = "jaba.graph.hammock.HammockGraphImpl";
		}

		java.lang.Class type = null;

		try
		{
			type = java.lang.Class.forName (typeStr);
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace ();
			throw new RuntimeException (e);
		}

		try
		{
			java.lang.Class c = java.lang.Class.forName ("jaba.graph.GraphAttribute");
			if (!c.isAssignableFrom (type))
			{
				throw new IllegalArgumentException
					(
					  "Graph.getAttributeOfType (): "
					+ type.getName ()
					+ " is not a subtype of GraphAttribute"
					);
			}
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace ();
			throw new RuntimeException (e);
		}

		for (Iterator e = attributes.iterator (); e.hasNext ();)
		{
			GraphAttribute ga = (GraphAttribute) e.next ();
			java.lang.Class storedAttrClass = ga.getClass();
			if (type.isAssignableFrom (storedAttrClass))
			{
				return ga;
			}
		}
	
		// attribute not found... now check if it is a DemandDrivenAttribute.
		// If so, create and initialize it by calling the load method on the
		// class corresponding to typeStr.
		if (typeStr.equals("jaba.graph.cdg.CDGImpl")) {
		    return(jaba.graph.cdg.CDGImpl.load(this));
		}
		if (typeStr.equals("jaba.graph.dom.DominanceFrontiersImpl")) {
		    return(jaba.graph.dom.DominanceFrontiersImpl.load(this));
		}
		if (typeStr.equals("jaba.graph.dom.PostDominanceFrontiersImpl")) {
		    return(jaba.graph.dom.PostDominanceFrontiersImpl.load(this));
		}
		if (typeStr.equals("jaba.graph.dom.DominatorTreeImpl")) {
		    return(jaba.graph.dom.DominatorTreeImpl.load(this));
		}
		if (typeStr.equals("jaba.graph.dom.PostDominatorTreeImpl")) {
		    return(jaba.graph.dom.PostDominatorTreeImpl.load(this));
		}
		if (typeStr.equals("jaba.graph.hammock.HammockGraphImpl")) {
		    return(jaba.graph.hammock.HammockGraphImpl.load(this));
		}
		System.err.println("JIMJONES: GraphImpl: typeStr = "+typeStr);

		try
		{
			java.lang.Class dda = java.lang.Class.forName ("jaba.sym.DemandDrivenAttribute");
			if (dda.isAssignableFrom (type))
			{
				GraphAttribute ga = null;
				try
				{
					java.lang.Class cc [] = new java.lang.Class [1];
					cc [0] = java.lang.Class.forName ("jaba.graph.Graph");
					java.lang.reflect.Method m = type.getMethod ("load", cc);
					ga = (GraphAttribute) m.invoke (null, new Object [] {this});
				}
				catch (NoSuchMethodException e1)
				{
					throw new RuntimeException (e1);
				}
				catch (IllegalAccessException e2)
				{
					throw new RuntimeException (e2);
				}
				catch (java.lang.reflect.InvocationTargetException e3)
				{
					throw new RuntimeException (e3);
				}

				return ga;
			}
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace ();
			throw new RuntimeException (e);
		}

		return null;
	}
    

    /**
     * Adds an edge to the graph.  If either the source or the sink node is
     * not already in the graph, this will add it(them), thus making it
     * redundant (but not harmful) to call addNode() with those nodes.
     * @param edge edge to be added.
     */
    public void addEdge( Edge edge )
	{
        Node source = edge.getSource();
        Node sink = edge.getSink();
	
	if ( !nodes.contains( source ) )
	  {
	    addNode ((jaba.graph.Node) source);
	  }
	if ( !nodes.contains( sink ) )
	  {
	    addNode ((jaba.graph.Node) sink);
	  }
	edges.add( edge );
	
        // update the out-edges hashmap
        Edge[] out = (Edge[])outEdges.get( source );
        if ( out == null ) {
            out = new Edge[] { edge };
        }
        else {
            Edge[] tmp = new Edge[out.length+1];
            for ( int i=0; i<out.length; i++ ) {
                tmp[i] = out[i];
            }
            tmp[out.length] = edge;
            out = tmp;
        }
        outEdges.put( source, out );

        // update the in-edges hashmap
        Edge[] in = (Edge[])inEdges.get( sink );
        if ( in == null ) {
            in = new Edge[] { edge };
        }
        else {
            Edge[] tmp = new Edge[in.length+1];
            for ( int i=0; i<in.length; i++ ) {
                tmp[i] = in[i];
            }
            tmp[in.length] = edge;
            in = tmp;
        }
        inEdges.put( sink, in );
    }

  /**
   * Removes an edge from this graph.  If the edge is not a part of this graph,
   * nothing will happen.
   * @param edge  Edge to removed from this graph.
   * @return  Boolean indicating whether <code>edge</code> was a part of the
   *          graph before (and thus removed).
   */
  public boolean removeEdge( Edge edge )
    {
      Node source = edge.getSource();
      Node sink = edge.getSink();
      // remove this edge from the outEdges of the source node
      Edge[] sourcesOutEdges = (Edge[])outEdges.get( source );
      Edge[] replacementOutEdges = new Edge[ sourcesOutEdges.length - 1 ];
      int j = 0;
      for ( int i = 0; i < sourcesOutEdges.length; i++ )
	{
	  if ( !sourcesOutEdges[i].equals( edge ) )
	    {
	      replacementOutEdges[j] = sourcesOutEdges[i];
	      j++;
	    }
	}
      outEdges.put( source, replacementOutEdges );
      // remove this edge from the inEdges of the sink node
      Edge[] sinksInEdges = (Edge[])inEdges.get( sink );
      Edge[] replacementInEdges = new Edge[ sinksInEdges.length - 1 ];
      j = 0;
      for ( int i = 0; i < sinksInEdges.length; i++ )
	{
	  if ( !sinksInEdges[i].equals( edge ) )
	    {
	      replacementInEdges[j] = sinksInEdges[i];
	      j++;
	    }
	}
      inEdges.put( sink, replacementInEdges );
      buildEntryNodes();
      buildExitNodes();
      return edges.removeElement( edge );
    }

  /**
   * Removes a node from this graph. Also removes all inedges to, and outedges
   * from the node.
   * If the node is not a part of this graph, nothing will happen.
   * @param node  Node to removed from this graph.
   * @return  Boolean indicating whether <code>node</code> was a part of the
   *          graph before (and thus removed).
   */
  public boolean removeNode( Node node )
    {
	// remove inedges, if any, of node
	if ( inEdges.containsKey( node ) ) {
	    Edge[] nodeInEdges = (Edge[])inEdges.get( node );
	    for ( int i = 0; i < nodeInEdges.length; i++ ) {
		removeEdge( nodeInEdges[i] );
	    }
	    inEdges.remove( node );
	}

	// remove outedges, if any, of node
	if ( outEdges.containsKey( node ) ) {
	    Edge[] nodeOutEdges = (Edge[])outEdges.get( node );
	    for ( int i = 0; i < nodeOutEdges.length; i++ ) {
		removeEdge( nodeOutEdges[i] );
	    }
	    outEdges.remove( node );
	}
	// remove the node
	boolean retVal = nodes.remove( node );

	// update entry and exit nodes for graph
	buildEntryNodes();
	buildExitNodes();
	return retVal;
    }

    /**
     * Returns entry nodes of the graph -- nodes that have no predecessors.
     * @return Array of entry nodes.
     */
    public Node [] getEntryNodes ()
    {
        buildEntryNodes ();
        Node [] n = new Node [entryNodes.size ()];
	int i = 0;
        for (Iterator e = entryNodes.iterator (); e.hasNext (); i++)
	{
            n [i] = (Node) e.next ();
        }
        return n;
    }

    /**
     * Returns exit nodes of the graph -- nodes that have no successors.
     * @return Array of exit nodes.
     */
    public Node [] getExitNodes ()
    {
        buildExitNodes ();
        Node [] n = new Node [exitNodes.size ()];
	int i = 0;
        for (Iterator e = exitNodes.iterator (); e.hasNext (); i++)
	{
            n [i] = (Node) e.next ();
        }
        return n;
    }

  /**
   * Returns all nodes in depth-first ordering (as defined in the
   * "Dragon Book").
   * @return Array of nodes in depth-first ordering.
   */
    public Node [] getNodesInDepthFirstOrder ()
    {
	Node [] nodes = getNodes ();
	Node [] orderedNodes = new Node [nodes.length];

	// create a mapping from node to a boolean flag indicating whether the node has been visited yet

	HashMap visitedMap = new HashMap ();

	// initialize visitedMap to all unvisited
        int i;

	for ( i = 0; i < nodes.length; i++ ){
	    visitedMap.put (nodes [i], new Boolean (false));
	}
	nodeNumber = nodes.length - 1; 
	buildEntryNodes ();

	for (Iterator e = entryNodes.iterator (); e.hasNext ();)
        {
	    Node currentNode = (Node) e.next ();
	    orderDepthFirst (currentNode, orderedNodes, visitedMap);
	}

	for ( i = 0; i < nodes.length; i++ ) {
	    if ( !((Boolean)visitedMap.get( nodes[i] )).booleanValue() ) {
		Debug.println("Warning: node not visited", Debug.GRAPH, 1);
 		orderDepthFirst (nodes [i], orderedNodes, visitedMap);
	    }
	}
	return orderedNodes;
    }

    /**
     * Returns edges incident from a given node.
     * @param node node to return out-edges for.
     * @return Array of edges (may be a zero length array if node has no 
     *         out-edges).
     * @throws IllegalArgumentException if <code>node</code> does not exist
     *                                  in this graph.
     */
    public Edge [] getOutEdges (Node node)
    {
        if (!nodes.contains (node))
	{
            throw new IllegalArgumentException( "Graph.getOutEdges(): no such node in graph" );
        }
	Edge [] returnVal;
	returnVal = (Edge []) outEdges.get (node);
	if ( returnVal == null )
	  returnVal = new Edge [0];
        return returnVal;

    }

    /**
     * Returns edges incident into a given node.
     * @param node node to return in-edges for.
     * @return Array of edges (may be a zero length array if node has no
     *         in-edges).
     * @throws IllegalArgumentException if <code>node</code> does not exist
     *                                  in this graph.
     */
    public Edge [] getInEdges (Node node)
    {
        if (!nodes.contains (node))
	{
            throw new IllegalArgumentException( "Graph.getInEdges(): no such node in graph" );
        }
	Edge [] returnVal;
	returnVal = (Edge []) inEdges.get (node);
	if (returnVal == null)
	  returnVal = new Edge [0];
        return returnVal;

    }

  /**
   * Returns all edges in the graph in arbitrary order.
   * @return All edges in the graph in an arbitrary order.
   */
  public Edge [] getEdges ()
    {
      Edge [] returnVal = new Edge [edges.size ()];
      int i = 0;
      for (Iterator e = edges.iterator (); e.hasNext (); i++)
	returnVal [i] = (Edge) e.next ();

	Arrays.sort (returnVal, new EdgeSortComparator ());

      return returnVal;

    }

  /**
   * Returns a boolean indicating whether an edge with the specified source
   * and sink is in the graph.
   * @param edge  The edge for which to test.
   * @return  A boolean indicating whether the specified edge is in the graph.
   */
  public boolean containsEdgeLike (Node source, Node sink)
    {
      Edge[] edgesFromSource = (Edge[])outEdges.get( source );
      if ( edgesFromSource == null ) return false;
      for ( int i = 0; i < edgesFromSource.length; i++ )
	if ( edgesFromSource[i].getSink() == sink ) return true;
      return false;
    }

	/**
	 * derived types should use this method to get the common graph
	 * components when building the XML string representation.  By not
	 * placing this in toString, it forces derived types to create a
	 * wrapper method that provides the proper tag container for that type.  This
	 * implementation will return the currently agreed upon default for the level
	 * of detail the graph output should contain (reduced or detailed)
	 */
	public String getString ()
	{
		return getStringReduced ();
	}

	/**
 	 * Returns a detailed representation of the contents of the graph without any graph container tags.
	 */
	public String getStringDetailed ()
	{
		StringBuffer buf = new StringBuffer (1000);

		// buf.append ("<graph>");   // provided by derived class
		buf.append ("<nodes>");
		Node [] n = getNodes ();
		// sort by node number in increasing order
		Arrays.sort (n, new NodeSortComparator ());
		for (int i = 0; i < n.length; i++)
		{
			buf.append (n [i]);
		}
		buf.append ("</nodes>");
		buf.append ("<edges>");
		Edge [] e = getEdges ();
		// sort primarily by source node number in increasing order, then by sink node number in increasing order, then by label
		Arrays.sort (e, new EdgeSortComparator ());
		for (int i = 0; i < e.length; i++)
		{
			buf.append (e [i]);
		}
		buf.append ("</edges>");
		// buf.append ("</graph>");  // provided by derived class

		return buf.toString ();
	}

	/**
	 * retuns the XML format of the graph with minimal node and edge information.
	 * This is useful for derived graphs when complete node and edge information is
	 * really redundant.
	 */
	public String getStringReduced ()
	{
		StringBuffer buf = new StringBuffer (1000);

		// buf.append ("<graph>");   // provided by derived class
		buf.append ("<nodes>");
		Node [] n = getNodes ();
		// sort by node number in increasing order
		Arrays.sort (n, new NodeSortComparator ());
		for (int i = 0; i < n.length; i++)
		{
			buf.append (((NodeImpl) n [i]).getStringReduced ());
		}
		buf.append ("</nodes>");

		buf.append ("<edges>");
		Edge [] e = getEdges ();
		// sort primarily by source node number in increasing order, then by sink node number in increasing order, then by label
		Arrays.sort (e, new EdgeSortComparator ());
		for (int i = 0; i < e.length; i++)
		{
			buf.append (((EdgeImpl) e [i]).getStringReduced ());
		}
		buf.append ("</edges>");
		// buf.append ("</graph>");  // provided by derived class

		return buf.toString ();
	}

  /**
   * Returns the default String representation of this Graph object.
   * Returns a string representation of this graph.  Subclasses should implement this method and build
   * the string by doing the following:
   * 1. add a start tag using the graph type (such as <cfg>)
   * 2. call getString () to get the nodes and edges in a common format
   * 3. add any graph specific pieces.  These should be rare to non-existant
   * 4. add a matching end tag (such as </cfg>)
   * @return A string representation of this graph.
   */
	public abstract String toString ();

  /**
   * Returns specifically the reduced String representation of this Graph object.
   * Returns a string representation of this graph.  Subclasses should implement this method and build
   * the string by doing the following:
   * 1. add a start tag using the graph type (such as <cfg>)
   * 2. call getReducedString () to get the nodes and edges in a common format
   * 3. add any graph specific pieces.  These should be rare to non-existant
   * 4. add a matching end tag (such as </cfg>)
   * @return A string representation of this graph.
   */
	public abstract String toStringReduced ();

  /**
   * Returns specifically the detailed String representation of this Graph object.
   * Returns a string representation of this graph.  Subclasses should implement this method and build
   * the string by doing the following:
   * 1. add a start tag using the graph type (such as <cfg>)
   * 2. call getDetailedString () to get the nodes and edges in a common format
   * 3. add any graph specific pieces.  These should be rare to non-existant
   * 4. add a matching end tag (such as </cfg>)
   * @return A string representation of this graph.
   */
	public abstract String toStringDetailed ();

  /**
   * Writes this graph to a file in the format that is employed by dotty, 
   * a graph drawing software from ATT research. 
   * @param filename The name of the file into which the graph has to be 
   *	    written.
   */
   public void createDottyFile(String filename, DottyOutputSpec spec)
       throws IOException, IllegalArgumentException {
      Writer out;
      String shape;
      String color;
      StatementNode writenode;
      Hashtable subgraph = new Hashtable();
      out = new FileWriter(filename);

      // The statements below write some configuration information into the
      // output file like the Font size, Font Name, Font color etc.
      out.write("digraph \"g\" { \n graph [ \n");

      //"size" and "bb" elements have been taken out so that dotty
      // can properly process the file 
      out.write("fontsize = \"14\" \n"); //fontname = \"Helvetica\" \n");
      out.write("fontcolor = \"black\" \n");
      out.write("lp = \"349,0\" \n");
      out.write("color = \"black\" \n ] \n");
      out.write("node [ \n fontsize = \"14\" \n");
      out.write("fontcolor = \"black\" \n shape = \"ellipse\" \n");
      out.write("color = \"black\" \n style = \"filled\" \n ] \n");

      // Get all the nodes in the Graph 
      Node [] nodes = getNodes();
      
      for(int i = 0; i < nodes.length; i++) {
	  if (nodes[i] instanceof StatementNode) {
	      writenode = (StatementNode) nodes[i];

              // If the "subgraph" Hashtable is empty or it doesnt contain a key
              // corresponding to the Containing method, Create a vector, add this node
              // to the vector and add the vector to the hashtable
              // using the containing method as the key

	      // Create a new subgraph for the node only if it is a call to an internal method.
	      // In case, this is a call to an external method, the Node need not be
	      // put in any subgraph.
	      Method subgraphMethod;
	      if ( ! isInlinedNode(writenode)) {
		  subgraphMethod = (jaba.sym.Method) writenode.getContainingMethod ();
	      }
	      else {
		  subgraphMethod = (jaba.sym.Method) ((StatementNode) getInEdges ((StatementNode) writenode) [0].getSource ()).getContainingMethod ();
	      }
     
	      if (subgraph.isEmpty() || (!subgraph.containsKey(
		  writenode.getContainingMethod()))) {
		  Vector V = new Vector();
		  V.addElement(writenode);   
		  subgraph.put(writenode.getContainingMethod(), 
			       V);
	      }
	      else {
                  // If the Hashtable contains a key corresponding to the Containing Method
                  // of this node, then add the this node to the vector in the Hash table 
                  // corresponding to the containing method of this node
		  ((Vector) subgraph.get(writenode.getContainingMethod())).
		      addElement(writenode);
	      }//end else
		
	      String nodetypstr;

              // The statements below writes the color and the shape information for 
              // each type of node
	      color = writenode.getColor();
	      shape = writenode.getShape();
	      nodetypstr = writenode.getShortTypeName();

                // The statements below write out the node information to the output file.
                // The information to be written out includes the shape and the color of 
                // the node. The text to be included in the node is specified in the 
                // "spec" parameter
     	        out.write("\"" + writenode.getNodeNumber() + " " + 
			writenode.getType() + "\" [\n");
		if(spec.getLabelSpec() == DottyOutputSpec.DEFAULT ||
			 spec.labelContains("NODE_NUMBER"))
			out.write("label = \"Node Number:" +
				writenode.getNodeNumber() + "\\n ");
		if(spec.getLabelSpec() == DottyOutputSpec.DEFAULT || 
			spec.labelContains("STATEMENT_NUMBER"))
			out.write("Statement No: " + 
				writenode.getSourceLineNumber() + "\\n ");
		if(spec.getLabelSpec() == DottyOutputSpec.DEFAULT || 
			spec.labelContains("BYTECODE_OFFSET"))
			out.write("Byte Code Offset: " + 
				writenode.getByteCodeOffset() + "\\n ");
		if(spec.getLabelSpec() == DottyOutputSpec.DEFAULT || 
			spec.labelContains("NODE_TYPE"))
			out.write("Node Type :" + 
				nodetypstr);
		out.write(" \"\nshape = \"" + shape + "\" \n");
		out.write("color = \"" + color + "\" \n ]\n");
	  }
	  
	  else if (nodes[i] instanceof ExternalCodeNode) {
	      shape = "ellipse";
	      color = "yellow";
	      
	      out.write("\"ECN\" [\n");
	      out.write("label = \"External Code Node");
	      out.write(" \"\nshape = \"" + shape + "\" \n");
	      out.write("color = \"" + color + "\" \n ]\n");
	  }
	  else if (nodes[i] instanceof DefaultNode) {
	      shape = "ellipse";
	      color = "maroon";
	      
	      out.write("\"Default-" + nodes [i].getNodeNumber() + "\" [\n");
	      out.write("label = \"Default\"\n");
	      out.write("shape = \"" + shape + "\" \n");
	      out.write("color = \"" + color + "\" \n ]\n");
	  }
	  else if (nodes[i] instanceof ClassEntryNode) { // It's a Class Entry Node
	      shape = "ellipse";
	      color = "green";
	      
	      out.write("\"ClassEntryNode-" + nodes [i].getNodeNumber() + "\" [\n");
	      out.write("label = \"" + ((ClassEntryNode) nodes[i]).getClassObject().getName() + "\"\n");
	      out.write("shape = \"" + shape + "\" \n");
	      out.write("color = \"" + color + "\" \n ]\n");
	  }

      }//end for loop that loops through all nodes

      java.util.Enumeration allsubgraphs = subgraph.elements ();
      int count = 0;
      // The statements below writes the subgraph information to the
      // output file.  The subgraph information consists of a label,
      // and the nodes belonging to that subgraph

      // Loops through a method, and displays the elements inside a
      // method and each element's node number and type
      while (allsubgraphs.hasMoreElements ())
      {
	Vector nextsubgraph = (Vector) allsubgraphs.nextElement ();
	out.write("subgraph" + " " + "cluster_"+ count + "{ \n");
	out.write("label = Subgraph" + count + ";\n");
	for (Iterator e = nextsubgraph.iterator (); e.hasNext ();)
	{
		 StatementNode S = (StatementNode) e.next ();
		 out.write("\"" + S.getNodeNumber() + " " + S.getType() + 
		 "\";\n");
	}
	out.write("}\n");
        count ++;
      }

      // The statements below write out the edge information into the
      // output file.

      // loops through each of the edges constructed for this graph.
      // for each edge, constructs the souce and sink as
      // statementNodes so that the nodeNumber and type can be
      // obtained
      for(Iterator e = edges.iterator (); e.hasNext ();)
      {
	   Edge currentEdge = (Edge) e.next ();
	  if( (currentEdge.getSource() instanceof StatementNode) &&
	      (currentEdge.getSink() instanceof StatementNode) ) {
	      StatementNode Source = (StatementNode)
		  currentEdge.getSource();
	      StatementNode Sink = (StatementNode)
		  currentEdge.getSink();
	      out.write("\"" + Source.getNodeNumber() + " " + 
		     	Source.getType() + "\"-> \"" +
		       	Sink.getNodeNumber() + " " + Sink.getType() + "\" ");
	      if(currentEdge.getLabel() != null){
		  out.write("[\n label = \"Edge Label:" +
			    currentEdge.getLabel() +
			    "\" \n");
		  if(Source.getContainingMethod() != Sink.getContainingMethod())
		      out.write("style = dotted \n ]\n");
		  else
		      out.write("]\n");
	      }
	      else if(Source.getContainingMethod() != 
		      Sink.getContainingMethod())
		  out.write("[ \n style = dotted \n ]\n");
	      else 
		  out.write("\n");
	  }
	  else if( currentEdge.getSource() instanceof ExternalCodeNode ) {
	      out.write("\"ECN\" -> \"ClassEntryNode-" +
			currentEdge.getSink().getNodeNumber() + "\" ");
	      if(currentEdge.getLabel() != null) {
		  out.write("[\n label = \"" + currentEdge.getLabel() + "\"\n]");
	      }
	      out.write("\n");
	  }
	  else if( (currentEdge.getSource() instanceof ClassEntryNode) &&
		   (currentEdge.getSink() instanceof StatementNode) ) {
	      out.write("\"ClassEntryNode-" +
			currentEdge.getSource().getNodeNumber() + "\" -> \"" +
			currentEdge.getSink().getNodeNumber() + " " +
			((StatementNode)currentEdge.getSink()).getType() +
			"\"");
	      if(currentEdge.getLabel() != null) {
		  out.write("[\n label = \"" + currentEdge.getLabel() + "\"\n]");
	      }
	      out.write("\n");
	  }
	  else if( (currentEdge.getSource() instanceof ClassEntryNode) &&
		   (currentEdge.getSink() instanceof DefaultNode) ) {
	      out.write("\"ClassEntryNode-" +
			currentEdge.getSource().getNodeNumber() +
			"\" -> \"Default-" +
			currentEdge.getSink().getNodeNumber() + "\"");
	      if(currentEdge.getLabel() != null) {
		  out.write("[\n label = \"" + currentEdge.getLabel() + "\"\n]");
	      }
	      out.write("\n");
	  }
	  else {
	      throw new IllegalArgumentException("Unknown edge type");
	  }
      }
      out.write("} \n");
      out.close();
      return;
   }

    /**
     * Clones and reverses all edges in a graph. The reversed graph does not
     * have the attributes of the original graph. The nodes in the graph
     * are not cloned.
     * @return A reverse of this graph.
     */
    public Graph reverse ()
    {
	// Clone the graph and make modifications henceforth only to the clone.
	GraphImpl clonedGraph = null;

	try {
	    clonedGraph = (GraphImpl) clone();
	}catch ( CloneNotSupportedException e){
	    e.printStackTrace();
	    throw new RuntimeException( e );
	}


	Vector graphEdges = clonedGraph.edges;

	clonedGraph.clear();
	clonedGraph.inEdges = new HashMap();
	clonedGraph.outEdges = new HashMap();
	clonedGraph.edges = new Vector();
	clonedGraph.nodes = new HashSet();
	clonedGraph.attributes = new Vector();

	int numEdges = graphEdges.size();
	for (Iterator e1 = graphEdges.iterator (); e1.hasNext ();)
	{
	    Edge e = (Edge) e1.next ();
	    clonedGraph.addEdge (new EdgeImpl ((jaba.graph.Node) e.getSink (), (jaba.graph.Node) e.getSource ()));
	}

	return clonedGraph;
    }

    /**
     * Clones a graph and all its edges; copies the nodes of
     * the graph without cloning them; does not copy the attributes of the
     * graph.
     * @param g Graph to clone.
     * @return The cloned graph.
     */ 
    public static Graph clone( Graph g ) {
	GraphImpl clonedGraph = null;
	try {
	    clonedGraph = (GraphImpl) ((GraphImpl) g).clone();
	}catch ( CloneNotSupportedException e){
	    e.printStackTrace();
	    throw new RuntimeException( e );
	}

	Vector graphEdges = clonedGraph.edges;

	clonedGraph.clear();
	clonedGraph.inEdges = new HashMap();
	clonedGraph.outEdges = new HashMap();
	clonedGraph.edges = new Vector();
	clonedGraph.nodes = new HashSet();
	clonedGraph.attributes = new Vector();

	// clone the edges of the graphby creating new edge objects
	int numEdges = graphEdges.size();
	for (Iterator e1 = graphEdges.iterator (); e1.hasNext ();)
	{
	    Edge e = (Edge) e1.next ();
	    clonedGraph.addEdge( new EdgeImpl ((jaba.graph.Node) e.getSource (), (jaba.graph.Node) e.getSink (),
					   e.getLabel() ) );
	}

	return clonedGraph;
    }

    /**
     * Returns whether a given <code>Node</code> should be
     * displayed in the same subgraph as it's callee,
     * or whether it should be displayed in a subgraph
     * of its own.
     * The default return value is <code>false</code>. Subclasses of <code>Graph</code> are expected
     * to override it to specify a different behavior.
     * @param node Node under consideration.
     * @return <code>true</code> if the Node should appear in the subgraph of its callee;<BR> <code>false</code> otherwise.
     */
    public boolean isInlinedNode(Node node) {
	if(
	   node instanceof StatementNode
	   && ((StatementNode)node).getType() == StatementNode.ENTRY_NODE
	   && ((StatementNode)node).getContainingMethod().getContainingType().isSummarized()
	   ) {
	    return true;
	}
	return false;
    }

    /**
     * Identifies entry nodes - nodes that have no in-edges.
     */
    private void buildEntryNodes() {
        entryNodes = new Vector();
        Node [] nodes = getNodes ();
        for ( int i = 0; i < nodes.length; i++ ) {
            Node node = nodes[i];
            if ( getInEdges( node ).length == 0 ) {
                entryNodes.addElement( node );
            }
        }
    }

    /**
     * Identifies exit nodes - nodes that have no out-edges.
     */
    private void buildExitNodes() {
        exitNodes = new Vector();
        Node [] nodes = getNodes ();
        for ( int i = 0; i < nodes.length; i++ ) {
            Node node = nodes[i];
            if ( getOutEdges( node ).length == 0 ) {
                exitNodes.addElement( node );
            }
        }
    }

  /**
   * Recursive procedure used to order nodes in a depth-first ordering.
   * @param currentNode:  The node to be visited Vector of current nodes.
   * @param resultingNodes  Array of nodes to be populated in depth-first
   *                        order.
   * @param visitedMap  Map of nodes to a boolean flag indicated whether each
   *                    node has been visited.
   * @return Next order position to be assigned (should be set to the number
   *         of nodes for the initial call is made).
   */
  private void orderDepthFirst (Node currentNode, Node [] resultingNodes, HashMap visitedMap)
    {
	if(((Boolean)visitedMap.get( currentNode )).booleanValue())
	    Debug.println("Warning: this node has already been visited", Debug.GRAPH, 1);
          visitedMap.put( currentNode, new Boolean( true ) );
          Edge[] currentNodeOutEdges = getOutEdges( currentNode );
          if ( currentNodeOutEdges != null )
	      {
		for ( int j = 0; j < currentNodeOutEdges.length; j++ )
		  {
		    Node succNode = currentNodeOutEdges[j].getSink ();
                  // if the successor has not been visited yet, make recursive call
                  if ( !((Boolean)visitedMap.get( succNode )).booleanValue() )
		      {
			  orderDepthFirst( succNode, resultingNodes,
					   visitedMap );
		      }
		  }
	      }
          resultingNodes[ nodeNumber ] = currentNode;
          nodeNumber--; 
    }

    /**
     * Clears nodes and edges in the graph.
     */
    private void clear() {
	inEdges = null;
	outEdges = null;
	edges = null;
	nodes = null;
	entryNodes = null;
	exitNodes = null;
	attributes = null;
    }

  /**
   * All nodes in graph.  Even though we do not want nodes to be associated
   * with a graph, but with the entity that the graph represents, we are
   * keeping this list internally for speed efficiency reasons
   */
  private HashSet nodes;

  /** Edges in the graph. */
  private Vector edges;

  /** Nodes that have no in-edges. */
  private Vector entryNodes;
  
  /** Nodes that have no out-edges. */
  private Vector exitNodes;
  
  /**
   * Edges incident on a graph node. Map from <code>Node</code> to
   * <code>Edge[]</code>.
   */
  private HashMap inEdges;

  /**
   * Edges incident from a graph node. Map from <code>Node</code> to
   * <code>Edge[]</code>.
   */
  private HashMap outEdges;
  
    /** Attributes of the graph */
    private Vector attributes;

  /**
   * Variable needed to be outside of the scope of orderDepthFirst to keep
   * track of the next order position to be assigned (should be set to
   * the number of nodes - 1 for the initial call)
   */
  private int nodeNumber;
}
