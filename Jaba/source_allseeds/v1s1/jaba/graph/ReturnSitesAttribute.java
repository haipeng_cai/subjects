/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import java.util.Vector;
import java.util.Enumeration;

/**
 * Attribute to specify the potential return sites for a call site. A call
 * site that has more than one potential return site is a PNRC.

 * @author S. Sinha
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/04 removed private node number in favor of one at Node
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface ReturnSitesAttribute extends StatementNodeAttribute
{
    /**
     * Adds a statement node to the set of potential return sites.
     * @param node statement node to be added.
     * @throws IllegalArgumentException if <code>node</code> is
     *                                  <code>null</code>.
     */
    public void addReturnSite( StatementNode node ) throws IllegalArgumentException;

    /**
     * Returns an array of statement nodes that are potential return sites
     * for the corresponding call site.
     * @return Array of potential return-site nodes.
     */
    public StatementNode[] getReturnSites();

    /**
     * Returns a string representation of a return-sites attribute.
     * @return String representation of return-sites attribute.
     */
    public String toString();
}
