/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import java.util.Vector;

/**
 * Represents a directed edge in a generic graph.
 *
 * @author S. Sinha
 * @author Huaxing Wu -- <i> Revised 7/17/02 -- Add toString() method </i>
 * @author Huaxing Wu -- <i> Revised 9/06/02 -- Add addAttributes() method </i>
 * @author Jay Lofstead -- 2003/05/29 changed toString to generate XML.
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/04 tweaked toString to only list the node numbers rather than the entire nodes.
 * @author Jay Lofstead 2003/06/06 added getStringReduced for minimal output
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 code merge with original branch
 * @author Jay Lofstead 2003/06/30 changed to an interface
 */
public interface Edge
{
    /**
     * Clones a graph edge.
     * @return Clone of this edge.
     */
    public Object clone ();

    /**
     * Sets the label for edge.
     * @param label label of edge.
     */
    public void setLabel( String label );

    /**
     * Adds an attribute to the set of attributes associated with an
     * edge.
     * @param attribute Attribute to add.
     * @throws IllegalArgumentException if <code>attribute</code> is
     *                                  <code>null</code>.
     */
    public void addAttribute( EdgeAttribute attribute );

     /**
      * Adds several attributes to the set of attributes associated with an
      * edge.
      * @param attArr An array of Attributes to add.
      * @throws IllegalArgumentException if <code>attribute</code> is
      *                                  <code>null</code>.
      */
    public void addAttributes(EdgeAttribute[] attArr);

     /**
      * Adds several attributes to the set of attributes associated with an
      * edge.
      * @param attArr A vector of Attribute objects to add.
      * @throws IllegalArgumentException if <code>attribute</code> is
      *                                  <code>null</code>.
      */
    public void addAttributes (Vector attArr);

    /** Change the source node of this edge
     * @param source node of edge.
     */
    public void setSource(Node newSource);

    /**
     * Returns the source node of an edge.
     * @return source node of edge.
     */
    public Node getSource();

    /**
     * Returns the sink node of an edge.
     * @return sink node of edge.
     */
    public Node getSink();

    /**
     * Returns the label of an edge.
     * @return label of edge.
     */
    public String getLabel();

    /**
     * Returns a Vector of <code>EdgeAttribute</code> objects
     * associated with an edge.
     * @return Vector of attributes associated with edge.
     * @see EdgeAttribute
     */
    public Vector getAttributesVector ();

	/** Returns an EdgeAttribute [] of the attributes for this edge */
	public EdgeAttribute [] getAttributes ();

    /**
     * Returns the number of attributes associated with an edge.
     * @return Number of attributes associated with edge.
     */
    public int getAttributeCount();

    /**
     * Returns the attribute of the specified type that may be associated
     * with an edge.
     * @param typeStr type of edge attribute. Must be a
     *                subtype of <code>EdgeAttribute</code>.
     * @return Edge attribute of the specified type, or
     *         <code>null</code> if edge has no attribute of the specified
     *         type.
     * @throws IllegalArgumentException if <code>type</code> is not a subtype
     *                                  of <code>EdgeAttribute</code>
     */
    public EdgeAttribute getAttributeOfType( String typeStr )
                         throws IllegalArgumentException;

	/**
	 * Return a string rpresentation of the Edge object
	 * @return string representation of the Edge
	 */
	public String toString ();

	/** returns a minimal form of the data in XML format */
	public String getStringReduced ();
}
