/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.ReferenceType;

/**
 * Attribute that needs to be stored with a node where an object (a class
 * instance or an array instance) is created.
 * @author Jay Lofstead -- 2003/05/29 changed toString to generate XML.
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/07/10 changed to implement NewInstanceAttribute
 */
public class NewInstanceAttributeImpl implements NewInstanceAttribute, StatementNodeAttribute
{
  /** The type of the object being created at a node. */
  private ReferenceType objectType;

  /**
   * Constructor.
   */
  public NewInstanceAttributeImpl ()
    {
      objectType = null;

    }


  /**
   * Assigns the type of object being created at a statement node.
   * @param type  The type of object being created at a statement node.
   * @throws IllegalArgumentException if <code>type</code> is <code>null</code>.
   */
  public void setObjectType( ReferenceType type )
              throws IllegalArgumentException
    {
      if ( type == null ) 
	throw new IllegalArgumentException( "jaba.graph." +
					    "NewInstanceAttribute." +
					    "setObjectType( null )" );

      objectType = type;

    }


  /**
   * Returns the type of object being created at a statement node.
   * @return The type of object being created at statement node.
   */
  public ReferenceType getObjectType()
    {
      return objectType;

    }


    /**
     * Returns a string representation of a new-instance attribute.
     * @return String representation of new-instance attribute.
     */
    public String toString()
	{
		return "<newinstance><type>" + objectType.getName () + "</type></newinstance>";
    }

}

