/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

/**
 * Attribute that needs to be stored with a finally exit node for exit from
 * finally block because of a goto statement.
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference a member
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface FinallyTransferAttribute extends StatementNodeAttribute
{
    /**
     * Assigns the target bytecode offset for an exit from a finally block
     * because of a goto statement (break/continue in the source code).
     * @param offset  The target bytecode offset for an exit from a finally
     *                block.
     */
    public void setBytecodeOffset( int offset );

    /**
     * Returns the target bytecode offset for an exit from a finally block.
     * @return  The target bytecode offset for an exit from a finally
     *          block.
     */
    public int getBytecodeOffset();

    /**
     * Returns a string representation of an exception attribute.
     * @return String representation of exception attribute.
     */
    public String toString();
}
