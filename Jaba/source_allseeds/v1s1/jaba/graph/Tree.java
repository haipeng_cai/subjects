package jaba.graph;

import java.util.Map;
import java.util.Vector;
import java.util.Iterator;

/**
 * Encapsulates a tree--really a forest.
 * <br><br>
 * Can have multiple roots, but one can optionally enforce single-rootedness
 * in one's own code.  If one makes that choice, the method
 * <code>getSingleRoot()</code> is provided as a convenience.  That method
 * assumes the tree is in fact a single-rooted tree.
 * <br><br>
 * Might also point out that there is no checking for setting a node's
 * parent to be one of its existing descendants.  This would violate the
 * implicit rules of the structure and cause undefined behavior.
 * <br><br>
 * @author <a href="mailto:peterd@cc.gatech.edu">Peter Dillinger</a>
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface Tree extends Graph
{
	/**
	 * Adds node <code>node</code> to the tree with parent <code>parent</code>.
	 * <br><br>
	 * Works like setParent().
	 * <br><br>
	 * @param node the new node
	 * @param parent the new parent of <code>node</code>
	 */
	public void add(Node newNode, Node parent);

	/**
	 * Sets the parent of <code>node</code> to <code>parent</code>.
	 * <br><br>
	 * Adds the nodes to the tree if necessary.
	 * <br><br>
	 * @param node a node
	 * @param parent the node to be parent of <code>node</code>
	 */
	public void setParent(Node node, Node parent);

	/**
	 * Gets the parent of a given node.
	 * <br><br>
	 * @param node node to get the parent of
	 * @return the parent <code>Node</code> or null if the node is a root node
	 */
	public Node getParentOf(Node node);

	/**
	 * Gets the children of a given node.
	 * <br><br>
	 * If children of <code>null</code> are requested, behaves just like
	 * getRootNodes().
	 * <br><br>
	 * @param node node to get the children of
	 * @return a <code>Node[]</code> of the children
	 */
	public Node[] getChildrenOf (Node node);

	/**
	 * Gets the root nodes of the forest.
	 * <br><br>
	 * @return a <code>Node[]</code> of the root nodes
	 */
	public Node[] getRootNodes();

	/**
	 * Gets the single root nodes this tree.
	 * <br><br>
	 * Makes the assertion that this object contains a single-rooted tree.
	 * This must be guaranteed by its use.
	 * <br><br>
	 * @return a <code>Node[]</code> of the root nodes
	 */
	public Node getSingleRoot();

	/**
	 * Returns an array of all ancestors of a particular node, including the
	 * node itself.
	 * <br><br>
	 * @param n the node
	 * @return <code>Node[]</code> of ancestors
	 */
	public Vector getAncestorsOfVector (Node n);

	/**
         * Convenience method for converting the Vector to an Array (less efficient) from <code>getAncestorsOfVector</code>
	 * @param n the node
	 * @return <code>Node[]</code> of ancestors
	 */
	public Node [] getAncestorsOf (Node n);

	/**
	 * Returns an array of all the strict ancestors of a particular node.
	 * <br><br>
	 * @param n the node
	 * @return <code>Node[]</code> of strict ancestors
	 */
	public Vector getStrictAncestorsOfVector (Node n);

	/**
         * Convenience method for converting the Vector to an Array (less efficient) from <code>getStrictAncestorsOfVector</code>
	 * @param n the node
	 * @return <code>Node[]</code> of strict ancestors
	 */
	public Node [] getStrictAncestorsOf (Node n);

	/**
	 * Returns whether the target node is an ancestor of the source node, or
	 * the same node.
	 * <br><br>
	 * @param source source node
	 * @param target target node
	 * @return whether the target node is an ancestor of the source node, or
	 *         the same node
	 */
	public boolean isAncestorOf(Node source, Node target);

	/**
	 * Returns whether the target node is a strict ancestor of the source
	 * node.
	 * <br><br>
	 * @param source source node
	 * @param target target node
	 * @return whether the target node is a strict ancestor of the source
	 *         node.
	 */
	public boolean isStrictAncestorOf(Node source, Node target);

	/**
	 * Returns whether the target node is a descendant of the source node, or
	 * the same node.
	 * <br><br>
	 * @param source source node
	 * @param target target node
	 * @return whether the target node is a descendant of the source node, or
	 *         the same node
	 */
	public boolean isDescendantOf(Node source, Node target);

	/**
	 * Returns whether the target node is a strict descendant of the source
	 * node.
	 * <br><br>
	 * @param source source node
	 * @param target target node
	 * @return whether the target node is a strict descendant of the source
	 *         node.
	 */
	public boolean isStrictDescendantOf(Node source, Node target);
}
