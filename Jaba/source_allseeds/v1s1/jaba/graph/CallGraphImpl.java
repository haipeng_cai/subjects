package jaba.graph;

import jaba.sym.Program;
import jaba.sym.ProgramAttribute;
import jaba.sym.Class;
import jaba.sym.Method;
import jaba.sym.DemandDrivenAttribute;

import jaba.main.DottyOutputSpec;

import jaba.tools.local.Factory;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Collection;
import java.util.Iterator;

import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;

/**
 * Represents a call graph.
 * A call graph shows the interrelationship of which methods call which other methods
 * @author Caleb Ho -- <i>Created 5/2001</i>
 * @author Huaxing Wu -- <iRevised 11/20/02, fix bugs in create graph, not 
 * create edge to interface method.Also use more scalable way to generate dotty</i> 
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author jay Lofstead 2003/06/05 cleaned up enumerations
 * @author Jay Lofstead 2003/06/05 changed toString to use getString
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/16 added toStringReduced (), toStringDetailed ()
 * @author Jay Lofstead 2003/07/01 changed to implement CallGraph
 * @author Jay Lofstead 2003/07/17 removed legacy load ().  Replaced with static load method.
 * @author Jay Lofstead 2003/07/17 changed to use Factory for attribute retrieval.
 */
public class CallGraphImpl extends ProgramGraphImpl implements DemandDrivenAttribute, CallGraph
{
    /**
     * For the specified program, this constructor creates a new CallGraph object. But,
     * for the analysis and call graph simulation to be performed, load (Program p) 
     * must be called on the CallGraph object. Based on the design convention of the graphs,
     * load is automatically called by the back-end
     * @param program the CallGraph is created for
     */
    private CallGraphImpl (Program p)
    {
	super(p);
    }

    /**
     * Creates the edges and nodes for this graph by calling createMethodNodes() and createEdges()
     * @param params
     */
    public static ProgramAttribute load (Program p)
    {
	CallGraphImpl cg = new CallGraphImpl (p);

	// initialize class variables
	// make sure an ICFG is built since the call graph depends on the ICFG analysis
	Factory.getICFG (p);
        cg.classes = p.getClassesCollection ();
        cg.methodNodes = new HashMap ();

        cg.createMethodNodes ();
	cg.createEdges ();

	return cg;
    }

    /**
     * Loops through all of the classes in the program, and then loops
     * through each of the methods in each class, creating a MethodNode
     * object for each unsummarized method. These MethodNodes are stored in the hashmap
     * methodNodes, mapping each Method object, to its corresponding MethodNode. 
     * These MethodNodes will later be used in createEdges() 
     */
    private void createMethodNodes(){
	for (Iterator i = classes.iterator (); i.hasNext ();)
	{
	    Collection methodsInClass = ((Class) i.next ()).getMethodsCollection ();

	    //for each method in a class, create a corresponding method node
	    for (Iterator j = methodsInClass.iterator (); j.hasNext ();)
	    {
		Method m = (Method) j.next ();

		MethodNode temp = new MethodNode (m);
		//adds node to super class' set of nodes
   		addNode (temp);

		//hash the method node into hashmap methodNodes, with Method object as key
		methodNodes.put (m, temp);
	    }
	}
    }//end createMethodNodes

    /**
     * Called after the createMethodNodes() method has been called. Loops through each class
     * and for each method in each class, figures out which other methods it calls. So, if method
     * x calls method y, an edge is created with x as the source, and y as the sink.  
     * An edge is represented by the Edge class, which manipulates the Node classes to serve as
     * source and sink.  MethodNode, a child class of Node, is used in
     * CallGraph to 1) represent Methods in this callgraph, 2) to allow Edge objects to be created
     */ 

    private void createEdges(){
	for (Iterator i = classes.iterator (); i.hasNext ();)
	{
	    Collection methods = ((Class) i.next ()).getMethodsCollection ();
	    for (Iterator j = methods.iterator (); j.hasNext ();)
	    {
		Method currentMethod = (Method) j.next ();

		//create out edges
		MethodNode sourceNode = (MethodNode) methodNodes.get(currentMethod);
		Collection calledMethods = currentMethod.getCalledMethodsCollection ();
		for (Iterator k = calledMethods.iterator (); k.hasNext ();)
		{
			Method m = (Method) k.next ();
		    if (m.getContainingType().isSummarized() )
			continue;
		    if(m.getContainingType() instanceof jaba.sym.Interface)
			continue;

		    MethodNode sinkNode = (MethodNode) methodNodes.get(m);
		    
		    addEdge (new EdgeImpl (sourceNode, sinkNode));
		}

//following code creates in edges for each method. In edges do not have 
//to be created for each method, because creating out
//edges for each method, should cover the domain of all edges.
//If it is discovered that in edges need to be created, just uncomment the following
//code 
/*
		MethodNode sinkNode = (MethodNode) methodNodes.get(currentMethod);
		Method[] callingMethods = methods[j].getCallingMethods();
		for (int k=0; k < callingMethods.length; k++){   //loop through calling methods
                if ( callingMethods[k].getContainingType().isSummarized() )
                    continue;

		    MethodNode sourceNode = (MethodNode) methodNodes.get(callingMethods[k]);
		    addEdge (new EdgeImpl (sourceNode, sinkNode));

		}
*/
    	    }
	}
    }//end createEdges

    /**
     * creates the file needed by Dotty to visually display the graph.
     * @param filename is the file the dotty simulation will be stored in.
     * @param spec is the DottyOutputSpec representing what customizations the user wants when
     *  displaying the graph  
     */
    public void createDottyFile(String filename, DottyOutputSpec spec) throws IOException{
	Writer outputFile = new FileWriter(filename);
	String shape, color, nodeType;
	Hashtable graph = new Hashtable();
	MethodNode nodeToWrite;

   	// The statements below write some basic configuration information into the
   	// dotty file like the Font size, Font Name, Font color etc.

      	outputFile.write("digraph \"g\" { \n graph [ \n");
      	//"size" and "bb" elements have been taken out so that dotty
      	// can properly process the file
      	outputFile.write("fontsize = \"14\" \n fontname = \"Times-Roman\" \n");
      	outputFile.write("fontcolor = \"black\" \n");
      	outputFile.write("lp = \"349,0\" \n");
      	outputFile.write("color = \"black\" \n ] \n");
      	outputFile.write("node [ \n fontsize = \"14\" \n" +
                "fontname = \"Times-Roman\" \n");
      	outputFile.write("fontcolor = \"black\" \n shape = \"ellipse\" \n");  
      	outputFile.write("color = \"black\" \n style = \"filled\" \n ] \n");

  	// Get all the nodes in the CallGraph in depth first order

        Node[] nodes = getNodesInDepthFirstOrder();

        for(int i = 0; i < nodes.length; i++) {  //create all the nodes
            nodeToWrite = (MethodNode) nodes[i];
	    graph.put(nodeToWrite.getMethod(), nodeToWrite);
	//for now, generically have all nodes be the same color and shape
            color = nodeToWrite.getColor();
	    shape = nodeToWrite.getShape();
	    nodeType = nodeToWrite.getNodeTypeName();

        // Write out the node information to the output file:
        // text that will be displayed in each node (method name, node number, node type)
        // and specify the color of each node

        //outputFile.write("\"" + nodeToWrite.getMethodNodeNumber() + " " + nodeType + "\" [\n");

	outputFile.write("\"" + nodeToWrite.toString() + "\" [\n");
	    outputFile.write("label = \"Method Name : " + nodeToWrite.toString() + "\\n ");
        if(spec.getLabelSpec() == DottyOutputSpec.DEFAULT || spec.labelContains("NODE_NUMBER"))
            outputFile.write("Node Number: " + nodeToWrite.getMethodNodeNumber() + "\\n ");
        if(spec.getLabelSpec() == DottyOutputSpec.DEFAULT || spec.labelContains("NODE_TYPE"))
            outputFile.write("Node Type:" + nodeType);

        outputFile.write(" \"\nshape = \"" + shape + "\" \n");
        outputFile.write("color = \"" + color + "\" \n ]\n");
	}//end creating nodes

	//create all the edges
	Edge[] edges = getEdges();

	for(int i = 0; i < edges.length; i++){
            MethodNode source = (MethodNode) edges[i].getSource();
            MethodNode sink = (MethodNode) edges[i].getSink();

            outputFile.write("\"" + source.toString() + "\"-> \"" + sink.toString() + "\" ");

            if(edges[i].getLabel() != null){
                outputFile.write("[\n label = \"Edge Label:" +	 //write the edge label
                    edges[i].getLabel() + "\" \n");

                outputFile.write("]\n");
            }
            else
                outputFile.write("\n");
        }

         outputFile.write("} \n");
         outputFile.close();
  	
    }//end createDottyFile


    /**
     * This allows the the user to only have to pass in the output specs, and then this
     * method automatically creates the dotty file, and immediately displays the graph after
     * the dotty file has been created
     */
    public void displayGraph( DottyOutputSpec spec ) throws java.io.IOException{
	String dottyFile = "CallGraph.dotty";
	createDottyFile(dottyFile, spec);
        try {
            Runtime.getRuntime().exec( "dotty " + dottyFile ).waitFor();
        }
        catch ( IOException e1 ) {
            throw new RuntimeException( "Error: IOException occurred while " +
					"calling CallGraph.dotty " +
					e1.getMessage() );
        }
        catch ( InterruptedException e2 ) {
            throw new RuntimeException( "Error: InterruptedException " + 
					"occurred while calling " +
					"CallGraph.dotty " +
					e2.getMessage() ) ;
        } 
    }

	/** returns the call graph in string format */
	public String toString()
	{
		return "<callgraph>" + getString () + "</callgraph>";
	}

	/** returns the reduced representation of this object */
	public String toStringReduced ()
	{
		return "<callgraph>" + getStringReduced () + "</callgraph>";
	}

	/** returns the detailed representation of this object */
	public String toStringDetailed ()
	{
		return "<callgraph>" + getStringDetailed () + "</callgraph>";
	}

     /**
      * A call graph is created for a specified program, which can use multiple classes.
      * These classes are stored in the classes variable   
      */
    private Collection classes;

     /**
      * The nodes in a CallGraph represent methods (these nodes are simulated with the MehtodNode class), 
      * while the edges in the CallGraph show which methods call which other methods. the methodNodes 
      * hashmap, is used to map a Method object to the MethodNode representing that particular Method
      * Read about how the createEdges() method works to understand why Nodes are needed
      */
    private HashMap methodNodes;
}
