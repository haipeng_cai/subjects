/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import java.util.Collection;
import java.util.Vector;
import java.util.Enumeration;

import jaba.instruction.SymbolicInstruction;

import jaba.du.DefUse;

import jaba.sym.Method;
import jaba.sym.DemandDrivenAttribute;

/**
 * A <code>StatementNode</code> represents  a source-code statement.
 * <code>StatementNode</code>s are used to construct graph representations,
 * such as a control-flow or control-dependence graphs, in which a node
 * corresponds to a source-code statement.
 *
 * @author S. Sinha
 * @author Lakshmish Ramaswamy -- <i> Revised Nov 3 2000. Added  nodeNumber
 * static class field and getnodeNumber() function </i>
 * @author Huaxing Wu -- <i> Revised 5/16/02. Fix bugs caused by copying info back and forward between uses and usesArr.
 * 			Change to use 'uses' only </i>
 * @author Huaxing Wu -- <i> Revised 7/16/02. Add two new node types, increase 
 *                       the MAX_NODE_TYPE_ID value of 2. Change getNodeTypeName() 
 *                       to return the name of these two new node type</i>
 * @author Huaxing Wu -- <i> Revised 8/12/02. Remove addAttribute to superclass
 *                       Node,Change the return type of getAttributeOfType as
 *                       it becomes to an inheirted method. </i>
 * @author S. Sinha -- <i> Revised 11/22/02. Added two new node types, 
 *                       increase the MAX_NODE_TYPE_ID value of 2.</i>
 *@author Huaxing Wu -- <i> Revised 12/6/02. Create class StatementNodeType to
 *represent statementnode, this is necessary because a node type has some attributes, such as its id, name, shape, color and so on. This will make it more modular. New type should be added by creating new instanceof statementnode.
 * @author Jay Lofstead -- 2003/05/29 updated toString to generate an XML format.  removed commented out code.
 * @author Jay Lofstead -- 2003/05/29 moved nodeNumber up to the Node level since it is needed for all nodes in a CDG.
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance.
 * @author Jay Lofstead 2003/06/04 cleaned up ordering of the node types.
 * @author Jay Lofstead 2003/06/04 added call to getString () in toString ()
 * @author Jay Lofstead 2003/06/06 changed XML type to just node for ease of manipulation
 * @author Jay Lofstead 2003/06/06 added buildUses to avoid the toArray call.
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/06/25 added implements serializable to support RMI
 * @author Jay Lofstead 2003/06/30 added implements StatementNode
 * @author Jay Lofstead 2003/07/17 replaced factory method with new mechanism
 */
public class StatementNodeImpl extends NodeImpl implements java.io.Serializable, StatementNode
{
    // Constants -- statement node types; a statement node can have only one
    // type

    /**
     * Entry node. The entry node of graph for a finally block has a
     * {@link jaba.graph.FinallyEntryAttribute FinallyEntryAttribute}.
     */
    public static final int ENTRY_NODE = 1;

    /** Exit node. */
    public static final int EXIT_NODE = 2;

    /** Predicate node. */
    public static final int PREDICATE_NODE = 3;

    /** Class-instantiation node; has a
        {@link jaba.graph.NewInstanceAttribute
        NewInstanceAttribute}. */
    public static final int CLASS_INSTANTIATION_NODE = 4;

    /** Array-instantiation node; has a {@link jaba.graph.NewInstanceAttribute NewInstanceAttribute}. */
    public static final int ARRAY_INSTANTIATION_NODE = 5;

    /**
     * Virtual-method-call node; has a
     * {@link jaba.graph.MethodCallAttribute
     * MethodCallAttribute}.
     */
    public static final int VIRTUAL_METHOD_CALL_NODE = 6;

    /**
     * Static-method-call node; has a
     * {@link jaba.graph.MethodCallAttribute
     * MethodCallAttribute}.
     */
    public static final int STATIC_METHOD_CALL_NODE = 19;

    /**
     * Finally-call node for the normal context; has a
     * {@link jaba.graph.FinallyCallAttribute
     * FinallyCallAttribute}.
     */
    public static final int NORMAL_FINALLY_CALL_NODE = 7;

    /**
     * Finally-call node for the exceptional context; has a
     * {@link jaba.graph.FinallyCallAttribute
     * FinallyCallAttribute}.
     */
    public static final int EXCEPTIONAL_FINALLY_CALL_NODE = 8;

    /** Return node -- this is a node that corresponds to a call node to represent the return from a method call */
    public static final int RETURN_NODE = 9;

    /** Throw node; has a {@link jaba.graph.ThrowStatementAttribute ThrowStatementAttribute}. */
    public static final int THROW_NODE = 10;

    /** Catch node; has an {@link jaba.graph.ExceptionAttribute ExceptionAttribute}. */
    public static final int CATCH_NODE = 11;

    /** Exceptional-exit node; has an {@link jaba.graph.ExceptionAttribute ExceptionAttribute}. */
    public static final int EXCEPTIONAL_EXIT_NODE = 12;

    /** Return-predicate node */
    public static final int RETURN_PREDICATE_NODE = 13;

    /** Super-exit node */
    public static final int SUPER_EXIT_NODE = 14;

    /** Statement node */
    public static final int STATEMENT_NODE = 15;

    /** Halt node */
    public static final int HALT_NODE = 16;

    /** Node created for the purpose of defining a needed temporary */
    public static final int TEMP_DEFINITION_NODE = 17;

    /** A return statement node */
    public static final int RETURN_STATEMENT_NODE = 18;

    /** A Try Node */
    public static final int TRY_NODE = 20;

    /** The start node of Synchronized block */
    public static final int SYNCHRONIZED_START_NODE = 21;

    /** The end node of Synchronized block */
    public static final int SYNCHRONIZED_END_NODE = 22;

    /**
     * The exit node (in the CFG for a finally block) for an exit because
     * a break or continue statement. These two are indistinguishable at
     * the bytecode level---both are implemented at goto statements
     */
    public static final int FINALLY_GOTO_EXIT_NODE = 23;

    /**
     * The exit node (in the CFG for a finally block) for an exit because
     * a return statement.
     */
    public static final int FINALLY_RETURN_EXIT_NODE = 24;

    /**
     * The start node for a finally block (used only in graphs in which
     * finally blocks have been inlined into their containing method's
     * representation)
     */
    public static final int FINALLY_START_NODE = 25;

    /**
     * The end node for a finally block (used only in graphs in which
     * finally blocks have been inlined into their containing method's
     * representation)
     */
    public static final int FINALLY_END_NODE = 26;

    /** Bytecode offset of an entry node. */
    public static final int ENTRY_NODE_OFFSET = -1;

    /** Bytecode offset of an exit or exceptional-exit node. */
    public static final int EXIT_NODE_OFFSET = -2;

    /** Bytecode offset of an exit or exceptional-exit node. */
    public static final int EXCEPTIONAL_EXIT_NODE_OFFSET = -3;

    /** Default bytecode offset of a node. */
    public static final int DEFAULT_NODE_OFFSET = -100;

	/** ??? */
    private static final int MIN_NODE_TYPE_ID = 1;

	/** ??? */
    private static final int MAX_NODE_TYPE_ID = 26;

    // Fields

    /** Sequence of symbolic instructions that correspond to a statement node. */
    private SymbolicInstruction[] instructions;

    /**
     * The offset of the node in the bytecode array. The offset of a node is
     * the offset of the first symbolic instruction in the node.
     */
    private int byteCodeOffset;

    /** The source line number that corresponds to the statement node. */
    private int sourceLineNumber;

    /** Variables used at a statement node. */
    private Vector uses;

    /**
     * Boolean indicating whether the symbolic instructions' uses have been
     * bubbled up to the node level.
     */
    private boolean usesBubbledUp;

    /** DU associations at a statement node. */
    private DefUse definition;

    /**
     * Boolean indicating whether the definition for this node has been set
     * or yet.
     */
    private boolean definitionSet;

    /**
     * Method in which this statement node exists.  This method may be either
     * a top-level method or a finally method.
     */
    private Method containingMethod;

	/** ??? */
    private StatementNodeType nodeType;
    // Constructor

    /**
     * Creates a new statement node.
     */
    public StatementNodeImpl () {
        type = 0;
        instructions = null;
        byteCodeOffset = DEFAULT_NODE_OFFSET;
        sourceLineNumber = 0;
        uses = new Vector();
        definition = null;
	containingMethod = null;
	usesBubbledUp = false;
	definitionSet = false;
    }


    // Methods

    /**
     * Sets the node type of a statement node.
     * @param type type of statement node.
     * @throws IllegalArgumentException if <code>type</code> is not a valid
     *                                  statement-node type.
     */
    public void setType( int type ) throws IllegalArgumentException {
        if ( type < MIN_NODE_TYPE_ID || type > MAX_NODE_TYPE_ID ) {
            throw new IllegalArgumentException( "StatementNode.setType(): "+
                type+" is not a valid statement node type." );
        }
        this.type = type;
	nodeType = StatementNodeType.getTypeObject(type);
    }

    /**
     * Sets the bytecode offset of a statement node.
     * @param offset bytecode offset of statement node.
     */
    public void setByteCodeOffset( int offset ) {
        byteCodeOffset = offset;
    }

    /**
     * Sets the source line number that corresponds to a statement node.
     * @param line source line number of statement node.
     */
    public void setSourceLineNumber( int line ) {
        sourceLineNumber = line;
    }

    /**
     * Adds a symbolic instruction to the set of symbolic instruction
     * that correspond to a statement node.
     * @param instruction Symbolic instruction to add.
     */
    public void addSymbolicInstruction( SymbolicInstruction instruction ) {
        if ( instructions == null ) {
            instructions = new SymbolicInstruction[1];
            instructions[0] = instruction;
            return;
        }
        SymbolicInstruction[] tmp;
        tmp = new SymbolicInstruction[instructions.length+1];
        for ( int i=0; i<instructions.length; i++ ) {
            tmp[i] = instructions[i];
        }
        tmp[tmp.length-1] = instruction;
        instructions = tmp;
    }

	/** ??? */
   public void clearSymbolicInstructions() {
       buildUses ();
       getDefinition ();
       instructions = null;
   }

    /**
     * Set the method in which this statement node exists.
     * @param method  The method in which this statement node exists.
     */
    public void setContainingMethod( Method method )
    {
        if ( method == null )
            throw new IllegalArgumentException( "StatementNode." +
 					        "setContainingMethod(): " +
					        "method is null." );
        containingMethod = method;
    }

    /**
     * Sets the definition for this node. each
     * <code>StatementNode</code> has at most one definition.
     * @param def Definition for the node.
     */
  public void setDefinition( DefUse def )
    {
      definition = def;
      definitionSet = true;
    }

    /**
     * Add a set of uses to this node.
     * @param uses vector of use to be added.
     */
  public void addUse(Vector useV) {
      //To ensure there is no duplicate in the uses
      uses.ensureCapacity(uses.size() + useV.size());
      uses.removeAll(useV);
      uses.addAll(useV);
  }

    /**
     * Adds a use to the set of uses for this node.
     * @param use Use to be added.
     */
    public void addUse( DefUse use ) {
        if ( !uses.contains( use ) ) {
	    uses.add( use );
        }
    }

    /**
     * Returns the type id of this statement node.
     * @return the Type id of this statement node.
     */
    public int getType() {
        return type;
    }

    /**
     * Returns the type name of this statement node.
     * @return Type name of this statement node.
     */
    public String getNodeTypeName() {
	return nodeType.getTypeName();
    }

    /**
     * Returns the color of this object.
     * @return the color of the type
     */
    public String getColor() { return nodeType.getColor();}

    /**
     * Returns the shape of this object.
     * @return the shape of the type
     */
    public String getShape() { return nodeType.getShape();}

    /**
     * Returns an array of <code>StatementNodeAttribute</code>s
     * associated with a statement node.
     * @return Array of attributes associated with node.
     * @see StatementNodeAttribute
     */
    public StatementNodeAttribute[] getAttributes() {
        StatementNodeAttribute[] a =
            new StatementNodeAttribute[attributes.size()];
	int i = 0;
        for (Enumeration e = attributes.elements (); e.hasMoreElements (); i++)
	{
            a [i] = (StatementNodeAttribute) e.nextElement ();
        }
        return a;
    }

    /**
     * Returns the bytecode offset of a statement node.
     * @return Bytecode offset of statement node.
     */
    public int getByteCodeOffset() {
        return byteCodeOffset;
    }

    /**
     * Returns the source line number of a statement node.
     * @return Source line number of statement node.
     */
    public int getSourceLineNumber() {
        return sourceLineNumber;
    }

    /**
     * Returns the number of attributes associated with a statement node.
     * @return Number of attributes associated with statement node.
     */
    public int getAttributeCount() {
        return attributes.size();
    }

    /**
     * Returns the symbolic instructions associated with a statement node.
     * @return array of symbolic instructions.
     */
    public SymbolicInstruction [] getSymbolicInstructions() {
        return instructions;
    }

	/** Builds the uses vector */
	public void buildUses ()
	{
	if ( !usesBubbledUp )
	  {
			// build uses for node by computing a union of uses at symbolic
			// instructions for this node
			if ( instructions != null )
			{
				for (int i = 0; i < instructions.length; i++)
				{
					Vector u = instructions [i].getUsesVector ();
					if (u == null)
					{
						continue;
					}
					for (Enumeration j = u.elements (); j.hasMoreElements ();)
					{
						DefUse du = (DefUse) j.nextElement ();
						if ( !uses.contains (du) )
						{
							uses.addElement (du);
						}
					}
				}
			}
			usesBubbledUp = true;
	  }
	}

    /**
     * Returns uses at a statement node.
     * @return Uses at statement node.
     */
	public Collection getUsesCollection ()
	{
		if ( !usesBubbledUp )
		{
			buildUses ();
		}

		return uses;
	}

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getUsesCollection</code>
     * @return Uses at statement node.
     */
	public DefUse [] getUses ()
	{
		if (!usesBubbledUp)
		{
			buildUses ();
		}

		return (DefUse []) uses.toArray (new DefUse [uses.size ()]);
	}


    /**
     * Returns the definition on this statement.  May be null if there is
     * no definition on this node
     * @return Definition at statement node.
     */
    public DefUse getDefinition () 
    {
      if ( definitionSet ) {
	return definition;
      }
	// definition is not assigned -- check the symbolic instructions to
	// see if there is a definition there
      if ( instructions == null ) return null;
      for ( int i = 0; i < instructions.length; i++ )
	{
	  DefUse tempDef = instructions[i].getDefinition();
	  if ( tempDef != null )
	    {
	      if ( definition != null ){
		  for ( int j = 0; j < instructions.length; j++ )
                      System.err.println( instructions[j] );
		  throw new RuntimeException("getDefinition(): more than one " +
					     "definition on statement node\n" +
					     "source line=" + sourceLineNumber +
					     "\n node type="+nodeType.getTypeName()
					     + "\n containing method = "+
					     containingMethod.getFullyQualifiedName());
	      }
	      definition = tempDef;
	    }
	}
      definitionSet = true;
      return definition;
    }

    /**
     * Returns the method in which this statement node exists.  This method
     * may be either a top-level method or a finally method.
     * @return The method in which this statement node exists.  This method
     *         may be either a top-level method or a finally method.
     */
    public Method getContainingMethod ()
    {
        return containingMethod;
    }

    /**
     * Returns a string representation of statement node.
     * @return string representation of statement node.
     */
    public String toString()
	{
		StringBuffer str = new StringBuffer (1000);

		str.append ("<node>");
		str.append (getString ());
		str.append ("<bytecodeoffset>").append (getByteCodeOffset ()).append ("</bytecodeoffset>");
		str.append ("<sourceline>").append (sourceLineNumber).append ("</sourceline>");
		str.append ("<nodetype>").append (nodeType.getTypeName ()).append ("</nodetype>");
		str.append ("<containingmethod>");
		str.append (containingMethod.getContainingType ().getName ()).append (".").append (jaba.tools.Util.toXMLValid (containingMethod.getSignature ()));
		str.append ("</containingmethod>");

		for (Enumeration e = attributes.elements (); e.hasMoreElements ();)
		{
			StatementNodeAttribute sna = (StatementNodeAttribute) e.nextElement ();

			str.append ("<attribute>").append (sna.toString ()).append ("</attribute>");
		}

		DefUse def = getDefinition ();
		if (def != null)
		{
			str.append ("<definition>");
			str.append (def.toString ());
			str.append ("</definition>");
		}

		buildUses ();
		for (Enumeration i = uses.elements (); i.hasMoreElements ();)
		{
			str.append ("<use>");
			str.append (i.nextElement ());
			str.append ("</use>");
		}

		str.append ("</node>");

		return str.toString ();
    }

    /**
     * Returns the short type name of this object.
     * @return the short name of its type
     */
    public String getShortTypeName() {
	return nodeType.getShortTypeName();
    }

	/**
	 * Returns the attribute of the specified type that may be associated
	 * with a statement node.
	 * @param typeStr type of statement-node attribute. Must be a
	 *             subtype of <code>StatementNodeAttribute</code>.
	 * @return Statement-node attribute of the specified type, or
	 *         <code>null</code> if node has no attribute of the specified
	 *         type.
	 * @throws IllegalArgumentException if <code>type</code> is not a subtype
	 *                                  of <code>StatementNodeAttribute</code>
	 */
	public NodeAttribute getAttributeOfType (String typeStr) throws IllegalArgumentException
	{
		if (typeStr.equals ("jaba.graph.FinallyEntryAttribute"))
		{
			typeStr = "jaba.graph.FinallyEntryAttributeImpl";
		}
		if (typeStr.equals ("jaba.graph.FinallyTransferAttribute"))
		{
			typeStr = "jaba.graph.FinallyTransferAttributeImpl";
		}
		if (typeStr.equals ("jaba.graph.CallAttribute"))
		{
			typeStr = "jaba.graph.CallAttributeImpl";
		}
		if (typeStr.equals ("jaba.graph.NewInstanceAttribute"))
		{
			typeStr = "jaba.graph.NewInstanceAttributeImpl";
		}
		if (typeStr.equals ("jaba.graph.ReturnSitesAttribute"))
		{
			typeStr = "jaba.graph.ReturnSitesAttributeImpl";
		}
		if (typeStr.equals ("jaba.graph.ExceptionAttribute"))
		{
			typeStr = "jaba.graph.ExceptionAttributeImpl";
		}
		if (typeStr.equals ("jaba.graph.ThrowStatementAttribute"))
		{
			typeStr = "jaba.graph.ThrowStatementAttributeImpl";
		}
		if (typeStr.equals ("jaba.graph.FinallyCallAttribute"))
		{
			typeStr = "jaba.graph.FinallyCallAttributeImpl";
		}
		if (typeStr.equals ("jaba.graph.MethodCallAttribute"))
		{
			typeStr = "jaba.graph.MethodCallAttributeImpl";
		}

		Class type = null;
		try
		{
			type = Class.forName (typeStr);
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace ();
			throw new RuntimeException (e);
		}

		try
		{
			Class c = java.lang.Class.forName ("jaba.graph.StatementNodeAttribute");
			if (!c.isAssignableFrom (type))
			{
				throw new IllegalArgumentException ("StatementNode.getAttributeOfType(): "
								+ type.getName ()
								+ " is not a subtype of StatementNodeAttribute"
								);
			}
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace ();
			throw new RuntimeException (e);
		}

		for (Enumeration e = attributes.elements (); e.hasMoreElements ();)
		{
			StatementNodeAttribute sna = (StatementNodeAttribute) e.nextElement ();
			Class storedAttrClass = sna.getClass();
			if (type.isAssignableFrom (storedAttrClass))
			{
				return sna;
			}
		}

		// attribute not found... now check if it is a DemandDrivenAttribute.
		// If so, create it and initialize it.
		try
		{
			java.lang.Class dda = java.lang.Class.forName ("jaba.sym.DemandDrivenAttribute");
			if (dda.isAssignableFrom (type))
			{
				StatementNodeAttribute attr = null;
				try
				{
					java.lang.Class cc [] = new java.lang.Class [1];
					cc [0] = java.lang.Class.forName ("jaba.graph.StatementNode");
					java.lang.reflect.Method m = type.getMethod ("load", cc);
					attr = (StatementNodeAttribute) m.invoke (null, new Object [] {this});
				}
				catch (NoSuchMethodException e1)
				{
					e1.printStackTrace ();
					throw new RuntimeException (e1);
				}
				catch (IllegalAccessException e2)
				{
					e2.printStackTrace ();
					throw new RuntimeException (e2);
				}
				catch (java.lang.reflect.InvocationTargetException e3)
				{
					e3.printStackTrace ();
					throw new RuntimeException (e3);
				}

				return attr;
			}
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace ();
			throw new RuntimeException (e);
		}

		return null;
	}

	/** ??? */
    private static class StatementNodeType implements java.io.Serializable
    {
	/** ??? */
	int typeId;

	/** ??? */
	String typeName;

	/** ??? */
	String shortTypeName;

	/** ??? */
	String color;

	/** ??? */
	String shape;

	/** ??? */
	StatementNodeType(int typeId, String typeName, String shortTypeName,
			  String color, String shape) {
	    this.typeId = typeId;
	    this.typeName = typeName;
	    this.shortTypeName = shortTypeName;
	    this.color = color;
	    this.shape = shape;
	}

	/** ??? */
	String getColor() { return color;}

	/** ??? */
	String getShortTypeName() { return shortTypeName; }

	/** ??? */
	String getTypeName() { return typeName; }

	/** ??? */
	int getTypeId() { return typeId;}

	/** ??? */
	String getShape() { return shape;}

	/** ??? */
	static StatementNodeType getTypeObject(int typeId) {
	    switch( typeId ) {
            case StatementNode.ENTRY_NODE:
                return ENTRY_TYPE;
            case StatementNode.EXIT_NODE:
                return EXIT_TYPE;
            case StatementNode.PREDICATE_NODE:
                return PREDICATE_TYPE;
            case StatementNode.CLASS_INSTANTIATION_NODE:
                return CLASS_INSTANTIATION_TYPE;
            case StatementNode.ARRAY_INSTANTIATION_NODE:
                return ARRAY_INSTANTIATION_TYPE;
            case StatementNode.STATIC_METHOD_CALL_NODE:
                return STATIC_METHOD_CALL_TYPE;
	    case StatementNode.VIRTUAL_METHOD_CALL_NODE:
                return VIRTUAL_METHOD_CALL_TYPE;
            case StatementNode.NORMAL_FINALLY_CALL_NODE:
                return NORMAL_FINALLY_CALL_TYPE;
            case StatementNode.EXCEPTIONAL_FINALLY_CALL_NODE:
                return EXCEPTIONAL_FINALLY_CALL_TYPE;
            case StatementNode.RETURN_NODE:
                return RETURN_TYPE;
            case StatementNode.THROW_NODE:
                return THROW_TYPE;
            case StatementNode.CATCH_NODE:
                return CATCH_TYPE;
            case StatementNode.EXCEPTIONAL_EXIT_NODE:
                return EXCEPTIONAL_EXIT_TYPE;
            case StatementNode.RETURN_PREDICATE_NODE:
                return RETURN_PREDICATE_TYPE;
            case StatementNode.SUPER_EXIT_NODE:
                return SUPER_EXIT_TYPE;
            case StatementNode.STATEMENT_NODE:
                return STATEMENT_TYPE;
            case StatementNode.HALT_NODE:
                return HALT_TYPE;
            case StatementNode.TEMP_DEFINITION_NODE:
                return TEMP_DEFINITION_TYPE;
            case StatementNode.RETURN_STATEMENT_NODE:
                return RETURN_STATEMENT_TYPE;
	    case StatementNode.TRY_NODE:
		return TRY_TYPE;
	    case StatementNode.SYNCHRONIZED_START_NODE:
	        return SYNCHRONIZED_START_TYPE;
	    case StatementNode.SYNCHRONIZED_END_NODE:
		return SYNCHRONIZED_END_TYPE;
	    case StatementNode.FINALLY_GOTO_EXIT_NODE:
		return FINALLY_GOTO_EXIT_TYPE;
	    case StatementNode.FINALLY_RETURN_EXIT_NODE:
		return FINALLY_RETURN_EXIT_TYPE;
	    case StatementNode.FINALLY_START_NODE:
		return FINALLY_START_TYPE;
	    case StatementNode.FINALLY_END_NODE:
		return FINALLY_END_TYPE;
	    default:
		System.out.println(" Illegal type, Please create a "+
				   "StatementNodeType object for this type");
	    }
	    return null;
	}

	/** ??? */
	static StatementNodeType ENTRY_TYPE = 
	    new StatementNodeType(ENTRY_NODE, "Entry", "EN", "green", "ellipse");

	/** ??? */
	static StatementNodeType EXIT_TYPE =
	    new StatementNodeType(EXIT_NODE, "Exit", "EX", "red", "ellipse");

	/** ??? */
	static StatementNodeType PREDICATE_TYPE = 
	    new StatementNodeType(PREDICATE_NODE, "Predicate", "PR", "yellow", "diamond");

	/** ??? */
	static StatementNodeType CLASS_INSTANTIATION_TYPE = 
	    new StatementNodeType(CLASS_INSTANTIATION_NODE, "Class instantiation", "CI", "yellow", "ellipse");	

	/** ??? */
	static StatementNodeType ARRAY_INSTANTIATION_TYPE =
	    new StatementNodeType(ARRAY_INSTANTIATION_NODE, "Array instantiation", "AI", "yellow", "ellipse");

	/** ??? */
	static StatementNodeType STATIC_METHOD_CALL_TYPE = 
	    new StatementNodeType(STATIC_METHOD_CALL_NODE, "Static method call", "SC", "orange", "ellipse");

	/** ??? */
	static StatementNodeType VIRTUAL_METHOD_CALL_TYPE = 
	    new StatementNodeType(VIRTUAL_METHOD_CALL_NODE, "Virtual method call", "VC", "orange", "ellipse");	

	/** ??? */
	static StatementNodeType NORMAL_FINALLY_CALL_TYPE =
	    new StatementNodeType(NORMAL_FINALLY_CALL_NODE, "Normal finally call", "NFC", "orange", "ellipse");

	/** ??? */
	static StatementNodeType EXCEPTIONAL_FINALLY_CALL_TYPE = 
	    new StatementNodeType(EXCEPTIONAL_FINALLY_CALL_NODE, "Exceptional finally call", "EFC", "orange", "ellipse");

	/** ??? */
	static StatementNodeType RETURN_TYPE = 
	    new StatementNodeType(RETURN_NODE, "Return", "RT", "lightblue", "ellipse");

	/** ??? */
	static StatementNodeType THROW_TYPE = 
	    new StatementNodeType(THROW_NODE, "Throw", "TH", "yellow", "ellipse");	

	/** ??? */
	static StatementNodeType CATCH_TYPE = 
	    new StatementNodeType(CATCH_NODE, "Catch", "CA", "yellow", "ellipse");	

	/** ??? */
	static StatementNodeType EXCEPTIONAL_EXIT_TYPE =
	    new StatementNodeType(EXCEPTIONAL_EXIT_NODE, "Exceptional exit", "EE", "red","ellipse");

	/** ??? */
	static StatementNodeType RETURN_PREDICATE_TYPE = 
	    new StatementNodeType(RETURN_PREDICATE_NODE, "Return predicate", "RPR", "yellow","diamond");

	/** ??? */
	static StatementNodeType SUPER_EXIT_TYPE = 
	    new StatementNodeType(SUPER_EXIT_NODE, "Super exit", "SE", "red", "ellipse");	

	/** ??? */
	static StatementNodeType STATEMENT_TYPE = 
	    new StatementNodeType(STATEMENT_NODE, "Statement", "ST", "blue", "ellipse");	

	/** ??? */
	static StatementNodeType HALT_TYPE = 
	    new StatementNodeType(HALT_NODE, "Halt", "HL", "red","ellipse");

	/** ??? */
	static StatementNodeType TEMP_DEFINITION_TYPE =
	    new StatementNodeType(TEMP_DEFINITION_NODE, "Temporary definition", "TD", "yellow","ellipse" );

	/** ??? */
	static StatementNodeType RETURN_STATEMENT_TYPE = 
	    new StatementNodeType(RETURN_STATEMENT_NODE, "Return statement", "RS", "lightblue", "ellipse");

	/** ??? */
	static StatementNodeType TRY_TYPE = 
	    new StatementNodeType(TRY_NODE, "Try node", "TRY", "yellow", "ellipse");

	/** ??? */
	static StatementNodeType SYNCHRONIZED_START_TYPE = 
	    new StatementNodeType(SYNCHRONIZED_START_NODE, "Synchronized start", "SYNCS", "yellow", "ellipse");	

	/** ??? */
	static StatementNodeType SYNCHRONIZED_END_TYPE = 
	    new StatementNodeType(SYNCHRONIZED_END_NODE, "Synchronized end", "SYNCE", "yellow","ellipse");	

	/** ??? */
	static StatementNodeType FINALLY_GOTO_EXIT_TYPE = 
	    new StatementNodeType(FINALLY_GOTO_EXIT_NODE, "Finally goto exit", "FGTE", "red","ellipse");	

	/** ??? */
	static StatementNodeType FINALLY_RETURN_EXIT_TYPE =  
	    new StatementNodeType(FINALLY_RETURN_EXIT_NODE, "Finally return exit", "FRE", "red", "ellipse");	

	/** ??? */
	static StatementNodeType FINALLY_START_TYPE =  
	    new StatementNodeType(FINALLY_START_NODE, "Finally start", "FS", "yellow","ellipse");	

	/** ??? */
	static StatementNodeType FINALLY_END_TYPE =  
	    new StatementNodeType(FINALLY_START_NODE, "Finally end", "FE", "yellow","ellipse");	
    }
}
