package jaba.graph;

import jaba.graph.Node;
import jaba.sym.Class;

/**
 * This node type represents a Class Entry Node.
 * @author Manas Tungare -- <i>Created, November 25, 2001.</i>.
 * @author Huaxing Wu -- <i> Add method getColor, getShape, getNodeTypeName </i>
 * @author Jay Lofstead converted toString to generate XML
 * @author Jay Lofstead 2003/06/04 removed private node number in favor of one at Node.
 * @author Jay Lofstead 2003/06/04 added call to getString () in toString ()
 * @author Jay Lofstead 2003/06/06 changed XML type to just node for ease of manipulation
 * @author Jay Lofstead 2003/06/25 added implements serializable to support RMI
 */
public class ClassEntryNode extends NodeImpl implements java.io.Serializable
{
  protected Class parentClass;

  public ClassEntryNode() {
  }

  public Class getClassObject() {
    return parentClass;
  }

  public void setClassObject(Class mClass) {
    parentClass = mClass;
  }

  public String toString()
	{
		return "<node>" + getString () + parentClass.toString () + "</node>";
  }

   /**
     * Returns the shape of this object.
     *@return the shape of the type
     */
    public String getShape() { return "ellipse"; }

    /**
    * Returns the color of this object.
    *@return the color of the type
    */
    public String getColor() { return "green";}

    /**
     * Returns the type name of this object.
     *@return the name of its type
     */
    public String getNodeTypeName() { return ("ClassEntryNode-" + getNodeNumber ()); }
}
