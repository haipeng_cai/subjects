/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;
import jaba.sym.Attribute;

/**
 * Interface for attributes of a graph.
 * @author S. Sinha
 */
public interface GraphAttribute extends Attribute
{
}
