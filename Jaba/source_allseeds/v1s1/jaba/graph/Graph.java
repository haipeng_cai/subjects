// Copyright (c) 1999, The Ohio State University

package jaba.graph;

import jaba.sym.Method;

import jaba.main.DottyOutputSpec;

import java.io.IOException;

/**
 * Represents a generic graph.
 *
 * @author S. Sinha -- <i>Created</i>
 * @author Jim Jones -- <i>Revised, Feb. 13, 1999, Added 
 *                      getNodesInDepthFirstOrder</i>
 * @author Lakshmish Ramaswamy -- <i> Revised Nov 3 2001 Added dottyop()</i>
 * @author Caleb Ho -- <i> Revised 5/2001 Gave dottyop a more intuitive name: createDottyFile() </i>
 * @author Manas Tungare -- <i>Revised 10/2001, Debugging createDottyFile() [currently throws NullPointerException</i>
 * @author Huaxing Wu -- <i>Revised 1/30/02. Fixing bugs in getNodesInDepthFirstOrder and createDottyFile() </i>
 * @author Huaxing Wu -- <i> Revised 7/18/02. Handling two new nodes in createDottyFile() </i>
 * @author Huaxing Wu -- <i> Revised 9/23/02. Remove fontname setting in
 *                      createDottyFile() because if the fontname is unfound, 
 *                      then the label will not be displayed. And this may
 *                      become a serious problem as fontname isn't platform
 *                      independent.</i>
 * @author Jay Lofstead 2003/05/22 fixed reflection access to jaba.sym.GraphAttribute and jaba.sym.graph.Graph to refer to their current locations under jaba.graph.{Graph,GraphAttribute}.
 * @author Jay Lofstead 2003/05/29 Changed toString to generate XML
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/04 removed private node number in favor of one at Node
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 made toString abstract and added getString to replace the functionality.
 * @author Jay Lofstead 2003/06/05 commented out the printStackTrace calls in getAttributeOfType to give user more control over output
 * @author Jay Lofstead 2003/06/06 changed ordering of edges in getString to be sorted by source then sink rather than random
 * @author Jay Lofstead 2003/06/06 extracted out the comparators into separate classes
 * @author Jay Lofstead 2003/06/06 added getStringReduced () for minimal output for derived graphs
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/06/16 changed reduced string to be the default and added getStringDetailed (), toStringDetailed (), toStringReduced ()
 * @author Jay Lofstead 2003/06/30 changed to an interface
 */
public interface Graph
{
  // Methods

  /**
   * Adds a node to the graph.
   * @param node  Node to be added to the graph.
   */
  public void addNode (Node node);
  
  /**
   * Returns all nodes in this graph.
   * @return Array of nodes.
   */
  public Node [] getNodes ();

    /**
     * Adds an attribute to this method.
     * @param attribute  The attribute to be added to this method.
     * @throws IllegalArgumentException if <code>attribute</code> is
     *                                  <code>null</code>.
     */
    public void addAttribute( GraphAttribute attribute )
	throws IllegalArgumentException;

    /**
     * Returns an array of attributes of this method.
     * @return All attributes defined for this method.
     */
    public GraphAttribute[] getAttributes();

    /**
     * Returns the attribute of the specified type that may be associated
     * with a graph.
     * @param type type of graph attribute.
     * @return Graph attribute of the specified type, or
     *         <code>null</code> if graph has no attribute of the specified
     *         type and the specified attribute is not a
     *         <link>DemandDrivenAttribute</link>.
     */
    public GraphAttribute getAttributeOfType( String typeStr );

    /**
     * Adds an edge to the graph.  If either the source or the sink node is
     * not already in the graph, this will add it(them), thus making it
     * redundant (but not harmful) to call addNode() with those nodes.
     * @param edge edge to be added.
     */
    public void addEdge( Edge edge );

  /**
   * Removes an edge from this graph.  If the edge is not a part of this graph,
   * nothing will happen.
   * @param edge  Edge to removed from this graph.
   * @return  Boolean indicating whether <code>edge</code> was a part of the
   *          graph before (and thus removed).
   */
  public boolean removeEdge( Edge edge );

  /**
   * Removes a node from this graph. Also removes all inedges to, and outedges
   * from the node.
   * If the node is not a part of this graph, nothing will happen.
   * @param node  Node to removed from this graph.
   * @return  Boolean indicating whether <code>node</code> was a part of the
   *          graph before (and thus removed).
   */
  public boolean removeNode( Node node );

    /**
     * Returns entry nodes of the graph -- nodes that have no predecessors.
     * @return Array of entry nodes.
     */
    public Node [] getEntryNodes ();

    /**
     * Returns exit nodes of the graph -- nodes that have no successors.
     * @return Array of exit nodes.
     */
    public Node [] getExitNodes ();

  /**
   * Returns all nodes in depth-first ordering (as defined in the
   * "Dragon Book").
   * @return Array of nodes in depth-first ordering.
   */
    public Node [] getNodesInDepthFirstOrder ();

    /**
     * Returns edges incident from a given node.
     * @param node node to return out-edges for.
     * @return Array of edges (may be a zero length array if node has no 
     *         out-edges).
     * @throws IllegalArgumentException if <code>node</code> does not exist
     *                                  in this graph.
     */
    public Edge [] getOutEdges (Node node);

    /**
     * Returns edges incident into a given node.
     * @param node node to return in-edges for.
     * @return Array of edges (may be a zero length array if node has no
     *         in-edges).
     * @throws IllegalArgumentException if <code>node</code> does not exist
     *                                  in this graph.
     */
    public Edge [] getInEdges (Node node);

  /**
   * Returns all edges in the graph in arbitrary order.
   * @return All edges in the graph in an arbitrary order.
   */
  public Edge [] getEdges ();

  /**
   * Returns a boolean indicating whether an edge with the specified source
   * and sink is in the graph.
   * @param edge  The edge for which to test.
   * @return  A boolean indicating whether the specified edge is in the graph.
   */
  public boolean containsEdgeLike (Node source, Node sink);

	/**
	 * derived types should use this method to get the common graph
	 * components when building the XML string representation.  By not
	 * placing this in toString, it forces derived types to create a
	 * wrapper method that provides the proper tag container for that type.  This
	 * implementation will return the currently agreed upon default for the level
	 * of detail the graph output should contain (reduced or detailed)
	 */
	public String getString ();

	/**
 	 * Returns a detailed representation of the contents of the graph without any graph container tags.
	 */
	public String getStringDetailed ();

	/**
	 * retuns the XML format of the graph with minimal node and edge information.
	 * This is useful for derived graphs when complete node and edge information is
	 * really redundant.
	 */
	public String getStringReduced ();

  /**
   * Returns the default String representation of this Graph object.
   * Returns a string representation of this graph.  Subclasses should implement this method and build
   * the string by doing the following:
   * 1. add a start tag using the graph type (such as <cfg>)
   * 2. call getString () to get the nodes and edges in a common format
   * 3. add any graph specific pieces.  These should be rare to non-existant
   * 4. add a matching end tag (such as </cfg>)
   * @return A string representation of this graph.
   */
	public String toString ();

  /**
   * Returns specifically the reduced String representation of this Graph object.
   * Returns a string representation of this graph.  Subclasses should implement this method and build
   * the string by doing the following:
   * 1. add a start tag using the graph type (such as <cfg>)
   * 2. call getReducedString () to get the nodes and edges in a common format
   * 3. add any graph specific pieces.  These should be rare to non-existant
   * 4. add a matching end tag (such as </cfg>)
   * @return A string representation of this graph.
   */
	public String toStringReduced ();

  /**
   * Returns specifically the detailed String representation of this Graph object.
   * Returns a string representation of this graph.  Subclasses should implement this method and build
   * the string by doing the following:
   * 1. add a start tag using the graph type (such as <cfg>)
   * 2. call getDetailedString () to get the nodes and edges in a common format
   * 3. add any graph specific pieces.  These should be rare to non-existant
   * 4. add a matching end tag (such as </cfg>)
   * @return A string representation of this graph.
   */
	public String toStringDetailed ();

  /**
   * Writes this graph to a file in the format that is employed by dotty, 
   * a graph drawing software from ATT research. 
   * @param filename The name of the file into which the graph has to be 
   *	    written.
   */
   public void createDottyFile(String filename, DottyOutputSpec spec)
       throws IOException, IllegalArgumentException;

    /**
     * Clones and reverses all edges in a graph. The reversed graph does not
     * have the attributes of the original graph. The nodes in the graph
     * are not cloned.
     * @return A reverse of this graph.
     */
    public Graph reverse ();

    /**
     * Returns whether a given <code>Node</code> should be
     * displayed in the same subgraph as it's callee,
     * or whether it should be displayed in a subgraph
     * of its own.
     * The default return value is <code>false</code>. Subclasses of <code>Graph</code> are expected
     * to override it to specify a different behavior.
     * @param node Node under consideration.
     * @return <code>true</code> if the Node should appear in the subgraph of its callee;<BR> <code>false</code> otherwise.
     */
    public boolean isInlinedNode(Node node);
}
