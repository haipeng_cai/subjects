/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import java.util.Collection;
import java.util.Vector;

import jaba.instruction.SymbolicInstruction;

import jaba.du.DefUse;

import jaba.sym.Method;
import jaba.sym.DemandDrivenAttribute;

/**
 * A <code>StatementNode</code> represents  a source-code statement.
 * <code>StatementNode</code>s are used to construct graph representations,
 * such as a control-flow or control-dependence graphs, in which a node
 * corresponds to a source-code statement.
 *
 * @author S. Sinha created
 * @author <br>Lakshmish Ramaswamy Nov 3 2000. Added  nodeNumber
 *				class field and getnodeNumber() function
 * @author <br>Huaxing Wu 5/16/02. Fix bugs caused by copying info back and
 * 			forward between uses and usesArr.
 * 			Change to use 'uses' only
 * @author <br>Huaxing Wu 7/16/02. Add two new node types, increase 
 *                       the MAX_NODE_TYPE_ID value of 2. Change getNodeTypeName() 
 *                       to return the name of these two new node type
 * @author <br>Huaxing Wu 8/12/02. Remove addAttribute to superclass
 *                       Node,Change the return type of getAttributeOfType as
 *                       it becomes to an inheirted method.
 * @author <br>S. Sinha 11/22/02. Added two new node types, 
 *                       increase the MAX_NODE_TYPE_ID value of 2.
 * @author <br>Huaxing Wu 12/6/02. Create class StatementNodeType to
 *			represent statementnode, this is necessary because a node type
 * 			has some attributes, such as its id, name, shape, color and so
 * 			on. This will make it more modular. New type should be added by
 * 			creating new instanceof statementnode.
 * @author <br>Jay Lofstead 2003/05/29 updated toString to generate an XML format.  removed commented out code.
 * @author <br>Jay Lofstead 2003/05/29 moved nodeNumber up to the Node level since it is needed for all nodes in a CDG.
 * @author <br>Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance.
 * @author <br>Jay Lofstead 2003/06/04 cleaned up ordering of the node types.
 * @author <br>Jay Lofstead 2003/06/04 added call to getString () in toString ()
 * @author <br>Jay Lofstead 2003/06/06 changed XML type to just node for ease of manipulation
 * @author <br>Jay Lofstead 2003/06/06 added buildUses to avoid the toArray call.
 * @author <br>Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author <br>Jay Lofstead 2003/06/09 code cleanup
 * @author <br>Jay Lofstead 2003/06/11 merged with main branch
 * @author <br>Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author <br>Jay Lofstead 2003/06/25 added implements serializable to support RMI
 * @author <br>Jay Lofstead 2003/06/30 changed to an interface
 */
public interface StatementNode extends Node
{
    // Constants -- statement node types; a statement node can have only one
    // type

    /**
     * Entry node. The entry node of graph for a finally block has a
     *  {@link jaba.graph.FinallyEntryAttribute FinallyEntryAttribute}.
     */
    public final int ENTRY_NODE = 1;

    /** Exit node. */
    public final int EXIT_NODE = 2;

    /** Predicate node. */
    public final int PREDICATE_NODE = 3;

    /**
     * Class-instantiation node; has a
     * {@link jaba.graph.NewInstanceAttribute
     * NewInstanceAttribute}.
     */
    public final int CLASS_INSTANTIATION_NODE = 4;

    /** Array-instantiation node; has a {@link jaba.graph.NewInstanceAttribute NewInstanceAttribute}. */
    public final int ARRAY_INSTANTIATION_NODE = 5;

    /**
     * Virtual-method-call node; has a
     * {@link jaba.graph.MethodCallAttribute
     * MethodCallAttribute}.
     */
    public final int VIRTUAL_METHOD_CALL_NODE = 6;

    /**
     * Static-method-call node; has a
     * {@link jaba.graph.MethodCallAttribute
     * MethodCallAttribute}.
     */
    public final int STATIC_METHOD_CALL_NODE = 19;

    /**
     * Finally-call node for the normal context; has a
     * {@link jaba.graph.FinallyCallAttribute
     * FinallyCallAttribute}.
     */
    public final int NORMAL_FINALLY_CALL_NODE = 7;

    /**
     * Finally-call node for the exceptional context; has a
     * {@link jaba.graph.FinallyCallAttribute
     * FinallyCallAttribute}.
     */
    public final int EXCEPTIONAL_FINALLY_CALL_NODE = 8;

    /** Return node -- this is a node that corresponds to a call node to represent the return from a method call */
    public final int RETURN_NODE = 9;

    /** Throw node; has a {@link jaba.graph.ThrowStatementAttribute ThrowStatementAttribute}. */
    public final int THROW_NODE = 10;

    /** Catch node; has an {@link jaba.graph.ExceptionAttribute ExceptionAttribute}. */
    public final int CATCH_NODE = 11;

    /** Exceptional-exit node; has an {@link jaba.graph.ExceptionAttribute ExceptionAttribute}. */
    public final int EXCEPTIONAL_EXIT_NODE = 12;

    /** Return-predicate node */
    public final int RETURN_PREDICATE_NODE = 13;

    /** Super-exit node */
    public final int SUPER_EXIT_NODE = 14;

    /** Statement node */
    public final int STATEMENT_NODE = 15;

    /** Halt node */
    public final int HALT_NODE = 16;

    /** Node created for the purpose of defining a needed temporary */
    public final int TEMP_DEFINITION_NODE = 17;

    /** A return statement node */
    public final int RETURN_STATEMENT_NODE = 18;

    /* A Try Node */
    public final int TRY_NODE = 20;

    /** The start node of Synchronized block */
    public final int SYNCHRONIZED_START_NODE = 21;

    /** The end node of Synchronized block */
    public final int SYNCHRONIZED_END_NODE = 22;

    /** The exit node (in the CFG for a finally block) for an exit because
        a break or continue statement. These two are indistinguishable at
        the bytecode level---both are implemented at goto statements */
    public final int FINALLY_GOTO_EXIT_NODE = 23;

    /** The exit node (in the CFG for a finally block) for an exit because
        a return statement. */
    public final int FINALLY_RETURN_EXIT_NODE = 24;

    /** The start node for a finally block (used only in graphs in which
	finally blocks have been inlined into their containing method's
	representation) */
    public final int FINALLY_START_NODE = 25;

    /** The end node for a finally block (used only in graphs in which
	finally blocks have been inlined into their containing method's
	representation) */
    public final int FINALLY_END_NODE = 26;

    /** Bytecode offset of an entry node. */
    public final int ENTRY_NODE_OFFSET = -1;

    /** Bytecode offset of an exit or exceptional-exit node. */
    public final int EXIT_NODE_OFFSET = -2;

    /** Bytecode offset of an exit or exceptional-exit node. */
    public final int EXCEPTIONAL_EXIT_NODE_OFFSET = -3;

    /** Default bytecode offset of a node. */
    public final int DEFAULT_NODE_OFFSET = -100;

    /**
     * Sets the node type of a statement node.
     * @param type type of statement node.
     * @throws IllegalArgumentException if <code>type</code> is not a valid
     *                                  statement-node type.
     */
    public void setType( int type ) throws IllegalArgumentException;

    /**
     * Sets the bytecode offset of a statement node.
     * @param offset bytecode offset of statement node.
     */
    public void setByteCodeOffset( int offset );

    /**
     * Sets the source line number that corresponds to a statement node.
     * @param line source line number of statement node.
     */
    public void setSourceLineNumber( int line );

    /**
     * Adds a symbolic instruction to the set of symbolic instruction
     * that correspond to a statement node.
     * @param instruction Symbolic instruction to add.
     */
    public void addSymbolicInstruction( SymbolicInstruction instruction );

	/** ??? */
   public void clearSymbolicInstructions();

    /**
     * Set the method in which this statement node exists.
     * @param method  The method in which this statement node exists.
     */
    public void setContainingMethod( Method method );

    /**
     * Sets the definition for this node. each
     * <code>StatementNode</code> has at most one definition.
     * @param def Definition for the node.
     */
  public void setDefinition( DefUse def );

    /** Add a set of uses to this node.
     *@param uses vector of use to be added.
     */
  public void addUse(Vector useV);

    /**
     * Adds a use to the set of uses for this node.
     * @param use Use to be added.
     */
    public void addUse( DefUse use );

    /**
     * Returns the type id of this statement node.
     * @return the Type id of this statement node.
     */
    public int getType();

    /**
     * Returns the type name of this statement node.
     * @return Type name of this statement node.
     */
    public String getNodeTypeName();

    /**
     * Returns the color of this object.
     *@return the color of the type
     */
    public String getColor();

    /**
     * Returns the shape of this object.
     *@return the shape of the type
     */
    public String getShape();

    /**
     * Returns an array of <code>StatementNodeAttribute</code>s
     * associated with a statement node.
     * @return Array of attributes associated with node.
     * @see StatementNodeAttribute
     */
    public StatementNodeAttribute[] getAttributes();

    /**
     * Returns the bytecode offset of a statement node.
     * @return Bytecode offset of statement node.
     */
    public int getByteCodeOffset();

    /**
     * Returns the source line number of a statement node.
     * @return Source line number of statement node.
     */
    public int getSourceLineNumber();

    /**
     * Returns the number of attributes associated with a statement node.
     * @return Number of attributes associated with statement node.
     */
    public int getAttributeCount();

    /**
     * Returns the symbolic instructions associated with a statement node.
     * @return array of symbolic instructions.
     */
    public SymbolicInstruction [] getSymbolicInstructions();

	/** Builds the uses vector */
	public void buildUses ();

    /**
     * Returns uses at a statement node.
     * @return Uses at statement node.
     */
	public Collection getUsesCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getUsesCollection</code>
     * @return Uses at statement node.
     */
	public DefUse [] getUses ();

    /**
     * Returns the definition on this statement.  May be null if there is
     * no definition on this node
     * @return Definition at statement node.
     */
    public DefUse getDefinition ();

    /**
     * Returns the method in which this statement node exists.  This method
     * may be either a top-level method or a finally method.
     * @return The method in which this statement node exists.  This method
     *         may be either a top-level method or a finally method.
     */
    public Method getContainingMethod ();

    /**
     * Returns a string representation of statement node.
     * @return string representation of statement node.
     */
    public String toString();

    /**
     * Returns the short type name of this object.
     *@return the short name of its type
     */
    public String getShortTypeName();

    /**
     * Returns the attribute of the specified type that may be associated
     * with a statement node.
     * @param typeStr type of statement-node attribute. Must be a
     *             subtype of <code>StatementNodeAttribute</code>.
     * @return Statement-node attribute of the specified type, or
     *         <code>null</code> if node has no attribute of the specified
     *         type.
     * @throws IllegalArgumentException if <code>type</code> is not a subtype
     *                                  of <code>StatementNodeAttribute</code>
     */
    public NodeAttribute getAttributeOfType( String typeStr )
                                  throws IllegalArgumentException;
}
