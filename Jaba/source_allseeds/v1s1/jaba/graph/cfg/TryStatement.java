/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

/**
 * Represents a try statement, and provides various methods
 * to access information about the try statement.
 * See jaba.classfile.ExceptionHandler for implementation details.
 * 
 * @author S. Sinha
 * @see CFG
 * @author Huaxing Wu -- <i> Revised 7/18/02. Change to create SynchronizedTryBlock instead of TryBlock when the block a synchronized block </i>
 * @author Jay Lofstead 2003/05/29 converted toString to return an XML format
 * @author Jay Lofstead 2003/06/02 removed elementAt in favor of enumeration/iteration for better performance.
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface TryStatement extends java.io.Serializable
{
    /**
     * Returns the try block of the try statement.
     * @return try block of the try statement.
     */
    public TryBlock getTryBlock();

    /**
     * Returns the catch blocks of the try statement or <code>null</code> if
     * the try statement has no catch blocks.
     * @return catch blocks of the try statement or <code>null</code> if the
     *         try statement has no catch blocks.
     */
    public CatchBlock[] getCatchBlocks();

    /**
     * Returns the finally block of the try statement or <code>null</code> if
     * the try statement has no finally block.
     * @return finally block of the try statement or <code>null</code> if
     *         the try statement has no finally block.
     */
    public FinallyBlock getFinallyBlock();

    /**
     * Returns a string for a try statement.
     * @return String representationfor try statement.
     */
	public String toString ();
}
