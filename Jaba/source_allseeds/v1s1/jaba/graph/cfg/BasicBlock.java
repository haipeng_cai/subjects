/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

import jaba.instruction.SymbolicInstruction;
import jaba.instruction.Wide;

import jaba.classfile.LineNumberTableAttribute;

import jaba.constants.OpCode;

import jaba.debug.Debug;

import java.util.Vector;
import java.util.Enumeration;

/**
 * An abstract class that identifies basic blocks in an array of
 * symbolic instructions, and maps each basic block to its corresponding
 * source line number.
 *
 * @author S. Sinha
 * @author Huaxing Wu -- <i>Revised 3/21/02. Change getBlocks() to handle situation when LineNumberTableAttribute is null
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 */
abstract class BasicBlock implements java.io.Serializable
{
    /** JVM instructions which end a basic block; an instruction that follows
        such an instruction begins a new basic block. */
    private static final int[] branchInstructions = {

        OpCode.OP_IFEQ, OpCode.OP_IFNE, OpCode.OP_IFLT, OpCode.OP_IFGE,
        OpCode.OP_IFGT, OpCode.OP_IFLE,
        OpCode.OP_IF_ICMPEQ, OpCode.OP_IF_ICMPNE, OpCode.OP_IF_ICMPLT,
        OpCode.OP_IF_ICMPGE, OpCode.OP_IF_ICMPGT, OpCode.OP_IF_ICMPLE,
        OpCode.OP_IF_ACMPEQ, OpCode.OP_IF_ACMPNE,
        OpCode.OP_GOTO,
        OpCode.OP_JSR,
        OpCode.OP_RET,
        OpCode.OP_TABLESWITCH,
        OpCode.OP_LOOKUPSWITCH,
        OpCode.OP_IRETURN, OpCode.OP_LRETURN, OpCode.OP_FRETURN,
        OpCode.OP_DRETURN, OpCode.OP_ARETURN, OpCode.OP_RETURN,
        OpCode.OP_INVOKEVIRTUAL, OpCode.OP_INVOKESPECIAL,
        OpCode.OP_INVOKESTATIC, OpCode.OP_INVOKEINTERFACE,
        OpCode.OP_NEWARRAY, OpCode.OP_ANEWARRAY, OpCode.OP_MULTIANEWARRAY,
        OpCode.OP_NEW,
        OpCode.OP_ATHROW,
        OpCode.OP_IFNULL, OpCode.OP_IFNONNULL,
        OpCode.OP_GOTO_W,
        OpCode.OP_JSR_W,
	// these have been added to break after definitions
	OpCode.OP_IINC, OpCode.OP_PUTSTATIC, OpCode.OP_PUTFIELD, OpCode.OP_NEW,
	OpCode.OP_ISTORE, OpCode.OP_LSTORE, OpCode.OP_FSTORE, 
	OpCode.OP_DSTORE, OpCode.OP_ASTORE, OpCode.OP_ISTORE_0, 
	OpCode.OP_ISTORE_1, OpCode.OP_ISTORE_2, OpCode.OP_ISTORE_3, 
	OpCode.OP_LSTORE_0, OpCode.OP_LSTORE_1, OpCode.OP_LSTORE_2, 
	OpCode.OP_LSTORE_3, OpCode.OP_FSTORE_0, OpCode.OP_FSTORE_1, 
	OpCode.OP_FSTORE_2, OpCode.OP_FSTORE_3, OpCode.OP_DSTORE_0, 
	OpCode.OP_DSTORE_1, OpCode.OP_DSTORE_2, OpCode.OP_DSTORE_3, 
	OpCode.OP_ASTORE_0, OpCode.OP_ASTORE_1, OpCode.OP_ASTORE_2, 
	OpCode.OP_ASTORE_3, OpCode.OP_IASTORE, OpCode.OP_LASTORE, 
	OpCode.OP_FASTORE, OpCode.OP_DASTORE, OpCode.OP_AASTORE, 
	OpCode.OP_BASTORE, OpCode.OP_CASTORE, OpCode.OP_SASTORE
    };

    /** Branch instructions that have a target address; these targets also
     * begin basic blocks. Exclude jsr and jsr_w instructions because their
     * targets are entry statements of finally blocks -- such statements
     * have already been processed for a separate CFG. The targets are also
     * no longer valid because the first instruction of each finally block
     * (that saves the return address is reset to OP_UNDEF) is reset.
     */
    private static final int[] branchTargets = {

        OpCode.OP_IFEQ, OpCode.OP_IFNE, OpCode.OP_IFLT, OpCode.OP_IFGE,
        OpCode.OP_IFGT, OpCode.OP_IFLE,
        OpCode.OP_IF_ICMPEQ, OpCode.OP_IF_ICMPNE, OpCode.OP_IF_ICMPLT,
        OpCode.OP_IF_ICMPGE, OpCode.OP_IF_ICMPGT, OpCode.OP_IF_ICMPLE,
        OpCode.OP_IF_ACMPEQ, OpCode.OP_IF_ACMPNE,
        OpCode.OP_GOTO,
        OpCode.OP_TABLESWITCH,
        OpCode.OP_LOOKUPSWITCH,
        OpCode.OP_IFNULL, OpCode.OP_IFNONNULL,
        OpCode.OP_GOTO_W
    };

    /**
     * Identifies basic blocks in <code>instructions</code> by determining
     * the beginning offset in <code>instructions</code> of each basic
     * block. Returns an array of <code>int</code>: entries that correspond
     * to the start of a basic block are non-zero; the remaining entries are 0.
     * The non-zero value is the source line number for a basic block,
     * @param instructions symbolic instructions for which to identify
     *                     basic blocks.
     * @return Beginning offsets (indicated by source line numbers) of basic
     * blocks in <code>instructions</code>.
     */
    static int[] getBlocks( SymbolicInstruction[] instructions,
                            ExceptionHandlers eh,
                            LineNumberTableAttribute lineNumberTable )
    {
        int[] blocks = new int[instructions.length];
        // initialize array elements to 0
        for ( int i=0; i<instructions.length; i++ ) {
            blocks[i] = 0;
        }

        // identify instructions that begin a basic block, and set the
        // corresponding position in the array to -1
        byte opCode;
	if ( blocks.length > 0 ) {
	    blocks[0] = -1;  // first instruction begins a new basic block
	}
        for ( int i=0; i<instructions.length-1; i++ ) {
            opCode = (byte)instructions[i].getOpcode();
            // mark instruction that follows a branch instruction
            for ( int j=0; j<branchInstructions.length; j++ ) {
                if ( opCode == branchInstructions[j] ) {
                    blocks[i+1] = -1;
                    break;
                }
            }
            // an instruction that causes the end of a basic block may be 
	    // encapsulated within a wide; in that case the wide instruction 
	    // ends a basic block
            if ( opCode == OpCode.OP_WIDE ) {
                Wide wideInst = (Wide)instructions[i];
		byte encapOpCode = (byte)wideInst.getEncapsulatedOpcode();
		for ( int k = 0; k < branchInstructions.length; k++ )
		  {
		    if ( encapOpCode == branchInstructions[k] )
		      {
			blocks[i+1] = -1;
			break;
		      }
		  }
            }
            // if branch has a target, mark the target instruction
            for ( int j=0; j<branchTargets.length; j++ ) {
                if ( opCode == branchTargets[j] ) {
                    Vector operands = instructions[i].getOperandsVector ();
                    try {
                        if ( opCode != OpCode.OP_LOOKUPSWITCH &&
                             opCode != OpCode.OP_TABLESWITCH ) {
                            int target = ((Integer)operands.get (0)).intValue();
                            blocks[getInstructionOffset( instructions, target )]
                                = -1;
                            break;
                        }
                        int target = ((Integer)operands.get (0)).intValue();
                        blocks[getInstructionOffset( instructions, target )]=-1;
                        int k = 1;
                        if ( opCode == OpCode.OP_LOOKUPSWITCH ) {
                           k = 2;
                        }
                        while ( k < operands.size () )
			{
                            target = ((Integer)operands.get (k)).intValue();
                            blocks[getInstructionOffset( instructions, target )] = -1;
                            if ( opCode == OpCode.OP_LOOKUPSWITCH ) {
                                k += 2;
                                continue;
                            }
                            k++;
                        }
                    }
                    catch ( RuntimeException re ) {
                        Debug.println( re.getClass().getName() + ": " +
				       re.getMessage(), Debug.CFG, 2 );
                    }
                }
            }
        }

        // each catch handler in the current instruction range begins a new
        // basic block.
        int[] handlers = ((ExceptionHandlersImpl) eh).getCatchHandlerOffsets();
        for ( int i=0; i<handlers.length; i++ ) {
            try {
                blocks[getInstructionOffset( instructions, handlers[i] )] = -1;
            }
            catch ( Exception e ) {
                // Ignore catch handlers that lie outside the instruction range
                // that is currently being processed.
            }
        }

        // refine basic blocks by using the source line numbers - a basic block
        // may be partitioned into separate blocks if its instructions appear
        // in different lines in the source code.

        for ( int i=0; i<blocks.length; i++ ) {
	    int blockLineNumber = -1;
	    if(lineNumberTable != null) {
		blockLineNumber = lineNumberTable.getLineNumberAtOffset(
                                  instructions[i].getByteCodeOffset() );
	    }
            if ( blockLineNumber > 0 ) {
                blocks[i] = blockLineNumber;
            }
        }

	// instructions in finally blocks may have -1 entries because the
	// line number for such instructions is stored at the jump offset, and
	// the instruction at that offset is reset during the CFG
	// construction
	for ( int i=0; i<blocks.length; i++ ) {
	    if ( blocks[i] < 0 ) {
		int finallyJumpOffset =
		    ((ExceptionHandlersImpl) eh).getFinallyJumpOffset( instructions[i].
					     getByteCodeOffset() );
		if ( finallyJumpOffset > 0 ) {
		    // instruction at offset i is the CFG-start offset for
		    // a finally block: get the line number for the
		    // corresponding jump offset for the finally block
		    int blockLineNumber = -1;
		    if(lineNumberTable != null) {
			blockLineNumber = lineNumberTable.
			    getLineNumberAtOffset( finallyJumpOffset );
		    }
		    if ( blockLineNumber > 0 ) {
			blocks[i] = blockLineNumber;
		    } 
		}
	    }
	}

        // there may be remaining -1 entries in the blocks array: such
        // instructions begin a basic block, but do not have an entry in the
        // line number table because they occur in the same line as the previous
        // instruction; find such instructions and set their line number
        for ( int i=0; i<blocks.length; i++ ) {
            if ( blocks[i] < 0 ) {
                for ( int j=i-1; j>=0; j-- ) {
                    if ( blocks[j] > 0 ) {
                        blocks[i] = blocks[j];
                        break;
                    }
                }
            }
        }

        return blocks;
    }

    /**
     * Returns the index in the symbolic-instruction array for which the
     * bytecode offset of the symbolic instruction matches the given
     * bytecode offset.
     * @param instr array of symbolic instructions.
     * @param byteCodeOffset bytecode offset to search for.
     * @return Index in the symbolic-instruction array.
     * @throws RuntimeException if no symbolic instruction begins at the
     *                          specified offset. This must only happen for
     *                          </i>goto<i> instructions in a finally block
     *                          that correspond to <code>break</code> or
     *                          <code>continue</code> statements in a loop
     *                          such that the loop encloses the finally block.
     */
    private static int getInstructionOffset( SymbolicInstruction[] instr,
                                             int byteCodeOffset )
                       throws RuntimeException
    {
        for ( int i=0; i<instr.length; i++ ) {
            if ( instr[i].getByteCodeOffset() == byteCodeOffset ) {
                return i;
            }
        }
        throw new RuntimeException(
            "WARNING: BasicBlocks.getInstructionOffset(): Jump target "+
            byteCodeOffset+" not found in instructions" );
    }

}
