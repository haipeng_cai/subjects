/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

import jaba.classfile.ExceptionHandler;

import jaba.sym.ConstantPoolMap;
import jaba.sym.Class;

import java.util.Vector;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Iterator;

/**
 * Represents a try statement, and provides various methods
 * to access information about the try statement.
 * <p>
 * @author S. Sinha
 * @see jaba.classfile.ExceptionHandler
 * @see CFG
 * @author Huaxing Wu -- <i> Revised 7/18/02. Change to create SynchronizedTryBlock instead of TryBlock when the block a synchronized block </i>
 * @author Jay Lofstead 2003/05/29 converted toString to return an XML format
 * @author Jay Lofstead 2003/06/02 removed elementAt in favor of enumeration/iteration for better performance.
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to implement TryStatement
 */
public class TryStatementImpl implements java.io.Serializable, TryStatement
{
    /** Try block of the try statement */
    private TryBlock tryBlock;

    /** Catch blocks of the try statement */
    private CatchBlock[] catchBlocks;

    /** Finally block of the try statement */
    private FinallyBlockImpl finallyBlock;

    /**
     * Constructs a <code>TryStatement</code> object. Initializes
     * internal data structures that hold information about try, catch, and
     * finally blocks.
     * @param exceptionTable exception-handler table for a method.
     * @param bytecode array of bytecode for a method.
     * @param constantPoolMap map from a constant-pool index to a symbol-table
     *                        object.
     * @throws MalformedTryStatementException if an exception is raised
     *                                        during initialization of the
     *                                        finally-block information.
     * @throws SymbolNotFoundException if <code>constantPoolMap</code> does
     *                                 not map a catch type from
     *                                 <code>exceptionTable</code> to a
     *                                 <code>Class</code>.
     */
    TryStatementImpl ( Vector exceptionTable,
		  byte[] bytecode, ConstantPoolMap constantPoolMap )
        throws MalformedTryStatementException,
               SymbolNotFoundException
    {
	tryBlock = null;
	catchBlocks = null;
	finallyBlock = null;
	loadFinallyBlock( exceptionTable, bytecode );
	loadCatchBlock( exceptionTable, constantPoolMap );
	loadTryBlock( exceptionTable );
    }

    /**
     * Returns the try block of the try statement.
     * @return try block of the try statement.
     */
    public TryBlock getTryBlock() {
	return tryBlock;
    }

    /**
     * Returns the catch blocks of the try statement or <code>null</code> if
     * the try statement has no catch blocks.
     * @return catch blocks of the try statement or <code>null</code> if the
     *         try statement has no catch blocks.
     */
    public CatchBlock[] getCatchBlocks() {
	return catchBlocks;
    }

    /**
     * Returns the finally block of the try statement or <code>null</code> if
     * the try statement has no finally block.
     * @return finally block of the try statement or <code>null</code> if
     *         the try statement has no finally block.
     */
    public FinallyBlock getFinallyBlock() {
	return finallyBlock;
    }


    /**
     * Loads the try block of the try statement.
     * @param exceptionTable exception-handler table for method.
     * @throws MalformedTryStatementException if try statement has neither
     *                                        catch nor finally blocks.
     */
    private void loadTryBlock( Vector exceptionTable )
        throws MalformedTryStatementException
    {
	int size = exceptionTable.size();
	
	// check if try statement is of the form try--finally
	if ( size == 0 ) {
	    if ( finallyBlock == null ) {
		throw new MalformedTryStatementException( "Try statement "+
                    "has neither catch blocks nor finally block" );
	    }
	    
	    //For Synchronized block, create synchronized try block
	    if(finallyBlock.isSynthetic()) {
		tryBlock = 
		    new SynchronizedTryBlockImpl (finallyBlock.rangeStartOffset,
					     finallyBlock.rangeEndOffset);
	    }else {
		tryBlock = new TryBlockImpl ( finallyBlock.rangeStartOffset,
				     finallyBlock.rangeEndOffset );
	    }
	    return;
	}
	for (Enumeration e = exceptionTable.elements (); e.hasMoreElements ();)
	{
	    ExceptionHandler handler = (ExceptionHandler) e.nextElement ();
	    if ( handler.getCatchType() != 0 )
	    {
		tryBlock = new TryBlockImpl (handler.getStartPC (), handler.getEndPC ());

		return;
	    }
	}
    }

    /**
     * Loads the finally block of the try statement.
     * @param exceptionTable exception-handler table for method.
     * @param bytecode bytecode instructions for the method.
     * @throws MalformedTryStatementException if finally block offsets cannot
     *                                        be parsed.
     */
    private void loadFinallyBlock (Vector exceptionTable, byte [] bytecode)
	throws MalformedTryStatementException
    {
	LinkedList toRemove = new LinkedList ();
	int size = exceptionTable.size();
	for (Enumeration e = exceptionTable.elements (); e.hasMoreElements ();)
	{
	    ExceptionHandler handler = (ExceptionHandler) e.nextElement ();
	    if ( handler.getCatchType() != 0 )
	    {
		continue;
	    }
	    finallyBlock = new FinallyBlockImpl ( handler, bytecode );
	    toRemove.add (handler);
	}
	
	for (Iterator i = toRemove.iterator (); i.hasNext ();)
	{
	    exceptionTable.remove (i.next ());
	}
    }

    /**
     * Loads catch blocks in the try statement.
     * @param exceptionTable exception-handler table for method.
     * @param constantPoolMap map from a constant-pool index to a symbol-table
     *                        object.
     * @throws SymbolNotFoundException if <code>constantPoolMap</code> does
     *                                 not map a catch type from
     *                                 <code>exceptionTable</code> to a
     *                                 <code>Class</code>.
     */
    private void loadCatchBlock( Vector exceptionTable,
				 ConstantPoolMap constantPoolMap )
        throws SymbolNotFoundException
    {
        catchBlocks = new CatchBlock [exceptionTable.size ()];
	int i = 0;
        for (Enumeration e = exceptionTable.elements (); e.hasMoreElements (); i++)
	{
	    ExceptionHandler handler = (ExceptionHandler) e.nextElement ();
            int catchType = handler.getCatchType();
            if ( catchType == 0 )  // skip finally block
	    {
		catchBlocks [i] = null;

                continue;
            }
            Integer key = new Integer( catchType );
            Object obj = constantPoolMap.get( key );
            if ( obj == null )
	    {
                throw new SymbolNotFoundException(
                    "ExceptionHandlers.initExceptionTypes(): constant pool "+
                    "index "+catchType+" for catch handler does not map to a"+
                    " SymbolTableEntry object." );
            }
            if ( !(obj instanceof Class) )
	    {
                throw new SymbolNotFoundException(
                    "ExceptionHandlers.initExceptionTypes(): constant pool "+
                    "index "+catchType+ " for catch handler does not map to"+
                    " expected Class type: "+obj.getClass().getName() );
            }
	    catchBlocks[i] = new CatchBlockImpl ( handler.getHandlerPC(),
					     (Class)obj );
        }
    }

    /**
     * Returns a string for a try statement.
     * @return String representationfor try statement.
     */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<trystatement>");

		buf.append (tryBlock);

		for (int i = 0; i < catchBlocks.length; i++)
		{
			buf.append (catchBlocks [i]);
		}

		if (finallyBlock != null)
		{
			buf.append (finallyBlock);
		}

		buf.append ("</trystatement>");

		return buf.toString ();
	}

    /** return whether this is a synchronized block 
     *@return true -- this is a Synchronized block, false -- normal try block
     */
    boolean isSynthetic() {
	return (tryBlock instanceof SynchronizedTryBlock);
    }
}
