/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

import jaba.graph.MethodGraph;
import jaba.graph.StatementNode;
import jaba.graph.Node;

import jaba.sym.Method;
import jaba.sym.MethodAttribute;
import jaba.sym.Class;
import jaba.sym.DemandDrivenAttribute;

/**
 * Represents a control-flow graph.
 *
 * @author S. Sinha
 * @author Huaxing wu -- <i> Revised 8/10/02. Chnage to use Primitve's
 *                  factory method valueOf to get Primitive object. </i>
 * @author S. Sinha -- <i> Revised 8/20/02 </i> Added a private vector to
 * store finally methods directly contained in the method for this CFG. These
 * are stored to avoid calling method.getFinallyMethods(), which can result
 * in an infinite loop (because method.getFinallyMethods() constructs
 * the CFG for the method before returning the array of finally methods).
 * @author Jay Lofstead 2003/05/29 created a toString that generates XML using the printGraph method.
 * 					updated printGraph to have a slightly better format.
 * @author Jay Lofstead 2003/06/02 removed elementAt in favor of enumeration/iteration for better performance.
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/12 added back in accidentially deleted line
 * @author Jay Lofstead 2003/06/16 added toStringRecuced (), toStringDetailed ()
 * @author Jay Lofstead 2003/06/20 changed to use the options from the program object
 * @author Jay Lofstead 2003/07/01 changed to be an interface
 */
public interface CFG extends MethodGraph, DemandDrivenAttribute, MethodAttribute
{
    // Methods

    /**
     * Returns the exceptional-exit nodes of this CFG.
     * @return Array of exceptional-exit nodes of this CFG. The length of
     *         the array is 0 if the CFG contains no exceptional-exit nodes.
     */
    public StatementNode [] getExceptionalExitNodes ();

    /**
     * Returns the number of exceptional-exit nodes in this CFG.
     * @return Number of exceptional-exit nodes that this CFG contains.
     */
    public int getExceptionalExitNodeCount();

    /**
     * Returns the runtime exceptions that are propagated by the
     * corresponding method. If this method is called after the CFG
     * construction but before the ICFG construction, the exceptions include
     * only those that are directly propagated by the method. Is this method
     * is called after the ICFG construction, the exceptions include
     * both directly and indirectly-propagated exceptions.
     * @return Array containing <code>String</code> fully-qualified
     *         (dot-separated) names of propagated runtime exceptions.
     */
    public String[] getPropagatedRuntimeExceptions();

    /**
     * Checks if an exception raised at a given bytecode offset propagates
     * out of the method. If it does, adds that exception to the
     * set of propagated implicit exceptions for the method.
     * @param offset bytecode offset at which exception is raised
     * @param exceptionName fully-qualified (dot-separated) name of exception
     *                      that is raised
     */
    public void checkExceptionPropagated( int offset, String exceptionName );

    /**
     * Determines if an exception raised at a given bytecode offset propagates
     * out of the method
     * @param offset bytecode offset at which exception is raised
     * @param exceptionName fully-qualified (dot-separated) name of exception
     *                      that is raised
     * @return boolean <code>true</code> if exception is propagated;
     *                 <code>false</code> otherwise
     */
    public boolean isExceptionPropagated( int offset, String exceptionName );

    /**
     * Returns the byte code offset of the last symbolic instruction in the
     * method corresponding to this CFG.
     * @return the byte code offset of the last symbolic instruction in the
     * method corresponding to this CFG.
     */
    public int getLastInstructionOffset();

    /**
     * Identifies the target node to which an intraprocedural edge
     * or an interprocedural (exceptional-return) edge is created.
     * The source node may be a throw-statement node (intraprocedural edge)
     * or an exceptional-exit node (interprocedural edge);
     * The type of the target node may be CATCH_NODE,
     * an EXCEPTIONAL_FINALLY_CALL_NODE,
     * or an EXCEPTIONAL_EXIT_NODE.
     * @param exceptionType type of exception raised at a throw statement or
     *                      a PNRC.
     * @param offset bytecode offset of the throw-statement node or the call
     *               node for the PNRC.
     * @return The target node to which the source node is to be connected.
     */
    public StatementNode getCatchNode (Class exceptionType, int offset);

    /**
     * Identifies the node to which a node for a goto instruction or
     * a finally-goto-exit node is connected.
     * @param targetOffset Target bytecode offset for the goto instruction or
     *                     the finally-goto-exit node.
     * @return The target node to which the finally-goto-exit node or the
     *         node for goto instruction is to be connected.
     */
    public StatementNode getGotoSuccessor( Integer targetOffset );

    /**
     * Identifies the node to which a node for a return instruction or
     * a finally-return-exit node is connected.
     * @param targetOffset Target bytecode offset for the goto instruction or
     *                     the goto-exit node.
     * @return The target node to which the goto-exit node or the node for
     *         goto instruction is to be connected.
     */
    public StatementNode getFinallyReturnSuccessor();

    /**
     * Identifies the node to which a node for a JSR instruction is connected.
     * @param targetOffset Target bytecode offset for the JRS instruction.
     * @return The target node to which the node for JSR instruction
     *         is to be connected.
     */
    public StatementNode getJsrSuccessor( Integer targetOffset );

    /**
     * Return an xml format of this cfg. It will first be node information, 
     * and at the end is the information about the entry information.The Node 
     * information  includes id, type, source line, successors. The entry info
     * only includes the entry node id. So the total length of the string will be 
     * the node number + 1.  
     * @return string array representation of this graph
     */ 
	public String printGraph ();

	/** returns an XML representation of this graph */
	public String toString ();

	/** returns the reduced representation of this object */
	public String toStringReduced ();

	/** returns the detailed representation of this object */
	public String toStringDetailed ();
}
