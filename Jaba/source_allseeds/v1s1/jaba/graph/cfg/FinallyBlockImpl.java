/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

import jaba.classfile.ExceptionHandler;
import jaba.constants.OpCode;

/**
 * Represents a finally block.
 * <p>
 * @author S. Sinha
 * @author Huaxing Wu -- <i> Revised 7/10/02, add getName() </i>
 * @author Jay Lofstead 2003/05/29 converted toString to return an XML format
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/12 removed redundant use of this for member access
 * @author Jay Lofstead 2003/07/10 changed to implement FinallyBlock
 */
public class FinallyBlockImpl implements java.io.Serializable, FinallyBlock
{
    /**
     * Bytecode offset that appears in the exception-table entry for the
     * finally block.
     */
    int jumpOffset;

    /**
     * Entry offset of finally block. <code>entryOffset</code> is the
     * bytecode offset at which code for a finally block begins
     * execution. The first statement saves the return address in
     * a temporary variable. This instruction is ignored during the
     * CFG construction; the first instruction following the instruction
     * at <code>entryOffset</code> is the instruction at
     * <code>cfgStartOffset</code>.
     */
    int entryOffset;

    /**
     * Offset of actual entry of finally block -- corresponds to the first
     * CFG node for that finally block. Bytecode entries
     * between <code>jumpOffset</code> and <code>cfgStartOffset</code>
     * are reset to OP_UNDEF because no instructions need be created
     * for them.
     */
    int cfgStartOffset;

    /**
     * Offset of the last instruction of finally block (is a <i>ret</i> or
     * a <i>wide</i> instruction).
     */
    int exitOffset;
    
    /**
     * Flag to indicate if finally is created to ensure proper monitor
     * release for a synchronized command. A CFG need not be built
     * for such finally blocks.
     */
    boolean isSynthetic;

    /** Name for finally block -- used at a call site to a finally block. */
    String name;

	/** ??? */
    int rangeStartOffset;

	/** ??? */
    int rangeEndOffset;

    /** bytecode instructions for method that contains the finally block */
    private byte[] bytecode;

    /**
     * Creates an object to represent a finally block.
     * @param handler exception-handler table entry for finally block.
     * @param bytecode bytecode instructions for the method in which the
     *                 finally block appears.
     * @throws MalformedTryStatementException if information about finally
     *                                        block cannot be parsed.
     */
    FinallyBlockImpl ( ExceptionHandler handler, byte[] bytecode )
        throws MalformedTryStatementException
    {
	this.bytecode = bytecode;
	initialize( handler );
    }

    /**
     * Initializes internal data structures that hold information about
     * finally block. The initalization requires parsing information in
     * the bytecode array to determine the values for
     * {@link FinallyBlock#jumpOffset jumpOffset},
     * {@link FinallyBlock#entryOffset entryOffset},
     * {@link FinallyBlock#cfgStartOffset cfgStartOffset},
     * {@link FinallyBlock#exitOffset exitOffset},
     * and identifying a synthetic finally block.
     * @throws MalformedTryStatementException if information is unable to be
     *                                        parsed.
     */
    private void initialize( ExceptionHandler handler )
        throws MalformedTryStatementException
    {
	int handlerPC = handler.getHandlerPC();
	jumpOffset = handlerPC;
	rangeStartOffset = handler.getStartPC();
	rangeEndOffset = handler.getEndPC();

	// The instruction at target of finally must be an astore
	// (to save the pending exception), unless the finally is
	// synthetic -- it is created for a synchronized command
	
	// The instruction at target of finally can be one of three:
	// 1. astore, to save the pending exception, for a finally block
	//    that does not end abruptly unconditionally
	// 2. pop, to discard the pending exception on the stack, for a
	//    finally block that ends abruptly unconditionally (Java
	//    does not stack up pending abrupt completions)
	// 3. aload, to load an release the monitor, for a synthetic
	//    finally, which is added by the compiler to ensure that the
	//    monitor is released even if an unhandled exception is
	//    raised within a synchronized block 
	// 4. astore, to save the pending exception. Then
	//    aload, to load and release the monitor, for a synthetic
	//    finally, which is added by the compiler to ensure that the
	//    monitor is released even if an unhandled exception is
	//    raised within a synchronized block 

	int endPC=handlerPC;
	try {
	    // check for case 1.
	    endPC = skipAstoreInstruction( handlerPC );
	    isSynthetic = false;
	}
	catch ( MalformedTryStatementException mts ) {

	    // control reaches here if case 2. or case 3. holds for the
	    // finally block

	    try {

		// check for case 3.
		endPC = skipAloadInstruction( endPC );
		
		// Control reaches here for synthetic finally blocks. Such
		// (synthetic) finally blocks contain three instructions,
		// the first of which is an aload instruction

		// second instruction must be a monitorexit
		if ( bytecode[endPC++] != OpCode.OP_MONITOREXIT ) {
		    throw new MalformedTryStatementException(
			"ExceptionHandlers.initFinallyBlocks(): "+
			"Expected monitorexit at bytecode offset: "+
			(endPC-1) );

		}
		// final instruction must be an athrow
		if ( bytecode[endPC] != OpCode.OP_ATHROW ) {
			throw new MalformedTryStatementException(
			    "ExceptionHandlers.initFinallyBlocks(): "+
			    "Expected athrow at bytecode offset: "+ endPC );
		}
		isSynthetic = true;
		entryOffset = jumpOffset;
		cfgStartOffset = jumpOffset;
		exitOffset = endPC;
		name = new String( "Finally_"+ entryOffset );
		return;
	    }
	    catch ( MalformedTryStatementException m ) {
		// control reaches here for case 2. or for some unknown
		// case; we cannot handle either of such cases currently
		String msg = "Found ";
		if ( bytecode[endPC] == OpCode.OP_POP ) {
		    msg += "finally block with unconditional abrupt "+
			"completion ";
		}
		else {
		    msg += "unknown finally block ";
		}
		msg += "starting at offset "+endPC+"\nExiting....\n";
		throw new MalformedTryStatementException( msg );
	    }
	}

	// instruction following the store at finally target can be two cases:
	// (1) This finally block is generated by compiler for synichronized block
	// It should be an aload following an monitorexit
	// (2) A real finally block 
	// It must be jsr or a jsr_w; calculate the branch address for either
	// instruction; 
	// 
	// If instruction is neither, throw a
	// MalformedTryStatementException

	try {
	    endPC = skipAloadInstruction(endPC);
	}catch (MalformedTryStatementException mts) {
	    // Control reaches here for a normal finally blocks, Should follow 
	    // a jsr or jsr_w
	    int startAddr;
	    if ( bytecode[endPC] == OpCode.OP_JSR ) {
		startAddr =
		    ((bytecode[endPC+1] << 8) |
		     (bytecode[endPC+2] & 0x000000ff)) + endPC;
	    }
	    else if ( bytecode[endPC] == OpCode.OP_JSR_W ) {
		startAddr =
		    ((bytecode[endPC+1] << 24) |
		     ((bytecode[endPC+2] << 16) & 0x00ff0000) |
		     ((bytecode[endPC+3] << 8) & 0x00ff0000) |
		     (bytecode[endPC+4] & 0x000000ff)) + endPC;
	    }
	    else {
		throw new MalformedTryStatementException(
							 "ExceptionHandlers.initFinallyBlocks(): "+
							 "Expected jsr or jsr_w at endPC offset for finally: "+endPC );
	    }
	
	    // instruction at startAddr must be an astore or wide (and astore);
	    // reset this instruction because it simply saves the return
	    // address

	    entryOffset = startAddr;
	    cfgStartOffset = skipAstoreInstruction( startAddr );
	    name = new String( "Finally_"+ entryOffset );
	    // exit offset is set by ExceptionHandlers
	    // exitOffset = getExitOffset( bytecode, entryOffset );
	    return;
	}

	 
	// Control reaches here for synthetic finally blocks. Such
	// (synthetic) finally blocks contain five instructions,
	// astore to store the exception
	// aload to load the monitor
	// monitorexit to release the monitor
	// aload to load the exception
	// athrow to throw the exception

	// The next instruction must be a monitorexit
	if ( bytecode[endPC++] != OpCode.OP_MONITOREXIT ) {
	    throw new MalformedTryStatementException(
						     "ExceptionHandlers.initFinallyBlocks(): "+
						     "Expected monitorexit at bytecode offset: "+
						     (endPC-1) );
	    
	}

	// The next should be an aload
	endPC = skipAloadInstruction(endPC);
	
	// final instruction must be an athrow
	if ( bytecode[endPC] != OpCode.OP_ATHROW ) {
	    throw new MalformedTryStatementException(
						     "ExceptionHandlers.initFinallyBlocks(): "+
						     "Expected athrow at bytecode offset: "+ endPC );
	}
	isSynthetic = true;
	entryOffset = jumpOffset;
	cfgStartOffset = jumpOffset;
	exitOffset = endPC;
	name = new String( "Finally_"+ entryOffset );
	return;
    }

    /**
     * Skips an <i>aload</i> or an <i>aload</i> instruction
     * (or their <i>wide</i> variants). 
     * @param offset bytecode offset for the beginning of the load instruction.
     * @return Bytecode offset of the instruction that follows the load
     *         instruction.
     * @throws MalformedTryStatementException if the instruction that begins
     *                                        at <code>offset</code> is not
     *                                        an <i>aload</i> instruction or
     *                                        its variants.
     */
    private int skipAloadInstruction( int offset )
                throws MalformedTryStatementException
    {
        if ( bytecode[offset] == OpCode.OP_ALOAD_0 ||
             bytecode[offset] == OpCode.OP_ALOAD_1 ||
             bytecode[offset] == OpCode.OP_ALOAD_2 ||
             bytecode[offset] == OpCode.OP_ALOAD_3 ) {
            return offset+1;
        }
        else if ( bytecode[offset] == OpCode.OP_ALOAD ) {
            return offset+2;
        }
        else if ( bytecode[offset] == OpCode.OP_WIDE &&
		  bytecode[offset+1] == OpCode.OP_ALOAD ) {
            return offset+4;
        }
        else {
            throw new MalformedTryStatementException(
                "ExceptionHandlers.skipLoadInstruction(): "+
                "Expected aload_n, aload, wide at bytecode offset: "+
                 offset );
        }
    }

    /**
     * Skips an <i>astore</i> or an <i>astore_n</i> instruction
     * (or their <i>wide</i> variants).
     * @param offset bytecode offset for the beginning of the store instruction.
     * @return Bytecode offset of the instruction that follows the store
     *         instruction.
     * @throws MalformedTryStatementException if the instruction that begins
     *                                        at <code>offset</code> is not
     *                                        an <i>astore</i> instruction or
     *                                        its variants.
     */
    private int skipAstoreInstruction( int offset )
                throws MalformedTryStatementException
    {
        if ( bytecode[offset] == OpCode.OP_ASTORE_0 ||
             bytecode[offset] == OpCode.OP_ASTORE_1 ||
             bytecode[offset] == OpCode.OP_ASTORE_2 ||
             bytecode[offset] == OpCode.OP_ASTORE_3 ) {
            return offset+1;
        }
        else if ( bytecode[offset] == OpCode.OP_ASTORE ) {
            return offset+2;
        }
        else if ( bytecode[offset] == OpCode.OP_WIDE &&
		  bytecode[offset+1] == OpCode.OP_ASTORE ) {
            return offset+4;
        }
        else {
            throw new MalformedTryStatementException(
                "ExceptionHandlers.skipStoreInstruction(): "+
                "Expected astore_n, astore, wide at bytecode offset: "+
                 offset );
        }
    }

    /**
     * Returns the jump offset for the finally block. The jump offset is
     * the bytecode offset that appears in the exception-table entry for the
     * finally block. This offset is used only when a finally block executes in
     * the exceptional context: the instructions at the jump offset save the
     * pending exception and rethrow it after the execution of the finally
     * block.
     * @return Jump offset for the finally block.
     */
    public int getJumpOffset() {
	return jumpOffset;
    }

    /**
     * Returns the entry offset for the finally block. The entry offset is
     * the bytecode offset at which code for a finally block begins execution.
     * The first statement at the entry offset saves the return address in a
     * temporary variable.This instruction is ignored during the
     * CFG construction; the first instruction following the instruction
     * at <code>entryOffset</code> is the instruction at
     * <code>cfgStartOffset</code>.
     * @return entry offset for the finally block.
     */
    public int getEntryOffset() {
	return entryOffset;
    }

    /**
     * Return the offset of the first instruction of the finally block for
     * which a node is created in the CFG. This instruction is the first
     * instruction after the entry offset for the finally block.
     * @return CFG start offset for the finally block.
     */
    public int getCfgStartOffset() {
	return cfgStartOffset;
    }

    /**
     * Returns a string for a finally block.
     * @return String representation for finally block.
     */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<finallyblock>");

		buf.append ("<jumpoffset>").append (jumpOffset).append ("</jumpoffset>");
		buf.append ("<entryoffset>").append (entryOffset).append ("</entryoffset>");
		buf.append ("<cfgstartoffset>").append (cfgStartOffset).append ("</cfgstartoffset>");
		buf.append ("<exitoffset>").append (exitOffset).append ("</exitoffset>");
		buf.append ("<issynthetic>").append (isSynthetic).append ("</issynthetic>");
		buf.append ("<name>").append (name).append ("</name>");

		buf.append ("</finallyblock>");

		return buf.toString ();
	}

    /**
     * Returns finallyblock's name.
     * @return name of the finallyblock 
     */
    public String getName() {
	return name;
    }

    /** Return whether this is a synchronized finally block
     * @return true -- Synchronized block, false -- other kinds of finally
     */
    public boolean isSynthetic() {
	return isSynthetic;
    }
}
