/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

/**
 *  Jay Lofstead 2003-05-19 changed constructor to public to
 *                          allow access in jaba.tools.graph.GDG
 */
public class MalformedTryStatementException extends Exception {

    public MalformedTryStatementException( String msg ) {
        super( msg );
    }

}
