/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

/**
 * Represents a try block.
 * <p>
 * @author S. Sinha
 * @author Jay Lofstead 2003/05/29 converted toString to return an XML format
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface TryBlock extends java.io.Serializable
{
    /**
     * Returns the bytecode offset of the first instruction of the try
     * block.
     * @return Bytecode offset of the first instruction of the try
     *         block.
     */
    public int getStartByteCodeOffset();

    /**
     * Returns the bytecode offset of the last instruction of the try
     * block.
     * @return Bytecode offset of the last instruction of the try
     *         block.
     */
    public int getEndByteCodeOffset();

    /**
     * Returns a string for a try block.
     * @return String representation for try block.
     */
	public String toString ();
}
