/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

import jaba.constants.OpCode;

/**
 * Represents a finally block.
 * <p>
 * @author S. Sinha
 * @author Huaxing Wu -- <i> Revised 7/10/02, add getName() </i>
 * @author Jay Lofstead 2003/05/29 converted toString to return an XML format
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/12 removed redundant use of this for member access
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface FinallyBlock extends java.io.Serializable
{
    /**
     * Returns the jump offset for the finally block. The jump offset is
     * the bytecode offset that appears in the exception-table entry for the
     * finally block. This offset is used only when a finally block executes in
     * the exceptional context: the instructions at the jump offset save the
     * pending exception and rethrow it after the execution of the finally
     * block.
     * @return Jump offset for the finally block.
     */
    public int getJumpOffset();

    /**
     * Returns the entry offset for the finally block. The entry offset is
     * the bytecode offset at which code for a finally block begins execution.
     * The first statement at the entry offset saves the return address in a
     * temporary variable.This instruction is ignored during the
     * CFG construction; the first instruction following the instruction
     * at <code>entryOffset</code> is the instruction at
     * <code>cfgStartOffset</code>.
     * @return entry offset for the finally block.
     */
    public int getEntryOffset();

    /**
     * Return the offset of the first instruction of the finally block for
     * which a node is created in the CFG. This instruction is the first
     * instruction after the entry offset for the finally block.
     * @return CFG start offset for the finally block.
     */
    public int getCfgStartOffset();

    /**
     * Returns a string for a finally block.
     * @return String representation for finally block.
     */
	public String toString ();

    /**
     * Returns finallyblock's name.
     * @return name of the finallyblock 
     */
    public String getName();

    /**
     * Return whether this is a synchronized finally block
     * @return true -- Synchronized block, false -- other kinds of finally
     */
    public boolean isSynthetic();
}
