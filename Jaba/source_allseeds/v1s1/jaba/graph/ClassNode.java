package jaba.graph;

import jaba.sym.Class;
import jaba.sym.NamedReferenceType;

/**
 * A <code>ClassNode </code> is an upper level representation of a class
 * in a program. A <code>ClassNode</code> is a child of Node, and Nodes are used
 * to create Edges. ClassNodes, InterfaceNodes, and Edges are used in
 * ClassHierarchyGraph to create a graph. In a ClassHierarchyGraph, ClassNodes are
 * represented as a gray ellipse
 *
 * @author Caleb Ho -- <i>Created 5/2001</i>
 * @author Huaxing Wu -- <i> Add method getColor, getShape, getNodeTypeName </i>
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/04 added call to getString () in toString ()
 * @author Jay Lofstead 2003/06/06 changed XML type to just node for ease of manipulation
 * @author Jay Lofstead 2003/06/25 added implements serializable to support RMI
 */
public class ClassNode extends TypeNode implements java.io.Serializable
{
    // Fields
    /**
     * Used to tabulate the number of ClassNodes created for a particular
     * ClassHierarchyGraph.
     */
   protected static int TOTAL_CLASS_NODES = 0;

   /** A reference to the Class object this ClassNode represents */
   private Class classReference;

   /** 
    * classNodeNumber stores the value of the static variable
    * TOTAL_CLASS_NODES when this ClassNode is initially instantiated.
    * It is used to provide each node with a numeric id, and does not
    * imply ordering of the nodes, because their order is generated randomly
    */	
   private int classNodeNumber;

  // Methods

  /**
   * Constructor for a class node. It creates a new class node and 
   * instantiates the necessary variables. 
   * @param c : the Class object this ClassNode will store and represent
   */
   public ClassNode ( Class c )
   {
	classReference = c;
	setTypeReference((NamedReferenceType) classReference);
	classNodeNumber = TOTAL_CLASS_NODES++;
	typeOf = TypeNode.CLASS_NODE;
   }

   /**
    * returns the reference to the Class object that this ClassNode was created for
    */
   public Class getClassReference()
   {
	return (classReference);
   }

   /**
    * Returns the classNodeNumber, a numeric id used only to track the nodes for analysis,
    * it in no way implies the ordering or importance of classes
    */
   public int getClassNodeNumber()
   {
	return (classNodeNumber);
   }

   /**
    * Returns the name of the Class object this ClassNode is storing
    */
   public String toString()
   {
	return "<node>" + getString () + "<name>" + classReference.getName () + "</name></node>";
   }

    /**
     * Returns the type name of this object.
     * @return the name of the type
     */
    public String getNodeTypeName() { return "Class"; }

    /**
     * Returns the shape of this object.
     * @return the shape of the type
     */
    public String getShape() { return "ellipse";}

    /**
     * Returns the color of this object.
     * @return the color of the type
     */
    public String getColor() { return "darkorchid"; }
}
