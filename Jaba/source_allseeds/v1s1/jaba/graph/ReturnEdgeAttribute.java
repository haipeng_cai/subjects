/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

/**
 * Attribute found on edges returning from a called method to the calling
 * method that specifies the call site node that causes the called method to
 * be called.
 * @author Jim Jones -- <i>Created, May 17, 1999</i>.
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/04 removed private node number in favor of one at Node
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface ReturnEdgeAttribute extends EdgeAttribute
{
  /**
   * Adds a call node to this attribute.
   * @param callNode  The call node to add.
   */
  public void addCallNode( StatementNode callNode );

  /**
   * Returns all call nodes that result in this return edge returning.
   * @return  All call nodes that result in this return edge returning.
   */
  public StatementNode[] getCallNodes();

  /**
   * Returns a string representation of this attribute.
   * @return A string representation of this attribute.
   */
  public String toString();
}
