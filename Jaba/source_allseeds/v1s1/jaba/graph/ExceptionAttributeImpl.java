/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Class;

/**
 * Attribute that needs to be stored with a throw or catch site node.
 * @author Jay Lofstead -- 2003/05/29 changed toString to generate XML
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference a member
 * @author Jay Lofstead 2003/07/10 changed to implement ExceptionAttribute
 */
public class ExceptionAttributeImpl implements ExceptionAttribute, StatementNodeAttribute
{
  /** Exception being thrown or caught. */
  private Class exceptionType;


  /**
   * Constructor.
   */
  public ExceptionAttributeImpl ()
    {
      exceptionType = null;

    }


  /**
   * Assigns the exception being thrown or caught at this node.
   * @param exception  The exception being thrown or caught at this node.
   */
  public void setExceptionType( Class exception )
  throws IllegalArgumentException
    {
      if ( exception == null )
	throw new IllegalArgumentException( "jaba.graph." +
					    "ExceptionAttribute." +
					    "setException( null )" );

      exceptionType = exception;

    }


  /**
   * Returns the exception being thrown or caught at this node.
   * @return  The exception being thrown or caught at this node.
   */
  public Class getExceptionType()
    {
      return exceptionType;

    }

    /**
     * Returns a string representation of an exception attribute.
     * @return String representation of exception attribute.
     */
    public String toString()
	{
		return "<exceptionattribute><class>" + exceptionType.getName () + "</class></exceptionattribute>";
    }
}
