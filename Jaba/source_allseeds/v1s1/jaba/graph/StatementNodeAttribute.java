/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

/**
 * Attribute of a statement node.  All statement level node attributes
 * inherit from this class.
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface StatementNodeAttribute extends NodeAttribute
{
    public abstract String toString ();
}
