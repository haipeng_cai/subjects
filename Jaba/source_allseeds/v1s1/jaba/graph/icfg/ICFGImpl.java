package jaba.graph.icfg;

import java.util.List;
import java.util.LinkedList;
import java.util.Vector;
import java.util.Iterator;
import java.util.Collection;

import jaba.graph.Graph;
import jaba.graph.ProgramGraph;
import jaba.graph.ProgramGraphImpl;
import jaba.graph.StatementNode;
import jaba.graph.MethodCallEdgeAttribute;
import jaba.graph.MethodCallEdgeAttributeImpl;
import jaba.graph.Edge;
import jaba.graph.EdgeImpl;
import jaba.graph.Node;
import jaba.graph.EdgeAttribute;
import jaba.graph.EdgeAttributeImpl;
import jaba.graph.MethodCallAttribute;
import jaba.graph.ReturnEdgeAttribute;
import jaba.graph.ReturnEdgeAttributeImpl;
import jaba.graph.ReturnSitesAttribute;
import jaba.graph.ReturnSitesAttributeImpl;
import jaba.graph.FinallyTransferAttribute;
import jaba.graph.FinallyCallAttribute;
import jaba.graph.FinallyContextAttribute;
import jaba.graph.FinallyContextAttributeImpl;
import jaba.graph.ExceptionAttribute;

import jaba.graph.cfg.CFG;
import jaba.graph.cfg.CFGImpl;

import jaba.sym.Program;
import jaba.sym.NamedReferenceType;
import jaba.sym.Class;
import jaba.sym.Method;
import jaba.sym.DemandDrivenAttribute;
import jaba.sym.ProgramAttribute;

import jaba.constants.Modifier;

import jaba.main.Options;

import jaba.debug.Debug;

import jaba.tools.local.Factory;

/**
 * Intermethod control-flow graph.
 * @author Jim Jones -- <i>Mar. 1, 1999.  Created.</i>
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/16 added toStringReduced (), toStringDetailed ()
 * @author Jay Lofstead 2003/06/20 changed to use the options from the program rather than global
 * @author Jay Lofstead 2003/07/01 changed to implement ICFG
 * @author Jay Lofstead 2003/07/17 removed legacy load () replaced with static load
 * @author Jay Lofstead 2003/07/17 changed to use Factory to get attributes
 */
public class ICFGImpl extends ProgramGraphImpl implements DemandDrivenAttribute, Modifier, ICFG
{
    /**
     * Constructor.
     * @param program  Program for which this graph describes.
     */
    private ICFGImpl (Program program)
    {
	super (program);
    }
    
    /**
     * Loads the ICFG.
     * @param program  Program object for which to build its ICFG.
     */
    public static ProgramAttribute load (Program p)
    {
	ICFGImpl icfg = new ICFGImpl (p);

	// copy all CFGs into the ICFG (except for the edges from call node to
	// return node of non-library methods)
	icfg.initializeICFG ();
	
	// create a worklist for methods that need to be processed.
	LinkedList worklist = new LinkedList ();
	
	// initialize worklist with all methods (and submethods)
	icfg.initializeWorklist (worklist);
	
	// while more methods on the worklist
	while (!worklist.isEmpty ())
	{
	    // take a method off of the worklist
	    Method calledMethod = (Method) worklist.removeFirst ();
	    CFG calledCFG = (CFG) CFGImpl.load (calledMethod);
	    
	    if (calledCFG == null)
	    {
		System.err.println ("WARNING: ICFG.load: CFG not available for: "
				+ calledMethod.getContainingType ().getName ()
				+ "." + calledMethod.getSignature ()
				);
		continue;
	    }

	    // for each method that calls calledMethod...
	    Collection callingMethods = calledMethod.getCallingMethodsCollection ();
	    for (Iterator i = callingMethods.iterator (); i.hasNext ();)
	    {
		Method callingMethod = (Method) i.next ();
		icfg.processCallingMethods (callingMethod, calledMethod, calledCFG, worklist);
	    }

	    Method finallyCallingMethod = calledMethod.getContainingMethod ();
	    if (finallyCallingMethod != null)
	    {
		icfg.processCallingMethods ((jaba.sym.Method) finallyCallingMethod, calledMethod, calledCFG, worklist);
	    }
	}
	
	// add this ICFG as an attribute to the program
	Program program = icfg.getProgram ();
	program.addAttribute (icfg);
	
	// Uncomment next block to get "Term's bug"
/*
	// remove the ConstantPoolMap attribute from all classes and interfaces
	NamedReferenceType [] nrts = program.getNamedReferenceTypes ();

 	for (int i = 0; i < nrts.length; i++)
	{
 	    nrts [i].removeAttributeOfType ("jaba.sym.ConstantPoolMap");
 	}
*/

	return icfg;
    }
    
    /**
     * Returns an XML string representation of this ICFG.
     * @return A string representation of this ICFG.
     */
	public String toString ()
	{
		return "<icfg>" + getString () + "</icfg>";
	}

	/** returns the reduced representation of this object */
	public String toStringReduced ()
	{
		return "<icfg>" + getStringReduced () + "</icfg>";
	}

	/** returns the detailed representation of this object */
	public String toStringDetailed ()
	{
		return "<icfg>" + getStringDetailed () + "</icfg>";
	}

	/** ??? */
    private void processCallingMethods( Method callingMethod, 
					Method calledMethod, CFG calledCFG,
					LinkedList worklist )
    {
	CFG callingCFG = Factory.getCFG (callingMethod);
	
	// for each call site in the calling method to the called method...
	Collection callSites = callingMethod.getCallSiteNodesCollection ();
	for (Iterator k = callSites.iterator (); k.hasNext ();)
	{
	    StatementNode callSite = (StatementNode) k.next ();
	    processCallSites( callSite, calledMethod, calledCFG, worklist,
			      callingMethod, callingCFG );
	}
	Collection finallyCallSites = callingMethod.getFinallyCallSiteNodesCollection ();
	for (Iterator k = finallyCallSites.iterator (); k.hasNext ();)
	{
	    StatementNode finallyCallSite = (StatementNode) k.next ();
	    processCallSites( finallyCallSite, calledMethod, calledCFG,
			      worklist, callingMethod, callingCFG );
	}
	
    }

	/** ??? */
    private void processCallSites( StatementNode callSite, 
				   Method calledMethod, 
				   CFG calledCFG, LinkedList worklist, 
				   Method callingMethod, CFG callingCFG )
    {
	// if this is a call to java.lang.System.exit(), we do not consider
	// this a call site, but as a halt
	if ( callSite.getType() == StatementNode.HALT_NODE ) return;
	
	if ( ( callSite.getType() != StatementNode.STATIC_METHOD_CALL_NODE ) &&
             ( callSite.getType() != StatementNode.VIRTUAL_METHOD_CALL_NODE )&&
	     ( callSite.getType() != 
	       StatementNode.NORMAL_FINALLY_CALL_NODE ) &&
	     ( callSite.getType() != 
	       StatementNode.EXCEPTIONAL_FINALLY_CALL_NODE ) ) {
	    RuntimeException e = new RuntimeException();
	    e.printStackTrace();
	    throw e;
	}
	
	// make sure that this call site calls the calledMethod, if not, 
	// return.  also, get return node for this call site
	StatementNode returnNode = null;
	if ( ( callSite.getType() == 
	       StatementNode.NORMAL_FINALLY_CALL_NODE ) ||
	     ( callSite.getType() == 
	       StatementNode.EXCEPTIONAL_FINALLY_CALL_NODE ) ) {
	    FinallyCallAttribute finallyCallAttr = null;
	    finallyCallAttr = Factory.getFinallyCallAttribute (callSite);
	    
	    if ( !finallyCallAttr.getName().equals( calledMethod.getName() ) )
		return;
	    returnNode = finallyCallAttr.getReturnNode();
	    
	    // this may be moved to CFG in the future
	    finallyCallAttr.setMethod( calledMethod );
	}
	else { // call site type is method call
	    MethodCallAttribute callSiteAttr = null;
	    callSiteAttr = Factory.getMethodCallAttribute (callSite);
	    
	    boolean callsCalledMethod = false;
	    Vector callSiteCalledMethods = callSiteAttr.getMethodsVector ();
	    for (Iterator l = callSiteCalledMethods.iterator (); l.hasNext ();)
	    {
		if (((Method) l.next ()) == calledMethod) {
		    callsCalledMethod = true;
		    break;
		}
	    }
	    
	    if ( !callsCalledMethod ) return;
	    returnNode = callSiteAttr.getReturnNode();
	}
	
	// get the return site attribute for the call site if one exists, if 
	// one does not exist, create one
	ReturnSitesAttribute returnSitesAttr = null;
	returnSitesAttr = Factory.getReturnSitesAttribute (callSite);

	if ( returnSitesAttr == null ) {
	    returnSitesAttr = new ReturnSitesAttributeImpl ();
	    callSite.addAttribute( returnSitesAttr );
	}

	// update proapagted runtime exceptions for calling method
	// based on propagated runtime exceptions for called method
	if (getProgram ().getOptions ().getAnalyzeRuntimeException ())
	{
	    String[] calledPropExcp = calledCFG.getPropagatedRuntimeExceptions();
	    for ( int j = 0; j < calledPropExcp.length; j++ ) {
	    callingCFG.checkExceptionPropagated( callSite.getByteCodeOffset(),
						 calledPropExcp[j] );
	    }
	}

	// for each exceptional exit in the called method...
	Node[] exitNodes = calledCFG.getExitNodes();

	for ( int j = 0; j < exitNodes.length; j++ ) {
	    StatementNode exitNode = (StatementNode)exitNodes[j];
	    int exitNodeType = exitNode.getType();

	    // process finally-goto and finally-return exit nodes
	    if ( exitNodeType == StatementNode.FINALLY_GOTO_EXIT_NODE ||
		 exitNodeType == StatementNode.FINALLY_RETURN_EXIT_NODE ) {
		StatementNode targetNode;
		// get the target node for either case
		if ( exitNodeType == StatementNode.FINALLY_GOTO_EXIT_NODE ) {
		    FinallyTransferAttribute attr = Factory.getFinallyTransferAttribute (exitNode);
		    // get the target node for the interprocedural edge
		    targetNode = (jaba.graph.StatementNode) callingCFG.getGotoSuccessor(
                        new Integer( attr.getBytecodeOffset() ) );
		}
		else {
		    targetNode = (jaba.graph.StatementNode) callingCFG.getFinallyReturnSuccessor(); 
		}

		// check if an edge exists between the two nodes; if not,
		// create an edge and add it to the ICFG
		Edge returnEdge = null;
		if ( containsEdgeLike( exitNode, targetNode ) ) {
		    Edge [] outEdges = getOutEdges (exitNode);
		    for ( int k = 0; k < outEdges.length; k++ ) {
			if ( outEdges[k].getSink() == targetNode ) {
			    returnEdge = (jaba.graph.Edge) outEdges[k];
			    break;
			}
		    }
		}
		else {
		    returnEdge = new EdgeImpl ( exitNode, targetNode );
		    addEdge( returnEdge );
		}

		// put an attribute on the returnEdge that identifies
		// the call site that causes this edge to be executed.
		ReturnEdgeAttribute edgeAttr = null;
		edgeAttr = Factory.getReturnEdgeAttribute (returnEdge);
	    
		if ( edgeAttr == null ) {
		    edgeAttr = new ReturnEdgeAttributeImpl ();
		    returnEdge.addAttribute( edgeAttr );
		}
		edgeAttr.addCallNode( callSite );
	    
		// add the targetNode to the return sites attribute for
		// the call site
		returnSitesAttr.addReturnSite( targetNode );

		// process the next exit node
		continue; 
	    }

	    if ( exitNodeType !=  StatementNode.EXCEPTIONAL_EXIT_NODE ) {
		continue;
	    }

	    ExceptionAttribute excAttr = null;
	    excAttr = Factory.getExceptionAttribute (exitNode);
	    
	    Class exceptionType = excAttr.getExceptionType();
	    
	    // get the appropriate catch handler node for the
	    // exceptional exit
	    int exceptionalExitCount = 
		callingCFG.getExceptionalExitNodeCount();

	    StatementNode catchHandlerNode;
	    if ( callSite.getType() ==
		 StatementNode.NORMAL_FINALLY_CALL_NODE ||
		 callSite.getType() ==
		 StatementNode.EXCEPTIONAL_FINALLY_CALL_NODE ) {

		int lastInstrOffset = calledCFG.getLastInstructionOffset();
		catchHandlerNode = (jaba.graph.StatementNode) 
		    callingCFG.getCatchNode( exceptionType, lastInstrOffset );
	    }
	    else {
		catchHandlerNode = (jaba.graph.StatementNode) 
		    callingCFG.getCatchNode( exceptionType, 
					     callSite.getByteCodeOffset() );
	    }
	    boolean createdNewExceptionalExit = 
		callingCFG.getExceptionalExitNodeCount() != 
		exceptionalExitCount;
	    
	    // check if the catchHandlerNode is already in the ICFG
	    boolean newCatchCreated = true;
	    try {
		Edge [] tempEdges = getInEdges (catchHandlerNode);
		if ( tempEdges.length == 0 )
		    newCatchCreated = true;
		else newCatchCreated = false;
	    } catch ( IllegalArgumentException e ) {
		newCatchCreated = true;
	    }
	    
	    // create an edge from the exceptional exit to it
	    // associated catch handler
	    Edge exceptionalReturnEdge = 
		new EdgeImpl ( exitNode, catchHandlerNode );
	    boolean containsEdgeAlready = 
		containsEdgeLike( exitNode, catchHandlerNode );
	    if ( !containsEdgeAlready )
		addEdge( exceptionalReturnEdge );
	    
	    // if the edge was already there, we need to get the reference to
	    // that edge so that we can associate or update the 
	    // ReturnEdgeAttribute.
	    if ( containsEdgeAlready ) {
		Edge [] outEdges = getOutEdges (exitNode);
		for ( int k = 0; k < outEdges.length; k++ ) {
		    if ( outEdges[k].getSink() == catchHandlerNode ) {
			exceptionalReturnEdge = (jaba.graph.Edge) outEdges[k];
			break;
		    }
		}
	    }
	    
	    // put an attribute on the exceptionalReturnEdge that identifies
	    // the call site that causes this edge to be executed.
	    ReturnEdgeAttribute edgeAttr = null;
	    edgeAttr = Factory.getReturnEdgeAttribute (exceptionalReturnEdge);
	    
	    if ( edgeAttr == null ) {
		edgeAttr = new ReturnEdgeAttributeImpl ();
		exceptionalReturnEdge.addAttribute( edgeAttr );
	    }
	    edgeAttr.addCallNode( callSite );
	    
	    // add the catchHandlerNode to the return sites attribute for the
	    // call site
	    returnSitesAttr.addReturnSite( catchHandlerNode );
	    
	    // if catchHandlerNode node is a finally-start node (used in
	    // inlined finally representations), add a finally context
	    // attribute to the return edge; finally-context attributes
	    // are attached to inedges to finally-start node and outedges
	    // from finally-end nodes (created in the CFG)
	    if ( catchHandlerNode.getType() ==
		 StatementNode.FINALLY_START_NODE ) {
		FinallyContextAttribute attr = null;
		attr = Factory.getFinallyContextAttribute (exceptionalReturnEdge);
		if ( attr == null ) {
		    attr = new FinallyContextAttributeImpl ();
		    exceptionalReturnEdge.addAttribute( attr );
		}
		attr.addExceptionType( exceptionType );
	    }

	    // if the catchHandlerNode is a finally call node, we need to walk
	    // from it all the way to the exceptional exit, inserting nodes and
	    // edges along the way
	    if ( ( catchHandlerNode.getType() == 
		   StatementNode.EXCEPTIONAL_FINALLY_CALL_NODE ) &&
		 newCatchCreated ) {
		StatementNode currentNode = catchHandlerNode;
		while ( ( currentNode.getType() != 
			  StatementNode.EXCEPTIONAL_EXIT_NODE ) &&
			( currentNode.getType() !=
			  StatementNode.CATCH_NODE ) ) {
		    Edge [] outEdges = callingCFG.getOutEdges (currentNode);
		    if ( outEdges.length != 1 ) {
			RuntimeException e = new RuntimeException();
			e.printStackTrace();
			throw e;
		    }
		    StatementNode nextNode = 
			(StatementNode)outEdges[0].getSink();
		    
		    // do not want to connect a finally call to its finally
		    // return
		    if ( currentNode.getType() != 
			 StatementNode.EXCEPTIONAL_FINALLY_CALL_NODE ) {
			// if nextNode is an exceptional-finally call node that
			// already has predecessors in the ICFG, do not
			// traverse down the path (until a catch or an
			// exceptional exit node) because the path starting
			// at nextNode has already been traversed. This
			// occurs when an exceptional-finally call node is
			// a join point; the subpath from the join onwards
			// need not be traversed multiple times
			// -- Sinha 05/20/02
			boolean traverseRemainingSubpath = true;
			try {
			    if ( nextNode.getType() == 
				 StatementNode.EXCEPTIONAL_FINALLY_CALL_NODE &&
				 getInEdges( nextNode ).length != 0 ) {
				traverseRemainingSubpath = false;
			    }
			}
			catch ( IllegalArgumentException e ) {
			    traverseRemainingSubpath = true;
			}
		       
			if ( !containsEdgeLike( currentNode, nextNode ) ) {
			    addEdge ((jaba.graph.Edge) outEdges [0]);
			}

			if ( !traverseRemainingSubpath ) {
			    break;
			}

		    }
		    else {
			// we need to find the method object for the finally
			// method being called, add this callSite to the
			// finallyCallSites for callingMethod, and add the
			// finally method onto the worklist
			callingMethod.addFinallyCallSiteNode( currentNode );
			Method topLevelMethod = callingMethod.getTopLevelContainingMethod();
			Collection finallyMethods = topLevelMethod.getTransitiveFinallyMethodsCollection ();
			FinallyCallAttribute finallyAttr = Factory.getFinallyCallAttribute (currentNode);
			
			String soughtMethodName = finallyAttr.getName();
			
			for (Iterator k = finallyMethods.iterator (); k.hasNext ();)
			{
				Method m = (Method) k.next ();
			    if ( soughtMethodName.equals (m.getName() ) ) {
				if ( !worklist.contains (m) )
				    worklist.addLast (m);
				
				// this line may be moved to CFG in the future
				finallyAttr.setMethod (m);
				
				break;
			    }
			}
			
			// we also need to place a return site attribue on this call
			ReturnSitesAttribute finallyReturnSitesAttr = Factory.getReturnSitesAttribute (currentNode);
			if ( finallyReturnSitesAttr == null ) {
			    finallyReturnSitesAttr = 
				new ReturnSitesAttributeImpl ();
			    currentNode.
				addAttribute( finallyReturnSitesAttr );
			}
			else {
			    RuntimeException e = new RuntimeException();
			    e.printStackTrace();
			    throw e;
			}
			finallyReturnSitesAttr.addReturnSite( nextNode );
			
			
		    }
		    
		    currentNode = nextNode;
		}
	    }
	    
	    // if new exceptional exits were added to the calling
	    // method as a result of exceptional exit propagation,
	    // we need to add the calling method onto the worklist
	    if ( createdNewExceptionalExit ) {
		if ( !worklist.contains( callingMethod ) ) {
		    worklist.addLast( callingMethod );
		}
	    }
	}
	
	// create an edge from the call site of the calling
	// method to the entry of the called method
	Node [] calledEntries = calledCFG.getEntryNodes ();
	StatementNode calledEntry = null;
	for ( int l = 0; l < calledEntries.length; l++ )
	    if ( ((StatementNode)calledEntries[l]).getType() == 
		 StatementNode.ENTRY_NODE ) {
		calledEntry = (StatementNode)calledEntries[l];
		break;
	    }
	Edge callEdge = new EdgeImpl ( callSite, calledEntry );
	boolean containsCallEdgeAlready = 
	    containsEdgeLike( callSite, calledEntry );
	if ( !containsCallEdgeAlready )
	    addEdge( callEdge );
	
	// if the edge was already there, we need to get the reference to that
	// edge so that we can associate or update a MethodCallEdgeAttribute
	if ( containsCallEdgeAlready ) {
	    Edge [] outEdges = getOutEdges (callSite);
	    for ( int k = 0; k < outEdges.length; k++ ) {
		if ( outEdges[k].getSink() == calledEntry ) {
		    callEdge = (jaba.graph.Edge) outEdges [k];
		    break;
		}
	    }
	}
	
	// put an attribute on the callEdge that identifies the object types
	// that enables the traversal of that edge
	if ( ( callSite.getType() != 
	       StatementNode.NORMAL_FINALLY_CALL_NODE ) &&
	     ( callSite.getType() != 
	       StatementNode.EXCEPTIONAL_FINALLY_CALL_NODE ) ) {
	    MethodCallEdgeAttribute callEdgeAttr = null;
	    callEdgeAttr = Factory.getMethodCallEdgeAttribute (callEdge);
	    
	    if ( callEdgeAttr == null ) {
		callEdgeAttr = new MethodCallEdgeAttributeImpl ();
		callEdge.addAttribute( callEdgeAttr );
	    }
	    populateMethodCallEdgeAttribute( callEdgeAttr, 
					     (jaba.sym.Method) calledEntry.getContainingMethod ());
	}
	
	// create an edge from the exit of the called method to
	// the return node (for the call site) in the called
	// method
	Node [] calledExits = calledCFG.getExitNodes ();
	StatementNode calledExit = null;
	for ( int l = 0; l < calledExits.length; l++ )
	    if ( ((StatementNode)calledExits[l]).getType() ==
		 StatementNode.EXIT_NODE ) {
		calledExit = (StatementNode)calledExits[l];
		break;
	    }
	// the exit node in the called procedure may be unreachable, so only
	// create the edge if calledExit isn't null
	if ( calledExit != null ) {
	    Edge returnEdge = new EdgeImpl ( calledExit, returnNode );
	    boolean containsEdgeAlready = 
		containsEdgeLike( calledExit, returnNode );
	    if ( !containsEdgeAlready )
		addEdge( returnEdge );
	    
	    // if the edge was already there, we need to get the reference to
	    // that edge so that we can associate or update the 
	    // ReturnEdgeAttribute.
	    if ( containsEdgeAlready ) {
		Edge [] outEdges = getOutEdges (calledExit);
		for ( int k = 0; k < outEdges.length; k++ ) {
		    if ( outEdges[k].getSink() == returnNode ) {
			returnEdge = (jaba.graph.Edge) outEdges [k];
			break;
		    }
		}
	    }
	    
	    // put an attribute on the returnEdge that identifies
	    // the call site that causes this edge to be executed.
	    ReturnEdgeAttribute edgeAttr = null;
	    edgeAttr = Factory.getReturnEdgeAttribute (returnEdge);
	    
	    if ( edgeAttr == null )  {
		edgeAttr = new ReturnEdgeAttributeImpl ();
		returnEdge.addAttribute( edgeAttr );
	    }
	    edgeAttr.addCallNode( callSite );
	    
	}
	
    }
    
    /**
     * Initializes ICFG by copying all CFG's into the ICFG except for edges
     * from call site nodes to their respective return node and halt statements
     * to the exit.
     */
    private void initializeICFG()
    {
	// for all methods...
	List methods = getAllMethodsList ();

	// load all CFGs to make sure that methods are created for finally
	// blocks; then get the number of methods again to ensure that
	// methods for finally blocks are returned (and the CFGs for finally
	// blocks are added to the ICFG
	for (Iterator i = methods.iterator (); i.hasNext ();)
	{
	    Method method = (Method) i.next ();
	    Debug.println("method " + method.getFullyQualifiedName(), Debug.ICFG, 1);
	    CFG cfg = Factory.getCFG (method);
	}
	    
	methods = getAllMethodsList ();
	for (Iterator i = methods.iterator (); i.hasNext ();)
	{
	    Method method = (Method) i.next ();

	    CFG cfg = Factory.getCFG (method);
	    
	    if ( cfg == null ) continue;
	    
	    // copy all edges (except call to return) from all CFGs into ICFG
	    Edge [] edges = cfg.getEdges ();
	    for ( int j = 0; j < edges.length; j++ ) {
		StatementNode source = (StatementNode)edges[j].getSource();
		if ( ( source.getType() == 
		       StatementNode.NORMAL_FINALLY_CALL_NODE ) ||
		     ( source.getType() == 
		       StatementNode.EXCEPTIONAL_FINALLY_CALL_NODE ) ) {
		    // we need to put a return sites attribute on the call
		    // node with the return node as a return site
		    ReturnSitesAttribute returnSitesAttr = Factory.getReturnSitesAttribute (source);
		    if ( returnSitesAttr == null ) {
			returnSitesAttr = new ReturnSitesAttributeImpl ();
			source.addAttribute( returnSitesAttr );
		    }
		    returnSitesAttr.addReturnSite( (StatementNode)edges[j].
						   getSink() );
		    
		    // continue so that we don't place an edge to the return node
		    continue;
		}
		
		// if this is a halt statement, we do not want to make the edge
		// from it to the exit node.
		if ( source.getType() == StatementNode.HALT_NODE )
		    continue;
		
		// if this is a method call, we need to make sure it is not
		// a library call, because if it is we still want to keep the
		// edge from call to return
		if ( ( source.getType() ==
                       StatementNode.STATIC_METHOD_CALL_NODE ) ||
                     ( source.getType() ==
                       StatementNode.VIRTUAL_METHOD_CALL_NODE ) ) {
		    // we need to put a return sites attribute on the call
		    // node with the return node as a return site
		    ReturnSitesAttribute returnSitesAttr = Factory.getReturnSitesAttribute (source);
		    if ( returnSitesAttr == null ) {
			returnSitesAttr = new ReturnSitesAttributeImpl ();
			source.addAttribute( returnSitesAttr );
		    }
		    returnSitesAttr.addReturnSite( (StatementNode)edges[j].
						   getSink() );

		    // Now, a particular method call can have successors
		    // of type entry node and of type return node
		    // concurrently if some of the possible methods
		    // called have CFG's and some others do not
		    MethodCallAttribute methodCallAttr = Factory.getMethodCallAttribute (source);
		    Vector calledMethods = methodCallAttr.getMethodsVector ();
		    boolean atLeastOneLib = false; // at least one CFG
		    boolean onlyAbstractOrNativeNonLibs = true;
		    for (Iterator k = calledMethods.iterator (); k.hasNext ();)
		    {
			Method m = (Method) k.next ();
			if ( m.getContainingType().isSummarized() ) 
			    atLeastOneLib = true;
			else {
			    if ( ( m.getModifiers() & Modifier.M_NATIVE ) != 0 )
				atLeastOneLib = true;
			    if (   ((m.getModifiers () & Modifier.M_NATIVE) == 0)
				&& ((m.getModifiers () & Modifier.M_ABSTRACT) == 0)
			       )
			    {
				onlyAbstractOrNativeNonLibs = false;
			    }
			}
		    }
		    if ( !atLeastOneLib && !onlyAbstractOrNativeNonLibs ) 
			continue;
		}
		
		addEdge ((jaba.graph.Edge) edges [j]);
	    }
	}
    }
    
    /**
     * Initializes the worklist with all methods (and submethods) in the
     * program that are not summarized.
     * @param worklist  The worklist to be initialized.
     */
    private void initializeWorklist( LinkedList worklist )
    {
	// currently this method is just placing the methods on the worklist
	// in an arbitrary order.  The ICFG construction is more efficient if
	// this worklist is populated in reverse depth-first order.  We expect
	// to sometime in the future, replace the implementation of this method
	// with one that populates this worklist as such.
	List methods = getAllMethodsList ();
	for (Iterator i = methods.iterator (); i.hasNext ();)
	{
		Method m = (Method) i.next ();
	    if (   (!m.getContainingType ().isSummarized ())
		&& ((m.getModifiers() & M_ABSTRACT) == 0)
		&& ((m.getModifiers() & M_NATIVE) == 0)
	       )
	    {
		worklist.addLast (m);
	    }
	}
    }
    
    /**
     * Returns all methods (and submethods) for the program.
     */
	private List getAllMethodsList ()
	{
		List returnVal = new LinkedList ();
		NamedReferenceType[] namedRefs = getProgram().getNamedReferenceTypes();
		for ( int i = 0; i < namedRefs.length; i++ )
		{
			if ( !(namedRefs[i] instanceof Class) )
				continue;
			Class classType = (Class)namedRefs[i];
			Collection methods = classType.getMethodsCollection ();
			for (Iterator j = methods.iterator (); j.hasNext ();)
			{
				Method method = (Method) j.next ();
				returnVal.add( method );
				Collection finallyMethods = method.getTransitiveFinallyMethodsCollection ();
				for (Iterator k = finallyMethods.iterator (); k.hasNext ();)
					returnVal.add ((Method) k.next ());
			}
		}
	
		return returnVal;
	}

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getAllMethodsList</code>
     */
	private Method [] getAllMethods ()
	{
		List l = getAllMethodsList ();

		return (Method []) l.toArray (new Method [l.size ()]);
	}

    /**
     * Given a MethodCallEdgeAttribute and the called Method, the
     * MethodCallEdgeAttribute is populated with all of the Class types that
     * may cause traversal of that edge.
     * @param callAttr  MethodCallEdgeAttribute to populate.
     * @param method  Method being called.
     */
    private void 
	populateMethodCallEdgeAttribute( MethodCallEdgeAttribute callAttr, 
					 Method method )
    {
	String sig = method.getSignature();
	callAttr.addTraversalType( (Class)method.getContainingType() );
	LinkedList worklist = new LinkedList();
	Collection subclasses = ((Class)method.getContainingType()).getDirectSubClassesCollection ();
	for (Iterator i = subclasses.iterator (); i.hasNext ();)
	    worklist.addLast((Class) i.next ());

	while ( !worklist.isEmpty() ) {
	    Class currClass = (Class)worklist.removeFirst();
	    if ( currClass.getInheritedMethod( sig ) == method ) {
		subclasses = currClass.getDirectSubClassesCollection ();
		for (Iterator i = subclasses.iterator (); i.hasNext ();)
		    worklist.addLast((Class) i.next ());
		callAttr.addTraversalType( currClass );
	    }
	}
    }
}
