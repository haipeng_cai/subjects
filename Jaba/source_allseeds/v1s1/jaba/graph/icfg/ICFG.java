package jaba.graph.icfg;

import jaba.graph.ProgramGraph;

/**
 * Intermethod control-flow graph.
 * @author Jim Jones -- <i>Mar. 1, 1999.  Created.</i>
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/16 added toStringReduced (), toStringDetailed ()
 * @author Jay Lofstead 2003/06/20 changed to use the options from the program rather than global
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface ICFG extends ProgramGraph
{
	/**
	 * Returns an XML string representation of this ICFG.
	 * @return A string representation of this ICFG.
	 */
	public String toString ();

	/** returns the reduced representation of this object */
	public String toStringReduced ();

	/** returns the detailed representation of this object */
	public String toStringDetailed ();
}
