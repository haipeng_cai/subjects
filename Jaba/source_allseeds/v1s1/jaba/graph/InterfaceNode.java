package jaba.graph;

import jaba.sym.Interface;
import jaba.sym.NamedReferenceType;

/**
 * An <code>InterfaceNode </code> is an upper level representation of an interface
 * in a program. An <code>InterfaceNode</code>, along with ClassNodes and Edges are 
 * used to construct graphical analysis of a ClassHierarchyGraph. InterfaceNodes
 * are light gray ellipses in a ClassHierarchyGraph.
 *
 * @author Caleb Ho -- <i>Created 5/2001 </i>
 * @author Huaxing Wu -- <i> Add method getColor, getShape, getNodeTypeName </i>
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/04 added call to getString () in toString ()
 * @author Jay Lofstead 2003/06/06 changed XML type to just node for ease of manipulation
 * @author Jay Lofstead 2003/06/25 added implements serializable to support RMI
 */
public class InterfaceNode extends TypeNode implements java.io.Serializable
{
    // Fields
    /** 
     * Used to tabulate the number of InterfaceNodes created for a particular
     * ClassHierarchyGraph
     */
   protected static int TOTAL_INTERFACE_NODES = 0;

   /** A reference to the Interface object this InterfaceNode represents */
   private Interface interfaceReference;


   /**
    * interfaceNodeNumber stores the value of the static variable
    * TOTAL_INTERFACE_NODES when this InterfaceNode is initially instantiated.
    * It is used to provide each node with a numeric id, and does not
    * imply ordering of the nodes, because their order is generated randomly
    */
    private int interfaceNodeNumber;

  // Methods

  /**
   * Constructor for an interface node. It creates a new interface node and 
   * instantiates the necessary variables.
   * @param : i is the Interface object this InterfaceNode will store and represent
   */
   public InterfaceNode ( Interface i )
   {
	interfaceReference = i;
	setTypeReference((NamedReferenceType) interfaceReference);
	interfaceNodeNumber = TOTAL_INTERFACE_NODES++;
	typeOf = TypeNode.INTERFACE_NODE;
   }

   /**
    * returns the reference to the Interface object that this InterfaceNode was created for
    */ 
   public Interface getInterfaceReference()
   {
	return (interfaceReference);
   }

    /**
     * Returns the interfaceNodeNumber, a numeric id used only to track the nodes for analysis,
     * it in no way implies the ordering or importance of interfaces
     */
   public int getInterfaceNodeNumber()
   {
	return (interfaceNodeNumber);
   }

    /**
     * Returns the name of the Interface object this InterfaceNode is storing
     */
   public String toString()
   {
	return "<node>" + getString () + "<name>" +  interfaceReference.getName () + "</name></node>";
   }

    /**
     * Returns the type name of this object.
     * @return the name of the type
     */
    public String getNodeTypeName() { return "Interface"; }

    /**
     * Returns the shape of this object.
     * @return the shape of the type
     */
    public String getShape() { return "ellipse";}

    /**
     * Returns the color of this object.
     * @return the color of the type
     */
    public String getColor() { return "orchid"; }
}
