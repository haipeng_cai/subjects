package jaba.graph.hammock;

import jaba.graph.Node;
import jaba.graph.NodeImpl;

/**
 * A <code>HammockNode</code> is a place holder node, which 
 * shows where a hammock header or exit is in a 
 * <code>HammockGraph</code>.
 * @author K. Repine repinekm@rose-hulman.edu
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/04 changed toString to generate XML.  added call to getString () in toString ().
 * @author Jay Lofstead 2003/06/05 removed explicit setting of the node number
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/06 changed XML type to just node for ease of manipulation
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/06/25 added implements serializable to support RMI
 * @author Jay Lofstead 2003/07/10 changed to implement HammockNode
 */
public class HammockNodeImpl extends NodeImpl implements java.io.Serializable, HammockNode
{
    // Constants
    //HammockNode types (can only be one)
    
    /** A header node represents the start of a hammock. */
    public static final int HAMMOCK_HEADER = 31;

    /** An exit node represents the exit of a hammock. */
    public static final int HAMMOCK_EXIT = 32;

    //Fields

    /** The type of <code>HammockNode</code>. */
    private int nodeType;

    /** The other matching <code>HammockNode</code>.  The exit for 
     *  a header or the header for an exit.
     */
    private HammockNode other;

    /** The node this is a place holder for.  For example for
     *  a hammock header node, this node would be the actual header
     *  in the graph.
     */
    private Node actualNode;

    /** The nesting depth of the hammock node. */
    private int nestingDepth;

    //Constructor

    /**
     * Creates a HammockNode.
     */
    public HammockNodeImpl ()
    {
	super();
	type = 0;
	other = null;
	actualNode = null;
	nestingDepth = -1;
    }

    //Methods

    /**
     * Sets the <code>HammockNode</code> type.
     * @param type The type of <code>HammockNode</code>.
     * @throws IllegalArgumentException if <code>type</code> is not a valid
     * <code>HammockNode</code> type.
     */
    public void setType(int type) throws IllegalArgumentException
    {
	if ((type!=HammockNode.HAMMOCK_HEADER)&&
	    (type!=HammockNode.HAMMOCK_EXIT))
	    throw(new IllegalArgumentException());
	else nodeType = type;
    }

    /**
     * Get the node type name of this object.
     *@return the string of node type name
     */
    public String getNodeTypeName() {
	if(type == HammockNode.HAMMOCK_HEADER)
	    return "HAMMOCK_HEADER";
	else
	    return "HAMMOCK_EXIT";
    }

    /**
     * Get color of this object.
     *@return the string of this object's color
     */
    public String getColor() {
	if(type == HammockNode.HAMMOCK_HEADER)
	    return "orange";
	else
	    return "red";
    }

    /**
     * Get the shape of this object.
     *@return the string of this object's shape
     */
    public String getShape() {
	return "ellipse";
    }

    /**
     * Sets the matching <code>HammockNode</code> to n.
     * @param n The other matching <code>HammockNode</code>.
     * @throws IllegalArgumentException If n is not the right
     * type of HammockNode.
     */
    public void setOther(HammockNode n)
    {
	switch(getType()){
	case(HammockNode.HAMMOCK_HEADER):
	    if (n.getType() == HammockNode.HAMMOCK_EXIT)
		other = n;
	    else throw(new IllegalArgumentException());
	    break;
	case(HammockNode.HAMMOCK_EXIT):
	    if (n.getType() == HammockNode.HAMMOCK_HEADER)
		other = n;
	    else throw(new IllegalArgumentException());
	    break;
	default:
	    throw(new IllegalArgumentException());
	}
    }

    /**
     * Sets the actual node for which the hammock node is 
     * a place holder for.
     * @param n The actual node.
     */
    public void setActual(Node n)
    {
	actualNode = n;
    }

    /**
     * Returns the type of <code>HammockNode</code>.
     * @return The type of HammockNode.
     */
    public int getType()
    {
	return nodeType;
    }

    /**
     * Return the other node for the hammock.
     * @return The other node for the hammock.
     */
    public HammockNode getOther()
    {
	return other;
    }

    /**
     * Returns the actual node for which the hammock node is 
     * a place holder for.
     * @return The actual node.
     */
    public Node getActual()
    {
	return actualNode;
    }

    /**
     * Returns the hammock nesting depth.
     * @return The hammock nesting depth.
     */
    public int getNestingDepth()
    {
	return nestingDepth;
    }

    /**
     * Sets the nesting depth.
     * @param n The number to set the nesting depth to.
     */
    public void setNestingDepth(int n)
    {
	nestingDepth = n;
    }

    /**
     * Sets up the relationships between new header and exit nodes,
     * with actual nodes set to the parameters header and exit.
     * @param header The actual header node.
     * @param exit The actual exit node.
     * @param base The base to start numbering above.
     * @return The place holder header node, with the correct relationships.
     */
    public static HammockNode setUpPair(Node header, Node exit, int depth)
    {
	HammockNode headerNode = new HammockNodeImpl ();
	HammockNode exitNode = new HammockNodeImpl ();

	headerNode.setType(HammockNode.HAMMOCK_HEADER);
	exitNode.setType(HammockNode.HAMMOCK_EXIT);

	headerNode.setOther(exitNode);
	exitNode.setOther(headerNode);

	headerNode.setActual(header);
	exitNode.setActual(exit);

	headerNode.setNestingDepth(depth);
	exitNode.setNestingDepth(depth);

	return headerNode;
    }

    /**
     * Returns the string representation of the node.
     * @return The string representing the node.
     */
    public String toString()
    {
	StringBuffer buf = new StringBuffer ();

	buf.append ("<node>");
	buf.append (getString ());
	buf.append (getActual ());
	buf.append ("</node>");

	return buf.toString ();
    }
}
