package jaba.graph.hammock;

import java.util.Vector;

import java.io.IOException;

import jaba.graph.Graph;
import jaba.graph.GraphAttribute;
import jaba.graph.Node;

import jaba.main.DottyOutputSpec;

import jaba.sym.DemandDrivenAttribute;

/**
 * Represents hammocks in a single entry single exit Graph, where a 
 * hammock is a group of nodes where all incoming edges to the hammock
 * go to one node in the hammock and all outgoing edges from the 
 * hammock go to one node outside of the hammock.  The hammock is 
 * represented in the graph by place holder nodes, which means a hammock 
 * entry node is placed directly before a hammock header and a hammock 
 * join node is placed directly before the hammock exit. Then all edges 
 * that enter the header in the Graph now enter the hammock 
 * entry node, and the only edge leaving the entry node goes to the 
 * header.  Also, all the edges entering the hammock exit in the 
 * Graph now enter the hammock join node and the only edge from the 
 * hammock join node goes to the hammock exit.  Throughout the rest of 
 * the documentation header and hammock exit nodes will refer to hammock 
 * entry nodes and hammock join nodes, unless otherwise stated.
 * Note: only the return edge of a call node is traversed.
 * things to do
 * fix up dotty stuff... (make it cleaner so it works for all node types)
 * optimizations
 * merge consecutive mod nodes
 * if there is a bug it is probably in load where I try
 *      to find where to insert the hammock nodes
 * a note on dotty output: sometimes what you
 *      see with regard to subgraphs isn't what the
 *      file says, when in doubt check the file
 * note: The dotty method was copied from Graph and modified
 * @author K. Repine repinekm@rose-hulman.edu
 * @author Jay Lofstead 2003/06/02 replaced elementAt with enumeration for better performance
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/05 removed node number parameter to setUpPair
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 added toString
 * @author Jay Lofstead 2003/06/05 added implementing of DemandDrivenAttribute to allow it to be created.
 * @author Jay Lofstead 2003/06/06 changed to use getStringReduced in toString
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/06/16 added toReducedString (), toDetailedString ()
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface HammockGraph extends Graph, GraphAttribute, DemandDrivenAttribute
{
    /**
     * Returns an array of all the hammock headers in the graph.
     * @return The hammock header place holder nodes.
     */
    public Node[] getHammockHeaders();

    /**
     * Returns the number of hammocks in the graph.
     * @return The number of hammocks in the graph.
     */
    public int getNumberHammocks();

    /**
     * Returns the maximum nesting of a hammock in the graph.  For example
     * if there were three hammocks in a graph and the second occurred
     * inside the first and the third occurred inside the second, then
     * the maximum nesting of a hammock in the graph would be three.
     * @return The maximum nesting depth of a hammock in the graph.
     */
    public int getMaxNestingDepth();

    /**
     * Returns the <code>Graph</code> for which this is the 
     * <code>HammockGraph</code> of.
     * @return The parent MethodGraph.
     */
    public Graph getParentGraph();

    /**
     * Returns the nesting depth of node n.  The nesting
     * depth can be viewed as the number of hammocks a
     * node is in.
     * @param n The node to find the nesting depth of.
     * @return The nesting depth of n.
     */
    public int getNestingDepth(Node n);

    /**
     * Returns the exit of the hammock, if header is a hammock header,
     * otherwise, it returns null.
     * @param header The hammock header to find the exit of.
     * @return The exit of the hammock with header, header, or null.
     */
    public Node getHammockExit(Node header);

    /**
     * Returns the header of the hammock, if exit is a hammock exit,
     * otherwise, it returns null.
     * @param exit The hammock exit to find the header of.
     * @return The header of the hammock with hammock exit, exit, or null.
     */
    public Node getHammockHeader(Node exit);

    /**
     * Returns the child of n, if n is a 1-node, otherwise it returns 
     * <code>null</code>.  A 1-node is a node with exactly one incoming 
     * edge and exactly one outgoing edge.  If n is a hammock header or 
     * exit, then n is treated like a collapsed hammock node, which means 
     * the incoming edges are the in-edges of the header and the outgoing 
     * edges are the out-edges of the exit.
     * @param n The node to find the child of.
     * @return The child of n, if n is a 1-node, otherwise <code>null</code>.
     */
    public Node getOneNodeChild(Node n);

    /**
     * Returns the header of the minimum hammock containing n, or 
     * <code>null</code> if none.
     * @param n The node to find the minimum hammock of.
     * @return The header of the minimum hammock containing n, or 
     * <code>null</code> if none.
     */
    public HammockNode getMinHammock(Node n);

    /**
     * Returns the set of nodes in the hammock with header, header,
     * not including the place holder nodes of that hammock.  Returns 
     * <code>null</code> if header is not a header.  Note that if there 
     * is a hammock within the hammock lead by header, then the header 
     * node of that hammock will be returned and none of the node 
     * interior nodes will be returned.
     * @param header A hammock header.
     * @return a Vector of Node objects in the hammock, or <code>null</code>.
     */
    public Vector getHammockNodesVector (Node header);

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getHammockNodesVector</code>
     *  @param header A hammock header.
     *  @return a Vector of Node objects in the hammock, or <code>null</code>.
     */
    public Node [] getHammockNodes(Node header);

    /**
     * Checking whether the node n is inside the hammock
     * whose header is h.
     * @param h The header of the hammock
     * @param n The node to be checked
     * @return true, if inside, if outside or h is not header, return false.
     */
    public boolean isNodeInsideHammock(Node h, Node m);

    /**
     * Calculates the number of nodes in the hammock
     * whose header is h.
     * @param h The header of the hammock.
     * @return The number of nodes between and including
     * h and h's exit.
     */
    public int getNumNodesInHammock(HammockNode h);

    /**
     * Writes this graph to a file in the format that is employed by dotty, 
     * a graph drawing software from ATT research. 
     * this should be redone in a more robust fashion.
     * @param filename The name of the file into which the graph has to be 
     *	    written.
     * @param spec ??
     */
    public void createDottyFile(String filename, DottyOutputSpec spec)  throws IOException;

	/** returns an XML representation of this object */
	public String toString ();

	/** returns the reduced representation of this object */
	public String toStringReduced ();

	/** returns the detailed representation of this object */
	public String toStringDetailed ();
}
