package jaba.graph.hammock;

import jaba.graph.Node;

/**
 * A <code>HammockNode</code> is a place holder node, which 
 * shows where a hammock header or exit is in a 
 * <code>HammockGraph</code>.
 * @author K. Repine repinekm@rose-hulman.edu
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/04 changed toString to generate XML.  added call to getString () in toString ().
 * @author Jay Lofstead 2003/06/05 removed explicit setting of the node number
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/06 changed XML type to just node for ease of manipulation
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/06/25 added implements serializable to support RMI
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface HammockNode extends Node, java.io.Serializable
{
    /** A header node represents the start of a hammock. */
    public static final int HAMMOCK_HEADER = 31;

    /** An exit node represents the exit of a hammock. */
    public static final int HAMMOCK_EXIT = 32;

    //Methods

    /**
     * Sets the <code>HammockNode</code> type.
     * @param type The type of <code>HammockNode</code>.
     * @throws IllegalArgumentException if <code>type</code> is not a valid
     * <code>HammockNode</code> type.
     */
    public void setType(int type) throws IllegalArgumentException;

    /**
     * Get the node type name of this object.
     * @return the string of node type name
     */
    public String getNodeTypeName();

    /**
     * Get color of this object.
     * @return the string of this object's color
     */
    public String getColor();

    /**
     * Get the shape of this object.
     * @return the string of this object's shape
     */
    public String getShape();

    /**
     * Sets the matching <code>HammockNode</code> to n.
     * @param n The other matching <code>HammockNode</code>.
     * @throws IllegalArgumentException If n is not the right
     * type of HammockNode.
     */
    public void setOther(HammockNode n);

    /**
     * Sets the actual node for which the hammock node is 
     * a place holder for.
     * @param n The actual node.
     */
    public void setActual(Node n);

    /**
     * Returns the type of <code>HammockNode</code>.
     * @return The type of HammockNode.
     */
    public int getType();

    /**
     * Return the other node for the hammock.
     * @return The other node for the hammock.
     */
    public HammockNode getOther();

    /**
     * Returns the actual node for which the hammock node is 
     * a place holder for.
     * @return The actual node.
     */
    public Node getActual();

    /**
     * Returns the hammock nesting depth.
     * @return The hammock nesting depth.
     */
    public int getNestingDepth();

    /**
     * Sets the nesting depth.
     * @param n The number to set the nesting depth to.
     */
    public void setNestingDepth(int n);

    /**
     * Returns the string representation of the node.
     * @return The string representing the node.
     */
    public String toString();
}
