/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

/**
 * Attribute that needs to be stored with a call (statement) node.
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface CallAttribute extends StatementNodeAttribute
{
  /** The statement node that represents the return from the call site. */
  public StatementNode returnNode = null;

  /**
   * Assigns the return node that represents the return from the called
   * procedure.
   * @param returnNode  Return node that represents the return from the called
   *                    procedure.
   */
  public void setReturnNode( StatementNode returnNode );

  /**
   * Returns the return node  that represents the return from the called
   * procedure.
   * @return Return node that represents the return from the called procedure.
   */
  public StatementNode getReturnNode();
}
