/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.acfg;

import jaba.sym.Program;
import jaba.sym.Method;
import jaba.sym.Class;
import jaba.sym.NamedReferenceType;

import jaba.graph.MethodCallAttribute;
import jaba.graph.FinallyCallAttribute;
import jaba.graph.StatementNode;
import jaba.graph.StatementNodeImpl;
import jaba.graph.ReturnSitesAttribute;
import jaba.graph.Node;

import jaba.graph.icfg.ICFG;

import jaba.debug.Debug;

import jaba.tools.local.Factory;

import java.util.Vector;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Enumeration;
import java.util.List;
import java.util.Iterator;
import java.util.Collection;

/**
 * @author Jay Lofstead 2003/06/02 converted elementAt into Enumeration for better performance
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/07/17 changed to use Factory to get attributes
 */
class PNRCInitializer implements java.io.Serializable
{
  /** ??? */
  private Program program;

  /** map from a called method to all of the call sites to it */
  private HashMap callSitesMap;

  /** ??? */
  private HashMap superExitMap;
  /** ??? */
    private HashMap methodVisitedMap;

  /** ??? */
  PNRCInitializer()
    {
      callSitesMap = new HashMap();
      superExitMap = new HashMap();
      methodVisitedMap = new HashMap();
    }

  /** ??? */
  void load( Program program )
    {
      if ( program.getIsPNRCAnalysisDone() ) {
            return;
      }
      this.program = program;

      /* load the ICFG in case it has not been loaded */
      ICFG icfg = Factory.getICFG (program);

      populateCallSitesMap();
      populateMethodVisitedMap();

      LinkedList worklist = new LinkedList();
      populateWorklist( worklist, icfg );

      while ( !worklist.isEmpty() )
	{
	  StatementNode callSite = (StatementNode)worklist.removeFirst();
	  Method callingMethod = callSite.getContainingMethod();
	  if ( callingMethod == null )
	    {
		RuntimeException e = new RuntimeException();
		e.printStackTrace();
		throw e;
	    }
	  ReturnSitesAttribute returnSitesAttr = null;
	  returnSitesAttr = Factory.getReturnSitesAttribute (callSite);
	  if ( returnSitesAttr == null ) {
	      RuntimeException e = new RuntimeException();
	      e.printStackTrace();
	      throw e;
	  }

	  StatementNode superExitNode = null;
	  if ( !superExitMap.containsKey( callingMethod ) )
	    {
	      superExitNode = new StatementNodeImpl ();
	      superExitNode.setType( StatementNode.SUPER_EXIT_NODE );
	      superExitNode.setContainingMethod ((jaba.sym.Method) callingMethod);
	      superExitMap.put( callingMethod, superExitNode );
	    }
	  else {
	      superExitNode =
		  (StatementNode)superExitMap.get( callingMethod );
	  }
	  returnSitesAttr.addReturnSite( superExitNode );

	  /* add call sites to this method to the worklist if they have
	     not previously been added */
	  if ( !((Boolean)methodVisitedMap.get( callingMethod )).
	       booleanValue() ) {
	      StatementNode[] callSitesForCallingMethod = 
		  getCallSitesForMethod ((jaba.sym.Method) callingMethod);
	      for ( int i = 0; i < callSitesForCallingMethod.length; i++ ) {
		  worklist.addLast( callSitesForCallingMethod[i] );
	      }
	      methodVisitedMap.put( callingMethod, new Boolean( true ) );
	  }
	}

        program.setIsPNRCAnalysisDone();
    }

  /** ??? */
  private void populateCallSitesMap()
    {
      Vector methods = getAllMethodsVector ();
     
      for (Enumeration i = methods.elements (); i.hasMoreElements ();)
      {
	Method m = (Method) i.nextElement ();
 	  Debug.println( "Initializing "+ m.getFullyQualifiedName()+
			 " in call sites map", Debug.ACFG, 1 );
	  callSitesMap.put (m, new Vector() );
      }

      for (Enumeration i = methods.elements (); i.hasMoreElements ();)
	{
	  Method method = (Method) i.nextElement ();
	  Vector callSiteVec = new Vector();
	  Collection callSites = method.getCallSiteNodesCollection ();
	  for (Iterator j = callSites.iterator (); j.hasNext ();)
	    {
	      StatementNode callSite = (StatementNode) j.next ();
	      MethodCallAttribute callAttr = Factory.getMethodCallAttribute (callSite);

	      Vector calledMethods = callAttr.getMethodsVector ();
	      for (Enumeration k = calledMethods.elements (); k.hasMoreElements ();)
		{
		  Vector callSitesForMethod = (Vector)callSitesMap.get((Method) k.nextElement ());
		  // if there is no entry in the callSitesMap, then this is
		  // a call to a method that is not analyzed in the program --
		  // a "lib" call.  in this case we will not be propogating
		  // halts from them, so we do not put them on the map
		  if ( callSitesForMethod == null )
			continue;

		  callSitesForMethod.add( callSite );
		}

	    }

	  callSites = method.getFinallyCallSiteNodesCollection ();
	  for (Iterator j = callSites.iterator (); j.hasNext ();)
	    {
	      StatementNode callSite = (StatementNode) j.next ();

	      FinallyCallAttribute callAttr = Factory.getFinallyCallAttribute (callSite);

 	      Debug.println( "finally call site:\n"+callSite+"\n",Debug.ACFG, 2 );
	      Method calledMethod = callAttr.getMethod();
	      if ( calledMethod == null )
	      {
		  throw new RuntimeException( "No method on finally call attribute!\n" + callSite );
	      }
 	      Debug.println( "Called method "+calledMethod.getFullyQualifiedName()+"\n",Debug.ACFG, 2 );
	      Vector callSitesForMethod = (Vector)callSitesMap.get( calledMethod );
	      if ( callSitesForMethod == null )
	      {
		  throw new RuntimeException( "not found in call sites map" );
	      }
	      callSitesForMethod.add( callSite );
	    }
	}
    }

  /** ??? */
  private StatementNode[] getCallSitesForMethod( Method calledMethod )
    {
      Vector callSitesForMethod = (Vector)callSitesMap.get( calledMethod );
      StatementNode[] returnVal = 
	new StatementNode[ callSitesForMethod.size() ];
	int i = 0;
      for (Enumeration e = callSitesForMethod.elements (); e.hasMoreElements (); i++)
	returnVal[i] = (StatementNode) e.nextElement ();
      return returnVal;
    }

  /** ??? */
  private void populateWorklist( LinkedList worklist, ICFG icfg )
    {
      Node[] exitNodes = icfg.getExitNodes();
      for ( int i = 0; i < exitNodes.length; i++ )
	{
	  StatementNode exitNode = (StatementNode)exitNodes[i];
	  if ( exitNode.getType() != StatementNode.HALT_NODE ) continue;
	  Method pnrp = exitNode.getContainingMethod();
	  if ( pnrp == null )
	    {
		RuntimeException e = new RuntimeException();
		e.printStackTrace();
		throw e;
	    }
	  StatementNode[] pnrcs = getCallSitesForMethod ((jaba.sym.Method) pnrp);
	  for ( int j = 0; j < pnrcs.length; j++ ) {
	      Debug.println( "Adding to worklist...\n"+pnrcs[j],
			     Debug.ACFG, 1 );
	      worklist.addLast( pnrcs[j] );
	  }
	  methodVisitedMap.put( pnrp, new Boolean( true ) );
	}
						    
    }

  /**
   * Returns all methods (and submethods) for the program.
   */
  private Vector getAllMethodsVector ()
    {
      Vector returnVec = new Vector();
      NamedReferenceType[] namedRefs = program.getNamedReferenceTypes ();
      for ( int i = 0; i < namedRefs.length; i++ )
	{
	  if ( !(namedRefs[i] instanceof Class) )
		continue;
	  Class classType = (Class)namedRefs[i];
	  Collection methods = classType.getMethodsCollection ();
	  for (Iterator j = methods.iterator (); j.hasNext ();)
	    {
	      Method method = (Method) j.next ();
	      returnVec.add( method );
	      Collection finallyMethods = method.getTransitiveFinallyMethodsCollection ();
	      for (Iterator k = finallyMethods.iterator (); k.hasNext ();)
		returnVec.add ((Method) k.next ());
	    }
	}

      return returnVec;
    }

	/**
         * Convenience method for converting the Vector to an Array (less efficient) from <code>getAllMethodsVector</code>
	 */
	private Method [] getAllMethods ()
	{
		Vector v = getAllMethodsVector ();

		return (Method []) v.toArray (new Method [v.size ()]);
	}

  /** ??? */
    private void populateMethodVisitedMap()
    {
	Vector methods = getAllMethodsVector ();
	for (Enumeration i = methods.elements (); i.hasMoreElements ();)
	{
	    methodVisitedMap.put ((Method) i.nextElement (), new Boolean (false));
	}
    }
}
