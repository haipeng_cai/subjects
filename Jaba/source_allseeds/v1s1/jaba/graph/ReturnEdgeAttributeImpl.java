/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import java.util.Vector;
import java.util.Enumeration;

/**
 * Attribute found on edges returning from a called method to the calling
 * method that specifies the call site node that causes the called method to
 * be called.
 * @author Jim Jones -- <i>Created, May 17, 1999</i>.
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/04 removed private node number in favor of one at Node
 * @author Jay Lofstead 2003/07/10 changed to implement ReturnEdgeAttribute
 */
public class ReturnEdgeAttributeImpl extends EdgeAttributeImpl implements ReturnEdgeAttribute
{
  /** Contains all call nodes that result in this return edge returning. */
  private Vector callNodes;

    /**
     * Array copy of the corresponding vector field. The first call to
     * <code>getCallNodes()</code> copies the vector to the array, and
     * sets the vector to null.
     */
    private StatementNode[] callNodesArr;

  /**
   * Constructor.
   */
  public ReturnEdgeAttributeImpl ()
    {
      callNodes = new Vector();
    }

  /**
   * Adds a call node to this attribute.
   * @param callNode  The call node to add.
   */
  public void addCallNode( StatementNode callNode )
    {
      if ( callNodes.contains( callNode ) )
	return;
      callNodes.add( callNode );
    }

  /**
   * Returns all call nodes that result in this return edge returning.
   * @return  All call nodes that result in this return edge returning.
   */
  public StatementNode[] getCallNodes()
    {
        if ( callNodesArr == null ) {
            callNodesArr = new StatementNode[ callNodes.size() ];
	    int i = 0;
            for (Enumeration e = callNodes.elements (); e.hasMoreElements (); i++)
	        callNodesArr [i] = (StatementNode) e.nextElement ();
            callNodes = null;
        }
        return callNodesArr;
    }

  /**
   * Returns a string representation of this attribute.
   * @return A string representation of this attribute.
   */
  public String toString()
    {
	StringBuffer str = new StringBuffer (1000);

	str.append ("<returnedgeattribute>");

	StatementNode [] callNodes = getCallNodes ();
	for (int i = 0; i < callNodes.length; i++)
	{
		str.append ("<callnodenumber>").append (callNodes [i].getNodeNumber ()).append ("</callnodenumber>");
	}

	str.append ("</returnedgeattribute>");

	return str.toString ();
    }
}
