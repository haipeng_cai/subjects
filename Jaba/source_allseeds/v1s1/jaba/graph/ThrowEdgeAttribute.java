/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Class;

/**
 * This attribute is placed on all edges from a throw statement.
 * @author Jim Jones -- <i>Created, May 29, 1999</i>.
 * @author Jay Lofstead 2003/05/29 converted toString to generated XML
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface ThrowEdgeAttribute extends EdgeAttribute
{
  /**
   * Sets the object type that causes this edge to be traversed.
   * @param classType  Object type that causes this edge to be traversed.
   */
  public void setTraversalType( Class classType );

  /**
   * Returns the object type that cause this edge to be traversed.
   * @return The object type that cause this edge to be traversed.
   */
  public Class getTraversalType();

  /**
   * Returns a string representation of this attribute.
   * @return A string representation of this attribute.
   */
  public String toString();
}
