package jaba.graph;

/**
 * Provides a Comparator for ordering a list of edges in order.
 * The sort order is primary by source node id, then sink node id, then label.
 *
 * @author Jay Lofstead 2003/06/06 created
 */
public class EdgeSortComparator implements java.util.Comparator
{
	public int compare (Object l, Object r)
	{
		Edge le = (Edge) l;
		Edge re = (Edge) r;

		if (le.getSource ().getNodeNumber () < re.getSource ().getNodeNumber ())
		{
			return -1;
		}
		else
		{
			if (le.getSource ().getNodeNumber () > re.getSource ().getNodeNumber ())
			{
				return 1;
			}
			else
			{
				if (le.getSink ().getNodeNumber () < re.getSink ().getNodeNumber ())
				{
					return -1;
				}
				else
				{
					if (le.getSink ().getNodeNumber () > re.getSink ().getNodeNumber ())
					{
						return 1;
					}
					else
					{
						String ll = le.getLabel ();
						String rl = re.getLabel ();

						if (ll != null && rl != null)
						{
							return ll.compareTo (rl);
						}
						else
						{
							if (ll != null)
							{
								return -1;
							}
							else
							{
								if (rl != null)
								{
									return 1;
								}
								else
								{
									return 0;
								}
							}
						}
					}
				}
			}
		}
	}

	public boolean equals (Object o)
	{
		return false;
	}
}
