/* Copyright (c) 1999, The Ohio State University */

package jaba.instruction;

import jaba.du.DefUse;

import java.util.Vector;
import java.util.List;

/**
 * Abstract base representation of all instructions from the JVM instruction
 * set.
 *
 * @author S. Sinha
 * @author Huaxing Wu -- <i> Revised 7/15/02 change addUse(DefUse) to check 
 *                       whether the DefUse Object is constant value. Add 
 *                       addUse(List) method to handle adding a collection
 *                       of DefUse objects </i>
 * @author Jay Lofstead 2003/06/02 converted elementAt to an enumeration for better performance
 * @author Jay Lofstead 2003/06/05 enumeration additions
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/07/10 changed to an interface
 * @author Jay Lofstead 2003/07/14 removed the accept method per Saurabh
 */
public interface SymbolicInstruction extends java.io.Serializable
{
	/** ??? */
    public String[] getRuntimeExceptions();

    /**
     * Returns the opcode of the instruction.
     * @return opcode of instruction.
     */
    public int getOpcode();

    /**
     * Returns the bytecode offset of the instruction.
     * @return bytecode offset of instruction.
     */
    public int getByteCodeOffset();

    /**
     * Returns the operands of the instruction, or <code>null</code> if the
     * instruction has no operands.
     * @return Operands of instruction, <code>null</code> if instruction has
     *         no operands.
     */
	public Vector getOperandsVector ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getOperandsVector</code>
     * @return Operands of instruction, <code>null</code> if instruction has
     *         no operands.
     */
	public Object [] getOperands ();

    /**
     * Returns the definition of the instruction, or <code>null</code> if no
     * variable is defined by the instruction.
     * @return Definition of instruction, or <code>null</code> if instruction
     *         has no definition.
     */
    public DefUse getDefinition();

    /**
     * Returns the uses of the instruction.
     * @return Uses of instruction.
     */
	public Vector getUsesVector ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getUsesVector</code>
     * @return Uses of instruction.
     */
	public DefUse [] getUses ();

    /**
     * Sets the bytecode offset of the instruction.
     * @param offset bytecode offset of instruction.
     * @throws IllegalArgumentException if <code>offset</code> is
     *                                  less than 0.
     */
    public void setByteCodeOffset( int offset ) throws IllegalArgumentException;

    /**
     * Sets the opcode of the instruction.
     * @param opcode opcode of instruction.
     * @throws IllegalArgumentException if <code>opcode</code> is invalid.
     */
    public void setOpcode( byte opcode ) throws IllegalArgumentException;

    /**
     * Adds an operand to the instruction.
     * @param operand operand to be added.
     * @throws IllegalArgumentException if <code>operand</code> is
     *                                  <code>null</code>.
     */
    public void addOperand( Object operand ) throws IllegalArgumentException;

    /**
     * Adds a collection of used variables for the instruction.
     * @param uses a collection of used variable to be added.Every element
     * of this collection has to be DefUse object
     * @throws IllegalArgumentException if <code>uses</code> is
     *                                  <code>null</code>.
     */
    public void addUse( List usev ) throws IllegalArgumentException;

    /**
     * Adds a used variable for the instruction.
     * @param use variable use to be added.
     * @throws IllegalArgumentException if <code>use</code> is
     *                                  <code>null</code>.
     */
    public void addUse( DefUse use ) throws IllegalArgumentException;

    /**
     * Sets the definition for the instruction.
     * @param definition variable defined by instruction.
     * @throws IllegalArgumentException if <code>definition</code> is
     *                                  <code>null</code>.
     */
    public void setDefinition( DefUse definition ) throws IllegalArgumentException;

    /**
     * Returns a string representation of the instruction.
     * @return string representation of symbolic instruction.
     */
    public String toString();
}

