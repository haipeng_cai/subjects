/* Copyright (c) 1999, The Ohio State University */

package jaba.instruction.visitor;

import jaba.instruction.*;

import jaba.sym.LocalVariableMap;
import jaba.sym.ConstantPoolMap;
import jaba.sym.LocalVariable;
import jaba.sym.Method;
import jaba.sym.Class;
import jaba.sym.Array;
import jaba.sym.ArrayImpl;
import jaba.sym.Interface;
import jaba.sym.Primitive;
import jaba.sym.PrimitiveImpl;
import jaba.sym.ReferenceType;

import jaba.constants.OpCode;

import jaba.classfile.LocalVariableTableAttribute;

import java.util.Vector;

/**
 * Vistor that loads a symbolic instruction. In loading a symbolic instruction,
 * the visitor initializes the opcode of an instruction, the index
 * of the instruction in the bytecode array, and the operands of the
 * instruction, if any.
 * <p>
 * The operands of a symbolic instruction are computed from the values of the
 * bytecodes that follow the opcode in the bytecode array.
 * The operands of an instruction may be symbol-table objects (for example,
 * load and store instructions), or integer constants that represent
 * jump addresses (for example, conditional and unconditional jump
 * instructions).
 *
 * @author S. Sinha
 * @author Huaxing Wu -- Revised <i> 8/8/02. Change visitnewarray to use
 *         changed Primitive class, i.e, use factory method valueof to 
 *         get Primitive object. </i>
 * @author Huaxing Wu -- Revised <i> 12/10/02. Several modification has been made To make sure there is only one type object, and this can be only achieved bycreating type object through Symboltable. So a method object containing the instruction has been kept, type has changed to be resolved through symbol table in method visit newarray, anewarray. </i>
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 */
public class InstructionLoadingVisitor extends SymbolicInstructionVisitorImpl
{
    // Fields

    /**
     * Value of current offset into the bytecode array.
     * <code>offset</code> is initialized when an
     * <code>InstructionLoadingVisitor</code> object is created, and is
     * updated as it visits various instructions. The order in which
     * instructions are visited is controlled externally.
     */
    private int offset;

    /** The array of bytecodes. */
    private byte[] bytecode;

    /**
     * Mapping from constant-pool index to symbol-table entry.
     * The visitor uses the <code>constantPoolMap</code> to identify
     * operands that are of type
     * {@link jaba.sym.ReferenceType ReferenceType} or
     * {@link jaba.sym.Member Member}
     */ 
    private ConstantPoolMap constantPoolMap;

    /**
     * Mapping from an index in the local-variable table to a
     * symbol-table entry. The visitor uses the <code>localVarMap</code>
     * to identify operands that are of type
     * {@link jaba.sym.LocalVariable LocalVariable}.
     */
    private LocalVariableMap localVarMap;

    /** Information about local variables. */
    private LocalVariableTableAttribute localVarTable;

    /** 
     * The method object where these instruction belongs to.
     */
    private Method method;

    /**
     * Creates a new <code>InstructionLoadingVisitor</code> object.
     * @param offset current offset into the bytecode array.
     * @param bytecode bytecode array.
     * @param constantPoolMap map from constant-pool index to symbol-table
     *                        entry.
     * @param localVarMap map from local-variable table index to local variable
     *                    entry in the symbol table.
     * @param localVarTab information about local variables.
     */
    public InstructionLoadingVisitor( int offset, byte[] bytecode,
                                      ConstantPoolMap constantPoolMap,
                                      LocalVariableMap localVarMap,
                                      LocalVariableTableAttribute localVarTab,
				      Method method)
    {
        this.offset = offset;
        this.bytecode = bytecode;
        this.constantPoolMap = constantPoolMap;
        this.localVarMap = localVarMap;
        this.localVarTable = localVarTab;
	this.method = method;
    }

    /**
     * Sets current offset in the bytecode array for this visitor.
     * @param offset current offset into the bytecode array.
     * @throws IllegalArgumentException if <code>offset</code> is less than 0.
     */
    private void setCurrentOffset( int offset )
                 throws IllegalArgumentException
    {
        if ( offset < 0 ) {
            throw new IllegalArgumentException(
                "InstructionLoadingVisitor.setCurrentOffset(): offset < 0 "+
                offset );
        }
        this.offset = offset;
    }

    /**
     * Sets the bytecode array for this visitor.
     * @param bytecode bytecode array.
     * @throws IllegalArgumentException if <code>bytecode</code> is
     *                                  <code>null</code>.
     */
    private void setBytecode( byte[] bytecode )
                 throws IllegalArgumentException
    {
        if ( bytecode == null ) {
            throw new IllegalArgumentException( 
            "InstructionLoadingVisitor.setCurrentOffset(): bytecode is null" );
        }
        this.bytecode = bytecode;
    }

    /**
     * Sets the constant-pool map for this visitor.
     * @param map map from constant-pool index to symbol-table entry.
     * @throws IllegalArgumentException if <code>map</code> is
     *                                  <code>null</code>.
     */
    private void setConstantPoolMap( ConstantPoolMap map )
                throws IllegalArgumentException
    {
        if ( map == null ) {
            throw new IllegalArgumentException(
            "InstructionLoadingVisitor.setConstantPoolMap(): map is null" );
        }
        constantPoolMap = map;
    }

    /**
     * Sets the local-variable map for this visitor.
     * @param map map from local-variable table index to local variable
     *            entry in the symbol table.
     * @throws IllegalArgumentException if <code>map</code> is
     *                                  <code>null</code>.
     */
    private void setLocalVariableMap( LocalVariableMap map )
                throws IllegalArgumentException
    {
        if ( map == null ) {
            throw new IllegalArgumentException(
            "InstructionLoadingVisitor.setConstantPoolMap(): map is null" );
        }
        localVarMap = map;
    }

    /**
     * Iterates over the bytecode array, creates symbolic instructions for
     * the bytecode, and loads the symbolic instructions.
     * @return Vector containing symbolic instructions that correspond to
     *                the bytecodes.
     */
    public Vector iterate() {
        Vector instructions = new Vector();
        SymbolicInstruction instr;
        while ( offset < bytecode.length ) {
            // skip over reset entries
            if ( bytecode[offset] == OpCode.OP_UNDEF ) {
                offset++;
                continue;
            }
            String opCodeName = OpCode.getName( bytecode[offset] );
            opCodeName = OpCode.namePrefix + opCodeName;
            try {
                java.lang.Class c = java.lang.Class.forName( opCodeName );
                instr = (SymbolicInstruction)c.newInstance();
                ((SymbolicInstructionImpl) instr).accept( this );
                instructions.addElement( instr );
                //System.out.print( instr.toString() );
            }catch ( ClassNotFoundException cnfe){
		cnfe.printStackTrace();
		throw new RuntimeException( cnfe );
	    }
	    catch ( IllegalAccessException iae){
		iae.printStackTrace();
		throw new RuntimeException( iae );
	    }
	    catch ( InstantiationException ie){
		ie.printStackTrace();
		throw new RuntimeException( ie );
	    }

        }
        return instructions;
    }

    /**
     * Visits the <i>nop</i> instruction.
     * @param inst <i>nop</i> instruction.
     */
    public void visitNop( Nop inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>aconst_null</i> instruction.
     * @param inst <i>aconst_null</i> instruction.
     */
    public void visitAconst_null( Aconst_null inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>iconst_m1</i> instruction.
     * @param inst <i>iconst_m1</i> instruction.
     */
    public void visitIconst_m1( Iconst_m1 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>iconst_0</i> instruction.
     * @param inst <i>iconst_0</i> instruction.
     */
    public void visitIconst_0( Iconst_0 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>iconst_1</i> instruction.
     * @param inst <i>iconst_1</i> instruction.
     */
    public void visitIconst_1( Iconst_1 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>iconst_2</i> instruction.
     * @param inst <i>iconst_2</i> instruction.
     */
    public void visitIconst_2( Iconst_2 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>iconst_3</i> instruction.
     * @param inst <i>iconst_3</i> instruction.
     */
    public void visitIconst_3( Iconst_3 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>iconst_4</i> instruction.
     * @param inst <i>iconst_4</i> instruction.
     */
    public void visitIconst_4( Iconst_4 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>iconst_5</i> instruction.
     * @param inst <i>iconst_5</i> instruction.
     */
    public void visitIconst_5( Iconst_5 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>lconst_0</i> instruction.
     * @param inst <i>lconst_0</i> instruction.
     */
    public void visitLconst_0( Lconst_0 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>lconst_1</i> instruction.
     * @param inst <i>lconst_1</i> instruction.
     */
    public void visitLconst_1( Lconst_1 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>fconst_0</i> instruction.
     * @param inst <i>fconst_0</i> instruction.
     */
    public void visitFconst_0( Fconst_0 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>fconst_1</i> instruction.
     * @param inst <i>fconst_1</i> instruction.
     */
    public void visitFconst_1( Fconst_1 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>fconst_2</i> instruction.
     * @param inst <i>fconst_2</i> instruction.
     */
    public void visitFconst_2( Fconst_2 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dconst_0</i> instruction.
     * @param inst <i>dconst_0</i> instruction.
     */
    public void visitDconst_0( Dconst_0 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dconst_1</i> instruction.
     * @param inst <i>dconst_1</i> instruction.
     */
    public void visitDconst_1( Dconst_1 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>bipush</i> instruction.
     * @param inst <i>bipush</i> instruction.
     */
    public void visitBipush( Bipush inst ) {
        setOpcodeAndOffset( inst );
        int val = bytecode[offset++];
        inst.addOperand( new Integer( val ) );
    }

    /**
     * Visits the <i>sipush</i> instruction.
     * @param inst <i>sipush</i> instruction.
     */
    public void visitSipush( Sipush inst ) {
        setOpcodeAndOffset( inst );
        int val = (bytecode[offset++] << 8) | bytecode[offset++];
        inst.addOperand( new Integer( val ) );
    }

    /**
     * Visits the <i>ldc</i> instruction.
     * @param inst <i>ldc</i> instruction.
     */
    public void visitLdc( Ldc inst ) {
        setOpcodeAndOffset( inst );
        int index = bytecode[offset++] & 0x000000ff;
	Object entry = constantPoolMap.getConst( new Integer( index ) );
	inst.addOperand( entry );
    }

    /**
     * Visits the <i>ldc_w</i> instruction.
     * @param inst <i>ldc_w</i> instruction.
     */
    public void visitLdc_w( Ldc_w inst ) {
        setOpcodeAndOffset( inst );
	int index = ((bytecode[offset++] << 8) & 0x0000ff00) |
                    (bytecode[offset++] & 0x000000ff);
	Object entry = constantPoolMap.getConst( new Integer( index ) );
	inst.addOperand( entry );
    }

    /**
     * Visits the <i>ldc2_w</i> instruction.
     * @param inst <i>ldc2_w</i> instruction.
     */
    public void visitLdc2_w( Ldc2_w inst ) {
        setOpcodeAndOffset( inst );
	int index = ((bytecode[offset++] << 8) & 0x0000ff00) |
                    (bytecode[offset++] & 0x000000ff);
	Object entry = constantPoolMap.getConst( new Integer( index ) );
	inst.addOperand( entry );
    }

    /**
     * Visits the <i>iload</i> instruction.
     * @param inst <i>iload</i> instruction.
     */
    public void visitIload( Iload inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, (bytecode[offset++] & 0x000000ff),
                            inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>lload</i> instruction.
     * @param inst <i>lload</i> instruction.
     */
    public void visitLload( Lload inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, (bytecode[offset++] & 0x000000ff),
                            inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>fload</i> instruction.
     * @param inst <i>fload</i> instruction.
     */
    public void visitFload( Fload inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, (bytecode[offset++] & 0x000000ff),
                            inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>dload</i> instruction.
     * @param inst <i>dload</i> instruction.
     */
    public void visitDload( Dload inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, (bytecode[offset++] & 0x000000ff),
                            inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>aload</i> instruction.
     * @param inst <i>aload</i> instruction.
     */
    public void visitAload( Aload inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, (bytecode[offset++] & 0x000000ff),
                            inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>iload_0</i> instruction.
     * @param inst <i>iload_0</i> instruction.
     */
    public void visitIload_0( Iload_0 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 0, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>iload_1</i> instruction.
     * @param inst <i>iload_1</i> instruction.
     */
    public void visitIload_1( Iload_1 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 1, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>iload_2</i> instruction.
     * @param inst <i>iload_2</i> instruction.
     */
    public void visitIload_2( Iload_2 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 2, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>iload_3</i> instruction.
     * @param inst <i>iload_3</i> instruction.
     */
    public void visitIload_3( Iload_3 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 3, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>lload_0</i> instruction.
     * @param inst <i>lload_0</i> instruction.
     */
    public void visitLload_0( Lload_0 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 0, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>lload_1</i> instruction.
     * @param inst <i>lload_1</i> instruction.
     */
    public void visitLload_1( Lload_1 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 1, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>lload_2</i> instruction.
     * @param inst <i>lload_2</i> instruction.
     */
    public void visitLload_2( Lload_2 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 2, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>lload_3</i> instruction.
     * @param inst <i>lload_3</i> instruction.
     */
    public void visitLload_3( Lload_3 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 3, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>fload_0</i> instruction.
     * @param inst <i>fload_0</i> instruction.
     */
    public void visitFload_0( Fload_0 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 0, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>fload_1</i> instruction.
     * @param inst <i>fload_1</i> instruction.
     */
    public void visitFload_1( Fload_1 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 1, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>fload_2</i> instruction.
     * @param inst <i>fload_2</i> instruction.
     */
    public void visitFload_2( Fload_2 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 2, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>fload_3</i> instruction.
     * @param inst <i>fload_3</i> instruction.
     */
    public void visitFload_3( Fload_3 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 3, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>dload_0</i> instruction.
     * @param inst <i>dload_0</i> instruction.
     */
    public void visitDload_0( Dload_0 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 0, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>dload_1</i> instruction.
     * @param inst <i>dload_1</i> instruction.
     */
    public void visitDload_1( Dload_1 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 1, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>dload_2</i> instruction.
     * @param inst <i>dload_2</i> instruction.
     */
    public void visitDload_2( Dload_2 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 2, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>dload_3</i> instruction.
     * @param inst <i>dload_3</i> instruction.
     */
    public void visitDload_3( Dload_3 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 3, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>aload_0</i> instruction.
     * @param inst <i>aload_0</i> instruction.
     */
    public void visitAload_0( Aload_0 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 0, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>aload_1</i> instruction.
     * @param inst <i>aload_1</i> instruction.
     */
    public void visitAload_1( Aload_1 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 1, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>aload_2</i> instruction.
     * @param inst <i>aload_2</i> instruction.
     */
    public void visitAload_2( Aload_2 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 2, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>aload_3</i> instruction.
     * @param inst <i>aload_3</i> instruction.
     */
    public void visitAload_3( Aload_3 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 3, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>iaload</i> instruction.
     * @param inst <i>iaload</i> instruction.
     */
    public void visitIaload( Iaload inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>laload</i> instruction.
     * @param inst <i>laload</i> instruction.
     */
    public void visitLaload( Laload inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>faload</i> instruction.
     * @param inst <i>faload</i> instruction.
     */
    public void visitFaload( Faload inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>daload</i> instruction.
     * @param inst <i>daload</i> instruction.
     */
    public void visitDaload( Daload inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>aaload</i> instruction.
     * @param inst <i>aaload</i> instruction.
     */
    public void visitAaload( Aaload inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>baload</i> instruction.
     * @param inst <i>baload</i> instruction.
     */
    public void visitBaload( Baload inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>caload</i> instruction.
     * @param inst <i>caload</i> instruction.
     */
    public void visitCaload( Caload inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>saload</i> instruction.
     * @param inst <i>saload</i> instruction.
     */
    public void visitSaload( Saload inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>istore</i> instruction.
     * @param inst <i>istore</i> instruction.
     */
    public void visitIstore( Istore inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, (bytecode[offset++] & 0x000000ff),
                            inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>lstore</i> instruction.
     * @param inst <i>lstore</i> instruction.
     */
    public void visitLstore( Lstore inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, (bytecode[offset++] & 0x000000ff),
                            inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>fstore</i> instruction.
     * @param inst <i>fstore</i> instruction.
     */
    public void visitFstore( Fstore inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, (bytecode[offset++] & 0x000000ff),
                            inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>dstore</i> instruction.
     * @param inst <i>dstore</i> instruction.
     */
    public void visitDstore( Dstore inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, (bytecode[offset++] & 0x000000ff),
                            inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>astore</i> instruction.
     * @param inst <i>astore</i> instruction.
     */
    public void visitAstore( Astore inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, (bytecode[offset++] & 0x000000ff),
                            inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>istore_0</i> instruction.
     * @param inst <i>istore_0</i> instruction.
     */
    public void visitIstore_0( Istore_0 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 0, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>istore_1</i> instruction.
     * @param inst <i>istore_1</i> instruction.
     */
    public void visitIstore_1( Istore_1 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 1, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>istore_2</i> instruction.
     * @param inst <i>istore_2</i> instruction.
     */
    public void visitIstore_2( Istore_2 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 2, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>istore_3</i> instruction.
     * @param inst <i>istore_3</i> instruction.
     */
    public void visitIstore_3( Istore_3 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 3, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>lstore_0</i> instruction.
     * @param inst <i>lstore_0</i> instruction.
     */
    public void visitLstore_0( Lstore_0 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 0, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>lstore_1</i> instruction.
     * @param inst <i>lstore_1</i> instruction.
     */
    public void visitLstore_1( Lstore_1 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 1, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>lstore_2</i> instruction.
     * @param inst <i>lstore_2</i> instruction.
     */
    public void visitLstore_2( Lstore_2 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 2, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>lstore_3</i> instruction.
     * @param inst <i>lstore_3</i> instruction.
     */
    public void visitLstore_3( Lstore_3 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 3, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>fstore_0</i> instruction.
     * @param inst <i>fstore_0</i> instruction.
     */
    public void visitFstore_0( Fstore_0 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 0, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>fstore_1</i> instruction.
     * @param inst <i>fstore_1</i> instruction.
     */
    public void visitFstore_1( Fstore_1 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 1, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>fstore_2</i> instruction.
     * @param inst <i>fstore_2</i> instruction.
     */
    public void visitFstore_2( Fstore_2 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 2, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>fstore_3</i> instruction.
     * @param inst <i>fstore_3</i> instruction.
     */
    public void visitFstore_3( Fstore_3 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 3, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>dstore_0</i> instruction.
     * @param inst <i>dstore_0</i> instruction.
     */
    public void visitDstore_0( Dstore_0 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 0, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>dstore_1</i> instruction.
     * @param inst <i>dstore_1</i> instruction.
     */
    public void visitDstore_1( Dstore_1 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 1, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>dstore_2</i> instruction.
     * @param inst <i>dstore_2</i> instruction.
     */
    public void visitDstore_2( Dstore_2 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 2, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>dstore_3</i> instruction.
     * @param inst <i>dstore_3</i> instruction.
     */
    public void visitDstore_3( Dstore_3 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 3, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>astore_0</i> instruction.
     * @param inst <i>astore_0</i> instruction.
     */
    public void visitAstore_0( Astore_0 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 0, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>astore_1</i> instruction.
     * @param inst <i>astore_1</i> instruction.
     */
    public void visitAstore_1( Astore_1 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 1, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>astore_2</i> instruction.
     * @param inst <i>astore_2</i> instruction.
     */
    public void visitAstore_2( Astore_2 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 2, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>astore_3</i> instruction.
     * @param inst <i>astore_3</i> instruction.
     */
    public void visitAstore_3( Astore_3 inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, 3, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>iastore</i> instruction.
     * @param inst <i>iastore</i> instruction.
     */
    public void visitIastore( Iastore inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>lastore</i> instruction.
     * @param inst <i>lastore</i> instruction.
     */
    public void visitLastore( Lastore inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>fastore</i> instruction.
     * @param inst <i>fastore</i> instruction.
     */
    public void visitFastore( Fastore inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dastore</i> instruction.
     * @param inst <i>dastore</i> instruction.
     */
    public void visitDastore( Dastore inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>aastore</i> instruction.
     * @param inst <i>aastore</i> instruction.
     */
    public void visitAastore( Aastore inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>bastore</i> instruction.
     * @param inst <i>bastore</i> instruction.
     */
    public void visitBastore( Bastore inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>castore</i> instruction.
     * @param inst <i>castore</i> instruction.
     */
    public void visitCastore( Castore inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>sastore</i> instruction.
     * @param inst <i>sastore</i> instruction.
     */
    public void visitSastore( Sastore inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>pop</i> instruction.
     * @param inst <i>pop</i> instruction.
     */
    public void visitPop( Pop inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>pop2</i> instruction.
     * @param inst <i>pop2</i> instruction.
     */
    public void visitPop2( Pop2 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dup</i> instruction.
     * @param inst <i>dup</i> instruction.
     */
    public void visitDup( Dup inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dup_x1</i> instruction.
     * @param inst <i>dup_x1</i> instruction.
     */
    public void visitDup_x1( Dup_x1 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dup_x2</i> instruction.
     * @param inst <i>dup_x2</i> instruction.
     */
    public void visitDup_x2( Dup_x2 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dup2</i> instruction.
     * @param inst <i>dup2</i> instruction.
     */
    public void visitDup2( Dup2 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dup2_x1</i> instruction.
     * @param inst <i>dup2_x1</i> instruction.
     */
    public void visitDup2_x1( Dup2_x1 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dup2_x2</i> instruction.
     * @param inst <i>dup2_x2</i> instruction.
     */
    public void visitDup2_x2( Dup2_x2 inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>swap</i> instruction.
     * @param inst <i>swap</i> instruction.
     */
    public void visitSwap( Swap inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>iadd</i> instruction.
     * @param inst <i>iadd</i> instruction.
     */
    public void visitIadd( Iadd inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>ladd</i> instruction.
     * @param inst <i>ladd</i> instruction.
     */
    public void visitLadd( Ladd inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>fadd</i> instruction.
     * @param inst <i>fadd</i> instruction.
     */
    public void visitFadd( Fadd inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dadd</i> instruction.
     * @param inst <i>dadd</i> instruction.
     */
    public void visitDadd( Dadd inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>isub</i> instruction.
     * @param inst <i>isub</i> instruction.
     */
    public void visitIsub( Isub inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>lsub</i> instruction.
     * @param inst <i>lsub</i> instruction.
     */
    public void visitLsub( Lsub inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>fsub</i> instruction.
     * @param inst <i>fsub</i> instruction.
     */
    public void visitFsub( Fsub inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dsub</i> instruction.
     * @param inst <i>dsub</i> instruction.
     */
    public void visitDsub( Dsub inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>imul</i> instruction.
     * @param inst <i>imul</i> instruction.
     */
    public void visitImul( Imul inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>lmul</i> instruction.
     * @param inst <i>lmul</i> instruction.
     */
    public void visitLmul( Lmul inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>fmul</i> instruction.
     * @param inst <i>fmul</i> instruction.
     */
    public void visitFmul( Fmul inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dmul</i> instruction.
     * @param inst <i>dmul</i> instruction.
     */
    public void visitDmul( Dmul inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>idiv</i> instruction.
     * @param inst <i>idiv</i> instruction.
     */
    public void visitIdiv( Idiv inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>ldiv</i> instruction.
     * @param inst <i>ldiv</i> instruction.
     */
    public void visitLdiv( Ldiv inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>fdiv</i> instruction.
     * @param inst <i>fdiv</i> instruction.
     */
    public void visitFdiv( Fdiv inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>ddiv</i> instruction.
     * @param inst <i>ddiv</i> instruction.
     */
    public void visitDdiv( Ddiv inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>irem</i> instruction.
     * @param inst <i>irem</i> instruction.
     */
    public void visitIrem( Irem inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>lrem</i> instruction.
     * @param inst <i>lrem</i> instruction.
     */
    public void visitLrem( Lrem inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>frem</i> instruction.
     * @param inst <i>frem</i> instruction.
     */
    public void visitFrem( Frem inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>drem</i> instruction.
     * @param inst <i>drem</i> instruction.
     */
    public void visitDrem( Drem inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>ineg</i> instruction.
     * @param inst <i>ineg</i> instruction.
     */
    public void visitIneg( Ineg inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>lneg</i> instruction.
     * @param inst <i>lneg</i> instruction.
     */
    public void visitLneg( Lneg inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>fneg</i> instruction.
     * @param inst <i>fneg</i> instruction.
     */
    public void visitFneg( Fneg inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dneg</i> instruction.
     * @param inst <i>dneg</i> instruction.
     */
    public void visitDneg( Dneg inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>ishl</i> instruction.
     * @param inst <i>ishl</i> instruction.
     */
    public void visitIshl( Ishl inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>lshl</i> instruction.
     * @param inst <i>lshl</i> instruction.
     */
    public void visitLshl( Lshl inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>ishr</i> instruction.
     * @param inst <i>ishr</i> instruction.
     */
    public void visitIshr( Ishr inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>lshr</i> instruction.
     * @param inst <i>lshr</i> instruction.
     */
    public void visitLshr( Lshr inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>iushr</i> instruction.
     * @param inst <i>iushr</i> instruction.
     */
    public void visitIushr( Iushr inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>lushr</i> instruction.
     * @param inst <i>lushr</i> instruction.
     */
    public void visitLushr( Lushr inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>iand</i> instruction.
     * @param inst <i>iand</i> instruction.
     */
    public void visitIand( Iand inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>land</i> instruction.
     * @param inst <i>land</i> instruction.
     */
    public void visitLand( Land inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>ior</i> instruction.
     * @param inst <i>ior</i> instruction.
     */
    public void visitIor( Ior inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>lor</i> instruction.
     * @param inst <i>lor</i> instruction.
     */
    public void visitLor( Lor inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>ixor</i> instruction.
     * @param inst <i>ixor</i> instruction.
     */
    public void visitIxor( Ixor inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>lxor</i> instruction.
     * @param inst <i>lxor</i> instruction.
     */
    public void visitLxor( Lxor inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>iinc</i> instruction.
     * @param inst <i>iinc</i> instruction.
     */
    public void visitIinc( Iinc inst ) {
        setOpcodeAndOffset( inst );
        setLocalVarOperand( inst, (bytecode[offset++] & 0x000000ff),
                            inst.getByteCodeOffset() );
        offset++;  // skip constant
    }

    /**
     * Visits the <i>i2l</i> instruction.
     * @param inst <i>i2l</i> instruction.
     */
    public void visitI2l( I2l inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>i2f</i> instruction.
     * @param inst <i>i2f</i> instruction.
     */
    public void visitI2f( I2f inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>i2d</i> instruction.
     * @param inst <i>i2d</i> instruction.
     */
    public void visitI2d( I2d inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>l2i</i> instruction.
     * @param inst <i>l2i</i> instruction.
     */
    public void visitL2i( L2i inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>l2f</i> instruction.
     * @param inst <i>l2f</i> instruction.
     */
    public void visitL2f( L2f inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>l2d</i> instruction.
     * @param inst <i>l2d</i> instruction.
     */
    public void visitL2d( L2d inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>f2i</i> instruction.
     * @param inst <i>f2i</i> instruction.
     */
    public void visitF2i( F2i inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>f2l</i> instruction.
     * @param inst <i>f2l</i> instruction.
     */
    public void visitF2l( F2l inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>f2d</i> instruction.
     * @param inst <i>f2d</i> instruction.
     */
    public void visitF2d( F2d inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>d2i</i> instruction.
     * @param inst <i>d2i</i> instruction.
     */
    public void visitD2i( D2i inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>d2l</i> instruction.
     * @param inst <i>d2l</i> instruction.
     */
    public void visitD2l( D2l inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>d2f</i> instruction.
     * @param inst <i>d2f</i> instruction.
     */
    public void visitD2f( D2f inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>i2b</i> instruction.
     * @param inst <i>i2b</i> instruction.
     */
    public void visitI2b( I2b inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>i2c</i> instruction.
     * @param inst <i>i2c</i> instruction.
     */
    public void visitI2c( I2c inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>i2s</i> instruction.
     * @param inst <i>i2s</i> instruction.
     */
    public void visitI2s( I2s inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>lcmp</i> instruction.
     * @param inst <i>lcmp</i> instruction.
     */
    public void visitLcmp( Lcmp inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>fcmpl</i> instruction.
     * @param inst <i>fcmpl</i> instruction.
     */
    public void visitFcmpl( Fcmpl inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>fcmpg</i> instruction.
     * @param inst <i>fcmpg</i> instruction.
     */
    public void visitFcmpg( Fcmpg inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dcmpl</i> instruction.
     * @param inst <i>dcmpl</i> instruction.
     */
    public void visitDcmpl( Dcmpl inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dcmpg</i> instruction.
     * @param inst <i>dcmpg</i> instruction.
     */
    public void visitDcmpg( Dcmpg inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>ifeq</i> instruction.
     * @param inst <i>ifeq</i> instruction.
     */
    public void visitIfeq( Ifeq inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>ifne</i> instruction.
     * @param inst <i>ifne</i> instruction.
     */
    public void visitIfne( Ifne inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>iflt</i> instruction.
     * @param inst <i>iflt</i> instruction.
     */
    public void visitIflt( Iflt inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>ifge</i> instruction.
     * @param inst <i>ifge</i> instruction.
     */
    public void visitIfge( Ifge inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>ifgt</i> instruction.
     * @param inst <i>ifgt</i> instruction.
     */
    public void visitIfgt( Ifgt inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>ifle</i> instruction.
     * @param inst <i>ifle</i> instruction.
     */
    public void visitIfle( Ifle inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>if_icmpeq</i> instruction.
     * @param inst <i>if_icmpeq</i> instruction.
     */
    public void visitIf_icmpeq( If_icmpeq inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>if_icmpne</i> instruction.
     * @param inst <i>if_icmpne</i> instruction.
     */
    public void visitIf_icmpne( If_icmpne inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>if_icmplt</i> instruction.
     * @param inst <i>if_icmplt</i> instruction.
     */
    public void visitIf_icmplt( If_icmplt inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>if_icmpge</i> instruction.
     * @param inst <i>if_icmpge</i> instruction.
     */
    public void visitIf_icmpge( If_icmpge inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>if_icmpgt</i> instruction.
     * @param inst <i>if_icmpgt</i> instruction.
     */
    public void visitIf_icmpgt( If_icmpgt inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>if_icmple</i> instruction.
     * @param inst <i>if_icmple</i> instruction.
     */
    public void visitIf_icmple( If_icmple inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>if_acmpeq</i> instruction.
     * @param inst <i>if_acmpeq</i> instruction.
     */
    public void visitIf_acmpeq( If_acmpeq inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>if_acmpne</i> instruction.
     * @param inst <i>if_acmpne</i> instruction.
     */
    public void visitIf_acmpne( If_acmpne inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>goto</i> instruction.
     * @param inst <i>goto</i> instruction.
     */
    public void visitGoto( Goto inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>jsr</i> instruction.
     * @param inst <i>jsr</i> instruction.
     */
    public void visitJsr( Jsr inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>ret</i> instruction.
     * @param inst <i>ret</i> instruction.
     */
    public void visitRet( Ret inst ) {
        setOpcodeAndOffset( inst );
        offset++;  // ignore index
    }

    /**
     * Visits the <i>tableswitch</i> instruction.
     * @param inst <i>tableswitch</i> instruction.
     */
    public void visitTableswitch( Tableswitch inst ) {
        setOpcodeAndOffset( inst );
        int opcodeOffset = inst.getByteCodeOffset();

        // compute the number of pad bytes
        int padLength = 3 - (opcodeOffset % 4);
        offset += padLength;  // start offset of default address

        // compute default offset
        int defaultOffset = (bytecode[offset++] << 24) |
                            ((bytecode[offset++] << 16) & 0x00ff0000) |
                            ((bytecode[offset++] << 8) & 0x0000ff00) |
                            (bytecode[offset++] & 0x000000ff);
        // compute low value
        int lowValue = (bytecode[offset++] << 24) |
                       ((bytecode[offset++] << 16) & 0x00ff0000) |
                       ((bytecode[offset++] << 8) & 0x0000ff00) |
                       (bytecode[offset++] & 0x000000ff);
        // compute high value
        int highValue = (bytecode[offset++] << 24) |
                        ((bytecode[offset++] << 16) & 0x00ff0000) |
                        ((bytecode[offset++] << 8) & 0x0000ff00) |
                        (bytecode[offset++] & 0x000000ff);

        // access (highValue-lowValue+1) remaining jump offsets
        int numJumpOffsets = highValue - lowValue + 1;
        int[] jumpOffsets = new int[numJumpOffsets];
        for ( int i=0; i<numJumpOffsets; i++ ) {
            jumpOffsets[i] = (bytecode[offset++] << 24) |
                             ((bytecode[offset++] << 16) & 0x00ff0000) |
                             ((bytecode[offset++] << 8) & 0x0000ff00) |
                             (bytecode[offset++] & 0x000000ff);
        }
        // compute jump addresses and store as operands
        inst.addOperand( new Integer( defaultOffset+opcodeOffset ) );
        for ( int i=0; i<numJumpOffsets; i++ ) {
            inst.addOperand( new Integer( jumpOffsets[i]+opcodeOffset ) );
        }
    }

    /**
     * Visits the <i>lookupswitch</i> instruction.
     * @param inst <i>lookupswitch</i> instruction.
     */
    public void visitLookupswitch( Lookupswitch inst ) {
        setOpcodeAndOffset( inst );
        int opcodeOffset = inst.getByteCodeOffset();

        // compute the number of pad bytes
        int padLength = 3 - (opcodeOffset % 4);
        offset += padLength;  // start offset of default address

        // compute default offset
        int defaultOffset = (bytecode[offset++] << 24) |
                            ((bytecode[offset++] << 16) & 0x00ff0000) |
                            ((bytecode[offset++] << 8) & 0x0000ff00) |
                            (bytecode[offset++] & 0x000000ff);
        // compute npairs
        int npairs = (bytecode[offset++] << 24) |
                     ((bytecode[offset++] << 16) & 0x00ff0000) |
                     ((bytecode[offset++] << 8) & 0x0000ff00) |
                     (bytecode[offset++] & 0x000000ff);

        // access (npairs) match-offset pairs; record match and offsets as
        // operands of the instruction
        int[] jumpOffsets = new int[npairs];
        int[] matchValues = new int[npairs];
        for ( int i=0; i<npairs; i++ ) {
            matchValues[i] = ((bytecode[offset++] << 24) & 0xff000000) |
                             ((bytecode[offset++] << 16) & 0x00ff0000) |
                             ((bytecode[offset++] << 8) & 0x0000ff00) |
                             (bytecode[offset++] & 0x000000ff);
            jumpOffsets[i] = (bytecode[offset++] << 24) |
                             ((bytecode[offset++] << 16) & 0x00ff0000) |
                             ((bytecode[offset++] << 8) & 0x0000ff00) |
                             (bytecode[offset++] & 0x000000ff);
        }
        // compute jump addresses and store as operands
        inst.addOperand( new Integer( defaultOffset+opcodeOffset ) );
        for ( int i=0; i<npairs; i++ ) {
            inst.addOperand( new Integer( matchValues[i] ) );
            inst.addOperand( new Integer( jumpOffsets[i]+opcodeOffset ) );
        }
    }

    /**
     * Visits the <i>ireturn</i> instruction.
     * @param inst <i>ireturn</i> instruction.
     */
    public void visitIreturn( Ireturn inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>lreturn</i> instruction.
     * @param inst <i>lreturn</i> instruction.
     */
    public void visitLreturn( Lreturn inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>freturn</i> instruction.
     * @param inst <i>freturn</i> instruction.
     */
    public void visitFreturn( Freturn inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>dreturn</i> instruction.
     * @param inst <i>dreturn</i> instruction.
     */
    public void visitDreturn( Dreturn inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>areturn</i> instruction.
     * @param inst <i>areturn</i> instruction.
     */
    public void visitAreturn( Areturn inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>return</i> instruction.
     * @param inst <i>return</i> instruction.
     */
    public void visitReturn( Return inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>getstatic</i> instruction.
     * @param inst <i>getstatic</i> instruction.
     */
    public void visitGetstatic( Getstatic inst ) {
        setOpcodeAndOffset( inst );
        setSymbolTableObjectOperand( inst, new String[] { "jaba.sym.FieldImpl" } );
    }

    /**
     * Visits the <i>putstatic</i> instruction.
     * @param inst <i>putstatic</i> instruction.
     */
    public void visitPutstatic( Putstatic inst ) {
        setOpcodeAndOffset( inst );
        setSymbolTableObjectOperand( inst, new String[] { "jaba.sym.FieldImpl" } );
    }

    /**
     * Visits the <i>getfield</i> instruction.
     * @param inst <i>getfield</i> instruction.
     */
    public void visitGetfield( Getfield inst ) {
        setOpcodeAndOffset( inst );
        setSymbolTableObjectOperand( inst, new String[] { "jaba.sym.FieldImpl" } );
    }

    /**
     * Visits the <i>putfield</i> instruction.
     * @param inst <i>putfield</i> instruction.
     */
    public void visitPutfield( Putfield inst ) {
        setOpcodeAndOffset( inst );
        setSymbolTableObjectOperand( inst, new String[] { "jaba.sym.FieldImpl" } );
    }

    /**
     * Visits the <i>invokevirtual</i> instruction.
     * @param inst <i>invokevirtual</i> instruction.
     */
    public void visitInvokevirtual( Invokevirtual inst ) {
        setOpcodeAndOffset( inst );
        setSymbolTableObjectOperand( inst, new String[]
				     { "jaba.sym.MethodImpl" } );
    }

    /**
     * Visits the <i>invokespecial</i> instruction.
     * @param inst <i>invokespecial</i> instruction.
     */
    public void visitInvokespecial( Invokespecial inst ) {
        setOpcodeAndOffset( inst );
        setSymbolTableObjectOperand( inst, new String[]
				     { "jaba.sym.MethodImpl" } );
    }

    /**
     * Visits the <i>invokestatic</i> instruction.
     * @param inst <i>invokestatic</i> instruction.
     */
    public void visitInvokestatic( Invokestatic inst ) {
        setOpcodeAndOffset( inst );
        setSymbolTableObjectOperand( inst, new String[]
				     { "jaba.sym.MethodImpl" } );
    }

    /**
     * Visits the <i>invokeinterface</i> instruction.
     * @param inst <i>invokeinterface</i> instruction.
     */
    public void visitInvokeinterface( Invokeinterface inst ) {
        setOpcodeAndOffset( inst );
        setSymbolTableObjectOperand( inst, new String[]
				     { "jaba.sym.MethodImpl" } );
        offset += 2;  // ignore nargs and 0
    }

    /**
     * Visits the <i>new</i> instruction.
     * @param inst <i>new</i> instruction.
     */
    public void visitNew( New inst ) {
        setOpcodeAndOffset( inst );
        setSymbolTableObjectOperand( inst, new String[] { "jaba.sym.ClassImpl" } );
    }

    /**
     * Visits the <i>newarray</i> instruction.
     * @param inst <i>newarray</i> instruction.
     */
    public void visitNewarray( Newarray inst ) {
        setOpcodeAndOffset( inst );
        Array array = new ArrayImpl ();
	Primitive type = null;
	switch ( bytecode[offset++] ) {
	case Newarray.T_BOOLEAN: 
	    type = PrimitiveImpl.valueOf(Primitive.BOOLEAN );
	    break;
	case Newarray.T_CHAR:    
	    type = PrimitiveImpl.valueOf(Primitive.CHAR );
	    break;
	case Newarray.T_FLOAT:   
	    type = PrimitiveImpl.valueOf(Primitive.FLOAT ); 
	    break;
	case Newarray.T_DOUBLE:  
	    type = PrimitiveImpl.valueOf(Primitive.DOUBLE ); 
	    break;
	case Newarray.T_BYTE:    
	    type = PrimitiveImpl.valueOf(Primitive.BYTE ); 
	    break;
	case Newarray.T_SHORT:   
	    type = PrimitiveImpl.valueOf(Primitive.SHORT ); 
	    break;
	case Newarray.T_INT:     
	    type = PrimitiveImpl.valueOf(Primitive.INT ); 
	    break;
	case Newarray.T_LONG:    
	    type = PrimitiveImpl.valueOf(Primitive.LONG );
	    break;
	default:
	    assert false: inst.toString() + "--" + bytecode[offset-1];
	}

        array.setElementType( type );
	array = ((jaba.sym.SymbolTable) method.getContainingType ().getProgram ().getSymbolTable ()).forArray (array);
        inst.addOperand( array );
    }

    /**
     * Visits the <i>anewarray</i> instruction.
     * @param inst <i>anewarray</i> instruction.
     */
    public void visitAnewarray( Anewarray inst ) {
        setOpcodeAndOffset( inst );
        int index = ((bytecode[offset++] << 8) & 0x0000ff00) |
                    (bytecode[offset++] & 0x000000ff);
        Integer key = new Integer( index );
        Object obj = constantPoolMap.get( key );
        if ( obj == null ) {
	    throw new RuntimeException( "Error: " +
		"InstructionLoadingVisitor.visitAnewarray(): "+
		"constant pool index "+index+
                " does not map to a SymbolTableEntry object");
	}
        if ( !(obj instanceof Class) && !(obj instanceof Interface) &&
             !(obj instanceof Array) ) {
	    throw new RuntimeException("Error: " +
                "InstructionLoadingVisitor.visitAnewarray(): "+
                "constant pool index "+index+
                " does not map to expected Class, Interface, or Array type: "+
                obj.getClass().getName());
	}
        Array array = new ArrayImpl ();
        array.setElementType( (ReferenceType)obj );
	array = ((jaba.sym.SymbolTable) method.getContainingType ().getProgram ().getSymbolTable ()).forArray (array);
        inst.addOperand( array );
    }

    /**
     * Visits the <i>arraylength</i> instruction.
     * @param inst <i>arraylength</i> instruction.
     */
    public void visitArraylength( Arraylength inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>athrow</i> instruction.
     * @param inst <i>athrow</i> instruction.
     */
    public void visitAthrow( Athrow inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>checkcast</i> instruction.
     * @param inst <i>checkcast</i> instruction.
     */
    public void visitCheckcast( Checkcast inst ) {
        setOpcodeAndOffset( inst );
        setSymbolTableObjectOperand( inst, new String[]
				     { "jaba.sym.ClassImpl",
				       "jaba.sym.InterfaceImpl",
                                       "jaba.sym.ArrayImpl" } );
    }

    /**
     * Visits the <i>instanceof</i> instruction.
     * @param inst <i>instanceof</i> instruction.
     */
    public void visitInstanceof( Instanceof inst ) {
        setOpcodeAndOffset( inst );
        setSymbolTableObjectOperand( inst, new String[]
				     { "jaba.sym.ClassImpl",
				       "jaba.sym.InterfaceImpl",
                                       "jaba.sym.ArrayImpl" } );
    }

    /**
     * Visits the <i>monitorenter</i> instruction.
     * @param inst <i>monitorenter</i> instruction.
     */
    public void visitMonitorenter( Monitorenter inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>monitorexit</i> instruction.
     * @param inst <i>monitorexit</i> instruction.
     */
    public void visitMonitorexit( Monitorexit inst ) {
        setOpcodeAndOffset( inst );
    }

    /**
     * Visits the <i>wide</i> instruction.
     * @param inst <i>wide</i> instruction.
     */
    public void visitWide( Wide inst ) {
        setOpcodeAndOffset( inst );
        byte encapsulatedInst = bytecode[offset++];
        inst.setEncapsulatedOpcode( encapsulatedInst );
        int index = ((bytecode[offset++] << 8) & 0x0000ff00) |
                     (bytecode[offset++] & 0x000000ff);
        setLocalVarOperand( inst, index, inst.getByteCodeOffset() );
        if ( encapsulatedInst == OpCode.OP_IINC ) {
            offset += 2;  // ignore const
        }
    }

    /**
     * Visits the <i>multianewarray</i> instruction.
     * @param inst <i>multianewarray</i> instruction.
     */
    public void visitMultianewarray( Multianewarray inst ) {
        setOpcodeAndOffset( inst );
        setSymbolTableObjectOperand( inst, new String[] { "jaba.sym.ArrayImpl" } );
        int val = bytecode[offset++];
        inst.addOperand( new Integer( val ) );
    }

    /**
     * Visits the <i>ifnull</i> instruction.
     * @param inst <i>ifnull</i> instruction.
     */
    public void visitIfnull( Ifnull inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>ifnonnull</i> instruction.
     * @param inst <i>ifnonnull</i> instruction.
     */
    public void visitIfnonnull( Ifnonnull inst ) {
        setOpcodeAndOffset( inst );
        setJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>goto_w</i> instruction.
     * @param inst <i>goto_w</i> instruction.
     */
    public void visitGoto_w( Goto_w inst ) {
        setOpcodeAndOffset( inst );
        setWideJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Visits the <i>jsr_w</i> instruction.
     * @param inst <i>jsr_w</i> instruction.
     */
    public void visitJsr_w( Jsr_w inst ) {
        setOpcodeAndOffset( inst );
        setWideJumpAddressOperand( inst, inst.getByteCodeOffset() );
    }

    /**
     * Sets the opcode and bytecode offset for a symbolic instruction.
     * @param inst symbolic instruction set opcode and offset for,
     */
    private void setOpcodeAndOffset( SymbolicInstruction inst ) {
        inst.setByteCodeOffset( offset );
        inst.setOpcode( bytecode[offset++] );
    }

    /**
     * Sets the operand for instructions that take a local variable as an
     * operand.
     * @param inst symbolic instruction to set operand for.
     * @param index index into the local variables of the method's stack frame.
     * @param opcodeOffset offset in the bytecode array for <code>inst</code>.
     * @see jaba.classfile.LocalVariableTableAttribute#getLocalVariableTableEntryIndex        
     */
    private void setLocalVarOperand( SymbolicInstruction inst, int index,
                                     int opcodeOffset )
    {
        int i;
        if ( index >= 256 ) {  // wide instruction
             i = localVarTable.getLocalVariableTableEntryIndex( index,
                 opcodeOffset, 3 );
        }
        else if ( index >= 4 ) {  // load/store instruction
             i = localVarTable.getLocalVariableTableEntryIndex( index,
                 opcodeOffset, 2 );
        }
        else {   // load_n/store_n instruction
             i = localVarTable.getLocalVariableTableEntryIndex( index,
                 opcodeOffset, 1 );
        }
        if ( i == -1 ) {
/* commenting this out for now...
            System.err.println(
                "WARNING: InstructionLoadingVisitor.setLocalVarOperand(): "+
                "No entry found in LocalVarTable for index "+index+
                " and offset "+opcodeOffset );
*/
            // add the local var index as the integer operand of the
            // instruction
            inst.addOperand( new Integer( index ) );
            return;
        }
        LocalVariable var;
        if ( (var = localVarMap.get( new Integer( i ) )) == null ) {
	    RuntimeException e = new RuntimeException();
	    e.printStackTrace();
	    throw e;
}
        inst.addOperand( var );
    }

    /**
     * Sets the operand for instructions that take a jump address as an
     * operand.
     * @param inst symbolic instruction to set operand for.
     * @param opcodeOffset offset in the bytecode array for <code>inst</code>.
     */
    private void setJumpAddressOperand( SymbolicInstruction inst,
                                        int opcodeOffset )
    {
        int branchAddr = ((bytecode[offset++] << 8) |
                          (bytecode[offset++] & 0x000000ff)) + opcodeOffset;
        inst.addOperand( new Integer( branchAddr ) );
    }

    /**
     * Sets the operand for instructions that take a wide jump address as an
     * operand.
     * @param inst symbolic instruction to set operand for.
     * @param opcodeOffset offset in the bytecode array for <code>inst</code>.
     */
    private void setWideJumpAddressOperand( SymbolicInstruction inst,
                                            int opcodeOffset )
    {
        int branchAddr = ((bytecode[offset++] << 24) |
                         ((bytecode[offset++] << 16) & 0x00ff0000) |
                         ((bytecode[offset++] << 8) & 0x00ff0000) |
                         (bytecode[offset++] & 0x000000ff)) + opcodeOffset;
        inst.addOperand( new Integer( branchAddr ) );
    }

    /**
     * Sets the operand for instructions that take a 
     * {@link jaba.sym.SymbolTableEntry SymbolTableEntry}
     * object as an operand.
     * @param inst symbolic instruction to set operand for.
     * @param expectedTypes array of strings that denote the expected types of
     *               of the oparand.
     */
    private void setSymbolTableObjectOperand( SymbolicInstruction inst,
                                              String[] expectedTypes )
    {
        // compute constant-pool index of object being accessed
        int index = ((bytecode[offset++] << 8) & 0x0000ff00) |
                    (bytecode[offset++] & 0x000000ff);
        Integer key = new Integer( index );
        Object obj = constantPoolMap.get( key );
        if ( obj == null ) {
	    RuntimeException e = new RuntimeException();
	    e.printStackTrace();
	    throw e;
	}
	boolean found = false;
	for ( int i=0; i<expectedTypes.length; i++ ) {
            if ( expectedTypes[i].equals( obj.getClass().getName() ) ) {
		found = true;
		break;
	    }
	}
	if ( !found ) {
	    String msg = "Error: " +
	        "InstructionLoadingVisitor."+
		"setSymbolTableObjectOperandload(): "+
                "constant pool index "+index+" does not map to expected ";
            for ( int i=0; i<expectedTypes.length; i++ ) {
		msg += expectedTypes[i] + " ";
	    }
            msg += "type: "+ obj.getClass().getName();
	    RuntimeException e = new RuntimeException( msg );
	    e.printStackTrace();
	    throw e;
	}
        inst.addOperand( obj );
    }

}
