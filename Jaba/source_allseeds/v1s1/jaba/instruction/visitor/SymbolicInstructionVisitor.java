/* Copyright (c) 1999, The Ohio State University */

package jaba.instruction.visitor;

import jaba.instruction.*;

/**
 * Abstract super class to all visitors for symbolic instructions.
 * @author Jim Jones
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to an interface and removed methods for client-server simplicity/clarity.
 */
public interface SymbolicInstructionVisitor extends java.io.Serializable
{
}
