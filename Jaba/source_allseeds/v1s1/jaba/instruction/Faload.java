/* Copyright (c) 1999, The Ohio State University */

package jaba.instruction;

import jaba.instruction.visitor.SymbolicInstructionVisitor;

/**
 * <i>faload</i> JVM bytecode instruction.
 * @author Jim Jones/Saurabh Sinha
 */
public class Faload extends SymbolicInstructionImpl
{

    /** Runtime exceptions. */
    private static final String[]
        runtimeExceptions = { "java.lang.NullPointerException",
                              "java.lang.ArrayIndexOutOfBoundsException" };

  /**
   * Method to facilitate Visitor design pattern.
   * @param visitor  Visitor class to be used to visit this statement.
   * @see jaba.instruction.visitor.SymbolicInstructionVisitor
   */
  public void accept( SymbolicInstructionVisitor visitor )
    {
      ((jaba.instruction.visitor.SymbolicInstructionVisitorImpl) visitor).visitFaload( this );
    }

    /**
     * Returns the list of runtime exceptions that can be raised by this
     * instruction.
     * @return String[] Runtime exceptions that can be raised by this
     *                  instruction.
     */
    public String[] getRuntimeExceptions() {
        return runtimeExceptions;
    }

}
