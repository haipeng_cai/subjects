/* Copyright (c) 1999, The Ohio State University */

package jaba.instruction;

import jaba.instruction.visitor.SymbolicInstructionVisitor;

/**
 * Represents <i>castore</i> JVM bytecode instruction.Pops a 32-bit integer 
 * from the stack, truncates it to a 16-bit unsigned value, and stores it in 
 * an array of characters.
 * @author Jim Jones/Saurabh Sinha
 */
public class Castore extends SymbolicInstructionImpl
{

    /** Runtime exceptions. */
    private static final String[] 
	runtimeExceptions = { "java.lang.NullPointerException",
			      "java.lang.ArrayIndexOutOfBoundsException" };
    
    /**
     * Method to facilitate Visitor design pattern.
     * @param visitor  Visitor class to be used to visit this statement.
     * @see jaba.instruction.visitor.SymbolicInstructionVisitor
     */
    public void accept( SymbolicInstructionVisitor visitor )
    {
	((jaba.instruction.visitor.SymbolicInstructionVisitorImpl) visitor).visitCastore( this );
    }
    
    /**
     * Returns the list of runtime exceptions that can be raised by this
     * instruction.
     * @return String[] Runtime exceptions that can be raised by this
     *                  instruction.
     */
    public String[] getRuntimeExceptions() {
        return runtimeExceptions;
    }

}
