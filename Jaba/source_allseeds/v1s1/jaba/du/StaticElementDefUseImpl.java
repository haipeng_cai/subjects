/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.NamedReferenceType;
import jaba.sym.TypeEntry;

/**
 * Represents a definition or use of a static (class) element.
 * @author Huaxing Wu -- <i> Revised 7/9/02, Add hashCode() as required by
 *                       Object.hashCode() contact: hasCode() must overridden 
 *                       if equals() overridden</i>
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor, bug fixing in
 *                           getParentDefUses()</i>
 * @author Jay Lofstead 2003/07/14 changed to implement StaticElementDefUse
 */
public class StaticElementDefUseImpl extends RecursiveDefUseImpl implements StaticElementDefUse
{
  /**
   * The type of the class or interface that contains the static element being
   * referenced or defined.
   */
  private NamedReferenceType type;

    /**
     * Construct a StaticElementDefUse with this type and element. This
     * is used to express static field of a class. 
     * E.G: Class A {static String version; } then type = A, element = version
     * @param type -- The class
     * @param element -- The static field
     */
    public StaticElementDefUseImpl (NamedReferenceType type, DefUse element) {
	super(element);
	this.type = type;
    }

  /**
   * Returns the type of the class or interface that contains the static 
   * element being referenced or defined.
   * @return The type of the class or interface that contains the static 
   *         element being referenced or defined.
   */
  public TypeEntry getType()
    {
      return type;
    }


  /**
   * Assigns the typeof the class or interface that contains the static 
   * element being referenced or defined.
   * @param type  The type of the class or interface that contains the static 
   *              element being referenced or defined.
   */
  public void setType( NamedReferenceType type )
    {
      this.type = type;
    }


  /**
   * Returns an array of DefUse object that are parent DefUse trees of this.
   * @return An array of DefUse object that are parent DefUse trees of this.
   */
  public DefUse[] getParentDefUses()
    {
      DefUse[] returnVal = null;
      // if my element is a recursive def use tree, call getParentDefUses on it
      DefUse element = getElement();
      if ( element instanceof RecursiveDefUse )
	{
	  DefUse[] elementParents = 
	    ((RecursiveDefUse)element).getParentDefUses();

	  // allocate the array to return the result -- the size of the 
	  // parents of the element
	  returnVal = new DefUse[ elementParents.length ];

	  // loop through each of the parents of the element and create a
	  // StaticElementDefUse with each of these items as elements (and
	  // all other attributes of this SE
	  for ( int i = 0; i < elementParents.length; i++ )
	    {
		DefUse elementParent = elementParents[i];
		StaticElementDefUse se = new StaticElementDefUseImpl (type, elementParent);
		returnVal[i] = se;
	      // Constructor change
// 	      se.setType( type );
// 	      se.setElement( elementParent );
	    }
	  
	}
      else {
	  returnVal = new DefUse[0];
      }

      return returnVal;

    }

  /**
   * Indicated whether another StaticElementDefUse is equal to this one.  This
   * compares contents, not DefUse object references.
   * @param du  The DefUse object to compare this object with.
   */
  public boolean equals( Object du )
    {
      if ( !(du instanceof StaticElementDefUse ) ) return false;

      StaticElementDefUse sedu = (StaticElementDefUse)du;

      return type.equals(sedu.getType()) &&
	  getElement().equals(sedu.getElement());
  //     if ( !type.equals( sedu.getType() ) ) return false;

//       return getElement().equals( sedu.getElement() );

    }

    /**
     * Returns a hash code for this object. 
     * The hash code = 31 * type.hashCode() + element.hashCode() 
     * @return An int hash code 
     */
    public int hashCode() {
	return 31 * type.hashCode() + getElement().hashCode();
    }

  /**
   * Returns a string representation of this static element def/use object.
   * @return A string representation of this static element def/use object.
   */
  public String toString()
    {
      StringBuffer buf = new StringBuffer();

	buf.append ("<staticelementdefuse>");
      buf.append( "( " );
      buf.append( "t=" + type.getName() + ", " );
      buf.append( "e=" + getElement().toString() + " )" );

	buf.append ("</staticelementdefuse>");

      return buf.toString();
    }

}
