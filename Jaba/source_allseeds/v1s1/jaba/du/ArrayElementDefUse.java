/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.LocalVariable;
import jaba.sym.Field;
import jaba.sym.TypeEntry;

/**
 * Represents a definition or a use of an array element.
 * @author Jim Jones -- <i>Created, Feb. 2, 1999</i>
 * @author Huaxing Wu -- <i> Revised 7/9/02, Add hashCode() as required by
 *                       Object.hashCode() contact: hasCode() must overridden 
 *                       if equals() overridden</i>
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor, bug fixing in
 *                           getParentDefUses()</i>
 * @author Jay Lofstead 2003/07/14 changed to an interface
 */
public interface ArrayElementDefUse extends RecursiveDefUse
{
    /**
     * Returns the type of the DefUse object at this level.
     * @return  The type of the DefUse object at this level.
     */
    public TypeEntry getType();

  /**
   * Returns the array being indexed into.
   * @return The array being indexed into.
   */
  public HasValue getArray();

  /**
   * Assigns the array being indexed into.
   * @param array  The array being indexed into.
   */
  public void setArray( HasValue array );

  /**
   * Assigns the index being used if a constant.
   * @param indexValue  The index being used if a constant.
   */
  public void setIndexValue( int indexValue );

  /**
   * Returns an array of DefUse object that are parent DefUse trees of this.
   * @return An array of DefUse object that are parent DefUse trees of this.
   */
  public DefUse[] getParentDefUses();

  /**
   * Indicated whether another ArrayElementDefUse is equal to this one.  This
   * compares contents, not DefUse object references.
   * @param du  The DefUse object to compare this object with.
   */
  public boolean equals( Object du );

    /**
     * Returns a hash code for this object. 
     * The hash code = 31 * array.hashCode() + element.hashCode() 
     * @return An int hash code 
     */
    public int hashCode();

  /**
   * Returns a string representation of this array element def/use object.
   * @return A string representation of this array element def/use object.
   */
  public String toString();
}
