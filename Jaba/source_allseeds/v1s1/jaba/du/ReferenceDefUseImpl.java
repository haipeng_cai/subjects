/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.LocalVariable;
import jaba.sym.Field;
import jaba.sym.TypeEntry;

/**
 * Represents a definition or a use of a reference data member.
 * @author Huaxing Wu -- <i> Revised 7/8/02, Change getType() to directly
 *                           call HasValue's getType() </i>
 * @author Huaxing Wu -- <i> Revised 7/9/02, Add hashCode() as required by
 *                           Object.hashCode() contact : hasCode() must 
 *                           overridden if equals() overridden</i>
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor</i> 
 * @author Jay Lofstead 2003/07/14 changed to implement ReferenceDefUse
 */
public class ReferenceDefUseImpl extends LeafDefUseImpl implements Cloneable, ReferenceDefUse
{
    /**
     * construct a new ReferenceDefUse for the Variable
     * @param variable a Def / Use HasValue
     */
    public ReferenceDefUseImpl (HasValue variable)
    {
	super(variable);
    }

    /**
     * Returns the type of the DefUse object at this level.
     * @return  The type of the DefUse object at this level.
     */
    public TypeEntry getType ()
    {
	return getVariable().getType();
    }


    /**
     * Indicated whether another ReferenceDefUse is equal to this one.  This
     * compares contents, not DefUse object references.
     * @param du  The DefUse object to compare this object with.
     */
    public boolean equals( Object du )
    {
	if ( !(du instanceof ReferenceDefUse ) ) return false;
	
	return getVariable().equals( ((ReferenceDefUse)du).getVariable() );
	
    }

    /** Returns a hash code for object. The hash code is 
     *  the same as the containing variable
     * @return An int hash code 
     */
    public int hashCode()
    {
	return  getVariable().hashCode();
    }

    /**
     * Creates a clone of this object.
     * @return A clone of this object.
     */
    public Object clone()
    {
	ReferenceDefUse newDU = null;
	try {
	    newDU = (ReferenceDefUse)super.clone();
	}catch ( CloneNotSupportedException e){
	    throw new RuntimeException(e);
	}

	return newDU;
	
    }
    
    /**
     * Returns a string representation of this reference def/use object.
     * @return A string representation of this reference def/use object.
     */
    public String toString()
    {
	StringBuffer buf = new StringBuffer();

	buf.append ("<referencedefuse>");
	HasValue variable = getVariable();
	String varType = variable.getClass().getName();
	buf.append( varType.substring( varType.lastIndexOf(".") + 1, 
				       varType.length() ) );
	if ( !( variable instanceof TempVariable ) && 
	     ( variable instanceof LocalVariable ) )
	    buf.append( "( " + ((LocalVariable)variable).getName() + " )" );
	else if ( variable instanceof Field )
	    buf.append( "( " + ((Field)variable).getName() + " )" );

	buf.append ("</referencedefuse>");

	return buf.toString();
    }
}
