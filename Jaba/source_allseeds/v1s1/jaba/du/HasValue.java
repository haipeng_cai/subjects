/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.TypeEntry;
/**
 * Defines an interface for objects that have a value associated with them,
 * and therefore can appear in a DU association.
 * @author S. Sinha
 * @author Huaxing Wu -- <i> Revised, 7/8/02,  Add method getType() </i> 
 */
public interface HasValue {
    /* Return the type of this Object.
     * @return The type of the object
     */
    public TypeEntry getType ();
}
