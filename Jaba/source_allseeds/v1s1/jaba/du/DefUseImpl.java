/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.TypeEntry;

/**
 * This class represents any definition or use.
 * @author Huaxing Wu -- <i> Comment out the variable "type". 
 *                           It's obsolete </i>
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/01 changed to implenent DefUse
 */
public abstract class DefUseImpl implements Cloneable, java.io.Serializable, DefUse
{
    /**
     * Returns the type of the DefUse object at this level.
     * @return  The type of the DefUse object at this level.
     */
    public abstract TypeEntry getType();

    /**
     * Returns the type of the entire DefUse tree using this as the
     * root.  The type of the entire tree is the type of the leaf --
     * this method returns the type of the leaf.
     * @return The type of the entire DefUse tree beneath the DefUse
     *         object on which this method is called.
     */
    public abstract TypeEntry getTreeType();

    /**
     * Returns a string representation of this def/use object.
     * @return A string representation of this def/use object.
     */
    public abstract String toString();
    
    /**
     * Indicated whether another DefUse is equal to this one.  This
     * compares contents, not DefUse object references.
     * @param du  The DefUse object to compare this object with.
     */
    public boolean equals( Object du )
    {
	assert false:"Defuse.equal()";
	if ( this instanceof PrimitiveDefUse ) 
	    return ((PrimitiveDefUse)this).equals( du );
	else if ( this instanceof ReferenceDefUse )
	    return ((ReferenceDefUse)this).equals( du );
	else if ( this instanceof ReferenceElementDefUse )
	    return ((ReferenceElementDefUse)this).equals( du );
	else if ( this instanceof StaticElementDefUse )
	    return ((StaticElementDefUse)this).equals( du );
	else if ( this instanceof ArrayElementDefUse )
	    return ((ArrayElementDefUse)this).equals( du );
	else if ( this instanceof TypeUse )
	    return ((TypeUse)this).equals( du );
	else
	    return false;
	
    }

}
