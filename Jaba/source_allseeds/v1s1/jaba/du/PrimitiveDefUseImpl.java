/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.LocalVariable;
import jaba.sym.Field;
import jaba.sym.TypeEntry;

/**
 * Represents a definition or a use of a primitive typed data member.
 * @author Huaxing Wu -- <i> Revised 7/8/02, Change getType() to directly
 *                           call HasValue's getType() 
 * @author Huaxing Wu -- <i> Revised 7/9/02, Add hashCode() as required by
 *                           Object.hashCode() contact: hasCode() must 
 *                           overridden if equals() overridden </i>
 * @author Jay Lofstead 2003/07/14 changed to implement PrimitiveDefUse
 */
public class PrimitiveDefUseImpl extends LeafDefUseImpl implements PrimitiveDefUse
{
    /**
     * Constructa a new PrimitiveDefuse for the Variable.
     * @param Variable a HasValue
     */
    public PrimitiveDefUseImpl (HasValue variable) {
	super(variable);
    }

    /**
     * Returns the type of the DefUse object at this level.
     * @return  The type of the DefUse object at this level.
     */
    public TypeEntry getType() {
	return getVariable().getType();
    }


    /**
     * Indicated whether another PrimitiveDefUse is equal to this one.  This
     * compares contents, not DefUse object references.
     * @param du  The DefUse object to compare this object with.
     */
    public boolean equals( Object du )
    {
	if ( !(du instanceof PrimitiveDefUse ) ) return false;
	
	return getVariable().equals( ((PrimitiveDefUse)du).getVariable() );
      
    }

    /**
     * Returns a hash code for this object. The hash code  is 
     * the same as the containing variable
     * @return An int hash code
     */

    public int hashCode() {
	return getVariable().hashCode();
    }

    /**
     * Creates a clone of this object.
     * @return A clone of this object.
     */
  public Object clone()
    {
      PrimitiveDefUse newDU = null;
      try
	{
	  newDU = (PrimitiveDefUse)super.clone();
	}catch ( CloneNotSupportedException e){
	    throw new RuntimeException(e);
	}

      return newDU;

    }

  /**
   * Returns a string representation of this primitive def/use object.
   * @return A string representation of this primitive def/use object.
   */
  public String toString()
    {
      StringBuffer buf = new StringBuffer();

	buf.append ("<primitivedefuse>");
      HasValue variable = getVariable();
      String varType = variable.getClass().getName();
      buf.append( varType.substring( varType.lastIndexOf(".") + 1, 
				     varType.length() ) );
      if ( !( variable instanceof TempVariable ) &&
	   ( variable instanceof LocalVariable ) )
	buf.append( "( " + ((LocalVariable)variable).getName() + " )" );
      else if ( variable instanceof Field )
	buf.append( "( " + ((Field)variable).getName() + " )" );

	buf.append ("</primitivedefuse>");

      return buf.toString();
    }
  
}
