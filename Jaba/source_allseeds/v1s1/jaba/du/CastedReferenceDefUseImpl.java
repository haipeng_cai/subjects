package jaba.du;

import jaba.sym.TypeEntry;
import jaba.sym.ReferenceType;
import jaba.sym.LocalVariable;
import jaba.sym.Field;
import jaba.du.HasValue;
import jaba.du.TempVariable;

/**
 * Represents a casted reference definition or use.
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor</i> 
 * @author Jay Lofstead 2003/07/14 changed to implement CastedReferenceDefUse
 */
public class CastedReferenceDefUseImpl extends ReferenceDefUseImpl implements CastedReferenceDefUse
{
    /** The casted type of this level of the DefUse tree. */
    ReferenceType type;

    /**
     * construct a new CastedReferenceDefUse. e.g Expression: (A)b,
     * type = A, variable = b
     * @param type a casted ReferenceType
     * @param variable a Def / Use HasValue
     */
    public CastedReferenceDefUseImpl (ReferenceType type, HasValue variable)
    {
	super(variable);
	this.type = type;
    }
    
    /**
     * Sets the casted type of this level of the DefUse tree.
     * @param type  The casted type of this level of the DefUse tree.
     */
    public void setType( ReferenceType type ) {
	this.type = type;
    }


    /**
     * Returns the casted type of this level of the DefUse tree.
     * @return The casted type of this level of the DefUse tree.
     */
    public TypeEntry getType ()
    {
	return type;
    }

    
    /**
     * Returns a string representation of this casted reference def/use 
     * object.
     * @return A string representation of this casted reference def/use 
     *         object.
     */
    public String toString()
    {
	StringBuffer buf = new StringBuffer();

	buf.append ("<castedreferencedefuse>");
	buf.append( "v=" );
	HasValue variable = getVariable();
	String varType = variable.getClass().getName();
	buf.append( varType.substring( varType.lastIndexOf(".") + 1, 
				       varType.length() ) );
	if ( !( variable instanceof TempVariable ) && 
	     ( variable instanceof LocalVariable ) )
	    buf.append( "( " + ((LocalVariable)variable).getName() + " )" );
	else if ( variable instanceof Field )
	    buf.append( "( " + ((Field)variable).getName() + " )" );
	buf.append( ", t=" + type.getName() );

	buf.append ("</castedreferencedefuse>");

	return buf.toString();
    }
}
