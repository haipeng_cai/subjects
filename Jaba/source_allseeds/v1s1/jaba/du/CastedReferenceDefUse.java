package jaba.du;

import jaba.sym.TypeEntry;
import jaba.sym.ReferenceType;
import jaba.sym.LocalVariable;
import jaba.sym.Field;
import jaba.du.HasValue;
import jaba.du.TempVariable;

/**
 * Represents a casted reference definition or use.
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor</i> 
 * @author Jay Lofstead 2003/07/14 changed to an interface
 */
public interface CastedReferenceDefUse extends ReferenceDefUse
{
    /**
     * Sets the casted type of this level of the DefUse tree.
     * @param type  The casted type of this level of the DefUse tree.
     */
    public void setType( ReferenceType type );

    /**
     * Returns the casted type of this level of the DefUse tree.
     * @return The casted type of this level of the DefUse tree.
     */
    public TypeEntry getType ();
    
    /**
     * Returns a string representation of this casted reference def/use 
     * object.
     * @return A string representation of this casted reference def/use 
     *         object.
     */
    public String toString();
}
