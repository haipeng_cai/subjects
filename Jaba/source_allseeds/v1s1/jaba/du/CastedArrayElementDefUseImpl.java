package jaba.du;

import jaba.sym.TypeEntry;
import jaba.sym.ReferenceType;
import jaba.sym.LocalVariable;
import jaba.sym.Field;
import jaba.du.HasValue;
import jaba.du.TempVariable;

/**
 * Represents a casted array element definition or use.
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor</i>
 * @author Jay Lofstead 2003/07/14 changed to implement CastedArrayElementDefUse
 */
public class CastedArrayElementDefUseImpl extends ArrayElementDefUseImpl implements CastedArrayElementDefUse
{
    /** The casted type of this level of the DefUse tree. */
    ReferenceType type;

    /**
     * Construct a CastedArrayElementDefUse. Its index is set defaule as -1
     * e.g: (T)a[i], type = T, array = a, element = PlaceHolderLocation for 
     * a[i].
     * @param type the casted type
     * @param array the reference
     * @param element the element of the array
     */
    public CastedArrayElementDefUseImpl (ReferenceType type, HasValue array, DefUse element)
    {
	this(type, array, element, -1);
    }

    /**
     * Construct a CastedArrayElementDefuse  
     * e.g: (T)a[i], type = T, array = a, element = PlaceHolderLocation for 
     * a[i], indexValue = i
     * @param type the casted type
     * @param array the reference
     * @param element the element of the array
     * @param indexvalue the index of the element
     */
    public CastedArrayElementDefUseImpl (ReferenceType type, HasValue array, DefUse element, int indexValue)
    {
	super(array, element, indexValue);
	this.type = type;
    }

    /**
     * Sets the casted type of this level of the DefUse tree.
     * @param type  The casted type of this level of the DefUse tree.
     */
    public void setType( ReferenceType type )
    {
	this.type = type;
    }


    /**
     * Returns the casted type of this level of the DefUse tree.
     * @return The casted type of this level of the DefUse tree.
     */
    public TypeEntry getType ()
    {
	return type;
    }


    /**
     * Returns a string representation of this casted array element def/use 
     * object.
     * @return A string representation of this casted array element def/use 
     *         object.
     */
    public String toString()
    {
	StringBuffer buf = new StringBuffer();

	buf.append ("<castedarrayelementdefuse>");
	buf.append( "( a=" );
	HasValue array = getArray();
	String arrayType = array.getClass().getName();
	buf.append( arrayType.substring( arrayType.lastIndexOf(".") + 1, 
					 arrayType.length() ) );
	if ( !( array instanceof TempVariable ) &&
	     ( array instanceof LocalVariable ) )
	    buf.append( "( " + ((LocalVariable)array).getName() + " )" );
	else if ( array instanceof Field )
	    buf.append( "( " + ((Field)array).getName() + " )" );
	buf.append( ", t=" );
	buf.append( type.getName() + " ), " );
	buf.append( "e=" + getElement().toString() + " )" );

	buf.append ("</castedarrayelementdefuse>");

	return buf.toString();
    }
}
