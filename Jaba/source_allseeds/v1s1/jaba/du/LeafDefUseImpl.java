package jaba.du;

import jaba.sym.TypeEntry;

/**
 * This class represents the bottom-most level of any def-use tree.  This
 * class is either a primitive or a reference def-use.
 * @author Jim Jones -- <i>Created, March 1999</i>.
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor</i> 
 * @author Jay Lofstead 2003/07/01 changed to extend DefUseImpl
 * @author Jay Lofstead 2003/07/14 changed to implement LeafDefUse
 */
public abstract class LeafDefUseImpl extends DefUseImpl implements LeafDefUse
{
    /** The variable or NewInstance that is being defined or used. */
    private HasValue variable;

    /**
     * Used only by subclass. Construct a LeafDefUse for a variable
     * @param variable a Def / Use HasValue
     */
    protected LeafDefUseImpl (HasValue variable)
    {
	this.variable = variable;
    }

    /**
     * Returns the data element that is being defined or used.
     * @return The data element that is being defined or used.
     */
    public HasValue getVariable ()
    {
	return variable;
    }
    
    
    /**
     * Returns the type of the entire DefUse tree using this as the
     * root.  The type of the entire tree is the type of the leaf --
     * this method returns the type of the leaf.
     * @return The type of the entire DefUse tree beneath the DefUse
     *         object on which this method is called.
     */
    public TypeEntry getTreeType ()
    {
	return getType ();
    }
    
    
    /**
     * Assigns the data element that is being defined or used.
     * @param variable  The data element that is being defined or used.
     */
    public void setVariable (HasValue variable)
    {
	this.variable = variable;
    }
    
}
