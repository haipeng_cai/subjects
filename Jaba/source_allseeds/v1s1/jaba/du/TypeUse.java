package jaba.du;

import jaba.sym.TypeEntry;
import jaba.sym.ReferenceType;

/**
 * Represents a use of the type of a named reference element in an
 * <code>instanceof</code>.  This type of use is differentiated from others
 * because the object of the <code>instanceof</code> is not used, but its
 * <i>type</i> is.
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference a member
 * @author Jay Lofstead 2003/07/14 changed to an interface
 */
public interface TypeUse extends RecursiveDefUse
{
    /**
     * Sets the type used in the <code>instanceof</code> instruction.
     * @param type  The type used in the <code>instanceof</code> instruction.
     */
    public void __setCheckedType( ReferenceType type );

    /**
     * Returns the type used in the <code>instanceof</code> instruction.
     * @return The type used in the <code>instanceof</code> instruction.
     */
    public ReferenceType getCheckedType();

    /**
     * Returns the type of this level of the DefUse tree.  Because a
     * TypeUse really doesn't have a "type", this returns the type of
     * its element.
     * @return The type of this level of the DefUse tree.
     */
    public TypeEntry getType();

    /**
     * Returns an array of DefUse object that are parent DefUse trees of this.
     * @return An array of DefUse object that are parent DefUse trees of this.
     */
    public DefUse[] getParentDefUses();

    /**
     * Returns a string representation of this type use object.
     * @return A string representation of this type use object.
     */
    public String toString();

	/** ??? */
    public boolean equals( Object du );
}
