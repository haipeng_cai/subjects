/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.LocalVariable;
import jaba.sym.Field;
import jaba.sym.TypeEntry;

/**
 * Represents a definition or a use of an array element.
 * @author Jim Jones -- <i>Created, Feb. 2, 1999</i>
 * @author Huaxing Wu -- <i> Revised 7/9/02, Add hashCode() as required by
 *                       Object.hashCode() contact: hasCode() must overridden 
 *                       if equals() overridden</i>
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor, bug fixing in
 *                           getParentDefUses()</i>
 * @author Jay Lofstead 2003/07/14 changed to implement ArrayElementDefUse
 */
public class ArrayElementDefUseImpl extends RecursiveDefUseImpl implements ArrayElementDefUse
{
  /** The array being indexed into. */
  private HasValue array;


  /** The index being used if a constant, otherwise it is set to -1 */
  private int indexValue;

    /**
     * Construct an ArrayElementDefUse. Its index is -1.
     * e.g; a[i], array = a, element = PlaceHolderLocation for a[i], 
     * Can be PrimitiveDefUse/ReferenceDefUse, depends on the type of a[i] 
     * @param array the reference of the array
     * @param element the element of the array
     */
    public ArrayElementDefUseImpl (HasValue array, DefUse element){
	this(array, element, -1);
    }

    /**
     * Construct an ArrayElementDefuse  
     * e.g; a[i], array = a, element = PlaceHolderLocation for a[i], 
     * Can be PrimitiveDefUse/ReferenceDefUse, depends on the type of a[i].
     * indexValue = i.
     * @param array the reference of the array
     * @param element the element of the array
     * @param indexValue the index of the element
     */
    public ArrayElementDefUseImpl (HasValue array, DefUse element, int indexValue) {
	super(element);
	this.array = array;
	this.indexValue = indexValue;
    }


    /**
     * Returns the type of the DefUse object at this level.
     * @return  The type of the DefUse object at this level.
     */
    public TypeEntry getType() {
	return array.getType();
    }

  /**
   * Returns the array being indexed into.
   * @return The array being indexed into.
   */
  public HasValue getArray()
    {
      return array;
    }


  /**
   * Assigns the array being indexed into.
   * @param array  The array being indexed into.
   */
  public void setArray( HasValue array )
    {
      this.array = array;
    }


  /**
   * Returns the index being used if a constant, if index is not statically a
   * constant value, a -1 is returned.
   * @return The index being used if a constant, if index is not statically a
   *         constant value, a -1 is returned.
   */
// This is commented out until this is fully implemented.  The index is
// currently being set in most cases but there are cases when it is not set
// and it is statically determinable: 1. ldc is used to load the constant, 
// and 2. a cast is performed on the constant value.
//   public int getIndexValue()
//     {
//       return indexValue;
//     }


  /**
   * Assigns the index being used if a constant.
   * @param indexValue  The index being used if a constant.
   */
  public void setIndexValue( int indexValue )
    {
      this.indexValue = indexValue;
    }

  /**
   * Returns an array of DefUse object that are parent DefUse trees of this.
   * @return An array of DefUse object that are parent DefUse trees of this.
   */
  public DefUse[] getParentDefUses()
    {
      DefUse[] returnVal = null;
      // if my element is a recursive def use tree, call getParentDefUses on it
      DefUse element = getElement();
      if ( element instanceof RecursiveDefUse )
	{
	  DefUse[] elementParents = 
	    ((RecursiveDefUse)element).getParentDefUses();

	  // allocate the array to return the result -- the size of the 
	  // parents of the element plus one (for the parent of this AE)
	  returnVal = new DefUse[ elementParents.length + 1 ];

	  // loop through each of the parents of the element and create a
	  // ArrayElementDefUse with each of these items as elements (and
	  // all other attributes of this AE
	  for ( int i = 0; i < elementParents.length; i++ )
	    {
	      DefUse elementParent = elementParents[i];
	      ArrayElementDefUse ae = new ArrayElementDefUseImpl (array, elementParent, indexValue);
	      returnVal[i] = ae;
	      // Constructor Change
// 	      ae.setArray( array );
// 	      ae.setIndexValue( indexValue );
// 	      ae.setElement( elementParent );
	    }

	}
      else
	returnVal = new DefUse[1];
	 
      // create reference def use for the parent of this AE
      ReferenceDefUse ref = new ReferenceDefUseImpl (array);
      //      ref.setVariable( array );
      returnVal[ returnVal.length - 1 ] = ref;

      return returnVal;

    }

  /**
   * Indicated whether another ArrayElementDefUse is equal to this one.  This
   * compares contents, not DefUse object references.
   * @param du  The DefUse object to compare this object with.
   */
  public boolean equals( Object du )
    {
      if ( !(du instanceof ArrayElementDefUse ) ) return false;

      ArrayElementDefUse aedu = (ArrayElementDefUse)du;
// this is commented out for now until the index value is properly tracked.
//      if ( indexValue != aedu.getIndexValue() ) return false;

      return array.equals(aedu.getArray()) &&
	  getElement().equals(aedu.getElement());
      // Replace the 3 lines with the line above
//      if ( !array.equals( aedu.getArray() ) ) return false;
//
//      return getElement().equals( aedu.getElement() );

    }

    /**
     * Returns a hash code for this object. 
     * The hash code = 31 * array.hashCode() + element.hashCode() 
     * @return An int hash code 
     */
    public int hashCode() {
	return 31 * array.hashCode() + getElement().hashCode();
    }

  /**
   * Returns a string representation of this array element def/use object.
   * @return A string representation of this array element def/use object.
   */
  public String toString()
    {
      StringBuffer buf = new StringBuffer();

	buf.append ("<arrayelementdefuse>");
      String arrayType = array.getClass().getName();
      buf.append( arrayType.substring( arrayType.lastIndexOf(".") + 1, 
				     arrayType.length() ) );
      if ( !( array instanceof TempVariable ) &&
	   ( array instanceof LocalVariable ) )
	buf.append( "( " + ((LocalVariable)array).getName() + " )" );
      else if ( array instanceof Field )
	buf.append( "( " + ((Field)array).getName() + " )" );
      buf.append( ", " );
      buf.append( "e=" + getElement().toString() + " )" );

	buf.append ("</arrayelementdefuse>");

      return buf.toString();
    }
}
