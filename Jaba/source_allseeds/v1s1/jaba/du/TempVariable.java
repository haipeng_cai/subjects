/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.LocalVariable;

/**
 * Represents a temporary variable that is used to store return values from
 * method invocations. An instance of <code>TempVariable</code> is pushed
 * onto the operand stack by invoke instructions - </i>invokeinterface</i>,
 * <i>invokespecial</i>, <i>invokestatic</i>, and <i>invokevirtual</i>.
 *
 * @author S. Sinha -- <i>Created.</i>
 * @author Jim Jones -- <i>Revised April 30, 1999 -- Stripped all functionality
 *                      and made it extend LocalVariable so that use of
 *                      LocalVariables and TempVariables can be uniform.</i>
 * @author Huaxing Wu -- <i> Revised 01/02/03. Add method equals, hashcode, tmpid and constructor.
 * 			These are added to fix an urgent bug. At this time, the tempvariable is
 * 			not put in the Symboltable, so it doesn't has an id (defined in
 * 			SymbolTableEntry), but since it inherits methods equals and hashcode from
 * 			SymboltableEntry, this id is used to decide whether two tempvariable equals
 * 			or not. Because tempvariable's id is never set, all tempvariable will be
 * 			equal and has same hashcode. And this is incorrect. This fix is based on
 * 			the observation that for one temp variable, only one instance will be
 * 			created. The tmpId is trying to speed it up if tempvariable is put in a
 * 			vector. This fix is also temporary because base on the inheritance, each
 * 			tempvariable should be put in the Symbol table and has different id.
 * @author Jay Lofstead 2003/06/11 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface TempVariable extends LocalVariable
{
  /**
   * Returns a string for this object.
   * @return  String representing this object.
   */
  public String toString();

    /**
     * Returns a hash code for this. The hash code is 
     * a temp variable id. This is a temporarily fix.
     * @return the hash code for this.
     */
    public int hashCode();

    /** 
     * Decide whether two temp variable equals or not. This equal method
     * internally calls ==, that is, a tempvariable can only equal to itself.
     * This is a temporaaily fix. A correct way to do it, based on the 
     * inheritance should be: get an id from the symbol table for each 
     * temp variable, and not overright this 'equal' method at all.
     * @return true -- if two objects are same instance. false --otherwise.
     */
    public boolean equals(Object o);
}
