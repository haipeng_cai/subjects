package jaba.du;

import jaba.sym.TypeEntry;

/**
 * Represents a non-leaf defintion/use object in a definition/use tree.
 * @author Huaxing Wu -- <i> Revise 7/3/02 Add a constructor, taking parameter: element </i>
 * @author Jay Lofstead 2003/07/01 changed to extend DefUseImpl
 * @author Jay Lofstead 2003/07/14 changed to an interface
 */
public interface RecursiveDefUse extends DefUse
{
	/**
	 * Returns the element of this def/use.
	 * @return The element of this def/use.
	 */
	public DefUse getElement ();

	/**
	 * Assigns the element of this def/use.
	 * @param element  The element of this def/use.
	 */
	public void setElement (DefUse element);

	/**
	 * Returns the leaf element of this def-use tree or subtree.
	 * @return The leaf element of this def-use tree or subtree.
	 */
	public LeafDefUse getLeafElement ();

	/**
	 * Returns the def/use element just above the leaf.
	 * @return The def/use element just above the leaf element.
	 */
	public RecursiveDefUse getLastRecursiveElement ();

	/**
	 * Creates a clone of this object.
	 * @return A clone of this object.
	 */
	public Object clone ();

	/**
	 * Returns an array of DefUse object that are parent DefUse trees of this.
	 * @return An array of DefUse object that are parent DefUse trees of this.
	 */
	public DefUse [] getParentDefUses ();

	/**
	 * Returns the type of the entire DefUse tree using this as the
	 * root.  The type of the entire tree is the type of the leaf --
	 * this method returns the type of the leaf.
	 * @return The type of the entire DefUse tree beneath the DefUse
	 *         object on which this method is called.
	 */
	public TypeEntry getTreeType ();
}
