/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.graph.cfg.CFG;

/**
 * This class performs a gathering of data definitions and uses over a
 * control-flow graph.
 * @author Jim Jones -- <i>Feb. 18, 1999, Created.</i>
 * @author Huaxing Wu -- <i> 8/2/02. Revised. Change to use DefUse objects'
 *                         constructor. </i>
 * @author Huaxing Wu -- <i> 8/28/02. Revised. Add method mergeType,
 *                       getStackOperandType and debugging method correctEdges.
 *                       getStackOperandType and correctEdges are extracted
 *                       from mergeOpStack. </i>
 *
 * @author jay Lofstead 2003/06/05 cleaned up enumerations
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/14 changed to an interface
 */
public interface DefUseInitializer
{
    /**
     * Gathers defintions and uses for all symbolic instructions in a CFG.
     * @param cfg  The control-flow graph to walk to gather its DU information.
     */
    public void initialize (CFG cfg);
}
