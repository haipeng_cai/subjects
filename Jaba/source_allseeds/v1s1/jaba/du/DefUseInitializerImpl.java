/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.instruction.visitor.DUGatheringVisitor;

import jaba.instruction.SymbolicInstruction;
import jaba.instruction.SymbolicInstructionImpl;

import jaba.du.OperandStack;

import jaba.graph.cfg.CFG;

import jaba.graph.StatementNode;
import jaba.graph.StatementNodeImpl;
import jaba.graph.Node;
import jaba.graph.Edge;
import jaba.graph.EdgeImpl;
import jaba.graph.ExceptionAttribute;

import jaba.sym.Primitive;
import jaba.sym.ReferenceType;
import jaba.sym.Method;
import jaba.sym.TypeEntry;

import jaba.debug.Debug;

import jaba.tools.local.Factory;

import java.util.Vector;
import java.util.HashMap;
import java.util.Enumeration;

/**
 * This class performs a gathering of data definitions and uses over a
 * control-flow graph.
 * @author Jim Jones -- <i>Feb. 18, 1999, Created.</i>
 * @author Huaxing Wu -- <i> 8/2/02. Revised. Change to use DefUse objects'
 *                         constructor. </i>
 * @author Huaxing Wu -- <i> 8/28/02. Revised. Add method mergeType,
 *                       getStackOperandType and debugging method correctEdges.
 *                       getStackOperandType and correctEdges are extracted
 *                       from mergeOpStack. </i>
 *
 * @author Jay Lofstead 2003/06/05 cleaned up enumerations
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/14 changed to implement DefUseInitializer
 * @author Jay Lofstead 2003/07/17 changed to use Factory for attribute retrieval.
 */
public class DefUseInitializerImpl implements java.io.Serializable, DefUseInitializer
{
    /**
     * Gathers defintions and uses for all symbolic instructions in a CFG.
     * @param cfg  The control-flow graph to walk to gather its DU information.
     */
    public void initialize( CFG cfg )
    {

	Debug.println(cfg.getMethod().getContainingType().getName() + 
			  "." + cfg.getMethod().getSignature(), Debug.DU, 1 );

	// create a visitor class to process DU info and process an operand
	// stack for each symbolic instruction
	DUGatheringVisitor duVisitor = new DUGatheringVisitor();
	
	// set the method in the def use gathering visitor.  this is used to 
	// call all "called by"/"calling" information in the methods
	duVisitor.setMethod( cfg.getMethod() );
	
	// set the CFG in the DU visitor.  this is needed because it
	// may add new nodes to the CFG if it finds instructions that
	// require the addition of new tempory variable definitions
	duVisitor.setCFG( cfg );
	
	// create a mapping from each node in the CFG to a vector of references
	// to operand stacks
	HashMap opStackMap = new HashMap();
	
	// get all of the nodes in the CFG in a depth-first ordering
	Node[] nodes = cfg.getNodesInDepthFirstOrder();
	
	// initialize the operand stack map with each node and an empty vector
	for ( int i = 0; i < nodes.length; i++ )
	    opStackMap.put( nodes[i], new Vector() );
	
	OperandStack opStack = null;
	Vector opStackVec = null;
	
	// loop through all nodes in the depth-first order
	for ( int i = 0; i < nodes.length; i++ ) {
	    // get the operand stack vector for this node
	    StatementNode node = (StatementNode)nodes[i];
	    opStackVec = (Vector)opStackMap.get( node );
	    
	    // if there is more than one operand stack in the vector, we need to merge them to one
	    if ( opStackVec.size() > 1 )
		opStackVec = mergeOpStacks( cfg, node, opStackVec );
	    
	    // if there are no in edges for this operand stack, this
	    // must be either a catch node or an entry node -- in such
	    // a case, we expect that there not be any operand stacks
	    // in the vector -- place a new empty operand stack the
	    // vector
	    if ( cfg.getInEdges( node ).length == 0 ) {
		EdgeOpStackPair pair = new EdgeOpStackPair();
		pair.edge = null;
		pair.opStack = new OperandStack();
		opStackVec.add( pair );
	    }
	    
	    // pull the operand stack out of the vector
	    opStack = ((EdgeOpStackPair)opStackVec.elementAt( 0 )).opStack;
	    
	    // if this node is a catch node, the first instruction expects an
	    // object reference on the stack -- so, we'll place a 
	    // R(PlaceholderLocation) on there
	    if ( node.getType() == StatementNode.CATCH_NODE) {
		ExceptionAttribute attr = Factory.getExceptionAttribute (node);
		jaba.sym.Class type = attr.getExceptionType();
	
		PlaceholderLocation loc = new PlaceholderLocationImpl ();
		loc.setType( type );
		ReferenceDefUse refDU = new ReferenceDefUseImpl ( loc );
		Vector refDUVec = new Vector();
		refDUVec.add( refDU );
		opStack.push( refDUVec );
	    }
	    
	    // set the operand stack in the visitor
	    duVisitor.setOperandStack( opStack );
	    
	    // set the current node in the visitor -- this is needed
	    // by the invoke* instructions so that the visitor can
	    // initialize call site attributes for these nodes
	    duVisitor.setNode( node );
	    
	    // get the symbolic instructions for this node
	    SymbolicInstruction[] instructions = 
		node.getSymbolicInstructions();
	    
	    // loop through all of the symbolic instructions in this node, 
	    // initializing the DU information for each of them and processing 
	    // their operand stacks
	    if ( instructions != null ) {
		for ( int j = 0; j < instructions.length; j++ ) {
		    SymbolicInstruction instruction = instructions[j];
		    Debug.println("bytecode " +
				       instruction.getByteCodeOffset() + 
				       "    " + 
				       instruction.getClass().getName(),
				       Debug.DU, 1);
		    ((SymbolicInstructionImpl) instruction).accept( duVisitor );
		    Debug.println("after, stack = "+opStack.toString(), Debug.DU,1);
		    Debug.print("def: ", Debug.DU, 1 );
		    Debug.println( ( (instruction.getDefinition() == 
					   null ) ? "" : 
					  instruction.getDefinition().
					  toString() ), Debug.DU, 1 );
		    Debug.print("uses: ", Debug.DU, 1);
		    Vector uses = instruction.getUsesVector ();
		    for (Enumeration k = uses.elements (); k.hasMoreElements ();)
		    {
			Debug.print (k.nextElement ().toString (), Debug.DU, 1);
			if (k.hasMoreElements ())
			    Debug.print( ", ", Debug.DU, 1 );
		    }
		    Debug.println("", Debug.DU, 1);
	        }
	    }
	    
	    // get all of the out edges for this node
	    Edge [] outEdges = cfg.getOutEdges (node);
	    
	    if ( outEdges.length != 0 ) {
		// for the first out edge, place the operand stack
		// that was just processed in the operand stack map
		// for the successor node
		opStackVec = (Vector)opStackMap.get( outEdges[0].getSink() );
		
		// if the opStackVec is zero, we must be at a new 
		// TEMP_DEFINITION_NODE that the DUGatheringVisitor created --
		// we need to create an empty vector and put it in the opstack
		// map
		if ( opStackVec == null ) {
		    opStackVec = new Vector();
		    opStackMap.put( outEdges[0].getSink(), opStackVec );
		}
		
		EdgeOpStackPair pair = new EdgeOpStackPair();
		pair.edge = outEdges[0];
		pair.opStack = opStack;
		opStackVec.add( pair );
		
		// for all of the other out edges (if there are any
		// more), place a clone of the operand stack into
		// those successors' slots in the operand stack map
		for ( int k = 1; k < outEdges.length; k++ ) {
		    OperandStack newOpStack = (OperandStack)opStack.clone();
		    opStackVec = 
			(Vector)opStackMap.get( outEdges[k].getSink() );
		    // if the opStackVec is zero, we must be at a new 
		    // TEMP_DEFINITION_NODE that the DUGatheringVisitor 
		    // created -- we need to create an empty vector and put it 
		    // in the opstack map
		    if ( opStackVec == null ) {
			opStackVec = new Vector();
			opStackMap.put( outEdges[k].getSink(), opStackVec );
		    }
		    pair = new EdgeOpStackPair();
		    pair.edge = outEdges[k];
		    pair.opStack = newOpStack;
		    opStackVec.add( pair );
		}
		
	    }
	    
	}
	
    }
    
    /**
     * Merges two or more operand stacks to one.  Also must go
     * backward along all in edges to this node and insert an extra
     * node in front of this node. This extra node will server as
     * a bridge: a temp variable will be defound by the top most element
     * of the stack, and each inedge's new node will define the same temp 
     * variable.This is necesary for correct
     * merging and processing of def/use information.
     * Note: The reason that a temp variable has to be introduced has lost 
     * in people's memory. One guess is to enforce there is only one object 
     * reference in all defuses that involve defining a definition. 
     * @param cfg  The control-flow graph.
     * @param node  The node for which we are merging multiple operand stacks.
     * @param opStackVec  Vector of two or more operands to be merged.
     * @return The resulting single-operand stack vector (the merged vector).
     */
    private Vector mergeOpStacks( CFG cfg, StatementNode node, 
				  Vector opStackVec ) {
	// as a sanity check, make sure that all operand stacks to be
	// merged are of the same size
	int lastDepth = ((EdgeOpStackPair)opStackVec.elementAt(0)).opStack.
	    getDepth();
	EdgeOpStackPair pair = null;
	OperandStack opStack = null;
	for (Enumeration i = opStackVec.elements (); i.hasMoreElements ();)
	{
	    pair = (EdgeOpStackPair) i.nextElement ();
	    opStack = pair.opStack;
	    int thisDepth = opStack.getDepth();
	    if ( lastDepth != thisDepth ) {
		throw new RuntimeException("Error: operand stacks of different size to be merged");
	    }
	}
	
	// if the depth of all of the operand stacks is zero, then, we should
	// just return -- nothing to merge
	if ( lastDepth == 0 ) {
	    Vector tempVec = new Vector();
	    tempVec.add( (EdgeOpStackPair)opStackVec.elementAt( 0 ) );
	    return tempVec;
	}
	
	// Control comes here only if it's dealing with two kinds of
	// java source. 
	// (1) a= bool1?b:c 
	// (2) String a = "Test " + !bool1;

	// define the temp variable that will summarize all uses
	// coming into the joining node.The temp variable's data type is 
	// decided by the top most entry of each opstack
	TypeEntry tempVarType = getStackOperandType(((EdgeOpStackPair)opStackVec.get(0)).opStack.peek());
	Enumeration i = opStackVec.elements ();
	i.nextElement (); // need to eat the 0 element
	do
	{
	    TypeEntry anotherType = getStackOperandType(((EdgeOpStackPair) i.nextElement ()).opStack.peek());
	    tempVarType = mergeType(cfg.getMethod(), tempVarType, anotherType);

	} while (i.hasMoreElements ());
 	Debug.println("type: " + tempVarType.getName(), Debug.DU, 1);

	TempVariableImpl tempVar = new TempVariableImpl ();
	tempVar.setContainingMethod( cfg.getMethod() );
	((TempVariable) tempVar).setType (tempVarType);
	LeafDefUse tempDU = null;
	if ( tempVarType instanceof ReferenceType )
	    tempDU = new ReferenceDefUseImpl (tempVar);
	else
	    tempDU = new PrimitiveDefUseImpl (tempVar);
	    
	// find all back edges for the joining node -- this is the number of
	// operand stacks to be merged
	// Verify that all edges in opStackVec are correct
	assert correctEdges (cfg.getInEdges(node), opStackVec) : node.toString ();

	// Insert a temp definition node between  each edge and this node
	for (i = opStackVec.elements (); i.hasMoreElements ();)
	{
	    pair = (EdgeOpStackPair) i.nextElement ();

	    opStack = pair.opStack;
	    Edge inEdge = pair.edge;

	    // Create a temp definition node for the top most defuse vector
	    StatementNode newNode = new StatementNodeImpl ();
	    newNode.setType (StatementNode.TEMP_DEFINITION_NODE);
	    newNode.setSourceLineNumber (node.getSourceLineNumber ());
	    newNode.setContainingMethod ((jaba.sym.Method) node.getContainingMethod ());
	    newNode.setDefinition (tempDU);
	    newNode.addUse (opStack.peek ());
	    cfg.addNode (newNode);
	    
	    // insert the newnode in front of this node by creating a new 
	    // edge and change the existing edge. i.e:
	    // n1 --e1-->n2 ==> n1 --ne-->newnode --e1-->n2
	    // Since we cannot change the source or sink of an existing edge,
	    // we need to create an replacement one for the existing one.
	    Edge newEdge = new EdgeImpl ((jaba.graph.Node) inEdge.getSource (), newNode,
				    inEdge.getLabel ());
	    cfg.addEdge (newEdge);
	    
	    // Change the existing one
	    //inEdge.setSource(newNode);
 	    Edge replacement = new EdgeImpl (newNode, (jaba.graph.Node) inEdge.getSink ());
 	    replacement.addAttributes (((jaba.graph.Edge) inEdge).getAttributes ());
 	    cfg.addEdge (replacement);
 	    pair.edge = replacement;
 	    cfg.removeEdge ((jaba.graph.Edge) inEdge);
	}
	
	// okay, now, we have summarized all of the topmost elements on each
	// operand stack.  we now need to create the merged operand stack to
	// return to the caller.  we can take any of the operand stacks that
	// were passed in to be merged and just replace its top element with
	// the new tempDU
	Vector tempVec = new Vector();
	EdgeOpStackPair newEdgeOpStackPair = 
	    (EdgeOpStackPair)opStackVec.elementAt( 0 );
	newEdgeOpStackPair.opStack.pop();
	Vector newDUVec = new Vector();
	newDUVec.add( tempDU );
	newEdgeOpStackPair.opStack.push( newDUVec );
	tempVec.add( newEdgeOpStackPair );
	return tempVec;
	
    }
    
    /**
     * This method identifies whether a given instruction could have
     * defined the topmost element on the operand stack.  
     * @param inst The symbolic instruction to evaluate as to whether
     *             it is an instruction that could define the topmost
     *             element on * the operand stack.
     * @return A boolean stating whether (true) <code>inst</code>
     *         could define the topmost element on the * operand stack
     *         or not (false).  */
    private boolean definesTopMostOpStackElement( SymbolicInstruction inst ) {
	int opCode = inst.getOpcode();
	if ( ( ( opCode >= 1 ) && ( opCode <= 53 ) ) ||
	     ( ( opCode >= 95 ) && ( opCode <= 115 ) ) ||
	     ( ( opCode >= 120 ) && ( opCode <= 131 ) ) ||
	     ( ( opCode >= 148 ) && ( opCode <= 152 ) ) ||
	     ( ( opCode >= 187 ) && ( opCode <= 189 ) ) ||
	     ( opCode == 178 ) ||
	     ( opCode == 180 ) ||
	     ( opCode == 197 ) )
	    return true;
	else if ( ( opCode >= 182 ) && ( opCode <= 185 ) ) {
	    // this is an invoke* instruction.  We need to check the
	    // return type.  If the return type is not void -- return
	    // true.
	    Method method = (Method)inst.getOperandsVector ().get (0);
	    
	    if ( ( method.getType() instanceof Primitive ) &&
		 ( ((Primitive)method.getType()).getType() == 
		   Primitive.VOID ) )
		return false;
	    else
		return true;
	}
	else if ( opCode == 196 ) {
	    // this is a wide instruction.  We need to check its encapsulated
	    // instruction
	    opCode = 
		((jaba.instruction.Wide)inst).getEncapsulatedOpcode();
	    return ( ( opCode >= 1 ) && ( opCode <= 45 ) );
	}
	return false;
    }

    /** A debug method for mergeOperandStack. Make sure all edges in 
     * opStackVec are correct. That is: exact what's in the array edges.
     *@param edges -- The corrected edge array
     *@param opStackVec -- Vector of EdgeOpStackPair
     * @return -- true if edges in opStackVec are correct, false otherwise.
     */
    private boolean correctEdges (Edge [] edges, Vector opStackVec)
    {
	if(opStackVec.size() != edges.length)
	    return false;

	for ( int i = 0; i < edges.length; i++ ) {
	    boolean edgeFound = false;
	    for (Enumeration j = opStackVec.elements (); j.hasMoreElements ();)
	    {
		EdgeOpStackPair	pair = (EdgeOpStackPair) j.nextElement ();
		if ( edges[i] == pair.edge ) {
		    edgeFound = true;
		    break;
		}
	    }
	    if ( !edgeFound ) {
		return false;
	    } 
	}
	return true;
    }

    /** return the representive data type of a vector. This vector is 
     * composed of DefUse pbject. And it's typically an entry of the operand
     * stack. The rule of the data type of a vector is:
     * (1) if any of the def use objects in the vector contain a
     *     reference def use object, then the representative type is that
     *     referencedefuse object's type.
     * (2) otherwise, take the last element in this vector's data type.
     *@param v -- a vector of Defuse objects
     *@return TypeEntry -- The representive data type of this vector.
     */
    private TypeEntry getStackOperandType(Vector v) {
	LeafDefUse leaf  = null;
	DefUse defuse = null;

	//Try to find the reference def use object
	for (Enumeration i = v.elements (); i.hasMoreElements ();)
	{
	    defuse = (DefUse) i.nextElement ();
	    if(defuse instanceof ReferenceDefUse) { //Leaf & Reference
		leaf = (LeafDefUse)defuse;
		break;
	    }else if(defuse instanceof RecursiveDefUse) { //Recursive
		RecursiveDefUse rDefuse = (RecursiveDefUse)defuse;
		if(rDefuse.getLeafElement() instanceof ReferenceDefUse) {
		    leaf = rDefuse.getLeafElement();
		    break;
		}
	    }
	}

	if(leaf == null) {
	    // No reference Def use found, take the last element
	    if(defuse instanceof RecursiveDefUse)
		leaf = ((RecursiveDefUse)defuse).getLeafElement();
	    else
		leaf = (LeafDefUse)defuse;
	}
	
	return leaf.getType();
    }

    /** Merge two types. If both are primitive, return the second one. 
     * If only one is Reference, return the reference. If both of them are
     * reference, return the common super type.
     * Note: it has simplified the looking for super type of Array and
     * Interface. Their super type will be Object.
     *@param method -- Containg method for this comparision
     *@param t1 -- A type to be merged with another
     *@param t2 -- Another type
     *@return the dorminant/common super types of two types.
     */
    private TypeEntry mergeType (Method method, TypeEntry t1, TypeEntry t2)
    {
	if(t1 == t2)
	    return t1;

	// If one is Primitive, return another one
	if(t1 instanceof Primitive)
	    return t2;

	if(t2 instanceof Primitive)
	    return t1;

	//Control comes here only if both are reference
	java.lang.Class t1Class = null;
	java.lang.Class t2Class = null;
	try {
	    java.net.URLClassLoader loader = (java.net.URLClassLoader) ((jaba.sym.Program) method.getContainingType ().getProgram ()).getClassLoader ();
	    t1Class = loader.loadClass(t1.getName());
	    t2Class = loader.loadClass(t2.getName());
	}catch (ClassNotFoundException e) {
	    throw new RuntimeException(e);
	}
	
	if(t1Class.isAssignableFrom(t2Class))
	    return t1;
	
	if(t2Class.isAssignableFrom(t1Class))
	    return t2;

	// For Interface or array, if comes here,
	// we simply return java.lang.Object 
	TypeEntry objectType = (TypeEntry) method.getContainingType().getProgram().
		getSymbolTable().getNamedReferenceType("java.lang.Object");
	//	System.out.println("Object: " + objectType.getName());

	if(t1 instanceof jaba.sym.Interface || t2 instanceof jaba.sym.Interface)
	    return objectType;

	if(t1 instanceof jaba.sym.Array || t2 instanceof jaba.sym.Array)
	    return objectType;

	//For Class type, dig into super class
	do {
	    t2 = ((jaba.sym.Class)t2).getSuperClass();
	    t2Class = t2Class.getSuperclass();
	} while(!t2Class.isAssignableFrom(t1Class));

	return t2;
    }
    /**
     * Pairing of an edge and an operand stack.  Needed to keep track
     * of the edge that brought a particular operand stack to a
     * successor.  This is useful when there are multiple operand
     * stacks to be merged for a joining node in the graph.  We need
     * to know the specific edge along which the operand stack flowed
     * to the joining node so that we may correctly summarize the
     * propogated variable use.
     * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
     */
    private static class EdgeOpStackPair implements java.io.Serializable
    {
	public Edge edge;
	public OperandStack opStack;
    }

}
