package jaba.main;

import jaba.sym.Program;
import jaba.sym.ProgramImpl;

import java.util.StringTokenizer;

import java.io.File;
import java.io.BufferedInputStream;

import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLConnection;

import profile.TimeReporter;


/**
 * Main driver class for the java analysis system. JABA users should
 * extend this class and redefine method {@link #run()} to create
 * their own drivers. Int addition, users can also redefine method
 * {@link #init(String[])}, {@link #quit()}, and {@link #usage()}.
 * @author Alex Orso and Huaxing Wu -- <i>Created.  October 11, 2002</i>
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/12 removed reference to the Arg Processor class.
 */
public class JABADriver implements java.io.Serializable
{
    static {
        boolean assertsEnabled = false;
        assert assertsEnabled = true; // Intentional side effect!!!
	File file = new File("./.noassertions");
        if ( (!assertsEnabled) && (!file.exists()) )
            throw new RuntimeException("\n" +
				       "\n********************************************************************" +
				       "\n*                                                                  *" +
				       "\n* JDK >= 1.4 must be used and asserts must be enabled to run JABA! *" +
				       "\n* either use the 'jaba' script provided with the distribution      *" +
				       "\n* or provide the '-ea' switch when invoking java                   *" +
				       "\n*                                                                  *" +
				       "\n********************************************************************" +
				       "\n"
				       );
    }

	/** ??? */
    public static Throwable exception = null;

	/** ??? */
    public static String arguments = "";

	/** ??? */
    protected static Program program = null;

	/** ??? */
    protected static String rcs[] = null;

	/** ??? */
    protected String resourceFileName = null;

    /**
     * Main driver for the java analysis system. It Creates an object
     * of the subclass of {@link #JABADriver} specified in the command
     * line and invokes three methods on the created object: (1)
     * method {@link #init(String[])}, passing all but the first
     * command-line parameters to it; (2) method {@link #run()}; and
     * (4) method {@link #quit()}
     * @param argv Command line arguments.  The first argument must be
     * the type of the user-defined driver. Although the second
     * argument is generally the resouce file for the program to be
     * analyzed, the set of parameters from the second on may actually
     * depend on the user-defined driver.
     */
    public static final void main( String[] argv )
    {

	String version = jaba.sym.ProgramImpl.getsSn();

	try {
	    int timeout = 1000; // one second
	    InetAddress ip = InetAddress.getByName("measure.cc.gt.atl.ga.us");
	    SocketAddress socketAddress =
		new InetSocketAddress(ip, 80);
	    Socket s = new Socket();
	    s.connect(socketAddress, timeout);
	    s.close();

	    // Network up!

	    String fileURL = "http://measure.cc.gt.atl.ga.us/jaba/versions";
	    URL url = new URL ( fileURL );

	    URLConnection conn = url.openConnection();

	    BufferedInputStream  bis = new
		BufferedInputStream(conn.getInputStream());

	    byte[] buff = new byte[2048];
	    int bytesRead;

	    String vers = new String();
	    while( (bytesRead = bis.read(buff, 0, buff.length)) != -1 ) {
		vers += new String(buff);
	    }
	    StringTokenizer st = new StringTokenizer(vers);
	    String token = null;
	    String latest = null;
	    while( st.hasMoreTokens() ) {
		token = st.nextToken();
		if( token.equals(version) ||
		    token.equals("*********") ) {
		    latest = st.nextToken();

// 		    if(!latest.equals(version)) {
// 			System.out.println("\nWarning: you are running an old version of JABA:\n" +
// 					   " your version is \"" +
// 					   version + "\"\n" +
// 					   " latest version is \"" +
// 					   latest + "\"\n" +
// 					   "Contact jabaadmin@measure.cc.gt.atl.ga.us to get an updated version\n");
// 		    }
		    break;
		}
	    }
	}
	catch(Exception e) {
	    // Network down; do nothing
	}

	for(int arg=0; arg < argv.length; arg++) {
	    arguments += argv[arg] + " ";
	}

 	// OptionsImpl.createLVT = true;

	// These three have been deprecated and removed.  They are unreferenced.
	//jaba.sym.Program.analysisPattern = true;
	//jaba.sym.Primitive.analysisPattern = false;
	//jaba.sym.SymbolTable.analysisPattern = true;

	try {
	    if( argv.length == 0 ) {
		System.err.println("Please specify the main class");
		throw(new IllegalArgumentException());
	    }

	    JABADriver jdriver =
		(JABADriver)java.lang.Class.forName(argv[0]).newInstance();

	    String params[] = new String[argv.length - 1];
	    for (int i = 0; i < params.length; i++) {
		params[i] = argv[i+1];
	    }

	    jdriver.init( params );
	    jdriver.run();
	    jdriver.quit();
	}
	catch(Throwable e) {
		e.printStackTrace ();
	    exception = e;
	    String ename = e.getClass().getName();
	    if( ename.equals("jaba.sym.IllegalUseException") ) {
		System.err.println(ename);
	    }
	    else {
		System.err.println(e);
	    }
	}
    }

    /**
     * Prints the version of JABA.
     */
    protected static String version()
    {
	return jaba.sym.ProgramImpl.getsSn();
    }

    /**
     * Initializes the java analysis system.  Reads in a resource file
     * specifying all of the class files to be analyzed, loads those
     * classfiles, creates a {@link Program} object for the system
     * under analysis, saves a reference to such object in attribute
     * {@link #program}, and saves any unused argument int
     * array {@link #rcs}.
     * @param argv Command line arguments.  The first argument
     * specifies the resouce file. Additional argument can be used by
     * other methods of the class through array {@link #rcs}.
     */
    protected void init( String[] argv )
    {
	// Set using RLVT by default
	// OptionsImpl.createLVT = true;

	int i;
	for(i=0; i<argv.length; i++) {
	    if( argv[i].equals("--") ) break;
	    else if( argv[i].equals("-l") ) OptionsImpl.createLVT = true;
	    else if( argv[i].equals("-r") ) OptionsImpl.analyzeRunTimeException = true;
		else if( argv[i].equals("-if") ) OptionsImpl.inlineFinally = true;
	    else if( argv[i].equals("-v") ) {
		System.out.println(ProgramImpl.getsSn());
		System.exit(0);
	    }
	    else if( argv[i].startsWith("-") ) {
		usage();
		System.exit(0);
	    }
	    else {
		resourceFileName = argv[i];
		ResourceFile rcFile = new ResourceFile(resourceFileName);
		// create and load program object
		program = new ProgramImpl (rcFile);
	    }
	}

	i++;
	int numopts = argv.length - i;
	if( numopts <= 0 ) rcs = new String[0];
	else {
	    rcs = new String[numopts];
	    for(int l=0; l<numopts; i++, l++) {
		rcs[l] = argv[i];
	    }
	}
    }

    /**
     * Performs the actual analysis. Must be redefined by the user.
     */
    protected void run()
    {
	System.err.println("You are running the default, empty driver");
    }

    /**
     * Prints usage information.
     */
    protected static void usage()
    {
        System.out.println("\nUsage: <your driver> [OPTION]* [<resource file>]* -- [<user-defined option>]*");
	System.out.println("\nOPTION is any of these:");
	System.out.println("  -v,                      "+
		    "output version information and exit.");
	System.out.println("  <resource file>*         "+
		    "one or more resource files for the system(s) to");
	System.out.println("                           be analyzed");
	System.out.println("  <user-defined option>*   "+
		    "one or more options used byte the user-defined");
	System.out.println("                           driver");
	System.out.println("");
    }

    /**
     * Empty method. Can be redefined by the user to perform some
     * final clean-up after the analysis.
     */
    protected void quit()
    {
    }

    static void __link() { TimeReporter.__link(); }
}
