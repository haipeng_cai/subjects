package jaba.main;

import java.util.Vector;
import java.util.StringTokenizer;

import java.io.File;

/**
 * Represents the parameters to the Graph output in the Dotty format
 *
 * @author Lakshmish Ramaswamy -- <i>Created</i>
 * @author Caleb Ho            -- <i>Revised
 * revisions: changed the naming convention of the methods to standard
 * naming convention i.e. from setlabelspec() to setLabelSpec. To help the
 * DottyOutputSpec class represent the parameters in a more object-oriented
 * fashion, the method getOutputParams was added, so the params can be obtained
 * directly through the DottyOutputSpec, rather than the old way of asking the
 * resource file for the params. The load(ResourceFile) method is no longer
 * needed</i>
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/06/23 added back in a modified load method for populating the object; fixed JavaDoc
 */
public class DottyOutputSpec implements java.io.Serializable
{
  /** The output specification in default mode */
  public static final int DEFAULT = 1;

  /** Output specification in customized mode as specified in Resource file */
  public static final int CUSTOMIZED = 0;

  /** Label indicating whether the output is in default or customized mode */
  private int labelspec = DEFAULT;

  /** Vector containing all the parameters that have to written to output */
  private Vector outputparam;

  /** Constructor */
  public DottyOutputSpec(){
   outputparam = new Vector();
  }

  /** Sets the label to specifed mode (Either DEFAULT or CUSTOMIZED */
  public void setLabelSpec(int LabelSpec) {
    labelspec = LabelSpec;
    return;
  }

  /** Returns the output mode (DEFAULT or CUSTOMIZED) */
  public int getLabelSpec(){
   return labelspec;
  }

  /** Adds a paramter to the output parameter vector */
  public void addParam(String param){
   outputparam.addElement(param);
   return;
  }

  /** Returns the vector containing all the output specs*/
  public Vector getOutputParams(){
    return outputparam;
  }

  /** Checks whether the output parameter vector conatins a particular parameter */
  public boolean labelContains(String param) {
   return outputparam.contains(param);
  }

  /** Loads the parameter vector from Resource file */
  public void load (String params)
  {
        StringTokenizer tokenizer = new StringTokenizer (params, File.pathSeparator);
        
        // modify the outputParams to know that it's customized
        setLabelSpec (CUSTOMIZED);
        
        // add the parameters into the object
        while (tokenizer.hasMoreTokens ())
	{
            String parameter = tokenizer.nextToken ().trim ();
            if (parameter.equals (""))
		continue;

            outputparam.addElement (parameter);
        }
  }
}
