/* Copyright (c) 1999, The Ohio State University */

package jaba.main;

import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;

import java.util.zip.ZipFile;

import java.util.Properties;
import java.util.StringTokenizer;
import java.util.List;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Represents a resource file specifying the program name, class path to use
 * for analysis, and class files to analyze.  This class reads and parses a
 * resource file and provides accessor methods to this data.
 * @author Jim Jones -- <i>Created.  April 1, 1999</i>
 * @author Lakshmish Ramaswamy -- <i> Revised January 2, 2001
 * added loadDottyOutputParamters() and getDottyOutputVector() functions </i>
 * @author Caleb Ho -- <i> Revised. Redisgned to be more object
 * oriented, by having the ResourceFile use the DottyOutputSpec class for storing and managing
 * output specs rather than going indirectly through the ResourceFile. </i>
 * @author Huaxing Wu -- <i> Revised. 3/21/02 Replace method loadClassFilePathNames() with loadClassFiles()
 * 			to just load the class files. Change method loadClassPaths() to validate each classpath
 * 			and initialize relative fields </i>
 * @author Jay Lofstead 2003/06/05 changed to use enumerations
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 */
public class ResourceFromFile extends GenericResource
{
    /**
     * Constructor.  Loads the resource file to initialize this object.
     * @param resourceFileName  Full path name (relative or absolute) of the
     *                          resource file to be loaded.
     * @exception IllegalArgumentException  This is thrown when the resource file
     *                                      is in an invalid format or variables
     *                                      defined within are invalid or absent.
     */
    public ResourceFromFile (String resourceFileName) throws IllegalArgumentException
    {
        super ();
	Properties rcProperties = null;
        try
	{
            // open resource file for reading
            FileInputStream resourceIn = new FileInputStream (resourceFileName);
            
            // read in all key/value pairs
            rcProperties = new Properties ();
            rcProperties.load (resourceIn);
	    resourceIn.close ();

		int index = resourceFileName.lastIndexOf (File.separator);
		if (index != -1)
		{
			setBaseDir (resourceFileName.substring (0, index));
		}
        }
        catch (IOException e)
	{
	    throw new RuntimeException ("Error: IOException occurred while reading resource file " + resourceFileName
					+ ": " + e.getMessage ()
					);
        }
        
        // load each of the variables from the Properties into my fields
        loadProgramName (rcProperties, resourceFileName);
        loadClassPaths (rcProperties, resourceFileName);
	loadClassFiles (rcProperties, resourceFileName);
        loadDottyOutputParameters (rcProperties, resourceFileName);
    }

    /** use parent class version */
    public String toString ()
    {
	return super.toString ();
    }

    /** Loads the program name from the resource file. */
    private void loadProgramName (Properties rcProperties, String resourceFileName)
    {
        String newName = rcProperties.getProperty ("ProgramName");

        if (newName != null && !newName.equals (""))
	    setProgramName (newName);
    }
    
    /** Loads the class paths from the resource file. */
    private void loadClassPaths (Properties rcProperties, String resourceFileName)
    {
        // parse the ClassPath variable
        String classPathString = rcProperties.getProperty ("ClassPath");
        if (classPathString == null || classPathString.equals (""))
	{
            throw new IllegalArgumentException ("ClassPath variable must be set in resource file: " + resourceFileName);
	}
        
	if (loadClassPaths (classPathString) == 0)
	{
            throw new IllegalArgumentException ("ClassPath variable must include at least one path: " + classPathString);
	}
    }
    
    /** Loads the class files from the resource file. */
    private void loadClassFiles (Properties rcProperties, String resourceFileName)
    {
        // parse the ClassFiles variable
        String classFilesString = rcProperties.getProperty ("ClassFiles");
        if (classFilesString == null || classFilesString.equals (""))
	{
            throw new IllegalArgumentException ("ClassFiles variable must be set in the resource file: " + resourceFileName);
	}
        
	if (loadClassFiles (classFilesString) == 0)
	{
            throw new IllegalArgumentException ("ClassFile variable must include at least one class: " + classFilesString);
	}
    }

    /** Loads the Output Parameters for the dotty graph output */
    private void loadDottyOutputParameters (Properties rcProperties, String resourceFileName)
    {
        // parse the output parameter variable
        String outputString = rcProperties.getProperty ("DottyOutputParameters");
        if (outputString == null || outputString.equals (""))
	{
            return;
        }

	dottyOutputParams.load (outputString);
    }
}
