package jaba.tools.remote;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;

import java.net.MalformedURLException;

import jaba.graph.Graph;
import jaba.graph.StatementNode;
import jaba.graph.FinallyEntryAttribute;
import jaba.graph.FinallyTransferAttribute;
import jaba.graph.FinallyContextAttribute;
import jaba.graph.FinallyCallAttribute;
import jaba.graph.MethodCallAttribute;
import jaba.graph.NewInstanceAttribute;
import jaba.graph.ExceptionAttribute;
import jaba.graph.ReturnSitesAttribute;
import jaba.graph.ThrowStatementAttribute;
import jaba.graph.ClassHierarchyGraph;
import jaba.graph.CallGraph;
import jaba.graph.Edge;
import jaba.graph.MethodCallEdgeAttribute;
import jaba.graph.ThrowEdgeAttribute;
import jaba.graph.ReturnEdgeAttribute;
import jaba.graph.Node;

import jaba.graph.acfg.ACFG;

import jaba.graph.cdg.CDG;

import jaba.graph.cfg.CFG;
import jaba.graph.cfg.ExceptionHandlers;

import jaba.graph.dom.DominanceFrontiers;
import jaba.graph.dom.DominatorTree;
import jaba.graph.dom.PostDominanceFrontiers;
import jaba.graph.dom.PostDominatorTree;

import jaba.graph.hammock.HammockGraph;
import jaba.graph.hammock.HammockNode;

import jaba.graph.icfg.ICFG;

import jaba.sym.Program;
import jaba.sym.Method;
import jaba.sym.NamedReferenceType;

import jaba.main.Options;
import jaba.main.ResourceFileI;
import jaba.main.DottyOutputSpec;

import jaba.tools.graph.GDG;

/**
 * This class provides an interface mechanism for retrieving various
 * JABA objects from either a server-side implementation or from a
 * local implementation with no change in the client code.
 *
 * @author Jay Lofstead 2003/06/23 created
 */
public class Factory
{
	/** host to obtain the objects from */
	public static String host = "localhost";

	/** flag to verify the security manager has been set */
	private static boolean securityManagerInstalled = false;

	/** returns the current build version of JABA */
	public static String getVersion ()
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getVersion ();
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** return a client-side Options object */
	public static Options getOptions ()
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getOptions ();
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** return a client-side Resources object */
	public static ResourceFileI getResources ()
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getResources ();
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** return a client-side Resources object */
	public static ResourceFileI getResources (String file)
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getResources (file);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** return a client-side Program object */
	public static Program getProgram (ResourceFileI res, Options opt)
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getProgram (res, opt);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** returns the ICFG associated with the passed in Program */
	public static ICFG getICFG (Program p)
	{
		// Since I know that I am building the ICFG before I get
		// a program object back in the client-server mode, I
		// can just get it from the program object directly
		// rather than causing a server call:
		return (ICFG) p.getAttributeOfType ("jaba.graph.icfg.ICFG");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getICFG (p);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** returns the CallGraph associated with the passed in Program */
	public static CallGraph getCallGraph (Program p)
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getCallGraph (p);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** returns the ClassHierarchyGraph associated with the passed in Program */
	public static ClassHierarchyGraph getClassHierarchyGraph (Program p)
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getClassHierarchyGraph (p);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** returns the CFG associated with the passed in Method */
	public static CFG getCFG (Method m)
	{
		// Since I am building the ICFG before the program
		// is returned, this method will already have the CFG.
		// Just return that rather than going to the server.
		return (CFG) m.getAttributeOfType ("jaba.graph.cfg.CFG");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getCFG (m);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** returns the ACFG associated with the passed in Program */
	public static ACFG getACFG (Method m)
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getACFG (m);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** returns the GDG associated with the passed in Program */
	public static GDG getGDG (Method m)
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getGDG (m);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** returns the GDG associated with the passed in Program */
	public static ExceptionHandlers getExceptionHandlers (Method m)
	{
		// Since these are built as part of the CFG creation, just
		// grab the one that is already in the method and return it.
		return (ExceptionHandlers) m.getAttributeOfType ("jaba.graph.cfg.ExceptionHandlers");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getExceptionHandlers (m);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** returns the CDG associated with the passed in Graph */
	public static CDG getCDG (Graph g)
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getCDG (g);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** returns the HammockGraph associated with the passed in Graph */
	public static HammockGraph getHammockGraph (Graph g)
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getHammockGraph (g);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** returns the PostDominatorTree associated with the passed in Graph */
	public static PostDominatorTree getPostDominatorTree (Graph g)
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getPostDominatorTree (g);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** returns the DominatorTree associated with the passed in Graph */
	public static DominatorTree getDominatorTree (Graph g)
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getDominatorTree (g);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** returns the DominanceFrontiers associated with the passed in Graph */
	public static DominanceFrontiers getDominanceFrontiers (Graph g)
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getDominanceFrontiers (g);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** returns the PostDominanceFrontiers associated with the passed in Graph */
	public static PostDominanceFrontiers getPostDominanceFrontiers (Graph g)
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getPostDominanceFrontiers (g);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** returns the FinallyEntryAttribute associated with the passed in StatementNode */
	public static FinallyEntryAttribute getFinallyEntryAttribute (StatementNode s)
	{
		// Since this isn't a demand drive attribute, just return the stored version
		return (FinallyEntryAttribute) s.getAttributeOfType ("jaba.graph.FinallyEntryAttribute");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getFinallyEntryAttribute (s);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** returns the FinallyTransferAttribute associated with the passed in StatementNode */
	public static FinallyTransferAttribute getFinallyTransferAttribute (StatementNode s)
	{
		// Since this isn't a demand drive attribute, just return the stored version
		return (FinallyTransferAttribute) s.getAttributeOfType ("jaba.graph.FinallyTransferAttribute");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getFinallyTransferAttribute (s);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** returns the NewInstanceAttribute associated with the passed in StatementNode */
	public static NewInstanceAttribute getNewInstanceAttribute (StatementNode s)
	{
		// Since this isn't a demand drive attribute, just return the stored version
		return (NewInstanceAttribute) s.getAttributeOfType ("jaba.graph.NewInstanceAttribute");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getNewInstanceAttribute (s);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** returns the ExceptionAttribute associated with the passed in StatementNode */
	public static ExceptionAttribute getExceptionAttribute (StatementNode s)
	{
		// Since this isn't a demand drive attribute, just return the stored version
		return (ExceptionAttribute) s.getAttributeOfType ("jaba.graph.ExceptionAttribute");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getExceptionAttribute (s);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** returns the ReturnSitesAttribute associated with the passed in StatementNode */
	public static ReturnSitesAttribute getReturnSitesAttribute (StatementNode s)
	{
		// Since this isn't a demand drive attribute, just return the stored version
		return (ReturnSitesAttribute) s.getAttributeOfType ("jaba.graph.ReturnSitesAttribute");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getReturnSitesAttribute (s);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** returns the ThrowStatementAttribute associated with the passed in StatementNode */
	public static ThrowStatementAttribute getThrowStatementAttribute (StatementNode s)
	{
		// Since this isn't a demand drive attribute, just return the stored version
		return (ThrowStatementAttribute) s.getAttributeOfType ("jaba.graph.ThrowStatementAttribute");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getThrowStatementAttribute (s);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** returns the MethodCallEdgeAttribute associated with the passed in Edge */
	public static MethodCallEdgeAttribute getMethodCallEdgeAttribute (Edge e)
	{
		// Since this isn't a demand drive attribute, just return the stored version
		return (MethodCallEdgeAttribute) e.getAttributeOfType ("jaba.graph.MethodCallEdgeAttribute");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getMethodCallEdgeAttribute (e);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** returns the ThrowEdgeAttribute associated with the passed in Edge */
	public static ThrowEdgeAttribute getThrowEdgeAttribute (Edge e)
	{
		// Since this isn't a demand drive attribute, just return the stored version
		return (ThrowEdgeAttribute) e.getAttributeOfType ("jaba.graph.ThrowEdgeAttribute");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getThrowEdgeAttribute (e);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** returns the ReturnEdgeAttribute associated with the passed in Edge */
	public static ReturnEdgeAttribute getReturnEdgeAttribute (Edge e)
	{
		// Since this isn't a demand drive attribute, just return the stored version
		return (ReturnEdgeAttribute) e.getAttributeOfType ("jaba.graph.ReturnEdgeAttribute");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getReturnEdgeAttribute (e);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** create a new DottyOutputSpec object */
	public static DottyOutputSpec newDottyOutputSpec ()
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.newDottyOutputSpec ();
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** create a new Edge object */
	public static Edge newEdge (Node source, Node sink)
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.newEdge (source, sink);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** create a new Edge object */
	public static Edge newEdge (Node source, Node sink, String label)
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.newEdge (source, sink, label);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** create a new StatementNode object */
	public static StatementNode newStatementNode ()
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.newStatementNode ();
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** create a new HammockNode object */
	public static HammockNode newHammockNode ()
	{
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.newHammockNode ();
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
	}

	/** get a FinallyCallAttribute from a StatementNode object */
	public static FinallyCallAttribute getFinallyCallAttribute (StatementNode sn)
	{
		// Since this isn't a demand drive attribute, just return the stored version
		return (FinallyCallAttribute) sn.getAttributeOfType ("jaba.graph.FinallyCallAttribute");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getFinallyCallAttribute (sn);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** get a FinallyContextAttribute from an Edge object  */
	public static FinallyContextAttribute getFinallyContextAttribute (Edge e)
	{
		// Since this isn't a demand drive attribute, just return the stored version
		return (FinallyContextAttribute) e.getAttributeOfType ("jaba.graph.FinallyContextAttribute");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getFinallyContextAttribute (e);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** get a MethodCallAttribute from a StatementNode object  */
	public static MethodCallAttribute getMethodCallAttribute (StatementNode sn)
	{
		// Since this isn't a demand drive attribute, just return the stored version
		return (MethodCallAttribute) sn.getAttributeOfType ("jaba.graph.MethodCallAttribute");
/*
		if (!securityManagerInstalled)
		{
			addSecurityManager ();
		}

		try
		{
			FactoryServerSideProxy server = (FactoryServerSideProxy) Naming.lookup ("//" + host + "/JABAServer");

			return server.getMethodCallAttribute (sn);
		}
		catch (RemoteException re)
		{
			System.err.println (re.getMessage ());
			re.printStackTrace ();

			return null;
		}
		catch (NotBoundException nbe)
		{
			System.err.println (nbe.getMessage ());
			nbe.printStackTrace ();

			return null;
		}
		catch (MalformedURLException mue)
		{
			System.err.println (mue.getMessage ());
			mue.printStackTrace ();

			return null;
		}
*/
	}

	/** installs the RMI security manager */
	private static void addSecurityManager ()
	{
		System.setSecurityManager (new java.rmi.RMISecurityManager ());
		securityManagerInstalled = true;
	}
}
