package jaba.tools.remote;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.RMISecurityManager;

import java.rmi.server.UnicastRemoteObject;

import java.net.InetAddress;

import jaba.graph.Graph;
import jaba.graph.StatementNode;
import jaba.graph.StatementNodeImpl;
import jaba.graph.FinallyEntryAttribute;
import jaba.graph.FinallyTransferAttribute;
import jaba.graph.FinallyContextAttribute;
import jaba.graph.FinallyCallAttribute;
import jaba.graph.MethodCallAttribute;
import jaba.graph.NewInstanceAttribute;
import jaba.graph.ExceptionAttribute;
import jaba.graph.ReturnSitesAttribute;
import jaba.graph.ThrowStatementAttribute;
import jaba.graph.ClassHierarchyGraph;
import jaba.graph.CallGraph;
import jaba.graph.Edge;
import jaba.graph.EdgeImpl;
import jaba.graph.MethodCallEdgeAttribute;
import jaba.graph.ThrowEdgeAttribute;
import jaba.graph.ReturnEdgeAttribute;
import jaba.graph.Node;

import jaba.graph.acfg.ACFG;

import jaba.graph.cdg.CDG;

import jaba.graph.cfg.CFG;
import jaba.graph.cfg.ExceptionHandlers;

import jaba.graph.dom.DominanceFrontiers;
import jaba.graph.dom.DominatorTree;
import jaba.graph.dom.PostDominanceFrontiers;
import jaba.graph.dom.PostDominatorTree;

import jaba.graph.hammock.HammockGraph;
import jaba.graph.hammock.HammockNode;
import jaba.graph.hammock.HammockNodeImpl;

import jaba.graph.icfg.ICFG;

import jaba.sym.Program;
import jaba.sym.Method;
import jaba.sym.NamedReferenceType;

import jaba.main.Options;
import jaba.main.ResourceFileI;
import jaba.main.DottyOutputSpec;

import jaba.tools.graph.GDG;

/**
 * This interface defines the methods available for the client-side Factory to
 * use for requesting objects.
 *
 * @author Jay Lofstead 2003/06/25 created
 */
public class FactoryServerSideProxyImpl extends UnicastRemoteObject implements FactoryServerSideProxy
{
	/** return a client-side Program object */
	public Program getProgram (ResourceFileI res, Options opt) throws RemoteException
	{
		Program p = new jaba.sym.ProgramImpl (res, opt);

		// in order for the Method objects to be completely built, the
		// CFGs for each will have to be built.  Do that now to avoid
		// problems with requesting attributes before they are built
		// by the CFG build process.  There are a few other attributes
		// that are only built by requesting the ICFG.  Since the CFGs
		// will be built when the ICFG is requested, go ahead and
		// request the ICFG instead to finish building the attributes.
		p.getAttributeOfType ("jaba.graph.icfg.ICFG");

		return p;
	}

	/** return a client-side Options object */
	public Options getOptions () throws RemoteException
	{
		return new jaba.main.OptionsImpl ();
	}

	/** return a client-side Resources object */
	public ResourceFileI getResources () throws RemoteException
	{
		return new jaba.main.ResourceBundle ();
	}

	/** return a client-side Resources object */
	public ResourceFileI getResources (String file) throws RemoteException
	{
		return new jaba.main.ResourceBundle (file);
	}

	/** returns the current build version of JABA */
	public String getVersion () throws RemoteException
	{
		return jaba.sym.ProgramImpl.getsSn ();
	}

	/** returns the ICFG associated with the passed in Program */
	public ICFG getICFG (Program p) throws RemoteException
	{
		return (ICFG) p.getAttributeOfType ("jaba.graph.icfg.ICFG");
	}

	/** returns the CFG associated with the passed in Program */
	public CFG getCFG (Method m) throws RemoteException
	{
		return (CFG) m.getAttributeOfType ("jaba.graph.cfg.CFG");
	}

	/** returns the ACFG associated with the passed in Program */
	public ACFG getACFG (Method m) throws RemoteException
	{
		return (ACFG) m.getAttributeOfType ("jaba.graph.acfg.ACFG");
	}

	/** returns the GDG associated with the passed in Program */
	public GDG getGDG (Method m) throws RemoteException
	{
		return (GDG) m.getAttributeOfType ("jaba.tools.graph.GDG");
	}

	/** returns the ExceptionHandlers associated with the passed in Method */
	public ExceptionHandlers getExceptionHandlers (Method m) throws RemoteException
	{
		return (ExceptionHandlers) m.getAttributeOfType ("jaba.graph.cfg.ExceptionHandlers");
	}

	/** returns the CDG associated with the passed in Program */
	public CDG getCDG (Graph g) throws RemoteException
	{
		return (CDG) g.getAttributeOfType ("jaba.graph.cdg.CDG");
	}

	/** returns the CallGraph associated with the passed in Program */
	public CallGraph getCallGraph (Program p) throws RemoteException
	{
		return (CallGraph) p.getAttributeOfType ("jaba.graph.CallGraph");
	}

	/** returns the ClassHierarchyGraph associated with the passed in Program */
	public ClassHierarchyGraph getClassHierarchyGraph (Program p) throws RemoteException
	{
		return (ClassHierarchyGraph) p.getAttributeOfType ("jaba.graph.ClassHierarchyGraph");
	}


	/** returns the DominatorTree associated with the passed in Graph */
	public DominatorTree getDominatorTree (Graph g) throws RemoteException
	{
		return (DominatorTree) g.getAttributeOfType ("jaba.graph.dom.DominatorTree");
	}

	/** returns the DominanceFrontiers associated with the passed in Graph */
	public DominanceFrontiers getDominanceFrontiers (Graph g) throws RemoteException
	{
		return (DominanceFrontiers) g.getAttributeOfType ("jaba.graph.dom.DominanceFrontiers");
	}

	/** returns the PostDominatorTree associated with the passed in Graph */
	public PostDominatorTree getPostDominatorTree (Graph g) throws RemoteException
	{
		return (PostDominatorTree) g.getAttributeOfType ("jaba.graph.dom.PostDominatorTree");
	}

	/** returns the PostDominanceFrontiers associated with the passed in Graph */
	public PostDominanceFrontiers getPostDominanceFrontiers (Graph g) throws RemoteException
	{
		return (PostDominanceFrontiers) g.getAttributeOfType ("jaba.graph.dom.PostDominanceFrontiers");
	}

	/** returns the HammockGraph associated with the passed in Graph */
	public HammockGraph getHammockGraph (Graph g) throws RemoteException
	{
		return (HammockGraph) g.getAttributeOfType ("jaba.graph.hammock.HammockGraph");
	}

	/** returns the FinallyEntryAttribute associated with the passed in StatementNode */
	public FinallyEntryAttribute getFinallyEntryAttribute (StatementNode s) throws RemoteException
	{
		return (FinallyEntryAttribute) s.getAttributeOfType ("jaba.graph.FinallyEntryAttribute");
	}

	/** returns the FinallyTransferAttribute associated with the passed in StatementNode */
	public FinallyTransferAttribute getFinallyTransferAttribute (StatementNode s) throws RemoteException
	{
		return (FinallyTransferAttribute) s.getAttributeOfType ("jaba.graph.FinallyTransferAttribute");
	}

	/** returns the NewInstanceAttribute associated with the passed in StatementNode */
	public NewInstanceAttribute getNewInstanceAttribute (StatementNode s) throws RemoteException
	{
		return (NewInstanceAttribute) s.getAttributeOfType ("jaba.graph.NewInstanceAttribute");
	}

	/** returns the ExceptionAttribute associated with the passed in StatementNode */
	public ExceptionAttribute getExceptionAttribute (StatementNode s) throws RemoteException
	{
		return (ExceptionAttribute) s.getAttributeOfType ("jaba.graph.ExceptionAttribute");
	}

	/** returns the ReturnSitesAttribute associated with the passed in StatementNode */
	public ReturnSitesAttribute getReturnSitesAttribute (StatementNode s) throws RemoteException
	{
		return (ReturnSitesAttribute) s.getAttributeOfType ("jaba.graph.ReturnSitesAttribute");
	}

	/** returns the ThrowStatementAttribute associated with the passed in StatementNode */
	public ThrowStatementAttribute getThrowStatementAttribute (StatementNode s) throws RemoteException
	{
		return (ThrowStatementAttribute) s.getAttributeOfType ("jaba.graph.ThrowStatementAttribute");
	}

	/** returns the MethodCallEdgeAttribute associated with the passed in Edge */
	public MethodCallEdgeAttribute getMethodCallEdgeAttribute (Edge e) throws RemoteException
	{
		return (MethodCallEdgeAttribute) e.getAttributeOfType ("jaba.graph.MethodCallEdgeAttribute");
	}

	/** returns the ThrowEdgeAttribute associated with the passed in Edge */
	public ThrowEdgeAttribute getThrowEdgeAttribute (Edge e) throws RemoteException
	{
		return (ThrowEdgeAttribute) e.getAttributeOfType ("jaba.graph.ThrowEdgeAttribute");
	}

	/** returns the ReturnEdgeAttribute associated with the passed in Edge */
	public ReturnEdgeAttribute getReturnEdgeAttribute (Edge e) throws RemoteException
	{
		return (ReturnEdgeAttribute) e.getAttributeOfType ("jaba.graph.ReturnEdgeAttribute");
	}

	/** create a new DottyOutputSpec object */
	public DottyOutputSpec newDottyOutputSpec () throws RemoteException
	{
		return new DottyOutputSpec ();
	}

	/** create a new Edge object */
	public Edge newEdge (Node source, Node sink) throws RemoteException
	{
		return new EdgeImpl (source, sink);
	}

	/** create a new Edge object */
	public Edge newEdge (Node source, Node sink, String label)
	{
		return new EdgeImpl (source, sink, label);
	}

	/** create a new StatementNode object */
	public StatementNode newStatementNode () throws RemoteException
	{
		return new StatementNodeImpl ();
	}

	/** create a new HammockNode object */
	public HammockNode newHammockNode () throws RemoteException
	{
		return new HammockNodeImpl ();
	}

	/** get a FinallyCallAttribute from a StatementNode object */
	public FinallyCallAttribute getFinallyCallAttribute (StatementNode sn) throws RemoteException
	{
		return (FinallyCallAttribute) sn.getAttributeOfType ("jaba.graph.FinallyCallAttribute");
	}

	/** get a FinallyContextAttribute from an Edge object  */
	public FinallyContextAttribute getFinallyContextAttribute (Edge e) throws RemoteException
	{
		return (FinallyContextAttribute) e.getAttributeOfType ("jaba.graph.FinallyContextAttribute");
	}

	/** get a MethodCallAttribute from a StatementNode object  */
	public MethodCallAttribute getMethodCallAttribute (StatementNode sn) throws RemoteException
	{
		return (MethodCallAttribute) sn.getAttributeOfType ("jaba.graph.MethodCallAttribute");
	}

	/** constructor for proxy objects */
	private FactoryServerSideProxyImpl () throws RemoteException
	{
	}

	/** server side RMI registration mechanism */
	public static void main (String args [])
	{
		// Create and install a security manager
		if (System.getSecurityManager () == null)
		{
			System.setSecurityManager (new RMISecurityManager ());
		}

		try
		{
			FactoryServerSideProxyImpl obj = new FactoryServerSideProxyImpl ();

			// Bind this object instance to the name "JABAServer"
			Naming.rebind ("//" + InetAddress.getLocalHost ().getHostName () + "/JABAServer", obj);

			System.out.println ("'JABAServer' bound in RMI registry");
		}
		catch (Exception e)
		{
			System.out.println ("FactoryServerSideProxyImpl err: " + e.getMessage ());
			e.printStackTrace ();
		}
	}
}
