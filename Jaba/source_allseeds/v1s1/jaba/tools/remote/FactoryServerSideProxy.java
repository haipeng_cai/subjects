package jaba.tools.remote;

import java.rmi.Remote;
import java.rmi.RemoteException;

import jaba.graph.Graph;
import jaba.graph.StatementNode;
import jaba.graph.StatementNodeImpl;
import jaba.graph.FinallyEntryAttribute;
import jaba.graph.FinallyTransferAttribute;
import jaba.graph.FinallyContextAttribute;
import jaba.graph.FinallyCallAttribute;
import jaba.graph.MethodCallAttribute;
import jaba.graph.NewInstanceAttribute;
import jaba.graph.ExceptionAttribute;
import jaba.graph.ReturnSitesAttribute;
import jaba.graph.ThrowStatementAttribute;
import jaba.graph.ClassHierarchyGraph;
import jaba.graph.CallGraph;
import jaba.graph.Edge;
import jaba.graph.EdgeImpl;
import jaba.graph.MethodCallEdgeAttribute;
import jaba.graph.ThrowEdgeAttribute;
import jaba.graph.ReturnEdgeAttribute;
import jaba.graph.Node;

import jaba.graph.acfg.ACFG;

import jaba.graph.cdg.CDG;

import jaba.graph.cfg.CFG;
import jaba.graph.cfg.ExceptionHandlers;

import jaba.graph.dom.DominanceFrontiers;
import jaba.graph.dom.DominatorTree;
import jaba.graph.dom.PostDominanceFrontiers;
import jaba.graph.dom.PostDominatorTree;

import jaba.graph.hammock.HammockGraph;
import jaba.graph.hammock.HammockNode;
import jaba.graph.hammock.HammockNodeImpl;

import jaba.graph.icfg.ICFG;

import jaba.sym.Program;
import jaba.sym.Method;
import jaba.sym.NamedReferenceType;

import jaba.main.Options;
import jaba.main.ResourceFileI;
import jaba.main.DottyOutputSpec;

import jaba.tools.graph.GDG;

/**
 * This interface defines the methods available for the client-side Factory to
 * use for requesting objects.
 *
 * @author Jay Lofstead 2003/06/25 created
 */
public interface FactoryServerSideProxy extends Remote
{
	/** returns the current build version of JABA */
	public String getVersion () throws RemoteException;

	/** return a client-side Options object */
	public Options getOptions () throws RemoteException;

	/** return a client-side Resources object */
	public ResourceFileI getResources () throws RemoteException;

	/** return a client-side Resources object */
	public ResourceFileI getResources (String file) throws RemoteException;

	/** return a client-side Program object */
	public Program getProgram (ResourceFileI res, Options opt) throws RemoteException;

	/** returns the ICFG associated with the passed in Program */
	public ICFG getICFG (Program p) throws RemoteException;

	/** returns the CallGraph associated with the passed in Program */
	public CallGraph getCallGraph (Program p) throws RemoteException;

	/** returns the ClassHierarchyGraph associated with the passed in Program */
	public ClassHierarchyGraph getClassHierarchyGraph (Program p) throws RemoteException;

	/** returns the CFG associated with the passed in Program */
	public CFG getCFG (Method m) throws RemoteException;

	/** returns the ACFG associated with the passed in Program */
	public ACFG getACFG (Method m) throws RemoteException;

	/** returns the GDG associated with the passed in Program */
	public GDG getGDG (Method m) throws RemoteException;

	/** returns the ExceptionHandlers associated with the passed in Method */
	public ExceptionHandlers getExceptionHandlers (Method m) throws RemoteException;

	/** returns the CDG associated with the passed in Graph */
	public CDG getCDG (Graph g) throws RemoteException;

	/** returns the DominatorTree associated with the passed in Graph */
	public DominatorTree getDominatorTree (Graph g) throws RemoteException;

	/** returns the DominanceFrontiers associated with the passed in Graph */
	public DominanceFrontiers getDominanceFrontiers (Graph g) throws RemoteException;

	/** returns the PostDominatorTree associated with the passed in Graph */
	public PostDominatorTree getPostDominatorTree (Graph g) throws RemoteException;

	/** returns the PostDominanceFrontiers associated with the passed in Graph */
	public PostDominanceFrontiers getPostDominanceFrontiers (Graph g) throws RemoteException;

	/** returns the HammockGraph associated with the passed in Graph */
	public HammockGraph getHammockGraph (Graph g) throws RemoteException;

	/** returns the FinallyEntryAttribute associated with the passed in StatementNode */
	public FinallyEntryAttribute getFinallyEntryAttribute (StatementNode s) throws RemoteException;

	/** returns the FinallyTransferAttribute associated with the passed in StatementNode */
	public FinallyTransferAttribute getFinallyTransferAttribute (StatementNode s) throws RemoteException;

	/** returns the NewInstanceAttribute associated with the passed in StatementNode */
	public NewInstanceAttribute getNewInstanceAttribute (StatementNode s) throws RemoteException;

	/** returns the ExceptionAttribute associated with the passed in StatementNode */
	public ExceptionAttribute getExceptionAttribute (StatementNode s) throws RemoteException;

	/** returns the ReturnSitesAttribute associated with the passed in StatementNode */
	public ReturnSitesAttribute getReturnSitesAttribute (StatementNode s) throws RemoteException;

	/** returns the ThrowStatementAttribute associated with the passed in StatementNode */
	public ThrowStatementAttribute getThrowStatementAttribute (StatementNode s) throws RemoteException;

	/** returns the MethodCallEdgeAttribute associated with the passed in Edge */
	public MethodCallEdgeAttribute getMethodCallEdgeAttribute (Edge e) throws RemoteException;

	/** returns the ThrowEdgeAttribute associated with the passed in Edge */
	public ThrowEdgeAttribute getThrowEdgeAttribute (Edge e) throws RemoteException;

	/** returns the ReturnEdgeAttribute associated with the passed in Edge */
	public ReturnEdgeAttribute getReturnEdgeAttribute (Edge e) throws RemoteException;

	/** create a new DottyOutputSpec object */
	public DottyOutputSpec newDottyOutputSpec () throws RemoteException;

	/** create a new Edge object */
	public Edge newEdge (Node source, Node sink) throws RemoteException;

	/** create a new Edge object */
	public Edge newEdge (Node source, Node sink, String label) throws RemoteException;

	/** create a new StatementNode object */
	public StatementNode newStatementNode () throws RemoteException;

	/** create a new HammockNode object */
	public HammockNode newHammockNode () throws RemoteException;

	/** get a FinallyCallAttribute from a StatementNode object */
	public FinallyCallAttribute getFinallyCallAttribute (StatementNode sn) throws RemoteException;

	/** get a FinallyContextAttribute from an Edge object  */
	public FinallyContextAttribute getFinallyContextAttribute (Edge e) throws RemoteException;

	/** get a MethodCallAttribute from a StatementNode object  */
	public MethodCallAttribute getMethodCallAttribute (StatementNode sn) throws RemoteException;
}
