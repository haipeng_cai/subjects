package jaba.tools.local;

import jaba.graph.Graph;
import jaba.graph.StatementNode;
import jaba.graph.FinallyEntryAttribute;
import jaba.graph.FinallyTransferAttribute;
import jaba.graph.FinallyContextAttribute;
import jaba.graph.FinallyCallAttribute;
import jaba.graph.MethodCallAttribute;
import jaba.graph.NewInstanceAttribute;
import jaba.graph.ExceptionAttribute;
import jaba.graph.ReturnSitesAttribute;
import jaba.graph.ThrowStatementAttribute;
import jaba.graph.ClassHierarchyGraph;
import jaba.graph.CallGraph;
import jaba.graph.Edge;
import jaba.graph.MethodCallEdgeAttribute;
import jaba.graph.ThrowEdgeAttribute;
import jaba.graph.ReturnEdgeAttribute;
import jaba.graph.Node;

import jaba.graph.acfg.ACFG;

import jaba.graph.cdg.CDG;

import jaba.graph.cfg.CFG;
import jaba.graph.cfg.ExceptionHandlers;

import jaba.graph.dom.DominanceFrontiers;
import jaba.graph.dom.DominatorTree;
import jaba.graph.dom.PostDominanceFrontiers;
import jaba.graph.dom.PostDominatorTree;

import jaba.graph.hammock.HammockGraph;
import jaba.graph.hammock.HammockNode;

import jaba.graph.icfg.ICFG;

import jaba.sym.Program;
import jaba.sym.Method;
import jaba.sym.NamedReferenceType;

import jaba.main.Options;
import jaba.main.ResourceFileI;
import jaba.main.DottyOutputSpec;

import jaba.tools.graph.GDG;

/**
 * This class provides an interface mechanism for retrieving various
 * JABA objects from either a server-side implementation or from a
 * local implementation with no change in the client code.
 *
 * @author Jay Lofstead 2003/06/23 created
 */
public class Factory
{
	/** return a client-side Program object */
	public static Program getProgram (ResourceFileI res, Options opt)
	{
		return new jaba.sym.ProgramImpl (res, opt);
	}

	/** return a client-side Options object */
	public static Options getOptions ()
	{
		return new jaba.main.OptionsImpl ();
	}

	/** return a client-side Resources object */
	public static ResourceFileI getResources ()
	{
		return new jaba.main.ResourceBundle ();
	}

	/** return a client-side Resources object */
	public static ResourceFileI getResources (String file)
	{
		return new jaba.main.ResourceBundle (file);
	}

	/** returns the current build version of JABA */
	public static String getVersion ()
	{
		return jaba.sym.ProgramImpl.getsSn ();
	}

	/** returns the ICFG associated with the passed in Program */
	public static ICFG getICFG (Program p)
	{
		return (ICFG) p.getAttributeOfType ("jaba.graph.icfg.ICFG");
	}

	/** returns the CFG associated with the passed in Program */
	public static CFG getCFG (Method m)
	{
		return (CFG) m.getAttributeOfType ("jaba.graph.cfg.CFG");
	}

	/** returns the ACFG associated with the passed in Program */
	public static ACFG getACFG (Method m)
	{
		return (ACFG) m.getAttributeOfType ("jaba.graph.acfg.ACFG");
	}

	/** returns the GDG associated with the passed in Program */
	public static GDG getGDG (Method m)
	{
		return (GDG) m.getAttributeOfType ("jaba.tools.graph.GDG");
	}

	/** returns the ExceptionHandlers associated with the passed in Method */
	public static ExceptionHandlers getExceptionHandlers (Method m)
	{
		return (ExceptionHandlers) m.getAttributeOfType ("jaba.graph.cfg.ExceptionHandlers");
	}

	/** returns the CDG associated with the passed in Program */
	public static CDG getCDG (Graph g)
	{
		return (CDG) g.getAttributeOfType ("jaba.graph.cdg.CDG");
	}

	/** returns the CallGraph associated with the passed in Program */
	public static CallGraph getCallGraph (Program p)
	{
		return (CallGraph) p.getAttributeOfType ("jaba.graph.CallGraph");
	}

	/** returns the ClassHierarchyGraph associated with the passed in Program */
	public static ClassHierarchyGraph getClassHierarchyGraph (Program p)
	{
		return (ClassHierarchyGraph) p.getAttributeOfType ("jaba.graph.ClassHierarchyGraph");
	}


	/** returns the DominatorTree associated with the passed in Graph */
	public static DominatorTree getDominatorTree (Graph g)
	{
		return (DominatorTree) g.getAttributeOfType ("jaba.graph.dom.DominatorTree");
	}

	/** returns the DominanceFrontiers associated with the passed in Graph */
	public static DominanceFrontiers getDominanceFrontiers (Graph g)
	{
		return (DominanceFrontiers) g.getAttributeOfType ("jaba.graph.dom.DominanceFrontiers");
	}

	/** returns the PostDominatorTree associated with the passed in Graph */
	public static PostDominatorTree getPostDominatorTree (Graph g)
	{
		return (PostDominatorTree) g.getAttributeOfType ("jaba.graph.dom.PostDominatorTree");
	}

	/** returns the PostDominanceFrontiers associated with the passed in Graph */
	public static PostDominanceFrontiers getPostDominanceFrontiers (Graph g)
	{
		return (PostDominanceFrontiers) g.getAttributeOfType ("jaba.graph.dom.PostDominanceFrontiers");
	}

	/** returns the HammockGraph associated with the passed in Graph */
	public static HammockGraph getHammockGraph (Graph g)
	{
		return (HammockGraph) g.getAttributeOfType ("jaba.graph.hammock.HammockGraph");
	}

	/** returns the FinallyEntryAttribute associated with the passed in StatementNode */
	public static FinallyEntryAttribute getFinallyEntryAttribute (StatementNode s)
	{
		return (FinallyEntryAttribute) s.getAttributeOfType ("jaba.graph.FinallyEntryAttribute");
	}

	/** returns the FinallyTransferAttribute associated with the passed in StatementNode */
	public static FinallyTransferAttribute getFinallyTransferAttribute (StatementNode s)
	{
		return (FinallyTransferAttribute) s.getAttributeOfType ("jaba.graph.FinallyTransferAttribute");
	}

	/** returns the NewInstanceAttribute associated with the passed in StatementNode */
	public static NewInstanceAttribute getNewInstanceAttribute (StatementNode s)
	{
		return (NewInstanceAttribute) s.getAttributeOfType ("jaba.graph.NewInstanceAttribute");
	}

	/** returns the ExceptionAttribute associated with the passed in StatementNode */
	public static ExceptionAttribute getExceptionAttribute (StatementNode s)
	{
		return (ExceptionAttribute) s.getAttributeOfType ("jaba.graph.ExceptionAttribute");
	}

	/** returns the ReturnSitesAttribute associated with the passed in StatementNode */
	public static ReturnSitesAttribute getReturnSitesAttribute (StatementNode s)
	{
		return (ReturnSitesAttribute) s.getAttributeOfType ("jaba.graph.ReturnSitesAttribute");
	}

	/** returns the ThrowStatementAttribute associated with the passed in StatementNode */
	public static ThrowStatementAttribute getThrowStatementAttribute (StatementNode s)
	{
		return (ThrowStatementAttribute) s.getAttributeOfType ("jaba.graph.ThrowStatementAttribute");
	}

	/** returns the MethodCallEdgeAttribute associated with the passed in Edge */
	public static MethodCallEdgeAttribute getMethodCallEdgeAttribute (Edge e)
	{
		return (MethodCallEdgeAttribute) e.getAttributeOfType ("jaba.graph.MethodCallEdgeAttribute");
	}

	/** returns the ThrowEdgeAttribute associated with the passed in Edge */
	public static ThrowEdgeAttribute getThrowEdgeAttribute (Edge e)
	{
		return (ThrowEdgeAttribute) e.getAttributeOfType ("jaba.graph.ThrowEdgeAttribute");
	}

	/** returns the ReturnEdgeAttribute associated with the passed in Edge */
	public static ReturnEdgeAttribute getReturnEdgeAttribute (Edge e)
	{
		return (ReturnEdgeAttribute) e.getAttributeOfType ("jaba.graph.ReturnEdgeAttribute");
	}

	/** create a new DottyOutputSpec object */
	public static DottyOutputSpec newDottyOutputSpec ()
	{
		return new DottyOutputSpec ();
	}

	/** create a new Edge object */
	public static Edge newEdge (Node source, Node sink)
	{
		return new jaba.graph.EdgeImpl (source, sink);
	}

	/** create a new Edge object */
	public static Edge newEdge (Node source, Node sink, String label)
	{
		return new jaba.graph.EdgeImpl (source, sink, label);
	}

	/** create a new StatementNode object */
	public static StatementNode newStatementNode ()
	{
		return new jaba.graph.StatementNodeImpl ();
	}

	/** create a new HammockNode object */
	public static HammockNode newHammockNode ()
	{
		return new jaba.graph.hammock.HammockNodeImpl ();
	}

	/** get a FinallyCallAttribute from a StatementNode object */
	public static FinallyCallAttribute getFinallyCallAttribute (StatementNode sn)
	{
		return (FinallyCallAttribute) sn.getAttributeOfType ("jaba.graph.FinallyCallAttribute");
	}

	/** get a FinallyContextAttribute from an Edge object  */
	public static FinallyContextAttribute getFinallyContextAttribute (Edge e)
	{
		return (FinallyContextAttribute) e.getAttributeOfType ("jaba.graph.FinallyContextAttribute");
	}

	/** get a MethodCallAttribute from a StatementNode object  */
	public static MethodCallAttribute getMethodCallAttribute (StatementNode sn)
	{
		return (MethodCallAttribute) sn.getAttributeOfType ("jaba.graph.MethodCallAttribute");
	}
}
