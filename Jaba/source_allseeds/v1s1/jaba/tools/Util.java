package jaba.tools;

/**
 * Class containing static utility methods used throughout JABA
 * 
 * @author Jay Lofstead 2003/06/16 created
 */
public class Util
{
	/** convert the given string to a valid XML format */
	public static String toXMLValid (String str)
	{
		char [] c = str.toCharArray ();
		StringBuffer n = new StringBuffer (c.length);

		for (int i = 0; i < c.length; i++)
		{
			if (c [i] == '"')
			{
				n.append ("&quot;");
			}
			else
			{
				if (c [i] == '&')
				{
					n.append ("&amp;");
				}
				else
				{
					if (c [i] == '<')
					{
						n.append ("&lt;");
					}
					else
					{
						if (c [i] == '>')
						{
							n.append ("&gt;");
						}
						else
						{
							if (c [i] == '\'')
							{
								n.append ("&apos;");
							}
							else
							{
								n.append (c [i]);
							}
						}
					}
				}
			}
		}

	        return n.toString ();
	}
}