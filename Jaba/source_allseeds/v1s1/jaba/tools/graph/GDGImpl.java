package jaba.tools.graph;

import jaba.sym.Method;
import jaba.sym.DemandDrivenAttribute;
import jaba.sym.MethodAttribute;

import jaba.main.Options;

import jaba.graph.Graph;
import jaba.graph.GraphImpl;
import jaba.graph.GraphAttribute;
import jaba.graph.MethodCallEdgeAttribute;
import jaba.graph.MethodCallEdgeAttributeImpl;
import jaba.graph.StatementNode;
import jaba.graph.StatementNodeImpl;
import jaba.graph.EdgeImpl;
import jaba.graph.Edge;
import jaba.graph.Node;
import jaba.graph.MethodCallAttribute;
import jaba.graph.FinallyCallAttribute;

import jaba.graph.cfg.MalformedTryStatementException;
import jaba.graph.cfg.CFG;
import jaba.graph.cfg.TryStatement;
import jaba.graph.cfg.TryBlock;
import jaba.graph.cfg.CatchBlock;
import jaba.graph.cfg.FinallyBlock;
import jaba.graph.cfg.SynchronizedTryBlock;
import jaba.graph.cfg.ExceptionHandlers;

import jaba.tools.local.Factory;

import java.util.Collection;
import java.util.Hashtable;
import java.util.HashSet;
import java.util.Vector;
import java.util.Iterator;
import java.util.Enumeration;
import java.util.Stack;
import java.util.LinkedList;
import java.util.List;

/**
 * Creates a Differencing Interclass Graph for Java Programs.
 * The Java Interclass Graph uses this class to create individual
 * DIGs and then merges them together with each other as well as
 * External Code Graph (ECG).
 * @author Manas Tungare <manas@cc.gatech.edu>, July 8, 2002.
 *
 * There are differences and similarities between the DIG that
 * for building Java Interclass Graph (JIG) and the DIG for
 * Differencing (javadiff). This class is implemented to be the general
 * differencing graph (GDG) but it is actually the same as the original
 * DIG for building JIG. The DIG for differencing will use this graph
 * as the base and modifies it.
 *
 * By asking for a GDG, the Options associated with the Program must have
 * the createLVT set to true or this will not work properly.
 * 
 * @author Taweesup Apiwattanapong <term@cc.gatech.edu>, April 4, 2003. 
 * @author Jay Lofstead 2003/06/05 changed to use enumerations and added dummy JavaDoc comments
 * @author Jay Lofstead 2003/06/05 added toString
 * @author Jay Lofstead 2003/06/05 added implementing MethodAttribute, DemandDrivenAttribute to allow this to be created
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/12 changed createGDG to not use temporary arrays and instead iteration for better efficiency.
 * @author Jay Lofstead 2003/06/16 added toStringDetailed () and toStringReduced ()
 * @author Term 2003/06/20 code cleanup and fix getFirstNodeWithByteCodeOffset(...),
 *                         getLastNodeWithByteCodeOffset(...)
 * @author Jay Lofstead 2003/06/25 removed option setting from constructor.  Has no effect on code.  Constructor now empty and removed.
 * @author Jay Lofstead 2003/07/17 removed legacy load ()
 * @author Jay Lofstead 2003/07/17 changed to use Factory to get attributes.
 */
public class GDGImpl extends GraphImpl implements GDG, GraphAttribute, MethodAttribute, DemandDrivenAttribute
{
    /** The method that this DIG is created for. */
    private Method method;

    /** ??? */
    private Hashtable entryNodesMap = new Hashtable ();

    /** ??? */
    private Hashtable nfcNodesMap = new Hashtable ();

    /** ??? */
    private Hashtable nfcReturnNodesMap = new Hashtable ();

    /** ??? */
    private Hashtable finallyEntryNodesMap = new Hashtable ();

    /** ??? */
    private Hashtable finallyExitNodesMap = new Hashtable ();

    /** ??? */
    private Vector startNodes = new Vector ();

	/** method used to obtain the GDG by Graph */
    public static GraphAttribute load (Method m) throws IllegalArgumentException
    {
	CFG cfg = Factory.getCFG (m);
		if (cfg == null) {
		    return null;
		}

		GraphAttribute[] cfgAttributes = cfg.getAttributes();
		for(int i = 0; i < cfgAttributes.length; i++) {
		    if (cfgAttributes[i] instanceof GDG) {
			return (GDG)cfgAttributes[i];
		    }
		}

		GDGImpl gdg = new GDGImpl ();
		try {
		    gdg.createGDG(m);
		}
		catch (Exception e) {
		    e.printStackTrace();
		    System.exit(1);
		}
		cfg.addAttribute(gdg);
		return gdg;
    }

    /** ??? */
    protected void createGDG (Method m) throws MalformedTryStatementException
    {
	method = m;
	Factory.getCFG (method); // Dummy call so that Method returns correct information.

	// Process all finally methods before inlining them
	Collection allMethods = method.getTransitiveFinallyMethodsCollection ();

	// Add the current method as the last method for processing
	//allMethods[allMethods.length - 1] = method;
	allMethods.add (method);

	// Begin loop for processing
	for (Iterator i = allMethods.iterator (); i.hasNext ();) //int i = 0; i < allMethods.length; i++) {
	{
		Method currentMethod = (Method) i.next ();
		// create the CFG for this method
		CFG currentCFG = Factory.getCFG (currentMethod);

	    //TA: All EFC nodes will be removed and the edges will be redirected to the NFC node
	    // because it is not necessary to distinguish normal finally call from exceptional one.
	    Vector edgesToEfc = new Vector();

	    Edge [] edges = currentCFG.getEdges();
	    
	    for (int h = 0; h < edges.length; h++) {
		//T. Apiwattanapong 04/01 : Fix creating DIG changes CFGs. Another alternative is to
		//copy each edge in the following loop but that is incorrect because. There are getOutEdges()
		//method calls that needs edges not yet added to be in.
		addEdge ((jaba.graph.Edge) edges[h]);
	    }

	    for (int j = 0; j < edges.length; j++) {
		StatementNode source = (StatementNode) edges[j].getSource();
		StatementNode sink = (StatementNode) edges[j].getSink();

		// If node is within finally method, and its containing method is the finally method,
		// set its containing method to the real method.
		// Use this approach to avoid inlining Entry Nodes, etc.
		// TA: the name of finally method is <real method name>.finally_<finally block start bytecode offset>
		// Therefore, if method.getTransitiveFinallyMethods() returns only methods which have the names
		// in the format above then these two 'if' expressions will always be true.
		if ( source.getContainingMethod().getSignature().indexOf( method.getSignature() ) > -1) {
		    source.setContainingMethod( method );
		}

		if ( sink.getContainingMethod().getSignature().indexOf( method.getSignature() ) > -1) {
		    sink.setContainingMethod( method );
		}
				
		if ( (source.getType() == StatementNode.STATIC_METHOD_CALL_NODE ||
		    source.getType() == StatementNode.VIRTUAL_METHOD_CALL_NODE)
		    //TA: Predicate return node cannot be a sink of any call nodes.
		    // && (sink.getType() == StatementNode.PREDICATE_RETURN_NODE ||
		    && sink.getType()   == StatementNode.RETURN_NODE ) {

		    removeEdge((jaba.graph.Edge) edges[j] );

		    // DIG+JIG : Add Call Edges from Call Node to Entry Nodes for each of methods.length
		    // methods that can be mapped to this call site.
					
		    Vector callableMethods = null;
		    MethodCallAttribute methodCallAttr = Factory.getMethodCallAttribute (source);
		    if (methodCallAttr != null) {
			callableMethods = methodCallAttr.getMethodsVector ();
		    }

		    // DIG : Add a Call Edge for each Method that can be mapped to this call site.
		    for(Iterator k = callableMethods.iterator (); k.hasNext ();)
		    {
			Method m1 = (Method) k.next ();
			// Put an attribute on the callEdge that identifies the object types
			// that enables the traversal of that edge.
			MethodCallEdgeAttribute callEdgeAttr = new MethodCallEdgeAttributeImpl ();
	
			if (m1.getName().equals("<init>") ||
			    m1.getName().equals("<clinit>") ||
			    source.getType() == StatementNode.STATIC_METHOD_CALL_NODE) {
			    callEdgeAttr.addTraversalType( (jaba.sym.Class) m1.getContainingType() );
			}
			else {
			    populateMethodCallEdgeAttribute( callEdgeAttr, m1);
			}

			// A Call Edge for each traversal type, labeled with the 
			// traversal type
			for (int p = 0; p < callEdgeAttr.getTraversalTypes().length; p++) {
			    jaba.sym.Class traversalType = callEdgeAttr.getTraversalTypes()[p];
			    Edge callEdge = new EdgeImpl (source, getEntryNodeOfMethod(m1), traversalType.getName() );

			    MethodCallEdgeAttribute individualCallEdgeAttr = new MethodCallEdgeAttributeImpl ();
			    individualCallEdgeAttr.addTraversalType(traversalType);
			    callEdge.addAttribute(individualCallEdgeAttr);
							
			    addEdge(callEdge);
			}

		    }

		    // Path Edge for the JIG
		    if (callableMethods.size () > 0) {
			Edge pathEdge = new EdgeImpl (source, sink, "no exception");
			addEdge(pathEdge);
		    }

		}
		else if( sink.getType() == StatementNode.NORMAL_FINALLY_CALL_NODE) {
		    // TA: the following is an old comment.
		    // This is an Edge to the NFC node.
		    // So see if it already exists in our Hashtable,
		    // else save it there.

		    FinallyCallAttribute finCallAttr = Factory.getFinallyCallAttribute (sink);
		    StatementNode commonNFCNode = (StatementNode) nfcNodesMap.get( finCallAttr.getName() );

		    if (commonNFCNode != null) {
		    	// TA: if common NFCNode equals to sink then nothing needs to be done.
		    	if (!commonNFCNode.equals(sink)) {
		    	    // TA: because commonNFCNode always equal to sink, adding and removing these edges yield
		    	    // the same result as doing nothing. Note that nfcNodesToRemove is of no use because there is
		    	    // no multiple copies of finally blocks.
		    	    // TA: 2003-04-05 This is not true. each cache block has its own NFC node and return
			    // but instead of putting nodes to the remove list, it is removed here
			    Edge edgeToCommonNFCNode = new EdgeImpl (source, commonNFCNode, edges[j].getLabel());
			    addEdge(edgeToCommonNFCNode);
			    removeEdge ((jaba.graph.Edge) edges[j] );
			    // TA: I'm not sure when we should stop deleting nodes and edges. In the old code
			    // it uses the StatementNode.STATEMENT_NODE as the last node to deleted but
			    // I will use the node 'n' with more than one incoming edges as the first node not be
			    // be deleted because if there are two or more NFC nodes then 'n' is the first node
			    // which both paths join. (Note that there should be no branching in between)
			    
			    StatementNode afterDupNFCnode = sink;
			    Collection nodesBeRemoved = new Vector();
			    Edge[] outEdges = getOutEdges(afterDupNFCnode);
			    // stop when there is not outedges or (node is not NFC and has 2 or more inedges)
			    while ((outEdges.length > 0) && 
			    	((afterDupNFCnode.getType() == StatementNode.NORMAL_FINALLY_CALL_NODE) || 
			    	  (getInEdges(afterDupNFCnode).length  < 1))) {
				nodesBeRemoved.add(afterDupNFCnode);
			    	afterDupNFCnode = (StatementNode) outEdges[0].getSink();
			    	removeEdge ((jaba.graph.Edge) outEdges[0]);
				outEdges = getOutEdges(afterDupNFCnode);
			    }
			    
			    for (Iterator nbrItr = nodesBeRemoved.iterator(); nbrItr.hasNext(); ) {
				Node beRemovedNode = (Node) nbrItr.next();
				if ((getInEdges(beRemovedNode).length == 0) &&
				     (getOutEdges(beRemovedNode).length == 0)) {
				    removeNode(beRemovedNode);
				}
			    }
		    	}
		    }
		    else {
			nfcNodesMap.put(finCallAttr.getName(), sink);
			StatementNode corrReturnNode = (StatementNode) getOutEdges(sink)[0].getSink();
			nfcReturnNodesMap.put(finCallAttr.getName(), corrReturnNode);
			// TA: There is no need to make a copy because we will use only NFC.
			// Also create copy of NFC (called EFC)
			
			//TA: connect the nodes before EFC to the NFC.
			for (Iterator eteItr = edgesToEfc.iterator(); eteItr.hasNext();) {
				Edge toEfcEdge = (Edge) eteItr.next();
				addEdge( new EdgeImpl ((jaba.graph.Node) toEfcEdge.getSource(), sink, toEfcEdge.getLabel()));
			}
		    }
		}
		else if ( sink.getType() == StatementNode.EXCEPTIONAL_FINALLY_CALL_NODE ) {
		    // TA: Because the JIG does not propagate exceptions to the caller method. There is
		    // no need to distinguish between a normal finally call and exceptional finally calls.
		    FinallyCallAttribute finCallAttr = Factory.getFinallyCallAttribute (sink);
		    StatementNode commonNFCNode = (StatementNode) nfcNodesMap.get( finCallAttr.getName() );
		    if (commonNFCNode == null) {
		    	// if the NFC node has not been processed, add it to the collection.
		    	//  When the NFC node is found, these edges will be redirected
		    	edgesToEfc.add(edges[j]);
		    } else {
		    	// if the NFC node is processed, do the redirection here.
			addEdge( new EdgeImpl (source, commonNFCNode, edges[j].getLabel()));
		    }
		    // remove all edges and nodes from EFC to Exceptional Exit.
		    // Remember not to ask for the sink node
		    // of edges in edgesToEfc. It is deleted here when no incoming edges is found.
		    Stack edgesFromEfc = new Stack();
		    edgesFromEfc.add(edges[j]);
		    // TA: Because nodes cannot be removed before all edges connected to them
		    // are removed so we remove them after all edges are removed.
		    Collection nodesToBeRemoved = new Vector();
		    while (!edgesFromEfc.isEmpty()) {
		    	Edge fromEfcEdge = (Edge) edgesFromEfc.pop();
		    	StatementNode afterEfcNode = (StatementNode) fromEfcEdge.getSink();
		    	removeEdge(fromEfcEdge);
		    	Edge[] outEdges = getOutEdges(afterEfcNode);
		    	for (int k = 0; k < outEdges.length; k++) {
			     edgesFromEfc.push(outEdges[k]); 
			}
			if (getInEdges(afterEfcNode).length == 0) {
			    nodesToBeRemoved.add(afterEfcNode);
			}
		    }
		    for (Iterator ntbrItr = nodesToBeRemoved.iterator();ntbrItr.hasNext();) {
				Node removedNode = (Node) ntbrItr.next();
				removeNode(removedNode);
			}
		}
		else if (sink.getType() == StatementNode.EXCEPTIONAL_EXIT_NODE) {
		    // TA: It is possible for some throw statement to connect to exceptional exit
		    // we will redirect those edges to normal exit because the exc. exit will be removed.
		    Node[] exitNodes = currentCFG.getExitNodes();
		    int idx = -1;
		    for (int k = 0; k < exitNodes.length; k++) {
			if (((StatementNode) exitNodes[k]).getType() == StatementNode.EXIT_NODE ) {
			   idx = k;
			}
		    }
		    if (idx >= 0) {
		   	addEdge(new EdgeImpl (source, (jaba.graph.Node) exitNodes[idx], edges[j].getLabel())); 
		    } else {
		    	throw new RuntimeException("No exit node in CFG");
		    }
		   removeEdge ((jaba.graph.Edge) edges[j]);
		   if (getInEdges(sink).length == 0) {
		   	removeNode(sink);
		   }
		}
		else {
		    // It's a regular node; do nothing.
		    if(source.getType() == StatementNode.ENTRY_NODE) {
				startNodes.add( source );
		    }
		}
	    }

	    // Add Exception-related edges to the graph.
	    StatementNode tryNode = null;
	    // StatementNode firstNodeAfterFinallyBlock;
	    ExceptionHandlers excpHandlers = Factory.getExceptionHandlers (currentMethod);
	    if (excpHandlers != null) { // Non-Abstract Method

		// getTryStatements() will return only non-synchronized try statements.
		TryStatement[] tryStatements = excpHandlers.getTryStatements();
		for (int j = 0; j < tryStatements.length; j++) {
		    // Add/Remove Edges for the Try Block
		    TryBlock tryBlock = tryStatements[j].getTryBlock();
		    StatementNode tryBlockStartNode = getFirstNodeWithByteCodeOffset( currentCFG, tryBlock.getStartByteCodeOffset() );
		    if (tryBlockStartNode != null) {
			// Add a TRY_NODE just before this try block.
			tryNode = new StatementNodeImpl ();
			tryNode.setType( StatementNode.TRY_NODE );
			tryNode.setContainingMethod( currentMethod );
			tryNode.setByteCodeOffset( tryBlockStartNode.getByteCodeOffset() );
			tryNode.setSourceLineNumber( tryBlockStartNode.getSourceLineNumber() );

			Edge[] inEdgesOfTryBlock = getInEdges( tryBlockStartNode );
			for (int k = 0; k < inEdgesOfTryBlock.length; k++) {
			    Edge toTryNode = new EdgeImpl ((jaba.graph.Node) inEdgesOfTryBlock[k].getSource(), tryNode, inEdgesOfTryBlock[k].getLabel() );
			    addEdge( toTryNode );
			    removeEdge((jaba.graph.Edge) inEdgesOfTryBlock[k] );
			}

			Edge tryNodeToStartOfTryBlock = new EdgeImpl ( tryNode, tryBlockStartNode, "");
			addEdge( tryNodeToStartOfTryBlock );
		    }
		    else {
			throw new MalformedTryStatementException("Unable to find start of try block within CFG "
								 + getMethodEdgeLabel( currentMethod )
								 + " at offset " + tryBlock.getStartByteCodeOffset() );
		    }

		    // Add/Remove Edges for the Catch Block(s)
		    CatchBlock[] catchBlocks = tryStatements[j].getCatchBlocks();
		    StatementNode prevNode = null;
		    StatementNode nextNode = null;
		    if (catchBlocks != null) {
			prevNode = tryNode; // First Edge begins from TRY_NODE
			HashSet alreadyCaughtExceptions = new HashSet();
			for (int k = 0; k < catchBlocks.length; k++) {
			    StatementNode catchNode = getFirstNodeWithByteCodeOffset( currentCFG, catchBlocks[k].getByteCodeOffset(), StatementNode.CATCH_NODE );
			    if (catchNode == null) {
				throw new MalformedTryStatementException("Unable to find start of catch block within CFG "
									 + getMethodEdgeLabel( currentMethod )
									 + " at offset " + tryBlock.getStartByteCodeOffset() );
			    }
			    else {
				// Collect Exception Information
				jaba.sym.Class caughtException = catchBlocks[k].getExceptionType();
				Collection allCaughtExceptions = caughtException.getAllSubClassesCollection ();

				// Create Exception Edge Attribute
				CaughtEdgeAttribute exceptionInfo = new CaughtEdgeAttributeImpl ();

				// Add the exception directly caught.
				if (! alreadyCaughtExceptions.contains( caughtException ) ) {
				    exceptionInfo.addException( caughtException );
				    alreadyCaughtExceptions.add( caughtException );
				}

				// Add the exception transitively caught.
				for (Iterator n = allCaughtExceptions.iterator (); n.hasNext ();)
				{
					jaba.sym.Class c = (jaba.sym.Class) n.next ();
				    if (! alreadyCaughtExceptions.contains (c))
				    {
					exceptionInfo.addException (c);
					alreadyCaughtExceptions.add (c);
				    }
				}

				// Create Exception Propagation Node
				StatementNode excPropagationNode = new StatementNodeImpl ();
				excPropagationNode.setType( StatementNode.STATEMENT_NODE );
				excPropagationNode.setContainingMethod( currentMethod );
				excPropagationNode.setByteCodeOffset( catchNode.getByteCodeOffset() );
				excPropagationNode.setSourceLineNumber( catchNode.getSourceLineNumber() );

				Edge caughtEdge = new EdgeImpl (excPropagationNode, catchNode, "caught");
				caughtEdge.addAttribute( exceptionInfo );
				addEdge( caughtEdge );

				nextNode = excPropagationNode;

				Edge pathEdge = new EdgeImpl (prevNode, nextNode, "exception");
				addEdge( pathEdge );

				prevNode = nextNode;
			    }
			}
		    }

		    // Add/Remove Edges for Finally Block
		    FinallyBlock finallyBlock = tryStatements[j].getFinallyBlock();
		    if (finallyBlock != null) {

			// TA: all exceptional exit is merged to nfc, so use nfc
			StatementNode normalFinallyNode = (StatementNode) nfcNodesMap.get( finallyBlock.getName() );
			if (normalFinallyNode != null) {
			    Edge lastCatchToFinallyNode = new EdgeImpl ( prevNode, normalFinallyNode, "exception");
			    addEdge(lastCatchToFinallyNode);
			}
		    }
		}

				
		// Now get the synchronized try statements.
		TryStatement[] syncTryStmts = excpHandlers.getSynchronizedTryStatements();
		for (int j = 0; j < syncTryStmts.length; j++) {
		    TryBlock tryBlock = syncTryStmts[j].getTryBlock();
		    if (tryBlock instanceof SynchronizedTryBlock) {
			SynchronizedTryBlock syncTryBlock = (SynchronizedTryBlock) tryBlock;
			int startOffset = syncTryBlock.getStartByteCodeOffset();
			int endOffset = syncTryBlock.getEndByteCodeOffset();

			StatementNode syncBlockStart = getFirstNodeWithByteCodeOffset(currentCFG, startOffset);
			// T. Apiwattanapong - JDK 1.4 adds an additional tryBlock entry in the exceptionHandler table
			// in case that the original code which can throw exception is inside a synchronize block.
			// This entry covers the "synthetic" part which is not handled by CFG so we cannot find
			// the start and end node for that block.
			if (syncBlockStart == null) { continue; }

			StatementNode syncBlockEnd = getLastNodeWithByteCodeOffset(currentCFG, endOffset);

			StatementNode syncBlockStartMarker = new StatementNodeImpl ();
			syncBlockStartMarker.setType(StatementNode.SYNCHRONIZED_START_NODE);
			syncBlockStartMarker.setContainingMethod( method );
			syncBlockStartMarker.setByteCodeOffset( syncBlockStart.getByteCodeOffset() );
			syncBlockStartMarker.setSourceLineNumber( syncBlockStart.getSourceLineNumber() );

			StatementNode syncBlockEndMarker = new StatementNodeImpl ();
			syncBlockEndMarker.setType(StatementNode.SYNCHRONIZED_END_NODE);
			syncBlockEndMarker.setContainingMethod( method );
			syncBlockEndMarker.setByteCodeOffset( syncBlockEnd.getByteCodeOffset() );
			syncBlockEndMarker.setSourceLineNumber( syncBlockEnd.getSourceLineNumber() );

			// Insert SYNCHRONIZATION_START_NODE & SYNCHRONIZATION_END_NODE
			Edge[] inEdgesOfSyncStart = getInEdges( syncBlockStart );
			for (int k = 0; k < inEdgesOfSyncStart.length; k++) {
			    addEdge( new EdgeImpl ((jaba.graph.Node) inEdgesOfSyncStart[k].getSource(), syncBlockStartMarker) );
			    removeEdge ((jaba.graph.Edge) inEdgesOfSyncStart[k]);
			}
			addEdge(new EdgeImpl ( syncBlockStartMarker, syncBlockStart ));

			Edge [] outEdgesOfSyncEnd = getOutEdges( syncBlockEnd );
			for (int k = 0; k < outEdgesOfSyncEnd.length; k++) {
			    assert(outEdgesOfSyncEnd[k].getSink() instanceof StatementNode);
			    StatementNode sink = (StatementNode)outEdgesOfSyncEnd[k].getSink();
			    addEdge( new EdgeImpl (syncBlockEndMarker, sink,
					      String.valueOf(sink.getByteCodeOffset())) );
			    removeEdge ((jaba.graph.Edge) outEdgesOfSyncEnd[k]);
			}
			addEdge(new EdgeImpl ( syncBlockEnd, syncBlockEndMarker ));
		    }
		}
	    }

	    //Remove multiple copies of finally blocks (i.e., one after each catch block)
	    // TA: There are no multiple copies of finally blocks in CFG. This code is useless.

	    // Make an exceptional copy of the finally block.
	    if(i.hasNext ()) { // It's not the main method

		// Mapping between Method Name and Entry Node
		// To be used later for connecting EFC Node to Method Entry
		finallyEntryNodesMap.put ( currentMethod.getName(), getEntryNodeOfMethod( currentMethod ) );

		// TA: No need to duplicate nodes
		// Duplicate the Nodes and save mapping
		for (int j = 0; j < currentCFG.getNodes().length; j++) {
		    StatementNode nodeToCopy = (StatementNode) currentCFG.getNodes()[j];
		    if (nodeToCopy.getType() != StatementNode.EXIT_NODE) {
		    }
		    else {
			StatementNode nodeBeforeExitNode = (StatementNode) getInEdges( nodeToCopy )[0].getSource();
			finallyExitNodesMap.put( currentMethod.getName(), nodeBeforeExitNode );
			
		    }
		}
	    }

	    // TA: No EFC nodes so connect only NFC node
	    // Connect EFC Nodes to corresponding Exceptional Finally Entry Nodes.
	    for (Enumeration e = nfcNodesMap.keys () ; e.hasMoreElements() ;) {
		String finallyMethodName = (String) e.nextElement();
		StatementNode nfcNode = (StatementNode) nfcNodesMap.get( finallyMethodName );
		StatementNode actualEntryNode = (StatementNode) finallyEntryNodesMap.get( finallyMethodName );
		StatementNode finallyExitNode = (StatementNode) finallyExitNodesMap.get( finallyMethodName );
		StatementNode nfcReturnNode = (StatementNode) nfcReturnNodesMap.get ( finallyMethodName );

		//Also delete the edge from NFCNode directly to its return node.
		removeEdge ((jaba.graph.Edge) getOutEdges(nfcNode)[0]);
		//Delete the edge from node before exit in finally method and that exit node.
		Edge [] outEdges = getOutEdges(finallyExitNode);
		for (int j = 0; j < outEdges.length; j++) {
		    Node exitNode = (jaba.graph.Node) outEdges[j].getSink();
		    removeEdge ((jaba.graph.Edge) outEdges[j]);
		    removeNode(exitNode);
		}

		Edge nfcNodeToFinallyEntry = new EdgeImpl (nfcNode, actualEntryNode);
		addEdge(nfcNodeToFinallyEntry);
		
		Edge finallyExitToAfterFinally = new EdgeImpl (finallyExitNode, nfcReturnNode);
		addEdge(finallyExitToAfterFinally);
	    }
	}
    }

    /**
     * Given a MethodCallEdgeAttribute and the called Method, the
     * MethodCallEdgeAttribute is populated with all of the Class types that
     * may cause traversal of that edge.
     * This method is taken from the ICFG.
     * @param callAttr  MethodCallEdgeAttribute to populate.
     * @param method  Method being called.
     */
    private void populateMethodCallEdgeAttribute( MethodCallEdgeAttribute callAttr, Method method ) {
	String sig = method.getSignature();

	// If the containing type is an interface, we skip it
	if( method.getContainingType() instanceof jaba.sym.Interface ) {
	    return;
	}

	callAttr.addTraversalType( (jaba.sym.Class)method.getContainingType() );
	Collection subclasses = ((jaba.sym.Class)method.getContainingType()).getDirectSubClassesCollection ();
	LinkedList worklist = new LinkedList();
	for (Iterator i = subclasses.iterator (); i.hasNext ();)
	{
		jaba.sym.Class c = (jaba.sym.Class) i.next ();
	    if (!c.isSummarized ())
	    {
		worklist.addLast (c);
	    }
	}

	while ( !worklist.isEmpty() ) {
	    jaba.sym.Class currClass = (jaba.sym.Class)worklist.removeFirst();
	    if ( currClass.getInheritedMethod( sig ) == method ) {
		subclasses = currClass.getDirectSubClassesCollection ();
		for (Iterator i = subclasses.iterator (); i.hasNext ();)
		{
			jaba.sym.Class c = (jaba.sym.Class) i.next ();
		    if (!c.isSummarized ())
		    {
			worklist.addLast (c);
		    }
		}
		callAttr.addTraversalType( currClass );
	    }
	}
    }

    /** ??? */
    private StatementNode getEntryNodeOfMethod(Method m) {
	if (! m.getContainingType().isSummarized()) { // Means this is an Internal Method
	    Graph cfg = Factory.getCFG (m);
	    if (cfg == null) {
		return createEntryNodeOfMethod(m);
	    }
	    else {
		if (cfg.getEntryNodes().length == 0) {
		    return createEntryNodeOfMethod(m);
		}
		else {
		    int entryNodeCounter = -1;
		    Node[] entryNodesToUse = cfg.getEntryNodes();
		    
		    for (int i = 0; i < entryNodesToUse.length; i++) {
			if (((StatementNode) entryNodesToUse[i]).getType() == StatementNode.ENTRY_NODE ) {
			    entryNodeCounter = i;
			    break;
			}
		    }

		    if (entryNodeCounter == -1) {
			return createEntryNodeOfMethod(m);
		    }
		    else {
			return (StatementNode) entryNodesToUse[entryNodeCounter];
		    }
		}
	    }
	}
	else {
	    return createEntryNodeOfMethod(m);
	}
    }

    /** ??? */
    private StatementNode createEntryNodeOfMethod(Method m) {
	if (entryNodesMap == null) {
	    entryNodesMap = new Hashtable();
	}

	StatementNode entryNode = (StatementNode) entryNodesMap.get(m);
	if (entryNode != null) {
	    return entryNode;
	}
	else {
	    StatementNode newEntryNode = new StatementNodeImpl ();
	    newEntryNode.setType(StatementNode.ENTRY_NODE);
	    newEntryNode.setContainingMethod(m);
	    newEntryNode.setByteCodeOffset(-1);

	    entryNodesMap.put(m, newEntryNode);
	    return newEntryNode;
	}
    }

    /** ??? */
    private StatementNode getFirstNodeWithByteCodeOffset(CFG cfg, int byteCodeOffset, int type) {
	StatementNode bestSofar = null;
	Node[] nodesDFO = cfg.getNodesInDepthFirstOrder();
	// traverse all nodes in depth first order
	for (int i = 0; i < nodesDFO.length; i++) {
	    if (nodesDFO[i] instanceof StatementNode) {
		StatementNode node = (StatementNode) nodesDFO[i];
		if ((type == -1) || (type == node.getType())) {
		    if (node.getByteCodeOffset() == byteCodeOffset) {
			// node with exact bytecode offset and type found; return it
			return node;
		    } else if (node.getByteCodeOffset() > byteCodeOffset) {
			// keep the best one found so far
			if ((bestSofar == null) ||
			    // in case it's equal, use the first one found.
			    (bestSofar.getByteCodeOffset() > node.getByteCodeOffset())) {
			    bestSofar = node;
			}
		    }
		}
	    }
	}

	// If you reach here, you haven't found a node yet.
	// So settle for the next node with BCO as given.
	System.err.println("WARNING: cannot find the exact node with bytecode offset " + byteCodeOffset);
	System.err.println("     for the start of synchronized or try block. Perhaps it is the additional sync try block created by jdk 1.4");
	return bestSofar;
    }

    /** ??? */
    private StatementNode getLastNodeWithByteCodeOffset(CFG cfg, int byteCodeOffset, int type) {
	CFG revCfg = (CFG) cfg.reverse();
        StatementNode bestSofar = null;
	Node[] nodesDFO = revCfg.getNodesInDepthFirstOrder();
	for (int i = 0; i < nodesDFO.length; i++) {
	    if (nodesDFO[i] instanceof StatementNode) {
		StatementNode node = (StatementNode) nodesDFO[i];
		if ( (node.getByteCodeOffset() > 0) && ((type == -1) || (type == node.getType()))) {
		    if (node.getByteCodeOffset() == byteCodeOffset) {
			// node with exact bytecode offset and type found; return it
			return node;
		    } else if (node.getByteCodeOffset() < byteCodeOffset) {
			// keep the best one found so far
			if ((bestSofar == null) ||
			    // in case it's equal, use the first one found.
			    (bestSofar.getByteCodeOffset() < node.getByteCodeOffset())) {
			    bestSofar = node;
			}
		    }
		}
	    }
	}

	// If you reach here, you haven't found a node yet.
	// So settle for the next node with BCO as given.
	System.err.println("WARNING: cannot find the exact node with bytecode offset " + byteCodeOffset);
	System.err.println("     for the end of sync block.");
	return bestSofar;
    }

    /** ??? */
    private StatementNode getFirstNodeWithByteCodeOffset(CFG cfg, int byteCodeOffset)
    {
	return getFirstNodeWithByteCodeOffset(cfg, byteCodeOffset, -1);
    }

    /** ??? */
    private StatementNode getLastNodeWithByteCodeOffset(CFG cfg, int byteCodeOffset)
    {
	return getLastNodeWithByteCodeOffset(cfg, byteCodeOffset, -1);
    }

    /** ??? */
    private String getMethodEdgeLabel(Method m)
    {
	return ( m.getContainingType().getName() + "." + m.getSignature() );
    }

    /** ??? */
    public Vector getStartNodes()
    {
	return startNodes;
    }

	/** returns an XML representation of this object */
	public String toString ()
	{
		return "<gdg>" + getString () + "</gdg>";
	}

	/** returns the reduced representation of this object */
	public String toStringReduced ()
	{
		return "<gdg>" + getStringReduced () + "</gdg>";
	}

	/** returns the detailed representation of this object */
	public String toStringDetailed ()
	{
		return "<gdg>" + getStringDetailed () + "</gdg>";
	}
}
