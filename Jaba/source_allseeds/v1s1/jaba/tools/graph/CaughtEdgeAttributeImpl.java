package jaba.tools.graph;

import jaba.graph.EdgeAttribute;
import jaba.graph.EdgeAttributeImpl;

import jaba.sym.Class;

import java.util.Vector;
import java.util.Enumeration;

/**
 * An edge having this attribute is marked as representing an exception catch edge in the graph.
 * 
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/07/10 changed to implement CaughtEdgeAttribute
 */
public class CaughtEdgeAttributeImpl extends EdgeAttributeImpl implements CaughtEdgeAttribute
{
	/** ??? */
	private Vector exceptions = new Vector();

	/** ??? */
	public void addException(Class exceptionType)
	{
		exceptions.add(exceptionType);
	}

	/** ??? */
	public void removeException(Class exceptionType)
	{
		exceptions.remove(exceptionType);
	}

	/**
	 * Convenience method for converting the Vector to an Array (less efficient) from <code>getExceptionsVector</code>
	 */
	public Class [] getExceptions ()
	{
		return (Class []) exceptions.toArray (new Class [exceptions.size ()]);
	}

	/** ??? */
	public Vector getExceptionsVector ()
	{
		return exceptions;
	}

	/** ??? */
	public String [] getExceptionNames ()
	{
		Vector exceps = getExceptionsVector ();
		String[] exceptionNames = new String[exceps.size ()];
		int i = 0;
		for (Enumeration e = exceps.elements (); e.hasMoreElements (); i++)
		{
			exceptionNames [i] = ((Class) e.nextElement ()).getName();
		}
		return exceptionNames;
	}

	/** generates an XML representation of this object */
	public String toString ()
	{
		StringBuffer str = new StringBuffer ();

		str.append ("<caughtedgeattribute>");

		String [] names = getExceptionNames ();

		for (int i = 0; i < names.length; i++)
		{
			str.append ("<exceptionname>").append (names [i]).append ("</exceptionname>");
		}

		str.append ("</caughtedgeattribute>");

		return str.toString ();
	}
}
