package jaba.tools.graph;

import jaba.graph.EdgeAttribute;

import jaba.sym.Class;

import java.util.Vector;

/**
 * An edge having this attribute is marked as representing an exception catch edge in the graph.
 * 
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface CaughtEdgeAttribute extends EdgeAttribute
{
	/** ??? */
	public void addException (Class exceptionType);

	/** ??? */
	public void removeException (Class exceptionType);

	/**
	 * Convenience method for converting the Vector to an Array (less efficient) from <code>getExceptionsVector</code>
	 */
	public Class [] getExceptions ();

	/** ??? */
	public Vector getExceptionsVector ();

	/** ??? */
	public String [] getExceptionNames ();

	/** generates an XML representation of this object */
	public String toString ();
}
