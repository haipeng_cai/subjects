/* Copyright (c) 1999, The Ohio State University */

package jaba.classfile;

/**
 * Represents a Synthetic attribute. A Synthetic attribute is used in
 * the FieldInfo and MethodInfo structures of a class file format.
 * The presence of the Synthetic attribute indicates that the corresponding
 * field or method is not declared in the source code, but is added by
 * the Java compiler.
 * <p>
 * This class only contains members that are
 * inherited from GenericAttribute. It extends GenericAttribute to
 * distinguish Synthetic attributes from Generic attributes.
 * @see jaba.classfile.FieldInfo
 * @see jaba.classfile.MethodInfo
 */

public class SyntheticAttribute extends GenericAttribute {

    // Constructor

    /**
     * Creates a new SyntheticAttribute object by invoking the superclass
     * constructor.
     * @param index Index in the constant pool for the attribute name.
     */
    public SyntheticAttribute( int index ) {
        super( index );
    }

}
