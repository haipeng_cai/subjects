package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;

/**
 * Represents a Local variable table.
 *
 * All file contents Copyright (C) 1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the LocalVariableTableAttribute class.
 *      The class is responsible for representing a LocalVariableTable attribute from
 *      a java .class file.  Current implementation uses Version 1.1.3 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 1/1/97.
 * @author Don Lance -- <i>Created</i>
 * @author Huaxing Wu -- <i>Revised 3/21/02. Add a new constructor </i>
 * @author Jay Lofstead 2003/05/29 added a toString for generating XML
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 */
public class LocalVariableTableAttribute extends AttributeInfoImpl implements Cloneable
{
    ////////////////
    // Attributes //
    ////////////////

    /** Length of local variable table */
    private short                      _LocalVariableTableLength;

    /** All local var info, such as code offets, etc. */
    private LocalVariableTableEntry[]  _LocalVariableTable;

    /** Support for data verification */
    private int                        _codeLength;


    /////////////
    // Methods //
    /////////////

    // Constructor

    /** ??? */
    public LocalVariableTableAttribute(int index, int codeLength)
    {
        _attributeNameIndex       = (short)index;
        _LocalVariableTableLength = 0;
        _codeLength               = codeLength;
    }

    /** ??? */
    public LocalVariableTableAttribute(LocalVariableTableEntry[] lves) {
	_attributeNameIndex = (short) -1;
	_codeLength = -1;
	_LocalVariableTableLength = (short)lves.length;
	_LocalVariableTable = lves;
    }

    
    // Accessor methods

    /** ??? */
    public short getLocalVariableTableLength()  { return _LocalVariableTableLength; }

    /** ??? */
    public int   getCodeLength()                { return _codeLength;               }

    /** ??? */
    public LocalVariableTableEntry getLocalVariableTableEntry(int index) throws IndexOutOfBoundsException
    {
        if ((index < 0) || (index >= _LocalVariableTableLength))
            throw new IndexOutOfBoundsException("Invalid index for local variable table: " + index);

        return (LocalVariableTableEntry)_LocalVariableTable[index];//.clone();
    }


    /**
     * Returns the index of the entry in the local-variable table that
     * matches the parameters. An entry <code>e</code> matches if
     * (1) name index of <code>e</code> matches <code>varIndex</code>, and
     * (2) <code>bytecodeOffset</code> falls in the instruction range
     * specified in <code>e</code>.
     * The parameter <code>offset</code> is used to extend the beginning of
     * the valid instruction
     * @param varIndex the local variable index that appears in a symbolic
     *                 instruction.
     * @param byteCodeOffset offset of the instruction opcode in the bytecode
     *                       array.
     * @param offset the value used to extend the beginning of the valid
     *               instruction range for a match.
     * @return The index of the matching entry in the local-variable table,
     *         or -1 if there is no matching entry.
     */
    public int getLocalVariableTableEntryIndex( int varIndex,
                                                int bytecodeOffset,
                                                int offset )
    {
        //for ( int i=0; i<_LocalVariableTableLength; i++ ) {
	for ( int i=_LocalVariableTableLength-1; i>=0; i-- ) {
            if ( _LocalVariableTable[i].getIndex() == varIndex ) {
                int startPC = _LocalVariableTable[i].getStartPC();
                int length = _LocalVariableTable[i].getLength();
                if ( (bytecodeOffset >= (startPC-offset)) &&
                     (bytecodeOffset <= (startPC+length)) ) {
                    return i;
                }
            }
        }
        return -1;
    }
    
    /**
     * Returns an array containing all entries in the local-variable
     * table.
     * @return Array of entries in the local-variable table; the array has
     *         length 0 if there are no entries in the table.
     */
    public LocalVariableTableEntry[] getLocalVariableTableEntries() {
        if ( _LocalVariableTable == null ) {
            return new LocalVariableTableEntry[0];
        }
        return _LocalVariableTable;
    }

    /** Method loads a LocalVariableTableAttribute attribute object from an input stream
     *  Method assumes that the attribute name index has already been read;
     *  stream is positioned to read attribute length
     */
    public boolean Load(DataInput instream, ConstantPool pool) throws IOException
    {
        // Load the attribute info length & verify

        int length = instream.readInt();

        if (length < 0)
            throw new ClassFormatError("Invalid length for local variable table attribute; length = " + length);

        _attributeLength = length;


        // Load the local variable table information

        _LocalVariableTableLength = (short)instream.readUnsignedShort();

        if (_LocalVariableTableLength < 0)
            throw new ClassFormatError("Invalid local table length: " + _LocalVariableTableLength);

        if (_LocalVariableTableLength > 0)
        {
            _LocalVariableTable = new LocalVariableTableEntry[_LocalVariableTableLength];

            for (int k=0; k<_LocalVariableTableLength; k++)
            {
                _LocalVariableTable[k] = new LocalVariableTableEntry();

                if (_LocalVariableTable[k].Load(instream, pool, _codeLength) == false)
                {
                    System.err.println("Load of LocalVariableTableEntry failed");
                    return false;
                }

            } // for

        } // if

        return true;

    } // Load

    /** Output all information about this attribute */
    public boolean print()
    {
        System.out.println("                   LOCALVARIABLETABLE ATTRIBUTE : ");
        System.out.println("Length of local variable table = " + _LocalVariableTableLength);
        System.out.println("\nLocal variable table entries : ");

        for (int k=0; k<_LocalVariableTableLength; k++)
            _LocalVariableTable[k].print();

        return true;

    } // print

	/**
	 * returns an XML representation of this object
	 */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<localvaraibletableattribute>");

		for (int i = 0; i < _LocalVariableTableLength; i++)
		{
			buf.append (_LocalVariableTable [i]);
		}

		buf.append ("</localvaraibletableattribute>");

		return buf.toString ();
	}
} // LocalVariableTableAttribute
