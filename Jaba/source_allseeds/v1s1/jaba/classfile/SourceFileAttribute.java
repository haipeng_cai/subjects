package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;

/**
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the SourceFileAttribute class.
 *      The class is responsible for representing a SourceFile attribute from
 *      a java .class file.  Current implementation uses Version 1.1.2 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 12/19/96.
 * @author Jay Lofstead 2003/05/29 added a toString for generating XML
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 */
public class SourceFileAttribute extends AttributeInfoImpl implements Cloneable
{
    ////////////////
    // Attributes //
    ////////////////

    /** Constant pool index to source file name */
    private short   _sourceFileIndex;

    /** Actual constant (copy) from constant pool */
    private String  _fileString;


    /////////////
    // Methods //
    /////////////

    // Constructor

    /** ??? */
    public SourceFileAttribute(int index)
    {
        _attributeNameIndex = (short)index;
    }


    ////////////////////
    // Accessor methods
    ////////////////////

    /** ??? */
    public short  getSourceFileIndex() { return _sourceFileIndex; }

    /** ??? */
    public String getFileString()      { return (_fileString == null) ? null : new String(_fileString); }

    /** ??? */
    private void setSourceFileIndex(short index) { _sourceFileIndex = index; }

    /** ??? */
    private void setFileString(String file)
    {
        _fileString = (file == null) ? null : new String(file);
    }

    /** Method loads a SourceFile attribute object from an input stream
     *  Method assumes that the attribute name index has already been read;
     *  stream is positioned to read attribute length
     */
    public boolean Load(DataInput instream, ConstantPool pool) throws IOException
    {
        // Load the attribute info length & verify

        int length = instream.readInt();

        if (length < 0)
            throw new ClassFormatError("Invalid length for source file attribute; length = " + length);

        _attributeLength = length;


        // Verify the length - a SourceFile attribute can only have an attribute length of 2

        if (length != 2)
            throw new ClassFormatError("SourceFile attribute length in class file not equal to 2; value = " + length);


        // Load the attribute information

        int sourceFileIndex = instream.readUnsignedShort();


        // Verify that we have a valid source file index 

        if (pool.IsThisAConstantPoolIndex(sourceFileIndex) == false)
            throw new ClassFormatError("Source file index for SourceFile attribute is invalid; value = " + sourceFileIndex);


        // Verify that the index refer to Utf8 constant

        if (pool.VerifyIndexIsConstantType(sourceFileIndex,ConstantPoolInfo.CONSTANT_Utf8) == false)
            throw new ClassFormatError("Source file index for SourceFile attribute doesn't refer to a CONSTANT_Utf8;" +
                                       " index = " + sourceFileIndex);

        _sourceFileIndex = (short)sourceFileIndex;


        // Now obtain a copy of the constant

        ConstantPoolInfo  poolEntry = pool.getConstantPoolEntry(sourceFileIndex);

        if (poolEntry == null)
        {
            System.err.println("Source file index for SourceFile attribute refers to a bad constant pool entry (null);" +
                               " index = " + sourceFileIndex);
            return false;
        }

        _fileString = poolEntry.getString();

        return true;

    } // Load

    /** Output all information about this attribute */
    public boolean print()
    {
        System.out.print("                   SOURCEFILE ATTRIBUTE : ");
        System.out.println("Source file index = " + _sourceFileIndex + " (" + _fileString + ") ");

        return true;
    } // print

	/**
	 * returns an XML representation of this object
	 */
	public String toString ()
	{
		return "<sourcefileattribute><index>" + _sourceFileIndex + "</index><filestring>" + _fileString
			+ "</filestring><sourcefileattribute>";
	}
} // SourceFileAttribute
