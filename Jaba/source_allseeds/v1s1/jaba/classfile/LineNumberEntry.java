package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;

/**
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the LineNumberEntry class.
 *      The class is responsible for representing a LineNumberEntry for
 *      a LineNumberTableAttribute, read froma java .class file.
 *      Current implementation uses Version 1.1.3 of the Java Virtual Machine (JVM) spec.
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 12/30/96.
 * @author Jay Lofstead 2003/05/29 added a toString for XML generation
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 */
public class LineNumberEntry implements Cloneable, java.io.Serializable
{
    ////////////////
    // Attributes //
    ////////////////

    /** Index into code array for new line in original source file */
    private short  _start_pc;

    /** Line number in the original source file */
    private short  _line_number;


    /////////////
    // Methods //
    /////////////

    // Constructor

    /** ??? */
    public LineNumberEntry()
    {
        _start_pc    = (short)0;
        _line_number = (short)0;
    }


    // Accessor methods

    /** ??? */
    public short GetStartPC()     { return _start_pc;    }

    /** ??? */
    public short GetLineNumber()  { return _line_number; }

    /** Provide a clone of objects of this class */
    public Object clone()
    {
        try
        {
            return super.clone();
        }
        catch (CloneNotSupportedException e)
        {
            // This shouldn't happen since we're cloneable
            System.err.println("CloneNotSupportedException OCCURRED IN LineNumberEntry::Clone - " + e.getMessage());
            return null;
        }
    } // clone

    /** Method loads a LineNumberEntry object from an input stream Uses codeLength to verify the data loaded */
    public boolean Load(DataInput instream, int codeLength) throws IOException
    {
        short  startpc = (short)instream.readUnsignedShort();

        if (startpc >= codeLength)
            throw new ClassFormatError("Code start index for LineNumberEntry is invalid; value = " + startpc);

        _start_pc    = startpc;
        _line_number = (short)instream.readUnsignedShort();

        return true;

    } // Load

    /** Output all information about this object */
    public boolean print()
    {
        System.out.println("    Line number entry : ");
        System.out.println("Starting code index = " + _start_pc);
        System.out.println("Original source line number = " + _line_number);
        return true;

    } // print

	/**
	 * returns an XML representation of this object
	 */
	public String toString ()
	{
		return "<linenumberentry><startingcodeindex>" + _start_pc + "</startingcodeindex><linenumber>" + _line_number + "</linenumber></linenumberentry>";
	}
} // LineNumberEntry
