package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;

/**
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the ExceptionsAttribute class.
 *      The class is responsible for representing an Exceptions attribute from
 *      a java .class file.  Current implementation uses Version 1.1.3 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 12/19/96.
 * @author Jay Lofstead 2003/05/29 added a toString method to generate XML
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 */
public class ExceptionsAttribute extends AttributeInfoImpl implements Cloneable
{
    ////////////////
    // Attributes //
    ////////////////

    /** Number of exception entries in the exception table */
    private short       _NumberOfExceptions;

    /** Exceptions that can be thrown */
    private short[]     _ExceptionIndexTable;

    /** Copy of exception classes from the constant pool */
    private String[]    _ExceptionStrings;

    /////////////
    // Methods //
    /////////////

    // Constructor

    /** ??? */
    public ExceptionsAttribute(int index)
    {
        _attributeNameIndex = (short)index;
        _NumberOfExceptions = 0;
        _ExceptionStrings   = null;
    }

    /////////////////////
    // Accessor methods
    /////////////////////

    /** ??? */
    public short getNumberOfExceptions() { return _NumberOfExceptions; }

    /** ??? */
    public short getExceptionIndex(int index) throws IndexOutOfBoundsException  
    { 
        if ((index < 0) || (index >= _NumberOfExceptions))
            throw new IndexOutOfBoundsException("Invalid index for exception list: " + index);

        return _ExceptionIndexTable[index]; 
    }

    /** ??? */
    public String getExceptionString(int index) throws IndexOutOfBoundsException
    {
        if ((index < 0) || (index >= _NumberOfExceptions))
            throw new IndexOutOfBoundsException("Invalid index for exception string list: " + index);

        return (_ExceptionStrings[index] == null) ? null : new String(_ExceptionStrings[index]);
    }

    /** Method loads a ExceptionsAttribute attribute object from an input stream
     *  Method assumes that the attribute name index has already been read;
     *  stream is positioned to read attribute length
     */
    public boolean Load(DataInput instream, ConstantPool pool) throws IOException
    {
        // Load the attribute info length & verify

        int length = instream.readInt();

        if (length < 0)
            throw new ClassFormatError("Invalid length for exceptions attribute; length = " + length);

        _attributeLength = length;


        // Load the exceptions information

        _NumberOfExceptions = (short)instream.readUnsignedShort();

        if (_NumberOfExceptions < 0)
            throw new ClassFormatError("Invalid number of exceptions: " + _NumberOfExceptions);

        if (_NumberOfExceptions > 0)
        {
            _ExceptionIndexTable = new short[_NumberOfExceptions];
            _ExceptionStrings    = new String[_NumberOfExceptions];

            for (int k=0; k<_NumberOfExceptions; k++)
            {
                int exceptionIndex = instream.readUnsignedShort();

                // Specification seems to indicate that a zero exception index is OK
                // Following code should ignore a zero index

                if (exceptionIndex > 0)
                {
                    // Verify that we have a valid exception index 

                    if (pool.IsThisAConstantPoolIndex(exceptionIndex) == false)
                        throw new ClassFormatError("Exception index for Exceptions attribute is invalid; value = " + exceptionIndex);

                    // Verify that the index refers to Class constant

                    if (pool.VerifyIndexIsConstantType(exceptionIndex,ConstantPoolInfo.CONSTANT_Class) == false)
                        throw new ClassFormatError("Exception index for Exceptions attribute doesn't refer to a CONSTANT_Class;" +
                                                   " index = " + exceptionIndex);
                } // if 


                // Store the entry information for this exception

                _ExceptionIndexTable[k] = (short)exceptionIndex;

                if (exceptionIndex > 0)
                    _ExceptionStrings[k] = pool.GetStringForClassConstant(exceptionIndex);
            } // for
        } // if

        return true;
    } // Load

    /** Output all information about this attribute */
    public boolean print()
    {
        System.out.print("                   EXCEPTIONS ATTRIBUTE : ");
        System.out.println("Number of exceptions = " + _NumberOfExceptions);

        for (int k=0; k<_NumberOfExceptions; k++)
            System.out.println("Exception index = " + _ExceptionIndexTable[k] + " (" + _ExceptionStrings[k] + ") ");

        return true;

    } // print

	/**
	 * returns an XML representation of the object
	 */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<exceptionsattribute>");
		for (int i = 0; i < _NumberOfExceptions; i++)
		{
			buf.append ("<index>" + _ExceptionIndexTable [i] + "</index>");
			buf.append ("<strings>" + _ExceptionStrings [i] + "</strings>");
		}
		buf.append ("</exceptionsattribute>");

		return buf.toString ();
	}
} // ExceptionsAttribute
