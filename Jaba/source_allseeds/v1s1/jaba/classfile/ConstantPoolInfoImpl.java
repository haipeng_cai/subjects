package jaba.classfile;

/**     This file contains the implementation of the ConstantPoolInfo class.
 *      The class is responsible for representing a constant (all types) from
 *      a java .class file.  Current implementation uses Version 1.1.3 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 12/4/96.
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable; updated JavaDoc
 * @author Jay Lofstead 2003/07/10 changed to implement ConstantPoolInfo
 */
public class ConstantPoolInfoImpl implements ConstantPoolInfo, Cloneable, java.io.Serializable
{
	/** Constant type ID */
	private byte    _tagValue;

	/** Used by constants Class, NameAndType */
	private int     _nameIndex;

	/** Used by constants Fieldref, Methodref, InterfaceMethodref */
	private int     _classIndex;

	/** Used by constants Fieldref, Methodref, InterfaceMethodref */
	private int     _nameTypeIndex;

	/** Used by constant String */
	private int     _stringIndex;

	/** Used by constant NameAndType */
	private int     _descriptorIndex;

	/** Used by constant Integer */
	private int     _iValue;

	/** Used by constant Float */
	private float   _fValue;

	/** Used by constant Long */
	private long    _lValue;

	/** Used by constant Double */
	private double  _dValue;

	/** Used by constant Utf8 */
	private String  _string;

    ///////////////
    // Constants //
    ///////////////
     

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Class = 7;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Fieldref = 9;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Methodref = 10;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_InterfaceMethodref = 11;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_String = 8;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Integer = 3;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Float = 4;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Long = 5;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Double = 6;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_NameAndType = 12;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Utf8 = 1;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM.
     * This one rarely seen except in old (1.0) class files.  This is not handled properly.
     */
    public static final byte CONSTANT_Unicode = 2;

    /////////////
    // Methods //
    /////////////


    /////////////////////////////////////////////////////
    // Accessor methods
    //      Read/Write access to private class members
    /////////////////////////////////////////////////////

    /** ??? */
    public byte   getTagValue()        { return _tagValue;             }

    /** ??? */
    public int    getNameIndex()       { return _nameIndex;            }

    /** ??? */
    public int    getClassIndex()      { return _classIndex;           }

    /** ??? */
    public int    getNameTypeIndex()   { return _nameTypeIndex;        }

    /** ??? */
    public int    getStringIndex()     { return _stringIndex;          }

    /** ??? */
    public int    getDescriptorIndex() { return _descriptorIndex;      }

    /** ??? */
    public int    getInteger()         { return _iValue;               }

    /** ??? */

    /** ??? */
    public float  getFloat()           { return _fValue;               }

    /** ??? */
    public long   getLong()            { return _lValue;               }

    /** ??? */
    public double getDouble()          { return _dValue;               }

    /** ??? */
    public String getString()          { return (_string == null) ? null : new String(_string); }

    /** This method validates a given tag value */
    public boolean isValidTag(byte tag)
    {
        if ( (tag == CONSTANT_Class) ||
             (tag == CONSTANT_Fieldref) ||
             (tag == CONSTANT_Methodref) ||
             (tag == CONSTANT_InterfaceMethodref) ||
             (tag == CONSTANT_String) ||
             (tag == CONSTANT_Integer) ||
             (tag == CONSTANT_Float) ||
             (tag == CONSTANT_Long) ||
             (tag == CONSTANT_Double) ||
             (tag == CONSTANT_NameAndType) ||
             (tag == CONSTANT_Utf8) ||
             (tag == CONSTANT_Unicode) )
	{
                return true;
	}

        return false;

    } // isValidTag

    /** Load a class constant (via name index) into a ConstantPoolInfo object */
    public void LoadClass(int nameIndex)
    {
        _tagValue  = CONSTANT_Class;
        _nameIndex = nameIndex;
    }

    /** Load a field reference constant into a ConstantPoolInfo object */
    public void LoadFieldRef(int classIndex, int nameTypeIndex)
    {
        _tagValue      = CONSTANT_Fieldref;
        _classIndex    = classIndex;
        _nameTypeIndex = nameTypeIndex;
    }

    /** Load a method reference constant into a ConstantPoolInfo object */
    public void LoadMethodRef(int classIndex, int nameTypeIndex)
    {
        _tagValue      = CONSTANT_Methodref;
        _classIndex    = classIndex;
        _nameTypeIndex = nameTypeIndex;
    }

    /** Load an interface method reference constant into a ConstantPoolInfo object */
    public void LoadInterfaceMethodRef(int classIndex, int nameTypeIndex)
    {
        _tagValue      = CONSTANT_InterfaceMethodref;
        _classIndex    = classIndex;
        _nameTypeIndex = nameTypeIndex;
    }

    /** Load a string constant (via string index) into a ConstantPoolInfo object */
    public void LoadString(int stringIndex)
    {
        _tagValue    = CONSTANT_String;
        _stringIndex = stringIndex;
    }

    /** Load an integer constant into a ConstantPoolInfo object */
    public void LoadInteger(int iValue)
    {
        _tagValue = CONSTANT_Integer;
        _iValue   = iValue;
    }

    /** Load a float constant into a ConstantPoolInfo object */
    public void LoadFloat(float fValue)
    {
        _tagValue = CONSTANT_Float;
        _fValue   = fValue;
    }

    /** Load a float (IEEE 754 format) constant into a ConstantPoolInfo object */
    public void LoadIEEE754Float(int iValue)
    {
        _tagValue = CONSTANT_Float;

        if (iValue == 0x7f800000)
            _fValue = Float.POSITIVE_INFINITY;      

        else if (iValue == 0xff800000)
            _fValue = Float.NEGATIVE_INFINITY;         

        else if ( (iValue >= 0x7f800001) && (iValue <= 0x7fffffff) ||
                  (iValue >= 0xff800001) && (iValue <= 0xffffffff) )
            _fValue = Float.NaN;                                              // "Not a Number"

        else
        {
            int s = ((iValue >> 31) == 0) ? 1 : -1;
            int e = (iValue >> 23) & 0xff;
            int m = (e == 0) ? (iValue & 0x7fffff) << 1 : (iValue & 0x7fffff) | 0x800000;
            _fValue = (float)(s * m * Math.pow(2.0d,(double)(e-150)));
        }

        return;
    }

    /** Load a long constant into a ConstantPoolInfo object */
    public void LoadLong(long lValue)
    {
        _tagValue = CONSTANT_Long;
        _lValue   = lValue;
    }

    /** Load a double constant into a ConstantPoolInfo object */
    public void LoadDouble(double dValue)
    {
        _tagValue = CONSTANT_Double;
        _dValue   = dValue;
    }

    /** Load a double (IEEE 754 format) constant into a ConstantPoolInfo object */
    public void LoadIEEE754Double(long lValue)
    {
        _tagValue = CONSTANT_Double;

        if (lValue == 0x7f80000000000000L)
            _dValue = Double.POSITIVE_INFINITY;         

        else if (lValue == 0xff80000000000000L)
            _dValue = Double.NEGATIVE_INFINITY;        

        else if ( (lValue >= 0x7ff0000000000001L) && (lValue <= 0x7fffffffffffffffL) ||
                  (lValue >= 0xfff0000000000001L) && (lValue <= 0xffffffffffffffffL) )
            _dValue = Double.NaN;                                                         // "Not a Number"

        else
        {
            int  s = ((lValue >> 63) == 0) ? 1 : -1;
            int  e = (int)((lValue >> 52) & 0x7ff1L);
            long m = (e == 0) ? (lValue & 0xfffffffffffff1L) << 1 : (lValue & 0xfffffffffffff1L) | 0x10000000000000L;
            _dValue = (double)(s * m * Math.pow(2.0d,(double)(e-1075)));
        }

        return;
    }

    /** Load a nametype constant into a ConstantPoolInfo object */
    public void LoadNameType(int nameIndex, int descriptorIndex)
    {
        _tagValue        = CONSTANT_NameAndType;
        _nameIndex       = nameIndex;
        _descriptorIndex = descriptorIndex;
    }

    /** Load a string constant into a ConstantPoolInfo object */
    public void LoadString(String stringConstant)
    {
        _tagValue = CONSTANT_Utf8;
        _string   = new String(stringConstant);
    }

    /** Load a Utf8 constant into a ConstantPoolInfo object */
    public boolean LoadUtf8(int length, byte[] utf8String)
    {
        byte  mask;
        char  newchar;
        int   k=0, numBytes;

        _tagValue = CONSTANT_Utf8;
        _string   = new String();

        while (k < length)
        {
            numBytes = 0;
            mask     = (byte)-1;

            while ((utf8String[k] & mask) == 0)  // Search for zero bit to determine number of bytes
            {
                mask >>>= (byte) numBytes;
                numBytes++;
            }

            numBytes++;

            switch(numBytes)
            {
                case 1:
                {
                    newchar = (char) utf8String[k];
                    k++;
                    break;
                }

                case 2:
                {
                    newchar = (char) ( ((utf8String[k] & 0x1f) << 6) + (utf8String[k+1] & 0x3f) );
                    k+=2;
                    break;
                }

                case 3:
                {
                    newchar = (char) ( ((utf8String[k] & 0xf) << 12) +
                                       ((utf8String[k+1] & 0x3f) << 6) + (utf8String[k+2] & 0x3f) );
                    k+=3;
                    break;
                }

                default:
                {
                    System.err.println("\nERROR> Utf8 string constant has more than 3 bytes per char\n");
                    return false;
                }

            } // switch

            _string += String.valueOf(newchar);

        } // while

        return true;

    } // LoadUtf8

    /** Output information about the type and data of the constant info object */
    public void print()
    {
        switch(_tagValue)
        {
            case CONSTANT_Class:
            {
                System.out.print("CONSTANT_Class: name index = " + _nameIndex);
                break;
            }

            case CONSTANT_Fieldref:
            {
                System.out.print("CONSTANT_Fieldref: name index = " + _classIndex);
                System.out.print(", name & type index = " + _nameTypeIndex);
                break;
            }

            case CONSTANT_Methodref:
            {
                System.out.print("CONSTANT_Methodref: name index = " + _classIndex);
                System.out.print(", name & type index = " + _nameTypeIndex);
                break;
            }

            case CONSTANT_InterfaceMethodref:
            {
                System.out.print("CONSTANT_InterfaceMethodref: name index = " + _classIndex);
                System.out.print(", name & type index = " + _nameTypeIndex);
                break;
            }

            case CONSTANT_String:
            {
                System.out.print("CONSTANT_String: string index = " + _stringIndex);
                break;
            }

            case CONSTANT_Integer:
            {
                System.out.print("CONSTANT_Integer: value = " + _iValue);
                break;
            }

            case CONSTANT_Float:
            {
                System.out.print("CONSTANT_Float: value = " + _fValue);
                break;
            }

            case CONSTANT_Long:
            {
                System.out.print("CONSTANT_Long: value = " + _lValue);
                break;
            }

            case CONSTANT_Double:
            {
                System.out.print("CONSTANT_Double: value = " + _dValue);
                break;
            }

            case CONSTANT_NameAndType:
            {
                System.out.print("CONSTANT_NameAndType: name index = " + _nameIndex);
                System.out.print(", descriptor index = " + _descriptorIndex);
                break;
            }

            case CONSTANT_Utf8:
            {
                System.out.print("CONSTANT_Utf8: string = " + _string);
                break;
            }

            case CONSTANT_Unicode:
            {
                System.out.print("CONSTANT_Unicode: currently not handled\n");
                break;
            }

            default:
            {
                // Ignore any undefined case
                break;
            }
        } // switch

        return;
    } // print
} // ConstantPoolInfo
