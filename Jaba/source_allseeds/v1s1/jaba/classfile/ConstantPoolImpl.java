package jaba.classfile;

import java.io.IOException;
import java.io.DataInput;

/**
 * Module : ConstantPool.java
 * 
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the ConstantPool class.
 *      The class is responsible for representing the constant pool from
 *      a java .class file.  Current implementation uses Version 1.1.3 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 12/4/96.
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to implement ConstantPool
 */
public class ConstantPoolImpl implements ConstantPool, Cloneable, java.io.Serializable
{
    ////////////////
    // Attributes //
    ////////////////

    /** Number of entries in constant pool table */
    private short               _ConstantPoolCount;

    /** The constant pool entries */
    private ConstantPoolInfo[]  _ConstantPoolEntries;

    /////////////
    // Methods //
    /////////////

    // Constructor

    /** ??? */
    public ConstantPoolImpl ()
    {
        _ConstantPoolCount = (short)0;
    }

    ////////////////////
    // Accessor methods
    ////////////////////

    /** ??? */
    public short getConstantPoolCount()  { return _ConstantPoolCount; }

    /** ??? */
    public ConstantPoolInfo getConstantPoolEntry(int index) throws IndexOutOfBoundsException
    { 
        if (IsThisAConstantPoolIndex(index) == false) 
            throw new IndexOutOfBoundsException("Index constant pool index: " + index);

        return (ConstantPoolInfo)_ConstantPoolEntries[index]; //.clone();
    }

    /** Load the constant pool from a stream for the java class file.  Function assumes stream is correctly positioned to read in pool. */
    public boolean LoadConstantPool(DataInput instream) throws IOException
    {
        byte    tag;
        byte[]  buffer;
        int     iValue, iValue2;
        long    lValue;
        int     poolcount = instream.readUnsignedShort();


        // Verify constant pool count
        // It can never be <=0, since the count always includes the JVM reserved entry

        if (poolcount <= 0)
            throw new ClassFormatError("Invalid number of constant pool items found, number = " + poolcount);

        _ConstantPoolCount = (short)poolcount;


        // Load the constant pool entries
        // Note we skip index 0, which is reserved for the JVM
        // This will sync up all index references elsewhere

        _ConstantPoolEntries = new ConstantPoolInfo[_ConstantPoolCount];

        for (int k=1; k<_ConstantPoolCount; k++)
        {
            _ConstantPoolEntries[k] = new ConstantPoolInfoImpl ();
            tag = instream.readByte();

            switch(tag)
            {
                case ConstantPoolInfo.CONSTANT_Class:
                {
                    iValue = instream.readUnsignedShort();
                    _ConstantPoolEntries[k].LoadClass(iValue);
                    break;
                }

                case ConstantPoolInfo.CONSTANT_Fieldref:
                {
                    iValue  = instream.readUnsignedShort();
                    iValue2 = instream.readUnsignedShort();
                    _ConstantPoolEntries[k].LoadFieldRef(iValue,iValue2);
                    break;
                }

                case ConstantPoolInfo.CONSTANT_Methodref:
                {
                    iValue  = instream.readUnsignedShort();
                    iValue2 = instream.readUnsignedShort();
                    _ConstantPoolEntries[k].LoadMethodRef(iValue,iValue2);
                    break;
                }

                case ConstantPoolInfo.CONSTANT_InterfaceMethodref:
                {
                    iValue  = instream.readUnsignedShort();
                    iValue2 = instream.readUnsignedShort();
                    _ConstantPoolEntries[k].LoadInterfaceMethodRef(iValue,iValue2);
                    break;
                }

                case ConstantPoolInfo.CONSTANT_String:
                {
                    iValue = instream.readUnsignedShort();
                    _ConstantPoolEntries[k].LoadString(iValue);
                    break;
                }

                case ConstantPoolInfo.CONSTANT_Integer:
                {
                    iValue = instream.readInt();
                    _ConstantPoolEntries[k].LoadInteger(iValue);
                    break;
                }

                case ConstantPoolInfo.CONSTANT_Float:
                {
                    iValue = instream.readInt();
                    _ConstantPoolEntries[k].LoadIEEE754Float(iValue);
                    break;
                }

                case ConstantPoolInfo.CONSTANT_Long:
                {
                    lValue = instream.readLong();
                    _ConstantPoolEntries[k].LoadLong(lValue);
		    // Extra constant slot used
                    _ConstantPoolEntries[++k] = new ConstantPoolInfoImpl ();
		    _ConstantPoolEntries[k].LoadLong(lValue);
                    break;
                }

                case ConstantPoolInfo.CONSTANT_Double:
                {
                    lValue = instream.readLong();
                    _ConstantPoolEntries[k].LoadIEEE754Double(lValue);
		    // Extra constant slot used
                    _ConstantPoolEntries[++k] = new ConstantPoolInfoImpl ();
		    _ConstantPoolEntries[k].LoadLong(lValue);
                    break;
                }

                case ConstantPoolInfo.CONSTANT_NameAndType:
                {
                    iValue  = instream.readUnsignedShort();
                    iValue2 = instream.readUnsignedShort();
                    _ConstantPoolEntries[k].LoadNameType(iValue,iValue2);
                    break;
                }

                case ConstantPoolInfo.CONSTANT_Utf8:
                {
                    iValue = instream.readUnsignedShort();
                    buffer = new byte[iValue];
                    instream.readFully(buffer, 0, iValue);
                    _ConstantPoolEntries[k].LoadUtf8(iValue,buffer);
                    break;
                }

                case ConstantPoolInfo.CONSTANT_Unicode:
                {
                    System.err.println("\nFound unicode constant pool type - old format not handled (ignored)!\n");
                    break;
                }

                default:
                {
                    // Probably should throw an exception, but I'll be forgiving...
                    // This is just in case a new constant type is created in the future
                    //throw new ClassFormatError("Unhandled constant pool type found");

                    // ignore the undefined type
                    System.err.println("\nUnhandled constant pool type found : " + tag + " (ignored)");
                }
            } // switch
        } // for

        return true;
    } // LoadConstantPool

    /**
     * Utility function - Verify an index maps into the constant pool.
     * Note that the zero index is reserved by the JVM Class file spec incorrectly says index is valid if < (m_ConstantPoolCount - 1)
     */
    public boolean IsThisAConstantPoolIndex(int index)
    {
        if ((index > 0) && (index < _ConstantPoolCount)) return true;
        return false;
    }

    /** Utility function - Verify an index refers to a certain constant type */
    public boolean VerifyIndexIsConstantType(int index, byte constantType)
    {
        byte  tag;

        // First verify that we have a valid index

        if (IsThisAConstantPoolIndex(index) == false) return false;

        // Now check the constant pool entry

        tag = _ConstantPoolEntries[index].getTagValue();
        if (tag != constantType) return false;

        return true;

    } // VerifyIndexIsConstantType

    /** Utility function - Obtain the class name for an index to a field constant */
    public String getClassStringForFieldConstant(int index)
    {
        if (VerifyIndexIsConstantType(index,ConstantPoolInfo.CONSTANT_Fieldref) == false)
            throw new RuntimeException("Index doesn't refer to field constant");  
            
        return GetStringForClassConstant(_ConstantPoolEntries[index].getClassIndex());
    }

    /** Utility function - Obtain the field string for a field constant index */
    public String getFieldStringForFieldConstant(int index)
    {
        if (VerifyIndexIsConstantType(index,ConstantPoolInfo.CONSTANT_Fieldref) == false)
            throw new RuntimeException("Index doesn't refer to field constant");  

        return getNameForNameTypeConstant(_ConstantPoolEntries[index].getNameTypeIndex());
    }

    /** Utility function - Obtain the descriptor string for a field constant index */
    public String getDescriptorStringForFieldConstant(int index)
    {
        if (VerifyIndexIsConstantType(index,ConstantPoolInfo.CONSTANT_Fieldref) == false)
            throw new RuntimeException("Index doesn't refer to field constant");  

        return getDescriptorForNameTypeConstant(_ConstantPoolEntries[index].getNameTypeIndex());
    }

    /** Utility function - Obtain the class name for an index to a methodRef constant */
    public String getClassStringForMethodConstant(int index)
    {
        if (VerifyIndexIsConstantType(index,ConstantPoolInfo.CONSTANT_Methodref) == false)
            throw new RuntimeException("Index doesn't refer to method constant");  
            
        return GetStringForClassConstant(_ConstantPoolEntries[index].getClassIndex());
    }

    /** Utility function - Obtain the method string for a method constant index */
    public String getMethodStringForMethodConstant(int index)
    {
        if (VerifyIndexIsConstantType(index,ConstantPoolInfo.CONSTANT_Methodref) == false)
            throw new RuntimeException("Index doesn't refer to method constant");  

        return getNameForNameTypeConstant(_ConstantPoolEntries[index].getNameTypeIndex());
    }

    /** Utility function - Obtain the descriptor string for a method constant index */
    public String getDescriptorStringForMethodConstant(int index)
    {
        if (VerifyIndexIsConstantType(index,ConstantPoolInfo.CONSTANT_Methodref) == false)
            throw new RuntimeException("Index doesn't refer to method constant");  

        return getDescriptorForNameTypeConstant(_ConstantPoolEntries[index].getNameTypeIndex());
    }

    /** Utility function - Obtain the class name for an index to an interfaceRef constant */
    public String getClassStringForInterfaceConstant(int index)
    {
        if (VerifyIndexIsConstantType(index,ConstantPoolInfo.CONSTANT_InterfaceMethodref) == false)
            throw new RuntimeException("Index doesn't refer to interface constant");  
            
        return GetStringForClassConstant(_ConstantPoolEntries[index].getClassIndex());
    }

    /** Utility function - Obtain the string for an interface constant index */
    public String getMethodStringForInterfaceConstant(int index)
    {
        if (VerifyIndexIsConstantType(index,ConstantPoolInfo.CONSTANT_InterfaceMethodref) == false)
            throw new RuntimeException("Index doesn't refer to interface constant");  

        return getNameForNameTypeConstant(_ConstantPoolEntries[index].getNameTypeIndex());
    }

    /** Utility function - Obtain the descriptor string for an interface constant index */
    public String getDescriptorStringForInterfaceConstant(int index)
    {
        if (VerifyIndexIsConstantType(index,ConstantPoolInfo.CONSTANT_InterfaceMethodref) == false)
            throw new RuntimeException("Index doesn't refer to interface constant");  

        return getDescriptorForNameTypeConstant(_ConstantPoolEntries[index].getNameTypeIndex());
    }

    /** Utility function - Obtain the name string for a name-and-type constant index */
    public String getNameForNameTypeConstant(int index)
    {
        if (VerifyIndexIsConstantType(index,ConstantPoolInfo.CONSTANT_NameAndType) == false)
            throw new RuntimeException("Index doesn't refer to name-and-type constant");  

        return _ConstantPoolEntries[_ConstantPoolEntries[index].getNameIndex()].getString();
    }

    /** Utility function - Obtain the descriptor string for a name-and-type constant index */
    public String getDescriptorForNameTypeConstant(int index)
    {
        if (VerifyIndexIsConstantType(index,ConstantPoolInfo.CONSTANT_NameAndType) == false)
            throw new RuntimeException("Index doesn't refer to name-and-type constant"); 
            
        return _ConstantPoolEntries[_ConstantPoolEntries[index].getDescriptorIndex()].getString(); 
    }

    /** Utility function - Obtain the class name string for an index to a CONSTANT_Class */
    public String GetStringForClassConstant(int index)
    {
        // First verify that the index points to a CONSTANT_Class entry

        if (VerifyIndexIsConstantType(index,ConstantPoolInfo.CONSTANT_Class) == false)
            throw new RuntimeException("Index doesn't refer to class constant");
                     
        return _ConstantPoolEntries[_ConstantPoolEntries[index].getNameIndex()].getString();
    }

    /** Utility function - Obtain the class name string for an index to a CONSTANT_Class */
    public String GetStringForUtf8Constant(int index)
    {
        // First verify that the index points to a CONSTANT_Utf8 entry

        if (VerifyIndexIsConstantType(index,ConstantPoolInfo.CONSTANT_Utf8) == false)
            throw new RuntimeException("Index doesn't refer to UTF8 constant");

        return _ConstantPoolEntries[index].getString();
    }

    /** Output the constant pool information by index */
    public void PrintByIndex()
    {
        System.out.println("\nConstant pool count (including first constant reserved by JVM) = " + _ConstantPoolCount);
        System.out.println("\nConstant pool item #0 - Reserved by the JVM");

        for (int k=1; k<_ConstantPoolCount; k++)   // Start at index 1 - JVM uses index 0
        {
            System.out.print("Constant pool item #"+k);
            System.out.print(" - ");
            _ConstantPoolEntries[k].print();
            System.out.println("");

            if ( (_ConstantPoolEntries[k].getTagValue() == ConstantPoolInfo.CONSTANT_Long) ||
                 (_ConstantPoolEntries[k].getTagValue() == ConstantPoolInfo.CONSTANT_Double) )

                 k++;
        } // for - k
    } // PrintByIndex

    /** Output the constant pool information by group (constant type) */
    public void PrintByGroup()
    {
        System.out.println("\nConstant pool count (including first constant reserved by JVM) = " + _ConstantPoolCount);
        
        System.out.println("\n\nCONSTANT_Class items :");
        OutputConstantPoolGroup(ConstantPoolInfo.CONSTANT_Class);

        System.out.println("\n\nCONSTANT_Fieldref items :");
        OutputConstantPoolGroup(ConstantPoolInfo.CONSTANT_Fieldref);

        System.out.println("\n\nCONSTANT_Methodref items :");
        OutputConstantPoolGroup(ConstantPoolInfo.CONSTANT_Methodref);

        System.out.println("\n\nCONSTANT_InterfaceMethodref items :");
        OutputConstantPoolGroup(ConstantPoolInfo.CONSTANT_InterfaceMethodref);

        System.out.println("\n\nCONSTANT_String items :");
        OutputConstantPoolGroup(ConstantPoolInfo.CONSTANT_String);

        System.out.println("\n\nCONSTANT_Integer items :");
        OutputConstantPoolGroup(ConstantPoolInfo.CONSTANT_Integer);

        System.out.println("\n\nCONSTANT_Float items :");
        OutputConstantPoolGroup(ConstantPoolInfo.CONSTANT_Float);

        System.out.println("\n\nCONSTANT_Long items :");
        OutputConstantPoolGroup(ConstantPoolInfo.CONSTANT_Long);

        System.out.println("\n\nCONSTANT_Double items :");
        OutputConstantPoolGroup(ConstantPoolInfo.CONSTANT_Double);

        System.out.println("\n\nCONSTANT_NameAndType items :");
        OutputConstantPoolGroup(ConstantPoolInfo.CONSTANT_NameAndType);

        System.out.println("\n\nCONSTANT_Utf8 items :");
        OutputConstantPoolGroup(ConstantPoolInfo.CONSTANT_Utf8);

        System.out.println("\n\nCONSTANT_Unicode items :");
        OutputConstantPoolGroup(ConstantPoolInfo.CONSTANT_Unicode);
    } // PrintByGroup

    /** Internal utility function - Output constant pool items by constant type */
    private void OutputConstantPoolGroup(byte constantType)
    {
        for (int k=1; k<_ConstantPoolCount; k++)   // Start at index 1 - JVM uses index 0
        {
            byte  tag = _ConstantPoolEntries[k].getTagValue();

            if (tag == constantType)
            {
                System.out.print("Constant pool item #"+k);
                System.out.print(" - ");
                _ConstantPoolEntries[k].print();
                System.out.println("");
            }

            if ( (tag == ConstantPoolInfo.CONSTANT_Long) ||
                 (tag == ConstantPoolInfo.CONSTANT_Double) )

                 k++;
        } // for - k
    } // OutputConstantPoolGroup
} // ConstantPool
