package jaba.classfile;

import jaba.constants.AccessLevel;
import jaba.constants.Modifier;

import java.io.DataInput;
import java.io.IOException;

import java.util.Vector;

/**
 * Represents a method attribute in a java class. It contains all attributes tgat are part of this attribute, such as code attribute.
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the MethodInfo class.
 * 
 * Revision History : 
 *      Initial version, released 12/20/96.
 *
 * @author Don Lance -- <i>Created</i>
 * @author Huaxing Wu -- <i>Revised 3/21/02. change getAttribute() to return a reference instead of a copy</i>
 * @author Jay Lofstead 2003/05/22 removed commented out code to aid in readability.  added isPrivate method.
 * @author Jay Lofstead 2003/05/29 added a toString method generating XML
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to implement MethodInfo
 */
public class MethodInfoImpl implements MethodInfo, Cloneable, AccessLevel, Modifier, jaba.sym.MethodAttribute, java.io.Serializable
{
    ////////////////
    // Attributes //
    ////////////////

    /** Access flags for method */
    private short               _accessFlags;

    /** Name index into constant pool for method */
    private short               _nameIndex;

    /** Descriptor index into constant pool for method */
    private short               _descriptorIndex;

    /** Number of attributes associated with method */
    private short               _attributesCount;

    /** All attributes associated to the method */
    private AttributeInfo[]     _attributes;

    // The following attributes are copies from the constant pool

    /** Copy of method name from constant pool */
    private String              _nameString;

    /** Copy of method descriptor from constant pool */
    private String              _descriptorString;

    /** does this method have a synthetic attribute? */
    boolean syntheticFlag = false;

    /////////////
    // Methods //
    /////////////

    /** ??? */
    public MethodInfoImpl ()
    {
        _accessFlags      = (short)0;
        _nameIndex        = (short)0;
        _descriptorIndex  = (short)0;
        _attributesCount  = (short)0;
        _attributes       = null;
        _nameString       = null;
        _descriptorString = null;
    }

    ////////////////////
    // Accessor methods
    ////////////////////

    /** ???  */
    public short  getAccessFlags()      { return _accessFlags;                  }

    /** ???  */
    public short  getNameIndex()        { return _nameIndex;                    }

    /** ???  */
    public short  getDescriptorIndex()  { return _descriptorIndex;              }

    /** ???  */
    public short  getAttributeCount()   { return _attributesCount;              }

    /** ???  */
    public String getNameString()       { return (_nameString == null)       ? null : new String(_nameString);       }

    /** ???  */
    public String getDescriptorString() { return (_descriptorString == null) ? null : new String(_descriptorString); }

    /** ???  */
    public boolean isSynthetic() { return syntheticFlag; }

    /** Find attribute of the specified type
     *	Return the index of the attribute.
     *	If not found, return -1
     */
    public int findAttribute(AttributeInfo attr) {
	for(int i = 0; i < _attributesCount; i++) {
	    if(_attributes[i].getClass().isInstance(attr)){
		return i;
	    }
	}
	return -1;
    }

    /** ???  */
    public AttributeInfo getAttribute(int index) throws IndexOutOfBoundsException
    {
        if ((index < 0) || (index >= _attributesCount))
            throw new IndexOutOfBoundsException("Invalid index into attributes: " + index);
	else
		return _attributes [index];

    } // getAttribute

	/** indicates if this is a private method or not */
	public boolean isPrivate ()
	{
		return (_accessFlags & A_PRIVATE) != 0 ? true : false;
	}

	/** indicates if this is a static method or not */
	public boolean isStatic ()
	{
		return (_accessFlags & M_STATIC) != 0 ? true : false;
	}

    /** Load a MethodInfo object from a input stream attached to a .class file
     *  We require the constant pool as a parameter to use in error checking
     */
    public boolean Load(DataInput instream, ConstantPool pool) throws IOException
    {
        // Load & verify the method access flags

        if (LoadAccessFlags(instream) == false)
        {
            System.err.println("Load of method access flags failed.");
            return false;
        }


        // Load & verify the method name index

        if (LoadNameIndex(instream,pool) == false)
        {
            System.err.println("Load of method name index failed.");
            return false;
        }


        // Load & verify the method descriptor index

        if (LoadDescriptorIndex(instream,pool) == false)
        {
            System.err.println("Load of method descriptor index failed.");
            return false;
        }


        // Load & verify method attributes

        if (LoadAttributes(instream,pool) == false)
        {
            System.err.println("Load of method attributes failed.");
            return false;
        }

        return true;

    } // Load

    /** Load & verify method access flags from an input stream attached to a .class file */
    private boolean LoadAccessFlags(DataInput instream) throws IOException
    {
        ///////////////////////////
        // Load the access flags
        ///////////////////////////

        short allbits = (short)(A_PUBLIC | A_PRIVATE       | A_PROTECTED | M_STATIC | 
                                M_FINAL  | M_SYNCHRONIZED  | M_NATIVE    | M_ABSTRACT);

        short flags = (short)instream.readUnsignedShort();
        
        // Verify that no improper method access bits are set

        // Make sure only one type of access is specified

        if ( ( ((flags & A_PUBLIC)  != 0) && ((flags & A_PRIVATE)   != 0) ) ||
             ( ((flags & A_PUBLIC)  != 0) && ((flags & A_PROTECTED) != 0) ) ||
             ( ((flags & A_PRIVATE) != 0) && ((flags & A_PROTECTED) != 0) ) )

            throw new ClassFormatError("Method in class file is set for multiple types of access;" +
                                       " (combinations of public, private, protected) : improper access bits specified");


        // Make sure M_ABSTRACT isn't matched with M_FINAL, M_NATIVE, M_SYNCHRONIZED

        if ( ( ((flags & M_ABSTRACT) != 0) && ((flags & M_FINAL) != 0) ) ||
             ( ((flags & M_ABSTRACT) != 0) && ((flags & M_NATIVE) != 0) ) ||
             ( ((flags & M_ABSTRACT) != 0) && ((flags & M_SYNCHRONIZED) != 0) ) )

            throw new ClassFormatError("Method in class file mixes \"abstract\" with final, native, or synchronized;" +
                                       " improper access bits specified");


        // Make sure M_ABSTRACT isn't matched with A_PRIVATE

        if ( ((flags & M_ABSTRACT) != 0) && ((flags & A_PRIVATE) != 0) ) 
            throw new ClassFormatError("Method in class file mixes \"abstract\" with private;" +
                                       " improper access bits specified");


        // Make sure M_ABSTRACT isn't matched with M_STATIC

        if ( ((flags & M_ABSTRACT) != 0) && ((flags & M_STATIC) != 0) ) 
            throw new ClassFormatError("Method in class file mixes \"abstract\" with static;" +
                                       " improper access bits specified");


        // Also - 
        //  constructors can only be private, public, and protected
        //  interfaces must have M_ABSTRACT and A_PUBLIC, with no other bits
        // Don't know how to verify this at this time

        _accessFlags = flags;

        return true;

    } // LoadAccessFlags

    /** Load & verify method name index from an input stream attached to a .class file */
    private boolean LoadNameIndex(DataInput instream, ConstantPool pool) throws IOException
    {
        // Load & verify the name constant pool index

        int nameIndex = instream.readUnsignedShort();

        if (pool.IsThisAConstantPoolIndex(nameIndex) == false)
            throw new ClassFormatError("Invalid constant pool index for method; value = " + nameIndex);

        if (pool.VerifyIndexIsConstantType(nameIndex,ConstantPoolInfo.CONSTANT_Utf8) == false)
            throw new ClassFormatError("Constant pool index for method is not CONSTANT_Utf8; index = " + nameIndex);

        _nameIndex = (short)nameIndex;


        // Now retrieve a copy of the method name from the constant pool

        ConstantPoolInfo  poolEntry = pool.getConstantPoolEntry(nameIndex);

        if (poolEntry == null)
        {
            System.err.println("Name index for method refers to invalid constant pool entry; index = " + nameIndex);
            return false;
        }

        _nameString = poolEntry.getString();

        return true;

    } // LoadNameIndex

    /** Load & verify method name index from an input stream attached to a .class file */
    private boolean LoadDescriptorIndex(DataInput instream, ConstantPool pool) throws IOException
    {
        // Load & verify the descriptor constant pool index

        int descIndex = instream.readUnsignedShort();

        if (pool.IsThisAConstantPoolIndex(descIndex) == false)
            throw new ClassFormatError("Invalid constant pool index for method; value = " + descIndex);

        if (pool.VerifyIndexIsConstantType(descIndex,ConstantPoolInfo.CONSTANT_Utf8) == false)
            throw new ClassFormatError("Constant pool index for method is not CONSTANT_Utf8; index = " + descIndex);

        _descriptorIndex = (short)descIndex;


        // Now get a copy of the descriptor string from the constant pool

        ConstantPoolInfo  poolEntry = pool.getConstantPoolEntry(descIndex);

        if (poolEntry == null)
        {
            System.err.println("Descriptor index for method refers to invalid constant pool entry");
            return false;
        }

        _descriptorString = poolEntry.getString();

        return true;

    } // LoadDescriptorIndex

    /** Load & verify method attributes from an input stream attached to a .class file */
    private boolean LoadAttributes(DataInput instream, ConstantPool pool) throws IOException
    {
        int     attributeCount;
        int     attributeIndex;
        String  attributeName;

        // Load the number of attributes

        attributeCount = instream.readUnsignedShort();

        if (attributeCount < 0)
            throw new ClassFormatError("Invalid number of field attributes found; value = " + attributeCount);

        _attributesCount = (short)attributeCount;


        // Load the attributes

        if (attributeCount > 0)
        {
            _attributes = new AttributeInfo[attributeCount];

            // LOOP : 
            //  Load and create each attribute from the input stream

            for (int k=0; k<attributeCount; k++)
            {
                // Read the attribute name index to find out type

                attributeIndex = instream.readUnsignedShort();

                // Verify the attribute name index is valid

                if (pool.IsThisAConstantPoolIndex(attributeIndex) == false)
                    throw new ClassFormatError("Invalid attribute name index found in method; value = " + attributeIndex);

                // Verify the attribute name index refers to a CONSTANT_Utf8

                if (pool.VerifyIndexIsConstantType(attributeIndex,ConstantPoolInfo.CONSTANT_Utf8) == false)
                    throw new ClassFormatError("Attribute name index for method is not CONSTANT_Utf8; index = " + attributeIndex);


                // Obtain the name of the attribute
                // Then create the appropiate attribute object

                attributeName = pool.GetStringForUtf8Constant(attributeIndex);

                AttributeInfo  attr=null;

                if (attributeName.compareTo("Code") == 0)
                    attr = (AttributeInfo) new CodeAttribute(attributeIndex);

                else if (attributeName.compareTo("Exceptions") == 0)
                    attr = (AttributeInfo) new ExceptionsAttribute(attributeIndex);

		 /* if a synthetic attribute exists for this method, 
		    we set this method as synthetic */
                else if (attributeName.compareTo("Synthetic") == 0) {
                    attr = (AttributeInfo) new SyntheticAttribute(attributeIndex);  
		    syntheticFlag = true;
		}

                else
                    attr = (AttributeInfo) new GenericAttribute(attributeIndex);


                if (attr.Load(instream,pool) == false)
                {
                    System.err.println("Attribute load failure occurred");
                    return false;
                }

                _attributes[k] = attr;

            } // for

        } // if

        return true;

    } // LoadAttributes

    /** This method builds a Vector containing the types of all parameters of a method */
    public Vector GetParamsFromDescriptor()
    {
        int      loc=0;
        boolean  gotAllParams=false;
        Vector   params = new Vector();

        // Error check

        if (_descriptorString == null)
            throw new NullPointerException("Descriptor is null");


        // LOOP:
        //      Loop through the decriptor string
        //      Extract and store all the parameters

        while (true)
        {
            // Act on the current descriptor char

            switch (_descriptorString.charAt(loc))
            {
                // If beginning of descriptor, continue

                case '(':  
                { 
                    loc++; 
                    break; 
                }


                // If end of parameter list, we're finished
                // Don't worry about return type

                case ')':  
                { 
                    loc++;
                    gotAllParams = true; 
                    break; 
                }


                // If a primitive type, store it in the Vector

                case 'B':   // byte
                case 'C':   // char
                case 'D':   // double
                case 'F':   // float
                case 'I':   // integer
                case 'J':   // long
                case 'S':   // short
                case 'Z':   // boolean
                {
                    char type = _descriptorString.charAt(loc);
                    params.addElement(String.valueOf(type));
                    loc++;
                    break;
                }


                // If an object reference, store entire object name

                case 'L':
                {
                    int end = _descriptorString.indexOf(';',loc);
                    params.addElement(_descriptorString.substring(loc,end+1));
                    loc = end + 1;
                    break;
                }


                // If an array, store whole array descriptor

                case '[':
                {
                    int end = loc + 1;

                    while (_descriptorString.charAt(end) == '[')
                        end++;

                    params.addElement(_descriptorString.substring(loc,end+1));
                    loc = end + 1;
                    break;
                }

                default:
                {
                    char type = _descriptorString.charAt(loc);
                    throw new RuntimeException("Unhandled case in method descriptor: " + type);
                }

            } // switch


            // Safety check to avoid weird cases
            //   that might cause infinite loops

            if (gotAllParams || (loc >= _descriptorString.length()) )
                break;

        } // while

        return params;
        
    } // GetParameters

    /** Output all information about a field */
    public boolean print()
    {
        System.out.println("   METHOD:");


        // Access flags

        System.out.print("      Access flags = ");

        if ((_accessFlags & A_PUBLIC) != 0)        System.out.print("PUBLIC ");
        if ((_accessFlags & A_PRIVATE) != 0)       System.out.print("PRIVATE ");
        if ((_accessFlags & A_PROTECTED) != 0)     System.out.print("PROTECTED ");
        if ((_accessFlags & M_STATIC) != 0)        System.out.print("STATIC ");
        if ((_accessFlags & M_FINAL) != 0)         System.out.print("FINAL ");
        if ((_accessFlags & M_SYNCHRONIZED) != 0)  System.out.print("SYNCHRONIZED ");
        if ((_accessFlags & M_NATIVE) != 0)        System.out.print("NATIVE ");
        if ((_accessFlags & M_ABSTRACT) != 0)      System.out.print("ABSTRACT ");
        System.out.println("");


        // Indices

        System.out.println("      Name index = " + _nameIndex + " (" + _nameString + ")");
        System.out.println("      Descriptor index = " + _descriptorIndex + " (" + _descriptorString + ")");
        

        // Attributes

        System.out.println("\n      Number of attributes = " + _attributesCount);
        System.out.println("      Attributes : ");

        for (int k=0; k<_attributesCount; k++)
            _attributes[k].print();

        return true;

    } // print

	/**
	 * Returns an XML representation of the object
	 */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<methodinfo>");

		buf.append ("<nameindex>").append (_nameIndex).append ("</nameindex>");
		buf.append ("<descriptorindex>").append (_descriptorIndex).append ("</descriptorindex>");

		for (int i = 0; _attributes != null && i < _attributes.length; i++)
		{
			buf.append (_attributes [i]);
		}

		if ((_accessFlags & A_PUBLIC) != 0)
		{
			buf.append ("<accessflag>public</accessflag>");
		}
		if ((_accessFlags & A_PRIVATE) != 0)
		{
			buf.append ("<accessflag>private</accessflag>");
		}
		if ((_accessFlags & A_PROTECTED) != 0)
		{
			buf.append ("<accessflag>protected</accessflag>");
		}
		if ((_accessFlags & M_STATIC) != 0)
		{
			buf.append ("<accessflag>static</accessflag>");
		}
		if ((_accessFlags & M_FINAL) != 0)
		{
			buf.append ("<accessflag>final</accessflag>");
		}
		if ((_accessFlags & M_SYNCHRONIZED) != 0)
		{
			buf.append ("<accessflag>synchronized</accessflag>");
		}
		if ((_accessFlags & M_NATIVE) != 0)
		{
			buf.append ("<accessflag>native</accessflag>");
		}
		if ((_accessFlags & M_ABSTRACT) != 0)
		{
			buf.append ("<accessflag>abstract</accessflag>");
		}

		buf.append ("</methodinfo>");

		return buf.toString ();
	}
} // MethodInfo
