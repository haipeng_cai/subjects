package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;

/**
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the ExceptionHandler class.
 *      The class is responsible for representing an exception handler from
 *      a java .class file.  Current implementation uses Version 1.1.3 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 12/20/96.
 * @author Jay Lofstead added a toStringMethod that returns XML formatted information
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 */
public interface ExceptionHandler extends Cloneable, java.io.Serializable
{
	/** ??? */
	public short  getStartPC();

	/** ??? */
	public short  getEndPC();

	/** ??? */
	public short  getHandlerPC();

	/** ??? */
	public short  getCatchType();

	/** ??? */
	public String getCatchString();

	/** Load & verify an ExceptionHandler from a DataInput */
	public boolean Load(DataInput instream, ConstantPool pool, int codeLength) throws IOException;

	/** Output all information about this handler */
	public boolean print();

	/** returns an XML format of the object */
	public String toString ();
}
