package jaba.classfile;

import jaba.constants.AccessLevel;
import jaba.constants.Modifier;

import java.io.DataInput;
import java.io.IOException;

import java.util.Vector;

/**
 * Represents a method attribute in a java class. It contains all attributes tgat are part of this attribute, such as code attribute.
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the MethodInfo class.
 * 
 * Revision History : 
 *      Initial version, released 12/20/96.
 *
 * @author Don Lance -- <i>Created</i>
 * @author Huaxing Wu -- <i>Revised 3/21/02. change getAttribute() to return a reference instead of a copy</i>
 * @author Jay Lofstead 2003/05/22 removed commented out code to aid in readability.  added isPrivate method.
 * @author Jay Lofstead 2003/05/29 added a toString method generating XML
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 */
public interface MethodInfo extends Cloneable, AccessLevel, Modifier, jaba.sym.MethodAttribute, java.io.Serializable
{
    /** ???  */
    public short  getAccessFlags();

    /** ???  */
    public short  getNameIndex();

    /** ???  */
    public short  getDescriptorIndex();

    /** ???  */
    public short  getAttributeCount();

    /** ???  */
    public String getNameString();

    /** ???  */
    public String getDescriptorString();

    /** ???  */
    public boolean isSynthetic();

    /**
     * Find attribute of the specified type
     * Return the index of the attribute.
     * If not found, return -1
     */
    public int findAttribute(AttributeInfo attr);

    /** ???  */
    public AttributeInfo getAttribute(int index) throws IndexOutOfBoundsException;

	/** indicates if this is a private method or not */
	public boolean isPrivate ();

	/** indicates if this is a static method or not */
	public boolean isStatic ();

    /**
     * Load a MethodInfo object from a input stream attached to a .class file
     * We require the constant pool as a parameter to use in error checking
     */
    public boolean Load(DataInput instream, ConstantPool pool) throws IOException;

    /** This method builds a Vector containing the types of all parameters of a method */
    public Vector GetParamsFromDescriptor();

    /** Output all information about a field */
    public boolean print();

	/**
	 * Returns an XML representation of the object
	 */
	public String toString ();
} // MethodInfo
