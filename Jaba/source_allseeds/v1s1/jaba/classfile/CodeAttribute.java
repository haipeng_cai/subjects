package jaba.classfile;

import java.io.IOException;
import java.io.DataInput;

/**
 * Module : CodeAttribute.java
 * 
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the CodeAttribute class.
 *      The class is responsible for representing a Code attribute from
 *      a java .class file.  Current implementation uses Version 1.1.3 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 12/20/96.
 * @author Jay Lofstead 2003/05/29 added a toString method for generating XML
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 fixed a typo in the toString method
 */
public class CodeAttribute extends AttributeInfoImpl implements Cloneable
{
    ////////////////
    // Attributes //
    ////////////////

    /** Maximum number of entries on operand stack for a method */
    private short               _maxStack;

    /** Number of local vars used in a method */
    private short               _maxLocals;

    /** Number of bytes in _code */
    private int                 _codeLength;

    /** Actual bytes of JVM code that implement a method */
    private byte[]              _code;

    /** Number of entries in _exceptionTable */
    private short               _exceptionTableLength;

    /** Exception handlers in the code array */
    private ExceptionHandler[]  _exceptionTable;

    /** Number of attributes attached to the code attribute */
    private short               _attributeCount;

    /** Attributes attached to the code attribute */
    private AttributeInfo[]     _attributes;

    /////////////
    // Methods //
    /////////////

    // Constructor
 
    /** ??? */
    public CodeAttribute(int index)
    {
        _attributeNameIndex   = (short)index;
        _maxStack             = (short)0;
        _maxLocals            = (short)0;
        _codeLength           = 0;
        _exceptionTableLength = (short)0;
        _attributeCount       = (short)0;
    }

    /////////////////////
    // Accessor methods
    /////////////////////

    /** ??? */
    public short  getMaxStack()           { return _maxStack;             }

    /** ??? */
    public short  getMaxLocals()          { return _maxLocals;            }

    /** ??? */
    public int    getCodeLength()         { return _codeLength;           }

    /** ??? */
    public short  getExceptionLength()    { return _exceptionTableLength; }

    /** ??? */
    public short  getAttributeCount()     { return _attributeCount;       }

    /** ??? */
    public byte[] getCode()               { return _code;                 }

    /** ??? */
    public byte getCodeByte(int index)
    { 
        if (index >= _codeLength) 
            throw new IndexOutOfBoundsException("Invalid index for code byte: " + index);

        return _code[index]; 

    } // getCodeByte

    /** ??? */
    public ExceptionHandler getExceptionHandler(int index)
    {
        if ((index < 0) || (index >= _exceptionTableLength))
            throw new IndexOutOfBoundsException("Invalid index for exception handler: " + index);

        return (ExceptionHandler)_exceptionTable[index]; //.clone();

    } // getExceptionHandler

    /** Find attribute of the specified type
     *	Return the index of the attribute.
     *	If not found, return -1
     */
    public int findAttribute(AttributeInfo attr) {
	for(int i = 0; i < _attributeCount; i++) {
	    if(_attributes[i].getClass().isInstance(attr)){
		return i;
	    }
	}
	return -1;
    }

    /** Auditate the LocalVariableTable:
     *	If it does not exist, create it
     *	If it exists, replace it
     **/
    public void mendLocalVariableTableAttribute(LocalVariableTableAttribute lvta)
    {
	int index = findAttribute(lvta);

	if(index < 0) {
	   
	    AttributeInfo[] tmp = new AttributeInfo[_attributeCount + 1];
	    if(_attributeCount > 0) 
		System.arraycopy( _attributes, 0, tmp, 0,  _attributeCount);
	    _attributeCount ++;
	     _attributes = tmp;
	    setAttribute(_attributeCount -1, lvta);
	   
	}else {
	    setAttribute(index, lvta);
	}
    }

    /** ??? */
    public AttributeInfo getAttribute(int index)
    {
        if ((index < 0) || (index >= _attributeCount))
            throw new IndexOutOfBoundsException("Invalid index for attribute: " + index);

        if (_attributes[index] instanceof GenericAttribute)
        {
            GenericAttribute attr = (GenericAttribute)_attributes[index];
            return (AttributeInfo)attr;//.clone();
        }

        else if (_attributes[index] instanceof SourceFileAttribute)
        {
            SourceFileAttribute attr = (SourceFileAttribute)_attributes[index];
            return (AttributeInfo)attr;//.clone();
        }

        else if (_attributes[index] instanceof ConstantValueAttribute)
        {
            ConstantValueAttribute attr = (ConstantValueAttribute)_attributes[index];
            return (AttributeInfo)attr;//.clone();
        }

        else if (_attributes[index] instanceof CodeAttribute)
        {
            CodeAttribute attr = (CodeAttribute)_attributes[index];
            return (AttributeInfo)attr;//.clone();
        }
        
        else if (_attributes[index] instanceof ExceptionsAttribute)
        {
            ExceptionsAttribute attr = (ExceptionsAttribute)_attributes[index];
            return (AttributeInfo)attr;//.clone();
        }
        
        else if (_attributes[index] instanceof LineNumberTableAttribute)
        {
            LineNumberTableAttribute attr = (LineNumberTableAttribute)_attributes[index];
            return (AttributeInfo)attr;//.clone();
        }

        else if (_attributes[index] instanceof LocalVariableTableAttribute)
        {
            LocalVariableTableAttribute attr = (LocalVariableTableAttribute)_attributes[index];
            return (AttributeInfo)attr;//.clone();
        }
        
        // There are no other attribute types, per the specification

        return null;

    } // getAttribute

    /** ??? */
    private void setAttribute(int index, AttributeInfo attribute)
    {
        // Error checks

        if ((index < 0) || (index >= _attributeCount))
            throw new IndexOutOfBoundsException("Invalid index for attribute: " + index);

        if (attribute == null)
            throw new NullPointerException("Null AttributeInfo given; not allowed");

	_attributes[index] = attribute;
    } // setAttribute

    /**
     * This method loads a CodeAttribute object from a DataInput
     * It is assumed that the name index has already been loaded
     * The constant pool is used for input verification
     * Method returns true on success, false otherwise
     * Possible Exceptions or Errors can be thrown when loading
     */
    public boolean Load(DataInput instream, ConstantPool pool) throws IOException
    {
        // Load the attribute info length & verify

        int length = instream.readInt();

        if (length < 0)
            throw new ClassFormatError("Invalid length for code attribute; length = " + length);

        _attributeLength = length;


        // Note - no verification required on length - code attribute is of variable length


        // Load the attribute information

        _maxStack  = (short)instream.readUnsignedShort();
        _maxLocals = (short)instream.readUnsignedShort();


        // Load the bytecodes for the attribute

        int codeLength = instream.readInt();

        if (codeLength <= 0)
            throw new ClassFormatError("Invalid code length for code attribute; code length = " + codeLength);

        _codeLength = codeLength;

        _code = new byte[codeLength];
        instream.readFully(_code);


        // Load any exceptions

        _exceptionTableLength = (short)instream.readUnsignedShort();

        if (_exceptionTableLength < 0)
            throw new ClassFormatError("Invalid exception table length for code attribute; length = " + _exceptionTableLength);

        if (_exceptionTableLength > 0)
        {
            _exceptionTable = new ExceptionHandler[_exceptionTableLength];

            for (int k=0; k<_exceptionTableLength; k++)
            {
                _exceptionTable[k] = new ExceptionHandlerImpl ();

                if (_exceptionTable[k].Load(instream, pool, codeLength) == false)
                {
                    System.err.println("Load failure occurred for ExceptionHandler");
                    return false;
                }

            } // for

        } // if

        // Load & verify code attributes
        if (LoadAttributes(instream,pool) == false)
        {
            System.err.println("Load of code attributes failed.");
            return false;
        }

        return true;
    } // Load

    /** Load & verify code attributes from an input stream attached to a .class file */
    private boolean LoadAttributes(DataInput instream, ConstantPool pool) throws IOException
    {
        int     attributeCount;
        int     attributeIndex;
        String  attributeName;

        // Load the number of attributes

        attributeCount = instream.readUnsignedShort();

        if (attributeCount < 0)
            throw new ClassFormatError("Invalid number of code attributes found; value = " + attributeCount);

        _attributeCount = (short)attributeCount;

        // Load the attributes
        if (attributeCount > 0)
        {
            _attributes = new AttributeInfo[attributeCount];

            // LOOP : 
            //  Load and create each attribute from the input stream

            for (int k=0; k<attributeCount; k++)
            {
                // Read the attribute name index to find out type

                attributeIndex = instream.readUnsignedShort();


                // Verify the attribute name index is valid

                if (pool.IsThisAConstantPoolIndex(attributeIndex) == false)
                    throw new ClassFormatError("Invalid attribute name index found in code attribute; value = " + attributeIndex);


                // Verify the attribute name index refers to a CONSTANT_Utf8

                if (pool.VerifyIndexIsConstantType(attributeIndex,ConstantPoolInfo.CONSTANT_Utf8) == false)
                    throw new ClassFormatError("Attribute name index for code attribute is not CONSTANT_Utf8; index = " + attributeIndex);


                // Obtain the name of the attribute (predefined attributes are reserved by the class file spec)
                // Then create the appropiate attribute object

                attributeName = pool.GetStringForUtf8Constant(attributeIndex);

                AttributeInfo  attr=null;

                if (attributeName.compareTo("LineNumberTable") == 0)
                    attr = (AttributeInfo) new LineNumberTableAttribute(attributeIndex,_codeLength);

                else if (attributeName.compareTo("LocalVariableTable") == 0)
                    attr = (AttributeInfo) new LocalVariableTableAttribute(attributeIndex,_codeLength);

                else
                    attr = (AttributeInfo) new GenericAttribute(attributeIndex);


                if (attr.Load(instream,pool) == false)
                {
                    System.err.println("Attribute load failure occurred");
                    return false;
                }

                _attributes[k] = attr;
            } // for
        } // if

        return true;
    } // LoadAttributes

    /** Output all information about this attribute */
    public boolean print()
    {
        System.out.println("====================================================================");
        System.out.println("              CODE ATTRIBUTE : ");
        System.out.println("                       Max stack entries        = " + _maxStack);
        System.out.println("                       Max number of local vars = " + _maxLocals);
        System.out.println("                       Code length              = " + _codeLength);
        System.out.println("                       Exception table length   = " + _exceptionTableLength);

        for (int k=0; k<_exceptionTableLength; k++)
            _exceptionTable[k].print();

        System.out.println("                       Number of attributes     = " + _attributeCount);

        for (int k=0; k<_attributeCount; k++)
            _attributes[k].print();

        System.out.println("====================================================================");

        return true;

    } // print

	/**
	 * returns an XML representation of the object
	 */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<codeattribute>");

		buf.append ("<maxstackentries>").append (_maxStack).append ("</maxstackentries>");
		buf.append ("<maxlocalvars>").append (_maxLocals).append ("</maxlocalvars>");
		buf.append ("<codelength>").append (_codeLength).append ("</codelength>");
		for (int i = 0; i < _exceptionTableLength; i++)
		{
			buf.append (_exceptionTable [i]);
		}

		for (int i = 0; i < _attributeCount; i++)
		{
			buf.append (_attributes [i]);
		}

		buf.append ("</codeattribute>");

		return buf.toString ();
	}
} // CodeAttribute
