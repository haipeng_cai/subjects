/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import jaba.classfile.ClassFile;
import java.util.HashMap;

/**
 * Used as an element of an array that is returned from the SymbolTable.load()
 * method.  Provides a pairing of a 
 * {@link jaba.classfile.ClassFile ClassFile} and a mapping of 
 * constant pool indeces to 
 * {@link jaba.sym.SymbolTableEntry symbol table entries}.
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 */
public class ClassFileConstantPoolMapPair implements java.io.Serializable
{
  /** Class file for which the mapping is provided. */
  public ClassFile classfile;

  /** 
   * Mapping from constant pool indeces to 
   * {@link jaba.sym.SymbolTableEntry symbol table entries}.
   */
  public HashMap constantPoolMap;

}
