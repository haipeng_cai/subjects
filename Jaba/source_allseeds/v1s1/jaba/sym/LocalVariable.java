/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import jaba.du.HasValue;
import jaba.constants.Modifier;

/**
 * Class that represents a local variable entry into the symbol table.
 * @author Jay Lofstead 2003/05/22 fixed a possible null pointer exception in toString if type was null
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/30 added implements LocalVariable
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface LocalVariable extends SymbolEntry, HasValue
{
    /**
     * Sets the type for this variable.
     * @param t Type of variable.
     */
    public void setType( TypeEntry t );

    /**
     * Sets the containing method for this variable.
     * @param m Containing method.
     */
    public void setContainingMethod( Method m );

    /**
     * Sets the modifier flags for this variable.
     * @param m Bit-mask for modofier flags for this variable.
     */
    public void setModifiers( int m );

    /**
     * Returns the type method for this variable..
     * @return Type of this variable..
     */
    public TypeEntry getType();

    /**
     * Returns the containing method for this variable..
     * @return Containing method of this variable..
     */
    public Method getContainingMethod();

    /**
     * Returns the bit-mask for the modifiers for this variable.
     * @return Modifiers for this variable.
     */
    public int getModifiers();

  /**
   * Returns a string for this object.
   * @return  String representing this object.
   */
	public String toString ();

  /**
   * Clones this object.
   * @return Clone of this object.
   */
  public Object clone();
}
