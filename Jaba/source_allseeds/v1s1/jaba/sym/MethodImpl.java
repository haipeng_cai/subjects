/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import jaba.graph.StatementNode;

import jaba.classfile.MethodInfo;
import jaba.classfile.ExceptionsAttribute;
import jaba.classfile.AttributeInfo;

import jaba.tools.local.Factory;

import java.util.Collection;
import java.util.Vector;
import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;

/**
 * Class that represents a method member.
 * @author Huaxing Wu -- <i>Revised 5/16/02. Fixing problems caused by 
 * transfering info back and forward between vector and array. Change to use 
 * vector only. </i>
 * @author Huaxing Wu -- <i>Revised 7/11/02. Several get methods's info are 
 * built when CFG built, so if the call on these methods are before CFG built,
 * these method will return wrong info (empty). The problem is fixed by 
 * building CFG first when these methods are used. These methods are: 
 * get..Methods, get..Noeds(). Totally 8. </i> 
 * @author Huaxing Wu -- <i> Revised 7/17/02. Previous fixing of those get 
 * methods which are depending on CFG built does not work. Because inside of 
 * CFG build,it first gether some information, and use set to add to put it in
 * the class, then later call get method to get them. So if the get method 
 * find the CFG is not built and try to build it, it may run into infinate
 * loop or some other problems. Now it fixed by having a boolean to indicate
 * whether an attribute is set or not. </i>
 * @author Huaxing Wu -- <i> Revised 11/20/02. Add method getSignatureString. </i>
 * @author Jay Lofstead 2003/05/22 added isStatic method.
 * 					fixed toString to work for a symbol table in progress
 * 					(CFG can't be created properly until it is done).
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/02 converted elementAt to enumeration for better performance.
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/06/30 added implements Method
 * @author Jay Lofstead 2003/07/01 changed to implement Method
 * @author Jay Lofstead 2003/07/17 changed to use the Factory for attribute retrieval.
 */
public class MethodImpl extends MemberImpl implements ContainsInnerNamedReferenceTypes, Method
{    
    /**
     * Creates a new method object. Invokes the constructor for Member to
     * initialize inherited fields.
     */
    public MethodImpl ()
    {
        formalParameters = new Vector();
	formalParameterTypes = new Vector();
        innerNamedReferenceTypes = new Vector();
        exceptions = new Vector();
	localVariables = new Vector();
	attributes = new Vector();
	calledMethods = new Vector();
	callSiteNodes = new Vector();
	finallyCallSiteNodes = new Vector();
	callingMethods = new Vector();
        finallyMethods = new Vector();
        containingMethod = null;
        nextNodeID = 3;  // entry node = 1, exit node = 2
    }
    
    /**
     * Creates a new method object. Invokes the constructor for Member to
     * initialize inherited fields.
     */
    public MethodImpl (NamedReferenceType namedRef, MethodInfo methodInfo)
    {
	super ();
        formalParameters = new Vector();
	formalParameterTypes = new Vector();
        innerNamedReferenceTypes = new Vector();
        exceptions = new Vector();
	localVariables = new Vector();
	attributes = new Vector();
	calledMethods = new Vector();
	callSiteNodes = new Vector();
	finallyCallSiteNodes = new Vector();
	callingMethods = new Vector();
        finallyMethods = new Vector();
        containingMethod = null;
        nextNodeID = 3;  // entry node = 1, exit node = 2

	// add method info as attributes of the method
	if(!namedRef.isSummarized())
	    addAttribute(methodInfo);

	// set the named reference to the method
	setContainingType( namedRef );

	// set the access level
	int accessFlags = methodInfo.getAccessFlags();
	if ( ( accessFlags & A_PUBLIC ) != 0 )
	    setAccessLevel( A_PUBLIC );
	else if ( ( accessFlags & A_PRIVATE ) != 0 )
	    setAccessLevel( A_PRIVATE );
	else if ( ( accessFlags & A_PROTECTED ) != 0 )
	    setAccessLevel( A_PROTECTED );
	else setAccessLevel( A_PACKAGE );
	
	// set the method's modifiers
	int modifiers = 0;
	if ( ( accessFlags & M_ABSTRACT ) != 0 ) modifiers |= M_ABSTRACT;
	if ( ( accessFlags & M_FINAL ) != 0 ) modifiers |= M_FINAL;
	if ( ( accessFlags & M_NATIVE ) != 0 ) modifiers |= M_NATIVE;
	if ( ( accessFlags & M_STATIC ) != 0 ) modifiers |= M_STATIC;
	if ( ( accessFlags & M_SYNCHRONIZED ) != 0 ) 
	    modifiers |= M_SYNCHRONIZED;
	
	setModifiers( modifiers );
	
	// set the method's name and descriptor
	setName( methodInfo.getNameString() );
	setDescriptor( methodInfo.getDescriptorString() );
	setSignature( getSignatureString(methodInfo.getNameString(),
		      methodInfo.getDescriptorString()) );
	setSynthetic(methodInfo.isSynthetic());

	// Get the exception names
	ExceptionsAttribute excAttribute = null;
	for ( int j = 0; j < methodInfo.getAttributeCount(); j++ ) {
	    AttributeInfo attribute = methodInfo.getAttribute( j );
	    if ( attribute instanceof ExceptionsAttribute ) {
		// we need to register all exceptions that may be thrown by
		// by this method in the method object
		excAttribute = (ExceptionsAttribute)attribute;
	    }
	}
	
	if(excAttribute != null) {
	    // loop through each exception that may be thrown
	    int excpNum = excAttribute.getNumberOfExceptions();
	    exceptionNames = new String[excpNum];
	    for ( int k = 0; k < excpNum; k++ ) {
		// create an exception class for temporary purposes --
		// it merely holds the name of the exception
		String name = 
		    excAttribute.getExceptionString( k );
		exceptionNames[k] = name;
	    }
	}
    }
    
	/** ??? */
    public boolean isConstructor() {
	return getName().equals(CONSTRUCTOR);
    }

	/** returns if this method is static */
	public boolean isStatic ()
	{
		return (getModifiers () & M_STATIC) != 0 ? true : false;
	}
    // Methods
    
    /**
     * Adds a formal parameter to the set of formal parameters for the method.
     * @param f Formal parameter to be added.
     */
    public void addFormalParameter( LocalVariable f ) {
        formalParameters.add( f );
    }
    
    /**
     * Adds a parameter type to the set of formal parameter types for the
     * method.
     * @param t Formal parameter type to be added.
     */
    public void addFormalParameterType( TypeEntry t ) {
        formalParameterTypes.add( t );
    }
    
    /**
     * Adds an inner class or interface (a named reference type) to the set
     * of inner named reference types contained within the method.
     * @param c Inner class or interface to be added.
     */
    public void addInnerNamedReferenceType( NamedReferenceType c ) {
        innerNamedReferenceTypes.add( c );
    }
    
    /**
     * Adds an exception to the set of exceptions that may be raised by
     * the method.
     * @param e Exception to be added.
     */
    public void addException( Class e ) {
        exceptions.add( e );
    }
    
    /**
     * Adds a local variable to the set of variables that are declared in this
     * method.
     * @param v variable to be added.
     */
    public void addLocalVariable( LocalVariable v ) {
        localVariables.add( v );
    }
    
    /**
     * Adds a method that is called by this method -- this does not include
     * finally methods.
     * @param method  Method that is called by this method.
     */
    public void addCalledMethod( Method calledMethod )
    {
	if ( calledMethods.contains( calledMethod ) )
	    return;
	calledMethods.add( calledMethod );
    }
    
    /**
     * Adds a call site StatementNode -- not including finally call sites.
     * @param callSiteNode  StatementNode that contains a call to a method.
     */
    public void addCallSiteNode( StatementNode callSiteNode )
    {
	if ( callSiteNodes.contains( callSiteNode ) )
	    return;
	callSiteNodes.add( callSiteNode );
    }
    
    /**
     * Adds a call site StatementNode that calls a finally method.
     * @param callSiteNode  StatementNode that contains a call to a finally
     *                      method.
     */
    public void addFinallyCallSiteNode( StatementNode callSiteNode )
    {
	if ( finallyCallSiteNodes.contains( callSiteNode ) )
	    return;
	finallyCallSiteNodes.add( callSiteNode );
    }
    
    /**
     * Adds a method that calls this method -- this does not include finally 
     * methods.
     * @param callingMethod  Method that calls this method.
     */
    public void addCallingMethod( Method callingMethod )
    {
	if ( callingMethods.contains( callingMethod ) ) {
	    return;
	}
	callingMethods.add( callingMethod );
    }
    
    /**
     * Adds an attribute to this method.
     * @param attribute  The attribute to be added to this method.
     * @throws IllegalArgumentException if <code>attribute</code> is
     *                                  <code>null</code>.
     */
    public void addAttribute( MethodAttribute attribute )
	throws IllegalArgumentException
    {
        if ( attribute == null ) {
            throw new IllegalArgumentException( "Method.addAttribute(): attribute is null" );
        }
        attributes.add( attribute );
    }
    
    /**
     * Adds an inner (finally) method to this method.
     * @param method  Finally method contained within this method.
     */
    public void addFinallyMethod( Method method )
    {
	if ( method == null )
	    throw new IllegalArgumentException( "Method.addFinallyMethod(): " +
						"method is null." );
	finallyMethods.add( method );
    }
    
    /**
     * Assigns a parent method.  Calling this method implies that "this" method
     * is a finally method.
     * @param method  Parent method.
     */
    public void setContainingMethod( Method method )
    {
	containingMethod = method;
    }
    
	/** ??? */
    private void resolveType() {	
	//Resolve the return type of this method
	String desc = getDescriptor();
	MethodDescriptor methodDescriptor = 
	    new MethodDescriptor(desc);
	Program program = (jaba.sym.Program) getContainingType ().getProgram ();
	SymbolTable symbolTable = (jaba.sym.SymbolTable) program.getSymbolTable();
	type = symbolTable.forType(methodDescriptor.getReturnType());

	//Resolve the parameters' type
	TypeEntry[] paramTypes = methodDescriptor.getParamTypes();
	// if this method is not static, then its first parameter is
	// the 'this' reference
	if ( ( getModifiers() & M_STATIC ) == 0 ) {
	    addFormalParameterType (getContainingType ());
	}
	for ( int k = 0; k < paramTypes.length; k++ ) {
	    TypeEntry param = symbolTable.forType( paramTypes[k]); 
	    addFormalParameterType(param);
	}

	//Resolve the Exceptions' type
	if(exceptionNames != null) {
	    for(int k = 0; k <exceptionNames.length; k++) {
		Class excep = (Class)symbolTable.forNamedReferenceType
		    ( exceptionNames[k]); 
		addException(excep);
	    }
	}
    }

	/** ??? */
    public TypeEntry getType() {
    	if(type == null) {
	    resolveType();
	}
        return type;
    }

    /**
     * Returns Formal parameters for the method.
     * @return Formal parameters.
     */
	public Collection getFormalParametersCollection ()
	{
		return formalParameters;
	}
    

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getFormalParametersCollection</code>
     * @return Formal parameters.
     */
	public LocalVariable [] getFormalParameters ()
	{
		return (LocalVariable []) formalParameters.toArray (new LocalVariable [formalParameters.size ()]);
	}
    
    /**
     * Returns formal parameter types for the method.
     * @return Formal parameter types.
     */
	public Collection getFormalParameterTypesCollection ()
	{
		if (type == null)
		{
			resolveType();
		}

		return formalParameterTypes;
	}
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getFormalParameterTypesCollection</code>
     * @return Formal parameter types.
     */
	public TypeEntry [] getFormalParameterTypes ()
	{
		if (type == null)
		{
			resolveType();
		}

		return (TypeEntry []) formalParameterTypes.toArray (new TypeEntry [formalParameterTypes.size ()]);
	}
    
    /**
     * Returns named reference types contained within
     * the method.
     * @return Contained named reference types.
     */
	public Collection getInnerNamedReferenceTypesCollection ()
	{
		return innerNamedReferenceTypes;
	}
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getInnerNamedReferneceTypesCollection</code>
     * @return Contained named reference types.
     */
	public NamedReferenceType [] getInnerNamedReferenceTypes()
	{
		return (NamedReferenceType []) innerNamedReferenceTypes.toArray (new NamedReferenceType [innerNamedReferenceTypes.size ()]);
	}
    
    /**
     * Returns exception types that may be raised by the
     * method.
     * @return Exceptions.
     */
	public Collection getExceptionsCollection ()
	{
		if(type == null)
		{
			resolveType();
			//After resolving the types, exceptionNames is no longer needed
			exceptionNames = null;
		}

		return exceptions;
	}
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getExceptionsVector</code>
     * @return Exceptions.
     */
	public Class [] getExceptions ()
	{
		if(type == null)
		{
			resolveType();
			//After resolving the types, exceptionNames is no longer needed
			exceptionNames = null;
		}

		return (Class []) exceptions.toArray (new Class [exceptions.size ()]);
	}
    
    /**
     * Returns local variables declared within this method.
     * @return Local variables.
     */
	public Collection getLocalVariablesCollection ()
	{
		return localVariables;
	}

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getLocalVariablesVector</code>
     * @return Local variables.
     */
	public LocalVariable [] getLocalVariables ()
	{
		return (LocalVariable []) localVariables.toArray (new LocalVariable [localVariables.size ()]);
	}

    /**
     * Returns number of formal parameters for the method.
     * @return number of formal parameters.
     */
    public int getFormalParameterCount () {
        return formalParameterTypes.size();
    }

    /**
     * Returns all methods called from this one -- not including finally 
     * methods.
     * @return Called Methods.
     */
	public Collection getCalledMethodsCollection ()
	{
		// To ensure CFG is built. As finally info is collected as CFG built.
		//  If  CFG is already there, it will cause no harm
		Factory.getCFG (this);

		return calledMethods;
    }
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getCalledMethodsCollection</code>
     * @return Called Methods.
     */
	public Method [] getCalledMethods ()
	{
		// To ensure CFG is built. As finally info is collected as CFG built.
		//  If  CFG is already there, it will cause no harm
		Factory.getCFG (this);

		return (Method []) calledMethods.toArray (new Method [calledMethods.size ()]);
    }
    
    /**
     * Returns all call sites in this method -- not including finally call 
     * sites.
     * @return Call sites.
     */
	public Collection getCallSiteNodesCollection ()
	{
		// To ensure CFG is built. As finally info is collected as CFG built.
		// If  CFG is already there, it will cause no harm
		Factory.getCFG (this);

		return callSiteNodes;
	}
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getCallSiteNodesCollection</code>
     * @return Call sites.
     */
	public StatementNode [] getCallSiteNodes ()
	{
		// To ensure CFG is built. As finally info is collected as CFG built.
		// If  CFG is already there, it will cause no harm
		Factory.getCFG (this);

		return (StatementNode []) callSiteNodes.toArray (new StatementNode [callSiteNodes.size ()]);
	}

    /**
     * Returns all call sites that call finally methods.
     * @return Finally call sites.
     */
	public Collection getFinallyCallSiteNodesCollection ()
	{
		// To ensure CFG is built. As finally info is collected as CFG built.
		// If  CFG is already there, it will cause no harm
		Factory.getCFG (this);

		return finallyCallSiteNodes;
	}
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getFinallyCallSiteNodesCollection</code>
     * @return Finally call sites.
     */
	public StatementNode [] getFinallyCallSiteNodes ()
	{
		// To ensure CFG is built. As finally info is collected as CFG built.
		// If  CFG is already there, it will cause no harm
		Factory.getCFG (this);

		return (StatementNode []) finallyCallSiteNodes.toArray (new StatementNode [finallyCallSiteNodes.size ()]);
	}
    
    /**
     * Returns all methods calling this one -- not including finally methods.
     * @return Calling methods.
     */
	public Collection getCallingMethodsCollection ()
	{
		// To ensure CFG is built. As finally info is collected as CFG built.
		// If  CFG is already there, it will cause no harm
		Factory.getCFG (this);

		return callingMethods;
	}
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getCallingMethodsCollection</code>
     * @return Calling methods.
     */
	public Method [] getCallingMethods ()
	{
		// To ensure CFG is built. As finally info is collected as CFG built.
		// If  CFG is already there, it will cause no harm
		Factory.getCFG (this);

		return (Method []) callingMethods.toArray (new Method [callingMethods.size ()]);
	}
    
    /**
     * Returns all finally methods immediately contained within this method.
     * @return All finally methods immediately contained within this method.
     */
    public Collection getFinallyMethodsCollection ()
    {
	// To ensure CFG is built. As finally info is collected as CFG built.
	// If  CFG is already there, it will cause no harm
	Factory.getCFG (this);
	return finallyMethods;
    }
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getFinallyMethodsCollection</code>
     * @return All finally methods immediately contained within this method.
     */
    public Method [] getFinallyMethods()
    {
	// To ensure CFG is built. As finally info is collected as CFG built.
	// If  CFG is already there, it will cause no harm
	Factory.getCFG (this);

	return (Method []) finallyMethods.toArray (new Method [finallyMethods.size ()]);
    }
    
    /**
     * Returns all finally methods directly and indirectly contained within
     * this method (returns all recursively contained methods).
     * @return All finally methods directly and indirectly contained within
     *         this method.
     */
	public Collection getTransitiveFinallyMethodsCollection ()
	{
		LinkedList ll = new LinkedList ();
		getTransitiveFinallyMethods (ll);

		return ll;
	}

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getTransitiveFinallyMethodsCollection</code>
     * @return All finally methods directly and indirectly contained within
     *         this method.
     */
	public Method [] getTransitiveFinallyMethods ()
	{
		LinkedList ll = new LinkedList ();
		getTransitiveFinallyMethods (ll);

		return (Method []) ll.toArray (new Method [ll.size ()]);
	}

    /**
     * Returns all finally methods directly and indirectly contained within
     * this method.
     * @param ll  Expects an instantiation of a LinkedList, will populate it
     *            with all finally methods directly and indirectly contained
     *            within this method.
     */
    private void getTransitiveFinallyMethods( LinkedList ll )
    {
	// To ensure CFG is built. As finally info is collected as CFG built.
	// If  CFG is already there, it will cause no harm
	Factory.getCFG (this);

	Collection immediateFinallyMethods = getFinallyMethodsCollection ();
	for (Iterator i = immediateFinallyMethods.iterator (); i.hasNext ();)
	    {
		MethodImpl m = (MethodImpl) i.next ();

		m.getTransitiveFinallyMethods( ll );
		ll.addLast (m);
	    }
    }

    /**
     * Returns the parent method of this method.  If this is not a finally 
     * method, <code>null</code> is returned.
     * @return The parent method of this method.  If this is not a finally 
     *         method, <code>null</code> is returned.
     */
    public Method getContainingMethod ()
    {
	return containingMethod;
    }
    
    /**
     * Returns the top-level parent method of this method.  If this is not a
     * finally method, <code>this</code> reference is returned. 
     * @return The top-level parent method of this method.  If this is not a
     *         finally method, <code>this</code> reference is returned.
     */
    public Method getTopLevelContainingMethod ()
    {
	if ( containingMethod == null ) return this;
	return containingMethod.getTopLevelContainingMethod();
    }

    /**
     * Returns the name of the method qualified with all containing methods
     * and the containing type.
     * @return Dot-separated names of this method, all containing methods,
     *         and the containing type of the top-level containing method.
     */
    public String getFullyQualifiedName()
    {
	if ( containingMethod == null ) {
	    return getContainingType().getName()+"."+getName();
	}
	return containingMethod.getFullyQualifiedName()+"."+getName();
    }
    
    /**
     * Returns an array of attributes of this method.
     * @return All attributes defined for this method.
     */
    public MethodAttribute [] getAttributes()
    {
        MethodAttribute[] m = new MethodAttribute[attributes.size()];
	int i = 0;
        for (Iterator e = attributes.iterator (); e.hasNext ();i++)
	{
            m [i] = (MethodAttribute) e.next ();
        }
        return m;
    }
    
    /**
     * Returns the attribute of the specified type that may be associated
     * with a method.
     * @param type type of method attribute.
     * @return Method attribute of the specified type, or
     *         <code>null</code> if method has no attribute of the specified
     *         type.
     */
    public MethodAttribute getAttributeOfType( String typeStr )
    {
	// update to the new implementation names
	if (typeStr.equals ("jaba.graph.cfg.CFG"))
	{
		typeStr = "jaba.graph.cfg.CFGImpl";
	}
	if (typeStr.equals ("jaba.graph.acfg.ACFG"))
	{
		typeStr = "jaba.graph.acfg.ACFGImpl";
	}
	if (typeStr.equals ("jaba.tools.graph.GDG"))
	{
		typeStr = "jaba.tools.graph.GDGImpl";
	}
	if (typeStr.equals ("jaba.classfile.MethodInfo"))
	{
		typeStr = "jaba.classfile.MethodInfoImpl";
	}
	if (typeStr.equals ("jaba.graph.cfg.ExceptionHandlers"))
	{
		typeStr = "jaba.graph.cfg.ExceptionHandlersImpl";
	}

	java.lang.Class type = null;
	try {
	    type = java.lang.Class.forName (typeStr);
	}catch ( ClassNotFoundException e){
	    e.printStackTrace();
	    throw new RuntimeException( e );
	}

	for (Iterator e = attributes.iterator (); e.hasNext ();)
	{
		MethodAttribute ma = (MethodAttribute) e.next ();
	    java.lang.Class storedAttrClass = ma.getClass();
	    if ( type.isAssignableFrom( storedAttrClass ) )
	    {
		return ma;
	    }
	}
	
	// attribute not found... now check if it is a DemandDrivenAttribute.
	// If so, create it and initialize it.
	if (typeStr.equals ("jaba.graph.cfg.CFGImpl"))
	{
	    return(jaba.graph.cfg.CFGImpl.load(this));
	}
	if (typeStr.equals ("jaba.graph.acfg.ACFGImpl"))
	{
	    return(jaba.graph.acfg.ACFGImpl.load(this));
	}
	System.err.println("JIMJONES: MethodImpl: typeStr = "+typeStr);
	try
	{
	    java.lang.Class dda = java.lang.Class.forName ("jaba.sym.DemandDrivenAttribute");
	    if (dda.isAssignableFrom (type))
	    {
		DemandDrivenAttribute attr = null;
		if ( getContainingType().isSummarized() ) return null;

		MethodAttribute ma = null;
		try {
		    java.lang.Class [] cc = new java.lang.Class [1];
		    Object [] oo = new Object [1];
		    cc [0] = java.lang.Class.forName ("jaba.sym.Method");
		    java.lang.reflect.Method m = type.getMethod ("load", cc);
		    oo [0] = this;
		    ma = (MethodAttribute) m.invoke (null, oo);
		}catch(java.lang.NoSuchMethodException e){
		    e.printStackTrace();
		    throw new RuntimeException( e );
		}
		catch(java.lang.IllegalAccessException e){
		    e.printStackTrace();
		    throw new RuntimeException( e );
		}
		catch ( java.lang.reflect.InvocationTargetException e3){
			// instead of printing out the exception, pass it along
		    //e3.printStackTrace();
		    throw new RuntimeException( e3 );
		}

		return ma;
	    }
	}
	catch (ClassNotFoundException e)
	{
		e.printStackTrace ();
		throw new RuntimeException (e);
	}

        return null;
    }
    
    
    /**
     * Removes the attribute of the specified type that may be associated
     * with a method.
     * @param typeStr type of method attribute. Must be a
     *             subtype of <code>MethodAttribute</code>.
     * @return <code>true</code> if method contained an
     *         attribute of the specified type, or
     *         <code>false</code> if method has no attribute of
     *         the specified type.
     * @throws IllegalArgumentException if <code>type</code> is not a subtype
     *                              of <code>MethodAttribute</code>.
     */
    public boolean removeAttributeOfType( String typeStr ) throws IllegalArgumentException
    {
	java.lang.Class type = null;
	try {
	    type = java.lang.Class.forName( typeStr );
	}catch ( ClassNotFoundException e){
	    e.printStackTrace();
	    throw new RuntimeException( e );
	}
	
	try
	{
            java.lang.Class c = java.lang.Class.forName ("jaba.sym.MethodAttribute");
            if ( !c.isAssignableFrom( type ) ) {
                throw new IllegalArgumentException( "Method.removeAttributeOfType(): "
						    + type.getName ()
						    + " is not a subtype of MethodAttribute"
						);
            }
	    int i = 0;
            for (Iterator e = attributes.iterator (); e.hasNext (); i++)
	    {
		MethodAttribute ma = (MethodAttribute) e.next ();
                java.lang.Class storedAttrClass = ma.getClass();
                if ( type.isAssignableFrom( storedAttrClass ) )
		{
                    attributes.remove (i);
                    return true;
                }
            }
	}
	catch (ClassNotFoundException e)
	{
		e.printStackTrace ();
		throw new RuntimeException (e);
	}

        return false;
    }

	/** ??? */
    public boolean isMainMethod() {
	return (((getModifiers() & M_STATIC) != 0)  &&
		(getAccessLevel() == A_PUBLIC) &&
		getDescriptor().equals( "([Ljava/lang/String;)V" ) && 
		getName().equals( "main" ) );
    } 
	 
    /**
     * Returns a string for this object.
     * @return  String representing this object.
     */
	public String toString ()
	{
		StringBuffer buffer = new StringBuffer ();

		buffer.append ("<method>");

		buffer.append (super.toString ());

		buffer.append ("<id>" + getID () + "</id>");
		buffer.append ("<name>" + jaba.tools.Util.toXMLValid (getName ()) + "</name>");
		buffer.append ("<signature>" + jaba.tools.Util.toXMLValid (getSignature ()) + "</signature>");

		int i = 0;
		for (Iterator e = formalParameters.iterator (); e.hasNext (); i++)
		{
			buffer.append ("<formalparameter id=\"" + i + "\">");
			buffer.append (((LocalVariable) e.next ()).getID ());
			buffer.append ("</formalparameter>");
		}

		for (Iterator e = formalParameterTypes.iterator (); e.hasNext ();)
		{
			buffer.append ("<formalparametertype>");
			buffer.append (((TypeEntry) e.next ()).getID ());
			buffer.append ("</formalparametertype>");
		}

		for (Iterator e = innerNamedReferenceTypes.iterator (); e.hasNext ();)
		{
			buffer.append ("<innernamedreferencetype>");
			buffer.append (((NamedReferenceType) e.next ()).getID ());
			buffer.append ("</innernamedreferencetype>");
		}

		for (Iterator e = exceptions.iterator (); e.hasNext ();)
		{
			buffer.append ("<exception>");
			buffer.append (((Class) e.next ()).getID ());
			buffer.append ("</exception>");
		}

		for (Iterator e = localVariables.iterator (); e.hasNext ();)
		{
			buffer.append ("<localvariable>");
			buffer.append (((LocalVariable) e.next ()).getID ());
			buffer.append ("</localvariable>");
		}

		try
		{
			for (Iterator e = calledMethods.iterator (); e.hasNext ();)
			{
				buffer.append ("<calledmethod>");
				buffer.append (((Method) e.next ()).getID ());
				buffer.append ("<calledmethod>");
			}
		}
		catch (Exception e)
		{
			//buffer.append ("method not completely analyzed yet.");
		}

		try
		{
			for (Iterator e = callingMethods.iterator (); e.hasNext ();)
			{
				buffer.append ("<callingmethod>");
				buffer.append (((Method) e.next ()).getID ());
				buffer.append ("</callingmethod>");
			}
		}
		catch (Exception e)
		{
			//buffer.append ("method not completely analyzed yet.");
		}

		buffer.append ("</method>");

		return buffer.toString ();
	}

    /**
     * Create a Signature string out of a name and a descriptor of a method
     * based on the rules.
     * @param name -- the name of a method.
     * @param descriptor -- the descriptor of a method
     * @return the signature string represented by the input
     */
    public static String getSignatureString(String name, String descriptor){
	int index = descriptor.indexOf(")");
	return name + descriptor.substring(0,index+1);
    }    

    // Fields

    /** The name of a Constructor */
    private static String CONSTRUCTOR = "<init>";
   
    /**
     * Formal parameters of the method. A formal parameter is an object
     * of type {@link jaba.sym.LocalVariable LocalVariable}.
     * If this is an instance 
     * method, and requires an object to invoke on, then this object
     * will be the first element in the type vector. 
     * e.g.,: a.m(b,c), vector = [a,b,c]
     */
    private Vector formalParameters;
    
    /**
     * Formal parameter types of the method. A parameter type is an object
     * of type {@link jaba.sym.TypeEntry TypeEntry}. If this is an instance 
     * method, and requires an object to invoke on, then this object's type
     * will be the first element in the type vector. 
     * e.g.,: A s; s.m(int, int). Then this vector = [A, int, int]
     */
    private Vector formalParameterTypes;
    
    /**
     * Inner classes and interfaces (named reference types) contained
     * within the method.
     */
    private Vector innerNamedReferenceTypes;
   
    /** Exceptions that may be raised by the method. */
    private Vector exceptions;
    
    /** Local variables declared in the method. */
    private Vector localVariables;

    /** Methods that this method calls. */
    private Vector calledMethods;

    /** StatementNodes that contain call sites to methods. */
    private Vector callSiteNodes;

    /** StatementNodes that contain call sites to finally methods. */
    private Vector finallyCallSiteNodes;
   
    /** Methods that call this method. */
    private Vector callingMethods;

    /** Extra attributes to be associated with a method. */
    private Vector attributes;
    
    /** Finally methods contained within this one. */
    private Vector finallyMethods;

    /** Parent method if this this a finally method (otherwise null). */
    private Method containingMethod;
    
    /** Node ID generator for statement node IDs. */
    private int nextNodeID;
    
    /** Node ID of entry node. */
    private static final int ENTRY_NODE_ID = 1;
    
    /** Node ID of exit node. */
    private static final int EXIT_NODE_ID = 2;

    /** the names of exceptions this method can throw */
    private String[] exceptionNames = null;
}
