// Copyright (c) 1999, The Ohio State University

package jaba.sym;

import jaba.main.ResourceFileI;

import jaba.main.Options;

import java.util.Vector;
import java.util.Enumeration;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import java.net.URL;
import java.net.URLClassLoader;

import java.io.File;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

/**
 * Represents the application being analyzed.
 * @author S. Sinha -- <i>Created.</i>
 * @author Jim Jones -- <i>Revised.  April 1, 1999,  Moved code performing 
 *                      initial analysis from the "main" method to
 *                      Program.load.</i>
 * @author Huaxing Wu -- <i>Revised. 3/21/02. Change load() to use new ResourceFile obejct, also add calling RLVT part.
 * @author Huaxing Wu -- <i>Revised 10/30/02. Remove RLVT part to SymboTable, 
 *                        as it's responsible for the program loading.</i>
 * @author Huaxing Wu -- <i>Revised 10/31/02. Provide access to Packages by
 *                       adding getPackages() and getPackage(String name).</i> 
 * @author Jay Lofstead 2003-05-22 removed the commented out code to aid in readability.
 * @author Jay Lofstead 2003/06/05 added a toString method
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/16 updated toString to include class and interface names
 * @author Jay Lofstead 2003/06/20 added a private Options object, added second constructor, added init method.
 * @author Jay Lofstead 2003/06/23 added implement of Program
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface Program extends SymbolTableEntry
{
	/**
	 * Returns the options associated with this Program object
	 */
	public Options getOptions ();

    /**
     * Adds a named reference type to the program.
     * @param namedRefType named reference type to be added.
     * @throws IllegalArgumentException if <code>namedRefType</code> is
     *                                  <code>null</code>.
     */
    public void addNamedReferenceType( NamedReferenceType namedRefType )
        throws IllegalArgumentException;

    /** Find the namedreferencetype for the given class/interface name */
    public NamedReferenceType findNamedReferenceType (String name);

    /**
     * Returns all to-be-analyzed(i.e: subject) Classes and Interfaces in this program.
     * @return Array of all to-be-analyzedClasses and Interfaces in program.
     */
    public NamedReferenceType [] getNamedReferenceTypes ();

    /**
     * Returns all Classes and Interfaces defined in this program.
     * @return Array of all Classes and Interfaces defined in program.
     */
	public Collection getLoadedNamedReferenceTypesCollection ();

    /**
     * Convenience method for converting the Collection to an Array (less efficient) from <code>getLoadedNamedReferenceTypesCollection</code>
     * @return Array of all Classes and Interfaces defined in program.
     */
	public NamedReferenceType [] getLoadedNamedReferenceTypes ();

  /**
   * Returns all Classes loaded in this program, including library classes and
   * suject classes (to-be-analyzed).
   * @return  Array of all Classes loaded in this program.
   */
	public Collection getLoadedClassesCollection ();

  /**
   * Convenience method for converting the Collection to an Array (less efficient) from <code>getLoadedClassesCollection</code>
   * @return  Array of all Classes loaded in this program.
   */
	public Class [] getLoadedClasses ();

  /**
   * Returns all to-be-analyzed Classes defined in this program. These
   * to-be-analyzed classes are usually specified in a ResourceFile.
   * @return  Array of all Classes defined in this program.
   */
	public Collection getClassesCollection ();

    /**
     * Convenience method for converting the Collection to an Array (less efficient) from <code>getClassesCollection</code>
     * @return  Array of all Classes defined in this program.
     */
	public Class [] getClasses ();

  /**
   * Returns all loaded Interfaces in this program, including library
   * interfaces and to-be-analyzed interfaces. to-be-analyzed interfaces
   * are typically specified in a ResourceFile.
   * @return  Array of all Interfaces defined in this program.
   */
	public Collection getLoadedInterfacesCollection ();

  /**
   * Convenience method for converting the Collection to an Array (less efficient) from <code>getLoadedInterfacesCollection</code>
   * @return  Array of all Interfaces defined in this program.
   */
	public Interface [] getLoadedInterfaces ();

  /**
   * Returns all to-be-analyzed Interfaces in this program. These
   * to-be-analyzed interfaces are usally specified in a ResourceFile.
   * @return  Array of all to-be-analyzed Interfaces in this program.
   */
	public Collection getInterfacesCollection ();

  /**
   * Convenience method for converting the Collection to an Array (less efficient) from <code>getInterfacesCollection</code>
   * @return  Array of all to-be-analyzed Interfaces in this program.
   */
	public Interface [] getInterfaces ();

  /**
   * Returns all "public static void main( String[] )" methods in all of the
   * to-be-analyzed Classes in this Program.
   * @return  An array of "public static void main( String[] )" methods.
   */
	public Collection getMainMethodsCollection ();

  /**
   * Convenience method for converting the Collection to an Array (less efficient) from <code>getMainMethodsCollection</code>
   * @return  An array of "public static void main( String[] )" methods.
   */
	public Method [] getMainMethods ();

  /**
   * Returns all "public static void main( String[] )" methods in all the
   * loaded Classes in this Program.
   * @return  An array of "public static void main( String[] )" methods.
   */
	public Collection getLoadedMainMethodsCollection ();

  /**
   * Convenience method for converting the Collection to an Array (less efficient) from <code>getLoadedMainMethodsCollection</code>
   * @return  An array of "public static void main( String[] )" methods.
   */
	public Method [] getLoadedMainMethods ();

    /**
     * Get all the packages in this program.
     * @return an array of packages
     */
	public Collection getPackagesCollection ();

    /**
     * Convenience method for converting the Collection to an Array (less efficient) from <code>getPackagesCollection</code>
     * @return an array of packages
     */
	public Package [] getPackages ();

    /** Get the package object for the specified package name.
     * @param name - the Package name
     * @return the package object for the name,null if the package not found.
     */
    public Package getPackage(String name);

    /**
     * Adds an attribute to this program.
     * @param attribute  The attribute to be added to this program.
     * @throws IllegalArgumentException if <code>attribute</code> is
     *                                  <code>null</code>.
     */
    public void addAttribute( ProgramAttribute attribute )
                throws IllegalArgumentException;

    /**
     * Returns the attribute of the specified type that may be associated
     * with a program.
     * @param type type of program attribute.
     * @return Program attribute of the specified type, or
     *         <code>null</code> if program has no attribute of the specified
     *         type.
     */
    public ProgramAttribute getAttributeOfType( String typeStr ) 
	throws IllegalArgumentException;

    /**
     * Removes the attribute of the specified type that may be associated
     * with a program.
     * @param typeStr type of program attribute. Must be a
     *                subtype of <code>ProgramAttribute</code>.
     * @return <code>true</code> if program contained an
     *         attribute of the specified type, or
     *         <code>false</code> if program has no attribute of
     *         the specified type.
     * @throws IllegalArgumentException if <code>type</code> is not a subtype
     *                           of <code>ProgramAttribute</code>.
     */
    public boolean removeAttributeOfType( String typeStr )
        throws IllegalArgumentException;
    
    /**
     * Returns the number of attributes associated with a program.
     * @return Number of attributes associated with program.
     */
    public int getAttributeCount();

    /**
     * Returns an array of <code>ProgramAttribute</code>s
     * associated with a program.
     * @return Array of attributes associated with program.
     * @see ProgramAttribute
     */
    public ProgramAttribute[] getAttributes();

    /**
     * Sets boolean to indicate that PNRC analysis has been performed for
     * the program.
     */
    public void setIsPNRCAnalysisDone();

    /**
     * Returns whether PNRC analysis has been performed for the program.
     * @return <code>true</code>, if PNRC analysis has been performed;
     *         <code>false</code> otherwise.
     */
    public boolean getIsPNRCAnalysisDone();

    /**
     * Returns the symbol table for this program.
     * @return  The symbol table for this program.
     */
    public SymbolTable getSymbolTable();

    /**
     * Returns a classloader that can load classes from this Program.
     * <br><br>
     * @return the classloader
     */
    public ClassLoader getClassLoader();

    /**
     * Returns the resource file for this Program object.
     * @return The resource file for this Program object.
     */
    public ResourceFileI getResourceFile();

    /**
     * Returns the Resources for this Program object.
     * @return The Resources for this Program object.
     */
    public ResourceFileI getResources ();

	/** returns a string version of this program */
	public String toString ();

    /**
     * We store, for each build, a SN consisting of a date and an
     * additional, developer-defined string
     */
    public final String sn = "xxx-string-here-xxx";
}
