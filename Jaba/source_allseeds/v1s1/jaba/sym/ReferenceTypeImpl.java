/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Abstract class that represents all reference types in the symbol
 * table.
 * @author Jim Jones
 * @author Jay Lofstead 2003/07/01 changed to implement ReferenceType
 */
public abstract class ReferenceTypeImpl extends TypeEntryImpl implements ReferenceType
{
}
