package jaba.sym;

/**
 * Represents a "this" local variable object self reference.  This class
 * extends FormalParameter, which in turn extends LocalVariable.  This is done
 * because, in Java (bytecodes), the "this" reference is treated as a 
 * formal parameter on methods.  This implementation also facilitates the
 * more efficient check for "instanceof This" rather than checking a given
 * LocalVariable for its String name to determine if it is a "this" reference.
 * @author Jim Jones -- <i>Created April 30, 1999</i>.
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface This extends FormalParameter
{
}
