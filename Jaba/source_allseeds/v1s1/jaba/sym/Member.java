/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import jaba.constants.AccessLevel;
import jaba.constants.Modifier;

/**
 * Abstract class that represents a member entry into the symbol table.
 * @author Huaxing Wu -- <i> Revised 8/12/02 add isAbstract method </i>
 * @author Huaxing Wu -- <i> Revised 11/18/02 remove setSignature(name, descriptor) method, since we now have ways to getsignature from these two strings. </i>
 * @author Jay Lofstead 2003/05/29 added getSignatureXMLValid.  changed toString to return data in an XML format
 * @author Jay Lofstead 2003/06/16 changed to use Util.toXMLValid for string conversion
 * @author Jay Lofstead 2003/06/30 added implements Member
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface Member extends SymbolEntry
{
    /**
     * Sets the access level of member.
     * @param acc Access level of member
     */
    public void setAccessLevel( int acc );

    /**
     * Sets the type of member.
     * @param type Type of member
     */
    public void setType( TypeEntry type );

    /**
     * Sets the containing type of member.
     * @param type Containing type of member
     */
    public void setContainingType( NamedReferenceType type );

    /**
     * Sets the modifier flags for the member.
     * @param mods Bit-masks representing modifier flags for the member.
     */
    public void setModifiers( int mods );

    /**
     * Sets the descriptor for the member.
     * @param desc String representation of member descriptor.
     */
    public void setDescriptor (String desc);

    /**
     * Sets the synthetic flag for the member.
     * @param flag Boolean value for the synthetic attribute.
     */
    public void setSynthetic (boolean flag);

    /**
     * Returns the access level of member.
     * @return access level of member - an integer value defined in
     *  {@link jaba.constants.AccessLevel Accesslevel}.
     */
    public int getAccessLevel ();

    /**
     * Returns the type of member.
     * @return type of member
     */
    public TypeEntry getType ();

    /**
     * Returns the containing type of member.
     * @return containing type of member
     */
    public NamedReferenceType getContainingType ();

    /**
     * Returns the modifier flags of member.
     * @return modifier flags of member
     */
    public int getModifiers();

    /**
     * Returns the member descriptor.
     * @return descriptor for member
     */
    public String getDescriptor();

    /**
     * Returns a boolean value indicating whether the member has its synthetic
     * attribute set.
     * @return true if member has its synthetic attribute set, false otherwise.
     */
    public boolean isSynthetic();

  /**
   * Sets a signature to a specified string.     
   * A method signature is the method name follow
   * by a sequence of formal-parameter types.
   * A field signature is the field name followed by its type descriptor.
   * Both method and field signatures use Java classfile descriptors.
   * @param signature  Specified signature.
   */
  public void setSignature( String signature );

    /**
     * Returns the signature of this method or field.
     * A method signature is the method name follow
     * by a sequence of formal-parameter types.
     * A field signature is the field name followed by its type descriptor.
     * Both method and field signatures use Java classfile descriptors.
     * @return Signature of method or field.
     */
    public String getSignature();

    /** Return a boolean to indicate whether this member is abstract.
     *@return true - this member is abtract. false -- member is concrete.
     */
    public boolean isAbstract();

  /**
   * Returns a string for this object.
   * @return  String representing this object.
   */
	public String toString ();
}
