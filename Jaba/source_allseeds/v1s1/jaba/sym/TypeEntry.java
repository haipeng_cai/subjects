/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Abstract class that represents a type entry in the symbol table,
 * namely a primitive or a reference type.
 * @author Jim Jones
 * @author Jay Lofstead 2003/06/30 added implements TypeEntry
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface TypeEntry extends SymbolTableEntry
{
}
