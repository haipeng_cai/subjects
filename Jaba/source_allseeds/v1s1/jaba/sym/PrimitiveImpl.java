/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Represents a primitive type in the symbol table.
 * @author Jim Jones
 * @author Huaxing Wu -- <i> 8/2/02. Based on the observation that Primitive 
 *          is an immutable class and there is total 11 instance, this class 
 *          has been changed to become immutable, and use private constructor
 *          and factory method to control instances of this class. </i>
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/06/30 adjusted toString to return XML, added implement Primitive
 * @author Jay Lofstead 2003/07/01 changed to implement Primitive
 */
public class PrimitiveImpl extends TypeEntryImpl implements Cloneable, Primitive
{
	/** ??? */
    private int type;

	/** ??? */
    private static Primitive PRIMITIVE_VOID = 
	new PrimitiveImpl (VOID, "void");
    
	/** ??? */
    private static  Primitive PRIMITIVE_BOOLEAN = 
	new PrimitiveImpl (BOOLEAN, "boolean");
  
	/** ??? */
    private static  Primitive PRIMITIVE_CHAR = 
	new  PrimitiveImpl (CHAR, "char");
    
	/** ??? */
    private static  Primitive PRIMITIVE_BYTE = 
	new  PrimitiveImpl (BYTE, "byte");
    
	/** ??? */
    private static  Primitive PRIMITIVE_SHORT = 
	new  PrimitiveImpl (SHORT, "short");
    
	/** ??? */
    private static  Primitive PRIMITIVE_INT = 
	new  PrimitiveImpl (INT, "int");
    
	/** ??? */
    private static  Primitive PRIMITIVE_LONG = 
	new  PrimitiveImpl (LONG, "long");
    
	/** ??? */
    private static  Primitive PRIMITIVE_FLOAT = 
	new  PrimitiveImpl (FLOAT, "float");
    
	/** ??? */
    private static  Primitive PRIMITIVE_DOUBLE = 
	new  PrimitiveImpl (DOUBLE, "double");
    
	/** ??? */
    private static  Primitive PRIMITIVE_STRING = 
	new  PrimitiveImpl (STRING, "string");

	/** ??? */
    private static  Primitive PRIMITIVE_NULL = 
	new  PrimitiveImpl (NULL, "null");

	/** ??? */
    private static final Primitive[] PRIMITIVE_VALUES = 
    { PRIMITIVE_VOID, PRIMITIVE_BOOLEAN, PRIMITIVE_CHAR, PRIMITIVE_BYTE,
      PRIMITIVE_SHORT, PRIMITIVE_INT, PRIMITIVE_LONG, PRIMITIVE_FLOAT,
      PRIMITIVE_DOUBLE, PRIMITIVE_STRING, PRIMITIVE_NULL};

    /** Get all the Primitive types.
     *@return an array of primitive data types.
     */
    public static Primitive[] getAllPrimitives() {
	return PRIMITIVE_VALUES;
    }

    /** Get the jaba Primitive object for the java primitive type.
     * This method serves as a translator, tranlates the java primitive
     * type to a jaba primitive type.
     * @param javaType Can be all Primitive type that this Primitive Class
     *             can represent.
     * @return The Primitive object corresponding to the java type.
     */
    public static Primitive forJavaType(Object javaType) {
	if(javaType instanceof java.lang.String)
	    return PRIMITIVE_STRING;
	else if(javaType instanceof java.lang.Float)
	    return PRIMITIVE_FLOAT;
	else if(javaType instanceof java.lang.Long)
	    return PRIMITIVE_LONG;
	else if(javaType instanceof java.lang.Double)
	    return PRIMITIVE_DOUBLE;
	else if(javaType instanceof java.lang.Short)
	    return PRIMITIVE_SHORT;
	else if(javaType instanceof java.lang.Integer)
	    return PRIMITIVE_INT;
	else if(javaType instanceof java.lang.Byte)
	    return PRIMITIVE_BYTE;
	else if(javaType instanceof java.lang.Boolean)
	    return PRIMITIVE_BOOLEAN;
	else if(javaType instanceof java.lang.Character)
	    return PRIMITIVE_CHAR;
	else if(javaType instanceof java.lang.Void)
	    return PRIMITIVE_VOID;
	else {
	    assert false: javaType.toString();
	    return null;
	}
    }

    /**Get the Primitive type for the specified type id. The type id
     * is defined as constant at the beginning of this class.
     *@param type -- the type id.
     *@return The Primitive Object corresponding to the type id.
     */
    public static Primitive valueOf(int type) {
	switch (type) {
	case BOOLEAN:
	    return PRIMITIVE_BOOLEAN;
	case CHAR:
	    return PRIMITIVE_CHAR;
	case BYTE:
	    return PRIMITIVE_BYTE;
	case SHORT:
	    return PRIMITIVE_SHORT;
	case INT:
	    return PRIMITIVE_INT;
	case LONG:
	    return PRIMITIVE_LONG;
	case FLOAT:
	    return PRIMITIVE_FLOAT;
	case DOUBLE:
	    return PRIMITIVE_DOUBLE;
	case VOID:
	    return PRIMITIVE_VOID;
	case STRING:
	    return PRIMITIVE_STRING;
	case NULL:
	    return PRIMITIVE_NULL;
	default:    
	    throw new IllegalArgumentException( "Exception thrown in " + 
						"aristotle.java.sym." +
						"Primitive.setType()" );
	}
    }

    /** Construct a Primitive object for the specified type and name.
    * @param type    One of the defined primitive constants.
    * @param name    The name of this type.
    */
    private PrimitiveImpl (int type, String name) {
	this.type = type;
	setName(name);
    }
    
    /**
     * Returns the type of this primitive type.
     * @return   Type of primitive
     */
    public int getType()
    {
	return type;
    }
    
    /**
     * Returns a string for this object.
     * @return  String representing this object.
     */
    public String toString ()
    {
	return "<primitive><id>" + getID () + "</id><name>" + getName () + "</name></primitive>";
    }
    
    /**
     * Clones this object.
     * @return Clone of this object.
     */
    public Object clone()
    {
	Primitive newPrim = null;
	try
	    {
		newPrim = (Primitive)super.clone();
	    }catch ( CloneNotSupportedException e){
		e.printStackTrace();
		throw new RuntimeException( e );
	    }

	return newPrim;
    }
    
}
