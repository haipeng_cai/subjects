/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Interface for attributes of a interface.
 * @author S. Sinha
 */
public interface InterfaceAttribute extends NamedReferenceTypeAttribute
{

}
