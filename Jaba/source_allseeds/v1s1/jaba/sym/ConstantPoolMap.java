/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import java.util.HashMap;

/**
 * Creates a map from an index in the constant pool to an entry in the
 * symbol table. The type of symbol table entry to which a constant-pool
 * index may map to is one of <code>Class</code>, <code>Interface</code>,
 * <code>Array</code>, <code>Method</code>, or <code>Field</code>.
 * A separate map - <code>LocalVariableMap</code> - is used
 * to map entries in the local variable table to symbol table objects of type
 * <code>LocalVariable</code>.
 *
 * @see jaba.classfile.ConstantPool
 * @see jaba.sym.Class
 * @see jaba.sym.Interface
 * @see jaba.sym.Array
 * @see jaba.sym.Method
 * @see jaba.sym.Field
 * @see jaba.sym.LocalVariableMap
 *
 * @author S. Sinha -- <i>Created</i>
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 */
public class ConstantPoolMap implements ClassAttribute, InterfaceAttribute, java.io.Serializable
{
    /** HashMap from an index in the constant pool to an entry in
        the symbol table. */
    private HashMap map;

    /**
     * Creates an empty <code>ConstantPoolMap</code> object.
     */
    public ConstantPoolMap() {
        map = new HashMap();
    }

    /**
     * Returns the symbol table object that maps to a given integer key.
     * Returns <code>null</code> if <code>key</code> is not mapped to
     * a symbol table object.
     * @param key key to retrieve mapping for.
     * @return variable that maps to <code>key</code>, or <code>null</code> if
     *         if <code>key</code> maps to no symbol.
     * @throws IllegalArgumentException if <code>key</code> is
     *                                  <code>null</code>.
     */
    public SymbolTableEntry get( Integer key ) throws IllegalArgumentException {
        if ( key == null ) {
            throw new IllegalArgumentException( "ConstantPoolMap.get(): "+
                "key is null" );
        }
        if ( !map.containsKey( key ) ) {
            return null;
        }
        return (SymbolTableEntry)map.get( key );
    }

    /**
     * Returns the Integer, Long, Float, Double, or String constant value 
     * that maps to a given integer key.
     * Returns <code>null</code> if <code>key</code> is not mapped to
     * a constant value object object.
     * @param key key to retrieve mapping for.
     * @return constant value that maps to <code>key</code>, or
     *         <code>null</code>
     *         if <code>key</code> maps to no value.
     * @throws IllegalArgumentException if <code>key</code> is
     *                                  <code>null</code>.
     */
    public Object getConst( Integer key )
	throws IllegalArgumentException
    {
        if ( key == null ) {
            throw new IllegalArgumentException( "ConstantPoolMap.get(): "+
                "key is null" );
        }
        if ( !map.containsKey( key ) ) {
            return null;
        }
        return map.get( key );
    }

    /**
     * Maps the given integey key to the given symbol-table object.
     * Replaces any existing mapping for key.
     * @param key key for mapping.
     * @param var variable to map <code>key</code> to.
     * @throws IllegalArgumentException if either <code>key</code>, or
     *                                  <code>var</code> is <code>null</code>,
     *                                  or the type of <code>var</code> is not
     *                                  one of <code>Class</code>,
     *                                  <code>Interface</code>,
     *                                  <code>Array</code>,
     *                                  <code>Method</code>,
     *                                  <code>Field</code>, or
     *                                  <code>Primitive</code>.
     */
    public void put( Integer key, SymbolTableEntry var )
                throws IllegalArgumentException
    {
        if ( key == null ) {
            throw new IllegalArgumentException( "LocalVariableMap.put(): "+
                "key is null" );
        }
        if ( var == null ) {
            throw new IllegalArgumentException( "LocalVariableMap.put(): "+
                "var is null" );
        }
        if ( !(var instanceof Class) && !(var instanceof Interface) &&
             !(var instanceof Array) && !(var instanceof Method ) &&
             !(var instanceof Field) && !(var instanceof Primitive) ) {
            throw new IllegalArgumentException( "ConstantPoolMap.put(): "+
                "var is not of the expected Class, Interface, Array, "+
                "Primitive, Method, or Field type: "+
                var.getClass().getName() );
        }
        map.put( key, var );
    }

    /**
     * Maps the given integey key to the integer, long, float, double, or
     * string constant.
     * Replaces any existing mapping for key.
     * @param key key for mapping.
     * @param constVal constant to map <code>key</code> to.
     * @throws IllegalArgumentException if either <code>key</code>, or
     *                                  <code>constVal</code> is
     *                                  <code>null</code>, or the type of
     *                                  <code>constVal</code> is not
     *                                  one of <code>Integer</code>,
     *                                  <code>Long</code>,
     *                                  <code>Float</code>,
     *                                  <code>Double</code>, or
     *                                  <code>String</code>.
     */
    public void putConst( Integer key, Object constVal )
                throws IllegalArgumentException
    {
        if ( key == null ) {
            throw new IllegalArgumentException( "ConstantPoolMap.put(): "+
                "key is null" );
        }
        if ( constVal == null ) {
            throw new IllegalArgumentException( "ConstantPoolMap.put(): "+
                "constVal is null" );
        }
        if ( !(constVal instanceof Integer) && !(constVal instanceof Long) &&
             !(constVal instanceof Float) && !(constVal instanceof Double ) &&
             !(constVal instanceof String) ) {
            throw new IllegalArgumentException( "ConstantPoolMap.put(): "+
                "constVal is not of the expected Integer, Long, Float, "+
                "Double, or String type: "+
                constVal.getClass().getName() );
        }
        map.put( key, constVal );
    }

    /**
     * Returns <code>true</code> if this map contains no key-value mappings.
     * @return <code>true</code> if this map contains no key-value mappings.
     */
    public boolean isEmpty() {
        return map.isEmpty();
    }

}
