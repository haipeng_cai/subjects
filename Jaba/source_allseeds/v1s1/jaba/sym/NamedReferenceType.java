/* Copyright (c) 2002, Georgia Institute of Technology */

package jaba.sym;

import jaba.constants.AccessLevel;
import jaba.constants.Modifier;

import java.util.Vector;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Enumeration;
import java.util.Collection;
import java.util.List;
import java.util.LinkedList;

import java.lang.Class;

import java.io.IOException;

/**
 * Class that represents either an interface or a class in the
 * symbol table.
 * @author Jim Jones
 * @author Alex uncommented getContainingPackage
 * @author Huaxing Wu -- <i> revised, 6/12/02. Add pretocted method 
 *                         getParents(). For Class, should return 
 *                         superclass+interfaces. For Interface, should return
 *                         all SuperInterface. This method should be 
 *                         reimplemented
 *                         by all subclasses. </i>
 * @author Huaxing Wu -- <i> Revised, 6/15/02. Remove all the methods and 
 *                              variables related to inherited methods and 
 *                              fields from Class to this class. Change the 
 *                              the method flattern to take multiple parents
 *                              Which for Class, can be superclass+interfaces.
 *                              For Interface, can be all SuperInterface.</i>
 * @author Huaxing Wu -- <i> Revised, 7/9/02. Remove 
 *                              innerNamedReferenceTypesArr, 
 *                              use innerNamedReferenceTypes only.
 *                              Change System.exit(1) to throw RuntimeException
 * @author Huaxing wu -- <i> Revised, 11/20/02. Add findMethod, findFiends methods.</i>
 * @author Jay Lofstead 2003/05/29 changed toString to generate XML
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/26 added implementing Serializable
 * @author Jay Lofstead 2003/06/30 added implement of NamedReferenceType
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface NamedReferenceType extends ReferenceType, ContainsInnerNamedReferenceTypes
{
    /**
     * Sets the containing program.
     * @param program  The program that contains this NamedReferenceType.
     */
    public void setProgram (Program program);

    /**
     * Returns the containing program.
     * @return  The program that contains this NamedReferenceType.
     */
    public Program getProgram ();

    /**
     * Adds a method to this class or interface.
     * @param method   The method to be added to this class or interface.
     * @exception IllegalArgumentException   Thrown if <code>method</code> is
     *                                       <code>null</code>, or signature of
     *                                       <code>method</code> is not set.
     */
    public void addMethod (Method method) throws IllegalArgumentException;
    
    /**
     * Returns methods in this class or interface.
     * @return   Methods defined in this named reference type.
     */
	public Collection getMethodsCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getMethodsCollection</code>
     * @return   Methods defined in this named reference type.
     */
	public Method [] getMethods ();

    /**
     * Returns the method from this named-reference type that has the
     * given signature.
     * @param signature signature of method to search for.
     * @return Method with the given signature, or <code>null</code> if none
     *         exists.
     */
    public Method getMethod( String signature );
    
    /**
     * Adds a field tho this class or interface.
     * @param field   The field to be added.
     * @exception IllegalArgumentException  Thrown if <code>field</code> is 
     *                                      <code>null</code>, or descriptor of
     *                                      <code>field</code> is not set.
     */
    public void addField( Field field ) throws IllegalArgumentException;
    
    /**
     * Returns fields defined directly in this class or interface.
     * @return   Fields defined in this named reference type.
     */
	public Collection getFieldsCollection ();
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getFieldsCollection</code>
     * @return   Fields defined directly in this named reference type.
     */
	public Field [] getFields ();
    
    /**
     * Returns the field from defined directly in this named-reference type that has the
     * given signature. A field signature is the field name followed by its
     * type.
     * @param signature signature of field to search for.
     * @return Field with the given signature, or <code>null</code> if none
     *         exists.
     * @see jaba.sym.Member#getSignature
     */
    public Field getField( String signature );
    
    /**
     * Assigns an access level value to this class or interface.
     * @param accessLevel  Access level of the class.  Valid values found in
     *                  {@link jaba.constants.AccessLevel AccessLevel}.
     */
    public void setAccessLevel( int accessLevel );

    /**
     * Returns the access level of this class or interface.
     * @return   The access level.  Valid values found in
     *           {@link jaba.constants.AccessLevel AccessLevel}.
     */
    public int getAccessLevel();

    /**
     * Assigns the package for which this class or interface belongs to.
     * @param containingPackage   The package of this class or interface.
     * @exception IllegalArgumentException  Thrown if 
     *                                      <code>containingPackage</code> is
     *                                      <code>null</code>.
     */
    public void setContainingPackage( Package containingPackage ) throws
	IllegalArgumentException;

    /**
     * Returns the containing package of this class or interface.
     * @return   The package that this class or interface belongs to.
     */
    public Package getContainingPackage() throws IllegalArgumentException;

    /**
     * Adds a class or interface to the list of inner classes or interfaces 
     * that this contains.
     * @param innerNamedReferenceType  The inner reference type that is 
     *                                 contained in this one.
     * @exception IllegalArgumentException  Thrown if the param is <code>null
     *                                      </code>.
     */
    public void addInnerNamedReferenceType( NamedReferenceType 
					    innerNamedReferenceType ) throws IllegalArgumentException;

    /**
     * Returns inner named reference types in this class or
     * interface.
     * @return  Inner named reference types.
     */
    public Collection getInnerNamedReferenceTypesCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from
     * <code>getInnerNamedReferenceTypesCollection</code>
     * @return  Inner named reference types.
     */
    public NamedReferenceType [] getInnerNamedReferenceTypes ();

    /**
     * Sets modifiers for this class.
     * @param modifier  Valid integer contants can be found in
     *                  {@link jaba.constants.Modifier Modifier}.
     */
    public void setModifiers( int modifiers );

    /**
     * Returns a bitmap of modifier constants.
     * @return  Bitmap of modifier constants.  Valid values can be found in
     *          {@link jaba.constants.Modifier Modifier}.
     */
    public int getModifiers();

    /**
     * Sets whether this class or interface is summarized.
     * Library named reference is summarized. Subject named reference is not.
     * @param isSummarized  Boolean indicating whether this class or interface
     *                      is summarized.
     */
    public void setIsSummarized( boolean isSummarized );

    /**
     * Returns whether this class or interface is summarized.
     * Library named reference is summarized. Subject named reference is not.
     * @return  A boolean indicating whether this class or interface is
     *          summarized.
     */
    public boolean isSummarized();

    /**
     * Sets the class, interface, or method for which this class is an inner 
     * class.
     * @param containingType  The type in which this class is an inner class, 
     *                        or null if this class is not an inner class.
     */
    public void setContainingType( ContainsInnerNamedReferenceTypes 
				   containingType );

    /**
     * Get the class, interface, or method for which this class is an inner
     * class.
     * @return The type for which this class is an inner class or null if this 
     *         class is not an inner class
     */
    public SymbolTableEntry getContainingType ();

    /**
     * Returns the attribute of the specified type that may be associated
     * with a class or an interface.
     * @param typeStr type of class or interface attribute.
     * @return Class or interface attribute of the specified type, or
     *         <code>null</code> if class or interface has no attribute of
     *         the specified type.
     */
    public NamedReferenceTypeAttribute getAttributeOfType( String typeStr );

    /**
     * Removes the attribute of the specified type that may be associated
     * with a class or an interface.
     * @param typeStr type of class or interface attribute. Must be a
     *                subtype of <code>NamedReferenceTypeAttribute</code>.
     * @return <code>true</code> if class or interface contained an
     *         attribute of the specified type, or
     *         <code>false</code> if class or interface has no attribute of
     *         the specified type.
     * @throws IllegalArgumentException if <code>type</code> is not a subtype
     *                           of <code>NamedReferenceTypeAttribute</code>.
     */
    public boolean removeAttributeOfType( String typeStr ) throws IllegalArgumentException;

    /**
     * Adds an attribute to a class or an interface. This method is invoked
     * by the <code>addAttribute()</code> method defined in <code>Class</code>
     * or <code>Interface</code>.
     * @param attribute attribute to add to a class or interface.
     * @throws IllegalArgumentException if <code>attribute</code> is
     *                                  <code>null</code>.
     */
    public void addAttribute( NamedReferenceTypeAttribute attribute ) throws IllegalArgumentException;

    /**
     * Returns the number of attributes associated with a named-reference type.
     * @return Number of attributes associated with named-reference type.
     */
    public int getAttributeCount();

    /**
     * Returns a string for this object.
     * @return  String representing this object.
     */
	public String toString ();

   /**
    * Adds an inherited method to this class.
    * @param method   The method to be added to this class.
    * @exception IllegalArgumentException   Thrown if <code>method</code> is
    *                                       <code>null</code>, or signature of
    *                                       <code>method</code> is not set.
    */
    public void addInheritedMethod( Method method ) 
	throws IllegalArgumentException;

    /** Returns methods that can be called from this class.
     *  will be methods + inheritedmethods
     *  @return All available methods of this class
     */
	public Collection getAllMethodsCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getAllMethodsList</code>
     *  @return All available methods of this class
     */
	public Method [] getAllMethods ();

    /**
     * Returns methods inherited by this class.
     * @return Methods inherited by this class.
     */
	public Collection getInheritedMethodsCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getInheritedMethodsCollection</code>
     * @return Methods inherited by this class.
     */
	public Method [] getInheritedMethods();

    /**
     * Returns the method object for an inherited method given the signature
     * for the inherited method.
     * @param signature  The signature of the inherited method.
     * @return  The method object for an inherited method given the signature
     *          for the inherited method.  If no inherited methods match the
     *          signature specified, <code>null</code> is returned.
     */
    public Method getInheritedMethod( String signature );

    /**
     * return the method decalred in this type and its inherited types given 
     * its signature.
     * @param signature signature of method to search for.
     * @return Method with the given signature, or <code>null</code> if none
     *         exists.
     */
    public Method findMethod (String signature);

    /**
     * Adds an inherited field to this class.
     * @param field   The field to be added.
     * @exception IllegalArgumentException  Thrown if <code>field</code> is 
     *                                      <code>null</code>, or descriptor of
     *                                      <code>field</code> is not set.
     */
    public void addInheritedField( Field field ) throws IllegalArgumentException;

    /** Returns fields that can be called from this class.
     *  will be fields + inheritedfieldss
     *  @return All available fields of this class
     */
    public Collection getAllFieldsCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getAllFieldsCollection</code>
     *  @return All available fields of this class
     */
    public Field [] getAllFields();

    /**
     * Returns fields inherited by this class.
     * @return Fields inherited by this class.
     */
	public Collection getInheritedFieldsCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getInheritedFieldsCollection</code>
     * @return Fields inherited by this class.
     */
	public Field [] getInheritedFields ();

   /**
    * Returns the field object for an inherited field given the signature
    * for the inherited field.
    * @param signature  The signature of the inherited field.
    * @return  The method object for an inherited field given the signature
    *          for the inherited field.  If no inherited fields match the
    *          signature specified, <code>null</code> is returned.
    */
    public Field getInheritedField(String signature);

   /**
    * return the field decalred in this type and its inherited types given 
    * its signature.
    * @param signature signature of field to search for.
    * @return Method with the given signature, or <code>null</code> if none
    *         exists.
    */
    public Field findField(String signature);

    /**
     * Sets the source file name.
     * @param The name of the source file.
     */
    public void setSourceFileName( String name );

    /**
     * Returns the source file name if set, and "" if not.
     * @return The source file name if specified in the classfile, and
     * "" if not.
     */
    public String getSourceFileName();

    /**
     * Returns the SourceFile object for this class/interface.
     * @return  SourceFile object for this class/interface.
     * @throws IOException thrown if an IOException occurs in opening
     * the SourceFile
     */
    public SourceFile getSourceFile() throws IOException;
}
