/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Represents an array in the symbol table.
 * @author Jim Jones
 * @author Jay Lofstead 2003/06/12 code cleanup
 * @author Jay Lofstead 2003/07/01 changed to implement Array
 */
public class ArrayImpl extends ReferenceTypeImpl implements Array
{
  
  /** Type of the elements of this array */
  private TypeEntry elementType;

  /** Type of leaf element of array. */
  private TypeEntry leafElementType;

  /**
   * Number of dimensions of array. For a single-dimensional array,
   * <code>elementType</code> and <code>leafElementType</code> are the same.
   */
  private int dimensions;

    /**
     * The superclass of this Array, which is always java.lang.Object.  This
     * field is provided as a convenience to the user as a direct link to
     * the java.lang.Object in the symbol table.
     */
    private static Class superClass;
  
  /** Constructor. */
  public ArrayImpl ()
    {
      setName("");
    }
  
  /**
   * Sets the type of the elements of the array.
   * @param elementType   type of the elements of the array.
   * @exception IllegalArgumentException   thrown if elementType is
   *                                       null
   */
  public void setElementType( TypeEntry elementType ) throws
  IllegalArgumentException
    {
      // check if the elementType passed in is a null reference, if so, throw an exception
      if ( elementType == null )
	{
	  throw new IllegalArgumentException( "Exception thrown in jaba.sym.Array.setElementType ()" );
	}
      
      this.elementType = elementType;

    }

  /**
   * Returns the type of the elements of this array.
   * @return   type of the elements of this array
   */
  public TypeEntry getElementType()
    {
      return elementType;
    }

  /**
   * Returns the non-array element type. The non-array element type appears
   * at the leaf of the recursive array structure for multi-dimensional
   * arrays.
   * @return Leaf element type of array.
   */
  public TypeEntry getLeafElementType() {
      if ( leafElementType == null ) {
          __initializeLeafTypeAndDimensions();
      }
      return leafElementType;
  }

  /**
   * Returns the number of dimensions of array.
   * @return number of dimensions of array.
   */
  public int getDimensions() {
      if ( dimensions == 0 ) {
          __initializeLeafTypeAndDimensions();
      }
      return dimensions;
  }

    /**
     * Sets the superclass of this array (which is always java.lang.Object).
     * @param superClass  The object which represents java.lang.Object.
     */
    public void __setSuperClass( Class superClass ) {
	ArrayImpl.superClass = superClass;
    }

    /**
     * Returns the superclass of this array (which is always java.lang.Object).
     * @return The superclass of this array (which is always java.lang.Object).
     */
    public Class getSuperClass() {
	return superClass;
    }

  /**
   * Initializes leaf element type and array dimensions by recursively
   * traversing the recursive array structure. This method is invoked
   * only on the first call to either <code>getLeafElementType()</code> or
   * <code>getDimensions()</code> on an <code>Array</code> object; the
   * method saves the computed values in fields - <code>leafElementType</code>
   * and <code>dimensions</code>. Subsequent calls to either of the
   * methods simply return the field values.  Also sets name of this array
   * and all subarrays.
   */
  public void __initializeLeafTypeAndDimensions()
    {
      TypeEntry type = elementType;
      String name;
      if ( type instanceof Array )
	{
	  leafElementType = ((Array)type).getLeafElementType();
	  dimensions = 1+((Array)type).getDimensions();
	  name = (leafElementType == null) ? "" : leafElementType.getName();
	  for ( int i = 0; i < dimensions; i++ )
	    name += "[]";
	}
      else // we have reached the one dimensional array
	{
	  leafElementType = type;
	  dimensions = 1;
	  name = (leafElementType == null) ? "" : leafElementType.getName();
	  name += "[]";
	}
      setName( name );
    }

  /**
   * Returns a string for this object.
   * @return  String representing this object.
   */
  public String toString()
    {
	TypeEntry let = getLeafElementType();
      return getID() + " Array: dimensions="+ getDimensions() +
             " element type = " + ((let != null) ? let.getName() : "null");

    }
}
