/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Abstract class that represents a Java descriptor for a field or a method.
 *
 * @author S. Sinha
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 */
public abstract class Descriptor implements java.io.Serializable
{
    // Constants

    /** Marker for a byte */
    private final static char D_BYTE = 'B';

    /** Marker for a char */
    private final static char D_CHAR = 'C';

    /** Marker for a double */
    private final static char D_DOUBLE = 'D';

    /** Marker for a float */
    private final static char D_FLOAT = 'F';

    /** Marker for a int */
    private final static char D_INT = 'I';

    /** Marker for a long */
    private final static char D_LONG = 'J';

    /** Marker for a class or an interface. */
    private final static char D_NAMED_REF = 'L';

    /** Marker for a short */
    private final static char D_SHORT = 'S';

    /** Marker for a boolean */
    private final static char D_BOOLEAN = 'Z';

    /** Marker for an array. */
    private final static char D_ARRAY = '[';

    /** Marker at end of a fully-qualified class or interface name. */
    private final static char D_NAMED_REF_END = ';';

    /** Marker for void */
    private final static char D_VOID = 'V';


    // Fields

    /** String representation of internal Java field (method) descriptor. */
    protected String descriptor;

    /** Type (Return type) of a field (method). */
    protected TypeEntry type;

    /** Array containing char markers for primitive types. */
    private char[] primitiveTypeArr = { D_BYTE, D_CHAR, D_DOUBLE, D_FLOAT,
                                        D_INT, D_LONG, D_SHORT, D_BOOLEAN };

    /** String containing legal primitive type char markers. */
    protected String primitiveFieldTypes = new String( primitiveTypeArr );

    // Methods

    /**
     * Parses type information from a descriptor and returns an appropriate
     * <code>TypeEntry</code> object corresponding to the field type.
     * @param desc Java descriptor.
     * @param primitiveTypes String of legal primitive type specifiers.
     * @return <code>TypeEntry</code> corresponding to field type.
     * @throws IllegalArgumentException if <code>desc</code> contains an illegal
     *                                  type specifier.
     */
    protected TypeEntry parseType( StringBuffer desc, String primitiveTypes )
                        throws IllegalArgumentException {
        if ( desc.charAt( 0 ) == D_NAMED_REF ) {
            String descString = desc.toString();
            int last = descString.indexOf( D_NAMED_REF_END , 0 );
	    String name = descString.substring( 1, last );
            desc = desc.delete( 0, last+1 );
	    NamedReferenceType type = new NamedReferenceTypeImpl ();
	    type.setName( name );
            return type;
        }
        if ( desc.charAt( 0 ) == D_ARRAY ) {
            Array type = new ArrayImpl ();
            desc = desc.deleteCharAt( 0 );
            type.setElementType( parseType( desc, primitiveTypes ) );
            return type;
        }

        if ( primitiveTypes.indexOf( desc.charAt( 0 ) ) > -1 ) {
            Primitive primitive = null; // = new Primitive();
	    //	    int primitiveType = 0;
	    switch ( desc.charAt( 0 ) ) {
	    case D_BYTE: 
		//	  primitiveType = PrimitiveImpl.BYTE;
		primitive = PrimitiveImpl.valueOf(PrimitiveImpl.BYTE);
		break;
	    case D_CHAR:
		//  primitiveType = PrimitiveImpl.CHAR;
		primitive = PrimitiveImpl.valueOf(PrimitiveImpl.CHAR);
		break;
	    case D_DOUBLE:
		//	primitiveType = PrimitiveImpl.DOUBLE;
		primitive = PrimitiveImpl.valueOf(PrimitiveImpl.DOUBLE);
		break;
	    case D_FLOAT:
		  //		primitiveType = PrimitiveImpl.FLOAT;
		  primitive = PrimitiveImpl.valueOf(PrimitiveImpl.FLOAT);
		  break;
	    case D_INT:
		//		primitiveType = PrimitiveImpl.INT;
		primitive = PrimitiveImpl.valueOf(PrimitiveImpl.INT);
		break;
	    case D_LONG:
		//		primitiveType = PrimitiveImpl.LONG;
		primitive = PrimitiveImpl.valueOf(PrimitiveImpl.LONG);
		break;
	    case D_SHORT:
		//		primitiveType = PrimitiveImpl.SHORT;
		primitive = PrimitiveImpl.valueOf(PrimitiveImpl.SHORT);
		break;
	    case D_BOOLEAN:
		//		primitiveType = PrimitiveImpl.BOOLEAN;
		primitive = PrimitiveImpl.valueOf(PrimitiveImpl.BOOLEAN);
		break;
	    case D_VOID:
		//		primitiveType = PrimitiveImpl.VOID;
		primitive = PrimitiveImpl.valueOf(PrimitiveImpl.VOID);
		break;
	    default:
		assert false:desc.charAt( 0 );
	      }
	    //	    primitive.setType( primitiveType );
            desc = desc.deleteCharAt( 0 );
	    return primitive;

        }
        throw new IllegalArgumentException( "Descriptor.parseType(): "+
            "descriptor contains illegal type specifier: "+desc.charAt( 0 ) );
    }

}
