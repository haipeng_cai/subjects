// Copyright (c) 1999, The Ohio State University

package jaba.sym;

import jaba.main.ResourceFileI;

import jaba.main.Options;
import jaba.main.OptionsImpl;

import java.util.Vector;
import java.util.Enumeration;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import java.net.URL;
import java.net.URLClassLoader;

import java.io.File;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

/**
 * Represents the application being analyzed.
 * @author S. Sinha -- <i>Created.</i>
 * @author Jim Jones -- <i>Revised.  April 1, 1999,  Moved code performing 
 *                      initial analysis from the "main" method to
 *                      Program.load.</i>
 * @author Huaxing Wu -- <i>Revised. 3/21/02. Change load() to use new ResourceFile obejct, also add calling RLVT part.
 * @author Huaxing Wu -- <i>Revised 10/30/02. Remove RLVT part to SymboTable, 
 *                        as it's responsible for the program loading.</i>
 * @author Huaxing Wu -- <i>Revised 10/31/02. Provide access to Packages by
 *                       adding getPackages() and getPackage(String name).</i> 
 * @author Jay Lofstead 2003-05-22 removed the commented out code to aid in readability.
 * @author Jay Lofstead 2003/06/05 added a toString method
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/16 updated toString to include class and interface names
 * @author Jay Lofstead 2003/06/20 added a private Options object, added second constructor, added init method.
 * @author Jay Lofstead 2003/06/23 changed to implement Program
 * @author Jay Lofstead 2003/07/17 changed factory mechanism to be in line with more recent versions.
 */
public class ProgramImpl extends SymbolTableEntryImpl implements jaba.constants.Modifier, Program
{
	/**
	 * Creates an empty program object.
	 * @param rcFile  Resource file specifying the program name, class paths to
	 *                use for analysis, class files to analyze, etc.
	 */
	public ProgramImpl (ResourceFileI resourceFile)
	{
		// default constructor copies the public static members
		options = new OptionsImpl ();

		init (resourceFile);

		load ();
	}

	/**
	 * Create a new Program object using the specified Options object
	 */
	public ProgramImpl (ResourceFileI resourceFile, Options opt)
	{
		options = new OptionsImpl (opt);

		init (resourceFile);

		load ();
	}

	/**
	 * Returns the options associated with this Program object
	 */
	public Options getOptions ()
	{
		return options;
	}

	/**
	 * private common initialization code for use by the constructors
	 */
	private void init (ResourceFileI resourceFile)
	{
		rcFile = (ResourceFileI) resourceFile;
		namedRefTypes = new Vector (rcFile.getClassFilesList ().size ());
		attributes = new Vector ();
		isPNRCAnalysisDone = false;

		if (options.getCreateLVT ())
		{
			System.out.println ("RLVT enabled");
		}

		if (options.getInlineFinally ())
		{
			System.out.println ("Inlined finally enabled");
		}
	}

    /**
     * Returns the version
     */
    public static String getsSn() { return sn; }

  /**
   * Loads all classfiles specified in the resource file and performs all
   * initial analysis.
   */
  private void load()
    {
      // set the name of this program
      setName( rcFile.getProgramName() );

      // create and load the symbol table
      symTable = new SymbolTableImpl (this);
      symTable.load();

      // dump itself into file Options.dumpfile
      if (options.getSerialize ())
      {
	  try {
	      FileOutputStream fos = new FileOutputStream (options.getDumpFile ());
	      ObjectOutputStream oos = new ObjectOutputStream(fos);
	      oos.writeObject(this);
	      oos.close();
	      fos.close();
	  }
	  catch(Exception e) {
	      System.err.println("Warning: cannot serialize to " +
				 options.getDumpFile ());
	      System.err.println(e.toString());
	  }
      }
	rcFile.cleanup ();
    }

    /**
     * Adds a named reference type to the program.
     * @param namedRefType named reference type to be added.
     * @throws IllegalArgumentException if <code>namedRefType</code> is
     *                                  <code>null</code>.
     */
    public void addNamedReferenceType( NamedReferenceType namedRefType )
        throws IllegalArgumentException
    {

        if ( namedRefType == null ) {
            throw new IllegalArgumentException(
                "Program.addNamedReferenceType(): namedRefType is null" );
        }
        namedRefTypes.addElement( namedRefType );
	namedRefType.setProgram( this );
    }

    /** Find the namedreferencetype for the given class/interface name */
    public NamedReferenceType findNamedReferenceType (String name)
    {
	return symTable.getNamedReferenceType(name);
    }

    /**
     * Returns all to-be-analyzed(i.e: subject) Classes and Interfaces in this program.
     * @return Array of all to-be-analyzedClasses and Interfaces in program.
     */
    public NamedReferenceType [] getNamedReferenceTypes ()
    {
	int subjectNum = rcFile.getClassFilesList ().size ();
	NamedReferenceType[] subjectNamedRefs = new NamedReferenceType[subjectNum];
	int index = 0;
	for (Enumeration i = namedRefTypes.elements (); i.hasMoreElements ();)
	{
	    NamedReferenceType namedRef = (NamedReferenceType) i.nextElement ();
	    // Skip library
	    if(!namedRef.isSummarized())
	    {
		subjectNamedRefs[index] = namedRef;
		index ++;
	    }
	}
	
	assert index==subjectNum;

	return subjectNamedRefs;
    }

    /**
     * Returns all Classes and Interfaces defined in this program.
     * @return Array of all Classes and Interfaces defined in program.
     */
	public Collection getLoadedNamedReferenceTypesCollection ()
	{
		return namedRefTypes;
	}

    /**
     * Convenience method for converting the Collection to an Array (less efficient) from <code>getLoadedNamedReferenceTypesCollection</code>
     * @return Array of all Classes and Interfaces defined in program.
     */
	public NamedReferenceType [] getLoadedNamedReferenceTypes ()
	{
		return (NamedReferenceType []) namedRefTypes.toArray (new NamedReferenceType [namedRefTypes.size ()]);
	}

  /**
   * Returns all Classes loaded in this program, including library classes and
   * suject classes (to-be-analyzed).
   * @return  Array of all Classes loaded in this program.
   */
	public Collection getLoadedClassesCollection ()
	{
		Vector classVec = new Vector(namedRefTypes.size());
		for (Enumeration e = namedRefTypes.elements (); e.hasMoreElements ();)
		{
			NamedReferenceType namedRef = (NamedReferenceType) e.nextElement ();
	  
			if ( namedRef instanceof Class )
			{
				classVec.add( namedRef );
			}
		}

		return classVec;
	}

  /**
   * Convenience method for converting the Collection to an Array (less efficient) from <code>getLoadedClassesCollection</code>
   * @return  Array of all Classes loaded in this program.
   */
	public Class [] getLoadedClasses ()
	{
		Collection v = getLoadedClassesCollection ();

		return (Class []) v.toArray (new Class [v.size ()]);
	}

  /**
   * Returns all to-be-analyzed Classes defined in this program. These
   * to-be-analyzed classes are usually specified in a ResourceFile.
   * @return  Array of all Classes defined in this program.
   */
	public Collection getClassesCollection ()
  	{
		int subjectNum = rcFile.getClassFilesList ().size ();
		Vector classVec = new Vector(subjectNum);
		for (Enumeration e = namedRefTypes.elements (); e.hasMoreElements ();)
		{
			NamedReferenceType namedRef = (NamedReferenceType) e.nextElement ();
			//Skip library
			if(namedRef.isSummarized())
				continue;

			if ( namedRef instanceof Class )
				classVec.add (namedRef);
		}

		return classVec;
	}

  /**
     * Convenience method for converting the Collection to an Array (less efficient) from <code>getClassesCollection</code>
   * @return  Array of all Classes defined in this program.
   */
	public Class [] getClasses ()
	{
		Collection v = getClassesCollection ();

		return (Class []) v.toArray (new Class [v.size ()]);
	}

  /**
   * Returns all loaded Interfaces in this program, including library
   * interfaces and to-be-analyzed interfaces. to-be-analyzed interfaces
   * are typically specified in a ResourceFile.
   * @return  Array of all Interfaces defined in this program.
   */
	public Collection getLoadedInterfacesCollection ()
	{
		Vector interfaceVec = new Vector(namedRefTypes.size());
		for (Enumeration i = namedRefTypes.elements (); i.hasMoreElements ();)
		{
			NamedReferenceType namedRef = (NamedReferenceType) i.nextElement ();

			if ( namedRef instanceof Interface )
				interfaceVec.add (namedRef);
		}

		return interfaceVec;
	}

  /**
   * Convenience method for converting the Collection to an Array (less efficient) from <code>getLoadedInterfacesCollection</code>
   * @return  Array of all Interfaces defined in this program.
   */
	public Interface [] getLoadedInterfaces ()
	{
		Collection v = getLoadedInterfacesCollection ();

		return (Interface []) v.toArray (new Interface [v.size ()]);
	}

  /**
   * Returns all to-be-analyzed Interfaces in this program. These
   * to-be-analyzed interfaces are usally specified in a ResourceFile.
   * @return  Array of all to-be-analyzed Interfaces in this program.
   */
	public Collection getInterfacesCollection ()
	{
		int subjectNum = rcFile.getClassFilesList ().size ();
		Vector interfaceVec = new Vector(subjectNum);
		for (Enumeration i = namedRefTypes.elements (); i.hasMoreElements ();)
		{
			NamedReferenceType namedRef = (NamedReferenceType) i.nextElement ();
			//Skip library
			if(namedRef.isSummarized())
				continue;

			if ( namedRef instanceof Interface )
				interfaceVec.add (namedRef);
		}

		return interfaceVec;
	}

  /**
   * Convenience method for converting the Collection to an Array (less efficient) from <code>getInterfacesCollection</code>
   * @return  Array of all to-be-analyzed Interfaces in this program.
   */
	public Interface [] getInterfaces ()
	{
		Collection v = getInterfacesCollection ();

		return (Interface []) v.toArray (new Interface [v.size ()]);
	}

  /**
   * Returns all "public static void main( String[] )" methods in all of the
   * to-be-analyzed Classes in this Program.
   * @return  An array of "public static void main( String[] )" methods.
   */
	public Collection getMainMethodsCollection ()
	{
		Vector mainMethodVec = new Vector ();
		for(Enumeration i = namedRefTypes.elements (); i.hasMoreElements ();)
		{
			NamedReferenceType namedRef = (NamedReferenceType) i.nextElement ();
			//Skip library
			if(namedRef.isSummarized())
				continue;

			//Skip interface
			if ( namedRef instanceof Interface )
				continue;

			Collection methods = namedRef.getMethodsCollection ();
			for (Iterator j = methods.iterator (); j.hasNext ();)
			{
				Method m = (Method) j.next ();
				if (m.isMainMethod())
					mainMethodVec.add (m);
			}
		}

		return mainMethodVec;
	}

  /**
   * Convenience method for converting the Collection to an Array (less efficient) from <code>getMainMethodsCollection</code>
   * @return  An array of "public static void main( String[] )" methods.
   */
	public Method [] getMainMethods ()
	{
		Collection v = getMainMethodsCollection ();

		return (Method []) v.toArray (new Method [v.size ()]);
	}

  /**
   * Returns all "public static void main( String[] )" methods in all the
   * loaded Classes in this Program.
   * @return  An array of "public static void main( String[] )" methods.
   */
	public Collection getLoadedMainMethodsCollection ()
	{
		Vector mainMethodVec = new Vector();
		for(Enumeration i = namedRefTypes.elements (); i.hasMoreElements ();)
		{
			NamedReferenceType namedRef = (NamedReferenceType) i.nextElement ();

			//Skip interface
			if ( namedRef instanceof Interface )
				continue;

			Collection methods = namedRef.getMethodsCollection ();
			for (Iterator j = methods.iterator (); j.hasNext ();)
			{
				Method m = (Method) j.next ();
				if (m.isMainMethod())
					mainMethodVec.add (m);
			}
		}

		return mainMethodVec;
	}

  /**
   * Convenience method for converting the Collection to an Array (less efficient) from <code>getLoadedMainMethodsCollection</code>
   * @return  An array of "public static void main( String[] )" methods.
   */
	public Method [] getLoadedMainMethods ()
	{
		Collection v = getLoadedMainMethodsCollection ();

		return (Method []) v.toArray (new Method [v.size ()]);
	}

    /**
     * Get all the packages in this program.
     * @return an array of packages
     */
	public Collection getPackagesCollection ()
	{
		return ((SymbolTableImpl) symTable).getPackageMap ().values ();
	}

    /**
     * Convenience method for converting the Collection to an Array (less efficient) from <code>getPackagesCollection</code>
     * @return an array of packages
     */
	public Package [] getPackages ()
	{
		Collection c = getPackagesCollection ();

		return (Package []) c.toArray (new Package [c.size ()]);
	}

    /** Get the package object for the specified package name.
     * @param name - the Package name
     * @return the package object for the name,null if the package not found.
     */
    public Package getPackage(String name)
    {
	return (Package) ((SymbolTableImpl) symTable).getPackageMap ().get (name.replace ('.','/'));
    }

    /**
     * Adds an attribute to this program.
     * @param attribute  The attribute to be added to this program.
     * @throws IllegalArgumentException if <code>attribute</code> is
     *                                  <code>null</code>.
     */
    public void addAttribute( ProgramAttribute attribute )
                throws IllegalArgumentException
    {
        if ( attribute == null ) {
            throw new IllegalArgumentException( "Program.addAttribute(): "+
                "attribute is null" );
        }
        attributes.add( attribute );
    }

    /**
     * Returns the attribute of the specified type that may be associated
     * with a program.
     * @param type type of program attribute.
     * @return Program attribute of the specified type, or
     *         <code>null</code> if program has no attribute of the specified
     *         type.
     */
	public ProgramAttribute getAttributeOfType (String typeStr) throws IllegalArgumentException
	{
		// adjust to the new implementation class names
		if (typeStr.equals ("jaba.graph.icfg.ICFG"))
		{
			typeStr = "jaba.graph.icfg.ICFGImpl";
		}
		if (typeStr.equals ("jaba.graph.CallGraph"))
		{
			typeStr = "jaba.graph.CallGraphImpl";
		}
		if (typeStr.equals ("jaba.graph.ClassHierarchyGraph"))
		{
			typeStr = "jaba.graph.ClassHierarchyGraphImpl";
		}

		java.lang.Class type = null;

		try
		{
			type = java.lang.Class.forName (typeStr);
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace ();
			throw new RuntimeException ("ClassNotFoundException: " + e);
		}

		try
		{
			java.lang.Class c = java.lang.Class.forName ("jaba.sym.ProgramAttribute");
			if (!c.isAssignableFrom (type))
			{
				throw new IllegalArgumentException
					(
					  "Program.getAttributeOfType (): "
					+ type.getName ()
					+ " is not a subtype of ProgramAttribute"
					);
			}
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace ();
			throw new RuntimeException (e);
		}

		for (Enumeration e = attributes.elements (); e.hasMoreElements ();)
		{
			ProgramAttribute pa = (ProgramAttribute) e.nextElement ();
			java.lang.Class storedAttrClass = pa.getClass ();
			if (type.isAssignableFrom (storedAttrClass))
			{
				return pa;
			}
		}

		if (typeStr.equals ("jaba.graph.icfg.ICFGImpl")) {
		    return(jaba.graph.icfg.ICFGImpl.load(this));
		}
		if (typeStr.equals ("jaba.graph.CallGraphImpl")) {
		    return(jaba.graph.CallGraphImpl.load(this));
		}
		if (typeStr.equals ("jaba.graph.ClassHierarchyGraphImpl")) {
		    return(jaba.graph.ClassHierarchyGraphImpl.load(this));
		}
		System.err.println("JIMJONES: ProgramImpl: typeStr = "+typeStr);
		// attribute not found... now check if it is a DemandDrivenAttribute.
		// If so, create it and initialize it.
		try
		{
			java.lang.Class dda = java.lang.Class.forName ("jaba.sym.DemandDrivenAttribute");
			if (dda.isAssignableFrom (type))
			{
				ProgramAttribute attr = null;
				try
				{
					java.lang.Class cc [] = new java.lang.Class [1];
					cc [0] = java.lang.Class.forName ("jaba.sym.Program");
					java.lang.reflect.Method m = type.getMethod ("load", cc);
					attr = (ProgramAttribute) m.invoke (null, new Object [] {this});
				}
				catch (NoSuchMethodException e1)
				{
					e1.printStackTrace ();
					throw new RuntimeException ("NoSuchMethodException: " + e1);
				}
				catch (IllegalAccessException e2)
				{
					e2.printStackTrace ();
					throw new RuntimeException ("IllegalAccessException: " + e2);
				}
				catch (java.lang.reflect.InvocationTargetException e3)
				{
					e3.printStackTrace ();
					throw new RuntimeException ("InvocationTargetException: " + e3);
				}

				// add demand-driven attribute to program
				addAttribute (attr);

				return attr;
			}
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace ();
			throw new RuntimeException (e);
		}

		return null;
	}

    /**
     * Removes the attribute of the specified type that may be associated
     * with a program.
     * @param typeStr type of program attribute. Must be a
     *                subtype of <code>ProgramAttribute</code>.
     * @return <code>true</code> if program contained an
     *         attribute of the specified type, or
     *         <code>false</code> if program has no attribute of
     *         the specified type.
     * @throws IllegalArgumentException if <code>type</code> is not a subtype
     *                           of <code>ProgramAttribute</code>.
     */
    public boolean removeAttributeOfType( String typeStr )
        throws IllegalArgumentException
    {
	java.lang.Class type = null;
	try {
	    type = java.lang.Class.forName( typeStr );
	}catch ( ClassNotFoundException e){
	    e.printStackTrace();
	    throw new RuntimeException( "ClassNotFoundException: " + e );
	}

	try
	{
            java.lang.Class c = java.lang.Class.forName ("jaba.sym.ProgramAttribute");
            if ( !c.isAssignableFrom( type ) ) {
                throw new IllegalArgumentException ("Program.removeAttributeOfType(): "
						+ type.getName ()
						+ " is not a subtype of ProgramAttribute"
						);
            }
            int i = 0;
            for (Enumeration e = attributes.elements (); e.hasMoreElements (); i++)
	    {
                java.lang.Class storedAttrClass = e.nextElement ().getClass();
                if ( type.isAssignableFrom( storedAttrClass ) ) {
                    attributes.remove( i );
                    return true;
                }
            }
	}
	catch (ClassNotFoundException e)
	{
		e.printStackTrace ();
		throw new RuntimeException (e);
	}

        return false;
    }
    
    /**
     * Returns the number of attributes associated with a program.
     * @return Number of attributes associated with program.
     */
    public int getAttributeCount()
    {
        return attributes.size();
    }

    /**
     * Returns an array of <code>ProgramAttribute</code>s
     * associated with a program.
     * @return Array of attributes associated with program.
     * @see ProgramAttribute
     */
    public ProgramAttribute[] getAttributes()
    {
        ProgramAttribute[] a = new ProgramAttribute[attributes.size()];
	int i = 0;
        for (Enumeration e = attributes.elements (); e.hasMoreElements (); i++)
	{
            a [i] = (ProgramAttribute) e.nextElement ();
        }
        return a;
    }

    /**
     * Sets boolean to indicate that PNRC analysis has been performed for
     * the program.
     */
    public void setIsPNRCAnalysisDone()
    {
        isPNRCAnalysisDone = true;
    }

    /**
     * Returns whether PNRC analysis has been performed for the program.
     * @return <code>true</code>, if PNRC analysis has been performed;
     *         <code>false</code> otherwise.
     */
    public boolean getIsPNRCAnalysisDone()
    {
        return isPNRCAnalysisDone;
    }

    /**
     * Returns the symbol table for this program.
     * @return  The symbol table for this program.
     */
    public SymbolTable getSymbolTable()
    {
	return symTable;
    }

    /**
     * Returns a classloader that can load classes from this Program.
     * <br><br>
     * @return the classloader
     */
    public ClassLoader getClassLoader()
    {
	List classPaths = rcFile.getClassPathsList ();
	URL[] urls = new URL [classPaths.size ()];
	int i = 0;
 	for (Iterator e = classPaths.iterator (); e.hasNext (); i++)
	{
		String cp = (String) e.next ();
 	    try
	    {
 		urls[i] = new File (cp).toURL ();
 	    }
	    catch (IOException ioe)
	    {
		throw new RuntimeException( "Error creating URL from classpath \"" + cp + 
					    "\" in rc file.\nAborting.\n" + ioe
					);
	    }
 	}

 	ClassLoader loader = new URLClassLoader (urls);

	return loader;
    }

    /**
     * Returns the resource file for this Program object.
     * @return The resource file for this Program object.
     */
    public ResourceFileI getResourceFile()
    {
	return rcFile;
    }

    /**
     * Returns the Resources for this Program object.
     * @return The Resources for this Program object.
     */
    public ResourceFileI getResources ()
    {
	return rcFile;
    }

	/** returns a string version of this program */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<program>");

		buf.append ("<name>").append (jaba.tools.Util.toXMLValid (getName ())).append ("</name>");

		buf.append ("<namedreferencetypes>");
		if (namedRefTypes != null)
		{
			for (Enumeration e = namedRefTypes.elements (); e.hasMoreElements ();)
			{
				NamedReferenceType t = (NamedReferenceType) e.nextElement ();

				buf.append ("<namedreferencetype>");
				buf.append (jaba.tools.Util.toXMLValid (t.getName ()));
				buf.append ("</namedreferencetype>");
			}
		}
		buf.append ("</namedreferencetypes>");

		buf.append ("</program>");

		return buf.toString ();
	}

	/** legacy variable no longer used. */
    public static boolean analysisPattern = false;

    /** Classes and interfaces defined in this program. */
    private Vector namedRefTypes;

    /** Attributes of this program. */
    private Vector attributes;

    /** Boolean to indicate if PNRC analysis has been performed for this program. */
    private boolean isPNRCAnalysisDone;

    /** Symbol table for this program. */
    private SymbolTable symTable;

    /** The resourcefile that defined this Program */
    private ResourceFileI rcFile = null;

	/** The set of options used to create this Program and derived objects */
	private Options options = null;
}
