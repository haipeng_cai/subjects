package jaba.sym;

/**
 * Provides a Comparator for ordering a list of SymbolTableEntry objects in order.
 * objects are ordered the internal node id.
 * @author Jay Lofstead 2003/06/16 created
 */
public class SymbolTableEntrySortComparator implements java.util.Comparator
{
	public int compare (Object l, Object r)
	{
		SymbolTableEntry lste = (SymbolTableEntry) l;
		SymbolTableEntry rste = (SymbolTableEntry) r;

		return lste.getID () - rste.getID ();
	}

	public boolean equals (Object o)
	{
		return false;
	}
}
