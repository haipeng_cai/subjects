/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import java.util.Collection;

/**
 * Defines an interface for types that can contain inner classes and inner
 * interfaces -- classes, interfaces, and methods.
 * @author Jim Jones
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 */
public interface ContainsInnerNamedReferenceTypes
{
  /**
   * Adds a named type (class or interface) to list of inner named types of
   * this class.
   * @param innerNamedReferenceType  Inner class or interface to be added.
   * @exception IllegalArgumentException   Thrown if innerNamedReferenceType 
   *                                       is null.
   */
  public void addInnerNamedReferenceType (NamedReferenceType innerNamedReferenceType);

  /**
   * Returns inner classes and interfaces contained in this
   * class.
   * @return Vector of NamedReferenceType objects representing the
   * Inner classes and interfaces of this class.
   */
  public Collection getInnerNamedReferenceTypesCollection ();

  /**
   * Convenience method for converting the Vector to an Array (less efficient) from <code>getInnerNamedReferenceTypesVector</code>
   * @return Vector of NamedReferenceType objects representing the
   * Inner classes and interfaces of this class.
   */
  public NamedReferenceType [] getInnerNamedReferenceTypes ();
}
