/* Copyright (c) 2002, Georgia Institute of Technology */

package jaba.sym;

/**
 * Interface for attributes of a SourceFile.
 * @author Jim Jones
 */
public interface SourceFileAttribute extends Attribute
{

}
