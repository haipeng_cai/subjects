/* Copyright (c) 2002, Georgia Institute of Technology */

package jaba.sym;

import java.util.Vector;

/**
 * Represents a source file.  Can be used if <i>SourceClassPath</i> is
 * defined in the ResourceFile.
 * @author Jim Jones -- <i>Created.  September 9, 2002</i>
 * @author Jay Lofstead 2003/06/02 changed elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 changed forName to check the directory structure as well as the default directory
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/16 changed toString to output XML valid text
 * @author Jay Lofstead 2003/07/10 changed to be an interface
 */
public interface SourceFile extends java.io.Serializable
{
    /**
     * Returns the specified line from the file.  The first line is
     * line 1.  If a line is requested that is beyond the number of
     * lines in the program, then an ArrayOutOfBounds exception will
     * be thrown.
     * @param line  The number of the line to be returned.
     * @return The specified line from the file.
     */
    public String getLine( int line );

    /**
     * Returns the name of the file using the full package name/class
     * name with File.separatorChar between directory/packages, and
     * without the ".java" extension -- example: jaba/sym/SourceFile.
     * Note, this may not be the same name as the class (a class may
     * be defined in a file named differently).
     * @return The name of the file.
     */
    public String getName();

    /**
     * Returns the number of lines in the source file.  Remember that
     * the lines are numbered 1 to N rather than 0 to N-1.
     * @return The number of lines in the source file.
     */
    public int getNumLines();
    
    /**
     * Returns the set of classes/interfaces that are defined in this
     * source file.  Note that this may not be an exhaustive list of
     * all classes and interfaces that are defined in this source
     * file.  Classes and interfaces are added to this list as
     * NamedReferenceType.getSourceFile() is called.
     * @return The set of classes/interfaces that are defined in this
     * source file.
     */
    public Vector getNamedReferenceTypesVector ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getNamedReferenceTypesVector</code>
     * @return The set of classes/interfaces that are defined in this
     * source file.
     */
	public NamedReferenceType [] getNamedReferenceTypes ();

    /**
     * Returns all of the lines in the source file.  Each element of
     * the array, in ascending order, contains the entire text of the
     * source file (except the newlines between the lines).
     * @return An array of all of the lines in the source file.
     */
    public Vector getLinesVector ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getLinesVector</code>
     * @return An array of all of the lines in the source file.
     */
	public String [] getLines ();

    /**
     * Returns the text of the file as one long string.
     * @return The text of the source file.
     */
	public String toString ();

	/** ??? */
    public void addNamedReferenceType( NamedReferenceType type );

   /**
    * Adds an attribute to this SourceFile.
    * @param attribute attribute to add to this SourceFile.
    */
    public void addAttribute( SourceFileAttribute attribute );

    /**
     * Returns the attributes that have been associated with this SourceFile.
     * @return The attributes that have been associated with this SourceFile.
     */
    public Vector getAttributesVector ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getAttributesVector</code>
     * @return The attributes that have been associated with this SourceFile.
     */
	public SourceFileAttribute [] getAttributes ();
}
