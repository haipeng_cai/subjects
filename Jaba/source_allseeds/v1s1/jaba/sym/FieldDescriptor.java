/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Represents a Java field descriptor.
 *
 * @author S. Sinha
 */

public class FieldDescriptor extends Descriptor {

    // Constructor

    /**
     * Creates a field descriptor object, and parses the parameter to extract
     * type information.
     * @param descriptor Java field descriptor.
     * @throws IllegalArgumentException if <code>descriptor</code> is not
     *                                  well-formed.
     */
    public FieldDescriptor( String descriptor ) {
        this.descriptor = new String( descriptor );
        StringBuffer desc = new StringBuffer( descriptor );
        type = parseType( desc, primitiveFieldTypes );
        if ( desc.length() != 0 ) {
            throw new IllegalArgumentException(
                "FieldDescriptor.FieldDescriptor(): Malformed descriptor: "+
                descriptor );
        }
    }


    // Methods

    /**
     * Returns type for a field descriptor.
     * @return type for this field descriptor.
     */
    public TypeEntry getType() {
        return type;
    }

    /**
     * Returns a string representation of this field descriptor.
     * @return string representation of this field descriptor.
     */
    public String toString() {
        return "type = "+type.toString();
    }

}
