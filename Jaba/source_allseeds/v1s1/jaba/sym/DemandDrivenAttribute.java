package jaba.sym;

/**
 * Interface for Attributes that are generated when needed.
 * @author Jim Jones
 * @author Jay Lofstead 2003/07/17 removed legacy load method.
 * 				By virtue of implementing this interface
 * 				is sufficient now with the new factory mechanism.
 * 				It will be assumed that a static load method is provided.
 */
public interface DemandDrivenAttribute extends Attribute
{
}
