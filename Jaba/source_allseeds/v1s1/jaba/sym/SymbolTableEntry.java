/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Abstract class that represents an entry into the symbol table.
 * @author Huaxing Wu -- <i> Revised 7/15/02. Add hashCode() method
 *                        since equals() has been overridden </i>
 * @author Jay Lofstead 2003/05/29 added getNameXMLValid for outputing in XML format
 * @author Jay Lofstead 2003/06/05 changed toString to be an abstract method
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/16 changed to use Util.toXMLValid
 * @author Jay Lofstead 2003/06/16 removed setID in favor of an internally maintained, autogenerated ID
 * @author Jay Lofstead 2003/06/26 moved symbol table entry ID into SymbolTable to eliminate static data memeber
 * @author Jay Lofstead 2003/06/30 added implements SymbolTableEntry
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface SymbolTableEntry
{
    /**
     * Sets the name for a symbol table entry.
     * @param name Name for the entry
     */
    public void setName (String name);

    /**
     * Returns the name of a symbol table entry.
     * @return Name of symbol table entry.
     */
    public String getName ();

    /**
     * Returns the ID of this symbol table entry.
     * @return  ID of this symbol table entry.
     */
    public int getID ();

	/**
	 * Sets the ID of this entry.  This method should only be called by the SymbolTable.
	 */
	public void setID (int newID);

    /**
     * Returns a string representation (not the name) of a symbol table
     * entry. The current implementation simply returns the class of the
     * object on which the method is invoked.  The format should be XML.
     * @return String representation of symbol table entry.
     */
    public String toString ();

	/** ??? */
    public boolean equals (Object o);

    /**
     * Returns a hash code for this entry. The hash code for an entry is 
     * the same as ID
     * @return A hash code for this entry
     */
    public int hashCode ();
}
