/* Copyright (c) 2002, Georgia Institute of Technology */

package jaba.sym;

import jaba.constants.AccessLevel;
import jaba.constants.Modifier;

import java.util.Vector;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Enumeration;
import java.util.Collection;
import java.util.List;
import java.util.LinkedList;

import java.lang.Class;

import java.io.IOException;

/**
 * Class that represents either an interface or a class in the
 * symbol table.
 * @author Jim Jones
 * @author Alex uncommented getContainingPackage
 * @author Huaxing Wu -- <i> revised, 6/12/02. Add pretocted method 
 *                         getParents(). For Class, should return 
 *                         superclass+interfaces. For Interface, should return
 *                         all SuperInterface. This method should be 
 *                         reimplemented
 *                         by all subclasses. </i>
 * @author Huaxing Wu -- <i> Revised, 6/15/02. Remove all the methods and 
 *                              variables related to inherited methods and 
 *                              fields from Class to this class. Change the 
 *                              the method flattern to take multiple parents
 *                              Which for Class, can be superclass+interfaces.
 *                              For Interface, can be all SuperInterface.</i>
 * @author Huaxing Wu -- <i> Revised, 7/9/02. Remove 
 *                              innerNamedReferenceTypesArr, 
 *                              use innerNamedReferenceTypes only.
 *                              Change System.exit(1) to throw RuntimeException
 * @author Huaxing wu -- <i> Revised, 11/20/02. Add findMethod, findFiends methods.</i>
 * @author Jay Lofstead 2003/05/29 changed toString to generate XML
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/26 added implementing Serializable
 * @author Jay Lofstead 2003/06/30 added implement of NamedReferenceType
 * @author Jay Lofstead 2003/07/01 changed to implemented NamedReferenceType
 * @author Jay Lofstead 2003/07/17 updated factory mechanism to newer standard
 */
public class NamedReferenceTypeImpl extends ReferenceTypeImpl
    implements ContainsInnerNamedReferenceTypes, AccessLevel, Modifier, java.io.Serializable, NamedReferenceType
{
    /**
     * Constructor for a NamedReferenceType.
     */
    public NamedReferenceTypeImpl ()
    {
	// initialize all fields
	methods = new HashMap();
	fields = new HashMap();
	accessLevel = A_PUBLIC;
	containingPackage = new PackageImpl (); // defaults to the root package
	innerNamedReferenceTypes = new Vector();
	modifiers = 0;
	isSummarized = false;
	containingType = null;
	attributes = new Vector();
	program = null;
    }
    
    /**
     * Sets the containing program.
     * @param program  The program that contains this NamedReferenceType.
     */
    public void setProgram (Program program)
    {
	this.program = program;
    }

    /**
     * Returns the containing program.
     * @return  The program that contains this NamedReferenceType.
     */
    public Program getProgram ()
    {
	return program;
    }

    /**
     * Adds a method to this class or interface.
     * @param method   The method to be added to this class or interface.
     * @exception IllegalArgumentException   Thrown if <code>method</code> is
     *                                       <code>null</code>, or signature of
     *                                       <code>method</code> is not set.
     */
    public void addMethod (Method method) throws IllegalArgumentException
    {
	// check if method is a null reference, if so, throw an exception
	if ( method == null )
	    throw new IllegalArgumentException( "Exception thrown in " + 
						"jaba.sym." + 
						"NamedReferenceType." + 
						"addMethod()" );
	
	String signature = method.getSignature();
	if ( signature == null ) {
	    throw new IllegalArgumentException( "NamedReferenceType." + 
						"addMethod(): signature of " +
						"method not set" );
	}
	// add the method to the methods vector
	methods.put( signature, method );
	
    }
    
    /**
     * Returns methods in this class or interface.
     * @return   Methods defined in this named reference type.
     */
	public Collection getMethodsCollection ()
	{
		return methods.values ();
	}

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getMethodsCollection</code>
     * @return   Methods defined in this named reference type.
     */
	public Method [] getMethods ()
	{
		Collection c = methods.values ();

		return (Method []) c.toArray (new Method [c.size ()]);
	}

    /**
     * Returns the method from this named-reference type that has the
     * given signature.
     * @param signature signature of method to search for.
     * @return Method with the given signature, or <code>null</code> if none
     *         exists.
     */
    public Method getMethod( String signature ) {
        return (Method)methods.get( signature );
    }
    
    /**
     * Adds a field tho this class or interface.
     * @param field   The field to be added.
     * @exception IllegalArgumentException  Thrown if <code>field</code> is 
     *                                      <code>null</code>, or descriptor of
     *                                      <code>field</code> is not set.
     */
    public void addField( Field field ) throws IllegalArgumentException
    {
	// check if the field is a null reference, if so, throw an exception
	if ( field == null )
	    throw new IllegalArgumentException( "Exception thrown in " + 
						"jaba.sym." + 
						"NamedReferenceType." +
						"addField()" );
	
	String signature = field.getSignature();
	if ( signature == null ) {
	    throw new IllegalArgumentException( "NamedReferenceType." + 
						"addField(): signature of " + 
						"field not set" );
	}
	// add the field
	fields.put( signature, field );
	
    }
    
    /**
     * Returns fields defined directly in this class or interface.
     * @return   Fields defined in this named reference type.
     */
	public Collection getFieldsCollection ()
	{
		return fields.values ();
	}
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getFieldsCollection</code>
     * @return   Fields defined directly in this named reference type.
     */
	public Field [] getFields ()
	{
		Collection c = fields.values ();

		return (Field []) c.toArray (new Field [c.size ()]);
	}
    
    /**
     * Returns the field from defined directly in this named-reference type that has the
     * given signature. A field signature is the field name followed by its
     * type.
     * @param signature signature of field to search for.
     * @return Field with the given signature, or <code>null</code> if none
     *         exists.
     * @see jaba.sym.Member#getSignature
     */
    public Field getField( String signature ) {
        return (Field)fields.get( signature );
    }
    
    /**
     * Assigns an access level value to this class or interface.
     * @param accessLevel  Access level of the class.  Valid values found in
     *                  {@link jaba.constants.AccessLevel AccessLevel}.
     */
    public void setAccessLevel( int accessLevel )
    {
	this.accessLevel = accessLevel;
    }
    
    
    /**
     * Returns the access level of this class or interface.
     * @return   The access level.  Valid values found in
     *           {@link jaba.constants.AccessLevel AccessLevel}.
     */
    public int getAccessLevel()
    {
	return accessLevel;
    }
    
    
    /**
     * Assigns the package for which this class or interface belongs to.
     * @param containingPackage   The package of this class or interface.
     * @exception IllegalArgumentException  Thrown if 
     *                                      <code>containingPackage</code> is
     *                                      <code>null</code>.
     */
    public void setContainingPackage( Package containingPackage ) throws
	IllegalArgumentException
    {
	// check if containingPackage is a null reference, if so, throw an exception
	if ( containingPackage == null )
	    throw new IllegalArgumentException( "Exception thrown in " +
						"jaba.sym." + 
						"NamedReferenceType." +
						"setContainingPackage()" );
	
	// set containing package
	this.containingPackage = containingPackage;
	
    }
    
    
    /**
     * Returns the containing package of this class or interface.
     * @return   The package that this class or interface belongs to.
     */
    public Package getContainingPackage() throws IllegalArgumentException
    {
	return containingPackage;
    }
    
    
    /**
     * Adds a class or interface to the list of inner classes or interfaces 
     * that this contains.
     * @param innerNamedReferenceType  The inner reference type that is 
     *                                 contained in this one.
     * @exception IllegalArgumentException  Thrown if the param is <code>null
     *                                      </code>.
     */
    public void addInnerNamedReferenceType( NamedReferenceType 
					    innerNamedReferenceType ) 
	throws IllegalArgumentException
    {
	// check if the param is a null reference, if so, throw an exception
	if ( innerNamedReferenceType == null )
	    throw new IllegalArgumentException( "Exception thrown in " + 
						"jaba.sym." + 
						"NamedReferenceType." +
						"addInnerNamedReferenceType()"
						);
	
	innerNamedReferenceTypes.addElement( (Object)innerNamedReferenceType );
	
    }

    /**
     * Returns inner named reference types in this class or
     * interface.
     * @return  Inner named reference types.
     */
    public Collection getInnerNamedReferenceTypesCollection ()
    {
	return innerNamedReferenceTypes;
    }

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from
     * <code>getInnerNamedReferenceTypesCollection</code>
     * @return  Inner named reference types.
     */
    public NamedReferenceType [] getInnerNamedReferenceTypes ()
    {
	return (NamedReferenceType []) innerNamedReferenceTypes.toArray (new NamedReferenceType [innerNamedReferenceTypes.size ()]);
    }

    /**
     * Sets modifiers for this class.
     * @param modifier  Valid integer contants can be found in
     *                  {@link jaba.constants.Modifier Modifier}.
     */
    public void setModifiers( int modifiers )
    {
	this.modifiers = modifiers;
    }
    
    
    /**
     * Returns a bitmap of modifier constants.
     * @return  Bitmap of modifier constants.  Valid values can be found in
     *          {@link jaba.constants.Modifier Modifier}.
     */
    public int getModifiers()
    {
	return modifiers;
    }
    
    
    /**
     * Sets whether this class or interface is summarized.
     * Library named reference is summarized. Subject named reference is not.
     * @param isSummarized  Boolean indicating whether this class or interface
     *                      is summarized.
     */
    public void setIsSummarized( boolean isSummarized )
    {
	this.isSummarized = isSummarized;
    }
    
    
    /**
     * Returns whether this class or interface is summarized.
     * Library named reference is summarized. Subject named reference is not.
     * @return  A boolean indicating whether this class or interface is
     *          summarized.
     */
    public boolean isSummarized()
    {
	return isSummarized;
    }
    
    
    /**
     * Sets the class, interface, or method for which this class is an inner 
     * class.
     * @param containingType  The type in which this class is an inner class, 
     *                        or null if this class is not an inner class.
     */
    public void setContainingType( ContainsInnerNamedReferenceTypes 
				   containingType )
    {
	this.containingType = containingType;
    }
    
    
    /**
     * Get the class, interface, or method for which this class is an inner
     * class.
     * @return The type for which this class is an inner class or null if this 
     *         class is not an inner class
     */
    public SymbolTableEntry getContainingType ()
    {
	return (SymbolTableEntry) containingType;
    }
    
	/**
	 * Returns the attribute of the specified type that may be associated
	 * with a class or an interface.
	 * @param typeStr type of class or interface attribute.
	 * @return Class or interface attribute of the specified type, or
	 *         <code>null</code> if class or interface has no attribute of
	 *         the specified type.
	 */
	public NamedReferenceTypeAttribute getAttributeOfType (String typeStr)
	{
		if (typeStr.equals ("jaba.classfile.ClassFile"))
		{
			typeStr = "jaba.classfile.ClassFileImpl";
		}

		java.lang.Class type = null;
		try
		{
			type = java.lang.Class.forName (typeStr);
		}
		catch (ClassNotFoundException e)
		{
			throw new RuntimeException (e);
		}

		try
		{
			Class c = java.lang.Class.forName ("jaba.sym.NamedReferenceTypeAttribute");
			if (!c.isAssignableFrom (type))
			{
				throw new IllegalArgumentException ("NamedReferenceType.getAttributeOfType(): "
								+ type.getName()
								+ " is not a subtype of NamedReferenceTypeAttribute"
								);
			}
		}
		catch (ClassNotFoundException cnfe)
		{
			cnfe.printStackTrace ();
			throw new RuntimeException (cnfe);
		}

		for (Enumeration e = attributes.elements (); e.hasMoreElements ();)
		{
			NamedReferenceTypeAttribute nrta = (NamedReferenceTypeAttribute) e.nextElement ();
			Class storedAttrClass = nrta.getClass ();
			if (type.isAssignableFrom (storedAttrClass))
			{
				return nrta;
			}
		}

		// attribute not found... now check if it is a DemandDrivenAttribute.
		// If so, create it and initialize it.
		try
		{
			java.lang.Class dda =  java.lang.Class.forName ("jaba.sym.DemandDrivenAttribute");
			if (dda.isAssignableFrom (type))
			{
				if (isSummarized ())
				{
					return null;
				}

				NamedReferenceTypeAttribute attr = null;
				try
				{
					java.lang.Class cc [] = new java.lang.Class [1];
					cc [0] = java.lang.Class.forName ("jaba.sym.NamedReferenceType");
					java.lang.reflect.Method m = type.getMethod ("load", cc);
					attr = (NamedReferenceTypeAttribute) m.invoke (null, new Object [] {this});
				}
				catch (NoSuchMethodException e1)
				{
					throw new RuntimeException (e1);
				}
				catch (IllegalAccessException e2)
				{
					throw new RuntimeException (e2);
				}
				catch (java.lang.reflect.InvocationTargetException e3)
				{
					throw new RuntimeException (e3);
				}

				// add demand-driven attribute to the class or interface
				addAttribute (attr);

				return attr;
			}
		}
		catch (ClassNotFoundException cnfe)
		{
			cnfe.printStackTrace ();
			throw new RuntimeException (cnfe);
		}

		return null;
	}
    
    
    /**
     * Removes the attribute of the specified type that may be associated
     * with a class or an interface.
     * @param typeStr type of class or interface attribute. Must be a
     *                subtype of <code>NamedReferenceTypeAttribute</code>.
     * @return <code>true</code> if class or interface contained an
     *         attribute of the specified type, or
     *         <code>false</code> if class or interface has no attribute of
     *         the specified type.
     * @throws IllegalArgumentException if <code>type</code> is not a subtype
     *                           of <code>NamedReferenceTypeAttribute</code>.
     */
    public boolean removeAttributeOfType( String typeStr ) throws IllegalArgumentException
    {
	Class type = null;
	try {
	    type = Class.forName( typeStr );
	}catch ( ClassNotFoundException e){
	    throw new RuntimeException(e);
	}

	try
	{
            Class c = java.lang.Class.forName ("jaba.sym.NamedReferenceTypeAttribute");
            if ( !c.isAssignableFrom( type ) ) {
                throw new IllegalArgumentException ( "NamedReferenceType.removeAttributeOfType(): "
						+ type.getName ()
						+ " is not a subtype of NamedReferenceTypeAttribute"
						);
            }
            int i = 0;
            for (Enumeration e = attributes.elements (); e.hasMoreElements (); i++)
	    {
                Class storedAttrClass = e.nextElement ().getClass();
                if ( type.isAssignableFrom( storedAttrClass ) ) {
                    attributes.remove( i );
                    return true;
                }
            }
	}
	catch (ClassNotFoundException cnfe)
	{
		cnfe.printStackTrace ();
		throw new RuntimeException (cnfe);
	}

        return false;
    }
    
    /**
     * Adds an attribute to a class or an interface. This method is invoked
     * by the <code>addAttribute()</code> method defined in <code>Class</code>
     * or <code>Interface</code>.
     * @param attribute attribute to add to a class or interface.
     * @throws IllegalArgumentException if <code>attribute</code> is
     *                                  <code>null</code>.
     */
    public void addAttribute( NamedReferenceTypeAttribute attribute ) throws IllegalArgumentException
    {
        if ( attribute == null ) {
            throw new IllegalArgumentException( "NamedReferenceType." + 
						"addAttribute(): attribute " +
						"is null" );
        }
        attributes.addElement( attribute );
    }
    
    /**
     * Returns all attributes of a class or interface. This method is invoked
     * by the <code>getAttributes()</code> method defined in
     * <code>Class</code> or <code>Interface</code>.
     * @return Collection of class or interface attributes.
     */
    protected Vector getNamedReferenceTypeAttributes()
    {
        return attributes;
    }
    
    /**
     * Returns the number of attributes associated with a named-reference type.
     * @return Number of attributes associated with named-reference type.
     */
    public int getAttributeCount() {
        return attributes.size();
    }
    
    /**
     * Returns a string for this object.
     * @return  String representing this object.
     */
	public String toString ()
	{
		StringBuffer buffer = new StringBuffer ();

		buffer.append ("<namedreferencetype>");
		for (Iterator iter = fields.values ().iterator (); iter.hasNext ();)
		{
			buffer.append ("<field>");
			buffer.append (((Field) iter.next ()).getID ());
			buffer.append ("</field>");
		}
		for (Iterator iter = methods.values ().iterator (); iter.hasNext ();)
		{
			buffer.append ("<method>");
			buffer.append (((Method) iter.next ()).getID ());
			buffer.append ("</method>");
		} 

		buffer.append ("<accesslevel>");
		if ((accessLevel & A_PUBLIC) != 0)
		{
			buffer.append ("public");
		}
		else
		{
			if ((accessLevel & A_PRIVATE) != 0)
			{
				buffer.append ("private");
			}
			else
			{
				if ((accessLevel & A_PROTECTED) != 0)
				{
					buffer.append ("protected");
				}
				else
				{
					buffer.append ("package");
				}
			}
		}
		buffer.append ("</accesslevel>");

		if ((modifiers & M_ABSTRACT) != 0)
		{
			buffer.append ("<modifier>");
			buffer.append ("abstract");
			buffer.append ("</modifier>");
		}
		if ((modifiers & M_FINAL) != 0)
		{
			buffer.append ("<modifier>");
			buffer.append ("final");
			buffer.append ("</modifier>");
		}
		if ((modifiers & M_SUPER) != 0)
		{
			buffer.append ("<modifier>");
			buffer.append ("super");
			buffer.append ("</modifier>");
		}
		if ((modifiers & M_INTERFACE) != 0)
		{
			buffer.append ("<modifier>");
			buffer.append ("interface");
			buffer.append ("</modifier>");
		}

		for (Enumeration i = innerNamedReferenceTypes.elements (); i.hasMoreElements ();)
		{
			buffer.append ("<innernamedreferencetype>");
			buffer.append (((NamedReferenceType) i.nextElement ()).getID ());
			buffer.append ("</innernamedreferencetype>");
		}

		if (containingType != null)
		{
			buffer.append ("<containingtype>" + ((SymbolTableEntry) containingType).getID () + "</containingtype>");
		}
		buffer.append ("<issummarized>" + (isSummarized ? "yes" : "no") + "</issummarized>");

		buffer.append ("</namedreferencetype>");

		return buffer.toString();
	}

   /**
    * Adds an inherited method to this class.
    * @param method   The method to be added to this class.
    * @exception IllegalArgumentException   Thrown if <code>method</code> is
    *                                       <code>null</code>, or signature of
    *                                       <code>method</code> is not set.
    */
    public void addInheritedMethod( Method method ) 
	throws IllegalArgumentException
    {
	// check if method is a null reference, if so, throw an exception
	if ( method == null ) {
	    throw new IllegalArgumentException( "Exception thrown in " +
						"jaba.sym.Class." +
						"addInheritedMethod(): " +
						"parameter method is null.");
	}
	String signature = method.getSignature();
	if ( signature == null ) {
	    throw new IllegalArgumentException( "Class." + 
						"addInheritedMethod(): " + 
						"signature of method not " + 
						"set" );
	}
	// add the method to the methods vector
	inheritedMethods.put( signature, method );
    }

    /** Returns methods that can be called from this class.
     *  will be methods + inheritedmethods
     *  @return All available methods of this class
     */
	public Collection getAllMethodsCollection ()
	{
		LinkedList ll = new LinkedList ();
		ll.addAll (getMethodsCollection ());
		ll.addAll (getInheritedMethodsCollection ());

		return ll;
	}

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getAllMethodsList</code>
     *  @return All available methods of this class
     */
	public Method [] getAllMethods ()
	{
		LinkedList ll = new LinkedList ();
		ll.addAll (getMethodsCollection ());
		ll.addAll (getInheritedMethodsCollection ());

		return (Method []) ll.toArray (new Method [ll.size ()]);
	}

    /**
     * Returns methods inherited by this class.
     * @return Methods inherited by this class.
     */
	public Collection getInheritedMethodsCollection ()
	{
		if(inheritedMethods == null)
		{
			inheritedMethods = new HashMap ();
			inheritedFields = new HashMap ();

			flatten ();
		}

		return inheritedMethods.values ();
	}


    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getInheritedMethodsCollection</code>
     * @return Methods inherited by this class.
     */
	public Method [] getInheritedMethods()
	{
		if(inheritedMethods == null)
		{
			inheritedMethods = new HashMap ();
			inheritedFields = new HashMap ();

			flatten ();
		}

		Collection c = inheritedMethods.values ();

		return (Method []) c.toArray (new Method [c.size ()]);
	}
    
    /**
     * Returns the method object for an inherited method given the signature
     * for the inherited method.
     * @param signature  The signature of the inherited method.
     * @return  The method object for an inherited method given the signature
     *          for the inherited method.  If no inherited methods match the
     *          signature specified, <code>null</code> is returned.
     */
    public Method getInheritedMethod( String signature )
    {
	if ( inheritedMethods == null ) {
	    inheritedMethods = new HashMap();
	    inheritedFields = new HashMap();
	    flatten();
	}
	return (Method) inheritedMethods.get( signature );
    }
     
    /**
     * return the method decalred in this type and its inherited types given 
     * its signature.
     * @param signature signature of method to search for.
     * @return Method with the given signature, or <code>null</code> if none
     *         exists.
     */
    public Method findMethod (String signature)
    {
	Method result = getMethod(signature);
	if(result == null)
	    result = (Method) getInheritedMethod(signature);

	return result;

    }

    /**
     * Adds an inherited field to this class.
     * @param field   The field to be added.
     * @exception IllegalArgumentException  Thrown if <code>field</code> is 
     *                                      <code>null</code>, or descriptor of
     *                                      <code>field</code> is not set.
     */
    public void addInheritedField( Field field ) 
	throws IllegalArgumentException
    {
	// check if the field is a null reference, if so, throw an exception
	if ( field == null )
	    throw new IllegalArgumentException( "Exception thrown in " +
						"jaba.sym.Class." +
						"addInheritedField(): " +
						"parameter field is null.");
	
	String signature = field.getSignature();
	if ( signature == null ) {
	    throw new IllegalArgumentException( "Class.addField(): " + 
						"signature of field not set" );
	}
	// add the field
	inheritedFields.put( signature, field );
	
    }

    /** Returns fields that can be called from this class.
     *  will be fields + inheritedfieldss
     *  @return All available fields of this class
     */
    public Collection getAllFieldsCollection ()
    {
	Collection fields = getFieldsCollection ();
	Collection inherited = getInheritedFieldsCollection ();

	List ll = new LinkedList ();
	ll.addAll (fields);
	ll.addAll (inherited);

	return ll;
    }

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getAllFieldsCollection</code>
     *  @return All available fields of this class
     */
    public Field [] getAllFields()
	{
		Collection fields = getFieldsCollection ();
		Collection inherited = getInheritedFieldsCollection ();

		List ll = new LinkedList ();
		ll.addAll (fields);
		ll.addAll (inherited);

		return (Field []) ll.toArray (new Field [ll.size ()]);
	}

    /**
     * Returns fields inherited by this class.
     * @return Fields inherited by this class.
     */
	public Collection getInheritedFieldsCollection ()
	{
		if(inheritedFields == null)
		{
			inheritedFields = new HashMap ();
			inheritedMethods = new HashMap ();

			flatten ();
		}

		return inheritedFields.values ();
	}  

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getInheritedFieldsCollection</code>
     * @return Fields inherited by this class.
     */
	public Field [] getInheritedFields ()
	{
		if(inheritedFields == null)
		{
			inheritedFields = new HashMap ();
			inheritedMethods = new HashMap ();

			flatten ();
		}

		Collection c = inheritedFields.values ();

		return (Field []) c.toArray (new Field [c.size ()]);
	}  
  
   /**
    * Returns the field object for an inherited field given the signature
    * for the inherited field.
    * @param signature  The signature of the inherited field.
    * @return  The method object for an inherited field given the signature
    *          for the inherited field.  If no inherited fields match the
    *          signature specified, <code>null</code> is returned.
    */
    public Field getInheritedField(String signature) {
	if ( inheritedFields == null ) {
	    inheritedFields = new HashMap();
	    inheritedMethods = new HashMap();
	    flatten();
	}
	return (Field)inheritedFields.get( signature );
    }

   /**
    * return the field decalred in this type and its inherited types given 
    * its signature.
    * @param signature signature of field to search for.
    * @return Method with the given signature, or <code>null</code> if none
    *         exists.
    */
    public Field findField(String signature)
   {
	Field result = getField(signature);
	if(result == null)
	    result = (jaba.sym.Field) getInheritedField(signature);

	return result;

    }

    /**
     * Sets the source file name.
     * @param The name of the source file.
     */
    public void setSourceFileName( String name ) {
	sourceFileName = name;
    }

    /**
     * Returns the source file name if set, and "" if not.
     * @return The source file name if specified in the classfile, and
     * "" if not.
     */
    public String getSourceFileName() {
	return sourceFileName;
    }

    /**
     * Returns the SourceFile object for this class/interface.
     * @return  SourceFile object for this class/interface.
     * @throws IOException thrown if an IOException occurs in opening
     * the SourceFile
     */
    public SourceFile getSourceFile() throws IOException {
	return SourceFileImpl.forName( this );
    }

   /**
    * Traverses the hierarchy of classes and interfaces upto the topmost 
    * interfaces and classes 
    * and then flattens the methods and fields between the 
    * topmost interfaces/classes and this NamedReferencedType.
    */
    protected void flatten()
    {
	Collection parentInterfaces = getParentsCollection ();

      	if(parentInterfaces == null || parentInterfaces.size () == 0)
	{
	    return;
	}

	for (Iterator k = parentInterfaces.iterator (); k.hasNext ();)
	{
	     NamedReferenceType parent = (NamedReferenceType) k.next ();
	     if(parent == null) {
		 System.out.println (this);
	     }

	    // Add all non-private methods of the parent of this class to this class.
	    Collection arrayDefMethods = parent.getMethodsCollection ();
	    for (Iterator i = arrayDefMethods.iterator (); i.hasNext ();)
	    {
		Method m = (Method) i.next ();
		if (m.getAccessLevel() != A_PRIVATE)
		{
		    // Check if method is not redefined in this class.
		    if((getMethod(m.getSignature())) == null)
		    {
			addInheritedMethod(m);
		    }
		}
	    }
	    // Add all inherited methods of the parent of this class to this class.
	    Collection arrayInhMethods = parent.getInheritedMethodsCollection ();
	    for (Iterator i = arrayInhMethods.iterator (); i.hasNext ();)
	    {
		Method m = (Method) i.next ();
		if ((getMethod (m.getSignature())) == null)
		{
		    addInheritedMethod (m);
		}
		
	    }
	
	    // Add all non-private fields of the parent of this class to this class.
	    Collection arrayDefFields = parent.getFieldsCollection ();
	    for (Iterator i = arrayDefFields.iterator (); i.hasNext ();)
	    {
		Field f = (Field) i.next ();
		if (f.getAccessLevel() != A_PRIVATE)
		{
		    addInheritedField (f);
		}
	    }
	
	    // Add all inherited fields of the parent of this class to this class.
	    Collection arrayInhFields = parent.getInheritedFieldsCollection ();
	    for (Iterator i = arrayInhFields.iterator (); i.hasNext ();)
	    {
		addInheritedField ((Field) i.next ());
	    }
	}
    }

    /**
     * Get the superclass and the immidate implemented interface.
     * This method should be overridden by all the subclass.
     */
    protected Collection getParentsCollection () {throw new UnsupportedOperationException ("Subclasses should override this method");}

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getParentsCollection</code>
     * This method should be overridden by all the subclass.
     */
    protected NamedReferenceType [] getParents () {throw new UnsupportedOperationException ("Subclasses should override this method");}

    /**
     * All loaded NamedreferenceType 
     * Key: name, Value: NamedReferenceType 
     */
    private static final HashMap namedRefs = new HashMap ();

    /**
     * All methods in this class or interface. 
     * Key: String Signature, Value: Method
     */
    private HashMap methods;
    
    /**
     * All fields in this class or interface. 
     * Key: String Signature, Value: Field 
     */
    private HashMap fields;
    
    /**
     * All methods inherited by this class.
     * Key: String Signature, Value: Method 
     */
    private HashMap inheritedMethods = null;

    /**
     * All fields inherited by this class. 
     * Key: String Signature, Value: Field 
     */
    private HashMap inheritedFields = null;
    
    /** Access level of this class or interface e.g., public, private, etc. */
    private int accessLevel;
    
    /** The package that this class or interface belongs to. */
    private Package containingPackage;
    
    /** All inner NamedReferenceTypes contained in this class or interface. */
    private Vector innerNamedReferenceTypes;
  
    /** Attributes of this class or interface. */
    private Vector attributes;
    
    /** 
     * If this is an inner named reference type, this is the named reference
     * type in which this is nested.  If not, this should be null.
     */
    private ContainsInnerNamedReferenceTypes containingType;
    
    /** Bit mask of modifiers applied to this class or interface. */
    private int modifiers;
    
    /** 
     * Boolean indicating whether this class or interface had all debugging
     * options analyzed.  When <code>true</code>, we assume that the debugging
     * fields were not available in the classfile.
     */
    private boolean isSummarized;
    
    /** Program that contains this NamedReferenceType. */
    private Program program;

    /** The name of the source file (initialized to "") */
    private String sourceFileName = "";
}
