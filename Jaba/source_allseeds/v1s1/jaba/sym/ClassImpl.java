/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import java.util.Collection;
import java.util.Vector;
import java.util.Iterator;
import java.util.Arrays;

/**
 * Represents a class in the symbol table.
 * @author Jim Jones -- <i>Created.  Feb. 1999.</i>
 * @author Ashish Gujarathi -- <i>Revised, Apr. 2, 1999, Added 
 *                             inheritedMethods and inheritedFields fields, 
 *                             and their accessors, and the private flatten() 
 *                             method to perform "class flattening".</i>
 * @author Manas Tungare -- <i>Revised, Feb 21, 2002. Added
 *                          method getFullyQualifiedName.</i>
 * @author Huaxing Wu -- <i> Revised, 6/15/02. (a) remove preprocessor
 *                           (b) remove vArr, only use v to contain info and 
 *			        send back info to caller. In this class  v
 *				are: interfaces, constructors and subclasses.
 *			    (c) remove all methods and variables related to
 *			        inherited methods and fields to base class
 *				NamedReferenceType since Another subClass 
 *				Interface also need them. </i>
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/02 changed use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/06 removed Quick Sort class reference.
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/06/26 added implementing Serializable
 * @author Jay Lofstead 2003/06/30 added implements Class
 * @author Jay Lofstead 2003/07/01 changed to implement Class
 */
public class ClassImpl extends NamedReferenceTypeImpl implements java.io.Serializable, Class
{
    /**
     * Constructor for Class.
     */
    public ClassImpl ()
    {
	// initialize all fields
	superClass = null;
	interfaces = new Vector();
	constructors = new Vector();
	subClasses = new Vector();
    }
    
    /**
     * Sets the superclass for this class.
     * @param superClass  The superclass of this class.
     * @exception IllegalArgumentException  Thrown if superClass is null.
     */
    public void setSuperClass( Class superClass ) 
	throws IllegalArgumentException
    {
	// check if superClass is null, if so, throw an exception
	if ( superClass == null )
	    throw new IllegalArgumentException( "Exception thrown in " + 
						"jaba.sym.Class." + 
						"setSuperClass()" );
	
	// set the superclass
	this.superClass = superClass;
	
    }
    
    /**
     * Get the superclass for this class.
     * @return The superclass of this class.
     */
    public Class getSuperClass()
    {
	return superClass;
    }
    
    /**
     * Adds an interface for which this class implements.
     * @param ifc   An interface for which this class implements.
     * @exception IllegalArgumentException  Thrown if the interface given is 
     *                                      null
     */
    public void addInterface( Interface ifc ) 
	throws IllegalArgumentException
    {
	// check if interface is a null reference, if so, throw an exception
	if ( ifc == null )
	    throw new IllegalArgumentException( "Exception thrown in " + 
						"jaba.sym.Class." + 
						"addInterface()" );
	
	// add this interface
	interfaces.addElement( (Object)ifc );
	
    }
    
    /**
     * Returns all interfaces that this class implements.
     * @return Interfaces that this class implements.
     */
	public Collection getInterfacesCollection ()
	{
		return interfaces;
	}
    
    /**
     * Convenience method for converting the Vector into an array (less efficient) from <code>getInterfacesCollection</code>.
     * @return Interfaces that this class implements.
     */
	public Interface [] getInterfaces ()
	{
		return (Interface []) interfaces.toArray (new Interface [interfaces.size ()]);
	}

    /**
     * Adds a constructor to this class.
     * @param constructor  Constructor for this class.
     * @exception IllegalArgumentException  Thrown if constructor is null.
     */
    public void addConstructor( Method constructor ) 
	throws IllegalArgumentException
    {
	// check if constructor is a null reference, if so, throw an 
	// exception
	if ( constructor == null )
	    throw new IllegalArgumentException( "Exception thrown in " +
						"jaba.sym.Class." + 
						"addConstructor()");
	
	// add the constructor
	constructors.addElement( (Object) constructor );
    }
    
    /**
     * Adds an attribute to this class.
     * @param attribute  The attribute to be added to this class.
     * @throws IllegalArgumentException if <code>attribute</code> is
     *                                  <code>null</code>.
     */
    public void addAttribute( ClassAttribute attribute )
	throws IllegalArgumentException
    {
        super.addAttribute( attribute );
    }
    
    /**
     * Returns the constructors of this class.
     * @return Constructors for this class.
     */
	public Collection getConstructorsCollection ()
	{
		return constructors;
	}
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getConstructorsCollection</code>
     * @return Constructors for this class.
     */
	public Method [] getConstructors ()
	{
		return (Method []) constructors.toArray (new Method [constructors.size ()]);
	}

    /**
     * Adds a subclass of this class.
     * @param subClass  Subclass of this class.
     * @exception IllegalArgumentException  Thrown if subClass is null.
     */
    public void addSubClass( Class subClass ) throws IllegalArgumentException
    {
	// check if subClass is a null reference, if so, throw an exception
	if ( subClass == null )
	    throw new IllegalArgumentException( "Exception thrown in " +
						"jaba.sym.Class." +
						"addSubClass()" );
	
	// add the subclass
	subClasses.addElement( (Object)subClass );
	
    }
    
    /**
     * Returns the direct subclasses of this class.
     * @return An array of direct subclasses of this class.
     */
	public Collection getDirectSubClassesCollection ()
	{
		return subClasses;
	}
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getDirectSubClassesCollection</code>
     * @return An array of direct subclasses of this class.
     */
	public Class [] getDirectSubClasses ()
	{
		return (Class []) subClasses.toArray (new Class [subClasses.size ()]);
	}

    /**
     * Returns all subclasses of this class.
     * @return Array of all subclasses for this class
     */
	public Collection getAllSubClassesCollection ()
	{
		Vector v = new Vector ();
		traverseSubclasses (v);

		return v;
	}

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getAllSubClasses</code>
     * @return Array of all subclasses for this class
     */
	public Class [] getAllSubClasses ()
	{
		Collection v = getAllSubClassesCollection ();

		return (Class []) v.toArray (new Class [v.size ()]);
	}

    /**
     * Determines if this class is the same as, or a super-class of the
     * parameter class <code>cls</code>.
     * @param cls class for which to test super-type relation.
     * @return <code>true</code> if <code>this</code> is assignable from
     *         (is a super-type of) <code>cls</code>.
     * @throws IllegalArgumentException if <code>cls</code> is
     *                                  <code>null</code>.
     */
    public boolean isAssignableFrom( Class cls )
        throws IllegalArgumentException
    {
        if ( cls == null ) {
            throw new IllegalArgumentException( "Class.isAssignableFrom(): "+
						"parameter class is null" );
        }
        String clsName = cls.getName();
        if ( clsName.equals( getName() ) ) {
            return true;
        }
        Collection subClasses = getAllSubClassesCollection ();
        for (Iterator i = subClasses.iterator (); i.hasNext ();)
	{
            if (clsName.equals (((Class) i.next ()).getName ()))
	    {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Recursively traverses subclasses starting at this class, and records
     * all subclasses in the vector <code>v</code>.
     * @param v vector in which to record subclasses.
     */
    private void traverseSubclasses( Vector v )
    {
        Collection c = getDirectSubClassesCollection ();
        for (Iterator e = c.iterator (); e.hasNext ();)
	{
	    ClassImpl c1 = (ClassImpl) e.next ();
            v.addElement (c1);
            c1.traverseSubclasses (v);
        }
    }
    
    /**
     * Returns an array of attributes of this class.
     * @return All attributes defined for this class.
     */
    public ClassAttribute[] getAttributes()
    {
        Vector attributes = super.getNamedReferenceTypeAttributes();
        ClassAttribute[] m = new ClassAttribute[attributes.size()];
	int i = 0;
        for (Iterator e = attributes.iterator (); e.hasNext (); i++)
	{
            m [i] = (ClassAttribute) e.next ();
        }
        return m;
    }
    
    /**
     * Returns a string for this object.
     * @return  String representing this object.
     */
	public String toString ()
	{
		StringBuffer buffer = new StringBuffer ();

		buffer.append ("<class>");

		buffer.append (super.toString ());

		buffer.append ("<id>" + getID () + "</id>");
	
		buffer.append( "<name>" + jaba.tools.Util.toXMLValid (getName ()) + "</name>" );
		if (superClass != null)
		{
			buffer.append ("<superclass>" + superClass.getID () + "</superclass>"); 
		}

		for (Iterator i = interfaces.iterator (); i.hasNext ();)
		{
			buffer.append ("<interface>");
			buffer.append (((Interface) i.next ()).getID ());
			buffer.append ("</interface>");
		}

		for (Iterator i = constructors.iterator (); i.hasNext ();)
		{
			buffer.append ("<constructor>");
			buffer.append (((Method) i.next ()).getID ());
			buffer.append ("</constructor>");
		}

		for (Iterator i = subClasses.iterator (); i.hasNext ();)
		{
			buffer.append ("<subclass>");
			buffer.append (((Class) i.next ()).getID ());
			buffer.append ("</subclass>");
		}

		buffer.append ("</class>");

		return buffer.toString ();
	}
    
	/** ??? */
    public String getFullyQualifiedName() {
	StringBuffer name = new StringBuffer();
	
	Class superClass = this;
	while (superClass != null) {
	    name.insert( 0, superClass.getName().replace('/', '.') + ":");
	    
	    Collection interfaces = superClass.getInterfacesCollection ();
	    String[] interfaceNames = new String[interfaces.size ()];
	    int i = 0;
	    for (Iterator e = interfaces.iterator (); e.hasNext (); i++)
	    {
		interfaceNames [i] = ((Interface) e.next ()).getName ();
	    }

	    Arrays.sort (interfaceNames);
	    for (i = 0; i < interfaceNames.length; i++) {
		name.insert(0, interfaceNames[i] + ":");
	    }

	    superClass = superClass.getSuperClass();
	}
	return name.toString();
    }

    /**
     * Get the superclass and the immidate implemented interface.
     * @return Array of class+interfaces
     */
	protected Collection getParentsCollection ()
	{
		Class superClass = getSuperClass ();
		int size = interfaces.size ();

		if(superClass != null)
		{
			size ++;
		}

		Vector ret = new Vector (size);

		int start = 0;

		if (superClass != null)
		{
			ret.add (getSuperClass ());
			start = 1;
		}

		ret.addAll (interfaces);

		return ret;
	}
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getParentsCollection</code>
     * @return Array of class+interfaces
     */
	protected NamedReferenceType [] getParents ()
	{
		Collection v = getParentsCollection ();

		return (NamedReferenceType []) v.toArray (new NamedReferenceType [v.size ()]);
	}

    /** This class's superclass. */
    private Class superClass;
    
    /** All of the interfaces that this class implements. */
    private Vector interfaces;
    
    /** All constructors in this class. */
    private Vector constructors;

    /** All subclasses of this class. */
    private Vector subClasses;
}
