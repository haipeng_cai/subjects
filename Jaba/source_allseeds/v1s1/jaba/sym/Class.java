/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import java.util.Collection;

/**
 * Represents a class in the symbol table.
 * @author Jim Jones -- <i>Created.  Feb. 1999.</i>
 * @author Ashish Gujarathi -- <i>Revised, Apr. 2, 1999, Added 
 *                             inheritedMethods and inheritedFields fields, 
 *                             and their accessors, and the private flatten() 
 *                             method to perform "class flattening".</i>
 * @author Manas Tungare -- <i>Revised, Feb 21, 2002. Added
 *                          method getFullyQualifiedName.</i>
 * @author Huaxing Wu -- <i> Revised, 6/15/02. (a) remove preprocessor
 *                           (b) remove vArr, only use v to contain info and 
 *			        send back info to caller. In this class  v
 *				are: interfaces, constructors and subclasses.
 *			    (c) remove all methods and variables related to
 *			        inherited methods and fields to base class
 *				NamedReferenceType since Another subClass 
 *				Interface also need them. </i>
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/02 changed use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/06 removed Quick Sort class reference.
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/06/26 added implementing Serializable
 * @author Jay Lofstead 2003/06/30 added implements Class
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface Class extends NamedReferenceType
{
    /**
     * Sets the superclass for this class.
     * @param superClass  The superclass of this class.
     * @exception IllegalArgumentException  Thrown if superClass is null.
     */
    public void setSuperClass( Class superClass ) throws IllegalArgumentException;

    /**
     * Get the superclass for this class.
     * @return The superclass of this class.
     */
    public Class getSuperClass();

    /**
     * Adds an interface for which this class implements.
     * @param ifc   An interface for which this class implements.
     * @exception IllegalArgumentException  Thrown if the interface given is 
     *                                      null
     */
    public void addInterface( Interface ifc ) throws IllegalArgumentException;

    /**
     * Returns all interfaces that this class implements.
     * @return Interfaces that this class implements.
     */
	public Collection getInterfacesCollection ();

    /**
     * Convenience method for converting the Vector into an array (less efficient) from <code>getInterfacesCollection</code>.
     * @return Interfaces that this class implements.
     */
	public Interface [] getInterfaces ();

    /**
     * Adds a constructor to this class.
     * @param constructor  Constructor for this class.
     * @exception IllegalArgumentException  Thrown if constructor is null.
     */
    public void addConstructor( Method constructor ) throws IllegalArgumentException;

    /**
     * Adds an attribute to this class.
     * @param attribute  The attribute to be added to this class.
     * @throws IllegalArgumentException if <code>attribute</code> is
     *                                  <code>null</code>.
     */
    public void addAttribute( ClassAttribute attribute ) throws IllegalArgumentException;

    /**
     * Returns the constructors of this class.
     * @return Constructors for this class.
     */
	public Collection getConstructorsCollection ();    

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getConstructorsCollection</code>
     * @return Constructors for this class.
     */
	public Method [] getConstructors ();

    /**
     * Adds a subclass of this class.
     * @param subClass  Subclass of this class.
     * @exception IllegalArgumentException  Thrown if subClass is null.
     */
    public void addSubClass( Class subClass ) throws IllegalArgumentException;

    /**
     * Returns the direct subclasses of this class.
     * @return An array of direct subclasses of this class.
     */
	public Collection getDirectSubClassesCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getDirectSubClassesCollection</code>
     * @return An array of direct subclasses of this class.
     */
	public Class [] getDirectSubClasses ();

    /**
     * Returns all subclasses of this class.
     * @return Array of all subclasses for this class
     */
	public Collection getAllSubClassesCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getAllSubClasses</code>
     * @return Array of all subclasses for this class
     */
	public Class [] getAllSubClasses ();

    /**
     * Determines if this class is the same as, or a super-class of the
     * parameter class <code>cls</code>.
     * @param cls class for which to test super-type relation.
     * @return <code>true</code> if <code>this</code> is assignable from
     *         (is a super-type of) <code>cls</code>.
     * @throws IllegalArgumentException if <code>cls</code> is
     *                                  <code>null</code>.
     */
    public boolean isAssignableFrom( Class cls ) throws IllegalArgumentException;
    
    /**
     * Returns an array of attributes of this class.
     * @return All attributes defined for this class.
     */
    public ClassAttribute[] getAttributes();

    /**
     * Returns a string for this object.
     * @return  String representing this object.
     */
	public String toString ();

	/** ??? */
    public String getFullyQualifiedName ();
}
