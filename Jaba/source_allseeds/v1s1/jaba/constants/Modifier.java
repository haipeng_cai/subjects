/* Copyright (c) 1999, The Ohio State University */

package jaba.constants;

/**
 * Defines constants for modifier keywords
 * in Java declarations. Each modifier applies only to certain
 * types of declarations, and some combinations of keywords are illegal
 * for certain declarations.
 */
public interface Modifier
{
    /**
     * Applies to class, interface, and method declarations. An abstract
     * class cannot be instantiated. An interface is abstract by default.
     * An abstract method has no implementation.
     */
    public static final int M_ABSTRACT = 0x0400;

    /**
     * Applies to class, field, local variable (parameter), and method
     * declarations. A final class cannot be extended. A final field or
     * local variable behaves as a constant. A final method cannot be
     * over-ridden by subclasses.
     */
    public static final int M_FINAL = 0x0010;

    /**
     * Applies to method declarations. A native method is implemented in
     * another language (such as C, C++).
     */
    public static final int M_NATIVE = 0x0100;

    /**
     * Applies to field and method declarations. Static fields and methods
     * are class members rather than instance members; they can be
     * accessed without instantiating theie containing classes.
     */
    public static final int M_STATIC = 0x0008;

    /**
     * Applies to block and method declarations. A synchronized block or
     * method is used to enforce mutual exclusion among different threads
     * that access a shared resource.
     */
    public static final int M_SYNCHRONIZED = 0x0020;

    /**
     * Applies to field declarations. A transient field cannot be
     * serialized.
     */
    public static final int M_TRANSIENT = 0x0080;

    /**
     * Applies to field declarations. A volatile field prevents compiler
     * optimizations from being applied to that field.
     */
    public static final int M_VOLATILE = 0x0040;

    /**
     * Applies to classes and interfaces. The Java compiler sets this flag
     * to treat superclass methods specially in <i> invokespecial </i>
     * instruction. The flag exsits for backward compatibility for code
     * compiled with Sun's older Java compilers.
     */
    public static final int M_SUPER = 0x0020;

    /**
     * Applies to interfaces. The Java compiler sets this flag for an
     * interface.
     */
    public static final int M_INTERFACE = 0x0200;
}
