/* Main Copyright (C) 1998-2001 Jochen Hoenicke.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: CstrLocalVariableTable.java,v 1.2 2003/02/23 23:03:26 orso Exp $
 */

package jode.jvm;
import jode.bytecode.BinaryInfo;
import jode.bytecode.ClassInfo;
import jode.bytecode.SearchPath;
import jode.GlobalOptions;

import jaba.classfile.ClassFile;
import jaba.classfile.MethodInfo;
import jaba.classfile.LocalVariableTableAttribute;
import jaba.classfile.CodeAttribute;


public class CstrLocalVariableTable {
 
    public static void rebuildDebugInfos(ClassFile[] classFiles,
					 String[]classPaths)
    {
	//Set up the enviorment
	//	int importPackageLimit = ImportHandler.DEFAULT_PACKAGE_LIMIT;
        //int importClassLimit = ImportHandler.DEFAULT_CLASS_LIMIT;;
	//ImportHandler imports = new ImportHandler(importPackageLimit,
	//					  importClassLimit);
	//Set Classpaths in ClassInfo
	String classPath = new String(classPaths[0]);
	for(int i = 1; i < classPaths.length; i++) 
	    classPath += SearchPath.altPathSeparatorChar + classPaths[i];

	ClassInfo.setClassPath(classPath);
			
	//Analyze each class in classFiles
	for(int i = 0; i < classFiles.length; i++) {
	    String className = classFiles[i].getFileName();
	 
	    ClassInfo clazz;
	    try {
		clazz = ClassInfo.forName(className);
		clazz.loadInfo(BinaryInfo.MOSTINFO);
	    } catch (IllegalArgumentException ex) {
		GlobalOptions.err.println
		    ("`"+className+"' is not a class name");
		return;
	    }
	    
	    /** Set the LocalVariableTable for each method in this class */
	    MethodInfo[] mi = classFiles[i].getMethodInfos();
	    if(mi == null) 
		continue;

	    //For each method in this class (Represented in jaba.method)
	    for(int index = 0; index < mi.length; index++){
		String methodName = mi[index].getNameString();
		String methodType = mi[index].getDescriptorString();
		
		//For each method, find its class in jode.MethodInfo
		jode.bytecode.MethodInfo ma = clazz.findMethod(methodName, methodType);
		
		if(ma == null) 
		    continue;
		
		jode.bytecode.BytecodeInfo code = ma.getBytecode();
		
		if(code == null)
		    continue;
		
		RegeneratorLVT regenator 
		    = new RegeneratorLVT(clazz, ma, code);
		LocalVariableTableAttribute lvta = regenator.dumpLocalVariableTable();
		
		//Allocate the code Attribute to put the lvt
		int maindex = mi[index].findAttribute(new CodeAttribute(0));
		if(maindex >= 0) {  
		    CodeAttribute ca = (CodeAttribute)mi[index].getAttribute(maindex);
		    ca.mendLocalVariableTableAttribute(lvta);
		}
	    }//For each method
	}//End of for each class
    }//End of method
    
}



