
/* CodeVerifier Copyright (C) 1999 Jochen Hoenicke.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: RegeneratorLVT.java,v 1.8 2003/02/23 23:03:26 orso Exp $
 */

package jode.jvm;

import jaba.classfile.LocalVariableTableEntry;
import jaba.classfile.LocalVariableTableAttribute;

import jode.AssertError;
import jode.GlobalOptions;
import jode.bytecode.BytecodeInfo;
import jode.bytecode.ClassInfo;
import jode.bytecode.Handler;
import jode.bytecode.Instruction;
import jode.bytecode.MethodInfo;
import jode.bytecode.Opcodes;
import jode.bytecode.Reference;
import jode.bytecode.TypeSignature;

import java.util.BitSet;
import java.util.Iterator;
import java.util.HashSet;
import java.util.Vector;

import jode.bytecode.LocalVariableInfo;

public class RegeneratorLVT implements Opcodes {
    ClassInfo ci;
    MethodInfo mi;
    BytecodeInfo bi;

    String methodType;
    String returnType;

    Vector params;
    /**
     * JLS 4.9.6: Verifying code that contains a finally clause:
     *  - Each instruction keeps track of the list of jsr targets.
     *  - For each instruction and each jsr needed to reach that instruction
     *    a bit vector is maintained of all local vars accessed or modified.
     */

    class VerifyInfo implements Cloneable {
	MyType[] stack = new MyType[bi.getMaxStack()];
	MyType[] locals = new MyType[bi.getMaxLocals()];

	Instruction[] jsrTargets = null;
	BitSet[] jsrLocals = null;
	int stackHeight = 0;
	int maxHeight = 0;

	/** Added by Huaxing Wu, to keep what local variables can reach this instruction */
	VariableSet[] vars = new VariableSet[bi.getMaxLocals()];
	
	/** Only used by store statement: the variable that created by this store instruction */
	LocalInfo gen = null;
	/** End of add */
	
	/* If this is a jsr target, this field contains the single 
	 * allowed ret instruction.
	 */
	Instruction retInstr = null;

	public Object clone() {
	    try {
		VerifyInfo result = (VerifyInfo) super.clone();
		result.stack = (MyType[]) stack.clone();
		result.locals = (MyType[]) locals.clone();
		result.vars = (VariableSet[])vars.clone();
		return result;
	    } catch(CloneNotSupportedException ex) {
		throw new AssertError("Clone not supported?");
	    }
	}

	public final void reserve(int count) throws VerifyException {
	    if (stackHeight + count > maxHeight) {
		maxHeight = stackHeight + count;
		if (maxHeight > stack.length)
		    throw new VerifyException("stack overflow");
	    }
	}
	
	public final void need(int count) throws VerifyException {
	    if (stackHeight < count)
		throw new VerifyException("stack underflow");
	}
	
	public final void push(MyType type) throws VerifyException {
	    reserve(1);
	    stack[stackHeight++] = type; 
	}
	
	public final MyType pop() throws VerifyException {
	    need(1);
	    return stack[--stackHeight];
	}
	
	public String toString() {
	    StringBuffer result = new StringBuffer("locals:[");
	    String comma = "";
	    for (int i=0; i<locals.length; i++) {
		result.append(comma).append(i).append(':');
		result.append(locals[i]);
		comma = ",";
	    }
	    result.append("], stack:[");
	    comma = "";
	    for (int i=0; i<stackHeight; i++) {
		result.append(comma).append(stack[i]);
		comma = ",";
	    }
	    if (jsrTargets != null) {
		result.append("], jsrs:[");
		comma = "";
		for (int i=0; i<jsrTargets.length; i++) {
		    result.append(comma).append(jsrTargets[i])
			.append(jsrLocals[i]);
		    comma = ",";
		}
	    }
	    
	    result.append("], \n Variables:[");
	    comma = "";
	    for(int i = 0; i < vars.length; i++) {
		if(vars[i] != null) {
		    result.append(comma).append(i).append(":").append(vars[i].toString());
		}
		comma = ",";
	    }
	    
	    return result.append("]").toString();
	}
    }


    public RegeneratorLVT(ClassInfo ci, MethodInfo mi, BytecodeInfo bi) {
	this.ci = ci;
	this.mi = mi;
	this.bi = bi;
	this.methodType = mi.getType();
	this.returnType = TypeSignature.getReturnType(methodType);
	params = new Vector(10);
    }

    public VerifyInfo initInfo() {
	VerifyInfo info = new VerifyInfo();
	int pos = 1;
	int slot = 0;
	int definePoint = 0;
	int codeLength = bi.getCodeLength();
	
	if (!mi.isStatic()) {
	    String clazzName = ci.getName().replace('.','/');
	    if (mi.getName().equals("<init>"))
		info.locals[slot++] = MyType.tType("N"+ clazzName+";", null);
	    else
		info.locals[slot++] = MyType.tType("L"+ clazzName+";");

	    /** Added by Huaxing: To identiy this **/
	    VariableSet vset = new VariableSet();
	    LocalInfo varThis = new LocalInfo(MyType.tType("L" + clazzName+";"), slot-1);
	    vset.add(varThis);
	    info.vars[slot-1] = vset;

	    params.add(varThis);
	}

	while (methodType.charAt(pos) != ')') {
	    int start = pos;
	    pos = TypeSignature.skipType(methodType, pos);
	    String paramType = methodType.substring(start, pos);
	    info.locals[slot++] = MyType.tType(paramType);
	    /** Added by Huaxing Wu: to idenitify Formal Parameters **/ 
	    VariableSet vset = new VariableSet();
	    LocalInfo param = new LocalInfo(MyType.tType(paramType), slot-1);
	    vset.add(param);
	    info.vars[slot-1] = vset;

	    params.add(param);
	    /** End of Add */

	    if (TypeSignature.getTypeSize(paramType) == 2) {
		info.locals[slot++] = MyType.tSecondPart;
		info.vars[slot-1] = VariableSet.vEmpty;
	    } 
	}

	while (slot < bi.getMaxLocals()) {
	    info.locals[slot++] = MyType.tNone;
	    info.vars[slot-1] = VariableSet.vEmpty;
	}
	
	return info;
    }
    
    public boolean mergeInfo(Instruction instr, VerifyInfo info) 
	throws VerifyException {
	if (instr.getTmpInfo() == null) {
	    instr.setTmpInfo(info);
	    return true;
	}
	boolean changed = false;
	VerifyInfo oldInfo = (VerifyInfo) instr.getTmpInfo();
	if (oldInfo.stackHeight != info.stackHeight)
	    throw new VerifyException("Stack height differ at: "
				      + instr.getDescription());
	for (int i=0; i < oldInfo.stackHeight; i++) {
	    MyType newType = oldInfo.stack[i].mergeType(info.stack[i]);
	    if (!newType.equals(oldInfo.stack[i])) {
		if (newType == MyType.tNone)
		    throw new VerifyException("Type error while merging: "
					      + oldInfo.stack[i]
					      + " and " + info.stack[i]);
		changed = true;
		oldInfo.stack[i] = newType;
	    }
	}
	for (int i=0; i < bi.getMaxLocals(); i++) {
	    MyType newType = oldInfo.locals[i].mergeType(info.locals[i]);
	    if (!newType.equals(oldInfo.locals[i])) {
		changed = true;
		oldInfo.locals[i] = newType;
	    }
	}

	/** Add by Huaxing Wu */
	/** Merge the Local Variable Set */
	for (int i = 0; i < bi.getMaxLocals(); i++) {
	    if(oldInfo.vars[i] != info.vars[i]) {
		VariableSet newSet = oldInfo.vars[i].mergeSet(info.vars[i]);
	
		if(!newSet.equals(oldInfo.vars[i])) {
		    changed = true;
		    oldInfo.vars[i] = newSet;
		   
		}
	    }
	}

	if (oldInfo.jsrTargets != null) {
	    int jsrDepth;
	    if (info.jsrTargets == null)
		jsrDepth = 0;
	    else {
		jsrDepth = info.jsrTargets.length;
		int infoPtr = 0;
	    oldInfo_loop:
		for (int oldInfoPtr=0; 
		     oldInfoPtr < oldInfo.jsrTargets.length; oldInfoPtr++) {
		    for (int i=infoPtr; i< jsrDepth; i++) {
			if (oldInfo.jsrTargets[oldInfoPtr]
			    == info.jsrTargets[i]) {
			    System.arraycopy(info.jsrTargets, i,
					     info.jsrTargets, infoPtr,
					     jsrDepth - i);
			    jsrDepth -= (i - infoPtr);
			    infoPtr++;
			    continue oldInfo_loop;
			}
		    }
		}
		jsrDepth = infoPtr;
	    }
	    if (jsrDepth != oldInfo.jsrTargets.length) {
		if (jsrDepth == 0)
		    oldInfo.jsrTargets = null;
		else {
		    oldInfo.jsrTargets = new Instruction[jsrDepth];
		    System.arraycopy(info.jsrTargets, 0, 
				     oldInfo.jsrTargets, 0, jsrDepth);
		}
		changed = true;
	    }
	}
	return changed;
    }


    String[] types = { 
	"I", "J", "F", "D", "+", "B", "C", "S"
    };
    String[] arrayTypes = {
	"[I", "[J", "[F", "[D", "[Ljava/lang/Object;", "[B", "[C", "[S"
    };

    public VerifyInfo modelEffect(Instruction instr, VerifyInfo prevInfo) 
	throws VerifyException {
	int jsrLength = 
	    prevInfo.jsrTargets != null ? prevInfo.jsrTargets.length : 0;
	VerifyInfo result = (VerifyInfo) prevInfo.clone();
	int opcode = instr.getOpcode();
	switch (opcode) {
	case opc_nop:
	case opc_goto:
	    break;
	case opc_ldc: {
	    MyType type;
	    Object constant = instr.getConstant();
	    if (constant == null)
		type = MyType.tNull;
	    else if (constant instanceof Integer)
		type = MyType.tInt;
	    else if (constant instanceof Float)
		type = MyType.tFloat;
	    else
		type = MyType.tString;
	    result.push(type);
	    break;
	}
	case opc_ldc2_w: {
	    MyType type;
	    Object constant = instr.getConstant();
	    if (constant instanceof Long)
		type = MyType.tLong;
	    else
		type = MyType.tDouble;
	    result.push(type);
	    result.push(MyType.tSecondPart);
	    break;
	}
	case opc_iload: 
	case opc_lload: 
	case opc_fload: 
	case opc_dload:
	case opc_aload: {
	    if (jsrLength > 0
		&& (!result.jsrLocals[jsrLength-1].get(instr.getLocalSlot())
		    || ((opcode & 0x1) == 0
			&& !result.jsrLocals[jsrLength-1]
			.get(instr.getLocalSlot()+1)))) {
		result.jsrLocals = (BitSet[]) result.jsrLocals.clone();
		result.jsrLocals[jsrLength-1]
		    = (BitSet) result.jsrLocals[jsrLength-1].clone();
		result.jsrLocals[jsrLength-1].set(instr.getLocalSlot());
		if ((opcode & 0x1) == 0)
		    result.jsrLocals[jsrLength-1].set(instr.getLocalSlot() + 1);
	    }
	    if ((opcode & 0x1) == 0
		&& result.locals[instr.getLocalSlot()+1] != MyType.tSecondPart)
		throw new VerifyException(instr.getDescription());
	    MyType type = result.locals[instr.getLocalSlot()];
	    if (!type.isOfType(types[opcode - opc_iload]))
		throw new VerifyException(instr.getDescription());
	    result.push(type);
	    if ((opcode & 0x1) == 0)
		result.push(MyType.tSecondPart);
	    break;
	}
	case opc_iaload: case opc_laload: 
	case opc_faload: case opc_daload: case opc_aaload:
	case opc_baload: case opc_caload: case opc_saload: {
	    if (!result.pop().isOfType("I"))
		throw new VerifyException(instr.getDescription());
	    MyType arrType = result.pop(); 
	    if (!arrType.isOfType(arrayTypes[opcode - opc_iaload])
		&& (opcode != opc_baload
		    || !arrType.isOfType("[Z")))
		throw new VerifyException(instr.getDescription());
	    
	    String typeSig = arrType.getTypeSig();
	    MyType elemType = (typeSig.charAt(0) == '[' 
			     ? MyType.tType(typeSig.substring(1))
			     : (opcode == opc_aaload ? MyType.tNull
				: MyType.tType(types[opcode - opc_iaload])));
	    result.push(elemType);
	    if (((1 << opcode - opc_iaload) & 0xa) != 0)
		result.push(MyType.tSecondPart);
	    break;
	}
	case opc_istore: case opc_lstore: 
	case opc_fstore: case opc_dstore: case opc_astore: {
	    if (jsrLength > 0
		&& (!result.jsrLocals[jsrLength-1].get(instr.getLocalSlot())
		    || ((opcode & 0x1) != 0
			&& !result.jsrLocals[jsrLength-1]
			.get(instr.getLocalSlot()+1)))) {
		result.jsrLocals = (BitSet[]) result.jsrLocals.clone();
		result.jsrLocals[jsrLength-1]
		    = (BitSet) result.jsrLocals[jsrLength-1].clone();
		result.jsrLocals[jsrLength-1].set(instr.getLocalSlot());
		if ((opcode & 0x1) != 0)
		    result.jsrLocals[jsrLength-1].set(instr.getLocalSlot() + 1);
	    }
	    if ((opcode & 0x1) != 0
		&& result.pop() != MyType.tSecondPart)
		throw new VerifyException(instr.getDescription());
	    MyType type = result.pop();
	    if (!type.isOfType(types[opcode - opc_istore]))
		if (opcode != opc_astore || !type.isOfType("R"))
		    throw new VerifyException(instr.getDescription());

	    /** Add by Huaxing Wu */
	    /** Store statement will create Local Variables 
		One store statement will only create a single local varibale
		but from different branch, the type of the store maybe different
		E.g: Object a = b? (Object)new Integer(1): (Object)new String("aa") */
	    int localSlot = instr.getLocalSlot();
	    VerifyInfo instrInfo = (VerifyInfo)instr.getTmpInfo();
	    if(instrInfo.gen == null) {
		/** First time analysis */
		instrInfo.gen = new LocalInfo(type, localSlot); 
		/** Put this variable into the result(out set) */
		VariableSet vset = new VariableSet();
		vset.add(instrInfo.gen);
		result.vars[localSlot] = vset;
	    }else {
		if(!instrInfo.gen.getType().equals(type)) {
		    instrInfo.gen.setType(type.mergeType(instrInfo.gen.getType()));
		}
		
		/** The Variable set of this slot should contain only instrInfo.gen */
		if(result.vars[localSlot].size() != 1 ||
		   !result.vars[localSlot].contains(instrInfo.gen)) {
		    //Need to create another VariableSet
		    VariableSet vset = new VariableSet();
		    vset.add(instrInfo.gen);
		    result.vars[localSlot] = vset;
		}
	    }
	    instrInfo.gen.setDefinePoint(instr.getAddr());

	    result.locals[instr.getLocalSlot()] = type;
	    if ((opcode & 0x1) != 0)
		result.locals[instr.getLocalSlot()+1] = MyType.tSecondPart;

	    break;
	}
	case opc_iastore: case opc_lastore:
	case opc_fastore: case opc_dastore: case opc_aastore:
	case opc_bastore: case opc_castore: case opc_sastore: {
	    if (((1 << opcode - opc_iastore) & 0xa) != 0
		&& result.pop() != MyType.tSecondPart)
		throw new VerifyException(instr.getDescription());
	    MyType type = result.pop();
	    if (!result.pop().isOfType("I"))
		throw new VerifyException(instr.getDescription());
	    MyType arrType = result.pop();
	    if (!arrType.isOfType(arrayTypes[opcode - opc_iastore])
		&& (opcode != opc_bastore || !arrType.isOfType("[Z")))
		throw new VerifyException(instr.getDescription());
	    String elemType = opcode >= opc_bastore ? "I"
		: types[opcode - opc_iastore];
	    if (!type.isOfType(elemType))
		throw new VerifyException(instr.getDescription());
	    break;
	}
	case opc_pop: case opc_pop2: {
	    int count = opcode - (opc_pop-1);
	    result.need(count);
	    result.stackHeight -= count;
	    break;
	}
	case opc_dup: case opc_dup_x1: case opc_dup_x2: {
	    int depth = opcode - opc_dup;
	    result.reserve(1);
	    result.need(depth+1);
	    if (result.stack[result.stackHeight-1] == MyType.tSecondPart)
		throw new VerifyException(instr.getDescription());
	    
	    int stackdepth = result.stackHeight - (depth + 1);
	    if (result.stack[stackdepth] == MyType.tSecondPart)
		throw new VerifyException(instr.getDescription()
					  + " on long or double");
	    for (int i=result.stackHeight; i > stackdepth; i--)
		result.stack[i] = result.stack[i-1];
	    result.stack[stackdepth] = result.stack[result.stackHeight++];
	    break;
	}
	case opc_dup2: case opc_dup2_x1: case opc_dup2_x2: {
	    int depth = opcode - opc_dup2;
	    result.reserve(2);
	    result.need(depth+2);
	    if (result.stack[result.stackHeight-2] == MyType.tSecondPart)
		throw new VerifyException(instr.getDescription()
					  + " on misaligned long or double");
	    int stacktop = result.stackHeight;
	    int stackdepth = stacktop - (depth + 2);
	    if (result.stack[stackdepth] == MyType.tSecondPart)
		throw new VerifyException(instr.getDescription()
					  + " on long or double");
	    for (int i=stacktop; i > stackdepth; i--)
		result.stack[i+1] = result.stack[i-1];
	    result.stack[stackdepth+1] = result.stack[stacktop+1];
	    result.stack[stackdepth] = result.stack[stacktop];
	    result.stackHeight+=2;
	    break;
	}
	case opc_swap: {
	    result.need(2);
	    if (result.stack[result.stackHeight-2] == MyType.tSecondPart
		|| result.stack[result.stackHeight-1] == MyType.tSecondPart)
		throw new VerifyException(instr.getDescription()
					  + " on misaligned long or double");
	    MyType tmp = result.stack[result.stackHeight-1];
	    result.stack[result.stackHeight-1] = 
		result.stack[result.stackHeight-2];
	    result.stack[result.stackHeight-2] = tmp;
	    break;
	}
        case opc_iadd: case opc_ladd: case opc_fadd: case opc_dadd:
        case opc_isub: case opc_lsub: case opc_fsub: case opc_dsub:
        case opc_imul: case opc_lmul: case opc_fmul: case opc_dmul:
        case opc_idiv: case opc_ldiv: case opc_fdiv: case opc_ddiv:
        case opc_irem: case opc_lrem: case opc_frem: case opc_drem: {
	    String type = types[(opcode - opc_iadd) & 3];
	    if ((opcode & 1) != 0
		&& result.pop() != MyType.tSecondPart)
		throw new VerifyException(instr.getDescription());
	    if (!result.pop().isOfType(type))
		throw new VerifyException(instr.getDescription());
	    if ((opcode & 1) != 0) {
		result.need(2);
		if (result.stack[result.stackHeight-1] != MyType.tSecondPart
		    || !result.stack[result.stackHeight-2].isOfType(type))
		    throw new VerifyException(instr.getDescription());
	    } else {
		result.need(1);
		if (!result.stack[result.stackHeight-1].isOfType(type))
		    throw new VerifyException(instr.getDescription());
	    }
	    break;
	}
        case opc_ineg: case opc_lneg: case opc_fneg: case opc_dneg: {
	    String type = types[(opcode - opc_ineg) & 3];
	    if ((opcode & 1) != 0) {
		result.need(2);
		if (result.stack[result.stackHeight-1] != MyType.tSecondPart
		    || !result.stack[result.stackHeight-2].isOfType(type))
		    throw new VerifyException(instr.getDescription());
	    } else {
		result.need(1);
		if (!result.stack[result.stackHeight-1].isOfType(type))
		    throw new VerifyException(instr.getDescription());
	    }
	    break;
	}
        case opc_ishl: case opc_lshl:
        case opc_ishr: case opc_lshr:
        case opc_iushr: case opc_lushr:
	    if (!result.pop().isOfType("I"))
		throw new VerifyException(instr.getDescription());
	    
	    if ((opcode & 1) != 0) {
		result.need(2);
		if (result.stack[result.stackHeight-1] != MyType.tSecondPart ||
		    !result.stack[result.stackHeight-2].isOfType("J"))
		    throw new VerifyException(instr.getDescription());
	    } else {
		result.need(1);
		if (!result.stack[result.stackHeight-1].isOfType("I"))
		    throw new VerifyException(instr.getDescription());
	    }
	    break;

        case opc_iand: case opc_land:
        case opc_ior : case opc_lor :
        case opc_ixor: case opc_lxor:
	    if ((opcode & 1) != 0
		&& result.pop() != MyType.tSecondPart)
		throw new VerifyException(instr.getDescription());
	    if (!result.pop().isOfType(types[opcode & 1]))
		throw new VerifyException(instr.getDescription());
	    if ((opcode & 1) != 0) {
		result.need(2);
		if (result.stack[result.stackHeight-1] != MyType.tSecondPart
		    || !result.stack[result.stackHeight-2].isOfType("J"))
		    throw new VerifyException(instr.getDescription());
	    } else {
		result.need(1);
		if (!result.stack[result.stackHeight-1].isOfType("I"))
		    throw new VerifyException(instr.getDescription());
	    }
	    break;

	case opc_iinc:
	    if (!result.locals[instr.getLocalSlot()].isOfType("I"))
		throw new VerifyException(instr.getDescription());

	    break;
        case opc_i2l: case opc_i2f: case opc_i2d:
        case opc_l2i: case opc_l2f: case opc_l2d:
        case opc_f2i: case opc_f2l: case opc_f2d:
        case opc_d2i: case opc_d2l: case opc_d2f: {
            int from = (opcode-opc_i2l)/3;
            int to   = (opcode-opc_i2l)%3;
            if (to >= from)
                to++;
	    if ((from & 1) != 0
		&& result.pop() != MyType.tSecondPart)
		throw new VerifyException(instr.getDescription());
	    if (!result.pop().isOfType(types[from]))
		throw new VerifyException(instr.getDescription());
		
	    result.push(MyType.tType(types[to]));
	    if ((to & 1) != 0)
		result.push(MyType.tSecondPart);
	    break;
	}
        case opc_i2b: case opc_i2c: case opc_i2s:
	    result.need(1);
	    if (!result.stack[result.stackHeight-1].isOfType("I"))
		throw new VerifyException(instr.getDescription());
	    break;

	case opc_lcmp:
	    if (result.pop() != MyType.tSecondPart)
		throw new VerifyException(instr.getDescription());
	    if (!result.pop().isOfType("J"))
		throw new VerifyException(instr.getDescription());
	    if (result.pop() != MyType.tSecondPart)
		throw new VerifyException(instr.getDescription());
	    if (!result.pop().isOfType("J"))
		throw new VerifyException(instr.getDescription());
	    result.push(MyType.tInt);
	    break;
	case opc_dcmpl: case opc_dcmpg:
	    if (result.pop() != MyType.tSecondPart)
		throw new VerifyException(instr.getDescription());
	    if (!result.pop().isOfType("D"))
		throw new VerifyException(instr.getDescription());
	    if (result.pop() != MyType.tSecondPart)
		throw new VerifyException(instr.getDescription());
	    if (!result.pop().isOfType("D"))
		throw new VerifyException(instr.getDescription());
	    result.push(MyType.tInt);
	    break;
	case opc_fcmpl: case opc_fcmpg:
	    if (!result.pop().isOfType("F"))
		throw new VerifyException(instr.getDescription());
	    if (!result.pop().isOfType("F"))
		throw new VerifyException(instr.getDescription());
	    result.push(MyType.tInt);
	    break;

	case opc_ifeq: case opc_ifne: 
	case opc_iflt: case opc_ifge: 
	case opc_ifgt: case opc_ifle:
	case opc_tableswitch:
	case opc_lookupswitch:
	    if (!result.pop().isOfType("I"))
		throw new VerifyException(instr.getDescription());
	    break;

	case opc_if_icmpeq: case opc_if_icmpne:
	case opc_if_icmplt: case opc_if_icmpge: 
	case opc_if_icmpgt: case opc_if_icmple: 
	    if (!result.pop().isOfType("I"))
		throw new VerifyException(instr.getDescription());
	    if (!result.pop().isOfType("I"))
		throw new VerifyException(instr.getDescription());
	    break;
	case opc_if_acmpeq: case opc_if_acmpne:
	    if (!result.pop().isOfType("+"))
		throw new VerifyException(instr.getDescription());
	    if (!result.pop().isOfType("+"))
		throw new VerifyException(instr.getDescription());
	    break;
	case opc_ifnull: case opc_ifnonnull:
	    if (!result.pop().isOfType("+"))
		throw new VerifyException(instr.getDescription());
	    break;

	case opc_ireturn: case opc_lreturn: 
	case opc_freturn: case opc_dreturn: case opc_areturn: {
	    if (((1 << opcode - opc_ireturn) & 0xa) != 0
		&& result.pop() != MyType.tSecondPart)
		throw new VerifyException(instr.getDescription());
	    MyType type = result.pop();
	    if (!type.isOfType(types[opcode - opc_ireturn])
		|| !type.isOfType(TypeSignature.getReturnType(methodType)))
		throw new VerifyException(instr.getDescription());
	    break;
	}
	case opc_jsr: {
	    Instruction jsrTarget = instr.getSingleSucc();
	    result.stack[result.stackHeight++] = MyType.tType("R", jsrTarget);
	    result.jsrTargets = new Instruction[jsrLength+1];
	    result.jsrLocals = new BitSet[jsrLength+1];
	    if (jsrLength > 0) {
		for (int i=0; i< prevInfo.jsrTargets.length; i++)
		    if (prevInfo.jsrTargets[i] == instr.getSingleSucc())
			throw new VerifyException(instr.getDescription()+
						  " is recursive");
		System.arraycopy(prevInfo.jsrTargets, 0, 
				 result.jsrTargets, 0, jsrLength);
		System.arraycopy(prevInfo.jsrLocals, 0, 
				 result.jsrLocals, 0, jsrLength);
	    }
	    result.jsrTargets[jsrLength] = instr.getSingleSucc();
	    result.jsrLocals[jsrLength] = new BitSet();
	    break;
	}
	case opc_return:
	    if (!returnType.equals("V"))
		throw new VerifyException(instr.getDescription());
	    break;
	case opc_getstatic: {
	    Reference ref = instr.getReference();
	    String type = ref.getType();
	    result.push(MyType.tType(type));
	    if (TypeSignature.getTypeSize(type) == 2)
		result.push(MyType.tSecondPart);
	    break;
	}
	case opc_getfield: {
	    Reference ref = instr.getReference();
	    String classType = ref.getClazz();
	    MyType mytype = result.pop();
// 	    if (!type.isOfType(classType))
// 		throw new VerifyException(instr.getDescription());
	    String type = ref.getType();
	    result.push(MyType.tType(type));
	    if (TypeSignature.getTypeSize(type) == 2)
		result.push(MyType.tSecondPart);
	    break;
	}
	case opc_putstatic: {
	    Reference ref = instr.getReference();
	    String type = ref.getType();
	    if (TypeSignature.getTypeSize(type) == 2
		&& result.pop() != MyType.tSecondPart)
		throw new VerifyException(instr.getDescription());
	    if (!result.pop().isOfType(type))
		throw new VerifyException(instr.getDescription());
	    break;
	}
	case opc_putfield: {
	    Reference ref = instr.getReference();
	    String type = ref.getType();
	    if (TypeSignature.getTypeSize(type) == 2
		&& result.pop() != MyType.tSecondPart)
		throw new VerifyException(instr.getDescription());
	    if (!result.pop().isOfType(type))
		throw new VerifyException(instr.getDescription());
	    String classType = ref.getClazz();
	    MyType stackType = result.pop();
// 	    if (!stackType.isOfType(classType)) {
// 		throw new VerifyException(instr.getDescription());
// 	    }
	    break;
	}
	case opc_invokevirtual:
	case opc_invokespecial:
	case opc_invokestatic :
	case opc_invokeinterface: {
	    Reference ref = instr.getReference();
	    String refmt = ref.getType();
	    String[] paramTypes = TypeSignature.getParameterTypes(refmt);
	    for (int i=paramTypes.length - 1; i >= 0; i--) {
		if (TypeSignature.getTypeSize(paramTypes[i]) == 2
		    && result.pop() != MyType.tSecondPart)
		    throw new VerifyException(instr.getDescription());
		MyType type = result.pop();
// 		if (!type.isOfType(paramTypes[i]))
// 		    throw new VerifyException(instr.getDescription());
	    }
	    if (ref.getName().equals("<init>")) {
	        MyType clazz = result.pop();
		String typeSig = clazz.getTypeSig();
		String refClazz = ref.getClazz();
		if (opcode != opc_invokespecial
		    || typeSig.charAt(0) != 'N'
		    || refClazz.charAt(0) != 'L')
		    throw new VerifyException(instr.getDescription());
		if (!typeSig.substring(1).equals(refClazz.substring(1))) {
		    ClassInfo uci = ClassInfo.forName
			(typeSig.substring(1, typeSig.length()-1)
			 .replace('/', '.'));
		    if (uci.getSuperclass()
			!= TypeSignature.getClassInfo(refClazz)
			|| clazz.getInstruction() != null)
			throw new VerifyException(instr.getDescription());
		}
		MyType newType = MyType.tType("L" + typeSig.
substring(1));
		for (int i=0; i< result.stackHeight; i++)
		    if (result.stack[i] == clazz)
			result.stack[i] = newType;
		for (int i=0; i< result.locals.length; i++)
		    if (result.locals[i] == clazz)
			result.locals[i] = newType;
	    } else if (opcode != opc_invokestatic) {
		String classType = ref.getClazz();
		if (!result.pop().isOfType(classType))
		    throw new VerifyException(instr.getDescription());
	    }
	    String type = TypeSignature.getReturnType(refmt);
	    if (!type.equals("V")) {
		result.push(MyType.tType(type));
		if (TypeSignature.getTypeSize(type) == 2)
		    result.push(MyType.tSecondPart);
	    }
	    break;
	}
	case opc_new: {
	    String clName = instr.getClazzType();
	    result.stack[result.stackHeight++] = 
		MyType.tType("N" + clName.substring(1), instr);
	    break;
	}
	case opc_arraylength: {
	    if (!result.pop().isOfType("[*"))
		throw new VerifyException(instr.getDescription());
	    result.push(MyType.tInt);
	    break;
	}
	case opc_athrow: {
	    if (!result.pop().isOfType("Ljava/lang/Throwable;"))
		throw new VerifyException(instr.getDescription());
	    break;
	}
	case opc_checkcast: {
	    String classType = instr.getClazzType();
	    if (!result.pop().isOfType("+"))
		throw new VerifyException(instr.getDescription());
	    result.push(MyType.tType(classType));
	    break;
	}
	case opc_instanceof: {
	    if (!result.pop().isOfType("Ljava/lang/Object;"))
		throw new VerifyException(instr.getDescription());
	    result.push(MyType.tInt);
	    break;
	}
	case opc_monitorenter:
	case opc_monitorexit:
	    if (!result.pop().isOfType("Ljava/lang/Object;"))
		throw new VerifyException(instr.getDescription());
	    break;
	case opc_multianewarray: {
	    int dimension = instr.getDimensions();
	    for (int i=dimension - 1; i >= 0; i--)
		if (!result.pop().isOfType("I"))
		    throw new VerifyException(instr.getDescription());
	    String classType = instr.getClazzType();
	    result.push(MyType.tType(classType));
	    break;
	}
	default:
	    throw new AssertError("Invalid opcode "+opcode);
	}
	return result;
    }
    
    public LocalVariableTableAttribute dumpLocalVariableTable() {
	try {
	    doVerify();
	}catch(VerifyException ve) {
	    /** Should not happen */
	    ve.printStackTrace();
	}

	findEqualVariable();

	getLocalInfoHint();

	//STEP 3: Generate the Local Variable Table
	LocalVariableTableEntry[] lvtes = generateLVT();

	/** Clean the temporary information */
	for (Iterator i = bi.getInstructions().iterator(); i.hasNext(); ) {
	    Instruction instr = (Instruction) i.next();
	    instr.setTmpInfo(null);
	}
	
	/*GlobalOptions.err.println("\n\n Method " + mi.getName() + "  Local Variable Table");

	 for(int i = 0; i < lvtes.length; i++) 
	     GlobalOptions.err.println(lvtes[i].toString());
	*/
	 return (new LocalVariableTableAttribute(lvtes));

    }

    public void doVerify() throws VerifyException {
	HashSet todoSet = new HashSet();
	Instruction firstInstr =  (Instruction) bi.getInstructions().get(0);
	firstInstr.setTmpInfo(initInfo());
	todoSet.add(firstInstr);
	Handler[] handlers = bi.getExceptionHandlers();
	while (!todoSet.isEmpty()) {
	    Iterator iter = todoSet.iterator();
	    Instruction instr = (Instruction) iter.next();
	    iter.remove();
	    if (!instr.doesAlwaysJump() && instr.getNextByAddr() == null)
		throw new VerifyException("Flow can fall off end of method");

	    VerifyInfo prevInfo = (VerifyInfo) instr.getTmpInfo();
	    int opcode = instr.getOpcode();
	    if (opcode == opc_ret) {
		MyType retVarType = prevInfo.locals[instr.getLocalSlot()];
		if (prevInfo.jsrTargets == null
		    || !retVarType.isOfType("R"))
		    throw new VerifyException(instr.getDescription());
		int jsrLength = prevInfo.jsrTargets.length - 1;
		Instruction jsrTarget = retVarType.getInstruction();
		while (jsrTarget != prevInfo.jsrTargets[jsrLength])
		    if (--jsrLength < 0) 
			throw new VerifyException(instr.getDescription());
		VerifyInfo jsrTargetInfo = (VerifyInfo) jsrTarget.getTmpInfo();
		if (jsrTargetInfo.retInstr == null)
		    jsrTargetInfo.retInstr = instr;
		else if (jsrTargetInfo.retInstr != instr)
		    throw new VerifyException
			("JsrTarget has more than one ret: "
			 + jsrTarget.getDescription());
		Instruction[] nextTargets;
		BitSet[] nextLocals;
		if (jsrLength > 0) {
		    nextTargets = new Instruction[jsrLength];
		    nextLocals = new BitSet[jsrLength];
		    System.arraycopy(prevInfo.jsrTargets, 0, 
				     nextTargets, 0, jsrLength);
		    System.arraycopy(prevInfo.jsrLocals, 0, 
				     nextLocals, 0, jsrLength);
		} else {
		    nextTargets = null;
		    nextLocals = null;
		}
		for (int i=0; i < jsrTarget.getPreds().length; i++) {
		    Instruction jsrInstr = jsrTarget.getPreds()[i];
		    if (jsrInstr.getTmpInfo() != null)
			todoSet.add(jsrInstr);
		}
	    } else {
		VerifyInfo info = modelEffect(instr, prevInfo);
		if (!instr.doesAlwaysJump())
		    if (mergeInfo(instr.getNextByAddr(), info))
			todoSet.add(instr.getNextByAddr());
		if (opcode == opc_jsr) {		   
		    VerifyInfo targetInfo = 
			(VerifyInfo) instr.getSingleSucc().getTmpInfo();

		    if (targetInfo != null && targetInfo.retInstr != null) {
			VerifyInfo afterJsrInfo
			    = (VerifyInfo) prevInfo.clone();
			VerifyInfo retInfo
			    = (VerifyInfo) targetInfo.retInstr.getTmpInfo();
			BitSet usedLocals
			    = retInfo.jsrLocals[retInfo.jsrLocals.length-1];
			for (int j = 0; j < bi.getMaxLocals(); j++) {
			    if (usedLocals.get(j)){
				/** Changed by Huaxing Wu */
				afterJsrInfo.vars[j] = retInfo.vars[j];
				/** End of change */
				afterJsrInfo.locals[j] = retInfo.locals[j];
			    }
			}
			if (mergeInfo(instr.getNextByAddr(), afterJsrInfo))
			    todoSet.add(instr.getNextByAddr());
		    }
		}
		if (instr.getSuccs() != null) {
		    for (int i=0; i< instr.getSuccs().length; i++) {
			if (mergeInfo(instr.getSuccs()[i],
				      (VerifyInfo) info.clone()))
			    todoSet.add(instr.getSuccs()[i]);
		    }
		}
		for (int i=0; i<handlers.length; i++) {
		    if (handlers[i].start.compareTo(instr) <= 0
			&& handlers[i].end.compareTo(instr) >= 0) {
			VerifyInfo excInfo = (VerifyInfo) prevInfo.clone();
			excInfo.stackHeight = 1;
			if (handlers[i].type != null)
			    excInfo.stack[0] = 
				MyType.tType("L" + handlers[i].type
					   .replace('.', '/') + ";");
			else
			    excInfo.stack[0]
				= MyType.tType("Ljava/lang/Throwable;");
		
			if (mergeInfo(handlers[i].catcher, excInfo))
			    todoSet.add(handlers[i].catcher);
		    }
		}
	    }
	}

	if ((GlobalOptions.debuggingFlags
	     & GlobalOptions.DEBUG_VERIFIER) != 0) {
	    for (Iterator i = bi.getInstructions().iterator(); i.hasNext(); ) {
		Instruction instr = (Instruction) i.next();

		VerifyInfo info = (VerifyInfo) instr.getTmpInfo();
		if (info != null) {
		    GlobalOptions.err.println(info.toString());
		}
		GlobalOptions.err.println(instr.getDescription());

	    }
	}
    }

    private LocalVariableTableEntry[] generateLVT() {	
	Vector lvt = new Vector(bi.getMaxLocals());
	int varNum = 0;
	int codeLength =  bi.getCodeLength();

	/** Put this and formal parameters in first */
	for(int i = 0; i < params.size(); i++) {
	    LocalInfo li = (LocalInfo)params.get(i);
	    if(!li.hasName())
		li.setName(varNum ++);
	    lvt.add(new LocalVariableTableEntry((short)0, (short)codeLength, (short)li.getSlot(), li.getName(), li.getType().toString()));
	}

	int paramSlot = params.size();
	
	/** Create entries for each def and usage of local variable */
	for (Iterator i = bi.getInstructions().iterator(); i.hasNext();) {
	    Instruction instr = (Instruction) i.next();
	    int opcode = instr.getOpcode();
	    switch(opcode) {
	    case opc_iload: 
	    case opc_lload: 
	    case opc_fload: 
	    case opc_dload:
	    case opc_aload: 
	    case opc_iinc : {
		int slot = instr.getLocalSlot();
		if(slot < paramSlot)
		    continue;

		VerifyInfo info = (VerifyInfo)instr.getTmpInfo();
		if(info == null) 
		    continue;

		LocalInfo li = info.vars[slot].getVariable();
		
		/** Ignore slot containing return address : the java 
		    compiler does not generate entry for this kind of slot **/
		    if(li.getType().toString().startsWith("R"))
			continue;

		/** For tNULL, the real type can be any Object */
		if(li.getType().toString().startsWith("0"))
		    li.setType(MyType.tObject);

		/** When static type is super class of the dynamic type, it 
		 will report this warning */
		/*	if(!li.getType().isOfType(info.locals[slot].toString())) {
		    GlobalOptions.err.println(" WARNING: Type does not match " + instr.getDescription() + " Type: " + info.locals[slot] + " variable : " + li.getType());
		    }*/

		if(!li.hasName()) {
		    li.setName(varNum++);
		}
	
		lvt.add(new LocalVariableTableEntry((short)instr.getAddr(), (short)0, (short)slot, li.getName(), li.getType().toString()));
		
		break;
	    }
	    
	    case opc_istore: 
	    case opc_lstore: 
	    case opc_fstore: 
	    case opc_dstore: 
	    case opc_astore: {
		/** Note: The variable created by the store may not appear in 
		    its variable set */
		if(instr.getLocalSlot() < paramSlot)
		    continue;

		int nextInstrAddress = instr.getAddr() + instr.getLength();
		VerifyInfo info = (VerifyInfo)instr.getTmpInfo();
		LocalInfo li = info.gen.getLocalInfo();
		
		/** Ignore slot containing return address : the java compiler
		    does not generate entry for this kind of slot **/
		if(li.getType().toString().startsWith("R"))
		    continue;
	
		if(li.getType().toString().startsWith("0"))
		    li.setType(MyType.tObject);
		
		if(!info.gen.hasName()) {
		    info.gen.setName(varNum++);
		}

		lvt.add(new LocalVariableTableEntry((short)nextInstrAddress, (short)0, (short)instr.getLocalSlot(), li.getName(), li.getType().toString()));
		break;
	    }
	    }
	}//End of for(instrs)

	LocalVariableTableEntry[] lves = new LocalVariableTableEntry[0];
	return (LocalVariableTableEntry[])(lvt.toArray(lves));

    }

    /** Use the local variable table in the bytecode to get the hint of 
     *	the variable name and type 
     */
    private void getLocalInfoHint()  {	  	

	LocalVariableInfo [] org_localVars = bi.getLocalVariableTable();
	if(org_localVars == null)
	    return;
 	
	int k = 0;
	if(!mi.isStatic()){
	    ((LocalInfo)params.get(0)).setName("this");
	    k = 1;
	}

	for(; k < params.size(); k++) {
	    int localSlot = ((LocalInfo)params.get(k)).getSlot();
	    for(int i = 0; i < org_localVars.length; i++) {
		if(org_localVars[i].slot == localSlot && 
		   org_localVars[i].start.getAddr() == 0) {
		    if(((LocalInfo)params.get(k)).useHint(org_localVars[i].name, org_localVars[i].type)) {
			break;
		    }
		}
	    }
	}

	int codeLength =  bi.getCodeLength();
	int paramSlot = params.size();
	for (Iterator i = bi.getInstructions().iterator(); i.hasNext();) {
	    Instruction instr = (Instruction) i.next();
	    int opcode = instr.getOpcode();
	    switch(opcode) {                         
	    case opc_istore: 
	    case opc_lstore: 
	    case opc_fstore: 
	    case opc_dstore: 
	    case opc_astore: {
		if(instr.getLocalSlot() < paramSlot)
		    continue;
		int localSlot = instr.getLocalSlot();
	
		int nextInstrAddress = instr.getAddr() + instr.getLength();
		
		for(int l = 0; l < org_localVars.length; l++) {
		    if(org_localVars[l].slot == localSlot) {
			int startAddr = org_localVars[l].start.getAddr();
			int endAddr = org_localVars[l].end.getAddr();
			/** when the end is the instr previous of the last one
			    the real end is the whole instructions */
			Instruction next = org_localVars[l].end.getNextByAddr();
			if(next != null && (next.getAddr()+ next.getLength() ==
			   codeLength)) {
			    endAddr = codeLength;
			}
		       if(nextInstrAddress >= startAddr &&
			  nextInstrAddress <= endAddr) {
			   if(((VerifyInfo)instr.getTmpInfo()).gen.useHint(org_localVars[l].name, org_localVars[l].type))
			       break;
		       }
		    }
		}
		break;
	    }
	    }
	}//End of for(instrs)
    }

    private void findEqualVariable() {
	for (Iterator i = bi.getInstructions().iterator(); i.hasNext();) {
	    Instruction instr = (Instruction) i.next();
	    int opcode = instr.getOpcode();
	    switch(opcode) {
	    case opc_iload: 
	    case opc_lload: 
	    case opc_fload: 
	    case opc_dload:
	    case opc_aload: 
	    case opc_iinc : {
		int slot = instr.getLocalSlot();
	
		if(instr.getTmpInfo() != null) {
		    VariableSet vars = ((VerifyInfo)instr.getTmpInfo()).vars[slot];
		    if(vars.size() > 1) {
			vars.setEquivalent();
		    }
		}
	    }
	    }//End of switch
	}
    }

    public void verify() throws VerifyException {
	try {
	    doVerify();
	} catch (VerifyException ex) {
	    for (Iterator i = bi.getInstructions().iterator(); i.hasNext(); ) {
		Instruction instr = (Instruction) i.next();
		VerifyInfo info = (VerifyInfo) instr.getTmpInfo();
		if (info != null)
		    GlobalOptions.err.println(info.toString());
		GlobalOptions.err.println(instr.getDescription());

		instr.setTmpInfo(null);
	    }
	    throw ex;
	}
    }
}
