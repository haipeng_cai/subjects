/* LocalInfo Copyright (C) 1998-1999 Jochen Hoenicke.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: LocalInfo.java,v 1.3 2003/02/23 23:03:26 orso Exp $
 */

package jode.jvm;

/**
 * The LocalInfo represents a local variable of a method.
 * The problem is that two different local variables may use the same
 * slot.  The current strategie is to make the range of a local variable 
 * as small as possible.<p>
 *
 * There may be more than one LocalInfo for a single local variable,
 * because the algorithm begins with totally disjunct variables and
 * then unifies locals.  One of the local is then a shadow object which
 * calls the member functions of the other local.<p>
 */
public class LocalInfo {
 
    private String name;
    
    private LocalInfo shadow;
 
    private int definePoint;
    private MyType type;

    private int slot;

    public LocalInfo(MyType type, int slot) {
	name = null;
	this.type = type;
	this.slot = slot;
    }


    /**
     * Combines the LocalInfo with another.  This will make this
     * a shadow object to the other local info.  That is all member
     * functions will use the new local info instead of data in this
     * object. <p>
     * If this is called with ourself nothing will happen.
     * @param li the local info that we want to shadow.
     */
    public void combineWith(LocalInfo shadow) {
        if (this.shadow != null) {
            getLocalInfo().combineWith(shadow);
	    return;
	}
	
        shadow = shadow.getLocalInfo();
	if (this == shadow)
	    return;
	
	/** Get the combined type */
	MyType newType = this.type.mergeType(shadow.type);
	this.shadow = shadow;
	shadow.setType(newType);
	
	/* Clear unused fields, to allow garbage collection.
	 */
	type = null;
	name = null;
    }

    /**
     * Get the real LocalInfo.  This may be different from the
     * current object if this is a shadow local info.
     */
    public LocalInfo getLocalInfo() {
        if (shadow != null) {
            while (shadow.shadow != null) {
                shadow = shadow.shadow;
            }
            return shadow;
        }
        return this;
    }

    /**
     * Returns true if the local already has a name.
     */
    public boolean hasName() {
	return getLocalInfo().name != null;
    }

    /** Use the suggested info to adjust the variable name and type. 
     * Currently, this info comes from Local variable table generated
     * by compiler.
     @param name This local variable's possible actual name.
     @param type  This local variable's possible actual type.
    */
    public boolean useHint(String name, String sType) {
        if (this.shadow != null) {
	    return getLocalInfo().useHint(name, sType);
        }
	
	if (this.name == null) {
	    if(type.isOfType(sType)) {
		this.name = name;
		setType(MyType.tType(sType));
		return true;
	    }else {
		return false;
	    }
	}
	return true;
    }
 
    public void setName(int hint) {
	LocalInfo li = getLocalInfo();
        li.name = "local_s" + slot + "_" + hint;
    }

    /**
     * Get the name of this local.
     */
    public String getName() {
        if (this.shadow != null) {
	    return getLocalInfo().getName();
        }
        return name;
    }

    /**
     * Get the slot of this local.
     */
    public int getSlot() {
        /* The slot may change when shadowing for anonymous locals */
        return getLocalInfo().slot;
    }

    /**
     * Set the name of this local.
     */
    public void setName(String name) {
        LocalInfo li = getLocalInfo();
        li.name = name;
    }

    /**
     * Get the type of this local.
     */
    public MyType getType() {
        return getLocalInfo().type;
    }

    public void setType(MyType type) {
	getLocalInfo().type = type;
    }

    public boolean isShadow() {
	return (shadow != null);
    }

    public boolean equals(Object obj) {
        return (obj instanceof LocalInfo
                && ((LocalInfo)obj).getLocalInfo() == getLocalInfo());
    }

    public String toString() {
        return getName();
    }

    public void setDefinePoint(int addr) {
	definePoint = addr;
    }

    public int getDefinePoint() {
	return definePoint;
    }
}

