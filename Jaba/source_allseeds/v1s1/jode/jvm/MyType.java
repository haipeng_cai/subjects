/* CodeVerifier Copyright (C) 1999 Jochen Hoenicke.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: MyType.java,v 1.2 2003/02/23 23:03:26 orso Exp $
 */

package jode.jvm;
//import jode.AssertError;
import jode.GlobalOptions;
//import jode.bytecode.BytecodeInfo;
import jode.bytecode.ClassInfo;
//import jode.bytecode.Handler;*/
import jode.bytecode.Instruction;
/*import jode.bytecode.MethodInfo;
import jode.bytecode.Opcodes;
import jode.bytecode.Reference;*/
import jode.bytecode.TypeSignature;

 /**
  * We need some more types, than mentioned in jvm.
  */
public class MyType{

    public static final MyType tNull    = MyType.tType("0");
    public static final MyType tInt     = MyType.tType("I");
    public static final MyType tLong    = MyType.tType("J");
    public static final MyType tFloat   = MyType.tType("F");
    public static final MyType tDouble  = MyType.tType("D");
    public static final MyType tString  = MyType.tType("Ljava/lang/String;");
    public static final MyType tNone    = MyType.tType("?");
    public static final MyType tSecondPart = new MyType("2");
    public static final MyType tObject  = new MyType("Ljava/lang/Object;");

    /* "ZBCSIFJD" are the normal primitive types.
     * "L...;" is normal class type.
     * "[..." is normal array type
     * "?" stands for type error
     * "N...;" stands for new uninitialized type.  
     * "0" stands for null type.
     * "R" stands for return address type.
     * "2" stands for second half of a two word type.
     */
    private String typeSig;

    /**
     * The dependant instruction.  This has two usages:
     * <dl> <dt>"N...;"</dt> 
     * <dd> The new instruction, or null if this is the this param
     * of &lt;init&gt;.</dd>
     * <dt>"R"</dt> <dd>The <i>target</i> of the jsr.
     */
    private Instruction instr;
    
    public MyType(String typeSig) {
	this.typeSig = typeSig;
    }
    
    public MyType(String typeSig, Instruction instr) {
	this.typeSig = typeSig;
	this.instr = instr;
    }
    
    public static MyType tType(String typeSig) {
	// unify them?
	return new MyType(typeSig);
    }
    
    public static MyType tType(String typeSig, Instruction instr) {
	// unify them?
	return new MyType(typeSig, instr);
    }
    
    public String getTypeSig() {
	return typeSig;
    }
    
    public Instruction getInstruction() {
	return instr;
    }

    /**
     * @param t2 the type signature of the type to check for.
     *   This may be one of the special signatures:
     *   <dl><dt>"[*"<dt><dd>array of something</dd>
     *   <dt>"+"</dt><dd>(uninitialized) object/returnvalue type</dd>
     *   <dt>"2", "R"</dt> <dd>as the typeSig parameter </dd>
     *   </dl>
     * @return true, iff this is castable to t2 by a
     * widening cast.  */
    public boolean isOfType(String destSig) {
	String thisSig = typeSig;
	if ((GlobalOptions.debuggingFlags
	     & GlobalOptions.DEBUG_VERIFIER) != 0)
		GlobalOptions.err.println("isOfType("+thisSig+","+destSig+")");
	if (thisSig.equals(destSig))
	    return true;
	
	char c1 = thisSig.charAt(0);
	char c2 = destSig.charAt(0);
	switch (c2) {
	case 'Z': case 'B': case 'C': case 'S': case 'I':
	    /* integer type */
	    return ("ZBCSI".indexOf(c1) >= 0);
	case '+':
	    return ("L[nNR0".indexOf(c1) >= 0);
	    
	case '[':
	    if (c1 == '0') 
		return true;
	    while (c1 == '[' && c2 == '[') {
		thisSig = thisSig.substring(1);
		destSig = destSig.substring(1);
		c1 = thisSig.charAt(0);
		c2 = destSig.charAt(0);
	    }
	    
	    if (c2 == '*')
		/* destType is array of unknowns */
		return true;
	    /* Note that short[] is only compatible to short[],
	     * therefore we only need to handle Object types specially.
	     */
	    
	    if (c2 != 'L')
		return false;
	    /* fall through*/
	case 'L':
	    if (c1 == '0') 
		return true;
	    if ("L[".indexOf(c1) < 0)
		return false;
	    
	    ClassInfo wantedType = TypeSignature.getClassInfo(destSig);
	    if (wantedType.isInterface()
		|| wantedType == ClassInfo.javaLangObject)
		return true;
	    if (c1 == 'L')
		return wantedType.superClassOf(TypeSignature
						   .getClassInfo(thisSig));
	}
	return false;
    }

    /**
     * @return The common super type of this and type2.
     */
    public MyType mergeType(MyType type2) {
	String sig1 = typeSig;
	String sig2 = type2.typeSig;
	
	if (this.equals(type2))
	    return this;
	
	char c1 = sig1.charAt(0);
	char c2 = sig2.charAt(0);
	if (c1 == '*')
	    return type2;
	if (c2 == '*')
	    return this;
	if ("ZBCSI".indexOf(c1) >= 0 && "ZBCSI".indexOf(c2) >= 0)
	    return this;
	
	if (c1 == '0')
	    return ("L[0".indexOf(c2) >= 0) ? type2 : tNone;
	if (c2 == '0')
	    return ("L[".indexOf(c1) >= 0) ? this : tNone;
	
	
	int dimensions = 0;
	/* Note that short[] is only compatible to short[],
	 * therefore we make the array handling after the primitive
	 * type handling.  Also note that we don't allow arrays of 
	 * special types.
	 */
	while (c1 == '[' && c2 == '[') {
	    sig1 = sig1.substring(1);
	    sig2 = sig2.substring(1);
	    c1 = sig1.charAt(0);
	    c2 = sig2.charAt(0);
	    dimensions++;
	}
	
	// One of them is array now, the other is an object,
	// the common super is tObject
	if ((c1 == '[' && c2 == 'L')
	    || (c1 == 'L' && c2 == '[')) {
	    if (dimensions == 0)
		return tObject;
	    StringBuffer result = new StringBuffer(dimensions + 18);
	    for (int i=0; i< dimensions; i++)
		result.append("[");
	    result.append("Ljava/lang/Object;");
	    return tType(result.toString());
	}

	if (c1 == 'L' && c2 == 'L') {
	    ClassInfo clazz1 = TypeSignature.getClassInfo(sig1);
	    ClassInfo clazz2 = TypeSignature.getClassInfo(sig2);
	    if (clazz1.superClassOf(clazz2))
		return this;
	    if (clazz2.superClassOf(clazz1))
		return type2;
	    do {
		clazz1 = clazz1.getSuperclass();
	    } while (!clazz1.superClassOf(clazz2));
	    StringBuffer result = new StringBuffer
		(dimensions + clazz1.getName().length() + 2);
	    for (int i=0; i< dimensions; i++)
		result.append("[");
	    result.append("L")
		.append(clazz1.getName().replace('.', '/')).append(";");
	    return tType(result.toString());
	    }
	
	// Both were arrays, but of different primitive types.  The
	// common super is tObject with one dimension less.
	if (dimensions > 0) {
	    if (dimensions == 1)
		return tObject;
	    StringBuffer result = new StringBuffer(dimensions + 17);
		for (int i=0; i < dimensions - 1; i++)
		    result.append("[");
		result.append("Ljava/lang/Object;");
		return tType(result.toString());
	}
	return tNone;
    }
    
    public boolean equals(Object other) {
	if (other instanceof MyType) {
	    MyType type2 = (MyType) other;
	    return typeSig.equals(type2.typeSig)
		&& instr == type2.instr;
	}
	return false;
    }

    public String toString() {
	//	if (instr != null)
	//   return typeSig+"@"+instr.getAddr();
	return typeSig;
    }
}
