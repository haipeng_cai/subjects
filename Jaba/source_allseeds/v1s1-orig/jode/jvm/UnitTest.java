package jode.jvm;
import jode.bytecode.ClassInfo;
import jode.bytecode.TypeSignature;

public class UnitTest {
    public static void testVarSetEquivalent() {
	VariableSet varSet = new VariableSet();
	LocalInfo l1 = new LocalInfo(MyType.tType("Ljava/lang/String;"), 2);
	LocalInfo l2 = new LocalInfo(MyType.tType("Ljava/lang/Integer;"),2);
	LocalInfo l3 = new LocalInfo(MyType.tType("Ljava/lang/Double;"),2);
			
	varSet.add(l1);
	varSet.add(l2);
	varSet.add(l3);

	l1.setName("l1");
	l2.setName("l2");
	l3.setName("l3");

	varSet.setEquivalent();

	if(!varSet.isEquivalent()) {
	    System.err.println("Class VariableSet: Either seqEquivalent() or isEquivalent() has bug");
	    System.exit(1);
	}

	LocalInfo repr = varSet.getVariable();
	if(! repr.getType().equals(MyType.tType("Ljava/lang/Object;"))) {
	    System.err.println("Class VariableSet: The type is not right for the equivalent class, should be Object but get " + repr.getType());
	}

	System.out.println(" The representative is : name " + repr.getName() + "  type: " + repr.getType());
    }

    public static void testTypeSigGetClassInfo() {
	
	ClassInfo clazz1 = TypeSignature.getClassInfo(STRING_CLASS);
	ClassInfo clazz2 = TypeSignature.getClassInfo(INTEGER_CLASS);
	clazz1.loadInfo(ClassInfo.FULLINFO);
	clazz2.loadInfo(ClassInfo.FULLINFO);
	System.out.println(clazz1.toString());
	if(clazz1.superClassOf(clazz2)) {
	    System.out.println("STRING_CLASS is the super class of INTEGER_CLASS");
	}

	if(clazz2.superClassOf(clazz1)) {
	    System.out.println("INTEGER_CLASS is the super class of STRING_CLASS");
	}
    }
    
    public static final String STRING_CLASS = "Ljava/lang/String;";
    public static final String INTEGER_CLASS = "Ljava/lang/Integer;";
    public static final String DOUBLK_CLASS = "Ljava/lang/Double;";

    public static void main(String[] args) {
	testTypeSigGetClassInfo();
	testVarSetEquivalent();	
    }
}
