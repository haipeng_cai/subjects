/* VerifyException Copyright (C) 1999 Jochen Hoenicke.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: VerifyException.java,v 1.2 2003/02/23 23:03:26 orso Exp $
 */

package jode.jvm;

/**
 * This exception is thrown by the CodeVerifier on various conditions.
 *
 * @author Jochen Hoenicke
 */
public class VerifyException extends Exception {
    public VerifyException(String detail) {
	super(detail);
    }
    public VerifyException() {
	super();
    }
}

