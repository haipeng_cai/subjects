import jaba.sym.Class;
import jaba.sym.Method;
import jaba.graph.icfg.ICFG;
import jaba.graph.acfg.ACFG;

/**
 * Example of user-defined driver class for the JABA analysis system.  
 * @author Jim Jones and Saurabh Sinha -- <i>Created. April 1, 1999</i>
 * @author Alex Orso -- <i>Modified. October 11, 2002</i>
 */
public class JABA extends jaba.main.JABADriver
{
   protected void run()
    {
	if( program == null ) {
	    usage();
	    throw(new IllegalArgumentException());
	}
	
	System.out.println( "Symbol table loaded..." );

	ICFG icfg = (ICFG)program.getAttributeOfType( "jaba.graph.icfg.ICFG" );
	System.out.println( "ICFG constructed..." );
	
	/* construct ACFG for each method in program */
	Class[] classes = (jaba.sym.Class []) program.getClasses();
	for ( int i = 0; i < classes.length; i++ ) {
	    if ( classes[i].isSummarized() ) continue;
	    Method[] methods = (jaba.sym.Method []) classes[i].getMethods();
	    for ( int j = 0; j < methods.length; j++ ) {
		ACFG acfg =
		    (ACFG)methods[j].getAttributeOfType("jaba.graph.acfg.ACFG" );
	    }
	}
	
	System.out.println( "\nAnalysis completed successfully" );
    }
}
