package jaba.tools.graph;

import jaba.sym.Method;
import jaba.sym.DemandDrivenAttribute;
import jaba.sym.MethodAttribute;

import jaba.graph.Graph;
import jaba.graph.GraphAttribute;

import java.util.Vector;

/**
 * Creates a Differencing Interclass Graph for Java Programs.
 * The Java Interclass Graph uses this class to create individual
 * DIGs and then merges them together with each other as well as
 * External Code Graph (ECG).
 * @author Manas Tungare <manas@cc.gatech.edu>, July 8, 2002.
 *
 * There are differences and similarities between the DIG that
 * for building Java Interclass Graph (JIG) and the DIG for
 * Differencing (javadiff). This class is implemented to be the general
 * differencing graph (GDG) but it is actually the same as the original
 * DIG for building JIG. The DIG for differencing will use this graph
 * as the base and modifies it.
 *
 * By asking for a GDG, the Options associated with the Program must have
 * the createLVT set to true or this will not work properly.
 * 
 * @author Taweesup Apiwattanapong <term@cc.gatech.edu>, April 4, 2003. 
 * @author Jay Lofstead 2003/06/05 changed to use enumerations and added dummy JavaDoc comments
 * @author Jay Lofstead 2003/06/05 added toString
 * @author Jay Lofstead 2003/06/05 added implementing MethodAttribute, DemandDrivenAttribute to allow this to be created
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/12 changed createGDG to not use temporary arrays and instead iteration for better efficiency.
 * @author Jay Lofstead 2003/06/16 added toStringDetailed () and toStringReduced ()
 * @author Term 2003/06/20 code cleanup and fix getFirstNodeWithByteCodeOffset(...),
 *                         getLastNodeWithByteCodeOffset(...)
 * @author Jay Lofstead 2003/06/25 removed option setting from constructor.  Has no effect on code.  Constructor now empty and removed.
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface GDG extends Graph, GraphAttribute, MethodAttribute, DemandDrivenAttribute
{
	/** ??? */
	public Vector getStartNodes ();

	/** returns an XML representation of this object */
	public String toString ();

	/** returns the reduced representation of this object */
	public String toStringReduced ();

	/** returns the detailed representation of this object */
	public String toStringDetailed ();
}
