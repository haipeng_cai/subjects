/* Copyright (c) 1999, The Ohio State University */

package jaba.instruction;

import jaba.constants.OpCode;

import jaba.instruction.visitor.SymbolicInstructionVisitor;

import jaba.du.DefUse;
import jaba.du.PrimitiveDefUse;
import jaba.du.ConstantValue;

import java.util.Vector;
import java.util.List;
import java.util.Enumeration;
import java.util.Iterator;

/**
 * Abstract base representation of all instructions from the JVM instruction
 * set.
 *
 * @author S. Sinha
 * @author Huaxing Wu -- <i> Revised 7/15/02 change addUse(DefUse) to check 
 *                       whether the DefUse Object is constant value. Add 
 *                       addUse(List) method to handle adding a collection
 *                       of DefUse objects </i>
 * @author Jay Lofstead 2003/06/02 converted elementAt to an enumeration for better performance
 * @author Jay Lofstead 2003/06/05 enumeration additions
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/07/10 changed to extend SymbolicInstruction
 */
public abstract class SymbolicInstructionImpl implements SymbolicInstruction, java.io.Serializable
{
    // Fields

    /** Opcode constant for instruction. */
    private byte opCode;

    /** Offset of this instruction in the bytecode array. */
    private int byteCodeOffset;

    /** Operands for the instruction. This field is <code>null</code> if the
        instruction takes its operands from the operand stack. */
    private Vector operands;

    /** Definitions for the instruction. This field is <code>null</code>
        for instructions that do not define a value. */
    private DefUse definition;

    /** Variables used by instruction. */
    private Vector uses;

    // Methods

	/** ??? */
    public abstract void accept( SymbolicInstructionVisitor visitor );

	/** ??? */
    public abstract String[] getRuntimeExceptions();

    /**
     * Returns the opcode of the instruction.
     * @return opcode of instruction.
     */
    public int getOpcode() {
        return opCode & 0x000000ff;
    }

    /**
     * Returns the bytecode offset of the instruction.
     * @return bytecode offset of instruction.
     */
    public int getByteCodeOffset() {
        return byteCodeOffset;
    }

    /**
     * Returns the operands of the instruction, or <code>null</code> if the
     * instruction has no operands.
     * @return Operands of instruction, <code>null</code> if instruction has
     *         no operands.
     */
	public Vector getOperandsVector ()
	{
		return (operands != null) ? operands : new Vector ();
	}

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getOperandsVector</code>
     * @return Operands of instruction, <code>null</code> if instruction has
     *         no operands.
     */
	public Object [] getOperands ()
	{
		return getOperandsVector ().toArray ();
	}

    /**
     * Returns the definition of the instruction, or <code>null</code> if no
     * variable is defined by the instruction.
     * @return Definition of instruction, or <code>null</code> if instruction
     *         has no definition.
     */
    public DefUse getDefinition() {
        return definition;
    }

    /**
     * Returns the uses of the instruction.
     * @return Uses of instruction.
     */
	public Vector getUsesVector ()
	{
		return (uses == null) ? new Vector (0) : uses;
	}

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getUsesVector</code>
     * @return Uses of instruction.
     */
	public DefUse [] getUses ()
	{
		Vector v = getUsesVector ();

		return (DefUse []) v.toArray (new DefUse [v.size ()]);
	}

    /**
     * Sets the bytecode offset of the instruction.
     * @param offset bytecode offset of instruction.
     * @throws IllegalArgumentException if <code>offset</code> is
     *                                  less than 0.
     */
    public void setByteCodeOffset( int offset )
        throws IllegalArgumentException
    {
        if ( offset < 0 ) {
            throw new IllegalArgumentException(
                "SymbolicInstruction.setByteCodeOffset(): negative offset: "+
                offset );
        }
        byteCodeOffset = offset;
    }

    /**
     * Sets the opcode of the instruction.
     * @param opcode opcode of instruction.
     * @throws IllegalArgumentException if <code>opcode</code> is invalid.
     */
    public void setOpcode( byte opcode ) throws IllegalArgumentException {
        int code = opcode & 0x000000ff;
        if ( code < (OpCode.OP_NOP & 0x000000ff) ||
             code > (OpCode.OP_JSR_W & 0x000000ff) ||
             code == (OpCode.OP_UNDEF & 0x000000ff) ) {
            throw new IllegalArgumentException(
                "SymbolicInstruction.setOpcode(): invalid opcode: "+
                code );
        }
        opCode = opcode;
    }

    /**
     * Adds an operand to the instruction.
     * @param operand operand to be added.
     * @throws IllegalArgumentException if <code>operand</code> is
     *                                  <code>null</code>.
     */
    public void addOperand( Object operand ) throws IllegalArgumentException {
        if ( operand == null ) {
            throw new IllegalArgumentException(
                "SymbolicInstruction.addOperand(): argument is null" );
        }
        if ( operands == null ) {
            operands = new Vector();
        }
        operands.addElement( operand );
    }

    /**
     * Adds a collection of used variables for the instruction.
     * @param uses a collection of used variable to be added.Every element
     * of this collection has to be DefUse object
     * @throws IllegalArgumentException if <code>uses</code> is
     *                                  <code>null</code>.
     */
    public void addUse( List usev ) throws IllegalArgumentException {        if ( usev == null ) {
            throw new IllegalArgumentException("SymbolicInstruction.addUse(): argument is null" );
        }

	for (Iterator i = usev.iterator (); i.hasNext ();)
	{
	    addUse ((DefUse) i.next ());
	}	    
    }

    /**
     * Adds a used variable for the instruction.
     * @param use variable use to be added.
     * @throws IllegalArgumentException if <code>use</code> is
     *                                  <code>null</code>.
     */
    public void addUse( DefUse use ) throws IllegalArgumentException {
        if ( use == null ) {
            throw new IllegalArgumentException(
                "SymbolicInstruction.addUse(): argument is null" );
        }
       
	if ( ( use instanceof PrimitiveDefUse ) &&
	   ( ((PrimitiveDefUse)use).getVariable() instanceof ConstantValue ))
	return;
	
        if ( uses == null ) {
            uses = new Vector();
        }
        uses.addElement( use );
    }

    /**
     * Sets the definition for the instruction.
     * @param definition variable defined by instruction.
     * @throws IllegalArgumentException if <code>definition</code> is
     *                                  <code>null</code>.
     */
    public void setDefinition( DefUse definition )
        throws IllegalArgumentException
    {
        if ( definition == null ) {
            throw new IllegalArgumentException(
                "SymbolicInstruction.setDefinition(): argument is null" );
        }
        this.definition = definition;
    }

    /**
     * Returns a string representation of the instruction.
     * @return string representation of symbolic instruction.
     */
    public String toString() {
        String str = new String();
        str += OpCode.getName( opCode )+ "(" + getOpcode() + ")\n";
        if ( operands != null ) {
            str += "\toperands = ";
            Vector o = getOperandsVector ();
            for (Enumeration i = o.elements (); i.hasMoreElements ();)
	    {
                str += i.nextElement ().toString() + " ";
            }
	    str += "\n";
	}
	if ( definition != null )
	  str += "\tdef = " + definition.toString() + "\n";
	if ( uses != null )
	  {
	    str += "\tuses: ";
	    for (Enumeration e = uses.elements (); e.hasMoreElements ();)
	      str += e.nextElement () + ",";
	    str += "\n";
	  }
        str += "\n";
        return str;
    }
}

