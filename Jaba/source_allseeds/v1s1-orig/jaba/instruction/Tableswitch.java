/* Copyright (c) 1999, The Ohio State University */

package jaba.instruction;

import jaba.instruction.visitor.SymbolicInstructionVisitor;
import jaba.constants.OpCode;

import java.util.Vector;
import java.util.Enumeration;

/**
 * <i>tableswitch</i> JVM bytecode instruction.
 * @author Jim Jones/Saurabh Sinha
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 */
public class Tableswitch extends SymbolicInstructionImpl
{

    /** Runtime exceptions. */
    private static final String[] runtimeExceptions = {};

  /**
   * Method to facilitate Visitor design pattern.
   * @param visitor  Visitor class to be used to visit this statement.
   * @see jaba.instruction.visitor.SymbolicInstructionVisitor
   */
  public void accept( SymbolicInstructionVisitor visitor )
    {
      ((jaba.instruction.visitor.SymbolicInstructionVisitorImpl) visitor).visitTableswitch( this );
    }

    /**
     * Returns a string representation of the <i>tableswitch</i>
     * instruction.
     * @return String representation of the <i>tableswitch</i> instruction.
     */
    public String toString() {
        String str = OpCode.getName( (byte)getOpcode() )+"("+getOpcode()+")\n";
        Vector operands = getOperandsVector ();
	Enumeration e = operands.elements ();
        str += "    default=" + e.nextElement () + "\n";
        for (int i = 1; e.hasMoreElements (); i++)
	{
            str += "    " + i + "=" + e.nextElement () + "\n";
        }
        return str;
    }

    /**
     * Returns the list of runtime exceptions that can be raised by this
     * instruction.
     * @return String[] Runtime exceptions that can be raised by this
     *                  instruction.
     */
    public String[] getRuntimeExceptions() {
        return runtimeExceptions;
    }

}
