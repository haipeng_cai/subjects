/* Copyright (c) 1999, The Ohio State University */

package jaba.instruction;

import jaba.instruction.visitor.SymbolicInstructionVisitor;

/**
 * <i>baload</i> JVM bytecode instruction. this instruction retrieves a byte 
 * from a byte array, expands it to an integer and places it on the operand 
 * stack. baload is also used to retrieve values from boolean arrays.
 * @author Jim Jones/Saurabh Sinha
 */
public class Baload extends SymbolicInstructionImpl
{

    /** Runtime exceptions. */
    private static final String[] 
	runtimeExceptions = { "java.lang.NullPointerException",
			      "java.lang.ArrayIndexOutOfBoundsException" };
    
    /**
     * Method to facilitate Visitor design pattern.
     * @param visitor  Visitor class to be used to visit this statement.
     * @see jaba.instruction.visitor.SymbolicInstructionVisitor
     */
    public void accept( SymbolicInstructionVisitor visitor )
    {
	((jaba.instruction.visitor.SymbolicInstructionVisitorImpl) visitor).visitBaload( this );
    }

    /**
     * Returns the list of runtime exceptions that can be raised by this
     * instruction.
     * @return String[] Runtime exceptions that can be raised by this
     *                  instruction.
     */
    public String[] getRuntimeExceptions() {
        return runtimeExceptions;
    }

}
