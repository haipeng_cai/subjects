/* Copyright (c) 1999, The Ohio State University */

package jaba.instruction;

import jaba.instruction.visitor.SymbolicInstructionVisitor;

/**
 * Represents <i>caload</i> JVM bytecode instruction.This instruction 
 * retrieves a character from an array of characters and pushes it on 
 * the operand stack.
 * @author Jim Jones/Saurabh Sinha
 */
public class Caload extends SymbolicInstructionImpl
{

    /** Runtime exceptions. */
    private static final String[]
        runtimeExceptions = { "java.lang.NullPointerException",
                              "java.lang.ArrayIndexOutOfBoundsException" };

  /**
   * Method to facilitate Visitor design pattern.
   * @param visitor  Visitor class to be used to visit this statement.
   * @see jaba.instruction.visitor.SymbolicInstructionVisitor
   */
  public void accept( SymbolicInstructionVisitor visitor )
    {
      ((jaba.instruction.visitor.SymbolicInstructionVisitorImpl) visitor).visitCaload( this );
    }

    /**
     * Returns the list of runtime exceptions that can be raised by this
     * instruction.
     * @return String[] Runtime exceptions that can be raised by this
     *                  instruction.
     */
    public String[] getRuntimeExceptions() {
        return runtimeExceptions;
    }

}
