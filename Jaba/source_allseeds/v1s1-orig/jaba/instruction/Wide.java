/* Copyright (c) 1999, The Ohio State University */

package jaba.instruction;

import jaba.instruction.visitor.SymbolicInstructionVisitor;

/**
 * <i>wide</i> JVM bytecode instruction. The Wide class will consume
 * the wide instruction and the one follows it as encapsulated instuction.
 * @author Jim Jones/Saurabh Sinha
 */
public class Wide extends SymbolicInstructionImpl
{

    /** Runtime exceptions. */
    private static final String[] runtimeExceptions = {};

  /** Opcode of the encapsulated instruction. */
  private byte encapsulatedOpCode;

  /**
   * Sets the opcode of the encapsulated instruction.
   * @param opcode opcode of the encapsulated instruction.
   */
  public void setEncapsulatedOpcode( byte opcode ) {
    encapsulatedOpCode = opcode;
  }

  /**
   * Returns the opcode of the encapsulated instruction.
   * @return Opcode of the encapsulated instruction.
   */
  public int getEncapsulatedOpcode() {
    return (encapsulatedOpCode & 0x000000ff);
  }

  /**
   * Method to facilitate Visitor design pattern.
   * @param visitor  Visitor class to be used to visit this statement.
   * @see jaba.instruction.visitor.SymbolicInstructionVisitor
   */
  public void accept( SymbolicInstructionVisitor visitor )
    {
      ((jaba.instruction.visitor.SymbolicInstructionVisitorImpl) visitor).visitWide( this );
    }

    /**
     * Returns the list of runtime exceptions that can be raised by this
     * instruction.
     * @return String[] Runtime exceptions that can be raised by this
     *                  instruction.
     */
    public String[] getRuntimeExceptions() {
        return runtimeExceptions;
    }

}
