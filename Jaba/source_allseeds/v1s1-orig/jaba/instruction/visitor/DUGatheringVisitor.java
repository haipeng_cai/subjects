/* Copyright (c) 2001, Georgia Institute of Technology */

package jaba.instruction.visitor;

import jaba.instruction.*;

import jaba.du.OperandStack;
import jaba.du.DefUse;
import jaba.du.LeafDefUse;
import jaba.du.ReferenceDefUse;
import jaba.du.ReferenceDefUseImpl;
import jaba.du.RecursiveDefUse;
import jaba.du.PrimitiveDefUse;
import jaba.du.PrimitiveDefUseImpl;
import jaba.du.ConstantValue;
import jaba.du.ConstantValueImpl;
import jaba.du.HasValue;
import jaba.du.TempVariable;
import jaba.du.TempVariableImpl;
import jaba.du.StaticElementDefUse;
import jaba.du.StaticElementDefUseImpl;
import jaba.du.ReferenceElementDefUse;
import jaba.du.ReferenceElementDefUseImpl;
import jaba.du.CastedReferenceDefUse;
import jaba.du.CastedReferenceDefUseImpl;
import jaba.du.TypeUse;
import jaba.du.TypeUseImpl;
import jaba.du.PlaceholderLocation;
import jaba.du.PlaceholderLocationImpl;
import jaba.du.ArrayElementDefUse;
import jaba.du.ArrayElementDefUseImpl;
import jaba.du.NewInstance;
import jaba.du.NewInstanceImpl;
import jaba.du.CastedReferenceElementDefUse;
import jaba.du.CastedReferenceElementDefUseImpl;
import jaba.du.ArrayLength;
import jaba.du.CastedArrayElementDefUse;
import jaba.du.CastedArrayElementDefUseImpl;

import jaba.sym.Method;
import jaba.sym.Class;
import jaba.sym.TypeEntry;
import jaba.sym.Field;
import jaba.sym.FieldImpl;
import jaba.sym.Primitive;
import jaba.sym.PrimitiveImpl;
import jaba.sym.LocalVariable;
import jaba.sym.LocalVariableImpl;
import jaba.sym.Array;
import jaba.sym.ReferenceType;
import jaba.sym.ReferenceTypeImpl;
import jaba.sym.Interface;

import jaba.graph.cfg.CFG;

import jaba.graph.StatementNode;
import jaba.graph.StatementNodeImpl;
import jaba.graph.MethodCallAttribute;
import jaba.graph.Edge;
import jaba.graph.EdgeImpl;
import jaba.graph.EdgeAttribute;

import jaba.constants.OpCode;

import jaba.tools.local.Factory;

import java.util.Vector;
import java.util.HashMap;
import java.util.Stack;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * Visitor to gather definition and use information across 
 * SymbolicInstructions. The definition and use information is kept
 * in StatementNode as DefUse objects. DefUse object is used to represent
 * memory location and type of an expression. The expression is parsed from 
 * left to right. Here are some
 * Examples. The examples are in the format of: type expression.
 * <pre>
 * Example 1: java object reference and field access.
 * MyObject1 o;   ==> R(o) Where R is short for ReferenceDefUse
 * MyObject2 o.a (accessing instance field a, whose type is MyObject2)  ==>
 *          RE(o=MyObject1(o), e=R(a)) Where RE means ReferenceElementDefUse,
 *  This o in o=.. means object, the e in e=.. means element.
 * int o.a.b (access instance field)  ==>
 *         RE(o=MyObject1(o), e=RE(o=MyObject2(a), e=P(b))) where
 *  P means PrimitiveDefUse
 * Example 2: java array element access
 * MyObject1[] arr; ==> R(arr)
 * MyObject1 arr[i]; ==> AE(a=MyObject1(arr), e=R(PlaceholderLocation))
 * where AE means ArrayElementDefUse, PlaceholderLocation is used to
 * represent the array element, as the memory address doesn't have a name.
 * int arr.length; ==> RE(o=MyObject[](arr), e=P(ArrayLength))
 * Where ArrayLength is used to represented the use of array's length.
 * MyObject2 arr[i].a; ==> 
 *    AE(a=MyObject1(arr), e=RE(o=PlaceholderLocation, e= R(a)))
 * Example 3: Casting Usage
 * ((String[])v)[i] (Cast array reference) ==>
 *    CAE(a = Object(v), t= String[], e=R(PlaceholderLocation))
 * (String)v[i] (Cast array element) ==>
 *    AE(a= Object(v), e= CR(v= R(PlaceholderLocation), t=String))
 * 
 *</pre>
 * The below documentation is intended for jaba developers and maintainers who
 * need to understand how the information is gathered.
 * The information is gathered by simulating the runtime operand stack. 
 * The result is kept in the corresponding Symbolic instructions as DefUse 
 * objects. later, it is summerized and 
 * kept in Node, mainly in StatementNode.
 * For method invocation instruction, such as invokespecial, besides 
 * gathering Def use info, it also collects some method call info. 
 * This includes the method called and calling relationship, call site node 
 * information. For overridable method invocation, such as invokeinterface,
 * it also gets all the callable methods. 
 *
 * Note: The operand stack simulation is similar to the runtime, with one
 * major EXCEPTION: for double-world operand, it consumes one entry in the
 * stack, instead of two. For the instructions which can operate on both
 * single and double word operand, it may introduce inaccuacy. In theory, it
 * may cause underflow on the stack, but it has not found such subject yet.
 *
 * @author Jim Jones -- <i>Created, Feb. 12, 1999</i>
 * @author Huaxing Wu -- <i> Revised, 8/7/02. Fixed several bugs, add 
 *                       comment. Create the DUVectorHelper class </i>
 * @author Jay Lofstead 2003/06/02 replaced elementAt with enumeration for better performance
 * @author Jay Lofstead 2003/06/05 enumeration additions
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/07/17 changed to use Factory to get attributes.
 */
public class DUGatheringVisitor extends SymbolicInstructionVisitorImpl
{
    /**A helper class to deal with the operand in the stack. The operand
     * in the stack is vector, and we have several common operations on
     * this vector. This class will provide an efficient way to get 
     * different information from the stack. Also it can provide some
     * checking to make sure the Stack is buliding correctly */
    private static class DUVectorHelper {
	private PrimitiveDefUse constPrim;
	private DefUse ref;
	private Vector nonref;
	private Vector nonConst;

	/** Construct an instance for the vector, walk through each
	 *  element in the vector, and put them in different fields
	 * based on their type.
	 * @param v -- The vector, its element suppose to be all DefUse.
	 */
	public DUVectorHelper(Vector v) {
	    nonref = new Vector(v.size());
	    nonConst = new Vector(v.size());
	    int constNum = 0;
	    int refNum = 0;
	    for (Iterator i = v.iterator (); i.hasNext ();)
	    {
		DefUse defuse = (DefUse) i.next ();
		if(defuse instanceof ReferenceDefUse) { //Leaf & Reference
		    ref = defuse;
		    refNum ++;
		    nonConst.add(defuse);
		}else if(defuse instanceof RecursiveDefUse) { //Recursive
		    RecursiveDefUse rDefuse = (RecursiveDefUse)defuse;
		    if(rDefuse.getLeafElement() instanceof ReferenceDefUse) {
			ref = defuse;
			refNum ++;
			nonConst.add(defuse);
		    }else { //Leafelement type is Primitive
			nonref.add(defuse);
			nonConst.add(defuse);
		    }
		}else { //PrimitiveDefUse
		    assert defuse instanceof PrimitiveDefUse : defuse;
		    
		    nonref.add(defuse);
		    
		    PrimitiveDefUse pDefuse = (PrimitiveDefUse)defuse;
		    if(pDefuse.getVariable() instanceof ConstantValue) {
			constPrim = pDefuse;
			constNum ++;
		    }else {
			nonConst.add(defuse);
		    }
		}
	    }//End of processing every element in the vector
	    assert constNum <=1:constNum;
	    assert nonref.size() + refNum == v.size():"nonref: "+ 
		nonref.size() + ", ref:" + refNum + " =? " + v.size();
	    assert refNum <= 1:refNum;
	    assert nonConst.size() + constNum == v.size() : "nonconst: " +
		nonConst.size() +", const:" + constNum + " =? " + v.size();
	}
    
	DefUse getReferenceElement() {
	    return ref;
	}
	
	/** A helper method to get the ReferenceDefUse object from a
	 *  DefUse Object, Assuming this object's leaf is ReferenceDefUse
	 */
	static ReferenceDefUse getReferenceDefUse(DefUse du) {
	    if(du instanceof ReferenceDefUse) {
		return (ReferenceDefUse)du;
	    }else if(du instanceof RecursiveDefUse) {
		DefUse leaf = ((RecursiveDefUse)du).getLeafElement();
		if(leaf instanceof ReferenceDefUse)
		return (ReferenceDefUse)leaf;
	    }
	    assert false;
	    return null;
	}

	PrimitiveDefUse getConstantElement() {
	    return constPrim;
	}
	
	Vector getNonRefElements() {
	    return nonref;
	}

	Vector getNonConstElements() {
	    return nonConst;
	}
    }	

  /** The operand stack to use while visiting instructions. */
  private OperandStack opStack;

  /** The current CFG node being processed.  This is used by the invoke*
   *  instructions to set up call site attributes for these nodes.
   */
  private StatementNode node;

  /** The method for which we are walking the CFG.  This is used by the
   *  invoke* instructions to set all "called by" and "calling" relationships
   *  in the Methods in the symbol table.
   */
  private Method method;

  /** The CFG being worked on.  This is used because new nodes may need to
   *  be added to the CFG if we find instructions that require the addition
   *  of new temporary definitions
   */
  private CFG cfg;

  /** A local variable table for all of those store instructions where the
   *  operand on the instruction does not refer to a local variable present
   *  in the source code.  We store them here and can retrieve them with a
   *  load instruction that also does not refer to a local variable present
   *  in the source.  This structure is a mapping from an Integer object 
   *  (representing the local variable table offset) to a Vector of DefUse
   *  objects (those DefUse's that were on the top of the stack for the 
   *  store instruction)
   */
  private HashMap syntheticLocalVariableMap;

  /**
   * Constructor.
   */
  public DUGatheringVisitor()
    {
      syntheticLocalVariableMap = new HashMap();
    }

  /**
   * Assigns the current operand stack.
   * @param opStack  The operand stack to use while visiting instructions.
   */
  public void setOperandStack( OperandStack opStack )
    {
      this.opStack = opStack;
    }

  /**
   * Returns the current operand stack.
   * @return The current operand stack.
   */
  public OperandStack getOperandStack()
    {
      return opStack;
    }

  /**
   * Assigns the current CFG node.
   * @param node  The current CFG node.
   */
  public void setNode( StatementNode node )
    {
      this.node = node;
    }

  /**
   * Returns the current CFG node.
   * @return The current CFG node.
   */
  public StatementNode getNode()
    {
      return node;
    }

  /**
   * Assigns the current method.
   * @param method  The current method being walked.
   */
  public void setMethod( Method method )
    {
      this.method = method;
    }

  /**
   * Assigns the CFG being worked on.
   * @param cfg  The CFG being worked on.
   */
  public void setCFG( CFG cfg )
    {
        this.cfg = cfg;
    }

  /**
   * Visits the <i>nop</i> instruction.
   * @param inst  <i>nop</i> instruction.
   */
  public void visitNop( Nop inst ) {}

  /**
   * Visits the <i>aconst_null</i> instruction. Create a referenceDefUse whose
   * value is constant null, and push it in the stack
   * @param inst  <i>aconst_null</i> instruction.
   */
  public void visitAconst_null( Aconst_null inst )
    {
      ConstantValue constant = new ConstantValueImpl ();
      constant.setPrimitive( PrimitiveImpl.valueOf(Primitive.NULL ) );
      constant.setIsValidValue( false );
      ReferenceDefUse refDU = new ReferenceDefUseImpl (constant );
      Vector item = new Vector();
      item.add( refDU );
      opStack.push( item );
    }

  /**
   * Visits the <i>iconst_m1</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>iconst_m1</i> instruction.
   */
  public void visitIconst_m1( Iconst_m1 inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>iconst_0</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>iconst_0</i> instruction.
   */
  public void visitIconst_0( Iconst_0 inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>iconst_1</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>iconst_1</i> instruction.
   */
  public void visitIconst_1( Iconst_1 inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>iconst_2</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>iconst_2</i> instruction.
   */
  public void visitIconst_2( Iconst_2 inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>iconst_3</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>iconst_3</i> instruction.
   */
  public void visitIconst_3( Iconst_3 inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>iconst_4</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>iconst_4</i> instruction.
   */
  public void visitIconst_4( Iconst_4 inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>iconst_5</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>iconst_5</i> instruction.
   */
  public void visitIconst_5( Iconst_5 inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>lconst_0</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>lconst_0</i> instruction.
   */
  public void visitLconst_0( Lconst_0 inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>lconst_1</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>lconst_1</i> instruction.
   */
  public void visitLconst_1( Lconst_1 inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>fconst_0</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>fconst_0</i> instruction.
   */
  public void visitFconst_0( Fconst_0 inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>fconst_1</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>fconst_1</i> instruction.
   */
  public void visitFconst_1( Fconst_1 inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>fconst_2</i> instruction.
   * @param inst  <i>fconst_2</i> instruction.
   */
  public void visitFconst_2( Fconst_2 inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>dconst_0</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>dconst_0</i> instruction.
   */
  public void visitDconst_0( Dconst_0 inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>dconst_1</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>dconst_1</i> instruction.
   */
  public void visitDconst_1( Dconst_1 inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>bipush</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>bipush</i> instruction.
   */
  public void visitBipush( Bipush inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>sipush</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>sipush</i> instruction.
   */
  public void visitSipush( Sipush inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>ldc</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>ldc</i> instruction.
   */
  public void visitLdc( Ldc inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>ldc_w</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>ldc_w</i> instruction.
   */
  public void visitLdc_w( Ldc_w inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>ldc2_w</i> instruction.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  <i>ldc2_w</i> instruction.
   */
  public void visitLdc2_w( Ldc2_w inst )
    {
      visitConstantPush( inst );
    }

  /**
   * Visits the <i>iload</i> instruction.
   * @param inst  <i>iload</i> instruction.
   */
  public void visitIload( Iload inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>lload</i> instruction.
   * @param inst  <i>lload</i> instruction.
   */
  public void visitLload( Lload inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>fload</i> instruction.
   * @param inst  <i>fload</i> instruction.
   */
  public void visitFload( Fload inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>dload</i> instruction.
   * @param inst  <i>dload</i> instruction.
   */
  public void visitDload( Dload inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>aload</i> instruction.
   * @param inst  <i>aload</i> instruction.
   */
  public void visitAload( Aload inst )
    {
      visitReferenceLoad( inst );
    }

  /**
   * Visits the <i>iload_0</i> instruction.
   * @param inst  <i>iload_0</i> instruction.
   */
  public void visitIload_0( Iload_0 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>iload_1</i> instruction.
   * @param inst  <i>iload_1</i> instruction.
   */
  public void visitIload_1( Iload_1 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>iload_2</i> instruction.
   * @param inst  <i>iload_2</i> instruction.
   */
  public void visitIload_2( Iload_2 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>iload_3</i> instruction.
   * @param inst  <i>iload_3</i> instruction.
   */
  public void visitIload_3( Iload_3 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>lload_0</i> instruction.
   * @param inst  <i>lload_0</i> instruction.
   */
  public void visitLload_0( Lload_0 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>lload_1</i> instruction.
   * @param inst  <i>lload_1</i> instruction.
   */
  public void visitLload_1( Lload_1 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>lload_2</i> instruction.
   * @param inst  <i>lload_2</i> instruction.
   */
  public void visitLload_2( Lload_2 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>lload_3</i> instruction.
   * @param inst  <i>lload_3</i> instruction.
   */
  public void visitLload_3( Lload_3 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>fload_0</i> instruction.
   * @param inst  <i>fload_0</i> instruction.
   */
  public void visitFload_0( Fload_0 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>fload_1</i> instruction.
   * @param inst  <i>fload_1</i> instruction.
   */
  public void visitFload_1( Fload_1 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>fload_2</i> instruction.
   * @param inst  <i>fload_2</i> instruction.
   */
  public void visitFload_2( Fload_2 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>fload_3</i> instruction.
   * @param inst  <i>fload_3</i> instruction.
   */
  public void visitFload_3( Fload_3 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>dload_0</i> instruction.
   * @param inst  <i>dload_0</i> instruction.
   */
  public void visitDload_0( Dload_0 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>dload_1</i> instruction.
   * @param inst  <i>dload_1</i> instruction.
   */
  public void visitDload_1( Dload_1 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>dload_2</i> instruction.
   * @param inst  <i>dload_2</i> instruction.
   */
  public void visitDload_2( Dload_2 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>dload_3</i> instruction.
   * @param inst  <i>dload_3</i> instruction.
   */
  public void visitDload_3( Dload_3 inst )
    {
      visitPrimitiveLoad( inst );
    }

  /**
   * Visits the <i>aload_0</i> instruction.
   * @param inst  <i>aload_0</i> instruction.
   */
  public void visitAload_0( Aload_0 inst )
    {
      visitReferenceLoad( inst );
    }

  /**
   * Visits the <i>aload_1</i> instruction.
   * @param inst  <i>aload_1</i> instruction.
   */
  public void visitAload_1( Aload_1 inst )
    {
      visitReferenceLoad( inst );
    }

  /**
   * Visits the <i>aload_2</i> instruction.
   * @param inst  <i>aload_2</i> instruction.
   */
  public void visitAload_2( Aload_2 inst )
    {
      visitReferenceLoad( inst );
    }

  /**
   * Visits the <i>aload_3</i> instruction.
   * @param inst  <i>aload_3</i> instruction.
   */
  public void visitAload_3( Aload_3 inst )
    {
      visitReferenceLoad( inst );
    }

  /**
   * Visits the <i>iaload</i> instruction.
   * @param inst  <i>iaload</i> instruction.
   */
  public void visitIaload( Iaload inst )
    {
      visitArrayElementLoad( inst );
    }

  /**
   * Visits the <i>laload</i> instruction.
   * @param inst  <i>laload</i> instruction.
   */
  public void visitLaload( Laload inst )
    {
      visitArrayElementLoad( inst );
    }

  /**
   * Visits the <i>faload</i> instruction.
   * @param inst  <i>faload</i> instruction.
   */
  public void visitFaload( Faload inst )
    {
      visitArrayElementLoad( inst );
    }

  /**
   * Visits the <i>daload</i> instruction.
   * @param inst  <i>daload</i> instruction.
   */
  public void visitDaload( Daload inst )
    {
      visitArrayElementLoad( inst );
    }

  /**
   * Visits the <i>aaload</i> instruction.Retrieves an object reference from 
   * an array of objects and places it on the stack. Before this instruction,
   * the top two entries of stack will be index and arrayref. arrayref is a 
   * reference to an array of objects, represented by a ReferenceDefUse object.
   * index is an int. The arrayref and index are removed from the stack, 
   * and the object reference at the given index in the array is pushed 
   * onto the stack. And this is represented as an arrayelementDefUse object,
   * evolved from the arrayref's ReferenceDefUse object.
   * @param inst  <i>aaload</i> instruction.
   */
  public void visitAaload( Aaload inst )
    {
      visitArrayElementLoad( inst );
    }

  /**
   * Visits the <i>baload</i> instruction. Retrieve a byte from a byte array 
   * or a boolean value from a boolean array. The process is similar to 
   * {@link #visitAaload(Aaload)}.
   * @param inst  <i>baload</i> instruction.
   */
  public void visitBaload( Baload inst )
    {
      visitArrayElementLoad( inst );
    }

  /**
   * Visits the <i>caload</i> instruction.
   * @param inst  <i>caload</i> instruction.
   */
  public void visitCaload( Caload inst )
    {
      visitArrayElementLoad( inst );
    }

  /**
   * Visits the <i>saload</i> instruction.
   * @param inst  <i>saload</i> instruction.
   */
  public void visitSaload( Saload inst )
    {
      visitArrayElementLoad( inst );
    }

  /**
   * Visits the <i>istore</i> instruction.
   * @param inst  <i>istore</i> instruction.
   */
  public void visitIstore( Istore inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>lstore</i> instruction.
   * @param inst  <i>lstore</i> instruction.
   */
  public void visitLstore( Lstore inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>fstore</i> instruction.
   * @param inst  <i>fstore</i> instruction.
   */
  public void visitFstore( Fstore inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>dstore</i> instruction.
   * @param inst  <i>dstore</i> instruction.
   */
  public void visitDstore( Dstore inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>astore</i> instruction.
   * @param inst  <i>astore</i> instruction.
   */
  public void visitAstore( Astore inst )
    {
      visitReferenceStore( inst );
    }

  /**
   * Visits the <i>istore_0</i> instruction.
   * @param inst  <i>istore_0</i> instruction.
   */
  public void visitIstore_0( Istore_0 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>istore_1</i> instruction.
   * @param inst  <i>istore_1</i> instruction.
   */
  public void visitIstore_1( Istore_1 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>istore_2</i> instruction.
   * @param inst  <i>istore_2</i> instruction.
   */
  public void visitIstore_2( Istore_2 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>istore_3</i> instruction.
   * @param inst  <i>istore_3</i> instruction.
   */
  public void visitIstore_3( Istore_3 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>lstore_0</i> instruction.
   * @param inst  <i>lstore_0</i> instruction.
   */
  public void visitLstore_0( Lstore_0 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>lstore_1</i> instruction.
   * @param inst  <i>lstore_1</i> instruction.
   */
  public void visitLstore_1( Lstore_1 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>lstore_2</i> instruction.
   * @param inst  <i>lstore_2</i> instruction.
   */
  public void visitLstore_2( Lstore_2 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>lstore_3</i> instruction.
   * @param inst  <i>lstore_3</i> instruction.
   */
  public void visitLstore_3( Lstore_3 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>fstore_0</i> instruction.
   * @param inst  <i>fstore_0</i> instruction.
   */
  public void visitFstore_0( Fstore_0 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>fstore_1</i> instruction.
   * @param inst  <i>fstore_1</i> instruction.
   */
  public void visitFstore_1( Fstore_1 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>fstore_2</i> instruction.
   * @param inst  <i>fstore_2</i> instruction.
   */
  public void visitFstore_2( Fstore_2 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>fstore_3</i> instruction.
   * @param inst  <i>fstore_3</i> instruction.
   */
  public void visitFstore_3( Fstore_3 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>dstore_0</i> instruction.
   * @param inst  <i>dstore_0</i> instruction.
   */
  public void visitDstore_0( Dstore_0 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>dstore_1</i> instruction.
   * @param inst  <i>dstore_1</i> instruction.
   */
  public void visitDstore_1( Dstore_1 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>dstore_2</i> instruction.
   * @param inst  <i>dstore_2</i> instruction.
   */
  public void visitDstore_2( Dstore_2 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>dstore_3</i> instruction.
   * @param inst  <i>dstore_3</i> instruction.
   */
  public void visitDstore_3( Dstore_3 inst )
    {
      visitPrimitiveStore( inst );
    }

  /**
   * Visits the <i>astore_0</i> instruction.
   * @param inst  <i>astore_0</i> instruction.
   */
  public void visitAstore_0( Astore_0 inst )
    {
      visitReferenceStore( inst );
    }

  /**
   * Visits the <i>astore_1</i> instruction.
   * @param inst  <i>astore_1</i> instruction.
   */
  public void visitAstore_1( Astore_1 inst )
    {
      visitReferenceStore( inst );
    }

  /**
   * Visits the <i>astore_2</i> instruction.
   * @param inst  <i>astore_2</i> instruction.
   */
  public void visitAstore_2( Astore_2 inst )
    {
      visitReferenceStore( inst );
    }

  /**
   * Visits the <i>astore_3</i> instruction.
   * @param inst  <i>astore_3</i> instruction.
   */
  public void visitAstore_3( Astore_3 inst )
    {
      visitReferenceStore( inst );
    }

  /**
   * Visits the <i>iastore</i> instruction.
   * @param inst  <i>iastore</i> instruction.
   */
  public void visitIastore( Iastore inst )
    {
      visitArrayElementStore( inst );
    }

  /**
   * Visits the <i>lastore</i> instruction.
   * @param inst  <i>lastore</i> instruction.
   */
  public void visitLastore( Lastore inst )
    {
      visitArrayElementStore( inst );
    }

  /**
   * Visits the <i>fastore</i> instruction.
   * @param inst  <i>fastore</i> instruction.
   */
  public void visitFastore( Fastore inst )
    {
      visitArrayElementStore( inst );
    }

  /**
   * Visits the <i>dastore</i> instruction.
   * @param inst  <i>dastore</i> instruction.
   */
  public void visitDastore( Dastore inst )
    {
      visitArrayElementStore( inst );
    }

  /**
   * Visits the <i>aastore</i> instruction.This stores an object reference in 
   * an array of objects. Before this instruction, the top 3 entries of stack 
   * are value, index, arrayref. arrayref is a reference to an array of object
   * references, represented as a ReferenceDefUse Object. index is an int. 
   * value is the object reference to be stored in the array. The value is
   * represented as a ReferenceDefUse Object.
   * This instruction will remove arrayref, index and value from the stack, 
   * and value is stored in the array at the given index, represented as an 
   * ArrayElementDefUse object. This is when this Array element is defined.
   * The runtime type of value must be assignment-compatible with the type 
   * of the array. 
   * @param inst  <i>aastore</i> instruction.
   */
  public void visitAastore( Aastore inst )
    {
      visitArrayElementStore( inst );
    }

  /**
   * Visits the <i>bastore</i> instruction.
   * @param inst  <i>bastore</i> instruction.
   */
  public void visitBastore( Bastore inst )
    {
      visitArrayElementStore( inst );
    }

  /**
   * Visits the <i>castore</i> instruction.
   * @param inst  <i>castore</i> instruction.
   */
  public void visitCastore( Castore inst )
    {
      visitArrayElementStore( inst );
    }

  /**
   * Visits the <i>sastore</i> instruction.
   * @param inst  <i>sastore</i> instruction.
   */
  public void visitSastore( Sastore inst )
    {
      visitArrayElementStore( inst );
    }

  /**
   * Visits the <i>pop</i> instruction.
   * @param inst  <i>pop</i> instruction.
   */
  public void visitPop( Pop inst )
    {
      opStack.pop();
    }

  /**
   * Visits the <i>pop2</i> instruction. Pop up the top vector, if
   * it has more than one element or the element is not a 64 bit data
   * type, then pop up one vector from the stack again.
   * @param inst  <i>pop2</i> instruction.
   */
  public void visitPop2( Pop2 inst )
    {
      Vector top = opStack.pop();
      if ( top.size() == 1 ) {
          if ( is64BitDataType( (DefUse)top.elementAt( 0 ) ) ) {
              return;
          }
      }
      assert top.size() == 1:inst.toString() + ":" + top;
      opStack.pop();
    }


    /**
     * Determines if a <code>DefUse</code> object represents a 64-bit
     * data type. <code>long</code> and <code>double</code> primitive types
     * 64-bit data types.
     * @param elem <code>DefUse</code> object.
     * @return <code>true</code> if <code>elem</code> represents a 64-bit
     *         data type; <code>false</code> otherwise.
     */
    private boolean is64BitDataType( DefUse elem ) {
        if ( elem instanceof PrimitiveDefUse ) {
            HasValue var = ((PrimitiveDefUse)elem).getVariable();
            if ( var instanceof ConstantValue ) 
                return ((ConstantValue)var).getIs64Bit();
            if ( ( var instanceof TempVariable ) ||
                 ( var instanceof LocalVariable ) ||
                 ( var instanceof Field ) ) {
                TypeEntry typeEntry;
                if ( var instanceof LocalVariable )
                    typeEntry = (jaba.sym.TypeEntry) ((LocalVariable)var).getType();
                else // var instanceof Field
                    typeEntry = (jaba.sym.TypeEntry) ((Field)var).getType();
                if ( typeEntry == null ) {
		    throw new RuntimeException("Error:" +
                        "DUGatheringVisitor.is64BitDataType():"+
                        "TempVariable has null type");
		}
                if ( typeEntry instanceof Primitive ) {
                    int type = ((Primitive)typeEntry).getType();
                    if ( type == Primitive.LONG || type == Primitive.DOUBLE ) {
                        return true;
                    }
                }
            }
        }
	if ( elem instanceof RecursiveDefUse ) {
            LeafDefUse leaf = ((RecursiveDefUse)elem).getLeafElement();
	    return is64BitDataType( leaf );
        }
        return false;
    }

  /**
   * Visits the <i>dup</i> instruction.
   * @param inst  <i>dup</i> instruction.
   */
  public void visitDup( Dup inst )
    {
      Vector stack1 = opStack.pop();
      opStack.push( vectorClone( stack1 ) );
      opStack.push( stack1 );
    }

  /**
   * Visits the <i>dup_x1</i> instruction.
   * @param inst  <i>dup_x1</i> instruction.
   */
  public void visitDup_x1( Dup_x1 inst )
    {
      Vector stack1 = opStack.pop();
      Vector stack2 = opStack.pop();
      opStack.push( vectorClone( stack1 ) );
      opStack.push( stack2 );
      opStack.push( stack1 );
    }

  /**
   * Visits the <i>dup_x2</i> instruction.
   * @param inst  <i>dup_x2</i> instruction.
   */
  public void visitDup_x2( Dup_x2 inst )
    {
      Vector stack1 = opStack.pop();
      Vector stack2 = opStack.pop();
      if ( is64BitDataType( (DefUse)stack2.elementAt( 0 ) ) ) {
	opStack.push( vectorClone( stack1 ) );
	opStack.push( stack2 );
	opStack.push( stack1 );
	return;
      }

      for(Iterator e = stack2.iterator (); e.hasNext ();)
      {
	  if (is64BitDataType ((DefUse) e.next ()))
	  {
	      assert false: inst.toString() + ": " + stack1;
	  }
      }

      Vector stack3 = opStack.pop();
      opStack.push( vectorClone( stack1 ) );
      opStack.push( stack3 );
      opStack.push( stack2 );
      opStack.push( stack1 );
    }

  /**
   * Visits the <i>dup2</i> instruction.
   * @param inst  <i>dup2</i> instruction.
   */
  public void visitDup2( Dup2 inst )
    {
      Vector stack1 = opStack.pop();
      if ( is64BitDataType( (DefUse)stack1.elementAt( 0 ) ) ) {
	opStack.push( vectorClone( stack1 ) );
	opStack.push( stack1 );
	return;
      }

      for (Iterator e = stack1.iterator (); e.hasNext ();)
      {
	  if (is64BitDataType ((DefUse) e.next ()))
	  {
	      assert false: inst.toString() + ": " + stack1;
	  }
      }
      Vector stack2 = opStack.pop();
      opStack.push( vectorClone( stack2 ) );
      opStack.push( vectorClone( stack1 ) );
      opStack.push( stack2 );
      opStack.push( stack1 );
    }

  /**
   * Visits the <i>dup2_x1</i> instruction.
   * @param inst  <i>dup2_x1</i> instruction.
   */
  public void visitDup2_x1( Dup2_x1 inst )
    {
      Vector stack1 = opStack.pop();
      Vector stack2 = opStack.pop();
      if ( is64BitDataType( (DefUse)stack1.elementAt( 0 ) ) ) {
	opStack.push( vectorClone( stack1 ) );
	opStack.push( stack2 );
	opStack.push( stack1 );
	return;
      }
      for (Iterator e = stack1.iterator (); e.hasNext ();)
      {
	  if (is64BitDataType ((DefUse) e.next ()))
	  {
	      assert false: inst.toString() + ": " + stack1;
	  }
      }

      Vector stack3 = opStack.pop();
      opStack.push( vectorClone( stack2 ) );
      opStack.push( vectorClone( stack1 ) );
      opStack.push( stack3 );
      opStack.push( stack2 );
      opStack.push( stack1 );
    }

  /**
   * Visits the <i>dup2_x2</i> instruction.
   * @param inst  <i>dup2_x2</i> instruction.
   */
  public void visitDup2_x2( Dup2_x2 inst )
    {
      Vector stack1 = opStack.pop();
      Vector stack2 = opStack.pop();
      Vector stack3;
      if ( is64BitDataType( (DefUse)stack1.elementAt( 0 ) ) ) {
	if ( is64BitDataType( (DefUse)stack2.elementAt( 0 ) ) ) {
	  opStack.push( vectorClone( stack1 ) );
	  opStack.push( stack2 );
	  opStack.push( stack1 );
	  return;
	}

	stack3 = opStack.pop();
	opStack.push( vectorClone( stack1 ) );
	opStack.push( stack3 );
	opStack.push( stack2 );
	opStack.push( stack1 );
	return;
      }

      for (Iterator e = stack1.iterator (); e.hasNext ();)
      {
	  if (is64BitDataType ((DefUse) e.next ()))
	  {
	      assert false: inst.toString() + ": " + stack1;
	  }
      }
      for (Iterator e = stack2.iterator (); e.hasNext ();)
      {
	  if (is64BitDataType ((DefUse) e.next ()))
	  {
	      assert false: inst.toString() + ": " + stack2;
	  }
      }
      
      stack3 = opStack.pop();
      Vector stack4 = opStack.pop();
      opStack.push( vectorClone( stack2 ) );
      opStack.push( vectorClone( stack1 ) );
      opStack.push( stack4 );
      opStack.push( stack3 );
      opStack.push( stack2 );
      opStack.push( stack1 );
    }

  /**
   * This method clones a vector so that each object on the stack (in this
   * case, I am assuming the elements are DefUse objects).  I had to provide
   * this because Vector.clone() does not seem to return a vector with a clone
   * of each of its elements.
   * @param origVec  Vector of DefUse objects to be cloned.
   * @return A clone of <code>origVec</code>.
   */
  private Vector vectorClone( Vector origVec )
    {
      Vector returnVec = new Vector();
      for (Iterator e = origVec.iterator (); e.hasNext ();)
	{
	  DefUse element = (DefUse) e.next ();
	  if ( element instanceof PrimitiveDefUse )
	    returnVec.add( ((PrimitiveDefUse) element).clone() );
	  else if ( element instanceof ReferenceDefUse )
	    returnVec.add( ((ReferenceDefUse) element).clone() );
	  else // element instance of RecursiveDefUse
	    returnVec.add( ((RecursiveDefUse) element).clone() );
	}
      return returnVec;
    }
      
  /**
   * Visits the <i>swap</i> instruction.
   * @param inst  <i>swap</i> instruction.
   */
  public void visitSwap( Swap inst )
    {
      Vector stack1 = opStack.pop();
      Vector stack2 = opStack.pop();
      opStack.push( stack1 );
      opStack.push( stack2 );
    }

  /**
   * Visits the <i>iadd</i> instruction.
   * @param inst  <i>iadd</i> instruction.
   */
  public void visitIadd( Iadd inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>ladd</i> instruction.
   * @param inst  <i>ladd</i> instruction.
   */
  public void visitLadd( Ladd inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>fadd</i> instruction.
   * @param inst  <i>fadd</i> instruction.
   */
  public void visitFadd( Fadd inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>dadd</i> instruction.
   * @param inst  <i>dadd</i> instruction.
   */
  public void visitDadd( Dadd inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>isub</i> instruction.
   * @param inst  <i>isub</i> instruction.
   */
  public void visitIsub( Isub inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>lsub</i> instruction.
   * @param inst  <i>lsub</i> instruction.
   */
  public void visitLsub( Lsub inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>fsub</i> instruction.
   * @param inst  <i>fsub</i> instruction.
   */
  public void visitFsub( Fsub inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>dsub</i> instruction.
   * @param inst  <i>dsub</i> instruction.
   */
  public void visitDsub( Dsub inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>imul</i> instruction.
   * @param inst  <i>imul</i> instruction.
   */
  public void visitImul( Imul inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>lmul</i> instruction.
   * @param inst  <i>lmul</i> instruction.
   */
  public void visitLmul( Lmul inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>fmul</i> instruction.
   * @param inst  <i>fmul</i> instruction.
   */
  public void visitFmul( Fmul inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>dmul</i> instruction.
   * @param inst  <i>dmul</i> instruction.
   */
  public void visitDmul( Dmul inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>idiv</i> instruction.
   * @param inst  <i>idiv</i> instruction.
   */
  public void visitIdiv( Idiv inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>ldiv</i> instruction.
   * @param inst  <i>ldiv</i> instruction.
   */
  public void visitLdiv( Ldiv inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>fdiv</i> instruction.
   * @param inst  <i>fdiv</i> instruction.
   */
  public void visitFdiv( Fdiv inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>ddiv</i> instruction.
   * @param inst  <i>ddiv</i> instruction.
   */
  public void visitDdiv( Ddiv inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>irem</i> instruction.
   * @param inst  <i>irem</i> instruction.
   */
  public void visitIrem( Irem inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>lrem</i> instruction.
   * @param inst  <i>lrem</i> instruction.
   */
  public void visitLrem( Lrem inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>frem</i> instruction.
   * @param inst  <i>frem</i> instruction.
   */
  public void visitFrem( Frem inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>drem</i> instruction.
   * @param inst  <i>drem</i> instruction.
   */
  public void visitDrem( Drem inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>ineg</i> instruction. This instruction will set the top
   * operand's value to negative of its current value if it's a constant.
   * @param inst  <i>ineg</i> instruction.
   */
  public void visitIneg( Ineg inst )
    {
      Vector stack1 = opStack.peek();
      DUVectorHelper operand = new DUVectorHelper(stack1);
      PrimitiveDefUse constOp1 = operand.getConstantElement();

      if ( constOp1 == null )
	  return;

      if ( !((ConstantValue)(constOp1.getVariable())).getIsValidValue() )
	  return;

      ((ConstantValue)(constOp1.getVariable())).setValue( 
		  -((ConstantValue)(constOp1.getVariable())).getValue() );
    }

  /**
   * Visits the <i>lneg</i> instruction.
   * @param inst  <i>lneg</i> instruction.
   */
  public void visitLneg( Lneg inst ){}

  /**
   * Visits the <i>fneg</i> instruction.
   * @param inst  <i>fneg</i> instruction.
   */
  public void visitFneg( Fneg inst ){}

  /**
   * Visits the <i>dneg</i> instruction.
   * @param inst  <i>dneg</i> instruction.
   */
  public void visitDneg( Dneg inst ){}

  /**
   * Visits the <i>ishl</i> instruction.
   * @param inst  <i>ishl</i> instruction.
   */
  public void visitIshl( Ishl inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>lshl</i> instruction.
   * @param inst  <i>lshl</i> instruction.
   */
  public void visitLshl( Lshl inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>ishr</i> instruction.
   * @param inst  <i>ishr</i> instruction.
   */
  public void visitIshr( Ishr inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>lshr</i> instruction.
   * @param inst  <i>lshr</i> instruction.
   */
  public void visitLshr( Lshr inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>iushr</i> instruction.
   * @param inst  <i>iushr</i> instruction.
   */
  public void visitIushr( Iushr inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>lushr</i> instruction.
   * @param inst  <i>lushr</i> instruction.
   */
  public void visitLushr( Lushr inst )
    {
      visitArithmeticInstruction( inst ); 
    }

  /**
   * Visits the <i>iand</i> instruction.
   * @param inst  <i>iand</i> instruction.
   */
  public void visitIand( Iand inst )
    {
      opStack.merge( 2 );
    }

  /**
   * Visits the <i>land</i> instruction.
   * @param inst  <i>land</i> instruction.
   */
  public void visitLand( Land inst )
    {
      opStack.merge( 2 );
    }

  /**
   * Visits the <i>ior</i> instruction.
   * @param inst  <i>ior</i> instruction.
   */
  public void visitIor( Ior inst )
    {
      opStack.merge( 2 );
    }

  /**
   * Visits the <i>lor</i> instruction.
   * @param inst  <i>lor</i> instruction.
   */
  public void visitLor( Lor inst )
    {
      opStack.merge( 2 );
    }

  /**
   * Visits the <i>ixor</i> instruction.
   * @param inst  <i>ixor</i> instruction.
   */
  public void visitIxor( Ixor inst )
    {
      opStack.merge( 2 );
    }

  /**
   * Visits the <i>lxor</i> instruction.
   * @param inst  <i>lxor</i> instruction.
   */
  public void visitLxor( Lxor inst )
    {
      opStack.merge( 2 );
    }

  /**
   * Visits the <i>iinc</i> instruction.
   * @param inst  <i>iinc</i> instruction.
   */
  public void visitIinc( Iinc inst )
    {
      Vector operands = inst.getOperandsVector ();
      for (Iterator i = operands.iterator (); i.hasNext ();)
	{
		Object o = i.next ();
	  if (o instanceof LocalVariable )
	    {
	      PrimitiveDefUse localVar = new PrimitiveDefUseImpl ((LocalVariableImpl) o);
	      checkDef( localVar );
	      inst.setDefinition( localVar );
	      inst.addUse( localVar );
	    }
	}
    }

  /**
   * Visits the <i>i2l</i> instruction.
   * @param inst  <i>i2l</i> instruction.
   */
  public void visitI2l( I2l inst )
    {
      visitWideningInst( inst, Primitive.LONG );
    }

  /**
   * Visits the <i>i2f</i> instruction.
   * @param inst  <i>i2f</i> instruction.
   */
  public void visitI2f( I2f inst ){}

  /**
   * Visits the <i>i2d</i> instruction.
   * @param inst  <i>i2d</i> instruction.
   */
  public void visitI2d( I2d inst )
    {
      visitWideningInst( inst, Primitive.DOUBLE );
    }

  /**
   * Visits the <i>l2i</i> instruction.
   * @param inst  <i>l2i</i> instruction.
   */
  public void visitL2i( L2i inst )
    {
      visitNarrowingInst( inst, Primitive.INT );
    }

  /**
   * Visits the <i>l2f</i> instruction.
   * @param inst  <i>l2f</i> instruction.
   */
  public void visitL2f( L2f inst )
    {
      visitNarrowingInst( inst, Primitive.FLOAT );
    }

  /**
   * Visits the <i>l2d</i> instruction.
   * @param inst  <i>l2d</i> instruction.
   */
  public void visitL2d( L2d inst ){}

  /**
   * Visits the <i>f2i</i> instruction.
   * @param inst  <i>f2i</i> instruction.
   */
  public void visitF2i( F2i inst ){}

  /**
   * Visits the <i>f2l</i> instruction.
   * @param inst  <i>f2l</i> instruction.
   */
  public void visitF2l( F2l inst )
    {
      visitWideningInst( inst, Primitive.LONG );
    }

  /**
   * Visits the <i>f2d</i> instruction.
   * @param inst  <i>f2d</i> instruction.
   */
  public void visitF2d( F2d inst )
    {
      visitWideningInst( inst, Primitive.DOUBLE );
    }

  /**
   * Visits the <i>d2i</i> instruction.
   * @param inst  <i>d2i</i> instruction.
   */
  public void visitD2i( D2i inst )
    {
      visitNarrowingInst( inst, Primitive.INT );
    }

  /**
   * Visits the <i>d2l</i> instruction.
   * @param inst  <i>d2l</i> instruction.
   */
  public void visitD2l( D2l inst ){}

  /**
   * Visits the <i>d2f</i> instruction.
   * @param inst  <i>d2f</i> instruction.
   */
  public void visitD2f( D2f inst )
    {
      visitNarrowingInst( inst, Primitive.FLOAT );
    }

  /**
   * Visits the <i>i2b</i> instruction.
   * @param inst  <i>i2b</i> instruction.
   */
  public void visitI2b( I2b inst ){}

  /**
   * Visits the <i>i2c</i> instruction.
   * @param inst  <i>i2c</i> instruction.
   */
  public void visitI2c( I2c inst ){}

  /**
   * Visits the <i>i2s</i> instruction.
   * @param inst  <i>i2s</i> instruction.
   */
  public void visitI2s( I2s inst ){}

  /**
   * Visits the <i>lcmp</i> instruction.
   * @param inst  <i>lcmp</i> instruction.
   */
  public void visitLcmp( Lcmp inst )
    {
      visitPrimitiveCompare( inst );
    }

  /**
   * Visits the <i>fcmpl</i> instruction.
   * @param inst  <i>fcmpl</i> instruction.
   */
  public void visitFcmpl( Fcmpl inst )
    {
      visitPrimitiveCompare( inst );
    }

  /**
   * Visits the <i>fcmpg</i> instruction.
   * @param inst  <i>fcmpg</i> instruction.
   */
  public void visitFcmpg( Fcmpg inst )
    {
      visitPrimitiveCompare( inst );
    }

  /**
   * Visits the <i>dcmpl</i> instruction.
   * @param inst  <i>dcmpl</i> instruction.
   */
  public void visitDcmpl( Dcmpl inst )
    {
      visitPrimitiveCompare( inst );
    }

  /**
   * Visits the <i>dcmpg</i> instruction.
   * @param inst  <i>dcmpg</i> instruction.
   */
  public void visitDcmpg( Dcmpg inst )
    {
      visitPrimitiveCompare( inst );
    }

  /**
   * Visits the <i>ifeq</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  <i>ifeq</i> instruction.
   */
  public void visitIfeq( Ifeq inst )
    {
      visitOneOpBranch( inst );
    }

  /**
   * Visits the <i>ifne</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  <i>ifne</i> instruction.
   */
  public void visitIfne( Ifne inst )
    {
      visitOneOpBranch( inst );
    }

  /**
   * Visits the <i>iflt</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  <i>iflt</i> instruction.
   */
  public void visitIflt( Iflt inst )
    {
      visitOneOpBranch( inst );
    }

  /**
   * Visits the <i>ifge</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  <i>ifge</i> instruction.
   */
  public void visitIfge( Ifge inst )
    {
      visitOneOpBranch( inst );
    }

  /**
   * Visits the <i>ifgt</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  <i>ifgt</i> instruction.
   */
  public void visitIfgt( Ifgt inst )
    {
      visitOneOpBranch( inst );
    }

  /**
   * Visits the <i>ifle</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  <i>ifle</i> instruction.
   */
  public void visitIfle( Ifle inst )
    {
      visitOneOpBranch( inst );
    }

  /**
   * Visits the <i>if_icmpeq</i> instruction.
   * Pop out the top two operand and also add useing these two operand 
   * in the inst.
   * @param inst  <i>if_icmpeq</i> instruction.
   */
  public void visitIf_icmpeq( If_icmpeq inst )
    {
      visitTwoOpBranch( inst );
    }

  /**
   * Visits the <i>if_icmpne</i> instruction.
   * Pop out the top two operand and also add useing these two operand 
   * in the inst.
   * @param inst  <i>if_icmpne</i> instruction.
   */
  public void visitIf_icmpne( If_icmpne inst )
    {
      visitTwoOpBranch( inst );
    }

  /**
   * Visits the <i>if_icmplt</i> instruction.
   * Pop out the top two operand and also add useing these two operand 
   * in the inst.
   * @param inst  <i>if_icmplt</i> instruction.
   */
  public void visitIf_icmplt( If_icmplt inst )
    {
      visitTwoOpBranch( inst );
    }

  /**
   * Visits the <i>if_icmpge</i> instruction.
   * Pop out the top two operand and also add useing these two operand 
   * in the inst.
   * @param inst  <i>if_icmpge</i> instruction.
   */
  public void visitIf_icmpge( If_icmpge inst )
    {
      visitTwoOpBranch( inst );
    }

  /**
   * Visits the <i>if_icmpgt</i> instruction.
   * Pop out the top two operand and also add useing these two operand 
   * in the inst.
   * @param inst  <i>if_icmpgt</i> instruction.
   */
  public void visitIf_icmpgt( If_icmpgt inst )
    {
      visitTwoOpBranch( inst );
    }

  /**
   * Visits the <i>if_icmple</i> instruction.
   * Pop out the top two operand and also add useing these two operand 
   * in the inst.
   * @param inst  <i>if_icmple</i> instruction.
   */
  public void visitIf_icmple( If_icmple inst )
    {
      visitTwoOpBranch( inst );
    }

  /**
   * Visits the <i>if_acmpeq</i> instruction.
   * Pop out the top two operand and also add useing these two operand 
   * in the inst.
   * @param inst  <i>if_acmpeq</i> instruction.
   */
  public void visitIf_acmpeq( If_acmpeq inst )
    {
      visitTwoOpBranch( inst );
    }

  /**
   * Visits the <i>if_acmpne</i> instruction.
   * Pop out the top two operand and also add useing these two operand 
   * in the inst.
   * @param inst  <i>if_acmpne</i> instruction.
   */
  public void visitIf_acmpne( If_acmpne inst )
    {
      visitTwoOpBranch( inst );
    }

  /**
   * Visits the <i>goto</i> instruction.
   * @param inst  <i>goto</i> instruction.
   */
  public void visitGoto( Goto inst ){}

  /**
   * Visits the <i>jsr</i> instruction.
   * @param inst  <i>jsr</i> instruction.
   */
  public void visitJsr( Jsr inst ){}

  /**
   * Visits the <i>ret</i> instruction.
   * @param inst  <i>ret</i> instruction.
   */
  public void visitRet( Ret inst ){}

  /**
   * Visits the <i>tableswitch</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  <i>tableswitch</i> instruction.
   */
  public void visitTableswitch( Tableswitch inst )
    {
      visitSwitch( inst );
    }

  /**
   * Visits the <i>lookupswitch</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  <i>lookupswitch</i> instruction.
   */
  public void visitLookupswitch( Lookupswitch inst )
    {
      visitSwitch( inst );
    }

  /**
   * Visits the <i>ireturn</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  <i>ireturn</i> instruction.
   */
  public void visitIreturn( Ireturn inst )
    {
      visitValuedReturn( inst );
    }

  /**
   * Visits the <i>lreturn</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  <i>lreturn</i> instruction.
   */
  public void visitLreturn( Lreturn inst )
    {
      visitValuedReturn( inst );
    }

  /**
   * Visits the <i>freturn</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  <i>freturn</i> instruction.
   */
  public void visitFreturn( Freturn inst )
    {
      visitValuedReturn( inst );
    }

  /**
   * Visits the <i>dreturn</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * Note: Widening date type (long. double) only assume one operand, not 
   * two operands, as most of the stack simulation do.
   * @param inst  <i>dreturn</i> instruction.
   */
  public void visitDreturn( Dreturn inst )
    {
      visitValuedReturn( inst );
    }

  /**
   * Visits the <i>areturn</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  <i>areturn</i> instruction.
   */
  public void visitAreturn( Areturn inst )
    {
      visitValuedReturn( inst );
    }

  /**
   * Visits the <i>return</i> instruction.
   * @param inst  <i>return</i> instruction.
   */
  public void visitReturn( Return inst ){}

  /**
   * Visits the <i>getstatic</i> instruction.
   * @param inst  <i>getstatic</i> instruction.
   */
  public void visitGetstatic( Getstatic inst )
    {
      Vector operands = inst.getOperandsVector();
      if ( operands.size () != 1 )
	{
	    throw new RuntimeException("Error: " +
		      "DUGatheringVisitor.visitGetstatic: must have " +
		      "one operand");
	}
	Object o = operands.get (0);
      if ( !(o instanceof Field) )
	{
	    throw new RuntimeException("Error: " +
				       "DUGatheringVisitor.visitGetstatic: operand not "
				       + "of Field type");
	}

      LeafDefUse element;
      if ( ((Field)o).getType() instanceof Primitive )
	  element = new PrimitiveDefUseImpl ((FieldImpl)o);
      else
	  element = new ReferenceDefUseImpl ((FieldImpl)o);

      StaticElementDefUse se = new StaticElementDefUseImpl ((jaba.sym.NamedReferenceTypeImpl) ((Field)o).getContainingType(), element);
  
      Vector seVec = new Vector();
      seVec.add( se );
      opStack.push( seVec );
    }

  /**
   * Visits the <i>putstatic</i> instruction.
   * @param inst  <i>putstatic</i> instruction.
   */
  public void visitPutstatic( Putstatic inst )
    {
      Vector value = opStack.pop();
      inst.addUse(value);

      Vector operands = inst.getOperandsVector();
      if ( operands.size () != 1 )
	  {
	    throw new RuntimeException( "Error: " +
					"DUGatheringVisitor.visitPutstatic: must have " +
					"one operand");
	}
	Object o = operands.get (0);
      if ( !(o instanceof Field) )
	{
	    throw new RuntimeException("Error: " +
				       "DUGatheringVisitor.visitPutstatic: operand not "
				       + "of Field type");
	}
     
      LeafDefUse element;
      if ( ((Field)o).getType() instanceof Primitive )
	element = new PrimitiveDefUseImpl ((FieldImpl)o);
      else
	element = new ReferenceDefUseImpl ((FieldImpl)o);
      
      StaticElementDefUse se = new StaticElementDefUseImpl ((jaba.sym.NamedReferenceTypeImpl) ((Field)o).getContainingType(), element );
      checkDef( se );
      inst.setDefinition( se );
    }

  /**
   * Visits the <i>getfield</i> instruction.
   * Pop up the top operand in the stack, evolve the reference
   * to ReferenceElement, push back the ReferenceElement to the stack
   * also with others that were in the oerand, to keep track of what has 
   * cotributed (help defined) this element.
   * Note: A widening/up casting will be performed for this case:
   * Class A {Vector c; ...}. Class B extends A {...}; B b = new B();
   * DefUse for expression b.c will be CRE{o=b, t=A, e=R=c} 
   * @param inst  <i>getfield</i> instruction.
   */
  public void visitGetfield( Getfield inst )
    {
      Vector mergedItem = new Vector();
      Vector operands = inst.getOperandsVector();
      if ( operands.size () != 1 || !(operands.get (0) instanceof Field) ) {
	  throw new RuntimeException( "getfield: must have one operand and the operand should be of Field type");
      }
    
      FieldImpl field = (FieldImpl)operands.get (0);
      Vector objectRefs = opStack.pop();
      DUVectorHelper refOperand = new DUVectorHelper(objectRefs);
      mergedItem.addAll(refOperand.getNonRefElements());
      
      DefUse objectRef = refOperand.getReferenceElement();
      assert objectRef != null:inst;
      ReferenceDefUse objectLeaf = DUVectorHelper.getReferenceDefUse(objectRef);

      // Create the RefElement for the field
      LeafDefUse element;
      if ( field.getType() instanceof Primitive )
	  element = new PrimitiveDefUseImpl (field);
      else
	  element = new ReferenceDefUseImpl (field);
      
      ReferenceElementDefUse re = null;
      if ( objectLeaf instanceof CastedReferenceDefUse ) {
	  re = new CastedReferenceElementDefUseImpl ((ReferenceType)objectLeaf.getType(), objectLeaf.getVariable(), element);
      }
      else {
	  re = new ReferenceElementDefUseImpl (objectLeaf.getVariable(), element);
      }
  
      // Evolve the Ref to Ref element
      if ( objectRef instanceof RecursiveDefUse ){
	  RecursiveDefUse lastRec = ((RecursiveDefUse)objectRef).
	      getLastRecursiveElement();
	  lastRec.setElement( re );
      }
      else {
	  objectRef = re;
      }

      mergedItem.add( objectRef );
      
      opStack.push( mergedItem );
    }

  /**
   * Visits the <i>putfield</i> instruction.
   * @param inst  <i>putfield</i> instruction.
   */
  public void visitPutfield( Putfield inst )
    {
      Vector stack1 = opStack.pop();
      inst.addUse(stack1);
    
      Vector operands = inst.getOperandsVector();
      if ( operands.size () != 1 ||  !(operands.get (0) instanceof Field) ) {
	  throw new RuntimeException("putfield instruction should have one operand and its type should be Field");
      }

      FieldImpl field = (FieldImpl)operands.get (0);
      Vector objectRefs = opStack.pop();
      DUVectorHelper objectOperand = new DUVectorHelper(objectRefs);
      inst.addUse(objectOperand.getNonRefElements());

      DefUse objectRef = objectOperand.getReferenceElement();
      ReferenceDefUse objectLeaf = DUVectorHelper.getReferenceDefUse(objectRef);

      // Create ReferenceElement for the field
      LeafDefUse element;
      if ( field.getType() instanceof Primitive )
	  element = new PrimitiveDefUseImpl (field);
      else
	  element = new ReferenceDefUseImpl (field);
      
      ReferenceElementDefUse re;
      if ( objectLeaf instanceof CastedReferenceDefUse ) {
	  re = new CastedReferenceElementDefUseImpl ((ReferenceType)objectLeaf.getType(),  objectLeaf.getVariable(), element);
      }
      else {
	  re = new ReferenceElementDefUseImpl (objectLeaf.getVariable(), element);
      }

      // Evolve the Reference to ReferenceElement
      if ( objectRef instanceof RecursiveDefUse ) {
	  RecursiveDefUse lastRec = ((RecursiveDefUse)objectRef).
	      getLastRecursiveElement();
	  lastRec.setElement( re );
      }
      else {
	  objectRef = re;
      }

      checkDef( objectRef);
      inst.setDefinition( objectRef );
    }
    

  /**
   * Visits the <i>invokevirtual</i> instruction. This method will simulate 
   * the stack change: pop up the parameters and object reference, then
   * push the return value in the stack if there is one.It will also collect
   * the callable.calling method information. 
   * @param inst  <i>invokevirtual</i> instruction.
   */
  public void visitInvokevirtual( Invokevirtual inst )
    {
      visitInvokeCall( inst );
    }

  /**
   * Visits the <i>invokespecial</i> instruction.
   * @param inst  <i>invokespecial</i> instruction.
   */
  public void visitInvokespecial( Invokespecial inst )
    {
      visitInvokeCall( inst );
    }

  /**
   * Visits the <i>invokestatic</i> instruction.
   * @param inst  <i>invokestatic</i> instruction.
   */
  public void visitInvokestatic( Invokestatic inst )
    {
      visitInvokeCall( inst );
    }

  /**
   * Visits the <i>invokeinterface</i> instruction.
   * @param inst  <i>invokeinterface</i> instruction.
   */
  public void visitInvokeinterface( Invokeinterface inst )
    {
      visitInvokeCall( inst );
    }

  /**
   * Visits the <i>new</i> instruction. Create an ReferenceDefUse for the 
   * new Object, push it in the stack, and also note the instruction define it.
   * @param inst  <i>new</i> instruction.
   */
  public void visitNew( New inst )
    {
      NewInstance heap = new NewInstanceImpl ();
      Vector operands = inst.getOperandsVector();
      boolean typeSet = false;
      assert operands.size () == 1 : inst + " " + operands.size ();
      for (Iterator i = operands.iterator (); i.hasNext ();)
	{
		Object o = i.next ();
	  if (o instanceof Class )
	    {
	      heap.setType ((Class) o);
	      typeSet = true;
	      break;
	    }
	}
      if ( !typeSet ) {
	  throw new RuntimeException("instruction new expects an operand with Class type");
      }

      ReferenceDefUse refDU = new ReferenceDefUseImpl (heap );
      Vector refVec = new Vector();
      refVec.add( refDU );
      opStack.push( refVec );

      // create another copy (with the same reference to the heap location,
      // and report a definition of it) We cannot use the one we put in the 
      // stack. Because the one in stack will be change and evolve as more
      // and more inst processed
      refDU = new ReferenceDefUseImpl (heap);
      inst.setDefinition( refDU );
    }

  /**
   * Visits the <i>newarray</i> instruction. Create an ReferenceDefUse to
   * represent the primitive Array. push it in the stack, and record define it.
   * @param inst  <i>newarray</i> instruction.
   */
  public void visitNewarray( Newarray inst ) {
      NewInstance heap = new NewInstanceImpl ();
      Vector operands = inst.getOperandsVector();
      boolean typeSet = false;
      for (Iterator i = operands.iterator (); i.hasNext ();)
	{
		Object o = i.next ();
	  if (o instanceof Array )
	    {
	      heap.setType ((Array) o);
	      typeSet = true;
	      break;
	    }
	}

      if ( !typeSet ){
	    throw new RuntimeException("Instruction newArray should have one operand with type Array");
      }

      ReferenceDefUse refDU = new ReferenceDefUseImpl (heap);
      Vector refVec = opStack.pop();
      refVec.add( refDU );
      opStack.push( refVec );

      // create another copy (with the same reference to the heap location,
      // and report a definition of it. As the one in stack may be changed 
      // later
      refDU = new ReferenceDefUseImpl (heap);
      inst.setDefinition( refDU );
    }

  /**
   * Visits the <i>anewarray</i> instruction. Create a ReferenceDefUse
   * to represent the array. push it in the stack, and record define it.
   * @param inst  <i>anewarray</i> instruction.
   */
  public void visitAnewarray( Anewarray inst ){
      NewInstance heap = new NewInstanceImpl ();
      Vector operands = inst.getOperandsVector();
      boolean typeSet = false;
      for (Iterator i = operands.iterator (); i.hasNext ();)
	{
		Object o = i.next ();
	  if (o instanceof Array ){
	      heap.setType ((Array) o);
	      typeSet = true;
	      break;
	    }
      }
      
      if ( !typeSet ) {
	  throw new RuntimeException("anewArray should have an Array type operand");
      }
      
      ReferenceDefUse refDU = new ReferenceDefUseImpl (heap);
      Vector refVec = opStack.pop();
      refVec.add( refDU );
      opStack.push( refVec );     
      
      // create another copy (with the same reference to the heap location,
      // and report a definition of it
      refDU = new ReferenceDefUseImpl (heap);
      inst.setDefinition( refDU );
    }

  /**
   * Visits the <i>arraylength</i> instruction.
   * Pop up the top operand in the stack. Find the array reference. Evolve
   * it to an ReferenceElementDefUse, whose Object is this array, but its
   * leaf is a Primitive (Its HasValue is ArrayLength). Push back the 
   * ReferenceElementDefUse along with other element in the operand
   * @param inst  <i>arraylength</i> instruction.
   */
  public void visitArraylength( Arraylength inst ){
      // Pop up the top vector from the stack, should contain the 
      // array reference
      Vector operand = opStack.pop();
      DUVectorHelper arrayOperand = new DUVectorHelper(operand);

      // The result vector that will be push back into the stack.\
      // let's first put all non-ref in it, as they won't be changed 
      // by this inst
      Vector mergedItem = new Vector(operand.size());
      mergedItem.addAll(arrayOperand.getNonRefElements());

      DefUse ref = arrayOperand.getReferenceElement();
      assert ref != null;
      
      // Find the ReferenceDefUse for the array, and gather useful info
      ReferenceDefUse arrayLeaf = DUVectorHelper.getReferenceDefUse(ref);
      HasValue array = arrayLeaf.getVariable();
      Array arrayType;
      if(arrayLeaf instanceof CastedReferenceDefUse) {
	  arrayType = (Array)arrayLeaf.getType();
      }else {
	  arrayType = (Array)array.getType();
      }

      // Create the ReferenceElementDefUse to represent the array.length
      ArrayLength length = new ArrayLength(array);
      PrimitiveDefUse duLength = new PrimitiveDefUseImpl (length);
      ReferenceElementDefUse refElement;
      if(arrayLeaf instanceof CastedReferenceDefUse) {
	  refElement = new CastedReferenceElementDefUseImpl (arrayType, array, duLength);
      }else {
	  refElement = new ReferenceElementDefUseImpl (array, duLength);
      }

      // replace the ReferenceDefUse of array to ReferenceElementDefUse
      if(ref instanceof RecursiveDefUse) {
	  RecursiveDefUse lastRec = ((RecursiveDefUse)ref).
	      getLastRecursiveElement();
	  lastRec.setElement( refElement );
      }else {
	  ref = refElement;
      }

      // Now we want to push this refElement(whose leaf is interger Primitve 
      // back to the stack, along with other element in this operand vector
      mergedItem.add(ref);
      opStack.push(mergedItem);
  }

  /**
   * Visits the <i>athrow</i> instruction.
   * @param inst  <i>athrow</i> instruction.
   */
  public void visitAthrow( Athrow inst )
    {
       Vector excRefs = opStack.pop();
       inst.addUse(excRefs);
    }

  /**
   * Visits the <i>checkcast</i> instruction.Replace the ReferenceDefUse
   * to CastedeferenceDefUse.
   * @param inst  <i>checkcast</i> instruction.
   */
  public void visitCheckcast( Checkcast inst ) {
      // make sure there is 1 operand and it is a ReferenceType
      Vector operands = inst.getOperandsVector();
      if ( ( operands.size () != 1 ) || 
	   !( operands.get (0) instanceof ReferenceType ) ) {
	 throw new RuntimeException("Instruction expect only one operand, and it should be a ReferenceType");
      }

      // get the cast type
      ReferenceType castType = (ReferenceType)operands.get (0);

      // get the top element of the opstack, get the object reference
      Vector dus = opStack.peek();
      DUVectorHelper operand = new DUVectorHelper(dus);
      DefUse objRef = operand.getReferenceElement();
      assert objRef != null;
      
      // modify the DefUse tree. There are two cases:
      // 1)Object DU is recursive, evolve the leaf from ReferenceDefUse to 
      // CastRef. If the leaf is already Casted, then change the type.
      // 2) Object DU is LeafDefUse, then replace it with a Casted Ref. If
      // it's already casted, replacment can be simplified as changing
      // the type.
      if ( objRef instanceof RecursiveDefUse ) {
	  if ( ((RecursiveDefUse)objRef).getLeafElement() instanceof 
	       CastedReferenceDefUse ) {
	      CastedReferenceDefUse castDU = 
		  (CastedReferenceDefUse)((RecursiveDefUse)objRef).
		  getLeafElement();
	      castDU.setType( castType );
	  }
	  else {
	      RecursiveDefUse lastRecDU = 
		  ((RecursiveDefUse)objRef).getLastRecursiveElement();
	      ReferenceDefUse leafDU = 
		  (ReferenceDefUse)((RecursiveDefUse)objRef).
		  getLeafElement();
	      CastedReferenceDefUse castDU = 
		  new CastedReferenceDefUseImpl (castType, leafDU.getVariable());

	      lastRecDU.setElement( castDU );
	  }
      }
      else { // objRef instanceof ReferenceDefUse
	  if ( objRef instanceof CastedReferenceDefUse ) {
	      ((CastedReferenceDefUse)objRef).setType( castType );
	  } else {
	      CastedReferenceDefUse castDU = 
		  new CastedReferenceDefUseImpl (castType,
				     ((ReferenceDefUse)objRef).getVariable() );
	      dus.add( castDU);
	      dus.remove( objRef );
	  }
      }   
      
  }

  /**
   * Visits the <i>instanceof</i> instruction.
   * Create a TypeUse of the object to indicate the type has been used, not the
   * content. Change the top operand in the stack, using this TypeUse to replace
   * the DefUse that represent the variable.
   * @param inst  <i>instanceof</i> instruction.
   */
  public void visitInstanceof( Instanceof inst ) {
      // make sure there is 1 operand and it is a ReferenceType
      Vector operands = inst.getOperandsVector();
      if ( ( operands.size () != 1 ) || 
	   !( operands.get (0) instanceof ReferenceType ) ) {
	  throw new RuntimeException("unexpected opearnds found for instanceof instruction");
	}

      // ensure that there is one and only one reference type defuse object on 
      // top of stack
      Vector dus = (Vector)opStack.peek();
      DUVectorHelper duOperand = new  DUVectorHelper(dus);
      DefUse du = duOperand.getReferenceElement();
      assert du != null;
    
      // add a TypeUse at the top of the DU tree being evaluated to
      // indicate that the DU object's type is being used -- not its
      // value/contents
      TypeUse tu = new TypeUseImpl ((ReferenceType)operands.get (0), du );

      // Replace the original Du object with its typeuse
      dus.remove(du );
      dus.add( tu );
  }

  /**
   * Visits the <i>monitorenter</i> instruction. Pop up the top operand from
   * the stack. All the DefUse in this operand is the used variables of this
   * instruction.
   * @param inst  <i>monitorenter</i> instruction.
   */
    public void visitMonitorenter( Monitorenter inst ){
	inst.addUse( opStack.pop() );
    }

  /**
   * Visits the <i>monitorexit</i> instruction.Pop up the top operand from
   * the stack. All the DefUse in this operand is the used variables of this
   * instruction.
   * @param inst  <i>monitorexit</i> instruction.
   */
    public void visitMonitorexit( Monitorexit inst )
    {
	inst.addUse(opStack.pop());
    }

  /**
   * Visits the <i>wide</i> instruction.Class Wide actually contains two
   * instructions, wide, and the instruction that follows it (to be wided).
   * It simulates the effect by visiting the instruction that follows
   * "wide".
   * @param inst  <i>wide</i> instruction.
   */
  public void visitWide( Wide inst )
    {
      int opcode = inst.getEncapsulatedOpcode();
      Vector operands = null;
      Vector uses = null;
      switch ( opcode )
	{
	case OpCode.OP_ILOAD: //21: //iload
	  Iload iload = new Iload();
	  operands = inst.getOperandsVector();
	  for (Iterator i = operands.iterator (); i.hasNext ();)
	    iload.addOperand (i.next ());
	  visitIload( iload );
	  if ( iload.getDefinition() != null ) 
	    inst.setDefinition( iload.getDefinition() );
	  uses = iload.getUsesVector ();
	  if ( uses != null )
	    {
		inst.addUse (uses);
	    }
	  break;
	case OpCode.OP_FLOAD: //23: //fload
	  Fload fload = new Fload();
	  operands = inst.getOperandsVector();
	  for (Iterator i = operands.iterator (); i.hasNext ();)
	    fload.addOperand (i.next ());
	  visitFload( fload );
	  if ( fload.getDefinition() != null ) 
	    inst.setDefinition( fload.getDefinition() );
	  uses = fload.getUsesVector ();
	  if ( uses != null )
	    {
		inst.addUse (uses);
	    }
	  break;
	case OpCode.OP_ALOAD: //25: //aload
	  Aload aload = new Aload();
	  operands = inst.getOperandsVector();
	  for (Iterator i = operands.iterator (); i.hasNext ();)
	    aload.addOperand (i.next ());
	  visitAload( aload );
	  if ( aload.getDefinition() != null ) 
	    inst.setDefinition( aload.getDefinition() );
	  uses = aload.getUsesVector ();
	  if ( uses != null )
	    {
		inst.addUse (uses);
	    }
	  break;
	case OpCode.OP_LLOAD: //22: //lload
	  Lload lload = new Lload();
	  operands = inst.getOperandsVector();
	  for (Iterator i = operands.iterator (); i.hasNext ();)
	    lload.addOperand (i.next ());
	  visitLload( lload );
	  if ( lload.getDefinition() != null ) 
	    inst.setDefinition( lload.getDefinition() );
	  uses = lload.getUsesVector ();
	  if ( uses != null )
	    {
		inst.addUse (uses);
	    }
	  break;
	case OpCode.OP_DLOAD: //24: //dload
	  Dload dload = new Dload();
	  operands = inst.getOperandsVector();
	  for (Iterator i = operands.iterator (); i.hasNext ();)
	    dload.addOperand (i.next ());
	  visitDload( dload );
	  if ( dload.getDefinition() != null ) 
	    inst.setDefinition( dload.getDefinition() );
	  uses = dload.getUsesVector ();
	  if ( uses != null ) {
	      inst.addUse (uses);
	  }
	  break;
	case OpCode.OP_ISTORE: //54: //istore
	  Istore istore = new Istore();
	  operands = inst.getOperandsVector();
	  for (Iterator i = operands.iterator (); i.hasNext ();)
	    istore.addOperand (i.next ());
	  visitIstore( istore );
	  if ( istore.getDefinition() != null ) 
	    inst.setDefinition( istore.getDefinition() );
	  uses = istore.getUsesVector ();
	  if ( uses != null )
	    {
		inst.addUse (uses);
	    }
	  break;
	case OpCode.OP_FSTORE: //56: //fstore
	  Fstore fstore = new Fstore();
	  operands = inst.getOperandsVector();
	  for (Iterator i = operands.iterator (); i.hasNext ();)
	    fstore.addOperand (i.next ());
	  visitFstore( fstore );
	  if ( fstore.getDefinition() != null ) 
	    inst.setDefinition( fstore.getDefinition() );
	  uses = fstore.getUsesVector ();
	  if ( uses != null )
	    {
		inst.addUse (uses);
	    }
	  break;
	case OpCode.OP_ASTORE: //58: //astore
	  Astore astore = new Astore();
	  operands = inst.getOperandsVector();
	  for (Iterator i = operands.iterator (); i.hasNext ();)
	    astore.addOperand (i.next ());
	  visitAstore( astore );
	  if ( astore.getDefinition() != null ) 
	    inst.setDefinition( astore.getDefinition() );
	  uses = astore.getUsesVector ();
	  if ( uses != null ){
		inst.addUse (uses);
	    }
	  break;
	case OpCode.OP_LSTORE: //55: //lstore
	  Lstore lstore = new Lstore();
	  operands = inst.getOperandsVector();
	  for (Iterator i = operands.iterator (); i.hasNext ();)
	    lstore.addOperand (i.next ());
	  visitLstore( lstore );
	  if ( lstore.getDefinition() != null ) 
	    inst.setDefinition( lstore.getDefinition() );
	  uses = lstore.getUsesVector ();
	  if ( uses != null )
	    {
		inst.addUse (uses);
	    }
	  break;
	case OpCode.OP_DSTORE: //57: //dstore
	  Dstore dstore = new Dstore();
	  operands = inst.getOperandsVector();
	  for (Iterator i = operands.iterator (); i.hasNext ();)
	    dstore.addOperand(i.next ());
	  visitDstore( dstore );
	  if ( dstore.getDefinition() != null ) 
	    inst.setDefinition( dstore.getDefinition() );
	  uses = dstore.getUsesVector ();
	  if ( uses != null )
	    {
		inst.addUse (uses);
	    }
	  break;
	case OpCode.OP_RET: //169: //ret
	  Ret ret = new Ret();
	  operands = inst.getOperandsVector();
	  for (Iterator i = operands.iterator (); i.hasNext ();)
	    ret.addOperand (i.next ());
	  visitRet( ret );
	  if ( ret.getDefinition() != null ) 
	    inst.setDefinition( ret.getDefinition() );
	  uses = ret.getUsesVector ();
	  if ( uses != null )
	    {
		inst.addUse (uses);
	    }
	  break;
	case OpCode.OP_IINC: //132: //iinc
	  Iinc iinc = new Iinc();
	  operands = inst.getOperandsVector();
	  for (Iterator i = operands.iterator (); i.hasNext ();)
	    iinc.addOperand (i.next ());
	  visitIinc( iinc );
	  if ( iinc.getDefinition() != null ) 
	    inst.setDefinition( iinc.getDefinition() );
	  uses = iinc.getUsesVector ();
	  if ( uses != null )
	    {
		inst.addUse (uses);
	    }
	  break;
	}
    }

  /**
   * Visits the <i>multianewarray</i> instruction. Pop the top n elements of
   * the stack. n is the dimension of the array. Create an ReferenceDefUse
   * to represent the array. push it in the stack, and record define it.
   * @param inst  <i>multianewarray</i> instruction.
   */
    public void visitMultianewarray( Multianewarray inst ) {
	Vector operands = inst.getOperandsVector();
	if ( operands.size () != 2 ) {
	    RuntimeException e = new RuntimeException();
	    e.printStackTrace();
	    throw e;
	}
      // loop through operands of the instruction to determine the number of
      // dimension of the new array and the type of the array being created.
      int numDimensions = 0;
      TypeEntry arrayType = null;
      for (Iterator i = operands.iterator (); i.hasNext ();)
	{
		Object o = i.next ();
	  if (o instanceof Integer )
	    numDimensions = ((Integer) o).intValue();
	  else if (o instanceof Array )
	    arrayType = (Array) o;
	}

      if ( numDimensions == 0 ){
	  throw new RuntimeException("Try to create 0 dimension array");
      }
      if ( arrayType == null ) {
	  throw new RuntimeException("Array type not specfied");
      }

      // Merge the top numDimension element as one lement and put the just
      // created ReferenceDefUse of this array in it
      opStack.merge( numDimensions );
      Vector topElement = opStack.pop();
      NewInstance heap = new NewInstanceImpl ();
      heap.setType( arrayType );

      ReferenceDefUse refDU = new ReferenceDefUseImpl (heap );
      topElement.add( refDU );
      opStack.push( topElement );

      // create another copy (with the same reference to the heap location,
      // and report a definition of it
      refDU = new ReferenceDefUseImpl (heap );
      inst.setDefinition( refDU );
    }

  /**
   * Visits the <i>ifnull</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  <i>ifnull</i> instruction.
   */
  public void visitIfnull( Ifnull inst )
    {
      visitOneOpBranch( inst );
    }

  /**
   * Visits the <i>ifnonnull</i> instruction.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  <i>ifnonnull</i> instruction.
   */
  public void visitIfnonnull( Ifnonnull inst )
    {
      visitOneOpBranch( inst );
    }

  /**
   * Visits the <i>goto_w</i> instruction.
   * @param inst  <i>goto_w</i> instruction.
   */
  public void visitGoto_w( Goto_w inst ){}

  /**
   * Visits the <i>jsr_w</i> instruction.
   * @param inst  <i>jsr_w</i> instruction.
   */
  public void visitJsr_w( Jsr_w inst ){}

  /**
   * Visits all instructions that push a constant onto the operand stack.
   * Create an ConstantValue to represent the operand, wrap it in 
   * PrimitiveDefUse, then push it in the stack.
   * @param inst  The symbolic instruction.
   */
  private void visitConstantPush( SymbolicInstruction inst )
    {
      int constVal = 0;
      boolean valid = false;
      Vector operands = inst.getOperandsVector();
     
      if ( ( operands.size () >= 1 ) && ( operands.get (0) instanceof Integer ) )
	{
	  constVal = ((Integer)(operands.get (0))).intValue();
	  valid = true;
	}
      else if ( inst instanceof Iconst_m1 )
	{
	  constVal = -1;
	  valid = true;
	}
      else if ( ( inst instanceof Iconst_0 ) ||
		( inst instanceof Lconst_0 ) )
	{
	  constVal = 0;
	  valid = true;
	}
      else if ( ( inst instanceof Iconst_1 ) ||
		( inst instanceof Lconst_1 ) )
	{
	  constVal = 1;
	  valid = true;
	}
      else if ( inst instanceof Iconst_2 )
	{
	  constVal = 2;
	  valid = true;
	}
      else if ( inst instanceof Iconst_3 ) 
	{
	  constVal = 3;
	  valid = true;
	}
      else if ( inst instanceof Iconst_4 )
	{
	  constVal = 4;
	  valid = true;
	}
      else if ( inst instanceof Iconst_5 )
	{
	  constVal = 5;
	  valid = true;
	}

      ConstantValue constant = new ConstantValueImpl ();
      if ( ( inst instanceof Iconst_m1 ) || ( inst instanceof Iconst_0 ) ||
	   ( inst instanceof Iconst_1 ) || ( inst instanceof Iconst_2 ) ||
	   ( inst instanceof Iconst_3 ) || ( inst instanceof Iconst_4 ) ||
	   ( inst instanceof Iconst_5 ) ) {
	  constant.setPrimitive( PrimitiveImpl.valueOf(Primitive.INT ) );
      }
      else if ( ( inst instanceof Lconst_0 ) || ( inst instanceof Lconst_1 ) ){
	  constant.setPrimitive( PrimitiveImpl.valueOf( Primitive.LONG ) );
      }
      else if ( ( inst instanceof Fconst_0 ) || ( inst instanceof Fconst_1 ) ||
		( inst instanceof Fconst_2 ) ){
	  constant.setPrimitive( PrimitiveImpl.valueOf( Primitive.FLOAT ) );
      }
      else if ( ( inst instanceof Dconst_0 ) || ( inst instanceof Dconst_1 ) ){
 	  constant.setPrimitive( PrimitiveImpl.valueOf( Primitive.DOUBLE ) );
      }
      else if ( inst instanceof Bipush ) {
	  constant.setPrimitive( PrimitiveImpl.valueOf( Primitive.BYTE ) );
      }
      else if ( inst instanceof Sipush ) {
	  constant.setPrimitive( PrimitiveImpl.valueOf( Primitive.SHORT ) );
      }
      else { //if ( ( inst instanceof Ldc ) || ( inst instanceof Ldc_w ) ||
	  //	( inst instanceof Ldc2_w ) ) {
	  constant.setPrimitive(PrimitiveImpl.forJavaType(operands.get (0)));
      }

      if ( ( inst instanceof Ldc2_w ) || ( inst instanceof Lconst_0 ) ||
	   ( inst instanceof Lconst_1 ) || ( inst instanceof Dconst_0 ) ||
	   ( inst instanceof Dconst_1 ) )
	constant.setIs64Bit( true );

      constant.setValue( constVal );
      constant.setIsValidValue( valid );
     
      PrimitiveDefUse primDU = new PrimitiveDefUseImpl (constant );
      Vector item = new Vector();
      item.add( primDU );
      opStack.push( item );

    }

  /**
   * Visit instructions that load a primitive onto the stack.
   * @param inst  The symbolic instruction loading the primitive.
   */
  private void visitPrimitiveLoad( SymbolicInstruction inst )
    {
      Vector operands = inst.getOperandsVector();
      if ( operands.size () == 0 )
	{
	    throw new RuntimeException("found no operands on the instruction");
	}
      LocalVariableImpl localVar = null;
	Object o = operands.get (0);
      if ( ( operands.size () >= 1 ) && 
	   ( o instanceof LocalVariable ) )
	localVar = (LocalVariableImpl)(o);
      else if ( ( operands.size () == 1 ) && 
		( o instanceof Integer ) )
	{
	  Vector synthDUVec = 
	    (Vector)syntheticLocalVariableMap.get (o);
	  Vector clonedVec = vectorClone(synthDUVec);
	  opStack.push( clonedVec );
	  return;
	}
      else
	{
	    RuntimeException e = new RuntimeException();
	    e.printStackTrace();
	    throw e;
	}
      PrimitiveDefUse primDU = new PrimitiveDefUseImpl (localVar );
      Vector item = new Vector();
      item.add( primDU );
      opStack.push( item );

    }

  /**
   * Visit instructions that load a reference onto the stack. Create the
   * ReferenceDefUse for the reference and push it on the stack
   * @param inst  The symbolic instruction loading the reference.
   */
  private void visitReferenceLoad( SymbolicInstruction inst )
    {
      Vector operands = inst.getOperandsVector();
      if ( operands.size () == 0 ) {
	  throw new RuntimeException(inst + " should have at least one operand");
      }

	Object o = operands.get (0);
      LocalVariableImpl localVar = null;
      if ( ( operands.size () >= 1 ) &&
	   ( o instanceof LocalVariable ) )
	localVar = (LocalVariableImpl)(o);
      else if ( ( operands.size () == 1 ) && 
		( o instanceof Integer ) )
	{
	  Vector synthDUVec = 
	    (Vector)syntheticLocalVariableMap.get (o);
	  Vector clonedVec = vectorClone(synthDUVec);
	  opStack.push( clonedVec );
	  return;
	}
      else{
	  throw new RuntimeException(inst + " Unrecognizable operand: " + o);
      }

      ReferenceDefUse refDU = new ReferenceDefUseImpl (localVar );
      Vector item = new Vector();
      item.add( refDU );
      opStack.push( item );
    }

  /**
   * Visit instructions that load an array element onto the stack.
   * Pop up the top 2 operand,(the index and the array reference)
   * They will be merged and push back in the stack.
   * The array reference defuse object will be updated to 
   * arrayelementdefuse object. It will try to calculate the index 
   * if the index is constant. The const index defuse object may not 
   * be in the stack any more. As constant usage is not kept tracking.
   * @param inst  The symbolic instruction loading the array element.
   */
   private void visitArrayElementLoad( SymbolicInstruction inst ){
      // Result vector, will be pushed in the stack
      Vector mergedItem = new Vector();

      // Pop up the index operand, put all the nonconstant to the 
      // result vector, calculate the index, if it's constant
      Vector indexes = opStack.pop();
      DUVectorHelper indexOperand = new DUVectorHelper(indexes);
      mergedItem.addAll(indexOperand.getNonConstElements());
      PrimitiveDefUse constIndex = indexOperand.getConstantElement();
      // Calculate the constant index
      int indexValue = -1;
      if ( constIndex != null ) {
	   indexValue = ((ConstantValue)constIndex.getVariable()).getValue();
      }

      // Pop up the array reference Operand, put all the non-reference
      // element to the result vector. Evolve the array reference defuse to
      // an array element defuse object
      Vector arrayTrees = opStack.pop();
      DUVectorHelper arrayOperand = new DUVectorHelper(arrayTrees);
      mergedItem.addAll(arrayOperand.getNonRefElements());

      // Deal with the array reference, it can be in a ReferenceDefUse or
      // RecursiveDefUse object whose leaf is a ReferenceDefUse. The code below
      // will evolve this array reference defuse object to an arrayElementDefUse
      // object. If the array defuse object is a casted object, than the 
      // arrayElementDefUse will be casted too.

      // Find the DefUse than contains the array reference E.g a[i], find a
      DefUse arrayTree = arrayOperand.getReferenceElement();
      assert arrayTree != null: "array reference not found on stack";
      
      // Find the array reference itself
      ReferenceDefUse arrayRef;
      if(arrayTree instanceof ReferenceDefUse) {
 	    arrayRef = (ReferenceDefUse)arrayTree;
      }else { //RecursiveDefUse with ReferenceDefUse element
 	    arrayRef = (ReferenceDefUse)((RecursiveDefUse)arrayTree).getLeafElement();
      }
      
      // Get the array variable(i.e: get a of a[i][j]), the array type
      // and the type of the element
      HasValue array = arrayRef.getVariable();
      Array arrayType;
      if(arrayRef instanceof CastedReferenceDefUse) {
	  arrayType = (Array)arrayRef.getType();
      }else { //ReferenceDefUse
	  arrayType = (Array)array.getType();
      }
      TypeEntry arrayElementType = arrayType.getElementType();

      // need to represent the array element: a[i], whose type will be the 
      // type of the array, Primitive (e.g: int, short) or reference 
      // (e.g: Object, String).
      // Since there is no name for the memory location of a[i], we use Class
      // PlaceholderLocation to represent it.
      PlaceholderLocation loc = new PlaceholderLocationImpl ();
      loc.setType(arrayElementType);
      
      // Create the element object of the ArrayElementDefUse object. 
      // This element object can be primitiveDefUse, or referenceDefUse, 
      // decide by the array type.  It's a wraper of a[i]
      LeafDefUse element = null;
      if ( inst instanceof Aaload ) { // The array is of object type
	  element = new ReferenceDefUseImpl (loc);
      }
      else { //The array element is of primitive(int, short) type
	  element = new PrimitiveDefUseImpl (loc);
      }

      // Instantiate arrayElemDU
      ArrayElementDefUse arrayElemDU = null;
      if ( arrayRef instanceof CastedReferenceDefUse ) {
	  arrayElemDU = 
	      new CastedArrayElementDefUseImpl (arrayType, array, 
					   element, indexValue);
      } else {
	  arrayElemDU = new ArrayElementDefUseImpl (array, element, indexValue);
      }

      // Updating the array reference to arrayelement defuse
      if ( arrayTree instanceof ReferenceDefUse ) {
	  arrayTree = arrayElemDU;
      }else {
	  RecursiveDefUse lastRec = ((RecursiveDefUse)arrayTree).
	      getLastRecursiveElement();
	  lastRec.setElement( arrayElemDU );
      }
      mergedItem.add( arrayTree );
      
      opStack.push( mergedItem );
      
   }

  /**
   * Visit instructions that store a primitive from the stack.
   * Pop up the top operand (the value), record using it. Then 
   * record define the local variable, which is specified as this
   * instructin's operand
   * @param inst  The instruction that is storing the primitive.
   */
  private void visitPrimitiveStore( SymbolicInstruction inst )
    {
      Vector operands = inst.getOperandsVector();
      assert operands.size () != 0 : inst;
      assert operands.size () ==1 : inst;

      Vector uses = (Vector)opStack.pop();
      // if the only operand on the instruction is an Integer, then this 
      // instruction is storing to a synthetic local variable table position 
      // (there is no corresponding local variable in the source for the 
      // position) -- we need to store this DefUse in our synthetic local
      // variable map so that we can retrieve it with a synthetic reference
      // load, and do not report any definition or uses
	Object o = operands.get (0);
      if ( ( operands.size () == 1 ) && 
	   ( o instanceof Integer ) )
	{
	  Vector synthDUVec = vectorClone(uses); 
	  syntheticLocalVariableMap.put( o, synthDUVec );
	  return;
	}

      inst.addUse(uses );

      LocalVariableImpl localVar = null;
      if ( ( operands.size () >= 1 ) &&
	   ( o instanceof LocalVariable ) )
	localVar = (LocalVariableImpl)(o);
      assert localVar != null : inst;
      PrimitiveDefUse primDU = new PrimitiveDefUseImpl ( localVar );
      checkDef( primDU );
      inst.setDefinition( primDU );
    }
  
  /**
   * Visit instructions that store a reference from the stack.
   * Pop up the top operand (the value), record using it. Then 
   * record define the local variable, which is specified as this
   * instructin's operand
   * @param inst  The instruction that is storing the reference.
   */
  private void visitReferenceStore( SymbolicInstruction inst )
    {
      Vector operands = inst.getOperandsVector();
      assert operands.size () != 0 : inst;
      assert operands.size () == 1 : inst;

      Vector uses = opStack.pop();
	Object o = operands.get (0);
      // if the only operand on the instruction is an Integer, then this 
      // instruction is storing to a synthetic local variable table position 
      // (there is no corresponding local variable in the source for the 
      // position) -- we need to store this DefUse in our synthetic local
      // variable map so that we can retrieve it with a synthetic reference
      // load, and do not report any definition or uses
      if ( ( operands.size () == 1 ) && 
	   ( o instanceof Integer ) )
	{
	  Vector synthDUVec = vectorClone(uses);
	  syntheticLocalVariableMap.put (o, synthDUVec);
	  return;
	}

      inst.addUse( uses );

      // REVISIT: The below checking should be unnecessary
      LocalVariableImpl localVar = null;
      if ( ( operands.size () >= 1 ) &&
	   ( o instanceof LocalVariable ) )
	  localVar = (LocalVariableImpl)(o);
      assert localVar != null : localVar;
      ReferenceDefUse refDU = new ReferenceDefUseImpl (localVar);
      checkDef(refDU);
      inst.setDefinition( refDU );
    }

  /**
   * Visit instructions that store an array element from the stack. 
   * The top 3 operands in the stack should be the value, the index, and
   * the array reference. Value, index, and all other element in the
   * array reference will be recorded as the used variable of this
   * instruction. The array reference will evolve to an arrayElementDefUse
   * object, and recorded as definition of this instruction.
   * @param inst  The instruction that is storing the array element.
   */
  private void visitArrayElementStore( SymbolicInstruction inst ) {
      // Pop up the value operand, and set using it
      Vector values = opStack.pop();
      inst.addUse(values);

      // Pop up the index Operand, set using it. Also get the constant
      // index if the index is constant
      Vector indexes = opStack.pop();
      inst.addUse(indexes);
      DUVectorHelper indexOperand = new DUVectorHelper(indexes);
      PrimitiveDefUse constIndex = indexOperand.getConstantElement();
      int indexValue = -1;
      if ( constIndex != null ) {
	  indexValue =  ((ConstantValue)constIndex.getVariable()).getValue();
      }
      // Pop up the array reference Operand. set Using all the non-reference
      // defuse object. Evolve the array refernce to an array element defuse
      // object, i.e: if the array reference is captured in a ReferenceDefuse,
      // then evolve it to an ArrayElementDefUse object, otherwise, it is
      // captured in a recursiveDefUse object, evolve the leaf, which should
      // be a ReferenceDefUse to an ArrayElementDefUse object
      Vector arrayRefs = opStack.pop();
      DUVectorHelper arrayOperand = new DUVectorHelper(arrayRefs);
      inst.addUse(arrayOperand.getNonRefElements());

      DefUse arrayRef = arrayOperand.getReferenceElement();
      assert arrayRef != null;
      ReferenceDefUse arrayRefVar;
      if(arrayRef  instanceof ReferenceDefUse) {
	  arrayRefVar = (ReferenceDefUse)arrayRef;
      }else {//Recursive DefUse whose leaf is ReferenceDefUse
	  arrayRefVar = (ReferenceDefUse)((RecursiveDefUse)arrayRef).getLeafElement();
      }

      // Get the array variable(i.e: get a of a[i][j]), the array type
      // and the type of the element
      HasValue array = arrayRefVar.getVariable();
      Array arrayType = null;
      if(arrayRefVar instanceof CastedReferenceDefUse) {
	  arrayType = (Array)arrayRefVar.getType();
      }else {
	  arrayType = (Array)array.getType();
      }
      TypeEntry arrayElementType = arrayType.getElementType(); 

      // Need to represent a[i]
      PlaceholderLocation loc = new PlaceholderLocationImpl ();
      loc.setType(arrayElementType);

      // Wrap a[i] in a DefUse object, as it's an ArrayElementDefUse's element part
      LeafDefUse element = null;
      if ( inst instanceof Aastore ) {
	  element = new ReferenceDefUseImpl (loc);
      } else {
	  element = new PrimitiveDefUseImpl (loc);
      }

      // Create the ArrayElement DefUse that will represent a and a[i].
      // It will replace the original Array's ReferenceDefUse
      ArrayElementDefUse arrayElem = null;
      if(arrayRefVar instanceof CastedReferenceDefUse ) {
	  arrayElem = new CastedArrayElementDefUseImpl (arrayType, array, element,
						   indexValue);
      } else {
	  arrayElem = new ArrayElementDefUseImpl (array,element, indexValue);
      }

      // Using the just created arrayElementDefuse object to replace the
      // array's ReferenceDefUse object
      if(arrayRef instanceof RecursiveDefUse) {
	  RecursiveDefUse lastRec = ((RecursiveDefUse)arrayRef).
	      getLastRecursiveElement();
	  lastRec.setElement( arrayElem );
      }  else {
	  arrayRef = arrayElem;
      }

      // This instruction defines this arrayelement
      checkDef(arrayRef);
      inst.setDefinition( arrayRef );
   }     
  
  /**
   * Visit instructions that perform an arithmetic instruction.
   * @param inst  The instruction that is performing the arithmetic operation.
   */
  private void visitArithmeticInstruction( SymbolicInstruction inst ) {
      Vector stack1 = opStack.pop();
      Vector stack2 = opStack.pop();
      Vector mergedItem = new Vector();

      //Get the constant element, and put the non constant in the merged item
      DUVectorHelper operand1 = new DUVectorHelper(stack1);
      PrimitiveDefUse constOp1 = operand1.getConstantElement();
      mergedItem.addAll(operand1.getNonConstElements());
      
      //Get the constant element, and put the non constant in the merged item
      DUVectorHelper operand2 = new DUVectorHelper(stack2);
      PrimitiveDefUse constOp2 = operand2.getConstantElement();
      mergedItem.addAll(operand2.getNonConstElements());

      //Calculate the constant
      if ( ( constOp1 != null ) && ( constOp2 != null ) )
	{
	  PrimitiveDefUse result = 
	    evaluateArithmeticInst( inst, constOp1, constOp2 );
	  if ( result != null )
	    mergedItem.add( result );
	  else
	    {
	      mergedItem.add( constOp1 );
	      mergedItem.add( constOp2 );
	    }
	}
      opStack.push( mergedItem );
      opStack.merge( 1 ); // this is done for safe purging (if all are const)
    }

  /**
   * Visit instructions that are comparing primitives.
   * Merge the top 2 operands in the stack.
   * @param inst  The instruction comparing the primitives.
   */
  private void visitPrimitiveCompare( SymbolicInstruction inst )
    {
      opStack.merge( 2 );
    }

    /**
     * Visit instructions that conditionally branch based on one operand.
     * Pop out the top operand and also add use this operand in the inst.
     * @param inst  The instruction conditionally branching.
     */
   private void visitOneOpBranch( SymbolicInstruction inst ) {
       inst.addUse(opStack.pop());
   }

  /**
   * Visit instructions that conditionally branch based on two operands.
   * Pop out the top two operand and also add useing these two operand 
   * in the inst.
   * @param inst  The instruction conditionally branching.
   */
  private void visitTwoOpBranch( SymbolicInstruction inst ) {
      inst.addUse(opStack.pop());
      inst.addUse(opStack.pop());
  }

  /**
   * Visit instructions that perform a switch.   
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  The instruction performing a switch.
   */
  private void visitSwitch( SymbolicInstruction inst )
    {
	inst.addUse( opStack.pop() );
    }

  /**
   * Visit instructions that return a value.
   * Pop out the top operand and also add use this operand in the inst.
   * @param inst  The instruction that return a value.
   */
  private void visitValuedReturn( SymbolicInstruction inst )
    {
	inst.addUse(opStack.pop());
   
    }

  /**
   * Visit instructions that call a method.
   * @param inst  The instruction that call a method.
   */
  private void visitInvokeCall( SymbolicInstruction inst )
    {
      Vector operands = inst.getOperandsVector();
      if ( ( operands.size () != 1 ) || !( operands.get (0) instanceof Method ) ){
	  throw new RuntimeException("Error: DUGatheringVisitor.visitInvokeCall: must have one operand of type Method");
      }

      Method calledMethod = (Method)operands.get (0);

      Collection params = calledMethod.getFormalParameterTypesCollection ();

      // Pop up all the parameters of this method (including the object
      // that this method in invoked on, if this method need one).
      Vector allActualParams = new Vector();
      for ( int i = 0; i < params.size (); i++ ) {
	  Vector actualParams = opStack.pop();
	  inst.addUse(actualParams);
	  
	  allActualParams.insertElementAt( actualParams, 0 );
      }

      // If this method return a value, then create a temp variable for 
      // the return value, push it into the stack
      TypeEntry returnType = (jaba.sym.TypeEntry) calledMethod.getType();
      if( !(( returnType instanceof Primitive ) &&
	    ((Primitive)returnType).getType() == Primitive.VOID ) ) {
	  //Create the DefUse object for the return value
	  TempVariableImpl tempVar = new TempVariableImpl ();
	  tempVar.setType( returnType );
	  tempVar.setContainingMethod( method );
	  LeafDefUse returnDU;
	  if(returnType instanceof Primitive) {
	      returnDU = new PrimitiveDefUseImpl (tempVar);
	  }else {
	      returnDU = new ReferenceDefUseImpl (tempVar);
	  }
	   
	  //Set the definition in the return node
	  Edge[] outEdges = cfg.getOutEdges( node );
          // jim jones  may 3, 1999 -- now setting the definition on the
          // return node rather than on the call node
          //  inst.setDefinition( returnPrimDU );
	  ((StatementNode)outEdges[0].getSink()).
	      setDefinition( returnDU );

	  //Push it in the stack
	  Vector returnDUVec = new Vector();
	  returnDUVec.add( returnDU );
	  opStack.push( returnDUVec );
      }

      // based on the instruction, get some information about this method.
      // non-Static method require an object reference for method call.
      // for overridable method, we need to get all callable methods
      boolean isStaticMethod = false;
      boolean overridable = false;
      if(inst instanceof Invokestatic) {
	  isStaticMethod = true;
       }

      if(inst instanceof Invokeinterface || 
	 inst instanceof Invokevirtual) {
	  overridable = true;
      }

      // Get the object reference of this method call
      TypeEntry callingType = null;
      if(!isStaticMethod) {
	  // Get the DU that contains the Object reference
	  DefUse callingObj;
	  Vector objectRefs = (Vector)allActualParams.elementAt(0);
	  DUVectorHelper objectOperands = new DUVectorHelper(objectRefs);
	  DefUse callingObjContainer = objectOperands.getReferenceElement();
	  if(callingObjContainer == null) { //The calling obj is a Primitive
	      assert objectRefs.size() == 1:objectRefs;
	      callingObj = (DefUse)objectRefs.get(0);
	      if(callingObj instanceof RecursiveDefUse) {
		  callingObj = ((RecursiveDefUse)callingObj).getLeafElement();
	      }
              // The callingObj should be String literal
	      assert ((ConstantValue)((LeafDefUse)callingObj).getVariable()).getType().getName().equals("string"):((LeafDefUse)callingObj).getVariable(); 
	      
	  }else {
	      callingObj = DUVectorHelper.getReferenceDefUse(callingObjContainer);
	  }
	  
	  // Get the data type of the object reference
	  callingType = (jaba.sym.TypeEntry) ((LeafDefUse)callingObj).getVariable().getType();
	  if(callingObj instanceof CastedReferenceDefUse) {
	      callingType = (jaba.sym.TypeEntry) ((CastedReferenceDefUse)callingObj).getType();
	  }
      }

  
      // now, we must look at all subclasses to see if there
      // are other possible methods in those subclasses that may be called.
      // we add these methods to the operands.
      Vector callableMethod = new Vector();
      if ( overridable ) {
	  String signature = calledMethod.getSignature();
	  if ( callingType instanceof Class ) {
	      addMethodForClass( (Class)callingType, signature, 
				 callableMethod );
	      Collection subclasses = ((Class) callingType).getAllSubClassesCollection ();
	      for (Iterator j = subclasses.iterator (); j.hasNext ();)
		  addMethodForClass ((Class) j.next (), signature, callableMethod );
	  } else if ( callingType instanceof Interface) {
	      Collection subclasses = ((Interface)callingType).getAllImplementingClassesCollection ();
	      for (Iterator j = subclasses.iterator (); j.hasNext ();)
	      {
                  // It's necessary to check super class. See detail
                  // in the comment of addMethodForClassCheckingSuper
		  addMethodForClassCheckingSuper((Class) j.next (), signature,callableMethod );
	      }
	  }else { //Primitive or Array 
              // Since only methods available for an array or a string 
              // is Object/String method. Let's use the CalledMethod's
              // class type
	      Class typeObject = (Class)calledMethod.getContainingType();
	      addMethodForClass( typeObject, signature, 
				 callableMethod );
	  }
	      
      }else { //Not overridable
	  callableMethod.add(calledMethod);
      }

      // get the call site attribute
      MethodCallAttribute attr = Factory.getMethodCallAttribute (node);
      
      attr.setSyntaxMethod(calledMethod);
      // add the gathered methods to the attribute
      for (Iterator e = callableMethod.iterator (); e.hasNext ();)
	attr.addMethod ((Method) e.next ());
      

      // add the actual parameters to this attribute
      for (Iterator e = allActualParams.iterator (); e.hasNext ();)
	attr.addActualParameters ((Vector) e.next ());
      
      // add the called methods to the "called" relationship of the calling method
      // add the calling method to the "calling" relationship of the called methods
      method.addCalledMethod( calledMethod );
      calledMethod.addCallingMethod( method );
      for (Iterator e = callableMethod.iterator (); e.hasNext ();)
	{
	  Method m1 = (Method) e.next ();
	  method.addCalledMethod (m1);
	  m1.addCallingMethod (method);
	}
      
      // add the call site to the calling method
      method.addCallSiteNode( node );

    }

  /**
   * Visits an instruction that widens a value from one 32 bit word to 64.
   * @param inst  The widening instruction.
   */
  private void visitWideningInst( SymbolicInstruction inst, int newType ) {
      Vector narrowDUs = opStack.peek();
      DUVectorHelper operand = new DUVectorHelper(narrowDUs);

      Vector primitiveDUs = operand.getNonRefElements();
      for (Iterator e = primitiveDUs.iterator (); e.hasNext ();)
      {
	  DefUse primitive = (DefUse) e.next ();
	  PrimitiveDefUse leaf = null;
	  HasValue var;
	  if(primitive instanceof RecursiveDefUse) {
	      leaf = (PrimitiveDefUse)((RecursiveDefUse)primitive).getLeafElement();
	      var = leaf.getVariable();
	  }else {
	      var = ((PrimitiveDefUse)primitive).getVariable();
	  }
	  
          // Change the Variable from 32 bit to 64 bit
	  assert var.getType() instanceof Primitive:var.toString();
	  if ( var instanceof ConstantValue ) {
	      ((ConstantValue)var).setIs64Bit( true );
	  }  else if ( var instanceof LocalVariable ) {
	      LocalVariableImpl newVar = (LocalVariableImpl)
		  ((LocalVariable)var).clone();
	      TypeEntry type = (jaba.sym.TypeEntry) ((LocalVariable)var).getType();
	      Primitive newPrim = PrimitiveImpl.valueOf(newType);
	      ((LocalVariable)newVar).setType( newPrim );
	      if ( primitive instanceof PrimitiveDefUse )
		  ((PrimitiveDefUse)primitive).setVariable( newVar );
	      else
		  leaf.setVariable( newVar );
	  } else if ( var instanceof Field ) {
	      FieldImpl newVar = (FieldImpl)((Field)var).clone();
	      TypeEntry type = (jaba.sym.TypeEntry) ((Field)var).getType();
	      Primitive newPrim = PrimitiveImpl.valueOf(newType);
	      ((Field)newVar).setType( newPrim );
	      if ( primitive instanceof PrimitiveDefUse )
		  ((PrimitiveDefUse)primitive).setVariable( newVar );
	      else
		  leaf.setVariable( newVar );
	  }else if (var instanceof PlaceholderLocation) {
	      Primitive newPrim = PrimitiveImpl.valueOf(newType);
	      PlaceholderLocation newVar = new PlaceholderLocationImpl ();
	      newVar.setType(newPrim);
	      if ( primitive instanceof PrimitiveDefUse )
		  ((PrimitiveDefUse)primitive).setVariable( newVar );
	      else
		  leaf.setVariable( newVar );
	  }else {
	      //    assert false:inst.toString() + var;
	  }
      }
  }

  /**
   * Visits an instruction that narrows a value from one 64 bit word to 32.
   * @param inst  The narrowing instruction.
   */
  private void visitNarrowingInst( SymbolicInstruction inst, int newType )
    {
      Vector wideDUs = opStack.peek();
      DUVectorHelper operands = new DUVectorHelper(wideDUs);

      Vector primitiveDUs = operands.getNonRefElements();
      for(Iterator e = primitiveDUs.iterator (); e.hasNext ();)
      {
	  DefUse primitive = (DefUse) e.next ();
	  PrimitiveDefUse leaf = null;
	  HasValue var;
	  if(primitive instanceof RecursiveDefUse) {
	      leaf = (PrimitiveDefUse)((RecursiveDefUse)primitive).getLeafElement();
	      var = leaf.getVariable();
	  }else {
	      var = ((PrimitiveDefUse)primitive).getVariable();
	  }

	  // Changing the variable from 64 bit to 32 bit
	  assert var.getType() instanceof Primitive:var.toString();
	  if ( var instanceof ConstantValue ) {
	      ((ConstantValue)var).setIs64Bit( false );
	  }else if ( var instanceof LocalVariable ) {
	      LocalVariableImpl newVar = (LocalVariableImpl)
		  ((LocalVariable)var).clone();
	      TypeEntry type = (jaba.sym.TypeEntry) ((LocalVariable)var).getType();
	      assert type instanceof Primitive:type;
	      Primitive newPrim = PrimitiveImpl.valueOf(newType );
	      ((LocalVariable)newVar).setType( newPrim );
	      if ( primitive instanceof PrimitiveDefUse )
		  ((PrimitiveDefUse)primitive).setVariable( newVar );
	      else
		  leaf.setVariable( newVar );
	  }else if ( var instanceof Field ) {
	      FieldImpl newVar = (FieldImpl)((Field)var).clone();
	      TypeEntry type = (jaba.sym.TypeEntry) ((Field)var).getType();
	      Primitive newPrim = PrimitiveImpl.valueOf(newType );
	      ((Field)newVar).setType( newPrim );
	      if ( primitive instanceof PrimitiveDefUse )
		  ((PrimitiveDefUse)primitive).setVariable( newVar );
	      else
		  leaf.setVariable( newVar );
	  }else if(var instanceof PlaceholderLocation) {
	      Primitive newPrim = PrimitiveImpl.valueOf(newType);
	      PlaceholderLocation newVar = new PlaceholderLocationImpl ();
	      newVar.setType(newPrim);
	      if(primitive instanceof PrimitiveDefUse)
		  ((PrimitiveDefUse)primitive).setVariable(newVar);
	      else
		  leaf.setVariable(newVar);
	  }else {
	      //	      assert false:inst.toString() + var;
	  }
      }
    }

  /**
   * Adds a method to the <code>methods</code> vector that meet the 
   * <code>signature</code> and are members of <code>classType</code>.
   * @param classType  The class to search for a matching method.
   * @param signature  The method signature to match.
   * @param methods  A vector to append if a matching method is found.
   */
  private boolean addMethodForClass( Class classType, String signature, 
				     Vector methods ) {
      boolean found = false;
      Collection classMethods = classType.getMethodsCollection ();
      for (Iterator i = classMethods.iterator (); i.hasNext ();)
      {
	  Method method = (Method) i.next ();
	  if ( method.getSignature().equals( signature )) {
	      found = true;
	      if(!methods.contains(method)) {
		  methods.add( method );
		  break;
	      }
	  }
      }
      return found;
  }

  /**
   * Adds a method to the <code>methods</code> vector that meet the 
   * <code>signature</code> and are members of <code>classType</code> or
   * its super class. Checking super classes sometimes is necessary. 
   * E.G: Interface A {m(); } Interface B extends A {m();..}.
   * Class ImplA { m(){..} } (ImplA implements method m().
   * Class ImplB extends ImplA implements B{..}.(ImplB inherits m() fromImplA).
   * B b = new ImplB(); b.m(). For this case, to find callable method
   * ImplA.m(), we need to check the super class of ImplB.
   * @param classType  The class to search for a matching method.
   * @param signature  The method signature to match.
   * @param methods  A vector to append if a matching method is found.
   */
  private void  addMethodForClassCheckingSuper(Class classType, 
					      String signature, Vector method){
      boolean found = false;
      Class superClass = classType;
      while(!found  && superClass != null ) {
	  found = addMethodForClass(superClass, signature, method);
	  superClass = superClass.getSuperClass();
      }   
  }

  /**
   * Evaluates integer instruction for two constant value operands.
   * @param inst  The instruction to evaluate.
   * @param op1  Operand 1.
   * @param op2  Operand 2.
   * @return The resulting value.
   */
  private PrimitiveDefUse evaluateArithmeticInst( SymbolicInstruction inst,
						  PrimitiveDefUse op1,
						  PrimitiveDefUse op2 )
    {     
      PrimitiveDefUse returnVal = null;
      ConstantValue const1 = (ConstantValue)op1.getVariable();
      ConstantValue const2 = (ConstantValue)op2.getVariable();
      if ( !const1.getIsValidValue() || !const2.getIsValidValue() )
	return returnVal;
      int constVal1 = const1.getValue();
      int constVal2 = const2.getValue();
      int resultVal = 0;
      switch ( inst.getOpcode() )
	{
	case 96: //iadd
	  resultVal = constVal1 + constVal2;
	  break;
	case 100: //isub
	  resultVal = constVal1 - constVal2;
	  break;
	case 104: //imul
	  resultVal = constVal1 * constVal2;
	  break;
	case 108: //idiv
	  resultVal = constVal1 / constVal2;
	  break;
	case 112: //irem
	  resultVal = constVal1 % constVal2;
	  break;
	case 120: //ishl
	  resultVal = constVal1 << constVal2;
	  break;
	case 122: //ishr
	case 124: //iushr
	  resultVal = constVal1 >> constVal2;
	  break;
	default:
	    assert false:inst;
	  return returnVal;
	}

      ConstantValue resultConst = new ConstantValueImpl ();
      resultConst.setValue( resultVal );
      resultConst.setIsValidValue( true );

      returnVal = new PrimitiveDefUseImpl (resultConst );
      return returnVal;

    }

  /**
   * Checks to see if a the <code>def</code> DefUse object is on the stack.
   * This method is called before a variable is defined.  If the defined
   * DefUse object is on the stack (or any DefUse objects on the stack that
   * <code>def</code> is a parent of), a temporary variable DefUse object is
   * created and will replace the said DefUse object on the stack.  This is
   * done to preserve the proper definition-use chains.  For example: the
   * source statement, "i = (i++) + (i++);", produces the following bytecode:
   * iload i, iinc i 1, iload i, iinc i 1, iadd, istore i.
   * Without this checking and substitution, we say there is 3 def/use pairs:
   * {i,(i)}, {i,(i)}, {i,(i,i)}, however this is wrong.  The proper def/use
   * pairs should be: {temp1,(i)}, {i,(i)}, {temp2, (i)}, {i,(i)}, 
   * {i,(temp1,temp2)}.
   * This method will also create (if necessary) predecessor nodes in the
   * CFG to the current node for the definition of those created temporary
   * variables.
   * @param def  Definition to check for on the operand stack.
   */
  private void checkDef( DefUse def )
    {
      // create temporary stack used to pop items off the op stack and place
      // on it after each has been checked, then at the end we will pop each
      // off and place back on op stack
      Stack tempStack = new Stack();

      // create a vector to store new DefUse objects that are created
      Vector tempDUVec = new Vector();

      // create a vector to store the DefUse objects that are replaced by
      // temps -- the indexes in this vector will correlate with the indexes
      // in the tempDUVec
      Vector replacedDUVec = new Vector();

      // for every position on the op stack...
      while ( !opStack.isEmpty() )
	{

          // for every def use object on the current position...
	  Vector duVec = opStack.pop();
	  int i = 0;
	  for (Iterator e = duVec.iterator (); e.hasNext (); i++)
	    {
	      boolean equiv = false;
	      DefUse du = (DefUse) e.next ();
	      // check if it or any of its parents are equivalent to the def
	      if ( def.equals( du ) ) equiv = true;
	      else
		{
		  if ( du instanceof RecursiveDefUse )
		    {
		      DefUse[] parents = 
			((RecursiveDefUse)du).getParentDefUses();
		      for ( int j = 0; j < parents.length; j++ )
			{
			  if ( def.equals( parents[j] ) )
			    {
			      equiv = true;
			      break;
			    }
			}
		    }
		}

	      if ( equiv )
		{
                  // if so, get a temp for it (function call -- takes two 
                  // parameters (besides the DefUse object to replace), 
                  // a vector of temps that have been created 
                  // and a vector of DU objects that they replaced.  Both 
                  // vectors are of the same size and an index in one 
                  // correlates to an index in the other)
		  DefUse tempDU = getTempDU( du, tempDUVec, replacedDUVec );
		  
		  // replace it with the temp
		  duVec.set( i, tempDU );

		}

	    }
	 
	  // push this vector onto the temp stack
	  tempStack.push( duVec );

	}

      // for every position on the temp stack...
      while ( !tempStack.isEmpty() )
	{
	  // place the vector back on the op stack
	  opStack.push( (Vector)tempStack.pop() );
	}

      // for every temp variable created (walk the vector backward -- we
      // want the bottom-most DU to be defined first)...
      for ( int i = (tempDUVec.size() - 1); i >= 0; i-- )
	{
	  // create a new statement node that will hold the definition of the
	  // new temp and use of the replaced DU
	  StatementNode newNode = new StatementNodeImpl ();
	  newNode.setType( StatementNode.TEMP_DEFINITION_NODE );
	  newNode.setSourceLineNumber( node.getSourceLineNumber() );
	  newNode.setContainingMethod ((jaba.sym.Method) node.getContainingMethod() );
	  newNode.setDefinition( (DefUse)tempDUVec.elementAt( i ) );
	  newNode.addUse( (DefUse)replacedDUVec.elementAt( i ) );
	  cfg.addNode( newNode );

	  // move all edges coming into the current node to new node
	  Edge [] inEdges = cfg.getInEdges( node );
	  for ( int j = 0; j < inEdges.length; j++ )
	    {
	      Edge replacementEdge = 
		new EdgeImpl ((jaba.graph.Node) inEdges[j].getSource(), newNode );
	      replacementEdge.setLabel( inEdges[j].getLabel() );
	      Vector attrs = ((jaba.graph.Edge) inEdges [j]).getAttributesVector ();
	      for (Iterator k = attrs.iterator (); k.hasNext ();)
		replacementEdge.addAttribute ((EdgeAttribute) k.next ());
	      cfg.removeEdge ((jaba.graph.Edge) inEdges[j] );
	      cfg.addEdge( replacementEdge );
	    }

	  // create a new edge from the new node to the current node
	  Edge newEdge = new EdgeImpl ( newNode, node );
	  cfg.addEdge( newEdge );

	}

    }

  /**
   * This private method is used by <code>checkDef</code>.  It checks whether
   * the <code>du</code> DefUse object has a temp variable assigned to it.  If
   * so, it returns that one.  If not, it creates a new temp variable DefUse
   * object of the proper type.
   * @param du  DefUse object to check for.
   * @param tempDUVec  Vector of temporary DefUse objects that have been
   *                   gathered for replacement.  Is appended to if a new
   *                   temp is created.
   * @param replacedDUVec  Vector of DefUse objects that have already been
   *                       assigned replacement temp var DU objects in this
   *                       node.  Is appended to if a new temp is created.
   * @return  Temporary DefUse object to use to replace <code>du</code>.
   */
  private DefUse getTempDU( DefUse du, Vector tempDUVec, Vector replacedDUVec )
    {
      // check if the DefUse object to be replaced is already on the vector
      // of replaced DU's -- if so, return the temp that has already been
      // assigned for it
	int i = 0;
      for (Iterator e = replacedDUVec.iterator (); e.hasNext (); i++)
	{
	  if (du.equals ((DefUse) e.next ()))
	    return (DefUse)tempDUVec.elementAt( i );
	}

      // there isn't one already, so we need to create one...
      // get the leaf of the du tree to replace -- we need to create either
      // a PrimitiveDefUse or a ReferenceDefUse object and we need to know
      // which
      LeafDefUse leafDU;
      if ( du instanceof RecursiveDefUse )
	leafDU = ((RecursiveDefUse)du).getLeafElement();
      else
	leafDU = (LeafDefUse)du;
      // now, get the type of the du being replaced and create a temp variable
      // of the type being replaced
      HasValue var = leafDU.getVariable();
      TempVariableImpl tempVar = new TempVariableImpl ();
      tempVar.setType( var.getType() );
      tempVar.setContainingMethod( method );

      // assign the leaf def use object the temp variable
      DefUse tempDU;
      if ( leafDU instanceof PrimitiveDefUse )
	tempDU = new PrimitiveDefUseImpl (tempVar);
      else
	tempDU = new ReferenceDefUseImpl (tempVar );

      // place this new temp du on the temp du vector and the replaced du
      // on the replaced du vector
      tempDUVec.add( tempDU );
      replacedDUVec.add( du );

      // return the new temp du
      return tempDU;

    }

}
