/* Copyright (c) 1999, The Ohio State University */

package jaba.constants;

/**
 * Defines constants for various access-level keywords
 * in Java declarations.
 */
public interface AccessLevel
{
    /** Public access: access restricted to class, subclass, package, world. */
    public static final int A_PUBLIC = 0x0001;

    /** Private access: access restricted to class. */
    public static final int A_PRIVATE = 0x0002;

    /** Protected access: access restricted to class, subclass, package. */
    public static final int A_PROTECTED = 0x0004;

    /** Package access: access restricted to class, package */
    public static final int A_PACKAGE = 0;
}
