/* Copyright (c) 1999, The Ohio State University */

package jaba.constants;

/**
 * Defines opcode constants for mnemonics in the Java
 * Virtual Machine instruction set. Some opcodes have explicit operands,
 * whereas others take their operands from the operand stack.
 * <p>
 * The _quick pseudo-instructions (opcodes 203-228) are not part of the JVM
 * instruction set. The _quick instructions are used to implement an
 * an optimization in Sun's JVM: the JVM dynamically replaces certain
 * instructions (the first time they are executed) by their _quick variants.
 * The _quick variant of an instruction assumes that the constant-pool
 * entry is resolved, and therefore executes faster.
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 */
public abstract class OpCode implements java.io.Serializable
{
    /** Do nothing - no change to the stack. */
    public static final byte OP_NOP = 0;

    //////////////////
    // Bytecodes for pushing constants onto the stack
    //////////////////

    /** Push null object reference onto the stack. */
    public static final byte OP_ACONST_NULL = (byte)1;

    /** Push integer -1 onto the stack. */
    public static final byte OP_ICONST_M1 = (byte)2;

    /** Push integer 0 onto the stack. */
    public static final byte OP_ICONST_0 = (byte)3;

    /** Push integer 1 onto the stack. */
    public static final byte OP_ICONST_1 = (byte)4;

    /** Push integer 2 onto the stack. */
    public static final byte OP_ICONST_2 = (byte)5;

    /** Push integer 3 onto the stack. */
    public static final byte OP_ICONST_3 = (byte)6;

    /** Push integer 4 onto the stack. */
    public static final byte OP_ICONST_4 = (byte)7;

    /** Push integer 5 onto the stack. */
    public static final byte OP_ICONST_5 = (byte)8;

    /** Push long integer 0 onto the stack. */
    public static final byte OP_LCONST_0 = (byte)9;

    /** Push long integer 1 onto the stack. */
    public static final byte OP_LCONST_1 = (byte)10;

    /** Push single-precision float 0 onto the stack. */
    public static final byte OP_FCONST_0 = (byte)11;

    /** Push single-precision float 1 onto the stack. */
    public static final byte OP_FCONST_1 = (byte)12;

    /** Push single-precision float 2 onto the stack. */
    public static final byte OP_FCONST_2 = (byte)13;

    /** Push double-precision float 0 onto the stack. */
    public static final byte OP_DCONST_0 = (byte)14;

    /** Push double-precision float 1 onto the stack. */
    public static final byte OP_DCONST_1 = (byte)15;

    /** Push one-byte signed integer onto the stack. */
    public static final byte OP_BIPUSH = (byte)16;

    /** Push two-byte signed integer onto the stack. */
    public static final byte OP_SIPUSH = (byte)17;

    /** Push item from constant pool (8-bit index) onto the stack. */
    public static final byte OP_LDC = (byte)18;

    /** Push item from constant pool (16-bit index) onto the stack. */
    public static final byte OP_LDC_W = (byte)19;

    /** Push long/double from constant pool onto the stack. */
    public static final byte OP_LDC2_W = (byte)20;

    //////////////////
    // Bytecodes for loading local variables (including arrays) onto the stack
    //////////////////

    /** Load integer from local var onto the stack. */
    public static final byte OP_ILOAD = (byte)21;

    /** Load long integer from local var onto the stack. */
    public static final byte OP_LLOAD = (byte)22;

    /** Load single float from local var onto the stack. */
    public static final byte OP_FLOAD = (byte)23;

    /** Load double float from local var onto the stack. */
    public static final byte OP_DLOAD = (byte)24;

    /** Load object ref from local var onto the stack. */
    public static final byte OP_ALOAD = (byte)25;

    /** Load integer from local var 0 onto the stack. */
    public static final byte OP_ILOAD_0 = (byte)26;

    /** Load integer from local var 1 onto the stack. */
    public static final byte OP_ILOAD_1 = (byte)27;

    /** Load integer from local var 2 onto the stack. */
    public static final byte OP_ILOAD_2 = (byte)28;

    /** Load integer from local var 3 onto the stack. */
    public static final byte OP_ILOAD_3 = (byte)29;

    /** Load long integer from local vars 0,1 onto the stack. */
    public static final byte OP_LLOAD_0 = (byte)30;

    /** Load long integer from local vars 1,2 onto the stack. */
    public static final byte OP_LLOAD_1 = (byte)31;

    /** Load long integer from local vars 2,3 onto the stack. */
    public static final byte OP_LLOAD_2 = (byte)32;

    /** Load long integer from local vars 3,4 onto the stack. */
    public static final byte OP_LLOAD_3 = (byte)33;

    /** Load single float from local var 0 onto the stack. */
    public static final byte OP_FLOAD_0 = (byte)34;

    /** Load single float from local var 1 onto the stack. */
    public static final byte OP_FLOAD_1 = (byte)35;

    /** Load single float from local var 2 onto the stack. */
    public static final byte OP_FLOAD_2 = (byte)36;

    /** Load single float from local var 3 onto the stack. */
    public static final byte OP_FLOAD_3 = (byte)37;

    /** Load double float from local var 0 onto the stack. */
    public static final byte OP_DLOAD_0 = (byte)38;

    /** Load double float from local var 1 onto the stack. */
    public static final byte OP_DLOAD_1 = (byte)39;

    /** Load double float from local var 2 onto the stack. */
    public static final byte OP_DLOAD_2 = (byte)40;

    /** Load double float from local var 3 onto the stack. */
    public static final byte OP_DLOAD_3 = (byte)41;

    /** Load object ref from local var 0 onto the stack. */
    public static final byte OP_ALOAD_0 = (byte)42;

    /** Load object ref from local var 1 onto the stack. */
    public static final byte OP_ALOAD_1 = (byte)43;

    /** Load object ref from local var 2 onto the stack. */
    public static final byte OP_ALOAD_2 = (byte)44;

    /** Load object ref from local var 3 onto the stack. */
    public static final byte OP_ALOAD_3 = (byte)45;

    /** Load integer from array onto the stack. */
    public static final byte OP_IALOAD = (byte)46;

    /** Load long integer from array onto the stack. */
    public static final byte OP_LALOAD = (byte)47;

    /** Load single-precision float from array onto operand stack. */
    public static final byte OP_FALOAD = (byte)48;

    /** Load double-precision float from array onto operand stack. */
    public static final byte OP_DALOAD = (byte)49;

    /** Load object ref from array onto operand stack. */
    public static final byte OP_AALOAD = (byte)50;

    /** Load signed byte from array onto operand stack. */
    public static final byte OP_BALOAD = (byte)51;

    /** Load character from array onto operand stack. */
    public static final byte OP_CALOAD = (byte)52;

    /** Load byte from array onto operand stack. */
    public static final byte OP_SALOAD = (byte)53;

    //////////////////
    // Bytecodes for storing stack values into local variables
    // (including arrays)
    //////////////////

    /** Store integer into local variable. */
    public static final byte OP_ISTORE = (byte)54;

    /** Store long integer into local variable. */
    public static final byte OP_LSTORE = (byte)55;

    /** Store single float into local variable. */
    public static final byte OP_FSTORE = (byte)56;

    /** Store double float into local variable. */
    public static final byte OP_DSTORE = (byte)57;

    /** Store object ref into local variable. */
    public static final byte OP_ASTORE = (byte)58;

    /** Store integer into local variable 0. */
    public static final byte OP_ISTORE_0 = (byte)59;

    /** Store integer into local variable 1. */
    public static final byte OP_ISTORE_1 = (byte)60;

    /** Store integer into local variable 2. */
    public static final byte OP_ISTORE_2 = (byte)61;

    /** Store integer into local variable 3. */
    public static final byte OP_ISTORE_3 = (byte)62;

    /** Store long integer into local vars 0,1. */
    public static final byte OP_LSTORE_0 = (byte)63;

    /** Store long integer into local vars 1,2. */
    public static final byte OP_LSTORE_1 = (byte)64;

    /** Store long integer into local vars 2,3. */
    public static final byte OP_LSTORE_2 = (byte)65;

    /** Store long integer into local vars 3,4. */
    public static final byte OP_LSTORE_3 = (byte)66;

    /** Store single float into local variable 0. */
    public static final byte OP_FSTORE_0 = (byte)67;

    /** Store single float into local variable 1. */
    public static final byte OP_FSTORE_1 = (byte)68;

    /** Store single float into local variable 2. */
    public static final byte OP_FSTORE_2 = (byte)69;

    /** Store single float into local variable 3. */
    public static final byte OP_FSTORE_3 = (byte)70;

    /** Store double float into local vars 0,1. */
    public static final byte OP_DSTORE_0 = (byte)71;

    /** Store double float into local vars 1,2. */
    public static final byte OP_DSTORE_1 = (byte)72;

    /** Store double float into local vars 2,3. */
    public static final byte OP_DSTORE_2 = (byte)73;

    /** Store double float into local vars 3,4. */
    public static final byte OP_DSTORE_3 = (byte)74;

    /** Store object ref into local variable 0. */
    public static final byte OP_ASTORE_0 = (byte)75;

    /** Store object ref into local variable 1. */
    public static final byte OP_ASTORE_1 = (byte)76;

    /** Store object ref into local variable 2. */
    public static final byte OP_ASTORE_2 = (byte)77;

    /** Store object ref into local variable 3. */
    public static final byte OP_ASTORE_3 = (byte)78;

    /** Store from stack into an integer array. */
    public static final byte OP_IASTORE = (byte)79;

    /** Store from stack into a long integer array. */
    public static final byte OP_LASTORE = (byte)80;

    /** Store from stack into a single float array. */
    public static final byte OP_FASTORE = (byte)81;

    /** Store from stack into a double float array. */
    public static final byte OP_DASTORE = (byte)82;

    /** Store from stack into an object ref array. */
    public static final byte OP_AASTORE = (byte)83;

    /** Store from stack into a signed byte array. */
    public static final byte OP_BASTORE = (byte)84;

    /** Store from stack into a character array. */
    public static final byte OP_CASTORE = (byte)85;

    /** Store from stack into a byte array. */
    public static final byte OP_SASTORE = (byte)86;

    //////////////////
    // Bytecodes for stack instructions
    //////////////////

    /** Pop top word from the stack. */
    public static final byte OP_POP = (byte)87;

    /** Pop two top words from the stack. */
    public static final byte OP_POP2 = (byte)88;

    /** Duplicate the top word on the stack. */
    public static final byte OP_DUP = (byte)89;

    /** Duplicate top word on stack, then insert copy 2 words down. */
    public static final byte OP_DUP_X1 = (byte)90;

    /** Duplicate top word on stack, then insert copy 3 words down. */
    public static final byte OP_DUP_X2 = (byte)91;

    /** Duplicate the top two words on the stack. */
    public static final byte OP_DUP2 = (byte)92;

    /** Duplicate top two words on stack, then insert copies 2 words down. */
    public static final byte OP_DUP2_X1 = (byte)93;

    /** Duplicate top two words on stack, then insert copies 3 words down. */
    public static final byte OP_DUP2_X2 = (byte)94;

    /** Swap the top two words on the stack. */
    public static final byte OP_SWAP = (byte)95;

    //////////////////
    // Bytecodes for arithmetic instructions on the stack
    //////////////////

    /** Add two integer words on stack, replace them by their sum. */
    public static final byte OP_IADD = (byte)96;

    /** Add two long integers (4 words) on stack, replace them by sum. */
    public static final byte OP_LADD = (byte)97;

    /** Add two single float words on stack, replace them by their sum. */
    public static final byte OP_FADD = (byte)98;

    /** Add two double floats (4 words) on stack, replace them by sum. */
    public static final byte OP_DADD = (byte)99;

    /** Subtract two integer words on stack, replace them by difference. */
    public static final byte OP_ISUB = (byte)100;

    /** Subtract two long integers (4 words) on stack, replace them by difference. */
    public static final byte OP_LSUB = (byte)101;

    /** Subtract two single float words on stack, replace them by difference. */
    public static final byte OP_FSUB = (byte)102;

    /** Subtract two double floats (4 words) on stack, replace them by difference. */
    public static final byte OP_DSUB = (byte)103;

    /** Multiply two integer words on stack, replace them by product. */
    public static final byte OP_IMUL = (byte)104;

    /** Multiply two long integers (4 words) on stack, replace them by product. */
    public static final byte OP_LMUL = (byte)105;

    /** Multiply two single float words on stack, replace them by product. */
    public static final byte OP_FMUL = (byte)106;

    /** Multiply two double floats (4 words) on stack, replace them by product. */
    public static final byte OP_DMUL = (byte)107;

    /** Divide two integer words on stack, replace them by quotien. */
    public static final byte OP_IDIV = (byte)108;

    /** Divide two long integers (4 words) on stack, replace them by quotient. */
    public static final byte OP_LDIV = (byte)109;

    /** Divide two single float words on stack, replace them by quotient. */
    public static final byte OP_FDIV = (byte)110;

    /** Divide two double floats (4 words) on stack, replace them by quotient. */
    public static final byte OP_DDIV = (byte)111;

    /** Divide two integer words on stack, replace them by remainder. */
    public static final byte OP_IREM = (byte)112;

    /** Divide two long integers (4 words) on stack, replace them by remainder. */
    public static final byte OP_LREM = (byte)113;

    /** Divide two single float words on stack, replace them by remainder. */
    public static final byte OP_FREM = (byte)114;

    /** Divide two double floats (4 words) on stack, replace them by remainder. */
    public static final byte OP_DREM = (byte)115;

    /** Negate the top integer word on the stack. */
    public static final byte OP_INEG = (byte)116;

    /** Negate the top long integer (2 words) on the stack. */
    public static final byte OP_LNEG = (byte)117;

    /** Negate the top single float on the stack. */
    public static final byte OP_FNEG = (byte)118;

    /** Negate the top double float (2 words) on the stack. */
    public static final byte OP_DNEG = (byte)119;

    //////////////////
    // Bytecodes for logical instructions on stack
    //////////////////

    /** Left-shift using 2 top int egers, replace with result. */
    public static final byte OP_ISHL = (byte)120;

    /** Left-shift using 2 top long integers, replace with result. */
    public static final byte OP_LSHL = (byte)121;

    /** Right-shift using 2 top signed integers, replace with result. */
    public static final byte OP_ISHR = (byte)122;

    /** Right-shift using 2 top long signed integers, replace with result. */
    public static final byte OP_LSHR = (byte)123;

    /** Right-shift using 2 top unsigned integers, replace with result. */
    public static final byte OP_IUSHR = (byte)124;

    /** Right-shift using 2 top long unsigned integers, replace with result. */
    public static final byte OP_LUSHR = (byte)125;

    /** Integer boolean AND using 2 top integers on stack. */
    public static final byte OP_IAND = (byte)126;

    /** Long integer boolean AND using 2 top long integers (4 words) on stack. */
    public static final byte OP_LAND = (byte)127;

    /** Integer boolean OR using 2 top integers on stack. */
    public static final byte OP_IOR = (byte)128;

    /** Long integer boolean OR using 2 top long integers (4 words) on stack. */
    public static final byte OP_LOR = (byte)129;

    /** Integer boolean XOR using 2 top integers on stack. */
    public static final byte OP_IXOR = (byte)130;

    /** Long integer boolean XOR using 2 top long integers (4 words) on stack. */
    public static final byte OP_LXOR = (byte)131;

    //////////////////
    // Bytecodes for incrementation of local variables
    //////////////////

    /** Increment local var integer by constant. */
    public static final byte OP_IINC = (byte)132;

    //////////////////
    // Bytecodes for conversion operations on stack
    //////////////////

    /** Convert integer to long integer on stack. */
    public static final byte OP_I2L = (byte)133;

    /** Convert integer to single-precision float on stack. */
    public static final byte OP_I2F = (byte)134;

    /** Convert integer to double-precision float on stack. */
    public static final byte OP_I2D = (byte)135;

    /** Convert long integer to integer on stack. */
    public static final byte OP_L2I = (byte)136;

    /** Convert long integer to single-precision float on stack. */
    public static final byte OP_L2F = (byte)137;

    /** Convert long integer to double-precision float on stack. */
    public static final byte OP_L2D = (byte)138;

    /** Convert single-precision float to integer on stack. */
    public static final byte OP_F2I = (byte)139;

    /** Convert single-precision float to long integer on stack. */
    public static final byte OP_F2L = (byte)140;

    /** Convert single-precision float to double-precision float on stack. */
    public static final byte OP_F2D = (byte)141;

    /** Convert double-precision float to integer on stack. */
    public static final byte OP_D2I = (byte)142;

    /** Convert double-precision float to long integer on stack. */
    public static final byte OP_D2L = (byte)143;

    /** Convert double-precision float to single-precision float on stack. */
    public static final byte OP_D2F = (byte)144;

    /** Convert integer to signed byte on stack. */
    public static final byte OP_I2B = (byte)145;

    /** Convert integer to char on stack. */
    public static final byte OP_I2C = (byte)146;

    /** Convert integer to byte on stack. */
    public static final byte OP_I2S = (byte)147;

    //////////////////
    // Bytecodes for stack value comparison instructions
    //////////////////

    /** Long integer compare of 2 long integers on stack. */
    public static final byte OP_LCMP = (byte)148;

    /** Single float less-than compare of 2 single floats on stack. */
    public static final byte OP_FCMPL = (byte)149;

    /** Single float greater-than compare of 2 single floats on stack. */
    public static final byte OP_FCMPG = (byte)150;

    /** Double float less-than compare of 2 double floats on stack. */
    public static final byte OP_DCMPL = (byte)151;

    /** Double float greater-than compare of 2 double floats on stack. */
    public static final byte OP_DCMPG = (byte)152;

    //////////////////
    // Bytecodes for control transfer instructions based on top stack value
    // (zero comparisons)
    //////////////////

    /** Branch if equal to zero. */
    public static final byte OP_IFEQ = (byte)153;

    /** Branch if not equal to zero. */
    public static final byte OP_IFNE = (byte)154;

    /** Branch if less than zero. */
    public static final byte OP_IFLT = (byte)155;

    /** Branch if greater than or equal to zero. */
    public static final byte OP_IFGE = (byte)156;

    /** Branch if greater than zero. */
    public static final byte OP_IFGT = (byte)157;

    /** Branch if less than or equal to zero. */
    public static final byte OP_IFLE = (byte)158;

    //////////////////
    // Bytecodes for control transfer instructions based on comparison of two
    // top stack values
    //////////////////

    /** Branch if two top integers on stack are equal. */
    public static final byte OP_IF_ICMPEQ = (byte)159;

    /** Branch if two top integers on stack are not equal. */
    public static final byte OP_IF_ICMPNE = (byte)160;

    /** Branch if one integer less than other integer on stack. */
    public static final byte OP_IF_ICMPLT = (byte)161;

    /** Branch if one integer greater than/equal to other integer on stack. */
    public static final byte OP_IF_ICMPGE = (byte)162;

    /** Branch if one integer greater than other integer on stac. */
    public static final byte OP_IF_ICMPGT = (byte)163;

    /** Branch if one integer less than/equal to other integer on stack. */
    public static final byte OP_IF_ICMPLE = (byte)164;

    /** Branch if two top object references on stack are equal. */
    public static final byte OP_IF_ACMPEQ = (byte)165;

    /** Branch if two top object references on stack are not equal. */
    public static final byte OP_IF_ACMPNE = (byte)166;

    //////////////////
    // Bytecodes for basic branching (word indices)
    //////////////////

    /** Unconditional branch (word index branch). */
    public static final byte OP_GOTO = (byte)167;

    /** Jump subroutine (word index). */
    public static final byte OP_JSR = (byte)168;

    /** Return from subroutine (word index). */
    public static final byte OP_RET = (byte)169;

    //////////////////
    // Bytecodes for accessing jump table
    //////////////////

    /** Access the jump table by index and jump. */
    public static final byte OP_TABLESWITCH = (byte)170;

    /** Access the jump table by key match and jump. */
    public static final byte OP_LOOKUPSWITCH = (byte)171;

    //////////////////
    // Bytecodes for method returns
    //////////////////

    /** Return integer from method. */
    public static final byte OP_IRETURN = (byte)172;

    /** Return long integer from method. */
    public static final byte OP_LRETURN = (byte)173;

    /** Return single-precision float from method. */
    public static final byte OP_FRETURN = (byte)174;

    /** Return double-precision float from method. */
    public static final byte OP_DRETURN = (byte)175;

    /** Return object reference from method. */
    public static final byte OP_ARETURN = (byte)176;

    /** Return from void from method. */
    public static final byte OP_RETURN = (byte)177;

    //////////////////
    // Bytecodes for manipulating object fields
    //////////////////

    /** Get static field from class. */
    public static final byte OP_GETSTATIC = (byte)178;

    /** Set static field in class. */
    public static final byte OP_PUTSTATIC = (byte)179;

    /** Get field from object. */
    public static final byte OP_GETFIELD = (byte)180;

    /** Set field in object. */
    public static final byte OP_PUTFIELD = (byte)181;

    //////////////////
    // Bytecodes for method invocation
    //////////////////

    /** Invoke instance method, dispatch based on run-time type. */
    public static final byte OP_INVOKEVIRTUAL = (byte)182;

    /**
     * Invoke instance method, dispatch based on compile-time type (special
     * handling for suoerclass, private, and instance-initialization
     * methods.
     */
    public static final byte OP_INVOKESPECIAL = (byte)183;

    /** Invoke a class (static) method. */
    public static final byte OP_INVOKESTATIC = (byte)184;

    /** Invoke interface method. */
    public static final byte OP_INVOKEINTERFACE = (byte)185;


    /**
     * Undefined bytecode. This bytecode is not used in the JVM instruction
     * set. It is used by the CFG construction process to ignore certain
     * instructions.
     */
    public static final byte OP_UNDEF = (byte)186;

    //////////////////
    // Bytecodes for object creation
    //////////////////

    /** Create new object. */
    public static final byte OP_NEW = (byte)187;

    //////////////////
    // Bytecodes for array management
    //////////////////

    /** Allocate new array. */
    public static final byte OP_NEWARRAY = (byte)188;

    /** Allocate new array of references to objects. */
    public static final byte OP_ANEWARRAY = (byte)189;

    /** Get length of array. */
    public static final byte OP_ARRAYLENGTH = (byte)190;

    //////////////////
    // Bytecodes for exception handling
    //////////////////

    /** Throw exception or error. */
    public static final byte OP_ATHROW = (byte)191;

    //////////////////
    // Bytecodes for object checking
    //////////////////

    /** Check whether object is of given type; throw exception for illegal
        typecasts. */
    public static final byte OP_CHECKCAST = (byte)192;

    /** Determine if object is of given type; return result on the stack. */
    public static final byte OP_INSTANCEOF = (byte)193;

    //////////////////
    // Bytecodes for multithreading control
    //////////////////

    /** Enter monitor for object (critical section entry). */
    public static final byte OP_MONITORENTER = (byte)194;

    /** Exit monitor for object (critical section exit). */
    public static final byte OP_MONITOREXIT = (byte)195;

    //////////////////
    // Miscellaneous bytecodes - multidimensional array support,
    // wide support, wide branching, etc.
    //////////////////

    /**
     * Extend index for local variable by additional bytes for a load, store,
     * or ret operation.
     */
    public static final byte OP_WIDE = (byte)196;

    /** Allocate new multi-dimensional array. */
    public static final byte OP_MULTIANEWARRAY = (byte)197;

    /** Branch if reference is null. */
    public static final byte OP_IFNULL = (byte)198;

    /** Branch if reference is not null. */
    public static final byte OP_IFNONNULL = (byte)199;

    /** Branch always (wide index). */
    public static final byte OP_GOTO_W = (byte)200;

    /** Jump subroutine (wide index). */
    public static final byte OP_JSR_W = (byte)201;

    //////////////////
    // Special bytecodes for debugging support
    // Note that they cannot appear inside of valid .class files
    //////////////////

    /** Stop and pass control to breakpoint handler. */
    public static final byte OP_BREAKPOINT = (byte)202;

    //////////////////
    // "Quick" bytecodes for JIT ("just-in-time") compiling at runtime
    // Note that they cannot appear inside of valid .class files
    // Bytecode 220: Undefined in the JVM specification
    // For a description of the quick bytecodes, refer to it counterpart
    // above.
    //////////////////

    /** Load item from constant pool onto the stack. */
    public static final byte OP_LDC_QUICK = (byte)203;

    /** Load item from constant pool onto the stack (wide index). */
    public static final byte OP_LDC_W_QUICK = (byte)204;

    /** Load long or double from constant pool onto the stack (wide index). */
    public static final byte OP_LDC2_W_QUICK = (byte)205;

    /** Get field from object. */
    public static final byte OP_GETFIELD_QUICK = (byte)206;

    /** Set field in object. */
    public static final byte OP_PUTFIELD_QUICK = (byte)207;

    /** Get long or double field from object. */
    public static final byte OP_GETFIELD2_QUICK = (byte)208;

    /** Set long or double field in object. */
    public static final byte OP_PUTFIELD2_QUICK = (byte)209;

    /** Get static field from class. */
    public static final byte OP_GETSTATIC_QUICK = (byte)210;

    /** Set static field in class. */
    public static final byte OP_PUTSTATIC_QUICK = (byte)211;

    /** Get static field from class (two-word wide fields). */
    public static final byte OP_GETSTATIC2_QUICK = (byte)212;

    /** Set static field in class (two-word wide fields). */
    public static final byte OP_PUTSTATIC2_QUICK = (byte)213;

    /** Invoke an instance method. */
    public static final byte OP_INVOKEVIRTUAL_QUICK = (byte)214;

    /**
     * Invoke an instance-initialization or a private method, dispatching
     * based on compile-time type.
     */
    public static final byte OP_INVOKENONVIRTUAL_QUICK = (byte)215;

    /** Invoke a superclass method, dispatching based on compile-time type. */
    public static final byte OP_INVOKESUPER_QUICK = (byte)216;

    /** Invoke a class (static) method. */
    public static final byte OP_INVOKESTATIC_QUICK = (byte)217;

    /** Invoke interface method. */
    public static final byte OP_INVOKEINTERFACE_QUICK = (byte)218;

    /** Invoke an instance method. */
    public static final byte OP_INVOKEVIRTUALOBJECT_QUICK = (byte)219;

    /** Create a new object. */
    public static final byte OP_NEW_QUICK = (byte)221;

    /** Create a new array. */
    public static final byte OP_ANEWARRAY_QUICK = (byte)222;

    /** Create a new multi-dimensional array. */
    public static final byte OP_MULTIANEWARRAY_QUICK = (byte)223;

    /** Check whether object is of given type. */
    public static final byte OP_CHECKCAST_QUICK = (byte)224;

    /** Determine if object is of given type. */
    public static final byte OP_INSTANCEOF_QUICK = (byte)225;

    /** Invoke an instance method (two-byte offset into the method table). */
    public static final byte OP_INVOKEVIRTUAL_QUICK_W = (byte)226;

    /** Get field from object (two-word wide fields). */
    public static final byte OP_GETFIELD_QUICK_W = (byte)227;

    /** Set field in object (wide index). */
    public static final byte OP_PUTFIELD_QUICK_W = (byte)228;

    //////////////////
    // Special bytecodes reserved for internal use by a JVM implementation
    // Note that they cannot appear inside of valid .class files
    //////////////////

    /** Trap for implementation-specific functionality implemented in software. */
    public static final byte OP_IMPDEP1 = (byte)254;

    /** Trap for implementation-specific functionality implemented in hardware. */
    public static final byte OP_IMPDEP2 = (byte)255;

    /**
     * String prefix for an op code name that is used to instantiate a
     * <code>SymbolicInstruction</code>.
     */
    public static final String namePrefix = "jaba.instruction.";

    /**
     * Returns a string name for a JVM instruction.
     * @return string name of a JVM instruction.
     */
    public static String getName (byte b)
    {
        int index = (b & 0x000000ff);
        return opCodeNames [index];
    }

    /** Names of opcodes */
    private static final String [] opCodeNames = { "Nop",
        "Aconst_null", "Iconst_m1", "Iconst_0", "Iconst_1", "Iconst_2",
        "Iconst_3", "Iconst_4", "Iconst_5", "Lconst_0", "Lconst_1",
        "Fconst_0", "Fconst_1", "Fconst_2", "Dconst_0", "Dconst_1",
        "Bipush", "Sipush", "Ldc", "Ldc_w", "Ldc2_w",
        "Iload", "Lload", "Fload", "Dload", "Aload",
        "Iload_0", "Iload_1", "Iload_2", "Iload_3", "Lload_0",
        "Lload_1", "Lload_2", "Lload_3", "Fload_0", "Fload_1",
        "Fload_2", "Fload_3", "Dload_0", "Dload_1", "Dload_2",
        "Dload_3", "Aload_0", "Aload_1", "Aload_2", "Aload_3",
        "Iaload", "Laload", "Faload", "Daload", "Aaload",
        "Baload", "Caload", "Saload", "Istore", "Lstore",
        "Fstore", "Dstore", "Astore", "Istore_0", "Istore_1",
        "Istore_2", "Istore_3", "Lstore_0", "Lstore_1", "Lstore_2",
        "Lstore_3", "Fstore_0", "Fstore_1", "Fstore_2", "Fstore_3",
        "Dstore_0", "Dstore_1", "Dstore_2", "Dstore_3", "Astore_0",
        "Astore_1", "Astore_2", "Astore_3", "Iastore", "Lastore",
        "Fastore", "Dastore", "Aastore", "Bastore", "Castore",
        "Sastore", "Pop", "Pop2", "Dup", "Dup_x1",
        "Dup_x2", "Dup2", "Dup2_x1", "Dup2_x2", "Swap",
        "Iadd", "Ladd", "Fadd", "Dadd", "Isub",
        "Lsub", "Fsub", "Dsub", "Imul", "Lmul",
        "Fmul", "Dmul", "Idiv", "Ldiv", "Fdiv",
        "Ddiv", "Irem", "Lrem", "Frem", "Drem",
        "Ineg", "Lneg", "Fneg", "Dneg", "Ishl",
        "Lshl", "Ishr", "Lshr", "Iushr", "Lushr",
        "Iand", "Land", "Ior", "Lor", "Ixor",
        "Lxor", "Iinc", "I2l", "I2f", "I2d",
        "L2i", "L2f", "L2d", "F2i", "F2l",
        "F2d", "D2i", "D2l", "D2f", "I2b",
        "I2c", "I2s", "Lcmp", "Fcmpl", "Fcmpg",
        "Dcmpl", "Dcmpg", "Ifeq", "Ifne", "Iflt",
        "Ifge", "Ifgt", "Ifle", "If_icmpeq", "If_icmpne",
        "If_icmplt", "If_icmpge", "If_icmpgt", "If_icmple", "If_acmpeq",
        "If_acmpne", "Goto", "Jsr", "Ret", "Tableswitch",
        "Lookupswitch", "Ireturn", "Lreturn", "Freturn", "Dreturn",
        "Areturn", "Return", "Getstatic", "Putstatic", "Getfield",
        "Putfield", "Invokevirtual", "Invokespecial", "Invokestatic",
        "Invokeinterface",
        "", "New", "Newarray", "Anewarray", "Arraylength",
        "Athrow", "Checkcast", "Instanceof", "Monitorenter", "Monitorexit",
        "Wide", "Multianewarray", "Ifnull", "Ifnonnull", "Goto_w",
        "Jsr_w"
    };
}
