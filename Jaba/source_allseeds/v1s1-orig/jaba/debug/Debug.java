package jaba.debug;

import java.io.PrintWriter;

/** Represents a debug class. This is not a "real" debug class, as it won't
 * do anything. The purpose of creating this fake debug class is for 
 * performace consideration. So it won't hurt the performance if people
 * are not in the debugging mode. You would need to get the real debug
 * class if you want to debug the system.
 * @author Huaxing Wu -- <i> Created </i>
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable; updated JavaDoc
 */
public class Debug implements java.io.Serializable
{
    /** ??? */
    public static PrintWriter writer = new PrintWriter(System.out, true);

    /** The int representaion of module */
    public static final int DU = 0;
    public static final int CFG = 1;
    public static final int ICFG = 2;
    public static final int ACFG = 3;
    public static final int ST = 4;
    public static final int GRAPH = 5;
   
    /** ??? */
    public static void println(String message, int module, int level) {
	return;
    }
    
    /** ??? */
    public static void println(Object obj, int module, int level) {
	return;
    }

    /** ??? */
    public static void print(String message, int module, int level) {
	return;
    }

    /** ??? */
    public static void print(Object obj, int module, int level) {
	return;
    }

    /** ??? */
    public static boolean setDebugging(int module, int level) {
	return false;
    }

    /**
     * Parse the argument given to the debugging flag.
     * Since this class is a fake one, it will remind the user
     * to put the real one on.
     * @return true, if the argument parsed without problems.
     */
    public static boolean setDebugging(String debuggingString) {
	writer.println("Please include the real debug class in classpath");
	writer.println("It is probably in debug.jar");
	writer.println("\n\n");
	return false;
    }
}
