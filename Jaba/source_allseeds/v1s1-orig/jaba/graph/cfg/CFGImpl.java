/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

import jaba.graph.MethodGraph;
import jaba.graph.MethodGraphImpl;
import jaba.graph.StatementNode;
import jaba.graph.StatementNodeImpl;
import jaba.graph.Node;
import jaba.graph.FinallyEntryAttribute;
import jaba.graph.FinallyEntryAttributeImpl;
import jaba.graph.FinallyContextAttribute;
import jaba.graph.FinallyContextAttributeImpl;
import jaba.graph.FinallyCallAttribute;
import jaba.graph.FinallyCallAttributeImpl;
import jaba.graph.FinallyTransferAttribute;
import jaba.graph.FinallyTransferAttributeImpl;
import jaba.graph.MethodCallAttribute;
import jaba.graph.MethodCallAttributeImpl;
import jaba.graph.NewInstanceAttribute;
import jaba.graph.NewInstanceAttributeImpl;
import jaba.graph.ExceptionAttribute;
import jaba.graph.ExceptionAttributeImpl;
import jaba.graph.Edge;
import jaba.graph.EdgeImpl;
import jaba.graph.CallAttribute;
import jaba.graph.ThrowStatementAttribute;
import jaba.graph.ThrowStatementAttributeImpl;
import jaba.graph.ThrowEdgeAttribute;
import jaba.graph.ThrowEdgeAttributeImpl;

import jaba.instruction.visitor.InstructionLoadingVisitor;

import jaba.classfile.LineNumberTableAttribute;
import jaba.classfile.ExceptionHandler;
import jaba.classfile.LocalVariableTableAttribute;
import jaba.classfile.MethodInfo;
import jaba.classfile.CodeAttribute;
import jaba.classfile.AttributeInfo;

import jaba.sym.Method;
import jaba.sym.MethodImpl;
import jaba.sym.MethodAttribute;
import jaba.sym.Class;
import jaba.sym.Interface;
import jaba.sym.Array;
import jaba.sym.LocalVariableMap;
import jaba.sym.Primitive;
import jaba.sym.PrimitiveImpl;
import jaba.sym.ConstantPoolMap;
import jaba.sym.DemandDrivenAttribute;

import jaba.instruction.SymbolicInstruction;

import jaba.constants.OpCode;

import jaba.du.DefUseInitializer;
import jaba.du.DefUseInitializerImpl;

import jaba.debug.Debug;

import jaba.main.Options;

import jaba.tools.local.Factory;

import java.util.HashMap;
import java.util.Collection;
import java.util.Vector;
import java.util.Iterator;

/**
 * Represents a control-flow graph.
 *
 * @author S. Sinha
 * @author Huaxing wu -- <i> Revised 8/10/02. Chnage to use Primitve's
 *                  factory method valueOf to get Primitive object. </i>
 * @author S. Sinha -- <i> Revised 8/20/02 </i> Added a private vector to
 * store finally methods directly contained in the method for this CFG. These
 * are stored to avoid calling method.getFinallyMethods(), which can result
 * in an infinite loop (because method.getFinallyMethods() constructs
 * the CFG for the method before returning the array of finally methods).
 * @author Jay Lofstead 2003/05/29 created a toString that generates XML using the printGraph method.
 * 					updated printGraph to have a slightly better format.
 * @author Jay Lofstead 2003/06/02 removed elementAt in favor of enumeration/iteration for better performance.
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/12 added back in accidentially deleted line
 * @author Jay Lofstead 2003/06/16 added toStringRecuced (), toStringDetailed ()
 * @author Jay Lofstead 2003/06/20 changed to use the options from the program object
 * @author Jay Lofstead 2003/07/01 changed to implement CFG
 * @author Jay Lofstead 2003/07/17 removed legacy load ()
 * @author Jay Lofstead 2003/07/17 changed to use Factory to get public attributes
 */
public class CFGImpl extends MethodGraphImpl implements DemandDrivenAttribute, MethodAttribute, CFG
{
    // Constructor

    /**
     * Creates a new CFG object with specified method.
     * @param method method for which CFG object is created.
     */
    private CFGImpl ( Method method ) {
        super( method );
        typeInference = new TypeInference (this);
        handlers = null;
        exceptionExitNodes = new HashMap();
        exceptionalFinallyCallNodes = new HashMap();
	propRuntimeExceptions = null;
	finallyMethods = new Vector();
	finallyGotoExitNodes = new HashMap();
	finallyReturnExitNode = null;
        bytecodeOffsetNodeMap = new HashMap();
	finallyNameToStartNodeMap = new HashMap();
	finallyNameToEndNodeMap = new HashMap();
    }

    /**
     * Creates a new CFG object with specified method, and exception-handler
     * table. This is a private constructor used internally to create CFGs for
     * finally blocks.
     * @param method method for which CFG object is created.
     * @param handlers exception-table class for method.
     */
    private CFGImpl ( Method method, ExceptionHandlers handlers ) {
        super( method );
        typeInference = new TypeInference (this);
        this.handlers = handlers;
        exceptionExitNodes = new HashMap();
        exceptionalFinallyCallNodes = new HashMap();
	finallyMethods = new Vector();
	finallyGotoExitNodes = new HashMap();
	finallyReturnExitNode = null;
        bytecodeOffsetNodeMap = new HashMap();
    }

    // Methods

    /**
     * Returns the exceptional-exit nodes of this CFG.
     * @return Array of exceptional-exit nodes of this CFG. The length of
     *         the array is 0 if the CFG contains no exceptional-exit nodes.
     */
    public StatementNode [] getExceptionalExitNodes ()
    {
        StatementNode[] r = new StatementNode[exceptionExitNodes.size()];
        Iterator iter = exceptionExitNodes.values().iterator();
        int i = 0;
        while ( iter.hasNext() ) {
            r[i] = (StatementNode)iter.next();
            i++;
        }
        return r;
    }

    /**
     * Returns the number of exceptional-exit nodes in this CFG.
     * @return Number of exceptional-exit nodes that this CFG contains.
     */
    public int getExceptionalExitNodeCount() {
        return exceptionExitNodes.size();
    }

    /**
     * Returns the runtime exceptions that are propagated by the
     * corresponding method. If this method is called after the CFG
     * construction but before the ICFG construction, the exceptions include
     * only those that are directly propagated by the method. Is this method
     * is called after the ICFG construction, the exceptions include
     * both directly and indirectly-propagated exceptions.
     * @return Array containing <code>String</code> fully-qualified
     *         (dot-separated) names of propagated runtime exceptions.
     */
    public String[] getPropagatedRuntimeExceptions()
    {
	String [] propExceptions = new String [propRuntimeExceptions.size ()];
	int i = 0;
	for (Iterator e = propRuntimeExceptions.iterator (); e.hasNext (); i++)
	{
	    propExceptions[i] = (String) e.next ();
	}
        return propExceptions;
    }

    /**
     * Initializes the set of implicit exceptions that are
     * propagated directly by the method corresponding to this CFG.
     */
    private void initDirectPropRuntimeExceptions( SymbolicInstruction[]
						  instr ) {
	propRuntimeExceptions = new Vector();
	for ( int i = 0; i < instr.length; i++ ) {
	    int offset = instr[i].getByteCodeOffset();
	    String[] runtimeExcp = instr[i].getRuntimeExceptions();
	    for ( int j = 0; j < runtimeExcp.length; j++ ) {
		checkExceptionPropagated( offset, runtimeExcp[j] );
	    }
	}
    }

    /**
     * Checks if an exception raised at a given bytecode offset propagates
     * out of the method. If it does, adds that exception to the
     * set of propagated implicit exceptions for the method.
     * @param offset bytecode offset at which exception is raised
     * @param exceptionName fully-qualified (dot-separated) name of exception
     *                      that is raised
     */
    public void checkExceptionPropagated( int offset, String exceptionName ) {
	if ( propRuntimeExceptions.contains( exceptionName ) ) {
	    return;
	}
	if ( isExceptionPropagated( offset, exceptionName ) ) {
	    propRuntimeExceptions.add( exceptionName );
	}
    }

    /**
     * Determines if an exception raised at a given bytecode offset propagates
     * out of the method
     * @param offset bytecode offset at which exception is raised
     * @param exceptionName fully-qualified (dot-separated) name of exception
     *                      that is raised
     * @return boolean <code>true</code> if exception is propagated;
     *                 <code>false</code> otherwise
     */
    public boolean isExceptionPropagated( int offset,
					  String exceptionName ) {
	try {
	    ClassLoader loader = ((jaba.sym.Program) getMethod ().getContainingType ().getProgram ()).getClassLoader ();
	    java.lang.Class exceptionType =
		loader.loadClass( exceptionName );
	    Class[] enclHandlers =
		((ExceptionHandlersImpl) handlers).getEnclosingHandlers( offset, firstInstructionOffset,
					       lastInstructionOffset );
	    for ( int i = 0; i < enclHandlers.length; i++ ) {
		if ( enclHandlers[i] == null ) { // skip finally blocks
		    continue;
		}
		String catchName = enclHandlers[i].getName();
		catchName = catchName.replace( '/', '.' );
		try {
		    java.lang.Class catchType =
			loader.loadClass( catchName );
		    if ( catchType.isAssignableFrom( exceptionType ) ) {
			return false;
		    }
		}
		catch ( ClassNotFoundException cnfe){
		    cnfe.printStackTrace();
		    throw new RuntimeException( cnfe );
		}

	    }
	    
	}
	catch ( ClassNotFoundException cnfe){
	    cnfe.printStackTrace();
	    throw new RuntimeException( cnfe );
	}

	return true;
    }

    /**
     * Returns the byte code offset of the last symbolic instruction in the
     * method corresponding to this CFG.
     * @return the byte code offset of the last symbolic instruction in the
     * method corresponding to this CFG.
     */
    public int getLastInstructionOffset() {
	return lastInstructionOffset;
    }

    /** ??? */
    private void constructCFGforFinally( Vector symInstr,
					 ConstantPoolMap constantPoolMap,
					 ExceptionHandlers handlers,
					 LineNumberTableAttribute lineNumTable,
					 int firstInstrOffset,
					 int lastInstrOffset )
    {
	// recursively build CFGs of all nested finally blocks
	int[] finallyStartOffsets =
	    ((ExceptionHandlersImpl) handlers).getToplevelFinallyCFGStartOffsets( firstInstrOffset,
							lastInstrOffset );
	int numFinallyBlocks = finallyStartOffsets.length;

	Debug.println( "  Number of finally blocks in instruction "+
		       "range "+firstInstrOffset+" and "+lastInstrOffset+
		       " = "+numFinallyBlocks, Debug.CFG, 1 );

	// recursively build CFGs for all finally blocks contained in the
	// method. For each finally block, create a method object, add it
	// to current method's list of finally-block methods.
	if ( numFinallyBlocks != 0 ) {
	    int[] finallyEndOffsets =
		((ExceptionHandlersImpl) handlers).getFinallyEndOffsets( finallyStartOffsets );
	    Method parentMethod = getMethod();
	    for ( int i=0; i<numFinallyBlocks; i++ ) {
		Method finallyMethod = new MethodImpl ();
		
		// set fields of the finally method object
		
		// fields inherited from SymbolTableEntry: name
		finallyMethod.setName(
		    ((ExceptionHandlersImpl) handlers).getFinallyNameForCFGStartOffset(
		        finallyStartOffsets[i] ) );

		// fields inherited from Member: type, containing type,
		// descriptor, signature
		finallyMethod.setType(PrimitiveImpl.valueOf(PrimitiveImpl.VOID));
		finallyMethod.setContainingType ((jaba.sym.NamedReferenceType) parentMethod.getContainingType ());
		((jaba.sym.SymbolTable) finallyMethod.getContainingType().getProgram().
		    getSymbolTable()).addSymbol( finallyMethod );
		finallyMethod.setDescriptor( new String("()V") );
		finallyMethod.setSignature( parentMethod.
					    getTopLevelContainingMethod().
					    getSignature() + "." + 
					    finallyMethod.getName() );

		// fields in Method: containing method
		finallyMethod.setContainingMethod( parentMethod );

		// add finally method to parent method and the private vector
		parentMethod.addFinallyMethod( finallyMethod );
		finallyMethods.add( finallyMethod );

		Debug.print( "  "+finallyMethod.getName()+
			     "["+finallyStartOffsets[i]+"-->"+
			     finallyEndOffsets[i]+"]\n", Debug.CFG, 1 );

		// recursively construct CFGs for all nested finally blocks
		CFGImpl finallyCFG = new CFGImpl ( finallyMethod, handlers );
		Vector inst =
		    removeInstructionsInRange( finallyStartOffsets[i],
					       finallyEndOffsets[i],
					       symInstr );
		finallyCFG.constructCFG( inst, constantPoolMap,
					 handlers, lineNumTable,
					 FINALLY_BLOCK_CFG );
		TryStatement[] tryStmtsInFinally = ((ExceptionHandlersImpl) handlers).
		    removeTryStatementsInRange( finallyStartOffsets[i],
						finallyEndOffsets[i] );
		finallyMethod.addAttribute(
		  new ExceptionHandlersImpl ( finallyMethod, tryStmtsInFinally ) );
		Debug.println( "  Done finally", Debug.CFG, 1 );
		finallyMethod.addAttribute( finallyCFG );
	    }
	}
    }

    /**
     * Constructs CFG for method, and recursively for finally blocks
     * contained in the method.
     * @param symInstr Symbolic instructions.
     * @param constantPoolMap map from constant-pool entries to symbol-table
     *                        objects.
     * @param handlers Exception-handler information.
     * @param lineNumTable Line number table.
     * @param cfgType CFG for a method or a finally block.
     */
    private void constructCFG( Vector symInstr,
			       ConstantPoolMap constantPoolMap,
                               ExceptionHandlers handlers,
                               LineNumberTableAttribute lineNumTable,
                               int cfgType )
    {

	Debug.println( "\nConstructing CFG for "+
			    getMethod().getFullyQualifiedName(), Debug.CFG, 1);

        int firstInstrOffset =
            ((SymbolicInstruction)symInstr.firstElement()).getByteCodeOffset();
        int lastInstrOffset =
            ((SymbolicInstruction)symInstr.lastElement()).getByteCodeOffset();
	this.firstInstructionOffset = firstInstrOffset;
	this.lastInstructionOffset = lastInstrOffset;

	// check if separate CFGs are to be built for finally blocks
	if (!getMethod ().getContainingType ().getProgram ().getOptions ().getInlineFinally ()) {
	    constructCFGforFinally( symInstr, constantPoolMap, handlers,
				    lineNumTable, firstInstrOffset,
				    lastInstrOffset );
	}

        // remove instructions that are targets of jsr instructions, but do
        // not correspond to finally blocks; such instructions appear in the
        // bytecodes for certain synchronized instructions
	updateSymbolicInstructions( symInstr );

        // identify basic blocks in the instructions
        SymbolicInstruction[] instr = new SymbolicInstruction[symInstr.size()];
	int i = 0;
        for (Iterator e = symInstr.iterator (); e.hasNext (); i++)
	{
            instr[i] = (SymbolicInstruction) e.next ();
        }

        int[] basicBlocks =
            BasicBlock.getBlocks( instr, handlers, lineNumTable );


	Debug.println( "  Number of basic blocks = "+basicBlocks.length,
		       Debug.CFG, 1 );


        // create a CFG node for each basic block
        StatementNode[] CFGnodes =
	    getCFGNodesForBasicBlocks( instr, basicBlocks,
				       bytecodeOffsetNodeMap );

        // determine node types and set node attributes
        determineNodeTypes( CFGnodes, handlers );

        // add nodes to the method for this CFG; set containing method for
        // each node
        Method method = getMethod();
        for (i = 0; i<CFGnodes.length; i++ ) {
            CFGnodes[i].setContainingMethod( method );
            addNode( CFGnodes[i] );
        }

        // add an entry node to the CFG
        StatementNode entryNode = new StatementNodeImpl ();
        entryNode.setType( StatementNode.ENTRY_NODE );
        entryNode.setByteCodeOffset( StatementNode.ENTRY_NODE_OFFSET );
	entryNode.setContainingMethod( getMethod() );
        // add finally-entry attribute to entry node if CFG is for a finally
        // block
        if ( cfgType == FINALLY_BLOCK_CFG ) {
            String name =
                ((ExceptionHandlersImpl) handlers).getFinallyNameForCFGStartOffset( firstInstrOffset );
            if ( name == null ) {
		RuntimeException e = new RuntimeException();
		e.printStackTrace();
		throw e;

	    }
            // add finally-entry attribute to entry node
            FinallyEntryAttribute attr = new FinallyEntryAttributeImpl ();
            attr.setName( name );
            entryNode.addAttribute( attr );
        }
        addNode( entryNode );

        // add an exit node to the CFG
        StatementNode exitNode = new StatementNodeImpl ();
        exitNode.setType( StatementNode.EXIT_NODE );
        exitNode.setByteCodeOffset( StatementNode.EXIT_NODE_OFFSET );
	exitNode.setContainingMethod( getMethod() );
        addNode( exitNode );

	// if the CFG is build for a method, set the exitNode as the
	// finally-return-exit node
	if ( cfgType == METHOD_CFG ) {
	    finallyReturnExitNode = exitNode;
	}

        // add edge from entry node to first node
        addEdge( new EdgeImpl ( entryNode, CFGnodes[0] ) );

        // create a separate catch node for each catch handler, and add an
        // edge from the catch node to the first node created for that
        // catch handler
        addCatchHandlerNodes( CFGnodes, constantPoolMap );

        // add edges to the CFG; create return nodes for call nodes and add
        // them to the CFG
        for (i = 0; i < CFGnodes.length; i++ ) {

            int nodeType = CFGnodes[i].getType();

            SymbolicInstruction[] inst = CFGnodes[i].getSymbolicInstructions();
            byte opCode = (byte)inst[inst.length-1].getOpcode();

            // create a return node for a call node; connect call to return,
            // and return to call's successor
            if ( nodeType == StatementNode.STATIC_METHOD_CALL_NODE ||
                 nodeType == StatementNode.VIRTUAL_METHOD_CALL_NODE ||
                 nodeType == StatementNode.NORMAL_FINALLY_CALL_NODE ) {
                CallAttribute callAttr;
                if ( nodeType == StatementNode.NORMAL_FINALLY_CALL_NODE ) {
                    callAttr = Factory.getFinallyCallAttribute (CFGnodes [i]);
                    if ( callAttr == null ) {
			throw new RuntimeException("No finally-call attribute found for call node");
			
		    }
                }
                else {
                    callAttr = Factory.getMethodCallAttribute (CFGnodes [i]);
                    if ( callAttr == null ) {
			throw new RuntimeException("No method call attribute found for call node");
			}
                    // if called method is java.lang.System.exit(), connect
                    // call node to exit node; do not create return node
                    Vector op = inst[inst.length-1].getOperandsVector ();
                    Method m = (Method) op.get (0);
                    if ( m.getName().equals( "exit" ) ) {
                        if ( m.getContainingType().getName().equals(
                             "java/lang/System" ) ) {
                            addEdge( new EdgeImpl ( CFGnodes[i], exitNode ) );
                            CFGnodes[i].setType( StatementNode.HALT_NODE );
                            continue;
                        }
                    }
                }
                StatementNode returnNode = new StatementNodeImpl ();
                returnNode.setByteCodeOffset( CFGnodes[i].getByteCodeOffset() );
                returnNode.setSourceLineNumber(
                    CFGnodes[i].getSourceLineNumber() );
                returnNode.setType( StatementNode.RETURN_NODE );
		returnNode.setContainingMethod( getMethod() );
                addNode( returnNode );
                addEdge( new EdgeImpl ( CFGnodes[i], returnNode ) );
                addEdge( new EdgeImpl ( returnNode, CFGnodes[i+1] ) );
                callAttr.setReturnNode( returnNode );
                continue;
            }

            // set edges for a predicate node
            if ( nodeType == StatementNode.PREDICATE_NODE ) {
                Vector op = inst[inst.length-1].getOperandsVector ();
                if ( opCode == OpCode.OP_TABLESWITCH ) {
                    Integer target = (Integer)op.get (0);
                    StatementNode targetNode =
                        (StatementNode)bytecodeOffsetNodeMap.get( target );
                    addEdge( new EdgeImpl ( CFGnodes[i], targetNode, "default" ) );
		    Iterator e = op.iterator (); e.next (); // skip the 0 element
                    for ( int j=1; j < op.size (); j++ ) {
                        target = (Integer)e.next ();
                        targetNode =
                            (StatementNode)bytecodeOffsetNodeMap.get( target );
                        addEdge( new EdgeImpl ( CFGnodes[i], targetNode,
                                           String.valueOf( j ) ) );
                    }
                    continue;
                }
                else if ( opCode == OpCode.OP_LOOKUPSWITCH ) {
                    Integer target = (Integer)op.get (0);
                    StatementNode targetNode =
                        (StatementNode)bytecodeOffsetNodeMap.get( target );
                    addEdge( new EdgeImpl ( CFGnodes[i], targetNode, "default" ) );
                    for ( int j=1; j<op.size (); j+=2 ) {
                        target = (Integer) op.get (j + 1);
                        targetNode =
                            (StatementNode)bytecodeOffsetNodeMap.get( target );
                        addEdge( new EdgeImpl ( CFGnodes[i], targetNode,
                                           op.get (j).toString() ) );
                    }
                    continue;
                }
                Integer targetOffset = (Integer)op.get (0);
                if ( !bytecodeOffsetNodeMap.containsKey( targetOffset ) ) {
		    RuntimeException e = new RuntimeException();
		    e.printStackTrace();
		    throw e;
		}
                StatementNode targetNode =
                    (StatementNode)bytecodeOffsetNodeMap.get( targetOffset );
                addEdge( new EdgeImpl ( CFGnodes[i], targetNode, "T" ) );
                addEdge( new EdgeImpl ( CFGnodes[i], CFGnodes[i+1], "F" ) );
                continue;
            }

            // set edges for a statement node with unconditional jump
            if ( opCode == OpCode.OP_GOTO || opCode == OpCode.OP_GOTO_W ) {
                Vector op = inst[inst.length-1].getOperandsVector ();
		Integer targetOffset = (Integer) op.get (0);
		if ( cfgType == METHOD_CFG ) {
		    if ( !bytecodeOffsetNodeMap.containsKey( targetOffset ) ) {
			RuntimeException e = new RuntimeException(
                            "Target of Goto not found for method CFG: "+
			    getMethod().getFullyQualifiedName() );
			e.printStackTrace();
			throw e;
		    }
		}
		StatementNode targetNode = (jaba.graph.StatementNode) getGotoSuccessor( targetOffset );
		addEdge( new EdgeImpl ( CFGnodes[i], targetNode ) );
                continue;
            }

	    // set edges for a JSR node; this node occurs only in CFGs
	    // with inlined finally blocks
            if ( opCode == OpCode.OP_JSR || opCode == OpCode.OP_JSR_W ) {
		if (!getMethod ().getContainingType ().getProgram ().getOptions ().getInlineFinally ()) {
		    RuntimeException e = new RuntimeException(
                        "JSR found for non-finally-inlined CFG: "+
			getMethod().getFullyQualifiedName()+"\n"+CFGnodes[i] );
		    e.printStackTrace();
		    throw e;
		}
                Vector op = inst[inst.length-1].getOperandsVector ();
		createOutedgesForJsr( (Integer)op.get (0), CFGnodes[i],
				      CFGnodes[i+1] );
		continue;
	    }

	    // set edges for a RET node; this node occurs only in method CFGs
	    // with inlined finally blocks or CFGs for finally blocks
            if ( opCode == OpCode.OP_RET ) {
		if ( cfgType == FINALLY_BLOCK_CFG ) {
		    addEdge( new EdgeImpl ( CFGnodes[i], exitNode ) );
		    continue;
		}
		// cfg is for a method; a ret instruction should only be
		// encountered for inlined finally CFGs
		if (!getMethod ().getContainingType ().getProgram ().getOptions ().getInlineFinally ()) {
		    RuntimeException e = new RuntimeException(
                        "RET found for non-finally-inlined CFG: "+
			getMethod().getFullyQualifiedName() );
		    e.printStackTrace();
		    throw e;
		}
		// the edge from RET (for a normal return) is set during the
		// processing of the corresponding JSR instruction
		continue;
	    }

            // set edges for return-statement node
            if ( opCode == OpCode.OP_IRETURN || opCode == OpCode.OP_LRETURN ||
                 opCode == OpCode.OP_FRETURN || opCode == OpCode.OP_DRETURN ||
                 opCode == OpCode.OP_ARETURN || opCode == OpCode.OP_RETURN ) {
		// check if the CFG is being created for a method or a
		// finally block
		StatementNode targetNode;
		if ( cfgType == METHOD_CFG ) {
		    targetNode = exitNode;
		}
		else {
		    // create a finally-return-exit node if one is not
		    // already created and create an edge to that node
		    targetNode = (jaba.graph.StatementNode) getFinallyReturnSuccessor ();
		}
		addEdge( new EdgeImpl ( CFGnodes[i], targetNode ) );
		continue;
            }

            // set edges for a throw node; this is done after type-inference
	    // has been performed, which in turn requires local def-use
	    // analysis
            if ( nodeType == StatementNode.THROW_NODE ) {
                continue;
            }

            // default: set edges for s statement node
            addEdge( new EdgeImpl ( CFGnodes[i], CFGnodes[i+1] ) );
        }

        // initialize DU information
        DefUseInitializer duInitializer = new DefUseInitializerImpl ();
	try {
	    duInitializer.initialize( this );
	}
	catch ( RuntimeException re ) {
	    String msg = re.getMessage()+"\n    in method "+
		getMethod().getFullyQualifiedName();
	    re.printStackTrace();
	    throw new RuntimeException( msg );
	}

        // perform type inference
        typeInference.inferTypes();

        // complete CFG construction by connecting each throw node to an
        // appropriate catch, exceptional-finally-call, or exceptional-exit
        // node.
        for (i = 0; i < CFGnodes.length; i++ ) {
            if ( CFGnodes[i].getType() != StatementNode.THROW_NODE ) {
                continue;
            }
            ThrowStatementAttribute attr = Factory.getThrowStatementAttribute (CFGnodes[i]);
            if ( attr == null ) {
		RuntimeException e = new RuntimeException(
                   "No ThrowStatementAttribute for throw node in "+
		   getMethod().getFullyQualifiedName()+"\n"+CFGnodes[i] );
		e.printStackTrace();
		throw e;
	    }
            Class[] exceptionTypes =
                ((ThrowStatementAttribute)attr).getExceptionTypes();
            int offset = CFGnodes[i].getByteCodeOffset();
            for ( int j=0; j<exceptionTypes.length; j++ ) {
                StatementNode succ = (jaba.graph.StatementNode) getCatchNode (exceptionTypes [j], offset);
                Edge newEdge = new EdgeImpl ( CFGnodes[i], succ, 
					 exceptionTypes[j].getName() );
                addEdge( newEdge );
                ThrowEdgeAttribute throwEdgeAttr = new ThrowEdgeAttributeImpl ();
                throwEdgeAttr.setTraversalType( exceptionTypes[j] );
                newEdge.addAttribute( throwEdgeAttr );
		// if succ is a finally-start node, add a finally-context
		// attribute to the edge
		if ( succ.getType() == StatementNode.FINALLY_START_NODE ) {
		    FinallyContextAttribute finallyContextAttr =
			new FinallyContextAttributeImpl ();
		    finallyContextAttr.addExceptionType( exceptionTypes[j] );
		    newEdge.addAttribute( finallyContextAttr );
		}
            }
        }

        // clear symbolic instructions in all CFG nodes
	if (getMethod ().getContainingType ().getProgram ().getOptions ().getRemoveSymbolicInstruction ()) {
	    for (i = 0; i < CFGnodes.length; i++ ) {
		CFGnodes[i].clearSymbolicInstructions();
	    }
	}
    }

    /**
     * Identifies the target node to which an intraprocedural edge
     * or an interprocedural (exceptional-return) edge is created.
     * The source node may be a throw-statement node (intraprocedural edge)
     * or an exceptional-exit node (interprocedural edge);
     * The type of the target node may be CATCH_NODE,
     * an EXCEPTIONAL_FINALLY_CALL_NODE,
     * or an EXCEPTIONAL_EXIT_NODE.
     * @param exceptionType type of exception raised at a throw statement or
     *                      a PNRC.
     * @param offset bytecode offset of the throw-statement node or the call
     *               node for the PNRC.
     * @return The target node to which the source node is to be connected.
     */
    public StatementNode getCatchNode (Class exceptionType, int offset)
    {
        Class[] enclHandlers =
	    ((ExceptionHandlersImpl) handlers).getEnclosingHandlers( offset, firstInstructionOffset,
					   lastInstructionOffset );
        int[] enclHandlerOffset =
	    ((ExceptionHandlersImpl) handlers).getEnclosingHandlerOffsets( offset,
						 firstInstructionOffset,
						 lastInstructionOffset );
        if ( enclHandlers.length != enclHandlerOffset.length ) {
	    RuntimeException e = new RuntimeException();
	    e.printStackTrace();
	    throw e;
	}

	if (!getMethod ().getContainingType ().getProgram ().getOptions ().getInlineFinally ()) {
	    return getCatchNodeForNoninlinedFinally( exceptionType, offset,
						     enclHandlers,
						     enclHandlerOffset );
	}
	else {
	    return getCatchNodeForInlinedFinally( exceptionType, enclHandlers,
						  enclHandlerOffset );
	}
    }

    /** ??? */
    private StatementNode getCatchNodeForNoninlinedFinally(
        Class exceptionType, int offset, Class[] enclHandlers,
	int[] enclHandlerOffset )
    {
        StatementNode mre = null;
        StatementNode retNode = null;
        String exceptionName = exceptionType.getName();
        for ( int i=0; i<enclHandlers.length; i++ ) {

            if ( enclHandlers[i] != null ) {  // catch handler
                String handlerName = enclHandlers[i].getName();
                if ( enclHandlers[i].isAssignableFrom ((jaba.sym.Class) exceptionType ) ) {
                    StatementNode matchingHandlerNode =
                        (StatementNode)catchOffsetNodeMap.get(
                        new Integer( enclHandlerOffset[i] ) );
                    if ( matchingHandlerNode == null ) {
                        // this happens when a throw statement is inside a
                        // finally and the catch handler is outside the
                        // finally
                        continue;
                    }
                    if ( retNode == null ) {
                        return matchingHandlerNode;
                    }
                    else {
                        addEdge( new EdgeImpl ( mre, matchingHandlerNode ) );
                        return retNode;
                    }
                }
                continue;
            }
            else {  // enclosing handler is a finally block
                String enclFinallyName = ((ExceptionHandlersImpl) handlers).getFinallyNameForJumpOffset(
                    enclHandlerOffset[i] );
                Vector finallyCallNodes = (Vector)
                    exceptionalFinallyCallNodes.get( exceptionName );
                StatementNode fCallNode;
                if ( finallyCallNodes != null )
		{
                    for (Iterator e = finallyCallNodes.iterator (); e.hasNext ();)
		    {
                        fCallNode = (StatementNode) e.next ();
                        FinallyCallAttribute attr = Factory.getFinallyCallAttribute (fCallNode);
                        if ( attr == null )
			{
			    RuntimeException re = new RuntimeException();
			    re.printStackTrace();
			    throw re;
			}
                        String callFinallyName = attr.getName();
                        if ( enclFinallyName.equals( callFinallyName ) )
			{
                            if ( retNode == null )
			    {
                                return fCallNode;
                            }
                            else
			    {
                                addEdge( new EdgeImpl ( mre, fCallNode ) );
                                return retNode;
                            }
                        }
                    }
                }
                // control reaches here if no finally-call node for the
                // given exception type and finally-block name has been
                // created; create a new finally-call and return node;
                // add them to the CFG, and the finally-call-node map;
                // update mre to point to the newly-created return node

                // create finally-call node
                fCallNode = new StatementNodeImpl ();
                fCallNode.setType( StatementNode.
				   EXCEPTIONAL_FINALLY_CALL_NODE );
		fCallNode.setContainingMethod( getMethod() );
		fCallNode.setByteCodeOffset( offset );

                // create return node
                StatementNode fRetNode = new StatementNodeImpl ();
                fRetNode.setType( StatementNode.RETURN_NODE );
		fRetNode.setContainingMethod( getMethod() );

                // add finally-call attribute to finally-call node
                FinallyCallAttribute attr = new FinallyCallAttributeImpl ();
                attr.setName( enclFinallyName );
                attr.setReturnNode( fRetNode );

                for (Iterator j = finallyMethods.iterator (); j.hasNext ();)
		{
			Method m = (Method) j.next ();

                    if (enclFinallyName.equals (m.getName ()))
		    {
                        attr.setMethod (m);
                        break;
                    }
                }
                fCallNode.addAttribute( attr );

                // add finally-call node to list of finally-call nodes in the
                // method
                getMethod().addFinallyCallSiteNode( fCallNode );

                // add call, return nodes to the CFG; connect them with an
                // edge
                addNode( fCallNode );
                addNode( fRetNode );
                addEdge( new EdgeImpl ( fCallNode, fRetNode ) );

                // add finally-call node to map
                if ( finallyCallNodes == null ) {
                    finallyCallNodes = new Vector();
                    exceptionalFinallyCallNodes.put( exceptionName,
                        finallyCallNodes );
                }
                finallyCallNodes.addElement( fCallNode );

                // update retNode and mre
                if ( retNode == null ) {
                    retNode = fCallNode;
                }
                if ( mre != null ) {
                    addEdge( new EdgeImpl ( mre, fCallNode ) );
                }
                mre = fRetNode;
            }
        }
        // create an exceptional-exit node for this type if required;
        // add that node to the CFG, and exception-exit map
        StatementNode eExitNode = (StatementNode)
            exceptionExitNodes.get( exceptionName );
        if ( eExitNode == null ) {
            // create exceptional-exit node
            eExitNode = new StatementNodeImpl ();
            eExitNode.setType( StatementNode.EXCEPTIONAL_EXIT_NODE );
            eExitNode.setByteCodeOffset(
                StatementNode.EXCEPTIONAL_EXIT_NODE_OFFSET );
	    eExitNode.setContainingMethod( getMethod() );

            // add exception attribute to exceptional-exit node
            ExceptionAttribute attr = new ExceptionAttributeImpl ();
            attr.setExceptionType ((jaba.sym.Class) exceptionType );
            eExitNode.addAttribute( attr );

            // add exceptional-exit node to CFG and exceptional-exit map
            addNode( eExitNode );
            exceptionExitNodes.put( exceptionName, eExitNode );
        }
        if ( retNode == null ) {
            return eExitNode;
        }
        else {
            addEdge( new EdgeImpl ( mre, eExitNode ) );
        }
        return retNode;
    }

    /** ??? */
    private StatementNode getCatchNodeForInlinedFinally(
        Class exceptionType, Class[] enclHandlers, int[] enclHandlerOffset )
    {
        StatementNode mre = null;
        StatementNode retNode = null;
        String exceptionName = exceptionType.getName();
        for ( int i=0; i<enclHandlers.length; i++ ) {

            if ( enclHandlers[i] != null ) {  // catch handler
                String handlerName = enclHandlers[i].getName();
                if ( enclHandlers[i].isAssignableFrom ((jaba.sym.Class) exceptionType ) ) {
                    StatementNode matchingHandlerNode =
                        (StatementNode)catchOffsetNodeMap.get(
                        new Integer( enclHandlerOffset[i] ) );
                    if ( matchingHandlerNode == null ) {
                        RuntimeException e = new RuntimeException(
                            "Catch node not found for catch offset "+
			    enclHandlerOffset[i]+" for method "+
			    getMethod().getFullyQualifiedName() );
			e.printStackTrace();
			throw e;
                    }
                    if ( retNode == null ) {
                        return matchingHandlerNode;
                    }
                    else {
			if ( mre == null ) {
			    // retNode != null && mre == null ==> found a
			    // finally block with no FINALLY_END_NODE
			    // (unconditional break/continue/return/throw):
			    // return retNode as the path will not be extended
			    // along enclosing handlers from the unconditional
			    // break/continue/return/throw
			    return retNode;
			}
                        addFinallyContextEdge( mre, matchingHandlerNode,
					       (jaba.sym.Class) exceptionType );
                        return retNode;
                    }
                }
                continue;
            }
            else {  // enclosing handler is a finally block
                String enclFinallyName = ((ExceptionHandlersImpl) handlers).getFinallyNameForJumpOffset(
                    enclHandlerOffset[i] );
		// get start node for finally
		StatementNode finallyStartNode;
		if ( finallyNameToStartNodeMap.
		     containsKey( enclFinallyName ) ) {
		    finallyStartNode = (StatementNode)
			finallyNameToStartNodeMap.get( enclFinallyName );
		}
		else {
		    // this occurs only if there is no normal exit from the
		    // the try block; so a jsr for normal execution of the
		    // finally does not occur and no finally start node has
		    // been created; create a new finally start node and add it
		    // to finallyNameToStartNodeMap
		    finallyStartNode = new StatementNodeImpl ();
		    finallyStartNode.setType( StatementNode.
					      FINALLY_START_NODE );
		    finallyStartNode.setByteCodeOffset( enclHandlerOffset[i] );
		    finallyStartNode.setContainingMethod( getMethod() );

		    // add finally-entry attribute to the start node
		    FinallyEntryAttribute attr = new FinallyEntryAttributeImpl ();
		    attr.setName( enclFinallyName );
		    finallyStartNode.addAttribute( attr );
		    finallyNameToStartNodeMap.put( enclFinallyName,
						   finallyStartNode );
		}
		if ( retNode == null ) {
		    retNode = finallyStartNode;
		}
		if ( mre != null ) {
		    addFinallyContextEdge( mre, finallyStartNode,
					   (jaba.sym.Class) exceptionType );
		}
		mre = (StatementNode)
		    finallyNameToEndNodeMap.get( enclFinallyName );
		// mre can be null at this point: this occurs if the
		// enclosing finally contains an unconditional
		// break/continue/return/throw
	    }
	}

        // create an exceptional-exit node for this type if required;
        // add that node to the CFG, and exception-exit map
        StatementNode eExitNode = (StatementNode)
            exceptionExitNodes.get( exceptionName );
        if ( eExitNode == null ) {
            // create exceptional-exit node
            eExitNode = new StatementNodeImpl ();
            eExitNode.setType( StatementNode.EXCEPTIONAL_EXIT_NODE );
            eExitNode.setByteCodeOffset(
                StatementNode.EXCEPTIONAL_EXIT_NODE_OFFSET );
	    eExitNode.setContainingMethod( getMethod() );

            // add exception attribute to exceptional-exit node
            ExceptionAttribute attr = new ExceptionAttributeImpl ();
            attr.setExceptionType ((jaba.sym.Class) exceptionType);
            eExitNode.addAttribute( attr );

            // add exceptional-exit node to CFG and exceptional-exit map
            addNode( eExitNode );
            exceptionExitNodes.put( exceptionName, eExitNode );
        }
        if ( retNode == null ) {
            return eExitNode;
        }
        else {
	    // mre can be null at this point: this occurs if some enclosing
	    // finally contains an unconditional break/continue/return/throw
	    if ( mre != null ) {
		addFinallyContextEdge (mre, eExitNode, (jaba.sym.Class) exceptionType);
	    }
	    
        }
        return retNode;
    }

    /**
     * Removes instructions that fall in the given range of offsets, and
     * returns a vector of the removed instructions.
     * @param startOffset start offset of the range of instructions to be
     *                    removed.
     * @param endOffset end offset of the range of instructions to be
     *                    removed.
     * @param v vector of symbolic instructions.
     * @return Vector of removed instructions.
     */
    private Vector removeInstructionsInRange( int startOffset, int endOffset,
                                              Vector v )
    {
        Vector r = new Vector();
        int num = v.size();
        int startIndex=0;
        int offset=0;
        SymbolicInstruction inst;
	int i = 0;
        for (Iterator e = v.iterator (); e.hasNext (); i++ )
	{
            inst = (SymbolicInstruction) e.next ();
            offset = inst.getByteCodeOffset ();
            if ( offset == startOffset )
	    {
                startIndex = i;
                break;
            }
        }
        if ( startIndex == 0 ) {
	    RuntimeException e = new RuntimeException();
	    e.printStackTrace();
	    throw e;
	}
        while ( offset != endOffset ) {
            r.addElement( v.remove( startIndex ) );
            inst = (SymbolicInstruction)v.elementAt( startIndex );
            offset = inst.getByteCodeOffset();
        }
        r.addElement( v.remove( startIndex ) );
        return r;
    }

    /**
     * Constructs a CFG node for each basic block. Determines the node type,
     * and adds appropriate attributes for the nodes.
     * @param instructions array of symbolic instructions.
     * @param basicBlocks offsets of basic blocks in <code>instructions</code>.
     * @param nodeMap mapping from a bytecode offset to the CFG node for that
     *                bytecode. The HashMap is initially empty, and is filled
     *                by <code>getCFGNodesForBasicBlocks()</code>.
     * @return Array of CFG nodes for the basic blocks in
     *         <code>instructions</code>.
     */
    private StatementNode[] getCFGNodesForBasicBlocks( SymbolicInstruction[]
						       instructions,
						       int[] basicBlocks,
						       HashMap nodeMap )
    {
        int numNodes=0;
        for ( int i=0; i<basicBlocks.length; i++ ) {
            if ( basicBlocks[i] > 0 ) {
                numNodes++;
            }
        }

        StatementNode[] CFGNodes = new StatementNode[numNodes];
        int secondNodeStart;
        StatementNode node;
        boolean firstBlock = false;
        node = new StatementNodeImpl ();
        node.setByteCodeOffset( instructions[0].getByteCodeOffset() );
	node.setContainingMethod( getMethod() );
        for ( secondNodeStart=0; secondNodeStart<basicBlocks.length;
              secondNodeStart++ ) {
            if ( basicBlocks[secondNodeStart] > 0 && firstBlock ) {
                break;
            }
            node.addSymbolicInstruction( instructions[secondNodeStart] );
            if ( basicBlocks[secondNodeStart] > 0 ) {
                firstBlock = true;
                node.setSourceLineNumber( basicBlocks[secondNodeStart] );
            }
        }
        nodeMap.put( new Integer( node.getByteCodeOffset() ), node );
        CFGNodes[0] = node;

        int i, j;

        for ( i=1, j=secondNodeStart; i<numNodes; i++ ) {
            node = new StatementNodeImpl ();
            node.addSymbolicInstruction( instructions[j] );
            node.setByteCodeOffset( instructions[j].getByteCodeOffset() );
            node.setSourceLineNumber( basicBlocks[j] );
	    node.setContainingMethod( getMethod() );
            j++;
	    while ( ( j < basicBlocks.length ) && ( basicBlocks[j] == 0 ) ) {
                node.addSymbolicInstruction( instructions[j] );
                j++;
            }
            nodeMap.put( new Integer( node.getByteCodeOffset() ), node );
            CFGNodes[i] = node;
        }
        return CFGNodes;
    }

    /**
     * Determines type of each node, and adds node attributes that may be
     * required for a node.
     * @param CFGNodes array of CFG nodes.
     */
    private void determineNodeTypes( StatementNode[] CFGNodes,
                                     ExceptionHandlers handlers )
    {

        // opcodes that determine a predicate node
        int[] predicateNodeOpcodes = {
            OpCode.OP_IFEQ, OpCode.OP_IFNE, OpCode.OP_IFLT, OpCode.OP_IFGE,
            OpCode.OP_IFGT, OpCode.OP_IFLE,
            OpCode.OP_IF_ICMPEQ, OpCode.OP_IF_ICMPNE, OpCode.OP_IF_ICMPLT,
            OpCode.OP_IF_ICMPGE, OpCode.OP_IF_ICMPGT, OpCode.OP_IF_ICMPLE,
            OpCode.OP_IF_ACMPEQ, OpCode.OP_IF_ACMPNE,
            OpCode.OP_TABLESWITCH,
            OpCode.OP_LOOKUPSWITCH,
            OpCode.OP_IFNULL, OpCode.OP_IFNONNULL
        };

        SymbolicInstruction[] instr;
        boolean match;

        CFGNodesLoop:  // set label for loop to enable multi-level continue
        for ( int i=0; i<CFGNodes.length; i++ ) {

            instr = CFGNodes[i].getSymbolicInstructions();

            byte opCode = (byte)instr[instr.length-1].getOpcode();

            // check if predicate node
            for ( int j=0; j<predicateNodeOpcodes.length; j++ ) {
                if ( opCode == predicateNodeOpcodes[j] ) {
                    CFGNodes[i].setType( StatementNode.PREDICATE_NODE );
                    continue CFGNodesLoop;
                }
            }

            // check if throw node
            if ( opCode == OpCode.OP_ATHROW ) {
                CFGNodes[i].setType( StatementNode.THROW_NODE );
		if ( i == 0 ) {
		    addThrowStatementAttribute( CFGNodes[i], null );
		}
		else {
		    addThrowStatementAttribute( CFGNodes[i], CFGNodes[i-1] );
		}
                continue;
            }

            // check if constructor-call or method-call node
            if ( opCode == OpCode.OP_INVOKEVIRTUAL ||
                 opCode == OpCode.OP_INVOKESTATIC ||
                 opCode == OpCode.OP_INVOKEINTERFACE ||
                 opCode == OpCode.OP_INVOKESPECIAL ) {
                if ( opCode == OpCode.OP_INVOKESTATIC ) {
                    CFGNodes[i].setType(
                        StatementNode.STATIC_METHOD_CALL_NODE );
		}
		else {
		    CFGNodes[i].setType(
			StatementNode.VIRTUAL_METHOD_CALL_NODE );
		}
		addMethodCallAttribute( CFGNodes[i] );
                continue;
            }

            // check if class-instantiation node
            if ( opCode == OpCode.OP_NEW ) {
                CFGNodes[i].setType( StatementNode.CLASS_INSTANTIATION_NODE );
                addNewInstanceAttribute( CFGNodes[i] );
                continue;
            }

            // check if array-instantiation node */
            if ( opCode == OpCode.OP_NEWARRAY ||
                 opCode == OpCode.OP_ANEWARRAY ||
                 opCode == OpCode.OP_MULTIANEWARRAY ) {
                CFGNodes[i].setType( StatementNode.ARRAY_INSTANTIATION_NODE );
                addNewInstanceAttribute( CFGNodes[i] );
                continue;
            }

            // check if finally-call node
            if ( opCode == OpCode.OP_JSR || opCode == OpCode.OP_JSR_W ) {
		if (!getMethod ().getContainingType ().getProgram ().getOptions ().getInlineFinally ()) {
		    CFGNodes[i].setType( StatementNode.
					 NORMAL_FINALLY_CALL_NODE );
		    addFinallyCallAttribute( CFGNodes[i], handlers );
		    // add node to method's finally call sites
		    getMethod().addFinallyCallSiteNode( CFGNodes[i] );
		}
		else {
		    CFGNodes[i].setType( StatementNode.STATEMENT_NODE );
		}
                continue;
            }

            // check if this is a return node
            if ( opCode == OpCode.OP_IRETURN || opCode == OpCode.OP_LRETURN ||
		 opCode == OpCode.OP_FRETURN || opCode == OpCode.OP_DRETURN ||
		 opCode == OpCode.OP_ARETURN || opCode == OpCode.OP_RETURN ) {
                 CFGNodes[i].setType( StatementNode.RETURN_STATEMENT_NODE );
                 continue;
	    }

            // if none of the above, node is a statement node
            CFGNodes[i].setType( StatementNode.STATEMENT_NODE );
        }

    }

    /**
     * Adds a catch node for each handler in the method. The catch node
     * is connected to the first node for that catch handler.
     * @param CFGnodes array of CFG nodes for the method.
     * @param constantPoolMap map from constant-pool entries to symbol-table
     *                        objects.
     */
    private void addCatchHandlerNodes( Node[] CFGnodes,
                                       ConstantPoolMap constantPoolMap )
    {
        if ( handlers == null ) { // no exception-table for method
            return;
        }
        catchOffsetNodeMap = new HashMap();
        for ( int i=0; i<CFGnodes.length; i++ ) {
            int offset = ((StatementNode)CFGnodes[i]).getByteCodeOffset();
            Class catchType;
            if ( (catchType = ((ExceptionHandlersImpl) handlers).getCatchHandlerType( offset )) != null ) {
                StatementNode catchNode = new StatementNodeImpl ();
                catchNode.setType( StatementNode.CATCH_NODE );
                catchNode.setByteCodeOffset( offset );
                catchNode.setSourceLineNumber(
                    ((StatementNode)CFGnodes[i]).getSourceLineNumber() );
		catchNode.setContainingMethod( getMethod() );
                addExceptionAttribute( catchNode, catchType );
                addNode( catchNode );
                addEdge( new EdgeImpl ( catchNode, CFGnodes[i] ) );
                catchOffsetNodeMap.put( new Integer( offset ), catchNode );
            }
        }
    }

    /**
     * Adds an exception attribute to a catch-handler node.
     * @param node CFG node to add attribute to.
     * @param type type of exception.
     */
    private void addExceptionAttribute( StatementNode node, Class type )
    {
        ExceptionAttribute attr = new ExceptionAttributeImpl ();
        attr.setExceptionType( type );
        node.addAttribute( attr );
    }

    /**
     * Adds a throw-statement attribute to a throw node.
     * @param node CFG node to add attribute to.
     * @see jaba.graph.ThrowStatementAttribute
     */
    private void addThrowStatementAttribute( StatementNode node,
                                             StatementNode pred ) {
        // a throw statement may take one of three forms (the exception type
        // is determined based on the form:
        // 1. throw e - declared type of local var e; e is the first operand
        //              of the aload instruction that preceeds athrow in node
        //              (invariant: node contains two symbolic instructions -
        //               aload and athrow)
        // 2. throw new E() - object type of new-instance attribute of pred
        //                    (invariant: node contains one symbolic
        //                     instruction - athrow)
        // 3. throw m() - return type of method called in pred
        //                (invariant: node contains one symbolic
        //                 instruction - athrow)
        SymbolicInstruction[] instr = node.getSymbolicInstructions();
        ThrowStatementAttribute attr = new ThrowStatementAttributeImpl ();
        if ( instr.length == 1 ) {
	    if ( pred == null ) {
		RuntimeException e = new RuntimeException();
		e.printStackTrace();
		throw e;
	    }
	    int predNodeType = pred.getType();
            if ( predNodeType != StatementNode.STATIC_METHOD_CALL_NODE &&
                 predNodeType != StatementNode.VIRTUAL_METHOD_CALL_NODE ) {
		RuntimeException e = new RuntimeException();
		e.printStackTrace();
		throw e;
	    }
            MethodCallAttribute callAttr = Factory.getMethodCallAttribute (pred);
            Vector m = callAttr.getMethodsVector ();
		Method m0 = (Method) m.get (0);
            if ( m0.getName().equals( "<init>" ) ) {
                attr.setExceptionType( (Class)m0.getContainingType() );
                attr.setExpressionType(
                    ThrowStatementAttribute.NEW_INSTANCE_THROW_EXPR );
            }
            else {
                attr.setExceptionType( (Class)m0.getType() );
                attr.setExpressionType(
                    ThrowStatementAttribute.METHOD_CALL_THROW_EXPR );
            }
        }
        else {
            attr.setExpressionType( ThrowStatementAttribute.VAR_THROW_EXPR );
        }
        node.addAttribute( attr );
    }

    /**
     * Adds a method-call attribute to a CFG node.
     * @param node CFG node to add attribute to.
     */
    private void addMethodCallAttribute( StatementNode node ) {
        SymbolicInstruction [] instr = node.getSymbolicInstructions();
        Vector operands = instr [instr.length - 1].getOperandsVector ();
        MethodCallAttribute attr = new MethodCallAttributeImpl ();
        Method m = (Method)operands.get (0);
        attr.addMethod( m );

        node.addAttribute( attr );
        // add actual parameters to call attribute
    }

    /**
     * Returns the set of methods that over-ride a given method in a given
     * class. This set is the conservative estimate of the methods that may be
     * invoked at a polymorphic call site.
     * @param c class in which given method is defined.
     * @param mSig signature of given method.
     * @param Vector containing methods that over-ride the given method.
     */
    private void getInvokableMethods( Class c, String mSig, Vector methods ) {
        Collection subClasses = c.getAllSubClassesCollection ();
        for (Iterator i = subClasses.iterator (); i.hasNext ();)
	{
            Method m = ((Class) i.next ()).getMethod (mSig);
            if ( m != null ) {
                methods.addElement( m );
            }
        }
    }

    /**
     * Returns the set of methods that implement a given method declaration in
     * a given interface. This set is the conservative estimate of the methods
     * that may be invoked at a polymorphic call site.
     * @param ifc interface in which given method is declared.
     * @param mSig signature of given method.
     * @return Array of methods that implement the given method.
     */
    private void getInvokableMethods( Interface ifc, String mSig,
                                      Vector methods ) {
        Collection implementingClasses = ifc.getAllImplementingClassesCollection ();
        for (Iterator i = implementingClasses.iterator (); i.hasNext ();)
	{
            Method m = ((Class) i.next ()).getMethod (mSig);
            if ( m != null ) {
                methods.addElement( m );
            }
        }
    }

    /**
     * Adds a finally-call attribute to a CFG node.
     * @param node CFG node to add attribute to.
     */
    private void addFinallyCallAttribute( StatementNode node,
                                          ExceptionHandlers handlers )
    {
        SymbolicInstruction [] instr = node.getSymbolicInstructions();
        Vector operands = instr [instr.length - 1].getOperandsVector ();
        int entryOffset = ((Integer)operands.get (0)).intValue();
        String name = ((ExceptionHandlersImpl) handlers).getFinallyNameForEntryOffset( entryOffset );
        if ( name == null ) {
            // jsr must be for monitor release; reset node type to a 
            // statement node
            node.setType( StatementNode.STATEMENT_NODE );
            return;
        }
        FinallyCallAttribute attr = new FinallyCallAttributeImpl ();
        node.addAttribute( attr );
        attr.setName( name );

        // locate method object for the finally block that is called
        for (Iterator i = finallyMethods.iterator (); i.hasNext ();)
	{
		Method m = (Method) i.next ();

            if (name.equals (m.getName()))
	    {
                attr.setMethod (m);
                return;
            }
        }
    }

    /**
     * Adds a new-instance attribute to a CFG node.
     * @param node CFG node to add attribute to.
     */
    private void addNewInstanceAttribute( StatementNode node ) {
        SymbolicInstruction [] instr = node.getSymbolicInstructions();
        Vector operands = instr [instr.length - 1].getOperandsVector ();
        NewInstanceAttribute attr = new NewInstanceAttributeImpl ();
	Object o = operands.get (0);
        // set object type for a class-instantiation node
        if ( o instanceof Class ) {
            attr.setObjectType( (Class) o );
        }
        // set object type for an array-instantiation node
        else if ( o instanceof Array ) {
            attr.setObjectType( (Array) o );
        }
        else {
	    RuntimeException e = new RuntimeException(
                "Unknown instance type of " + o );
	    e.printStackTrace();
	    throw e;
	}
        node.addAttribute( attr );
    }

    /**
     * Returns the type-inference object for this CFG.
     * @return Type-inference object for this CFG.
     */
    public TypeInference getTypeInference() {
        return typeInference;
    }

    /**
     * Removes those instructions from the array of symbolic instructions
     * that are targets of jsr instructions, but do not correspond to
     * finally blocks. Such instructions are created for certain synchronized
     * instructions - to ensure monitor release.
     * @param symInstr vector of symbolic instructions that is updated.
     */
    private void updateSymbolicInstructions( Vector symInstr ) {
        SymbolicInstruction instr;
        int symSize = symInstr.size();
        Vector jsrInstr = new Vector();
        for (Iterator e = symInstr.iterator (); e.hasNext ();)
	{
            instr = (SymbolicInstruction) e.next ();
            byte opCode = (byte)instr.getOpcode();
            if ( opCode == OpCode.OP_JSR || opCode == OpCode.OP_JSR_W )
	    {
                jsrInstr.addElement( instr );
            }
        }
        int jsrSize = jsrInstr.size();
        for ( int i=0; i<jsrSize; i++ )
	{
            instr = (SymbolicInstruction)jsrInstr.elementAt( i );
            Vector operands = instr.getOperandsVector ();
            int target = ((Integer)operands.get (0)).intValue();
            symSize = symInstr.size();
	    int j = 0;
            for (Iterator e1 = symInstr.iterator (); e1.hasNext (); j++)
	    {
                if (((SymbolicInstruction) e1.next ()).getByteCodeOffset () == target)
		{
                    break;
                }
            }
            if ( j == symSize ) {  // target of jsr is a finally block
                continue;
            }
            int start = j, end = 0;
            // remove instructions from symInstr until and including a ret or
            // a wide (that encapsulates a ret) is encountered
            for ( ; j<symSize; j++ ) {
                instr = (SymbolicInstruction)symInstr.elementAt( j );
                if ( instr instanceof jaba.instruction.Ret ) {
                    end = j;
                    break;
                }
                if ( instr instanceof jaba.instruction.Wide ) {
                    byte op = (byte)((jaba.instruction.Wide)instr).
                        getEncapsulatedOpcode();
                    if ( op == OpCode.OP_RET ) {
                        end = j;
                        break;
                    }
                }
            }
            // j must be less than sym size
            if ( end == 0 ) {
		RuntimeException e = 
		    new RuntimeException("no ret instruction found after " +
					 "target of jump " + target);
		e.printStackTrace();
		throw e;
	    }
            for ( int count=0; count<(end-start+1); count++ ) {
                symInstr.removeElementAt( start );
            }
        }
    }

    /**
     * Constructs the CFG for the corresponding method.
     * <p>
     * <code>load()</code> first queries the corresponding method object,
     * to obtain
     * information required during the CFG construction. This includes the
     * array of bytecodes that denote the statements in the method. Using
     * the bytecode array and the instruction-loading visitor,
     * <code>load()</code> builds an array of
     * <link>SymbolicInstruction</link>s.
     *
     * <code>load()</code> then recursively
     * constructs the CFGs for all finally blocks in the method, and after that
     * constructs the CFG for the method. To construct each CFG,
     * <code>load()</code>
     * <UL>
     *   <LI> first, constructs a partial CFG, in which nodes corresponding to
     *        throw statements have no outgoing edges. To construct a partial
     *        CFG, <code>load()</code>
     *   <UL>
     *     <LI> identifies the range of symbolic instruction that correspond to
     *          a CFG
     *     <LI> partitions those symbolic instructions into basic blocks
     *          using the source line numbers, and sources and targets of jump
     *          statements
     *     <LI> creates a CFG node for each basic block, and depending on the
     *          node's type, adds attrbites to the CFG nodes
     *     <LI> adds edges among the CFG nodes (throw nodes have no outgoing
     *          edges)
     *   </UL>
     *   <LI> then, gathers definition and use information for each CFG node
     *        by iterating the DefUse-gathering visitor over the partial CFG
     *   <LI> then, performs type inference to determine potential exception
     *        types for each throw statement
     *   <LI> finally, creates outgoing edges from throw nodes, one for each
     *        exception type, creates finally call and return nodes for each
     *        exceptional context, and creates exceptional-exit nodes for
     *        for those exception types that are propagated by the method.
     * </UL>
     *
     * @param method The method for which the CFG has to be loaded.
     * @return The CFG for that method.
     */
    static public MethodAttribute load(Method method) {
	if ( method == null ) {
	    RuntimeException e = new RuntimeException();
	    e.printStackTrace();
	    throw e;
	}

	Debug.println( "\nLoading CFG for "+
			    method.getFullyQualifiedName(), Debug.CFG, 1);

	MethodAttribute[] attributes = method.getAttributes();
	int numAttr = attributes.length;
	for ( int i=0; i<numAttr; i++ ) {
	    if ( attributes[ i ] instanceof jaba.graph.cfg.CFG ) {
		return (MethodAttribute)attributes[ i ];
	    }
	}
	
	// attribute not found... create it and initialize it.
	CFGImpl cfg = new CFGImpl (method);
	return loadCFG( method, cfg );
    }

    /** ??? */
    private static MethodAttribute loadCFG( Method method, CFG cfgParam )
    {
	CFGImpl cfg = (CFGImpl) cfgParam;

        // get constant-pool map from method's containing type
        ConstantPoolMap constantPoolMap = (ConstantPoolMap) ((jaba.sym.NamedReferenceType) method.getContainingType ()).getAttributeOfType ("jaba.sym.ConstantPoolMap");
        if ( constantPoolMap == null ) {
	    RuntimeException e = new RuntimeException(
                "No ConstantPoolMap for method " + 
		method.getFullyQualifiedName() );
	    //e.printStackTrace();  // just return it to the caller and let them print it.
	    throw e;
	}

        // get local-var map from method, and remove that attrobute
        LocalVariableMap localVarMap = (LocalVariableMap) method.getAttributeOfType ("jaba.sym.LocalVariableMap");
        if ( localVarMap == null ) {
	    RuntimeException e = new RuntimeException(
                "No local variable map for method "+
		method.getFullyQualifiedName() );
	    e.printStackTrace();
	    throw e;
	}
        method.removeAttributeOfType( "jaba.sym.LocalVariableMap" );

        // get method info attribute from method to access other information,
        // and remove that attribute
        MethodInfo methodInfo = (MethodInfo) method.getAttributeOfType ("jaba.classfile.MethodInfo");
        if ( methodInfo == null ) {
	    RuntimeException e = new RuntimeException(
                "No jaba.classfile.MethodInfo attribute for method "+
		method.getFullyQualifiedName() );
	    e.printStackTrace();
	    throw e;
	}
        method.removeAttributeOfType( "jaba.classfile.MethodInfo" );

        // get code attribute from method info
        CodeAttribute codeAttr = null;
        int mac = methodInfo.getAttributeCount();
        AttributeInfo ai;
        for ( int i=0; i<mac; i++ ) {
            ai = methodInfo.getAttribute( i );
            if ( ai instanceof CodeAttribute ) {
                codeAttr = (CodeAttribute)ai;
                break;
            }
        }
        if ( codeAttr == null ) { // for abstract methods
	    method.addAttribute( (MethodAttribute)cfg );
	    return (MethodAttribute)cfg;
        }

        // get bytecode array and exception table from code attribute
        byte[] bytecode = codeAttr.getCode();
        int ehLen = codeAttr.getExceptionLength();
        ExceptionHandler[] exceptionTable = new ExceptionHandler[ehLen];
        for ( int i=0; i<ehLen; i++ ) {
            exceptionTable[i] = codeAttr.getExceptionHandler( i );
        }

        // get local-var table and line-number table from code attribute
        int ac = codeAttr.getAttributeCount();
        LocalVariableTableAttribute localVarTable = null;
        LineNumberTableAttribute lineNumTable = null;
        for ( int i=0; i<ac; i++ ) {
            ai = codeAttr.getAttribute( i );
            if ( ai instanceof LocalVariableTableAttribute ) {
                localVarTable = (LocalVariableTableAttribute)ai;
            }
            if ( ai instanceof LineNumberTableAttribute ) {
                lineNumTable = (LineNumberTableAttribute)ai;
            }
        }
	if ( lineNumTable == null ) {
	    RuntimeException e = 
		new RuntimeException( "LineNumberTable is null for " + 
				      method.getFullyQualifiedName() );
	    e.printStackTrace();
	    throw e;
	}

        // encapsulate exception table to utilize accessor methods
        //ExceptionHandlers handlers = null;
        try {
	    //if ( exceptionTable.length > 0 ) {
	    cfg.handlers = new ExceptionHandlersImpl ( method, exceptionTable,
						  bytecode, constantPoolMap );
	    //method.addAttribute( cfg.handlers );
		//}
        }
	catch ( Exception e ) {
	    String msg = "Exception encountered for method "+
		method.getFullyQualifiedName()+"\n"+e.getMessage();
	    throw new RuntimeException( msg );
	}
	// check if finally blocks with unconditional break/continue/return/
	// throw were identified during parsing of exception handlers;
	// if so, create CFG with inlined finally blocks for this method
	if ( ((ExceptionHandlersImpl) cfg.handlers).inlineFinally ) {
	    if (!cfg.getMethod ().getContainingType ().getProgram ().getOptions ().getInlineFinally ()) {
		RuntimeException e = new RuntimeException(
                    "\nUnconditional break/continue/return/throw found in a"+
		    " finally block in\n"+method.getFullyQualifiedName()+"\n"+
		    "Please build CFGs with inlined finally blocks:\n"+
		    "    set the inlineFinally Option to true...\n" );
		e.printStackTrace();
		throw e;
	    }
	}

        // update bytecode array to reset instructions that:
        // - are related to exceptional invocation of finally blocks
        // - save the return address (at the beginning of a finally block)
	((ExceptionHandlersImpl) cfg.handlers).updateBytecodeArray( bytecode );

        // instantiate instruction-loading visitor, and iterate over the
        // bytecode array to obtain a vector of symbolic instructions
        InstructionLoadingVisitor visitor =
            new InstructionLoadingVisitor( 0, bytecode, constantPoolMap,
                                           localVarMap, localVarTable,
					   method );
        Vector symInstr = visitor.iterate();
	
        cfg.constructCFG( symInstr, constantPoolMap, cfg.handlers,
			  lineNumTable, METHOD_CFG );

	method.addAttribute( cfg.handlers );

	// add demand-driven attribute to program
	method.addAttribute( (MethodAttribute)cfg );
	return (MethodAttribute)cfg;
    }

    /**
     * Identifies the node to which a node for a goto instruction or
     * a finally-goto-exit node is connected.
     * @param targetOffset Target bytecode offset for the goto instruction or
     *                     the finally-goto-exit node.
     * @return The target node to which the finally-goto-exit node or the
     *         node for goto instruction is to be connected.
     */
    public StatementNode getGotoSuccessor( Integer targetOffset )
    {
	
	if ( bytecodeOffsetNodeMap.containsKey( targetOffset ) ) {
	    return (StatementNode)bytecodeOffsetNodeMap.get( targetOffset );
	}

	// this can happen for a break or continue statement
	// in a finally block where the loop encloses the finally
	// block -- the target of goto, therefore, lies in an
	// enclosing CFG;
	// check if a FINALLY_GOTO_EXIT_NODE has been created
	// in the CFG for the same target offset

	StatementNode gotoExitNode = (StatementNode)
	    finallyGotoExitNodes.get( targetOffset );
	if ( gotoExitNode == null ) {
	    gotoExitNode = new StatementNodeImpl ();
	    gotoExitNode.setType( StatementNode.FINALLY_GOTO_EXIT_NODE );
	    gotoExitNode.setByteCodeOffset( StatementNode.
					    EXCEPTIONAL_EXIT_NODE_OFFSET );
	    gotoExitNode.setContainingMethod( getMethod() );

	    // add finally-transfer attribute to finally-goto-exit node
	    FinallyTransferAttribute attr = new FinallyTransferAttributeImpl ();
	    attr.setBytecodeOffset( targetOffset.intValue() );
	    gotoExitNode.addAttribute( attr );

	    // add node to finally-goto-exit map
	    addNode( gotoExitNode );
	    finallyGotoExitNodes.put( targetOffset, gotoExitNode );
	}
	return gotoExitNode;
    }

    /**
     * Identifies the node to which a node for a return instruction or
     * a finally-return-exit node is connected.
     * @param targetOffset Target bytecode offset for the goto instruction or
     *                     the goto-exit node.
     * @return The target node to which the goto-exit node or the node for
     *         goto instruction is to be connected.
     */
    public StatementNode getFinallyReturnSuccessor()
    {

	// if CFG is built for a method, return the exit node as the
	// successor; otherwise return the finally-return-exit node
	// for this CFG
	if ( getMethod().getContainingMethod() == null ) {
	    // CFG is for a method; return the exit node, which was saved
	    // in finallyReturnExitNode
	    return finallyReturnExitNode;
	}
	// CFG is for a finally block; return the finally-return-exit node
	if ( finallyReturnExitNode == null ) {
	    finallyReturnExitNode = new StatementNodeImpl ();
	    finallyReturnExitNode.
		setType( StatementNode.FINALLY_RETURN_EXIT_NODE );
	    finallyReturnExitNode.
		setByteCodeOffset( StatementNode.
				   EXCEPTIONAL_EXIT_NODE_OFFSET );
	    finallyReturnExitNode.setContainingMethod( getMethod() );
	}
	return finallyReturnExitNode;
    }

    /**
     * Identifies the node to which a node for a JSR instruction is connected.
     * @param targetOffset Target bytecode offset for the JRS instruction.
     * @return The target node to which the node for JSR instruction
     *         is to be connected.
     */
    public StatementNode getJsrSuccessor( Integer targetOffset )
    {
	
	if ( bytecodeOffsetNodeMap.containsKey( targetOffset ) ) {
	    return (StatementNode)bytecodeOffsetNodeMap.get( targetOffset );
	}

	// otherwise, return the statement node whose bytecode offset
	// is the smallest value that is greater than targetOffset

	StatementNode targetStmt = null;
	Node[] cfgNodes = getNodes();
	int offset = targetOffset.intValue();

	for ( int i = 0; i < cfgNodes.length; i++ ) {
	    StatementNode stmt = (StatementNode)cfgNodes[i];
	    if ( stmt.getByteCodeOffset() > offset ) {
		if ( targetStmt == null ) {
		    targetStmt = stmt;
		}
		else {
		    if ( stmt.getByteCodeOffset() <
			 targetStmt.getByteCodeOffset() ) {
			targetStmt = stmt;
		    }
		}
	    }
	}
	return targetStmt;
    }

    /**
     * Identifies the node to which a node for a JSR instruction is connected.
     * @param targetOffset Target bytecode offset for the JRS instruction.
     * @return The target node to which the node for JSR instruction
     *         is to be connected.
     */
    private void createOutedgesForJsr( Integer targetOffset,
				       StatementNode jsrNode,
				       StatementNode jsrSuccNode ) {

	// label the outedge from finally-end node with the bytecode offset
	// of the target node
	String finallyEndOutedgeLabel = String.valueOf( jsrSuccNode.
							getByteCodeOffset() );

	// check if a finally start node has already been created for
	// the finally to which the jump occurs; if it has, create an
	// edge from the jsr node to the finally end node and add a
	// finally context attribute to the edge; also add an edge from the
	// finally end node to the jsr succ node and add a finally context
	// attribute to the edge
	String finallyName =
	    ((ExceptionHandlersImpl) handlers).getFinallyNameForEntryOffset( targetOffset.intValue() );
	if ( finallyNameToStartNodeMap.containsKey( finallyName ) ) {
	    StatementNode finallyStartNode = (StatementNode)
		finallyNameToStartNodeMap.get( finallyName );
	    Edge jsrToFinallyEdge = new EdgeImpl ( jsrNode, finallyStartNode );
	    addEdge( jsrToFinallyEdge );
	    FinallyContextAttribute attr = new FinallyContextAttributeImpl ();
	    jsrToFinallyEdge.addAttribute( attr );

	    // a finally end node should exist unless the finally contains an
	    // unconditional break/continue/return/throw
	    if ( finallyNameToEndNodeMap.containsKey( finallyName ) ) {
		StatementNode finallyEndNode = (StatementNode)
		    finallyNameToEndNodeMap.get( finallyName );
		Edge finallyEndToJsrSuccEdge =
		    new EdgeImpl ( finallyEndNode, jsrSuccNode,
			      finallyEndOutedgeLabel);
		addEdge( finallyEndToJsrSuccEdge );
		finallyEndToJsrSuccEdge.addAttribute( attr );
	    }
	    return;
	}

	// get the first node in finally block---the finally start node
	// is connected to this node
	StatementNode targetNode = (jaba.graph.StatementNode) getJsrSuccessor( targetOffset );
	Debug.println( "Target for JSR = "+targetOffset, Debug.CFG, 2 );
	Debug.println( "Target Node = "+targetNode.getByteCodeOffset(),
		       Debug.CFG, 2 );

	// create a FINALLY_START_NODE, add a finally-entry attribute to the
	// node, add the node to the CFG as successor of jsrNode and
	// predecessor of the target node, add finally-context attribute to
	// (jsr, start node) edge, add start  node to
	// finallyNameToStartNodeMap
	StatementNode finallyStartNode = new StatementNodeImpl ();
	finallyStartNode.setType( StatementNode.FINALLY_START_NODE );
	finallyStartNode.setByteCodeOffset( targetNode.getByteCodeOffset() );
	finallyStartNode.setContainingMethod( getMethod() );

	// add finally-entry attribute to the start node
	FinallyEntryAttribute attr = new FinallyEntryAttributeImpl ();
	attr.setName( finallyName );
	finallyStartNode.addAttribute( attr );

	// add start node to the CFG, create edges from jsr node to the
	// start node and from start node to the target node
	Edge jsrToFinallyEdge = new EdgeImpl ( jsrNode, finallyStartNode );
	addEdge( jsrToFinallyEdge );
	addEdge( new EdgeImpl ( finallyStartNode, targetNode ) );

	// add finally-context attribute to (jsr, start node) edge
	FinallyContextAttribute finallyContextAttr =
	    new FinallyContextAttributeImpl ();
	jsrToFinallyEdge.addAttribute( finallyContextAttr );

	// add start node to finallyNameToStartNodeMap
	finallyNameToStartNodeMap.put( finallyName, finallyStartNode );

	// create FINALLY_END_NODE, add node to the CFG as successor of
	// the RET node for the finally and predecessor of the next CFG
	// node, add finally-context attribute to (end node, jsr succ) edge,
	// add end node to finallyNameToEndNodeMap
	int retOffset = ((ExceptionHandlersImpl) handlers).
	    getFinallyExitOffsetForEntryOffset( targetOffset.intValue() );
	Debug.println( "RET for JSR = "+retOffset, Debug.CFG, 2 );
	if ( retOffset == 0 ) {
	    // finally has unconditional break/continue/return/throw;
	    // no finally-end node is created
	    return;
	}
	// get the statement node corresponding to RET instruction offset
	Node [] nodes = getNodes ();
	StatementNode retNode = null;
	for ( int i = 0; i < nodes.length; i++ ) {
	    SymbolicInstruction[] symInstr = ( (StatementNode)nodes[i] ).
		getSymbolicInstructions();
	    if ( symInstr == null ) {
		continue;
	    }
	    if ( (byte)symInstr[symInstr.length-1].getOpcode() ==
		 OpCode.OP_RET &&
		 symInstr[symInstr.length-1].getByteCodeOffset() ==
		 retOffset ) {
		retNode = (StatementNode)nodes[i];
		break;
	    }
	}
	//	StatementNode retNode = (StatementNode)
	//    bytecodeOffsetNodeMap.get( new Integer( retOffset ) );
	if ( retNode == null ) {
	    RuntimeException e = new RuntimeException(
	        "CFG node for RET instruction "+retOffset+
		" not found: "+getMethod().getFullyQualifiedName() );
	    e.printStackTrace();
	    throw e;
	}
	// create finally-end node
	StatementNode finallyEndNode = new StatementNodeImpl ();
	finallyEndNode.setType( StatementNode.FINALLY_END_NODE );
	finallyEndNode.setByteCodeOffset( retNode.getByteCodeOffset() );
	finallyEndNode.setContainingMethod( getMethod() );

	// create edges to and from the finally-end node
	Edge finallyEndToJsrSuccEdge = new EdgeImpl ( finallyEndNode, jsrSuccNode,
						 finallyEndOutedgeLabel );
	addEdge( new EdgeImpl ( retNode, finallyEndNode ) );
	addEdge( finallyEndToJsrSuccEdge );

	// add finally-context attribute to (end node, jsr succ) edge
	finallyEndToJsrSuccEdge.addAttribute( finallyContextAttr );

	// add end node to finallyNameToEndNodeMap
	finallyNameToEndNodeMap.put( finallyName, finallyEndNode );
    }

    /**
     * Adds an edge that is incident to a finally-start node or incident from
     * a finally-end node. If an edge exists between <code>srcNode</code>
     * and <code>sinkNode</code>, adds <code>exceptionType</code> to
     * to the set of exception types stored in the
     * <code>FinallyContextAttribute</code> that is attached to the edge,
     * <code>exceptionType</code>. If no edge exists, creates an edge and
     * <code>FinallyContextAttribute</code> with <code>exceptionType</code>
     * to the edge.
     * @param srcNode Source node for the edge.
     * @param sinkNode Sink node for the edge.
     * @param exceptionType Exception type to add to the
     *                      <code>FinallyContextAttribute</code> of the edge.
     * @throws IllegalArgumentException
     *         - if neither <code>srcNode</code> is FINALLY_END_NODE nor
     *           <code>sinkNode</code> is FINALLY_START_NODE
     *         - if <code>exceptionType</code> is null
     */
    private void addFinallyContextEdge( StatementNode srcNode,
					StatementNode sinkNode,
					Class exceptionType ) 
        throws IllegalArgumentException
    {

	Debug.println( "Adding finally-context edge in "+
		       getMethod().getFullyQualifiedName()+" for type "+
		       exceptionType.getName(), Debug.CFG, 1 );
	if ( exceptionType == null ) {
	    IllegalArgumentException e = new IllegalArgumentException(
                "exceptionType is null: "+
		getMethod().getFullyQualifiedName() );
	    e. printStackTrace();
	    throw e;
	}
	if ( srcNode.getType() != StatementNode.FINALLY_END_NODE &&
	     sinkNode.getType() != StatementNode.FINALLY_START_NODE ) {
	    IllegalArgumentException e = new IllegalArgumentException(
                "neither srcNode is FINALLY_END_NODE nor sinkNode is "+
		"FINALLY_START_NODE: "+getMethod().getFullyQualifiedName() );
	    e. printStackTrace();
	    throw e;
	}
	// check if the edge already exists in the CFG
	if ( containsEdgeLike( srcNode, sinkNode ) ) {
	    // get the edge and update its finally-context attribute
	    Edge existingEdge = null;
	    Edge [] outEdges = getOutEdges (srcNode);
	    for ( int i = 0; i < outEdges.length; i++ ) {
		if ( outEdges[i].getSink() == sinkNode ) {
		    existingEdge = outEdges[i];
		    break;
		}
	    }
	    FinallyContextAttribute attr = null;
	    attr = Factory.getFinallyContextAttribute (existingEdge);
	    if ( attr == null ) {
		RuntimeException e = new RuntimeException(
		    "Edge between node "+srcNode.getByteCodeOffset()+" (type "+
		    srcNode.getType()+") and node"+
		    sinkNode.getByteCodeOffset()+" (type "+sinkNode.getType()+
		    " has no FinallyContextAttribute:\n"+
		    getMethod().getFullyQualifiedName() );
		e.printStackTrace();
		throw e;
	    }
	    attr.addExceptionType( exceptionType );
	}
	else {
	    // create a new edge and add finally-context attribute to the edge
	    Edge newEdge = new EdgeImpl ( srcNode, sinkNode );
	    if ( srcNode.getType() == StatementNode.FINALLY_END_NODE ) {
		newEdge.setLabel( exceptionType.getName() );
	    }
	    addEdge( newEdge );
	    FinallyContextAttribute attr = new FinallyContextAttributeImpl ();
	    attr.addExceptionType( exceptionType );
	    newEdge.addAttribute( attr );
	}
    }

    /**
     * Return an xml format of this cfg. It will first be node information, 
     * and at the end is the information about the entry information.The Node 
     * information  includes id, type, source line, successors. The entry info
     * only includes the entry node id. So the total length of the string will be 
     * the node number + 1.  
     * @return string array representation of this graph
     */ 
	public String printGraph ()
	{
	StringBuffer buf = new StringBuffer (1000);

		// Put the cfg information
		String className = jaba.tools.Util.toXMLValid (getMethod ().getContainingType ().getName ());
		String methodName = jaba.tools.Util.toXMLValid (getMethod ().getName ());
		buf.append("<cfg><name>" + className + ":" + methodName  + "</name>");

		// while the assert below may seem correct, it is not.  An entry to a
		// catch handler might not have a predecessor and therefore be
		// classified as an entry node (no predecessor edges).
		// since this is not correct, the whole block of code is unnecessary.

		// Put the entry information
		//Node [] entries = getEntryNodes ();
		//assert entries.length == 1 : ("CFG.printGraph: entries.length == " + entries.length + " (should be 1)");
		//StatementNode entry = (StatementNode) entries [0];
		//buf.append ("<entry>" + entry.getNodeNumber () + "</entry>");

		// Put the node information
		Node [] allNodes = getNodes ();
		buf.append ("<nodes><num>" + allNodes.length + "</num>");
		for(int i = 0; i < allNodes.length; i++)
		{
			StatementNode currentNode = (StatementNode) allNodes [i];
			buf.append("<node>");
			buf.append("<id>" + currentNode.getNodeNumber () + "</id>");
			buf.append("<type>" + currentNode.getShortTypeName () + "</type>");
			buf.append("<sourceline>" + currentNode.getSourceLineNumber () +"</sourceline>");

			Edge [] outs = getOutEdges (currentNode);
			for(int j = 0; j < outs.length; j++)
			{
				buf.append("<succesor>");
				StatementNode sink = (StatementNode) outs [j].getSink ();
				int sinkId = sink.getNodeNumber ();
				String edgeLabel = outs [j].getLabel ();

				buf.append ("<sinkid>" + sinkId + "</sinkid>");
				buf.append ("<edgelabel>");
				if(edgeLabel != null)
				{
					buf.append (edgeLabel);
				}
				buf.append("</edgelabel>");
				buf.append("</succesor>");
			}
			buf.append("</node>");
		}
		buf.append ("</nodes>");
		buf.append ("</cfg>");

		return buf.toString();
	}

	/** returns an XML representation of this graph */
	public String toString ()
	{
		return "<cfg>" + getString () + "</cfg>";
	}

	/** returns the reduced representation of this object */
	public String toStringReduced ()
	{
		return "<cfg>" + getStringReduced () + "</cfg>";
	}

	/** returns the detailed representation of this object */
	public String toStringDetailed ()
	{
		return "<cfg>" + getStringDetailed () + "</cfg>";
	}

    /** Construct a CFG for a method. */
    private static final int METHOD_CFG = 1;

    /** Construct a CFG for a finally block. */
    private static final int FINALLY_BLOCK_CFG = 2;

    /** Offset of the first symbolic instruction for the CFG */
    private int firstInstructionOffset;

    /** Offset of the last symbolic instruction for the CFG */
    private int lastInstructionOffset;

    /** Wrapper for method's exception table. */
    private ExceptionHandlers handlers;

    /** Type inference object for this CFG. */
    private TypeInference typeInference;

    /** Exception types propagated by this method. */
    private Class[] propagatedExceptions;

    /** Exceptional-exit nodes of this CFG. */
    private HashMap exceptionExitNodes;

    /**
     * Exceptional-finally-call nodes of this CFG. The map is from an
     * exception type to a vector of exceptional-finally-call nodes; the
     * vector contains one finally-call node for each finally block that
     * executes in the exceptional context for that exception type.
     */
    private HashMap exceptionalFinallyCallNodes;

    /**
     * Map from the name of a finally block to the FINALLY_START_NODE for
     * that block; the hashmap is populated only if the CFG has inlined
     * finally blocks
     */
    private HashMap finallyNameToStartNodeMap;

    /**
     * Map from the name of a finally block to the FINALLY_END_NODE for
     * that block; the hashmap is populated only if the CFG has inlined
     * finally blocks
     */
    private HashMap finallyNameToEndNodeMap;

    /** Map from byte-code offset for catch handlers to statement node. */
    private HashMap catchOffsetNodeMap;

    /** Map from byte-code offset  to statement node. */
    private HashMap bytecodeOffsetNodeMap;

    /**
     * Vector of String names for exception types that are propagated
     * directly or indirectly by the method corresponding to this CFG
     */
    private Vector propRuntimeExceptions;

    /**
     * Finally methods directly contained in the method for this CFG. These
     * are stored to avoid calling method.getFinallyMethods(), which can result
     * in an infinite loop (because method.getFinallyMethods() constructs the
     * the CFG for the method before returning the array of finally methods).
     */
    private Vector finallyMethods;

    /**
     * Hashmap containing goto-exit nodes created for unconditional transfers
     * from the finally block; the hashmap is empty for CFGs for methods;
     * for CFGs for finally blocks, it contains one exit node for each
     * break or continue that transfers control (outside the finally) to
     * a distinct instruction. Although each unlabeled break (and,
     * likewise, unlabeled continue) can have a single destination
     * outside the finally---to the lexically enclosing loop---labeled
     * breaks (and continues) can have different destinations.
     */
    private HashMap finallyGotoExitNodes;

    /** Exit node for a return statement within a finally; is non-null */
    private StatementNode finallyReturnExitNode;
}
