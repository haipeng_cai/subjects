/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

import jaba.sym.Method;
import jaba.sym.MethodAttribute;

/**
 * Encapsulates an exception-handler table, and provides various methods
 * to access information in the table.
 * <p>
 * An <code>ExceptionHandlers</code> object is created from
 * an array of <code>ExceptionHandler</code> objects that hold
 * exception-handler information in the form that it is represented
 * in a class file. The class provides several methods to access
 * information about catch handlers and finally blocks that is required
 * during the CFG construction. <code>ExceptionHandlers</code> is created
 * and used by </code>CFG.load()</code>.
 * See jaba.classfile.ExceptionHandler for implementation details.
 *
 * @author S. Sinha
 * @see CFG
 * @author Huaxing Wu -- <i> Revised 7/18/02. Change getTryStatements method,
 *                       add getSynchronizedTryStatements method </i>
 * @author Jay Lofstead 2003/05/29 added toString that outputs XML
 * @author Jay Lofstead 2003/06/02 removed elementAt in favor of enumeration/iteration for better performance.
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/07/10 changed to be an interface
 */
public interface ExceptionHandlers extends MethodAttribute, java.io.Serializable
{
	/**
	 * Returns the method associated with this exception-handlers object.
	 * @return Method associated with this exception-handlers object.
	 */
	public Method getMethod ();

	/**
	 * Returns the try statements that appear in the method associated with
	 * this exception-handlers object.
	 * Note: Compiler will generate an exception entry for synchronized 
	 * statements. This method will not return synchronized statements.
	 * @return Try statements that appear in the method associated with
	 *         this exception-handlers object.
	 */
	public TryStatement [] getTryStatements ();

	/**
	 * Returns the Synchronized statements that appear in the method 
	 * associated with this exception-handlers object. 
	 * Note: Compilers will generate exception entry for 
	 * synchronized statements 
	 * @return Synchronized statements that appear in the method associated 
	 * with this exception-handlers object.
	 */
	public TryStatement [] getSynchronizedTryStatements ();

	/**
	 * returns an XML format of the object
	 */
	public String toString ();
}
