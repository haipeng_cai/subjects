/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

/**
 * Thrown to indicate that an index in the constant pool (that is referenced
 * by a bytecode instruction) is not mapped to a <code>SymbolTableEntry</code>
 * object, or if mapped, the type of the object is not the expected subtype
 * of <code>SymbolTableEntry</code>.
 *
 * @author S. Sinha
 */

public class SymbolNotFoundException extends Exception {

    public SymbolNotFoundException( String msg ) {
        super( msg );
    }

}
