/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

/**
 * Represents a try block.
 * <p>
 * @author S. Sinha
 * @author Jay Lofstead 2003/05/29 converted toString to return an XML format
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to implement TryBlock
 */
public class TryBlockImpl implements java.io.Serializable, TryBlock
{
    /** Byte code offset of the first instruction in the try block */
    private int startByteCodeOffset;

    /** Byte code offset of the last instruction in the try block */
    private int endByteCodeOffset;

    /**
     * Constructs an <code>TryBlock</code> object.
     * @param handler exception-handler table entry for a catch block
     *                associated with the try block.
     */
    TryBlockImpl ( int startPC, int endPC )
    {
        startByteCodeOffset = startPC;
	endByteCodeOffset = endPC;
    }

    /**
     * Returns the bytecode offset of the first instruction of the try
     * block.
     * @return Bytecode offset of the first instruction of the try
     *         block.
     */
    public int getStartByteCodeOffset() {
	return startByteCodeOffset;
    }

    /**
     * Returns the bytecode offset of the last instruction of the try
     * block.
     * @return Bytecode offset of the last instruction of the try
     *         block.
     */
    public int getEndByteCodeOffset() {
	return endByteCodeOffset;
    }

    /**
     * Returns a string for a try block.
     * @return String representation for try block.
     */
	public String toString ()
	{
		return "<tryblock><start>" + startByteCodeOffset + "</start><end>" + endByteCodeOffset + "</end></tryblock>";
	}
}
