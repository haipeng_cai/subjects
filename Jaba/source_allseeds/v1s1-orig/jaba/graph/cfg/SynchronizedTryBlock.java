/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

/**
 * Represents a Synchronized block. It's similar to a try/finally block in 
 * the class file. As all completion of the block, normally or 
 * abruptly(exception), has to release the lock first. Which is exactly 
 * what a finally block does. So compiler is creating an exception entry
 * for the synchronized block.
 * <p>
 * @author Huaxing Wu
 * @author Jay Lofstead 2003/07/14 changed to an interface
 */
public interface SynchronizedTryBlock extends TryBlock
{
    /**
     * Returns a string for a synchronized block.
     * @return String representation for synchronized block.
     */
    public String toString ();
}
