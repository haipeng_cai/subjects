/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

import jaba.sym.Class;

/**
 * Represents a catch block.
 * <p>
 * @author S. Sinha
 * @author Jay Lofstead 2003/05/29 converted toString to return an XML format
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to implement CatchBlock
 */
public class CatchBlockImpl implements java.io.Serializable, CatchBlock
{
    /** Byte code offset of the first instruction in the catch block */
    private int startByteCodeOffset;

    /** Type of exception handled by the catch block */
    private Class exceptionType;

    /**
     * Constructs an <code>CatchBlock</code> object.
     * @param startOffset bytecode offset of the first instruction in the
     *                    catch block.
     * @param type        type of exception handled by catch block.
     * @throws IllegalArgumentException if <code>type</code> is null.
     */
    CatchBlockImpl ( int startOffset, Class type )
        throws IllegalArgumentException
    {
	if ( type == null ) {
	    throw new IllegalArgumentException( "CatchBlock.CatchBlock(): "+
						"type is null" );
	}
        startByteCodeOffset = startOffset;
	exceptionType = type;
    }

    /**
     * Returns the bytecode offset of the first instruction of the catch
     * block.
     * @return Bytecode offset of the first instruction of the catch
     *         block.
     */
    public int getByteCodeOffset() {
	return startByteCodeOffset;
    }

    /**
     * Returns the type of exception caught by the catch block.
     * @return Type of exception caught by the catch block.
     */
    public Class getExceptionType() {
	return exceptionType;
    }

    /**
     * Returns a string for a catch block.
     * @return String representation for catch block.
     */
	public String toString ()
	{
		return "<catchblock><start>" + startByteCodeOffset
			+ "</start><exceptiontype>" + exceptionType.getName ()
			+ "</exceptiontype></catchblock>"
			;
	}
}
