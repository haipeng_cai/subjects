/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

import jaba.sym.Class;

/**
 * Represents a catch block.
 * <p>
 * @author S. Sinha
 * @author Jay Lofstead 2003/05/29 converted toString to return an XML format
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface CatchBlock extends java.io.Serializable
{
    /**
     * Returns the bytecode offset of the first instruction of the catch
     * block.
     * @return Bytecode offset of the first instruction of the catch
     *         block.
     */
    public int getByteCodeOffset();

    /**
     * Returns the type of exception caught by the catch block.
     * @return Type of exception caught by the catch block.
     */
    public Class getExceptionType();

    /**
     * Returns a string for a catch block.
     * @return String representation for catch block.
     */
	public String toString ();
}
