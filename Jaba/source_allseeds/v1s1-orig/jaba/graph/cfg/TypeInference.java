/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

import jaba.sym.Class;
import jaba.sym.ReferenceType;
import jaba.sym.TypeEntry;
import jaba.sym.LocalVariable;
import jaba.sym.Field;
import jaba.sym.Method;

import jaba.du.DefUse;
import jaba.du.LeafDefUse;
import jaba.du.RecursiveDefUse;
import jaba.du.HasValue;
import jaba.du.CastedReferenceDefUse;
import jaba.du.PrimitiveDefUse;
import jaba.du.ReferenceDefUse;
import jaba.du.ArrayElementDefUse;
import jaba.du.TempVariable;
import jaba.du.ConstantValue;
import jaba.du.NewInstance;
import jaba.du.PlaceholderLocation;

import jaba.graph.Node;
import jaba.graph.Edge;
import jaba.graph.ThrowStatementAttribute;
import jaba.graph.MethodCallAttribute;
import jaba.graph.NewInstanceAttribute;
import jaba.graph.StatementNode;
import jaba.graph.ExceptionAttribute;

import jaba.tools.local.Factory;

import java.util.Vector;
import java.util.Collection;
import java.util.Iterator;

/**
 * Implements the type-inference algorithm to determine exception types that
 * may be raised at throw statements.
 *
 * @see jaba.graph.ThrowStatementAttribute
 * @author S. Sinha
 * @author Jay Lofstead 2003/06/02 removed elementAt in favor of enumeration/iteration for better performance.
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/17 changed to use Factory to get attributes.
 */
public class TypeInference implements java.io.Serializable
{
    /**
     * Inner class to hold information generated during data-flow
     * analysis. One such object is created for each throw statement whose
     * expression type is <code>VAR_THROW_EXPR</code>.
     * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializeable
     */
    class DataFlowAnalysisInfo implements java.io.Serializable
    {

        /** Size of type set that was inferred precisely by data-flow
            analysis. */
        int preciseTypeSetSize;

        /** Size of type set that was conservatively approximated by
            data-flow analysis at the entry node. */
        int safeEntryTypeSetSize;

        /** Sizes of type sets that were conservatively approximated by
            data-flow analysis at each catch node. */
        int[] safeCatchTypeSetSize;

        /** Sizes of type sets that were conservatively approximated by
            data-flow analysis at each method-call node. */
        int[] safeMethodCallTypeSetSize;

        /** Total number of types inferred during analysis. */
        int typeSetSize;

        /**
         * Creates a new <code>DataFlowAnalysisInfo</code> object.
         */
        DataFlowAnalysisInfo() {
            preciseTypeSetSize = 0;
            safeEntryTypeSetSize = 0;
            safeCatchTypeSetSize = null;
            safeMethodCallTypeSetSize = null;
            typeSetSize = 0;
        }

        /**
         * Returns a string representation of data-flow analysis info.
         * @return String representation of data-flow analysis info.
         */
        public String toString() {
            String str = " type set size = "+typeSetSize+
               "\n  precise type set = "+preciseTypeSetSize+
               "\n  safe entry type set = "+safeEntryTypeSetSize+
               "\n  safe catch type set = ";
            if ( safeCatchTypeSetSize != null ) {
                for ( int i=0; i<safeCatchTypeSetSize.length; i++ ) {
                    str += safeCatchTypeSetSize[i]+" ";
                }
            }
            str += "\n safe method call type set = ";
            if ( safeMethodCallTypeSetSize != null ) {
                for ( int i=0; i<safeMethodCallTypeSetSize.length; i++ ) {
                    str += safeMethodCallTypeSetSize[i]+" ";
                }
            }
            str += "\n";
            return str;
        }
    }

    /**
     * Creates a new <code>TypeInference</code> object.
     */
    TypeInference ( CFG cfg ) {
        this.cfg = cfg;
        throwStatementCount = 0;
        newInstanceExprCount = 0;
        methodCallExprCount = 0;
        varExprCount = 0;
        avgFailureTypeSet = 0;
        avgMethodCallExprTypeSet = 0;
        maxFailureTypeSet = 0;
        dataFlowInfo = new Vector();
    }

    /** ??? */
    public void printDataFlowInfo() {
        if ( dataFlowInfo.size() == 0 ) {
            return;
        }
	int i = 0;
	for (Iterator e = dataFlowInfo.iterator (); e.hasNext (); i++)
	{
		System.out.println (i + " " + e.next ());
	}
    }

    /**
     * Returns number of throw statements processed by this type-inference
     * object.
     * @return Number of throw statements processed.
     */
    public int getThrowStatementCount() {
        return throwStatementCount;
    }

    /**
     * Returns the number of times data-flow analysis failed: analysis reached
     * method entry with a non-empty relevant-variable set.
     * @return Number of times type inference resulted in imprecise, safe
     *         estimate.
     */
    public int getFailureCount() {
        int count = 0;
        for (Iterator e = dataFlowInfo.iterator (); e.hasNext ();)
	{
            DataFlowAnalysisInfo info = (DataFlowAnalysisInfo) e.next ();
            if (   info.safeEntryTypeSetSize > 0
                || info.safeCatchTypeSetSize != null
                || info.safeMethodCallTypeSetSize != null
	       )
	    {
                count++;
            }
        }
        return count;
    }

    /**
     * Returns the type-set sizes for cases when data-flow analysis
     * succeeded.
     * @return Array of type-set sizes when data-flow analysis succeeded.
     */
    public int[] getSuccessTypeSet()
    {
        Vector v = new Vector ();
        for (Iterator e = dataFlowInfo.iterator (); e.hasNext ();)
	{
            DataFlowAnalysisInfo info = (DataFlowAnalysisInfo) e.next ();
            if (   info.safeEntryTypeSetSize == 0
                && info.safeCatchTypeSetSize == null
                && info.safeMethodCallTypeSetSize == null
	       )
	    {
                v.addElement( new Integer( info.preciseTypeSetSize ) );
            }
        }
	int i = 0;
        int [] r = new int[v.size()];
        for (Iterator e = v.iterator (); e.hasNext (); i++)
	{
            r [i] = ((Integer) e.next ()).intValue ();
        }
        return r;
    }

    /**
     * Returns the type-set sizes for cases when data-flow analysis
     * hit the entry of the method.
     * @return Array of type-set sizes when data-flow analysis made
     *         conservative estimate at method entry.
     */
    public int[] getSafeEntryTypeSetSize()
    {
        Vector v = new Vector ();
        for (Iterator e = dataFlowInfo.iterator (); e.hasNext ();)
	{
            DataFlowAnalysisInfo info = (DataFlowAnalysisInfo) e.next ();
            if ( info.safeEntryTypeSetSize != 0 )
	    {
                 v.addElement( new Integer( info.safeEntryTypeSetSize ) );
            }
        }
	int i = 0;
        int [] r = new int[v.size()];
        for (Iterator e = v.iterator (); e.hasNext (); i++)
	{
            r [i] = ((Integer) e.next ()).intValue ();
        }
        return r;
    }

    /**
     * Returns the type-set sizes for cases when data-flow analysis
     * hit a catch node. A catch node may be encountered several times
     * during the analysis for a single throw node.
     * @return Vector of type-set sizes when data-flow analysis made
     *         conservative estimate at a catch node. Each element of the
     *         vector is an array of <code>int</code>, and stores the type-set
     *         size for each time a catch node was hit.
     */
    public Vector getSafeCatchTypeSetSize()
    {
        Vector v = new Vector ();
        for (Iterator e = dataFlowInfo.iterator (); e.hasNext ();)
	{
            DataFlowAnalysisInfo info = (DataFlowAnalysisInfo) e.next ();
            if ( info.safeCatchTypeSetSize != null )
	    {
                v.addElement( info.safeCatchTypeSetSize );
            }
        }
        return v;
    }

    /**
     * Returns the type-set sizes for cases when data-flow analysis
     * hit a method-call node. A method-call node may be encountered several
     * times during the analysis for a single throw node.
     * @return Vector of type-set sizes when data-flow analysis made
     *         conservative estimate at a method-call node. Each element of the
     *         vector is an array of <code>int</code>, and stores the type-set
     *         size for each time a method-call node was hit.
     */
    public Vector getSafeMethodCallTypeSetSize()
    {
        Vector v = new Vector ();
        for (Iterator e = dataFlowInfo.iterator (); e.hasNext ();)
	{
            DataFlowAnalysisInfo info = (DataFlowAnalysisInfo) e.next ();
            if ( info.safeMethodCallTypeSetSize != null ) {
                v.addElement( info.safeMethodCallTypeSetSize );
            }
        }
        return v;
    }

    /**
     * Returns the average size of the inferred type set in cases where
     * the throw-expression type is a method-call expression.
     * @return Average type set size for method-call expressions.
     */
    public int getAvgMethodCallExprTypeSet() {
        return avgMethodCallExprTypeSet;
    }

    /**
     * Returns number of specified type of throw-statement expressions
     * encountered during type inference.
     * @param type type of throw expression.
     * @return Number of throw statements with specified expression type that
     *         were processed during type inference.
     * @throws IllegalArgumentException if <code>type</code> is not a valid
     *                                  throw-statement-expression type.
     * @see jaba.graph.ThrowStatementAttribute
     */
    public int getExprTypeCount( int type ) {
        if ( type == ThrowStatementAttribute.NEW_INSTANCE_THROW_EXPR ) {
            return newInstanceExprCount;
        }
        if ( type == ThrowStatementAttribute.METHOD_CALL_THROW_EXPR ) {
            return methodCallExprCount;
        }
        if ( type == ThrowStatementAttribute.VAR_THROW_EXPR ) {
            return varExprCount;
        }
        throw new IllegalArgumentException(
            "TypeInference.getExprTypeCount(): type "+type+
            " is not not a valid expression type" );
    }

    /**
     * Infers types of exceptions that may be raised at throw statements
     * in a CFG, and those types to the throw-statement attributes of those
     * throw statement nodes.
     * @param cfg CFG in which to determine types of exceptions
     */
    void inferTypes() {
        int totFailureTypeSet=0;
        int totSuccessTypeSet=0;
        int totMethodCallExprTypeSet=0;

        Node [] nodes = cfg.getNodes ();
        for ( int i=0; i<nodes.length; i++ ) {
            StatementNode node = (StatementNode)nodes[i];
            if ( node.getType() != StatementNode.THROW_NODE ) {
                continue;
            }
            throwStatementCount++;
            ThrowStatementAttribute throwAttr = Factory.getThrowStatementAttribute (node);
            if ( throwAttr == null ) {
		RuntimeException e = new RuntimeException();
		e.printStackTrace();
		throw e;
	    }
            int exprType = throwAttr.getExpressionType();
            if ( exprType ==
		 ThrowStatementAttribute.NEW_INSTANCE_THROW_EXPR ) {
                throwAttr.addExceptionType( throwAttr.getExceptionType() );
                newInstanceExprCount++;
                continue;
            }
            if ( exprType == ThrowStatementAttribute.METHOD_CALL_THROW_EXPR ) {
                Class type = throwAttr.getExceptionType();
                Collection subTypes = type.getAllSubClassesCollection ();
                throwAttr.addExceptionType( type );
                //System.out.print( "Method-call exceptions = "+type.getName() );
                for (Iterator j = subTypes.iterator (); j.hasNext ();)
		{
                    throwAttr.addExceptionType((Class) j.next ());
                    //System.out.print( subTypes[j].getName()+" " );
                }
                //System.out.print( "\n" );
                methodCallExprCount++;
                totMethodCallExprTypeSet += subTypes.size () + 1;
                continue;
            }
            varExprCount++;

            // set the type of variable used at throw node -- this was not
            // done when the CFG node was created because def-use information
            // was not available at that time
            Collection uses = node.getUsesCollection ();
	    if ( uses == null || uses.size () == 0 ) {
		throw new RuntimeException( "Error: " + 
					    "TypeInference.inferTypes(): " +
					    "Throw node for variable " + 
					    "expression throw has no uses\n" +
					    node.toString() );
	    }
	    LeafDefUse leafThrowUse;
		DefUse temp = (DefUse) uses.iterator ().next (); // get the first element
            if (temp instanceof LeafDefUse ) {
		leafThrowUse = (LeafDefUse)temp;
            }
            else {
		leafThrowUse = ((RecursiveDefUse)temp).getLeafElement();
            }
            HasValue throwVar = leafThrowUse.getVariable();
            TypeEntry declThrowVarType = null;
            if ( throwVar instanceof LocalVariable ) {
                declThrowVarType = (jaba.sym.TypeEntry) ((LocalVariable)throwVar).getType();
            }
            else if ( throwVar instanceof Field ) {
                declThrowVarType = (jaba.sym.TypeEntry) ((Field)throwVar).getType();
            }
            else {
		throw new RuntimeException( "Error: " + 
					    "TypeInference.inferTypes(): " +
					    "Argument of throw is not a " + 
					    "LocalVariable or a Field: " +
					    throwVar.getClass().getName() );
            }
            if ( declThrowVarType instanceof Class ) {
                throwAttr.setExceptionType( (Class)declThrowVarType );
            }
            else {
		throw new RuntimeException( "Error: " + 
					    "TypeInference.inferTypes(): " +
					    "Type of throw variable is not " +
					    "Class: " +
					    declThrowVarType.getName() );
            }

	    // check is the variable is casted to some type; if it is,
	    // we know the exact type of the exception and need not
	    // perform type inference
	    if ( leafThrowUse instanceof CastedReferenceDefUse ) {
		TypeEntry castedType = (jaba.sym.TypeEntry) 
		    ((CastedReferenceDefUse)leafThrowUse).getType();
		// casted type must be a class
		if ( !( castedType instanceof Class ) ) {
		    throw new RuntimeException( "Error: " +
						"TypeInference.inferTypes(): "+
						"Casted type at throw is not "+
						"a Class: " + castedType );
		}
		Class castedClass = (Class)castedType;
		throwAttr.addExceptionType( castedClass );
		throwAttr.setContainsTypecast( true );
		continue;
	    }

            //System.out.print( "\nDetermining types for\n"+node.toString() );
            Class[] types = determineTypes( node );
            //System.out.print( "\nException types = " );
            for ( int j=0; j<types.length; j++ ) {
                //System.out.print( types[j].getName()+ " " );
                throwAttr.addExceptionType( types[j] );
            }
            //System.out.print( "\n" );
        }
        if ( methodCallExprCount != 0 ) {
            avgMethodCallExprTypeSet = totMethodCallExprTypeSet /
                methodCallExprCount;
        }
    }

    /**
     * Inner class that defines a worklist node.
     * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
     */
    class WorklistNode implements java.io.Serializable
    {
        StatementNode node;  // statement node
        DefUse relevantVar;  // relevant variable for that node
        // constructor
        WorklistNode( StatementNode node, DefUse relevantVar ) {
            this.node = node;
            this.relevantVar = relevantVar;
        }

        /**
         * Returns the string representation of a worklist node.
         * @return String representation of a worklist node.
         */
        public String toString() {
            return "Node: "+node.toString()+"Var:  "+relevantVar.toString();
        }

        /**
         * Determines if one <code>WorklistNode</code> object equals another
         * <code>WorklistNode</code> object.
         * @param obj object that is tested for equality with <code>this</code>.
         * @return <code>true</code> if <code>obj</code> equals
         *         <code>this</code>; <code>false</code> otherwise.
         */
        public boolean equals( Object obj ) {
            if ( !(obj instanceof WorklistNode) ) {
                return false;
            }
            WorklistNode n = (WorklistNode)obj;
            return ( (this.node == n.node) &&
                     (this.relevantVar == n.relevantVar) );
        }

	public int hashCode() {
	    int result = 31;
	    result += 17 * node.getNodeNumber() + 5*relevantVar.hashCode();
	    return result;

	}
    }

    /** Vector to keep track of nodes that have been processed on worklist --
     *  to avoid re-processing nodes
     */
    private Vector worklistNodes;

    /**
     * Performs a reverse data-flow analysis starting at a given throw node in
     * the given CFG, and conservatively estimates types of exceptions that
     * can be raised at the throw node.
     * @param nodes Throw node for which to infer exception types.
     * @return Array of classes for the exception types that may be raised
     *         at <code>node</code>.
     */
    private Class[] determineTypes( StatementNode throwNode ) {

        worklistNodes = new Vector();

        // stores inferred exception types
        Vector typeSet = new Vector();

        // stores information about data-flow analysis
        DataFlowAnalysisInfo dfInfo = new DataFlowAnalysisInfo();

        // initialize worklist
        Vector worklist = new Vector();
        Collection uses = throwNode.getUsesCollection ();
        // find non-primitive use
        for (Iterator i = uses.iterator (); i.hasNext ();)
	{
		DefUse du = (DefUse) i.next ();
            //System.out.println (i + ": " + du);
            if ( !(du instanceof PrimitiveDefUse) ) {
                addNodeToWorklist( new WorklistNode( throwNode, du),
                                   worklist );
                break;
            }
        }
        // report error if worklist is empty -- throw node must have a 
        // non-primitive use
        if ( worklist.isEmpty() ) {
	    RuntimeException e = new RuntimeException();
	    e.printStackTrace();
	    throw e;
	}

        WorklistNode wNode;
        StatementNode sNode;
        DefUse relDefUse;

        // iterate over worklist nodes
        while ( !worklist.isEmpty() )
	{
            wNode = (WorklistNode)worklist.remove( 0 );
            //System.out.println( "\n"+wNode.toString() );
            sNode = wNode.node;
            relDefUse = wNode.relevantVar;

            if ( (relDefUse instanceof ArrayElementDefUse) ) {
		RuntimeException e = new RuntimeException();
		e.printStackTrace();
		throw e;
	    }

            switch ( sNode.getType() ) {
                case StatementNode.ENTRY_NODE:
                    //System.out.println( "processing entry node" );
                    dfInfo.safeEntryTypeSetSize +=
                        processEntryNode( sNode, relDefUse, worklist, typeSet );
                    break;
                case StatementNode.CATCH_NODE:
                    //System.out.println( "processing catch node" );
                    //System.out.println( sNode.toString() );
                    addToSafeCatchTypeSet( dfInfo,
                          processEntryNode( sNode, relDefUse, worklist,
                                            typeSet ) );
                    break;
                case StatementNode.STATIC_METHOD_CALL_NODE:
 	        case StatementNode.VIRTUAL_METHOD_CALL_NODE:
                    //System.out.println( "processing method-call node" );
                    addToSafeMethodCallTypeSet( dfInfo,
                        processMethodCallNode( sNode, relDefUse, worklist,
                                               typeSet ) );
                    break;
                case StatementNode.CLASS_INSTANTIATION_NODE:
                    //System.out.println( "processing instantiation node" );
                    dfInfo.preciseTypeSetSize +=
                        processInstantiationNode( sNode, relDefUse, worklist,
                                                  typeSet );
                    break;
                case StatementNode.STATEMENT_NODE:
                    //System.out.println( "processing statement node" );
                    processStatementNode( sNode, relDefUse, worklist );
                    break;
                default:
                    //System.out.println( "processing default" );
                    addPredecessorsToWorklist( sNode, relDefUse, worklist );
            }

        }

        // return array of Class for intferred exception types
        Class[] c = new Class[typeSet.size()];
	int i = 0;
        for (Iterator e = typeSet.iterator (); e.hasNext (); i++)
	{
            c[i] = (Class) e.next ();
        }
        dfInfo.typeSetSize = c.length;
        dataFlowInfo.addElement( dfInfo );

        return c;
    }

    /**
     * Processes a statement node from the worklist.
     * @param node statement node to process.
     * @param relDefUse relevant variables at that node.
     * @param worklist worklist.
     */
    private void processStatementNode( StatementNode node,
                                       DefUse relDefUse, Vector worklist )
    {
      // jim jones -- had to modify so that this would work with minimal
      // rework now that nodes only have at most one definition
      DefUse def = node.getDefinition ();
      DefUse[] defs;
      if ( def == null )
	defs = new DefUse[0];
      else
	{
	  defs = new DefUse[1];
	  defs[0] = (jaba.du.DefUse) def;
	}
        for ( int i=0; i<defs.length; i++ ) {
            if ( defs[i].equals( relDefUse ) )
	    {
                Collection u = node.getUsesCollection ();
		DefUse du = (DefUse) u.iterator ().next ();  // get the first element
                if (du instanceof ReferenceDefUse ) {
                    HasValue var = ((ReferenceDefUse) du).getVariable();
                    if ( var instanceof ConstantValue ) {
                        return;
                    }
                    addPredecessorsToWorklist( node, du, worklist );
                    return;
                }
            }
        }
        addPredecessorsToWorklist( node, relDefUse, worklist );
    }

    /**
     * Processes a class-instantiation node from the worklist.
     * @param node statement node to process.
     * @param relDefUse relevant variables at that node.
     * @param worklist worklist.
     * @param typeSet set of exception types inferred so far; the type inferred
     *                at <code>node</code>, if any, is added to
     *                <code>typeSet</code>.
     */
    private int processInstantiationNode( StatementNode node,
                                          DefUse relDefUse, Vector worklist,
                                          Vector typeSet )
    {
      // jim jones -- had to modify so that this would work with minimal
      // rework now that nodes only have at most one definition
      DefUse def = node.getDefinition();
      DefUse[] defs;
      if ( def == null )
	defs = new DefUse[0];
      else
	{
	  defs = new DefUse[1];
	  defs[0] = (jaba.du.DefUse) def;
	}
        //System.out.println( "Definitions = " );
        for ( int i=0; i<defs.length; i++ ) {
            //System.out.println( defs[i].toString() );
            if ( defs[i].equals( relDefUse ) ) {
                NewInstanceAttribute attr = Factory.getNewInstanceAttribute (node);
                // node must have a new-instance attribute
                if ( attr == null ) {
		    RuntimeException e = new RuntimeException();
		    e.printStackTrace();
		    throw e;
		}
                ReferenceType rt = attr.getObjectType();
                // object must be a class type
                if ( !(rt instanceof Class ) ) {
		    RuntimeException e = new RuntimeException();
		    e.printStackTrace();
		    throw e;
		}
                Class c = (Class)rt;
                //System.out.println( rt.getName() );
                if ( !typeSet.contains( c ) ) {
                    typeSet.addElement( c );
                    return 1;
                }
                return 0;
            }
        }
        addPredecessorsToWorklist( node, relDefUse, worklist );
        return 0;
    }

    /**
     * Processes a method-call node from the worklist.
     * @param node statement node to process.
     * @param relDefUse relevant variables at that node.
     * @param worklist worklist.
     * @param typeSet set of exception types inferred so far; the type inferred
     *                at <code>node</code>, if any, is added to
     *                <code>typeSet</code>.
     */
    private int processMethodCallNode( StatementNode node,
                                       DefUse relDefUse, Vector worklist,
                                       Vector typeSet )
    {
      // jim jones -- had to modify so that this would work with minimal
      // rework now that nodes only have at most one definition
      DefUse def = node.getDefinition();
      DefUse[] defs;
      if ( def == null )
	defs = new DefUse[0];
      else
	{
	  defs = new DefUse[1];
	  defs[0] = (jaba.du.DefUse) def;
	}
        for ( int i=0; i<defs.length; i++ ) {
            if ( defs[i].equals( relDefUse ) ) {
                HasValue var = ((ReferenceDefUse)relDefUse).getVariable();
                if ( !( var instanceof TempVariable ) ) {
                    Collection u = node.getUsesCollection ();
                    addPredecessorsToWorklist( node, (DefUse) u.iterator ().next (), worklist ); // get the first element of u
                    return 0;
                }
                MethodCallAttribute attr = Factory.getMethodCallAttribute (node);
                // node must have a method-call attribute
                if ( attr == null ) {
		    RuntimeException e = new RuntimeException();
		    e.printStackTrace();
		    throw e;
		}
                Vector methods = attr.getMethodsVector ();
                int count = 0;
                for (Iterator j = methods.iterator (); j.hasNext ();)
		{
                    TypeEntry type = ((Method) j.next ()).getType ();
                    // return type of method must be a class type
                    if ( !(type instanceof Class ) ) {
			RuntimeException e = new RuntimeException();
			e.printStackTrace();
			throw e;
		    }
                    if ( !typeSet.contains( type ) ) {
                        typeSet.addElement( type );
                    }
                    count++;
                    Collection types = ((Class) type).getAllSubClassesCollection ();
                    for (Iterator k = types.iterator (); k.hasNext ();)
		    {
			Class c = (Class) k.next ();
                        if (!typeSet.contains (c))
			{
                            typeSet.addElement (c);
                        }
                        count++;
                    }
                }
                return count;
            }
        }
        addPredecessorsToWorklist( node, relDefUse, worklist );
        return 0;
    }

    /**
     * Processes an entry or catch node from the worklist.
     * @param node statement node to process.
     * @param relDefUse relevant variables at that node.
     * @param worklist worklist.
     * @param typeSet set of exception types inferred so far; the conservative
     *                type set estimate at <code>node</code> is added to
     *                <code>typeSet</code>.
     */
    private int processEntryNode( StatementNode node, DefUse relDefUse,
                                  Vector worklist, Vector typeSet )
    {
        HasValue relVar;
        if ( relDefUse instanceof LeafDefUse ) {
            relVar = ((LeafDefUse)relDefUse).getVariable();
        }
        else {
            relVar = ((RecursiveDefUse)relDefUse).getLeafElement().
                     getVariable();
        }
        TypeEntry type=null;
        if ( node.getType() == StatementNode.ENTRY_NODE ) {
            if ( !(relVar instanceof LocalVariable) &&
                 !(relVar instanceof Field) ) {
		RuntimeException e = new RuntimeException();
		e.printStackTrace();
		throw e;
		}
        }
        else { // catch node
            if ( !(relVar instanceof LocalVariable) &&
                 !(relVar instanceof Field) &&
                 !(relVar instanceof NewInstance) &&
		 !(relVar instanceof PlaceholderLocation ) ) {
		RuntimeException e = new RuntimeException();
		e.printStackTrace();
		throw e;
		}
	    // jim jones  may 3, 1999 -- changed "instanceof NewInstance" to
	    // "instanceof PlaceholderLocation" to reflect the division of
	    // these two types, and because DefUseInitializer is now placing
	    // this on the stack when a catch node is found
            if ( relVar instanceof PlaceholderLocation ) {
                ExceptionAttribute attr = Factory.getExceptionAttribute (node);
                //System.out.println( attr.getExceptionType().getName() );
                type = attr.getExceptionType();
            }
        }
        if ( relVar instanceof LocalVariable ) {
            type = (jaba.sym.TypeEntry) ((LocalVariable)relVar).getType();
        }
        else if ( relVar instanceof Field ) {
            type = (jaba.sym.TypeEntry) ((Field)relVar).getType();
        }
        if ( !(type instanceof Class ) ) {
	    RuntimeException e = new RuntimeException();
	    e.printStackTrace();
	    throw e;
		}
        int count = 0;
        if ( !typeSet.contains( type ) ) {
            typeSet.addElement( type );
        }
        count++;
        Collection types = ((Class) type).getAllSubClassesCollection ();
        for (Iterator i = types.iterator (); i.hasNext ();)
	{
		Class c = (Class) i.next ();
            if (!typeSet.contains (c))
	    {
                typeSet.addElement (c);
            }
            count++;
        }

        return count;
    }

    /**
     * Adds the precessors of the given node, with the given relevant var
     * to the worklist.
     * @param node node whose predecessors are to be added to worklist.
     * @param relVar variable that is relevant for data-flow analysis at the
     *               the predecessors.
     * @param worklist list in which to add node.
     */
    private void addPredecessorsToWorklist( StatementNode node,
                                            DefUse relDefUse, Vector worklist )
    {
        Edge [] inEdges = cfg.getInEdges (node);
        if ( inEdges == null ) {
            return;
        }
        for ( int i=0; i<inEdges.length; i++ ) {
            StatementNode pred =
                (StatementNode)inEdges[i].getSource();
            addNodeToWorklist( new WorklistNode( pred, relDefUse ),
                               worklist );
        }
    }

    /**
     * Adds a <code>WorklistNode</code> to the worklist.
     * @param node node to be added to worklist.
     * @param worklist worklist to add node to.
     */
    private void addNodeToWorklist( WorklistNode node, Vector worklist )
    {
        for (Iterator e = worklistNodes.iterator (); e.hasNext ();)
	{
            WorklistNode n = (WorklistNode) e.next ();
            if (node.equals (n))
	    {
                return;
            }
        }
        worklist.addElement( node );
        worklistNodes.addElement( node );
    }

    /** ??? */
    private void addToSafeCatchTypeSet( DataFlowAnalysisInfo info, int val ) {
        if ( info.safeCatchTypeSetSize == null ) {
            info.safeCatchTypeSetSize = new int[1];
            info.safeCatchTypeSetSize[0] = val;
            return;
        }
        int[] t = new int[info.safeCatchTypeSetSize.length+1];
        for ( int i=0; i<info.safeCatchTypeSetSize.length; i++ ) {
            t[i] = info.safeCatchTypeSetSize[i];
        }
        t[t.length-1] = val;
        info.safeCatchTypeSetSize = t;
    }

    /** ??? */
    private void addToSafeMethodCallTypeSet( DataFlowAnalysisInfo info,
                                             int val )
    {
        if ( val == 0 ) {
            return;
        }
        if ( info.safeMethodCallTypeSetSize == null ) {
            info.safeMethodCallTypeSetSize = new int[1];
            info.safeMethodCallTypeSetSize[0] = val;
            return;
        }
        int[] t = new int[info.safeCatchTypeSetSize.length+1];
        for ( int i=0; i<info.safeCatchTypeSetSize.length; i++ ) {
            t[i] = info.safeCatchTypeSetSize[i];
        }
        t[t.length-1] = val;
        info.safeMethodCallTypeSetSize = t;
    }

    /** CFG on which type inference is performed. */
    private CFG cfg;

    /** Number of throw statements processed by this <code>TypeInference</code> object. */
    private int throwStatementCount;

    /** Throw statements of with expression type <code>NEW_INSTANCE_THROW_EXPR</code>. */
    private int newInstanceExprCount;

    /** Throw statements of with expression type <code>METHOD_CALL_THROW_EXPR</code>. */
    private int methodCallExprCount;

    /** Throw statements of with expression type <code>VAR_THROW_EXPR</code>. */
    private int varExprCount;

    /** Average type set size for cases when data-flow analysis failed - reached
     *  entry with a non-empty relevant variable set.
     */
    private int avgFailureTypeSet;

    /** Average type set size for method-call expr. */
    private int avgMethodCallExprTypeSet;

    /** Largest type set in cases where data-flow analysis failed. */
    private int maxFailureTypeSet;

    /** Information gathered during data-flow analysis. */
    private Vector dataFlowInfo;
}
