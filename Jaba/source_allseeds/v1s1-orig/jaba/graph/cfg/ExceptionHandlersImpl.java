/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.cfg;

import jaba.classfile.ExceptionHandler;

import jaba.constants.OpCode;

import jaba.sym.ConstantPoolMap;
import jaba.sym.Class;
import jaba.sym.Method;
import jaba.sym.MethodAttribute;

import jaba.debug.Debug;

import java.util.Vector;
import java.util.HashMap;
import java.util.Enumeration;

/**
 * Encapsulates an exception-handler table, and provides various methods
 * to access information in the table.
 * <p>
 * An <code>ExceptionHandlers</code> object is created from
 * an array of <code>ExceptionHandler</code> objects that hold
 * exception-handler information in the form that it is represented
 * in a class file. The class provides several methods to access
 * information about catch handlers and finally blocks that is required
 * during the CFG construction. <code>ExceptionHandlers</code> is created
 * and used by </code>CFG.load()</code>.
 *
 * @author S. Sinha
 * @see jaba.classfile.ExceptionHandler
 * @see CFG
 * @author Huaxing Wu -- <i> Revised 7/18/02. Change getTryStatements method,
 *                       add getSynchronizedTryStatements method </i>
 * @author Jay Lofstead 2003/05/29 added toString that outputs XML
 * @author Jay Lofstead 2003/06/02 removed elementAt in favor of enumeration/iteration for better performance.
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/07/10 changed to implement ExceptionHandlers
 */
public class ExceptionHandlersImpl implements MethodAttribute, java.io.Serializable, ExceptionHandlers
{
	/** ??? */
    private ExceptionHandler[] exceptionTable;

	/** ??? */
    private byte[] bytecode;

    /** Method for which the exception handlers attribute is created */
    private Method method;

    /** Try statements in the corresponding method */
    //   private Vector tryStmts;
    TryStatement[] tryStmtsArr;

    /** Map from an interger catch type to class of corresponding exception
        type. */
    private HashMap exceptionTypes;

    /** Entries for finally blocks in an exception-handler table. */
    private FinallyBlock[] finallyBlocks;

    /** Boolean indicating if a finally block contains unconditional
     *  break/continue/return/throw; the exit offset for such finally
     *  blocks cannot be determined. Therefore, the CFG builder inlines
     *  the finally blocks into the CFG of the containing method
     */
    boolean inlineFinally;

    /**
     * Constructs an <code>ExceptionHandlers</code> object. Initializes
     * internal data structures that hold information about finally blocks.
     * @param exceptionTable exception-handler table for a method.
     * @param bytecode array of bytecode for a method.
     * @param constantPoolMap map from a constant-pool index to a symbol-table
     *                        object.
     * @throws MalformedTryStatementException if an exception is raised
     *                                        during initialization of the
     *                                        finally-block information.
     * @throws SymbolNotFoundException if <code>constantPoolMap</code> does
     *                                 not map a catch type from
     *                                 <code>exceptionTable</code> to a
     *                                 <code>Class</code>.
     */
    ExceptionHandlersImpl ( Method method, ExceptionHandler[] exceptionTable,
                       byte[] bytecode, ConstantPoolMap constantPoolMap )
        throws MalformedTryStatementException,
	       SymbolNotFoundException
    {
	this.method = method;
        this.exceptionTable = exceptionTable;
	inlineFinally = false;
        this.bytecode = new byte[bytecode.length];
        for ( int i=0; i<bytecode.length; i++ ) {
            this.bytecode[i] = bytecode[i];
        }
	// initialize mapping from integer catch type to exception type
        initExceptionTypes( constantPoolMap );

	// initialize try statements
	Vector tryStmts = new Vector(); //Helper
	tryStmtsArr = null;
	
	for ( int i = 0; i < exceptionTable.length; ) {
	    int tryStartOffset = exceptionTable[i].getStartPC();

	    // Skip over entries for which the handler PC is equal to the
	    // start PC. Such entries represent synthetic finally blocks
	    // added by the compiler to trap exceptions raised during
	    // the housekeeping required for exceptional invocation of
	    // finally blocks. Exceptions raised during the housekeeping
	    // cause the recursive invocation of the same finally block.
	    if ( tryStartOffset == exceptionTable[i].getHandlerPC() ) {
		i++;
		continue;
	    }

	    int tryCurrOffset = tryStartOffset;
	    Vector tryStmtEntries = new Vector();
	    // identify entries in the exception table that correspond to a
	    // try statement: each try statement has a different start PC;
	    // therefore, all entries with the same start PC belong to the
	    // same try statement
	    while ( tryCurrOffset == tryStartOffset ) {
		tryStmtEntries.add( exceptionTable[i] );
		i++;
		if ( i < exceptionTable.length ) {
		    tryCurrOffset = exceptionTable[i].getStartPC();
		}
		else {
		    break;
		}
	    }
	    // instantiate the try statement
	    TryStatement tryStmt = new TryStatementImpl ( tryStmtEntries, bytecode,
						     constantPoolMap );
	    tryStmts.add( tryStmt );
	}

	tryStmtsArr = (TryStatement[])tryStmts.toArray(new TryStatement[0]);

	// store links to finally blocks locally for efficient access
	// during the CFG construction; also set the exit offset for each
	// non-synthetic finally block---these could not be set during the
	// creation of the finally-block objects because the algorithm for
	// determining the the exit offset requires that the jump offset of
	// finally block be initialized (and this information is not
	// available to the constructor of FinallyBlock; the exit offsets for
	// synthetic finally blocks is set by the constructor of
	// FinallyBlock
	Vector finallyBlks = new Vector();
	for ( int i = 0; i < tryStmtsArr.length; i++ ) {
	    FinallyBlock fb = tryStmtsArr[i].getFinallyBlock();
	    if ( fb != null ) {
		finallyBlks.add( fb );
	    }
	}
	int numFinallyBlocks = finallyBlks.size();
	Debug.println( "Number of finally blocks in method = "+
		       numFinallyBlocks, Debug.CFG, 2);
	finallyBlocks = new FinallyBlock [numFinallyBlocks];
	int i = 0;
	for (Enumeration e = finallyBlks.elements (); e.hasMoreElements (); i++)
	{
	    finallyBlocks[i] = (FinallyBlock) e.nextElement ();
	    if ( finallyBlocks[i] == null )
	    {
		throw new RuntimeException( "NULL entry for finally block!!\n"+
					    "Method "+method.getFullyQualifiedName());
	    }
	    if ( ((FinallyBlockImpl) finallyBlocks[i]).isSynthetic )
	    {
		if ( ((FinallyBlockImpl) finallyBlocks[i]).exitOffset == 0 )
		{
		    throw new RuntimeException("Error:  exitOffset "+
			"not set for non-synthetic finally block: "+
			finallyBlocks[i]);
		}
		continue;
	    }
	}
	for (i = 0; i < numFinallyBlocks; i++ )
	{
 	    if ( !((FinallyBlockImpl) finallyBlocks[i]).isSynthetic )
	    {
		((FinallyBlockImpl) finallyBlocks[i]).exitOffset =
		    getExitOffset( ((FinallyBlockImpl) finallyBlocks[i]).entryOffset );
		Debug.println( "  "+i+": "+finallyBlocks[i], Debug.CFG, 2 );
	    }
	}
	    
    }

    /**
     * Constructs an <code>ExceptionHandlers</code> object for the
     * specified method and containing the specified try statements.
     * The specified try statements are those that are contained
     * in the method and not in a sub-method.
     * @param method method for which this <code>ExceptionHandlers</code>
     *               attribute is created.
     * @param tryStmts try statements that are contained in the method and
     *                 not in a sub-method.
     */
    ExceptionHandlersImpl ( Method method, TryStatement[] tryStmts )
    {
	this.method = method;
	tryStmtsArr = tryStmts;
	exceptionTable = null;
	bytecode = null;
	exceptionTypes = null;
	finallyBlocks = null;
    }

    /**
     * Initalizes the map from catch type to exception class.
     * @param constantPoolMap map from a constant-pool index to a symbol-table
     *                        object.
     * @throws SymbolNotFoundException if <code>constantPoolMap</code> does
     *                                 not map a catch type from
     *                                 <code>exceptionTable</code> to a
     *                                 <code>Class</code>.
     */
    private void initExceptionTypes( ConstantPoolMap constantPoolMap )
        throws SymbolNotFoundException
    {
        exceptionTypes = new HashMap();
        for ( int i=0; i<exceptionTable.length; i++ ) {
            int catchType = exceptionTable[i].getCatchType();
            if ( catchType == 0 ) {  // skip finally block
                continue;
            }
            Integer key = new Integer( catchType );
            Object obj = constantPoolMap.get( key );
            if ( obj == null ) {
                throw new SymbolNotFoundException(
                    "ExceptionHandlers.initExceptionTypes(): constant pool "+
                    "index "+catchType+" for catch handler does not map to a"+
                    " SymbolTableEntry object." );
            }
            if ( !(obj instanceof Class) ) {
                throw new SymbolNotFoundException(
                    "ExceptionHandlers.initExceptionTypes(): constant pool "+
                    "index "+catchType+ " for catch handler does not map to"+
                    " expected Class type: "+obj.getClass().getName() );
            }
            exceptionTypes.put( key, (Class)obj );
        }
    }

    /**
     * Returns the {@link FinallyBlock#exitOffset exitOffset} of a finally
     * block that appears first after the given offset in the bytecode array.
     * The exit offset may be 0 if the finally block has no RET
     * instruction; this happens for finally blocks that contain an
     * unconditional break, continue, return, or throw statement.
     * @param offset offset in bytecode array to start search from.
     * @return exit offset of a finally block.
     */
    private int getExitOffset( int offset )
    {
        int depth = 1;
        for ( int i=offset; i<bytecode.length; i++ ) {
            if ( isFinallyBlock( i ) ) {
                depth++;
            }
            if ( bytecode[i] == OpCode.OP_RET ) {
                depth--;
                if ( depth == 0 ) {
                    return ( bytecode[i-1] == OpCode.OP_WIDE ) ? (i-1) : i;
                }
            }
        }
	// exit offset not found for finally; this should be because the
	// finally block contains unconditional break/continue/return/throw;
	// set inlineFinally flag to true
	inlineFinally = true;
	return 0;
    }

    /**
     * Checks if a handler in the exception-handler table corresponds to a
     * non-synthetic finally block.
     * @param offset jump offset from the exception-handler table.
     * @return <code>true</code> if handler is for a finally block,
     *         <code>false</code> otherwise.
     */
    private boolean isFinallyBlock( int offset ) {
	for ( int j=0; j<finallyBlocks.length; j++ ) {
	    if ( finallyBlocks[j] == null ) {
		throw new RuntimeException( "Finally block is null at offset "+j );
	    }
	    if ( ((FinallyBlockImpl) finallyBlocks[j]).jumpOffset == offset ) {
		if ( ((FinallyBlockImpl) finallyBlocks[j]).isSynthetic ) {
		    return false;
		}
		return true;
	    }
	}
        return false;
    }

    /**
     * Updates bytecode array to reset instructions that need not be
     * be considered during the CFG construction. Such instructions are either
     * instructions for a synthetic finally block (created to ensure monitor
     * release), or instructions that lie in the range
     * [<code>jumpOffset</code>, <code>entryOffset</code>] for a
     * non-synthetic finally block. The reset bytecode entries are set
     * to {@link jaba.constants.OpCode#OP_UNDEF OpCode.OP_UNDEF}.
     * @param bytecode bytecode array to be updated.
     */
    void updateBytecodeArray( byte[] bytecode ) {
        for ( int i=0; i<finallyBlocks.length; i++ ) {
            if ( ((FinallyBlockImpl) finallyBlocks[i]).isSynthetic ) {
                for ( int j=((FinallyBlockImpl) finallyBlocks[i]).entryOffset;
                      j<=((FinallyBlockImpl) finallyBlocks[i]).exitOffset; j++ ) {
                    bytecode[j] = OpCode.OP_UNDEF;
                }
            }
            else {
                for ( int j=((FinallyBlockImpl) finallyBlocks[i]).jumpOffset;
                      j<((FinallyBlockImpl) finallyBlocks[i]).cfgStartOffset; j++ ) {
                    bytecode[j] = OpCode.OP_UNDEF;
                }
            }
        }
    }

    /**
     * Returns the CFG-start offsets for top level finally blocks in the
     * specified instruction range.
     * @param start start of the instruction range.
     * @param end end of the instruction range.
     * @return Array of {@link FinallyBlock#cfgStartOffset cfgStartOffsets} for
     *         top-level finally blocks in the instruction range.
     */
    int[] getToplevelFinallyCFGStartOffsets( int start, int end ) {
        Vector v = new Vector();
        for ( int i=0; i<finallyBlocks.length; i++ ) {
            if ( ((FinallyBlockImpl) finallyBlocks[i]).isSynthetic ) {
                continue;
            }
            if ( ((FinallyBlockImpl) finallyBlocks[i]).cfgStartOffset > end ) {
                break;
            }
            if (  ((FinallyBlockImpl) finallyBlocks[i]).cfgStartOffset > start ) {
                v.addElement( new Integer( ((FinallyBlockImpl) finallyBlocks[i]).cfgStartOffset ) );
                start = ((FinallyBlockImpl) finallyBlocks[i]).exitOffset;
            }
        }
        int[] r = new int[v.size()];
	int i = 0;
        for (Enumeration e = v.elements (); e.hasMoreElements (); i++)
	{
            r[i] = ((Integer) e.nextElement ()).intValue();
        }
        return r;
    }

    /**
     * Returns an array of finally-block end offsets corresponding to
     * an array of CFG start offsets for finally blocks.
     * @param startOffset Array of
                          {@link FinallyBlock#cfgStartOffset cfgStartOffsets}.
     * @return Array of {@link FinallyBlock#exitOffset exitOffsets}. An exit
     *         offset at index <code>i</code> corresponds to the finally block
     *         whose CFG-start offset is at index <code>i</code> in
     *         <code>startOffset</code>.
     */
    int[] getFinallyEndOffsets( int[] startOffset ) {
        int[] r = new int[startOffset.length];
        for ( int i=0; i<startOffset.length; i++ ) {
            for ( int j=0; j<finallyBlocks.length; j++ ) {
                if ( ((FinallyBlockImpl) finallyBlocks[j]).cfgStartOffset == startOffset[i] ) {
                    r[i] = ((FinallyBlockImpl) finallyBlocks[j]).exitOffset;
                    break;
                }
            }
        }
        return r;
    }

    /**
     * Returns the entry offset ({@link FinallyBlock#entryOffset entryOffset}),
     * given a CFG-start offset
     * ({@link FinallyBlock#cfgStartOffset cfgStartOffset}) for a finally block.
     * @param cfgStartOffset CFG-start offset for a finally block.
     * @return Entry offset for that finally block, or <code>-1</code> if
     *         <code>cfgStartOffset</code> is invalid.
     */
    int getFinallyEntryOffset( int cfgStartOffset ) {
        for ( int i=0; i<finallyBlocks.length; i++ ) {
            if ( ((FinallyBlockImpl) finallyBlocks[i]).isSynthetic ) {
                continue;
            }
            if ( ((FinallyBlockImpl) finallyBlocks[i]).cfgStartOffset == cfgStartOffset ) {
                return ((FinallyBlockImpl) finallyBlocks[i]).entryOffset;
            }
        }
        return -1;
    }

    /**
     * Returns the jump offset ({@link FinallyBlock#entryOffset entryOffset}),
     * given a CFG-start offset
     * ({@link FinallyBlock#cfgStartOffset cfgStartOffset}) for a finally block.
     * @param cfgStartOffset CFG-start offset for a finally block.
     * @return Jump offset for that finally block, or <code>-1</code> if
     *         <code>cfgStartOffset</code> is invalid.
     */
    int getFinallyJumpOffset( int cfgStartOffset ) {
        for ( int i=0; i<finallyBlocks.length; i++ ) {
            if ( ((FinallyBlockImpl) finallyBlocks[i]).isSynthetic ) {
                continue;
            }
            if ( ((FinallyBlockImpl) finallyBlocks[i]).cfgStartOffset == cfgStartOffset ) {
                return ((FinallyBlockImpl) finallyBlocks[i]).jumpOffset;
            }
        }
        return -1;
    }

    /**
     * Returns the finally-block name ({@link FinallyBlock#name name}),
     * given the CFG-start offset
     * ({@link FinallyBlock#cfgStartOffset cfgStartOffset}) for a finally block.
     * @param cfgStartOffset CFG-start offset for a finally block.
     * @return Name for that finally block, or <code>null</code> if
     *         <code>cfgStartOffset</code> is invalid.
     */
    String getFinallyNameForCFGStartOffset( int cfgStartOffset ) {
        for ( int i=0; i<finallyBlocks.length; i++ ) {
            if ( ((FinallyBlockImpl) finallyBlocks[i]).isSynthetic ) {
                continue;
            }
            if ( ((FinallyBlockImpl) finallyBlocks[i]).cfgStartOffset == cfgStartOffset ) {
                return ((FinallyBlockImpl) finallyBlocks[i]).name;
            }
        }
        return null;
    }

    /**
     * Returns the finally-block name ({@link FinallyBlock#name name}),
     * given the jump offset
     * ({@link FinallyBlock#jumpOffset jumpOffset}) for a finally block.
     * @param jumpOffset Jump offset for a finally block.
     * @return Name for that finally block, or <code>null</code> if
     *         <code>jumpOffset</code> is invalid.
     */
    String getFinallyNameForJumpOffset( int jumpOffset ) {
        for ( int i=0; i<finallyBlocks.length; i++ ) {
            if ( ((FinallyBlockImpl) finallyBlocks[i]).isSynthetic ) {
                continue;
            }
            if ( ((FinallyBlockImpl) finallyBlocks[i]).jumpOffset == jumpOffset ) {
                return ((FinallyBlockImpl) finallyBlocks[i]).name;
            }
        }
        return null;
    }

    /**
     * Returns the finally-block name ({@link FinallyBlock#name name}),
     * given an entry offset
     * ({@link FinallyBlock#entryOffset entryOffset}) for a finally block.
     * @param entryOffset Entry offset for a finally block.
     * @return Name for that finally block, or <code>null</code> if
     *         <code>entryOffset</code> is invalid.
     */
    String getFinallyNameForEntryOffset( int entryOffset ) {
        for ( int i=0; i<finallyBlocks.length; i++ ) {
            if ( ((FinallyBlockImpl) finallyBlocks[i]).isSynthetic ) {
                continue;
            }
            if ( ((FinallyBlockImpl) finallyBlocks[i]).entryOffset == entryOffset ) {
                return ((FinallyBlockImpl) finallyBlocks[i]).name;
            }
        }
        return null;
    }

    /**
     * Returns the bytecode offset of the RET instruction given the entry
     * offset ({@link FinallyBlock#jumpOffset jumpOffset}) for a finally
     * block. The exit offset may be 0 if the finally block has no RET
     * instruction; this happens for finally blocks that contain an
     * unconditional break, continue, return, or throw statement.
     * @param jumpOffset Jump offset for a finally block.
     * @return Exit offset (offset of RET instruction) for finally, or
     *         <code>-1</code> if <code>jumpOffset</code> is invalid.
     */
    int getFinallyExitOffsetForEntryOffset( int entryOffset ) {
        for ( int i=0; i<finallyBlocks.length; i++ ) {
            if ( ((FinallyBlockImpl) finallyBlocks[i]).isSynthetic ) {
                continue;
            }
            if ( ((FinallyBlockImpl) finallyBlocks[i]).entryOffset == entryOffset ) {
                return ((FinallyBlockImpl) finallyBlocks[i]).exitOffset;
            }
        }
        return -1;
    }

    /**
     * Returns an array of catch-handler offsets for a method.
     * @return Array of offsets for the beginning of catch-handler code.
     */
    int[] getCatchHandlerOffsets() {
        Vector v = new Vector();
        for ( int i=0; i<exceptionTable.length; i++ ) {
            if ( exceptionTable[i].getCatchType() == 0 ) {
                continue;
            }
            v.addElement( new Integer( exceptionTable[i].getHandlerPC() ) );
        }
        int[] r = new int[v.size()];
	int i = 0;
        for (Enumeration e = v.elements (); e.hasMoreElements (); i++)
	{
            r[i] = ((Integer) e.nextElement ()).intValue();
        }
        return r;
    }

    /**
     * Returns an array of handler types (including finally-blocks)
     * that enclose the given bytecode offset, and occur within the specified
     * range of instructions, in the bytecode array.
     * @param offset offset in bytecode array.
     * @param firstInstrOffset start offset for the range of instructions
     *                         within which to search for handlers.
     * @param lastInstrOffset end offset for the range of instructions
     *                        within which to search for handlers.
     * @return Array of handlers that enclose <code>offset</code>. The array
     *         contains a <code>Class</code> for each catch handler that
     *         encloses <code>offset</code> and a <code>null</code> for
     *         each finally block that encloses <code>offset</code>.
     */
    Class[] getEnclosingHandlers( int offset, int firstInstrOffset,
				  int lastInstrOffset ) {
        Vector v = new Vector();
        outerLoop:
        for ( int i=0; i<exceptionTable.length; i++ ) {
            if ( offset >= exceptionTable[i].getStartPC() &&
                 offset <= exceptionTable[i].getEndPC() ) {
                int catchType = exceptionTable[i].getCatchType();
		int jumpOffset = exceptionTable[i].getHandlerPC();
                if ( catchType == 0 ) {
                    // check if finally is synthetic
                    for ( int j=0; j<finallyBlocks.length; j++ ) {
                        if ( ((FinallyBlockImpl) finallyBlocks[j]).jumpOffset == jumpOffset ) {
                            if ( ((FinallyBlockImpl) finallyBlocks[j]).isSynthetic ) {
                                continue outerLoop;
                            }
                        }
                    }
                }
		if ( jumpOffset >= firstInstrOffset &&
		     jumpOffset <= lastInstrOffset ) {
		    v.addElement( new Integer( catchType ) );
		}
            }
        }
        Class[] r = new Class[v.size()];
	int i = 0;
        for (Enumeration e = v.elements (); e.hasMoreElements (); i++)
	{
            Integer catchType = (Integer) e.nextElement ();
            if ( catchType.intValue() == 0 )
	    {
                r[i] = null;

                continue;
            }
            r[i] = (Class)exceptionTypes.get( catchType );
        }
        return r;
    }

    /**
     * Returns an array of handler offsets (including finally-blocks offsets)
     * that enclose the given bytecode offset, and occur within the specified
     * range of instructions, in the bytecode array.
     * @param offset offset in bytecode array.
     * @param firstInstrOffset start offset for the range of instructions
     *                         within which to search for handlers.
     * @param lastInstrOffset end offset for the range of instructions
     *                        within which to search for handlers.
     * @return Array of offsets for handlers that enclose <code>offset</code>.
     */
    int[] getEnclosingHandlerOffsets( int offset, int firstInstrOffset,
				      int lastInstrOffset ) {
        Vector v = new Vector();
        outerLoop:
        for ( int i=0; i<exceptionTable.length; i++ ) {
            if ( offset >= exceptionTable[i].getStartPC() &&
                 offset <= exceptionTable[i].getEndPC() ) {
                int jumpOffset = exceptionTable[i].getHandlerPC();
                if ( exceptionTable[i].getCatchType() == 0 ) {
                    // check if finally is synthetic
                    for ( int j=0; j<finallyBlocks.length; j++ ) {
                        if ( ((FinallyBlockImpl) finallyBlocks[j]).jumpOffset == jumpOffset ) {
                            if ( ((FinallyBlockImpl) finallyBlocks[j]).isSynthetic ) {
                                continue outerLoop;
                            }
                        }
                    }
                }
		if ( jumpOffset >= firstInstrOffset &&
		     jumpOffset <= lastInstrOffset ) {
		    v.addElement( new Integer( jumpOffset ) );
		}
            }
        }
        int[] r = new int[v.size()];
	int i = 0;
        for (Enumeration e = v.elements (); e.hasMoreElements (); i++)
	{
            r[i] = ((Integer) e.nextElement ()).intValue();
        }
        return r;
    }

    /**
     * Returns the class for an integer catch type.
     * @param type integer catch-handler type.
     * @return Class of that catch-handler type.
     */
    Class getCatchHandlerClass( int type ) {
        return (Class)exceptionTypes.get( new Integer( type ) );
    }

    /**
     * Returns the type of a catch handler given the beginning offset of
     * the instructions for that catch handler.
     * @param offset beginning offset of instructions for catch handler.
     * @return Type of catch handler, or <code>null</code> if
     *         <code>offset</code> is invalid. The type is class of the
     *         exception.
     */
    Class getCatchHandlerType( int offset ) {
        for ( int i=0; i<exceptionTable.length; i++ ) {
            if ( exceptionTable[i].getHandlerPC() == offset ) {
                Integer key = new Integer( exceptionTable[i].getCatchType() );
                return (Class)exceptionTypes.get( key );
            }
        }
        return null;
    }

    /**
     * Removes the try statements that fall within the specified instruction
     * ranges and returns the removed try statements.
     * @param start start offset of instruction range.
     * @param end end offset of instruction range.
     * @return Array of try statements that are removed.
     */
    TryStatement[] removeTryStatementsInRange( int start, int end ) {
	Vector removedTryStmts = new Vector();
	Vector remainingTryStmts = new Vector();
	for ( int i = 0; i < tryStmtsArr.length; i++ ) {
	    TryBlock tryBlock = tryStmtsArr[i].getTryBlock();
	    if ( tryBlock.getStartByteCodeOffset() >= start &&
		 tryBlock.getEndByteCodeOffset() <= end ) {
		removedTryStmts.add( tryStmtsArr[i] );
	    }
	    else {
		remainingTryStmts.add( tryStmtsArr[i] );
	    }
	}
	tryStmtsArr = ( TryStatement[] )
	    remainingTryStmts.toArray( new TryStatement[0] );
	return ( TryStatement[] )
	    removedTryStmts.toArray( new TryStatement[0] );
    }

    /**
     * Returns the method associated with this exception-handlers object.
     * @return Method associated with this exception-handlers object.
     */
    public Method getMethod() {
	return method;
    }

    /**
     * Returns the try statements that appear in the method associated with
     * this exception-handlers object.
     * Note: Compiler will generate an exception entry for synchronized 
     * statements. This method will not return synchronized statements.
     * @return Try statements that appear in the method associated with
     *         this exception-handlers object.
     */
    public TryStatement[] getTryStatements() {
	Vector result = new Vector(tryStmtsArr.length);
	for(int i = 0; i < tryStmtsArr.length; i++ ) {
	    if(!((TryStatementImpl) tryStmtsArr[i]).isSynthetic() ) {
		result.add(tryStmtsArr[i]);
	    }
	}
	return (TryStatement[])result.toArray( new TryStatement[0]);
    }

    /**
     * Returns the Synchronized statements that appear in the method 
     * associated with this exception-handlers object. 
     * Note: Compilers will generate exception entry for 
     * synchronized statements 
     * @return Synchronized statements that appear in the method associated 
     * with this exception-handlers object.
     */
    public TryStatement[] getSynchronizedTryStatements() {
	Vector result = new Vector(tryStmtsArr.length);
	 for(int i = 0; i < tryStmtsArr.length; i++ ) {
	     if(((TryStatementImpl) tryStmtsArr[i]).isSynthetic() ) {
		 result.add(tryStmtsArr[i]);
	     }
	 }
	 return (TryStatement[])result.toArray(new TryStatement[0]);
    }

	/**
	 * returns an XML format of the object
	 */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<exceptionhandlers>");

		for (int i = 0; exceptionTable != null && i < exceptionTable.length; i++)
		{
			buf.append (exceptionTable [i]);
		}

		buf.append ("<method>" + method.getID () + "</method>");

		for (int i = 0; tryStmtsArr != null && i < tryStmtsArr.length; i++)
		{
			buf.append (tryStmtsArr [i]);
		}

		java.util.Set keySet = exceptionTypes.keySet ();
		for (java.util.Iterator i = keySet.iterator (); i.hasNext ();)
		{
			Integer key = (Integer) i.next ();
			Class c = (Class) exceptionTypes.get (key);

			buf.append ("<exceptiontype>");
			buf.append ("<key>").append (key).append ("</key>");
			buf.append ("<value>").append (jaba.tools.Util.toXMLValid (c.getName ())).append ("</value>");
			buf.append ("</exceptiontype>");
		}

		for (int i = 0; i < finallyBlocks.length; i++)
		{
			buf.append (finallyBlocks [i]);
		}

		buf.append ("<inlinefinally>").append (inlineFinally).append ("</inlinefinally>");

		buf.append ("</exceptionhandlers>");

		return buf.toString ();
	}
}
