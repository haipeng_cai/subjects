package jaba.graph;

import jaba.graph.Node;

/**
 * This node type represents an External Code Node.
 * @author Manas Tungare -- <i>Created, November 25, 2001.</i>.
 * @author Huaxing Wu -- <i> Add method getColor, getShape, getNodeTypeName </i>
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/04 added call to getString () in toString ()
 * @author Jay Lofstead 2003/06/06 changed XML type to just node for ease of manipulation
 * @author Jay Lofstead 2003/06/25 added implements serializable to support RMI
 */
public class ExternalCodeNode extends NodeImpl implements java.io.Serializable
{
    /**
     * Returns the shape of this object.
     *@return the shape of the type
     */
    public String getShape() { return "ellipse"; }
    
    /**
    * Returns the color of this object.
    *@return the color of the type
    */
    public String getColor() { return "yellow"; }
    
    /**
     * Returns the type name of this object.
     *@return the name of its type
     */
    public String getNodeTypeName() { return "ECN"; }
    public String toString()
	{
		return "<node>" + getString () + "</node>";
    }
}

