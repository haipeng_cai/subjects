/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Class;

/**
 * This attribute is placed on all edges from a throw statement.
 * @author Jim Jones -- <i>Created, May 29, 1999</i>.
 * @author Jay Lofstead 2003/05/29 converted toString to generated XML
 * @author Jay Lofstead 2003/07/10 changed to implement ThrowEdgeAttribute
 */
public class ThrowEdgeAttributeImpl extends EdgeAttributeImpl implements ThrowEdgeAttribute
{
  /**
   * All object types that enable the edge that this attribute is attached to,
   * to be traversed.
   */
  private Class traversalType;

  /**
   * Sets the object type that causes this edge to be traversed.
   * @param classType  Object type that causes this edge to be traversed.
   */
  public void setTraversalType( Class classType )
    {
      traversalType = classType;
    }

  /**
   * Returns the object type that cause this edge to be traversed.
   * @return The object type that cause this edge to be traversed.
   */
  public Class getTraversalType()
    {
      return traversalType;
    }

  /**
   * Returns a string representation of this attribute.
   * @return A string representation of this attribute.
   */
  public String toString()
    {
	StringBuffer str = new StringBuffer ();

	str.append ("<throwedgeattribute><traversaltype>");

	if (traversalType == null)
	{
		str.append (traversalType.getName ());
	}
	else
	{
		str.append ("null");
	}

	str.append ("</traversaltype></throwedgeattribute>");

	return str.toString ();
    }
}
