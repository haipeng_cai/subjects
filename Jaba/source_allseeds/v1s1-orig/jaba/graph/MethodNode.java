package jaba.graph;

import jaba.sym.Method;

/**
 * A <code>MethodNode </code> is an upper level representation of a method
 * in a program. A <code>MethodNode</code> is used to construct graphical 
 * analysis for a call-graph.
 *
 * @author Caleb Ho
 * @author Huaxing Wu -- <i> Add method getColor, getShape, getNodeTypeName </i>
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/04 added call to getString () in toString ()
 * @author Jay Lofstead 2003/06/06 changed XML type to just node for ease of manipulation
 * @author Jay Lofstead 2003/06/25 added implements serializable to support RMI
 */
public class MethodNode extends NodeImpl implements java.io.Serializable
{
    // Fields
    /** Used to tabulate the number of MethodNodes created for a particular analysis*/
    protected static int TOTAL_METHOD_NODES = 0;  

    /** A reference to the Method object this MethodNode represents. */
    private Method method;

    /** Stores the number indicating which method this instantiation is*/
    private int methodNodeNumber;

    // Methods

    /**      
     * Constructor for a method node. It creates a new method node, and instantiates the necessary 
     * variables. It increments the static variable TOTAL_METHOD_NODES as to keep track the number of
     * method nodes created up to this pointA method node basically is a software representation of a method
     * used primarily in constructing a call graph
     */  
    public MethodNode ( Method m )
    {
	method = m;
        methodNodeNumber = TOTAL_METHOD_NODES++;
    }
/*
    public void setMethod( Method m )
    {
        method = m;
    }
*/

    /**
     * Accessor to get the reference to the symbolic Method this node represents
     */
    public Method getMethod()
    {
	return (method);
    }

    /**
     * Accessor to get the methodNodeNumber of this method node. The methodNodeNumber is used to assign
     * a numeric id to a method which can be useful in analysis
     */
    public int getMethodNodeNumber()
    {
	return(methodNodeNumber);
    }

    /**
     * The toString of this method node returns the signature of the Method this MethodNode represents.
     * A Method's signature is composed of the method name and its paramaters
     */
    public String toString()
    {
	return "<node>" + getString () + "<signature>" + jaba.tools.Util.toXMLValid (method.getSignature ()) + "</signature></node>";
    }

    /**
     * Returns the color of this object.
     * @return the color of the type
     */
    public String getColor() { return "gray";}

    /**
     * Returns the shape of this object.
     * @return the shape of the type
     */
    public String getShape() { return "ellipse";}

    /**
     * Returns the type name of this object.
     * @return the name of its type
     */
    public String getNodeTypeName() { return "Method";}

}
