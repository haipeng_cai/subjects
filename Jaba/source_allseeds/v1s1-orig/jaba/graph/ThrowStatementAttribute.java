/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Class;

import java.util.Vector;
import java.util.Enumeration;

/**
 * Attribute for a throw statement. The attribute stores information about
 * the type of a throw-statement expression, and the set of possible
 * exception types that can be raised at a throw statement
 * (that are determined by the type-inference algorithm).
 * <p>
 * A throw-statement expression may be one of three types:
 * <l>
 *  <li> New-instance expression - <i>throw new Exception()</i>
 *  <li> Method-call expression - <i>throw object.method()</i>
 *  <li> Variable - <i>throw e </i>
 * </l>
 * <p>
 * The type of a throw-statement expression is set during the partial CFG
 * construction. Following that the type-inference algorithm is
 * invoked, which determines a set of possible exception types, based on
 * the throw-statement expression type, and adds those exception types
 * to the throw-statement attribute. CFG construction then continues, and
 * used the inferred types to create edges in the CFG.
 * <p>
 * Throw-statement attribute extends exception attribute: the exception type
 * stored in exception attribute represents the common super-type of all
 * exceptions that can be raised. For the various throw statement expression
 * types:
 * <l>
 *  <li> New-instance expression - Type of created object.
 *  <li> Method-call expression - Return type of called method.
 *  <li> Variable - Declared type of variable.
 * </l>
 * <p>
 * Exception types stored in throw-statement attribute may include or exclude
 * the type stored in exception attribute, and may include additional types.
 *
 * @author S. Sinha
 * @author Jay Lofstead 2003/05/29 coverted toString to generate XML
 * @author Jay Lofstead 2003/06/02 converted elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface ThrowStatementAttribute extends ExceptionAttribute
{
    /** Throw-statement expression is a new-instance expression. */
    public final int NEW_INSTANCE_THROW_EXPR = 1;

    /** Throw-statement expression is a method-call expression. */
    public final int METHOD_CALL_THROW_EXPR = 2;

    /** Throw-statement expression is a variable */
    public final int VAR_THROW_EXPR = 3;

    /**
     * Sets the expression type for a throw-statement attribute.
     * @param type type of throw-statement expression.
     * @throws IllegalArgumentException if <code>type</code> is not one of
     *         {@link #NEW_INSTANCE_THROW_EXPR NEW_INSTANCE_THROW_EXPR},
     *         {@link #METHOD_CALL_THROW_EXPR METHOD_CALL_THROW_EXPR}, or
     *         {@link #VAR_THROW_EXPR VAR_THROW_EXPR}.
     */
    public void setExpressionType( int type ) throws IllegalArgumentException;

    /**
     * Returns the expression type for this throw-statement attribute.
     * @return Expression type for this throw-statement attribute.
     */
    public int getExpressionType();

    /**
     * Returns true if the expression for this throw statement contains
     * a typecase, or false otherwise.
     * @return Boolean indicating whether the expression of the throw
     * statement contains a typecast.
     */
    public boolean containsTypecast();

    /**
     * Sets the boolean value indicating if the expression for this throw
     * statement contains a typecast.
     * @param value Boolean value indicating if the expression for this throw
     * statement contains a typecast.
     */
    public void setContainsTypecast( boolean value );

    /**
     * Adds an exception type to the set of exception types for this
     * throw-statement attribute.
     * @param type exception type to add.
     * @throws IllegalArgumentException if <code>type</code> is
     *                                     <code>null</code>.
     */
    public void addExceptionType( Class type ) throws IllegalArgumentException;

    /**
     * Returns the exception types for this throw-statement attribute.
     * @return Array of exception types for this throw-statement attribute.
     */
    public Class[] getExceptionTypes();

    /**
     * Returns a string representation of a throw-statement attribute.
     * @return String representation of throw-statement attribute.
     */
    public String toString();
}
