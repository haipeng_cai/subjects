/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Program;
import jaba.sym.ProgramAttribute;

/**
 * Represents a graph created for a program.
 * @author Jim Jones -- <i>Mar. 1, 1999.  Created.</i>
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface ProgramGraph extends Graph, ProgramAttribute
{
	/** ??? */
	public Program getProgram ();
}
