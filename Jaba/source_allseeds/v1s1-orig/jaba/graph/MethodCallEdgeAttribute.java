/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Class;

import java.util.Vector;

/**
 * This attribute is placed on all edges from a call node to the entry of a
 * method.
 * @author Jim Jones -- <i>Created, May 28, 1999</i>.
 * @author Jay Lofstead -- 2003/05/29 changed toString to generate XML.
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface MethodCallEdgeAttribute extends EdgeAttribute
{
  /**
   * Adds an object type that causes this edge to be traversed.
   * @param classType  Object type that causes this edge to be traversed.
   */
  public void addTraversalType( Class classType );

  /**
   * Returns all object types that cause this edge to be traversed.
   * @return All object types that cause this edge to be traversed.
   */
  public Class[] getTraversalTypes();

  /**
   * Returns a string representation of this attribute.
   * @return A string representation of this attribute.
   */
  public String toString();
}
