package jaba.graph;

import jaba.sym.DemandDrivenAttribute;

import jaba.main.DottyOutputSpec;

import java.io.IOException;

/**
 * Represents a class hierarchy graph.
 * A class heirarchy graph shows the degrees of inheritance among
 * the different classes used in a program
 * @author Caleb Ho -- <i>Created 5/2001</i>
 * @author Huaxing Wu -- <i> Revised 9/24/02. Fix bugs that introduces null 
 *                          nodes into the graph. The problem is it tries to
 *                          create edges between subject classes and library
 *                          interfaces, while no node is created for a lib 
 *                          interface. Fixed by creating node for library 
 *                          classes and interfaces.</i>
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 added toString method
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/16 added toStringReduced (), toStringDetailed ()
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface ClassHierarchyGraph extends ProgramGraph, DemandDrivenAttribute
{
	/**
	 * This method automatically will create a dotty file for a ClassHierarchyGraph. The file
	 * will be outputted into the file specified by the input parameter, filename.This method
	 * is also called by displayGraph
	 * @param filename - this is the filename that the dotty file witll be written to
	 * @param DottyOutputSpec - the specs that this graph needs, so that it knows what
	 * customizations the user wants when displaying the graph
	 */
	public void createDottyFile(String filename, DottyOutputSpec spec) throws IOException;

	/**
	 * Calling this method will autmatically create a ClassHierarchyGraph. The dotty file
	 * is defaulted to being written into "ClassHierarchyGraph.dotty". Once the dotty file has
	 * been created, the graph is automatically displayed
	 * @param spec - specifies what the user wants displayed on the graph 
	 */
	public void displayGraph( DottyOutputSpec spec ) throws java.io.IOException;

	/** returns an XML representation of this object */
	public String toString ();

	/** returns the reduced representation of this object */
	public String toStringReduced ();

	/** returns the detailed representation of this object */
	public String toStringDetailed ();
}
