/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Method;
import jaba.sym.MethodAttribute;

/**
 * Represents a graph created for a method.
 *
 * @author S. Sinha
 * @author Jay Lofstead 2003/07/01 changed to implement MethodGraph
 */
public abstract class MethodGraphImpl extends GraphImpl implements MethodAttribute, MethodGraph
{
    /** Method for which the graph is built. */
    private Method method;

    /**
     * Constructor.
     * @param method  Method for which this graph is built.
     */
    public MethodGraphImpl ( Method method )
    {
	this.method = method;
    }

    /**
     * Returns the method for which the graph is built.
     * @return Method for which graph is built.
     */
    public Method getMethod()
    {
        return method;
    }
}
