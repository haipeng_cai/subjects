/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.acfg;

import jaba.graph.cfg.CFG;
import jaba.graph.cfg.CFGImpl;

import jaba.graph.MethodGraph;
import jaba.graph.MethodGraphImpl;
import jaba.graph.Node;
import jaba.graph.StatementNode;
import jaba.graph.StatementNodeImpl;
import jaba.graph.Edge;
import jaba.graph.EdgeImpl;
import jaba.graph.ReturnSitesAttribute;

import jaba.sym.Method;
import jaba.sym.MethodAttribute;
import jaba.sym.DemandDrivenAttribute;

import jaba.tools.local.Factory;

import java.util.Iterator;
import java.util.Collection;

/**
 * Represents an augmented control-flow graph.
 *
 * @author S. Sinha
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 added toString method
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/16 added toStringReduced (), toStringDetailed ()
 * @author Jay Lofstead 2003/07/17 removed legacy load ()
 * @author Jay Lofstead 2003/07/17 changed to use Factory to get attributes.
 */
public class ACFGImpl extends MethodGraphImpl implements DemandDrivenAttribute,
						 MethodAttribute, ACFG
{
    /**
     * Creates a new ACFG object.
     * @param method method for which ICFG object is created
     */
    private ACFGImpl ( Method method ) {
        super( method );
    }

    /**
     * Clones the parameter edges, and adds them to the ACFG.
     * @param edges array of edges to be added.
     */
    private void addEdges (Edge [] edges)
    {
        if ( edges == null ) return;
        for ( int i=0; i<edges.length; i++ ) {
            addEdge ((Edge) ((jaba.graph.Edge) edges [i]).clone ());
        }
    }

    /**
     * Constructs the ACFG for the corresponding method.
     * 
     * TBD
     *
     * @param method The method for which the ICFG has to be loaded.
     * @return The ICFG for that method.
     */
    static public MethodAttribute load(Method method) {

	if ( method == null ) {
	    RuntimeException e = new RuntimeException();
	    e.printStackTrace();
	    throw e;
	}
	
	MethodAttribute[] attributes = method.getAttributes();
	int numAttr = attributes.length;
	for ( int i=0; i<numAttr; i++ ) {
	    if ( attributes[ i ] instanceof jaba.graph.acfg.ACFG ) {
		return (MethodAttribute)attributes[ i ];
	    }
	}
	
	// attribute not found... create it and initialize it.
	ACFG acfg = new ACFGImpl ( method );

        // perform PNRC analysis and construct the ACFG for the method
        PNRCInitializer pnrcInit = new PNRCInitializer ();
        pnrcInit.load ((jaba.sym.Program) acfg.getMethod ().getContainingType ().getProgram ());
	constructACFG (acfg);

	// construct the ACFG for each finally block contained within method
	Collection finallyMethods = method.getTransitiveFinallyMethodsCollection ();
	for (Iterator i = finallyMethods.iterator (); i.hasNext ();)
	{
		Method m = (Method) i.next ();
	    ACFG finallyACFG = new ACFGImpl (m);
	    constructACFG (finallyACFG);
	    // add finally ACFG as attribute of the finally method
	    m.addAttribute ((MethodAttribute) finallyACFG);
	}
	
	// add the ACFG as an attribute of the method
	method.addAttribute( (MethodAttribute)acfg );
	return (MethodAttribute)acfg;
    }

    /** ??? */
    private static void constructACFG( ACFG aParam )
    {
	ACFGImpl acfg = (ACFGImpl) aParam;
        // obtain the corresponding CFG from the associated method.
        CFG cfg = (CFG)CFGImpl.load( acfg.getMethod() );

        Node[] cfgNodes = cfg.getNodes();
        for ( int i=0; i<cfgNodes.length; i++ ) {
            Edge[] outEdges = cfg.getOutEdges( cfgNodes[i] );
            StatementNode sNode = (StatementNode)cfgNodes[i];
            int nodeType = sNode.getType();
            if ( nodeType != StatementNode.STATIC_METHOD_CALL_NODE &&
                 nodeType != StatementNode.VIRTUAL_METHOD_CALL_NODE &&
                 nodeType != StatementNode.NORMAL_FINALLY_CALL_NODE &&
                 nodeType != StatementNode.EXCEPTIONAL_FINALLY_CALL_NODE &&
		 nodeType != StatementNode.HALT_NODE ) {
                acfg.addEdges( outEdges );
                continue;
            }

	    // add the halt node to the acfg, not its outedge; the halt node
	    // is an exit point in the ACFG
	    if ( nodeType == StatementNode.HALT_NODE ) {
		((jaba.graph.Graph) acfg).addNode ((jaba.graph.Node) cfgNodes [i]);
		continue;
	    }

            // access return-sites attribute of method-call or finally-call node
	    ReturnSitesAttribute rattr = Factory.getReturnSitesAttribute (sNode);
            if ( rattr == null ) {
		RuntimeException e = new RuntimeException();
		e.printStackTrace();
		throw e;
	    }
            StatementNode[] retSites = rattr.getReturnSites();
            if ( retSites.length == 1 ) {  // DRC
                if ( retSites[0].getType() != StatementNode.RETURN_NODE ) {
		    RuntimeException e = new RuntimeException();
		    e.printStackTrace();
		    throw e;
		}
                acfg.addEdges (outEdges);
                continue;
            }
            // call site is PNRC: create a return-predicate node
            StatementNode retPredNode = new StatementNodeImpl ();
            retPredNode.setByteCodeOffset( sNode.getByteCodeOffset() );
            retPredNode.setSourceLineNumber( sNode.getSourceLineNumber() );
            retPredNode.setType( StatementNode.RETURN_PREDICATE_NODE );
            retPredNode.setContainingMethod( acfg.getMethod() );

            // add node to the ACFG, connect PNRC to the node
            acfg.addNode( retPredNode );
            acfg.addEdge( new EdgeImpl ( sNode, retPredNode ) );

            // create edges from return-predicate node to nodes that appear
            // in return sites; label each edge with the ID of the sink node
            for ( int j=0; j<retSites.length; j++ ) {
                acfg.addEdge( new EdgeImpl ( retPredNode, retSites[j],
					String.valueOf( retSites[j].
							getNodeNumber() ) ) );
            }
        }
    }

	/** return an XML format of this object */
	public String toString ()
	{
		return "<acfg>" + getString () + "</acfg>";
	}

	/** returns the reduced representation of this object */
	public String toStringReduced ()
	{
		return "<acfg>" + getStringReduced () + "</acfg>";
	}

	/** returns the detailed representation of this object */
	public String toStringDetailed ()
	{
		return "<acfg>" + getStringDetailed () + "</acfg>";
	}
}
