/* Copyright (c) 1999, The Ohio State University */

package jaba.graph.acfg;

import jaba.graph.MethodGraph;

/**
 * Represents an augmented control-flow graph.
 *
 * @author S. Sinha
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 added toString method
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/16 added toStringReduced (), toStringDetailed ()
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface ACFG extends MethodGraph
{
	/** return an XML format of this object */
	public String toString ();

	/** returns the reduced representation of this object */
	public String toStringReduced ();

	/** returns the detailed representation of this object */
	public String toStringDetailed ();
}
