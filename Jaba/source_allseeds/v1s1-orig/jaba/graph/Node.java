/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

/**
 * Represents a generic graph node.
 *
 * @author S. Sinha
 * @author Huaxing Wu -- <i>Revised 8/12/02. Move addAttribute() and 
 *                       getAttributesOf() from StatementNode to here </i>
 * @author Huaxing Wu -- <i> Add method getColor, getShape, getNodeTypeName </i>
 * @author Jay Lofstead 2003/05/29 added nodeNumber and related from StatementNode since it is needed in CDG.
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/04 fixed getnodeNumber to be standard and unique in this class for all node types.
 * @author Jay Lofstead 2003/06/04 added getString () for generating the XML for the data stored at this level.
 * @author Jay Lofstead 2003/06/06 added getStringReduced for returning a minimal XML representation of this object
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/30 changed to an interface
 */
public interface Node
{
    /** all derived types will provide a toString method using <code>getString</code> to add the standard pieces to the XML format.
     *  This only returns a fragment of XML that is not a complete node in the XML tree.  It should be embedded in another XML node.
     */
    public String toString();

    /**
     * Returns the color of this object.
     * @return the color of the type
     */
    public String getColor();
    
    /**
     * Returns the shape of this object.
     * @return the shape of the type
     */
    public String getShape();
    
    /**
     * Returns the type name of this object.
     * @return the name of its type
     */
    public String getNodeTypeName();

    /**
     * Returns the unique identifier for this node.
     */
    public int getNodeNumber ();

    /**
     * Adds an attribute to the set of attributes associated with a
     * node.
     * @param attribute Attribute to add.
     * @throws IllegalArgumentException if the node has an
     *                                  existing attribute of the type of
     *                                  <code>attribute</code>, or
     *                                  <code>attribute</code> is
     *                                  <code>null</code>.
     */
    public void addAttribute( NodeAttribute attribute )
                throws IllegalArgumentException;

    /**
     * Returns the attribute of the specified type that may be associated
     * with a node.
     * @param typeStr type of node attribute. Must be a
     *             subtype of <code>NodeAttribute</code>.
     * @return node attribute of the specified type, or
     *         <code>null</code> if node has no attribute of the specified
     *         type.
     * @throws IllegalArgumentException if <code>type</code> is not a subtype
     *                                  of <code>NodeAttribute</code>
     */
    public NodeAttribute getAttributeOfType( String typeStr )
                                  throws IllegalArgumentException;
}
