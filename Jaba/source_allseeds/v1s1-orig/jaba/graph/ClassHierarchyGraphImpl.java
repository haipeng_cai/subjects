package jaba.graph;

import jaba.sym.Program;
import jaba.sym.ProgramAttribute;
import jaba.sym.Class;
import jaba.sym.Interface;
import jaba.sym.DemandDrivenAttribute;

import jaba.main.DottyOutputSpec;

import jaba.tools.local.Factory;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Iterator;
import java.util.Collection;

import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;

/**
 * Represents a class hierarchy graph.
 * A class heirarchy graph shows the degrees of inheritance among
 * the different classes used in a program
 * @author Caleb Ho -- <i>Created 5/2001</i>
 * @author Huaxing Wu -- <i> Revised 9/24/02. Fix bugs that introduces null 
 *                          nodes into the graph. The problem is it tries to
 *                          create edges between subject classes and library
 *                          interfaces, while no node is created for a lib 
 *                          interface. Fixed by creating node for library 
 *                          classes and interfaces.</i>
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 added toString method
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/16 added toStringReduced (), toStringDetailed ()
 * @author Jay Lofstead 2003/07/01 changed to implement ClassHierarchyGraph
 * @author Jay Lofstead 2003/07/17 replaced legacy load () with new static load method.
 * @author Jay Lofstead 2003/07/17 changed to use Factory to get attributes.
 */
public class ClassHierarchyGraphImpl extends ProgramGraphImpl implements DemandDrivenAttribute, ClassHierarchyGraph
{
    /**
     * Creates a new ClassHierarchyGraph object for the specified Program
     * @param program that needs to have a ClassHierarchyGraph created for
     */
    private ClassHierarchyGraphImpl (Program p)
    {
        super (p);
    }

    /**
     * Creates the edges and nodes in the graph by calling createClassNodes(), createInterfaceNodes()
     * and createEdges()
     */
    public static ProgramAttribute load (Program p)
    {
	ClassHierarchyGraphImpl chg = new ClassHierarchyGraphImpl (p);

	Factory.getICFG (p);
	chg.classes = p.getLoadedClassesCollection ();
	chg.interfaces = p.getLoadedInterfacesCollection ();

        chg.classNodes = new HashMap ();
        chg.interfaceNodes = new HashMap ();

	chg.createClassNodes ();
	chg.createInterfaceNodes ();
        chg.createEdges ();

	return chg;
    }

    /**
     * Loops through all loaded classes in the program,  creating a ClassNode
     * object for each one. For unsummarized class node, that is, 
     * application's class (Normally specified in a ResourceFile), 
     * they will be put into the graph. all ClassNodes are stored in the 
     * hashmap
     * classNodes, mapping each Class object, to its corresponding ClassNode.
     * The Class object is used as a key to the classNodes hashmap
     * These ClassNodes will later be used in createEdges()
     * Note: Summarized class's node is created for later
     * creating edges between subject's classes/interfaces and their 
     * super classes. It's possible that a node is created but never
     * used.
     */
    private void createClassNodes()
    {
  	ClassNode tempClassNode;

        for (Iterator i = classes.iterator (); i.hasNext ();)
	{
		Class c = (Class) i.next ();
	    //loop through all classes	
	    tempClassNode = new ClassNode (c);

	    //Add to the graph if it's part of analysis class
	    if (!c.isSummarized ())
	    {
		addNode (tempClassNode);
	    }

	    classNodes.put (c, tempClassNode);
        }// end loop through all classes
	
    }//end createClassNodes

    /**
     * Loops through all the interfaces loaded, creating an InterfaceNode
     * object for each interface. Unsummarized interface Node (belong to the 
     * subject) will be added to the graph (addNode). 
     * All These InterfaceNodes are stored in the 
     * hashmap interfaceNodes, mapping each Inteface object, to its 
     * corresponding IntefaceNode. 
     * The Interface object is used for a key to get to the IntefaceNode
     * These InterfaceNodes will later be used in createEdges().
     * Note: Summarized interface node(library interface) is created for later
     * creating edges between subject's classes/interfaces and their 
     * implemented interfaces. It's possible that a node is created but never
     * used.
     */
    private void createInterfaceNodes()
    {
	for (Iterator i = interfaces.iterator (); i.hasNext ();)
	{
		Interface intf = (Interface) i.next ();
	    InterfaceNode tempInterfaceNode = new InterfaceNode (intf);
	    //Add to the graph if it's part of analysis class
	    if (!intf.isSummarized()){
		addNode (tempInterfaceNode);
	    }
	    
	    interfaceNodes.put (intf, tempInterfaceNode);
	}
    }

    /**
     * Called after the createClassNodes() and createInterfaceNodes() methods have been called. 
     * Loops through each class, and interface determining the inheritance relationships and
     * interface implementation relationships, creating an edge for each such relationship.
     * If class x implements inteface y, an edge is created with x as the sink, and y as the source.
     * If class x also inherits from class z, and edge is created with x as the sink, and z as the source
     * An edge is represented by the Edge class, which manipulates the Node classes to serve as
     * source and sink. ClassNode and InterfaceNOde, children of Node, are used to
     * 1) represent Classes and Interfaces in this class hierarchy graph, 2) to allow Edge objects to be 
     * created
     */
    private void createEdges()
    {
	for (Iterator i = classes.iterator (); i.hasNext ();)
	{
	    Class currentClass = (Class) i.next ();
	    if (currentClass.isSummarized ())   //check to see if current class is summmarized
	    {
	 	continue;
	    }
	    ClassNode sourceNode = (ClassNode) classNodes.get(currentClass);
 	    Class parentClass = currentClass.getSuperClass(); 
	  
	    ClassNode classSinkNode = (ClassNode) classNodes.get(parentClass);	
	    
	    Edge tempEdge = new EdgeImpl (sourceNode, classSinkNode);
	    addEdge(tempEdge);

	    Collection allImplementedInterfaces = currentClass.getInterfacesCollection ();
	    for (Iterator j = allImplementedInterfaces.iterator (); j.hasNext ();)
	    {
		Interface implementedInterface = (Interface) j.next ();
		InterfaceNode interfaceSinkNode = (InterfaceNode) interfaceNodes.get(implementedInterface);
		tempEdge = new EdgeImpl (sourceNode, interfaceSinkNode);
		addEdge(tempEdge);
	    }
	
	}
    }

    /**
     * This method automatically will create a dotty file for a ClassHierarchyGraph. The file
     * will be outputted into the file specified by the input parameter, filename.This method
     * is also called by displayGraph
     * @param filename - this is the filename that the dotty file witll be written to
     * @param DottyOutputSpec - the specs that this graph needs, so that it knows what
     * customizations the user wants when displaying the graph
     */
    public void createDottyFile(String filename, DottyOutputSpec spec) throws IOException {
	Writer outputFile = new FileWriter(filename);
	String shape, color, nodeType;
	Hashtable graph = new Hashtable();
	TypeNode nodeToWrite;

        // The statements below write some configuration information into the
        // output file like the Font size, Font Name, Font color etc.

        outputFile.write("digraph \"g\" { \n graph [ \n");
        //"size" and "bb" elements have been taken out so that dotty
        // can properly process the file
        outputFile.write("fontsize = \"14\" \n fontname = \"Times-Roman\" \n");
        outputFile.write("fontcolor = \"black\" \n");
        outputFile.write("lp = \"349,0\" \n"); 
        outputFile.write("color = \"black\" \n ] \n");
        outputFile.write("node [ \n fontsize = \"14\" \n" +
                "fontname = \"Times-Roman\" \n");
        outputFile.write("fontcolor = \"black\" \n shape = \"ellipse\" \n");
        outputFile.write("color = \"black\" \n style = \"filled\" \n ] \n");

        // Get all the nodes in the CallGraph in depth first order

        Node[] nodes = getNodesInDepthFirstOrder();

        for(int i = nodes.length; i > 0; i--) {  //create all the nodes
            nodeToWrite = (TypeNode) nodes[i-1];
	    nodeType = nodeToWrite.getNodeTypeName();
	    color = nodeToWrite.getColor();
	    shape = nodeToWrite.getShape();
            graph.put(nodeToWrite.toString(), nodeToWrite);

        // Write out the node information to the output file.
        // :the shape and color of the node. The text to be included in the node is specified in the
        // "spec" parameter
        outputFile.write("\"" + nodeToWrite.toString() + "\" [\n");
	if (nodeToWrite.getTypeOf() == TypeNode.CLASS_NODE)
            outputFile.write("label = \"Class Name : " + nodeToWrite.toString() + "\\n ");
	else
	    outputFile.write("label = \"Interface Name : " + nodeToWrite.toString() + "\\n ");
        if(spec.getLabelSpec() == DottyOutputSpec.DEFAULT || spec.labelContains("NODE_NUMBER"))
            outputFile.write("Node Number: " + nodeToWrite.getTypeNodeNumber() + "\\n ");
        if(spec.getLabelSpec() == DottyOutputSpec.DEFAULT || spec.labelContains("NODE_TYPE"))
            outputFile.write("Node Type:" + nodeType);

        outputFile.write(" \"\nshape = \"" + shape + "\" \n");
        outputFile.write("color = \"" + color + "\" \n ]\n");
        }//end creating nodes

	//create all the edges
        Edge[] edges = getEdges();

        for(int i = edges.length; i > 0; i--){
            TypeNode source = (TypeNode) edges[i-1].getSource();
            TypeNode sink = (TypeNode) edges[i-1].getSink();
	    if (sink.getTypeOf() == TypeNode.CLASS_NODE){
            	//outputFile.write("\"" + source.toString() + "\"-> \"" + sink.toString() + "\" ");
		outputFile.write("\"" + sink.toString() + "\"-> \"" + source.toString() + "\" ");
                if(edges[i-1].getLabel() != null){
                    outputFile.write("[\n label = \"Edge Label:" +                                     
                        edges[i-1].getLabel() + "\" \n");
                    outputFile.write("]\n");
                }
                else
                    outputFile.write("\n");
	    }
	    else{
	//outputFile.write("\"" + source.toString() + "\"-> \"" + sink.toString() + "\" ");
	outputFile.write("\"" + sink.toString() + "\"-> \"" + source.toString() + "\" ");
		outputFile.write("[\n style = dotted \n ]\n");
                if(edges[i-1].getLabel() != null){
                    outputFile.write("[\n label = \"Edge Label:" +                                     
                        edges[i-1].getLabel() + "\" \n");
                    outputFile.write("]\n");
                }
                else
                    outputFile.write("\n");	
	    }

        }

         outputFile.write("} \n");
         outputFile.close();

    }//end createDottyFile

    /**
     * Calling this method will autmatically create a ClassHierarchyGraph. The dotty file
     * is defaulted to being written into "ClassHierarchyGraph.dotty". Once the dotty file has
     * been created, the graph is automatically displayed
     * @param spec - specifies what the user wants displayed on the graph 
     */
    public void displayGraph( DottyOutputSpec spec ) throws java.io.IOException{ 
	String dottyFile = "ClassHierarchyGraph.dotty"; 
        createDottyFile(dottyFile, spec);
        try {
            Runtime.getRuntime().exec( "dotty " + dottyFile ).waitFor();
	    } 
        catch ( IOException e1 ) { 
	    throw new RuntimeException( "Error: IOException occurred while " +
					"calling dotty:\n" + e1 ); 
	}
        catch ( InterruptedException e2 ) { 
            throw new RuntimeException( "Error: InterruptedException " +
					"occurred while calling dotty:\n" +
					e2 ); 
        }
    }        

	/** returns an XML representation of this object */
	public String toString ()
	{
		return "<classhierarchygraph>" + getString () + "</classhierarchygraph>";
	}

	/** returns the reduced representation of this object */
	public String toStringReduced ()
	{
		return "<classhierarchygraph>" + getStringReduced () + "</classhierarchygraph>";
	}

	/** returns the detailed representation of this object */
	public String toStringDetailed ()
	{
		return "<classhierarchygraph>" + getStringDetailed () + "</classhierarchygraph>";
	}

     /** 
      * A class hierarchy graph is created for a specified program, which can use multiple classes.
      * these classes are stored in the classes array                                
      */
    private Collection classes;

    /** A program can also implemente interfaces, which are stored then stored in this variable */
    private Collection interfaces;

     /** The nodes in a class hierarchy graph can represent classes (simulated with ClassNode)
      * or interfaces (simulated with InterfaceNode)
      * the edges in the class hierarchy graph show the inheritance relationships between different
      * different classes. 
      * The classNodes hashmap, is used to map a Class object to the ClassNode representing that 
      * particular Class. Read about how the createEdges method works to see why Nodes are needed
      */    
    private HashMap classNodes;

     /** Like the classNodes hashmap, but maps Interace objects to the InterfaceNodes */
    private HashMap interfaceNodes;
}
