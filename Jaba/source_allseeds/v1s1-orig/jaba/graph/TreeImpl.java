package jaba.graph;

import java.util.Map;
import java.util.Vector;
import java.util.Iterator;

/**
 * Encapsulates a tree--really a forest.
 * <br><br>
 * Can have multiple roots, but one can optionally enforce single-rootedness
 * in one's own code.  If one makes that choice, the method
 * <code>getSingleRoot()</code> is provided as a convenience.  That method
 * assumes the tree is in fact a single-rooted tree.
 * <br><br>
 * Might also point out that there is no checking for setting a node's
 * parent to be one of its existing descendants.  This would violate the
 * implicit rules of the structure and cause undefined behavior.
 * <br><br>
 * @author <a href="mailto:peterd@cc.gatech.edu">Peter Dillinger</a>
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/07/10 changed to implement Tree
 */
public abstract class TreeImpl extends GraphImpl implements Tree
{
	/**
	 * Creates a new, empty tree.
	 */
	public TreeImpl () {}

	/**
	 * Creates a tree based on the given mappings from children to parents.
	 *
	 * @param parentMap a <code>Map</code> mapping children nodes to parent
	 *                  nodes
	 */
	public TreeImpl (Map parentMap)
	{
		Iterator entries = parentMap.entrySet().iterator();
		while (entries.hasNext()) {
			Map.Entry entry = (Map.Entry) entries.next();
			add((Node) entry.getKey(), (Node) entry.getValue());
		}
	}

	/** METHOD FROM GRAPH THAT SHOULD NOT BE USED */
	public void addEdge(Edge edge)
	{
		throw new RuntimeException("Operation not supported.");
	}

	/** METHOD FROM GRAPH THAT SHOULD NOT BE USED */
	public void addNode(Node node)
	{
		throw new RuntimeException("Operation not supported.");
	}

	/** METHOD FROM GRAPH THAT SHOULD NOT BE USED */
	public boolean removeEdge(Edge edge)
	{
		throw new RuntimeException("Operation not supported.");
	}

	//========================== MODIFIERS =========================//

	/**
	 * Adds node <code>node</code> to the tree with parent <code>parent</code>.
	 * <br><br>
	 * Works like setParent().
	 * <br><br>
	 * @param node the new node
	 * @param parent the new parent of <code>node</code>
	 */
	public void add(Node newNode, Node parent)
	{
		setParent(newNode, parent);
	}

	/**
	 * Sets the parent of <code>node</code> to <code>parent</code>.
	 * <br><br>
	 * Adds the nodes to the tree if necessary.
	 * <br><br>
	 * @param node a node
	 * @param parent the node to be parent of <code>node</code>
	 */
	public void setParent(Node node, Node parent)
	{
		// Add the each node if it doesn't exist.
		if (node != null) super.addNode(node);
		if (parent != null) super.addNode(parent);
		
		// Remove any existing edges coming into this node.
		Edge [] inEdges = getInEdges (node);
		for (int i = 0; i < inEdges.length; i++) {
			removeEdge ((jaba.graph.Edge) inEdges[i]);
		}

		// If not specified as root node, add edge from parent to node.
		if (parent != null) {
			super.addEdge(createNewEdge(parent,node));
		}
	}

	//========================== ACCESSORS =========================//

	/**
	 * Gets the parent of a given node.
	 * <br><br>
	 * @param node node to get the parent of
	 * @return the parent <code>Node</code> or null if the node is a root node
	 */
	public Node getParentOf(Node node)
	{
		Edge [] inEdges = getInEdges (node);
		if (inEdges.length > 1) {
		    RuntimeException e = new RuntimeException();
		    e.printStackTrace();
		    throw e;
		}

		if (inEdges.length == 0) {
			return null;  //indicating a root node
		} else {
			return (jaba.graph.Node) inEdges [0].getSource();
		}
	}

	/**
	 * Gets the children of a given node.
	 * <br><br>
	 * If children of <code>null</code> are requested, behaves just like
	 * getRootNodes().
	 * <br><br>
	 * @param node node to get the children of
	 * @return a <code>Node[]</code> of the children
	 */
	public Node[] getChildrenOf (Node node)
	{
		if (node == null) {
			return getRootNodes();
		} else {
			Edge[] outEdges = getOutEdges(node);
			Node[] children = new Node[outEdges.length];
			for (int i = 0; i < outEdges.length; i++) {
				children[i] = (jaba.graph.Node) outEdges[i].getSink();
			}
			return children;
		}
	}

	/**
	 * Gets the root nodes of the forest.
	 * <br><br>
	 * @return a <code>Node[]</code> of the root nodes
	 */
	public Node[] getRootNodes()
	{
		return getEntryNodes();
	}

	/**
	 * Gets the single root nodes this tree.
	 * <br><br>
	 * Makes the assertion that this object contains a single-rooted tree.
	 * This must be guaranteed by its use.
	 * <br><br>
	 * @return a <code>Node[]</code> of the root nodes
	 */
	public Node getSingleRoot()
	{
		Node[] roots = getRootNodes();
		try {
			if (roots.length != 1) throw new RuntimeException();
		} catch (RuntimeException e) {
			System.err.println("Failed runtime assertion that Tree is single-rooted, i.e. not a forest.");
			e.printStackTrace();
		}
		return (jaba.graph.Node) roots[0];
	}

	/**
	 * Returns an array of all ancestors of a particular node, including the
	 * node itself.
	 * <br><br>
	 * @param n the node
	 * @return <code>Node[]</code> of ancestors
	 */
	public Vector getAncestorsOfVector (Node n)
	{
		Vector v = new Vector();
		addAncestors (n, v);

		return v;
	}

	/**
         * Convenience method for converting the Vector to an Array (less efficient) from <code>getAncestorsOfVector</code>
	 * @param n the node
	 * @return <code>Node[]</code> of ancestors
	 */
	public Node [] getAncestorsOf (Node n)
	{
		Vector v = getAncestorsOfVector (n);

		return (Node []) v.toArray (new Node [v.size ()]);
	}

	/** ??? */
	private void addAncestors(Node n, Vector v)
	{
		if (n != null) {
			v.add(n);
			addAncestors(getParentOf(n), v);
		}
	}

	/**
	 * Returns an array of all the strict ancestors of a particular node.
	 * <br><br>
	 * @param n the node
	 * @return <code>Node[]</code> of strict ancestors
	 */
	public Vector getStrictAncestorsOfVector (Node n)
	{
		return getAncestorsOfVector (getParentOf (n));
	}

	/**
         * Convenience method for converting the Vector to an Array (less efficient) from <code>getStrictAncestorsOfVector</code>
	 * @param n the node
	 * @return <code>Node[]</code> of strict ancestors
	 */
	public Node [] getStrictAncestorsOf (Node n)
	{
		Vector v = getStrictAncestorsOfVector (n);

		return (Node []) v.toArray (new Node [v.size ()]);
	}

	/**
	 * Returns whether the target node is an ancestor of the source node, or
	 * the same node.
	 * <br><br>
	 * @param source source node
	 * @param target target node
	 * @return whether the target node is an ancestor of the source node, or
	 *         the same node
	 */
	public boolean isAncestorOf(Node source, Node target)
	{
		return (target == source) || isStrictAncestorOf(source, target);
	}

	/**
	 * Returns whether the target node is a strict ancestor of the source
	 * node.
	 * <br><br>
	 * @param source source node
	 * @param target target node
	 * @return whether the target node is a strict ancestor of the source
	 *         node.
	 */
	public boolean isStrictAncestorOf(Node source, Node target)
	{
		Node parent = getParentOf(source);
		if ( parent == null ) {
		    return false;
		} else {
		    return ( target == parent ) ||  isStrictAncestorOf(parent, target);
		}
	}

	/**
	 * Returns whether the target node is a descendant of the source node, or
	 * the same node.
	 * <br><br>
	 * @param source source node
	 * @param target target node
	 * @return whether the target node is a descendant of the source node, or
	 *         the same node
	 */
	public boolean isDescendantOf(Node source, Node target)
	{
		return isAncestorOf(target, source);
	}

	/**
	 * Returns whether the target node is a strict descendant of the source
	 * node.
	 * <br><br>
	 * @param source source node
	 * @param target target node
	 * @return whether the target node is a strict descendant of the source
	 *         node.
	 */
	public boolean isStrictDescendantOf(Node source, Node target)
	{
		return isStrictAncestorOf(target, source);
	}

	//======================== INTERNAL USE ========================//

	/**
	 * Creates a new edge for the tree.
	 * <br><br>
	 * If subclasses want a specific way to add edges, they should override
	 * this method.
	 * <br><br>
	 * This method should only be used internally to this class and its
	 * subclasses.
	 * <br><br>
	 * @param source source node for new edge
	 * @param sink sink node for new edge
	 * @return a new <code>Edge</code>
	 */
	protected Edge createNewEdge(Node source, Node sink)
	{
		return new EdgeImpl (source, sink);
	}
}
