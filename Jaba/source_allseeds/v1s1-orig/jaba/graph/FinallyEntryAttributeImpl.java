/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

/**
 * Attribute that needs to be stored with a finally-entry node.
 * @author Jay Lofstead 2003/05/29 converted toString to output XML
 * @author Jay Lofstead 2003/07/10 changed to implement FinallyEntryAttribute
 */
public class FinallyEntryAttributeImpl implements FinallyEntryAttribute, StatementNodeAttribute
{
  /** All actual parameters used at this call site. */
  private String name;

  /**
   * Constructor
   */
  public FinallyEntryAttributeImpl ()
    {
      // initialize all fields
      name = null;
    }


  /**
   * Sets name of finally block that is called at a finally-call node.
   * @param name  The name of finally block.
   */
  public void setName( String name ) 
    {
      this.name = name;
    }


  /**
   * Returns the of finally block that is called at a finally-call node.
   * @return The name of finally block.
   */
  public String getName()
    {
      return name;
    }

    /**
     * Returns a string representation of a finally-entry attribute.
     * @return String representation of finally-entry attribute.
     */
    public String toString()
	{
		return "<finallyentryattribute><name>" + name + "</name></finallyentryattribute>";
    }

}
