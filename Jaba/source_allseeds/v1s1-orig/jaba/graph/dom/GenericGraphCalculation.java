package jaba.graph.dom;

import jaba.graph.Graph;
import jaba.graph.Node;
import jaba.graph.Edge;
import jaba.graph.Tree;

/**
 * Encapsulates some calculation on a graph.
 * <br><br>
 * @author <a href="mailto:peterd@cc.gatech.edu">Peter Dillinger</a>
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 */
abstract class GenericGraphCalculation implements Runnable, java.io.Serializable
{
	/** ??? */
	private boolean reverse;

	/** ??? */
	protected Graph cfg;
	
	/** ??? */
	GenericGraphCalculation (Graph cfg, boolean reverse)
	{
		this.cfg = cfg;
		this.reverse = reverse;
	}

	/** ??? */
	GenericGraphCalculation (Graph cfg)
	{
		this(cfg, false);
	}

	/**
	 * Retrieves the predecessors of a given node.
	 * <br><br>
	 * @param node given node
	 * @return its predecessors
	 */
	protected Node [] getPredecessors (Node node)
	{
		return getRelative (node, false);
	}

	/**
	 * Retrieves the successors of a given node.
	 * <br><br>
	 * Works much like getPredecessors().
	 * <br><br>
	 * @param node the node
	 * @return its successors
	 */
	protected Node [] getSuccessors(Node node)
	{
		return getRelative(node, true);
	}

	/** ??? */
	private Node [] getRelative (Node node, boolean direction)
	{
		Node [] results;
		if (direction ^ reverse) { // Successor if not reversed
			Edge [] outEdges = cfg.getOutEdges (node);
			results = new Node[outEdges.length];
			for (int i = 0; i < outEdges.length; i ++) {
				results[i] = outEdges[i].getSink();
			}
		} else { // Predecessor if not reversed
			Edge[] inEdges = cfg.getInEdges(node);
			results = new Node[inEdges.length];
			for (int i = 0; i < inEdges.length; i++) {
				results[i] = inEdges[i].getSource();
			}
		}
		return results;
	}

	/** ??? */
	protected Node [] getEntryNodes ()
	{
		return getEntryOrExitNodes(false);
	}

	/** ??? */
	protected Node [] getExitNodes ()
	{
		return getEntryOrExitNodes(true);
	}

	/** ??? */
	private Node [] getEntryOrExitNodes (boolean direction)
	{
		if (direction ^ reverse) {
			return cfg.getExitNodes();
		} else {
			return cfg.getEntryNodes();
		}
	}

	/** ??? */
	protected Node [] getNodes ()
	{
		return cfg.getNodes ();
	}

	/** ??? */
	protected Tree getDominatorTree ()
	{
		if (reverse) {
			return (Tree) PostDominatorTreeImpl.load (cfg);
		} else {
			return (Tree) DominatorTreeImpl.load (cfg);
		}
	}
}
