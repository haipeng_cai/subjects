package jaba.graph.dom;

import jaba.sym.DemandDrivenAttribute;

import jaba.graph.GraphAttribute;
import jaba.graph.Node;
import jaba.graph.Tree;

import java.util.Vector;

/**
 * DominatorTree - Encapsulates dominator information about a cfg
 * using a dominator tree.
 * <br><br>
 * @author <a href="mailto:peterd@cc.gatech.edu">Peter Dillinger</a>
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 added toString
 * @author Jay Lofstead 2003/06/06 changed toString to use getStringReduced
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/16 added toStringReduced (), toStringDetailed ()
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface DominatorTree extends Tree, GraphAttribute, DemandDrivenAttribute
{
	//================================================================//
	//                       A C C E S S O R S                        //
	//================================================================//

	/**
	 * Gets the immediate dominator of the specified node.
	 * <br><br>
	 * @param node node whose immediate dominator to find
	 * @return the immediate dominator of the given node.
	 */
	public Node idom(Node node);

	/**
	 * Gets the immediate dominator of the specified node.
	 * <br><br>
	 * @param node node whose immediate dominator to find
	 * @return the immediate dominator of the given node.
	 */
	public Node getImmediateDominatorOf(Node node);

	/**
	 * Gets all the dominators of the specified node.
	 * <br><br>
	 * @param node node whose dominators to find
	 * @return all dominators of the given node.
	 */
	public Vector domVector (Node node);

	/**
         * Convenience method for converting the Vector to an Array (less efficient) from <code>domVector</code>
	 * @param node node whose dominators to find
	 * @return all dominators of the given node.
	 */
	public Node[] dom (Node node);

	/**
	 * Gets all the dominators of the specified node.
	 * <br><br>
	 * @param node node whose dominators to find
	 * @return all dominators of the given node.
	 */
	public Vector getDominatorsOfVector (Node node);

	/**
         * Convenience method for converting the Vector to an Array (less efficient) from <code>getDominatorsOfVector</code>
	 * @param node node whose dominators to find
	 * @return all dominators of the given node.
	 */
	public Node[] getDominatorsOf(Node node);

	/**
	 * Gets the strict dominators of the specified node.
	 * <br><br>
	 * The strict dominators of a node are all the dominators of that node
	 * except the node itself.
	 * <br><br>
	 * @param node node whose strict dominators to find
	 * @return all dominators of the given node.
	 */
	public Vector getStrictDominatorsOfVector (Node node);

	/**
	 * Convenience method for converting the Vector to an Array (less efficient) from <code>getStrictDominatorsOfVector</code>
	 */
	public Node[] getStrictDominatorsOf(Node node);

	/** Returns true iff <code>a</code> dominates <code>b</code>. */
	public boolean dominates(Node a, Node b);
	
	/** Returns true iff <code>a</code> strictly dominates <code>b</code>. */
	public boolean strictlyDominates(Node a, Node b);	

	/** returns an XML representation of this object */
	public String toString ();

	/** returns the reduced representation of this object */
	public String toStringReduced ();

	/** returns the detailed representation of this object */
	public String toStringDetailed ();
}
