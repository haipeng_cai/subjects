package jaba.graph.dom;

import jaba.sym.DemandDrivenAttribute;

import jaba.graph.GraphAttribute;
import jaba.graph.Node;
import jaba.graph.Tree;

import java.util.Vector;

/**
 * PostDominatorTree - Encapsulates postdominator information about a
 * cfg using a dominator tree generated from the reverse cfg.
 *
 * @author <a href="mailto:peterd@cc.gatech.edu">Peter Dillinger</a>
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 added toString
 * @author Jay Lofstead 2003/06/06 changed toString to use getStringReduced
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/16 added toStringReduced (), toStringDetailed ()
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface PostDominatorTree extends Tree, GraphAttribute, DemandDrivenAttribute
{
	//================================================================//
	//                       A C C E S S O R S                        //
	//================================================================//

	/**
	 * Gets the immediate postdominator of the specified node.
	 * <br><br>
	 * @param node node whose immediate postdominator to find
	 * @return the immediate postdominator of the given node.
	 */
	public Node ipdom(Node node);

	/**
	 * Gets the immediate postdominator of the specified node.
	 * <br><br>
	 * @param node node whose immediate postdominator to find
	 * @return the immediate postdominator of the given node.
	 */
	public Node getImmediatePostDominatorOf(Node node);

	/**
	 * Gets all the postdominators of the specified node.
	 * <br><br>
	 * @param node node whose postdominators to find
	 * @return all postdominators of the given node.
	 */
	public Vector pdomVector (Node node);

	/**
	 * Convenience method for converting the Vector to an Array (less efficient) from <code>pdomVector</code>
	 * @param node node whose postdominators to find
	 * @return all postdominators of the given node.
	 */
	public Node[] pdom(Node node);

	/**
	 * Gets all the postdominators of the specified node.
	 * <br><br>
	 * @param node node whose postdominators to find
	 * @return all postdominators of the given node.
	 */
	public Vector getPostDominatorsOfVector (Node node);

	/**
	 * Convenience method for converting the Vector to an Array (less efficient) from <code>getPostDominatorsOfVector</code>
	 * @param node node whose postdominators to find
	 * @return all postdominators of the given node.
	 */
	public Node[] getPostDominatorsOf(Node node);

	/**
	 * Gets the strict postdominators of the specified node.
	 * <br><br>
	 * The strict postdominators of a node are all the postdominators of that
	 * node except the node itself.
	 *
	 * @param node node whose strict postdominators to find
	 * @return all postdominators of the given node.
	 */
	public Vector getStrictPostDominatorsOfVector (Node node);

	/**
	 * Convenience method for converting the Vector to an Array (less efficient) from <code>getStrictPostDominatorsOfVector</code>
	 * @param node node whose strict postdominators to find
	 * @return all postdominators of the given node.
	 */
	public Node[] getStrictPostDominatorsOf(Node node);

	/**
	 * Returns true iff <code>a</code> postdominates <code>b</code>.
	 */
	public boolean postDominates(Node a, Node b);
	
	/**
	 * Returns true iff <code>a</code> strictly postdominates <code>b</code>.
	 */
	public boolean strictlyPostDominates(Node a, Node b);

	/** returns an XML representation of this object */
	public String toString ();

	/** returns the reduced representation of this object */
	public String toStringReduced ();

	/** returns the detailed representation of this object */
	public String toStringDetailed ();
}
