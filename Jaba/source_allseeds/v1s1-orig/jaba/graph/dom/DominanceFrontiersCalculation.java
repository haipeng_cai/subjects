package jaba.graph.dom;

import jaba.graph.Node;
import jaba.graph.Edge;
import jaba.graph.Tree;
import jaba.graph.Graph;

import java.util.Vector;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/**
 * DominanceFrontiersCalculation - Encapsulates the calculation of dominance
 * frontiers.
 * <br><br>
 * We can think of the dominance frontier of a node as the set of nodes just
 * beyond the nodes dominated by our original node.
 * <br><br>
 * More precisely, the dominance frontier of a node is the set of all nodes
 * with a predecessor that is dominated by the original node but themselves
 * aren't strictly dominated by the original node.
 * <br><br>
 * Notice that this means a node could be in its own dominance frontier.
 * <br><br>
 * Brief algorithm description:
 * <br><br>
 * Our algorithm is a recursive algorithm.  The nodes that are possibly in
 * a node's dominance frontier are its immediate successors in the cfg and
 * all nodes in the dominance frontiers of its children in the dominator
 * tree.  Of these possibles, the ones that are in the dominance frontier of 
 * the original node are the ones whose immediate dominator is not the original
 * node.
 * <br><br>
 * @author <a href="mailto:peterd@cc.gatech.edu">Peter Dillinger</a>
 * @author Jay Lofstead 2003/06/02 converted elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/04 updated some of the JavaDoc
 * @author Jay Lofstead 2003/06/09 code cleanup
 */
class DominanceFrontiersCalculation extends GenericGraphCalculation
{
	/** ??? */
	DominanceFrontiersCalculation(Graph g)
	{
		super(g);
	}

	/** ??? */
	DominanceFrontiersCalculation(Graph g, boolean reverse)
	{
		super(g,reverse);
	}

	/** Run the calculation. */
	public void run()
	{
		init();
		bootstrap();
		generateResults();
	}

	/**
	 * Retrieve the computed frontiers.
	 * <br><br>
	 * Performs the calculation if it hasn't been done yet.
	 * <br><br>
	 * @return a map of the frontiers computed
	 */
	public Map getFrontiers()
	{
		if (results == null) {
			run();
		}
		return results;
	}

	/**
	 * Retrieve the computed frontiers for control-dependence calculation.
	 * <br><br>
	 * Performs the calculation if it hasn't been done yet.
	 * <br><br>
	 * @return a map of the frontiers computed
	 */
	Map getCDFrontiers()
	{
		if (results == null) {
			run();
		}
		return cdResults;
	}

	/** Initialize stuff. */
	private void init()
	{
	    domNodes = getNodes();
	    
	    domTree = getDominatorTree();

	    int frontierHashSize = domNodes.length > 5 ? domNodes.length * 2 : 11;
	    frontiers = new HashMap(domNodes.length * 2);
	    cdFrontiers = new HashMap(domNodes.length * 2);

	    // First, make mappings for empty frontiers.
	    for (int i = 0; i < domNodes.length; i ++) {
		frontiers.put(domNodes[i], new Vector());
		cdFrontiers.put(domNodes[i], new Vector());
	    }
	}
	
	/** Bootstrap into our recursive calculation. */
	private void bootstrap()
	{
	    Node[] startNodes = domTree.getRootNodes();
	    
	    for (int i = 0; i < startNodes.length; i++) {
		postOrderDescent(startNodes[i]);
	    }
	}

	/** Perform calculations in a post-order traversal. */
	private void postOrderDescent (Node nodeX)
	{
	    // Keep track of possible frontier nodes.
	    HashSet ySet = new HashSet();

	    // Add to set of possibles all frontier nodes from the children
	    // (in dominator tree).
	    Node[] zNodes = domTree.getChildrenOf ((jaba.graph.Node) nodeX);

	    for (int i = 0; i < zNodes.length; i++) {

		postOrderDescent(zNodes[i]);

		Vector frontierOfZ = (Vector)cdFrontiers.get(zNodes[i]);
		
		for (Enumeration e = frontierOfZ.elements (); e.hasMoreElements ();)
		{
		    ySet.add ((Edge) e.nextElement ());
		}
	    }

	    // Add to set of possibles all the local frontier nodes
	    // (applicable successors (in the cfg)).
	    Edge[] successors = getInEdges (nodeX);

	    for (int i = 0; i < successors.length; i++) {
		ySet.add( successors[i] );
	    }

	    Iterator yIter = ySet.iterator();
	    while (yIter.hasNext()) {
		Edge edgeY = (Edge) yIter.next();
		Node nodeY = (jaba.graph.Node) edgeY.getSource ();
		if (domTree.getParentOf(nodeY) != nodeX) {
		    ((Vector)frontiers.get(nodeX)).add(nodeY);
		    ((Vector)cdFrontiers.get(nodeX)).add(edgeY);
		    if (DEBUG) {
 			System.err.println("" +
 					   nodeX.getNodeNumber() + " -> " + 
 					   nodeY.getNodeNumber());
			System.err.println("" +
					   nodeX.getNodeNumber() + " -> " + 
					   edgeY.getSource().getNodeNumber()+
					   edgeY.getLabel());
		    }
		}
	    }
	}
	    
	/**
	 * Convert the generated vectors into useful arrays.
	 */
	private void generateResults()
	{
		results = new HashMap();
		cdResults = new HashMap();
		
		Node[] empty = new Node[0];
		
		for (int i = 0; i < domNodes.length; i++) {
			results.put(domNodes[i], ((Vector)frontiers.get(domNodes[i])).toArray(empty));
			cdResults.put( domNodes[i], ( (Vector)
                                       cdFrontiers.get( domNodes[i] ) ).
				       toArray( new Edge[0] ) );
		}

		frontiers = null;
		cdFrontiers = null;
		domNodes = null;
	}

    /**
     * Returns the inedges of the specified node in the graph for which
     * postdominance frontiers are being calculated.
     * @param node Node for which to return inedges.
     * @return Edge[] inedges for node.
     */
    private Edge [] getInEdges (Node node)
    {
	return cfg.getInEdges( node );
    }

	/** ??? */
	private static final boolean DEBUG = false;

	/** Computed by init() */
	private Node [] domNodes;

	/** Result to contruct. */
	private HashMap results;

	/** Temporary map with vectors. */
	private HashMap frontiers;
	
	/** Temporary map with vectors; this is used for computing control
	 * dependences. Unlike frontiers, which contains vectors of nodes,
	 * cdFrontiers contains vectors of edges; this enables the CDG to
	 * have labels on the edges
	 */
	private HashMap cdFrontiers;
	
	/** Result to contruct. */
	private HashMap cdResults;

	/** [Post]Dominator tree associated with the calculation. */
	private Tree domTree;
}
