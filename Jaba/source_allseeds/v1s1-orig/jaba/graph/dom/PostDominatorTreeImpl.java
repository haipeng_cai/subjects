package jaba.graph.dom;

import jaba.sym.DemandDrivenAttribute;

import jaba.graph.Node;
import jaba.graph.Tree;
import jaba.graph.TreeImpl;
import jaba.graph.GraphAttribute;
import jaba.graph.Edge;
import jaba.graph.Graph;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

/**
 * PostDominatorTree - Encapsulates postdominator information about a
 * cfg using a dominator tree generated from the reverse cfg.
 *
 * @author <a href="mailto:peterd@cc.gatech.edu">Peter Dillinger</a>
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 added toString
 * @author Jay Lofstead 2003/06/06 changed toString to use getStringReduced
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/16 added toStringReduced (), toStringDetailed ()
 * @author Jay Lofstead 2003/07/10 changed to implement PostDominatorTree
 * @author Jay Lofstead 2003/07/17 removed legacy load ()
 */
public class PostDominatorTreeImpl extends TreeImpl implements GraphAttribute, DemandDrivenAttribute, PostDominatorTree
{
	/** 
	 * Constructs a PostDominatorTree based on the given
	 * <code>Map</code> from nodes to their immediate dominators.
	 * <br><br>
	 * @param immediatePostDominatorMap <code>Map</code> from nodes to their
	 *                                  immediate postdominators
	 */
	PostDominatorTreeImpl (Map immediatePostDominatorMap)
	{
		super(immediatePostDominatorMap);
	}

	/**
	 * Creates an edge for the tree.
	 * <br><br>
	 * This method is used internally.
	 * <br><br>
	 * @param source source for the new edge
	 * @param sink sink for the new edge
	 * @return the new edge
	 */
	protected Edge createNewEdge(Node source, Node sink)
	{
		Edge newEdge = super.createNewEdge(source, sink);
		newEdge.setLabel("postdominates");
		return newEdge;
	}

	//================================================================//
	//                       A C C E S S O R S                        //
	//================================================================//

	/**
	 * Gets the immediate postdominator of the specified node.
	 * <br><br>
	 * @param node node whose immediate postdominator to find
	 * @return the immediate postdominator of the given node.
	 */
	public Node ipdom(Node node)
					{ return getImmediatePostDominatorOf(node); }

	/**
	 * Gets the immediate postdominator of the specified node.
	 * <br><br>
	 * @param node node whose immediate postdominator to find
	 * @return the immediate postdominator of the given node.
	 */
	public Node getImmediatePostDominatorOf(Node node)
					{ return getParentOf(node);}

	/**
	 * Gets all the postdominators of the specified node.
	 * <br><br>
	 * @param node node whose postdominators to find
	 * @return all postdominators of the given node.
	 */
	public Vector pdomVector (Node node)
	{ return getPostDominatorsOfVector (node); }

	/**
	 * Convenience method for converting the Vector to an Array (less efficient) from <code>pdomVector</code>
	 * @param node node whose postdominators to find
	 * @return all postdominators of the given node.
	 */
	public Node[] pdom(Node node)
	{ return getPostDominatorsOf(node); }

	/**
	 * Gets all the postdominators of the specified node.
	 * <br><br>
	 * @param node node whose postdominators to find
	 * @return all postdominators of the given node.
	 */
	public Vector getPostDominatorsOfVector (Node node)
	{ return getAncestorsOfVector (node); }

	/**
	 * Convenience method for converting the Vector to an Array (less efficient) from <code>getPostDominatorsOfVector</code>
	 * @param node node whose postdominators to find
	 * @return all postdominators of the given node.
	 */
	public Node[] getPostDominatorsOf(Node node)
	{ return getAncestorsOf(node); }

	/**
	 * Gets the strict postdominators of the specified node.
	 * <br><br>
	 * The strict postdominators of a node are all the postdominators of that
	 * node except the node itself.
	 *
	 * @param node node whose strict postdominators to find
	 * @return all postdominators of the given node.
	 */
	public Vector getStrictPostDominatorsOfVector (Node node)
	{ return getStrictAncestorsOfVector (node); }

	/**
	 * Convenience method for converting the Vector to an Array (less efficient) from <code>getStrictPostDominatorsOfVector</code>
	 * @param node node whose strict postdominators to find
	 * @return all postdominators of the given node.
	 */
	public Node[] getStrictPostDominatorsOf(Node node)
	{ return getStrictAncestorsOf(node); }

	/**
	 * Returns true iff <code>a</code> postdominates <code>b</code>.
	 */
	public boolean postDominates(Node a, Node b)
	{ return isDescendantOf(a,b); }
	
	/**
	 * Returns true iff <code>a</code> strictly postdominates <code>b</code>.
	 */
	public boolean strictlyPostDominates(Node a, Node b)
	{ return isStrictDescendantOf(a,b); }

	//================================================================//
	//                   F A C T O R Y   S T U F F                    //
	//================================================================//

	/**
	 * Factory method for <code>PostDominatorTree</code>s.
	 * <br><br>
	 * @param cfg the <code>Graph</code> representing the cfg this attribute
	 *            should describe
	 * @return a new <code>PostDominatorTree</code> associated with the given cfg.
	 */
	public static GraphAttribute load(Graph cfg)
	{
		// calculate the dominator info
		DominatorsCalculation pdom = new DominatorsCalculation(cfg, true);
		
		// and give it to a new instance
		PostDominatorTree newAttribute =
				new PostDominatorTreeImpl (pdom.getImmediateDominatorMap());

		// store the attribute
		cfg.addAttribute(newAttribute);

		return newAttribute;
	}

	/** returns an XML representation of this object */
	public String toString ()
	{
		return "<postdominatortree>" + getStringReduced () + "</postdominatortree>";
	}

	/** returns the reduced representation of this object */
	public String toStringReduced ()
	{
		return "<postdominatortree>" + getStringReduced () + "</postdominatortree>";
	}

	/** returns the detailed representation of this object */
	public String toStringDetailed ()
	{
		return "<postdominatortree>" + getStringDetailed () + "</postdominatortree>";
	}
}
