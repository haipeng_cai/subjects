package jaba.graph.dom;

import jaba.graph.Graph;
import jaba.graph.Node;

import java.util.HashMap;
import java.util.BitSet;
import java.util.Map;
import java.util.Arrays;

/**
 * Encapsulates the calculation of dominators of all nodes in a cfg and 
 * calculation of the immediate dominator of each node in the same cfg.
 * <br><br>
 * The algorithm used is an adaptation of the algorithm given on page 671
 * of the dragon book.  Here is a summary:
 * <br><br>
 * Originally we assume that every node is dominated by every other node in the
 * cfg.  From here we can apply the recursive definition that a node's
 * dominator set is the intersection of all of its successors' dominator sets
 * plus the node itself.  We are going to make this computation on a node any
 * time we mark it as needing to be recomputed, and we mark a node as needing
 * to be recomputed any time its successor's dominator set changes.  We
 * bootstrap into the process by marking the entry nodes as needing to be
 * recomputed.  When their sets change, their children will get in line to be 
 * updated, and so on until no more need to be recalculated.
 * <br><br>
 * Note that we could have nodes updated more than once if we have cycles in
 * our cfg, which is quite common.  If we have a cycle, that means somewhere
 * in our algorithm we're going to make a calculation on a node with a
 * successor that has had no updates.  And beause every node eventually gets
 * updated and that successor isn't dominated by everything (which we assume
 * initially) the original node is going to be updated at leaast one more time.
 * <br><br>
 * If we were to process the nodes needing updates in no particular order, we
 * would run into many instances of repeated updates, even if the cfg has no
 * directed cycles.  If, however, we use a queue, processing the nodes in a
 * breadth-first ordering, we eliminate this possiblilty on graphs with no
 * directed cycles (I think).  The result is a much more efficient algorithm.
 * <br><br>
 * Our queue is a coalescing queue because it's possible for more than one
 * successor to request that a node be updated before that node has a chance to
 * be updated, and a single update would be sufficient to fulfill all requests
 * pending.  So we simply ignore requests for update of nodes already pending
 * an update.
 * <br><br>
 * @author <a href="mailto:peterd@cc.gatech.edu">Peter Dillinger</a>
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 */
class DominatorsCalculation extends GenericGraphCalculation
{
	/** ??? */
	private static final boolean DEBUG = false;

	/** A constant representing a NIL reference. */
	private static final int NIL = -1;

	/** A constant representing a non-existant reference. */
	private static final int EMPTY = -2;

	/** ??? */
	DominatorsCalculation(Graph g)
	{
		super(g);
	}

	/** ??? */
	DominatorsCalculation(Graph g, boolean reverse)
	{
		super(g,reverse);
	}

	/**
	 * Encapsulates a coalescing queue.
	 * <br><br>
	 * The input/output range is <code>int</code>s from zero to the given size
	 * minus one.
	 * <br><br>
	 * This class is important because the dequeue is O(1) and the coalescing
	 * enqueue (eliminates duplicates) is also O(1).
	 * @author Jay Lofstead 2003/06/12 changed to implement java.io.Serializable
	 */
	private static class IntQueue implements java.io.Serializable {
		/**
		 * The queue itself.
		 * <br><br>
		 * The queue is actually a linked list whose node-space is preallocated
		 * and O(1) indexable (hence, an array).  The indexes into the array
		 * are the actual values and the values at those indexes are the "next
		 * pointers", which are actually indexes into the array.
		 * <br><br>
		 * An <code>EMPTY</code> value means the index is not an element in the
		 * queue.  The last element in the queue is going to has a <code>NIL
		 * </code> value, though.
		 */
		int[] intQueue;

		/**
		 * Index of the "head pointer".
		 * <br><br>
		 * Notice how intQueue is of length <code>size + 1</code>.  The element
		 * at index <code>size</code> is used as a head pointer.  This
		 * eliminates special cases that would be necessary if the tail
		 * couldn't explicitly point to the head of an empty list.
		 * <br><br>
		 * This variable is initialized to <code>size</code>.
		 */
		final int queueHeadIndex;

		/**
		 * Index of the last entry in the list.
		 */
		int queueTail;

		/**
		 * Creates a new <code>IntQueue</code> with size <code>size</code>.
		 * <br><br>
		 * @param size size of the node-space
		 */
		IntQueue(int size) {
			intQueue = new int[size + 1];
			Arrays.fill(intQueue, EMPTY);
			queueHeadIndex = size;
			queueTail = queueHeadIndex;
			intQueue[queueHeadIndex] = NIL;
		}

		/**
		 * Coalescing enqueue.  If the element is already in the queue,
		 * nothing changes.
		 * <br><br>
		 * @param i element/node to ensure is in the queue
		 */
		void enqueue(int i)
		{
			if (intQueue[i] == EMPTY) {
				intQueue[queueTail] = i;
				intQueue[i] = NIL;
				queueTail = i;
				//if (DEBUG) System.err.println("queue: " + i + " enqueued.");
			} else {
				//if (DEBUG) System.err.println("queue: " + i + " already enqueued.");
			}
		}

		/**
		 * Dequeue.  Removes the element that has been in the queue the longest
		 * and returns it.
		 * <br><br>
		 * @return the element removed
		 */
		int dequeue()
		{
			int toRet = intQueue[queueHeadIndex];
			if (toRet != NIL) {
				intQueue[queueHeadIndex] = intQueue[toRet];
				intQueue[toRet] = EMPTY;
				if (toRet == queueTail) {
					queueTail = queueHeadIndex;
				}
			}
			return toRet;
		}
	}

	/**
	 * Retrieves the predecessors of a given node.
	 * <br><br>
	 * Because we must translate the given <code>int</code> to its
	 * corresponding <code>Node</code> and translate the results back, we are
	 * going to cache the results in a <code>HashMap</code>.
	 * <br><br>
	 * The first call for a particular node will determing the information from
	 * the cfg and throw thw results in the cache.  Any subsequent calls on
	 * that node will retrieve from the cache.
	 * <br><br>
	 * @param node an <code>int</code> corresponding to the node whose
	 *             predecessors to retrieve
	 * @return an <code>int[]</code> conatining the <code>int</code>s
	 *         corresponding to the predecessors of the given node
	 */
	private int[] getMappedPredecessors(int node)
	{
		if (predecessorCache[node] == null) {  // If it's not in the cache

			// Find it from the cfg
			Node [] predecessors = getPredecessors ((jaba.graph.Node) nodes [node]);
			int[] mapped = new int[predecessors.length];
			for (int i = 0; i < predecessors.length; i ++) {
				mapped[i] = ((Integer)ints.get(predecessors[i])).intValue();
			}

			// Throw it in the cache
			predecessorCache[node] = mapped;
		}

		// In either case, return it from the cache
		return predecessorCache[node];
	}

	/**
	 * Retrieves the successors of a given node.
	 * <br><br>
	 * Works much like getMappedPredecessors().
	 * <br><br>
	 * @param node the node
	 * @return its successors
	 */
	private int[] getMappedSuccessors(int node)
	{
		if (successorCache[node] == null) {
			Node [] successors = getSuccessors ((jaba.graph.Node) nodes [node]);
			int[] mapped = new int[successors.length];
			for (int i = 0; i < successors.length; i ++) {
				mapped[i] = ((Integer)ints.get(successors[i])).intValue();
			}

			successorCache[node] = mapped;
		}
		return successorCache[node];
	}

	/** Maps <code>Node</code>s to <code>Node[]</code>s. */
	private HashMap dominatorsMap;

	/** Maps <code>Node</code>s to <code>Node</code>s. */
	private HashMap immediateDominatorMap;

	/** Maps <code>Node</code>s to <code>Integer</code>s. */
	private HashMap ints;

	/** Maps <code>int</code>s to <code>Node</code>s. */
	private Node[] nodes;

	/** Maps <code>int</code>s to <code>int[]</code>s. */
	private int[][] predecessorCache;

	/** Maps <code>int</code>s to <code>int[]</code>s. */
	private int[][] successorCache;

	/** Coalescing queue of <code>int</code>s. */
	private IntQueue nodeQueue;

	/** Maps <code>int</code>s to <code>BitSet</code>s. */
	private BitSet[] dominators;
	
	/** Perform dominator calculations of the specified cfg. */
	public void run()
	{
		// Make sure we haven't been called yet.
		if (dominatorsMap != null) {
			throw new RuntimeException("Calculation already performed.");
		}

		// Initialize *a lot* of stuff.
		dominatorsMap = new HashMap();
		
		nodes = getNodes();
		ints = new HashMap();

		predecessorCache = new int[nodes.length][];
		successorCache = new int[nodes.length][];
	
		nodeQueue = new IntQueue(nodes.length);

		dominators = new BitSet[nodes.length];
		BitSet tempSet = new BitSet(nodes.length);

		for (int i = 0; i < nodes.length; i++) {
			ints.put(nodes[i], new Integer(i));

			tempSet.set(i);
		}

		// Each set initially contains all nodes.
		for (int i = 0; i < nodes.length; i++) {
			dominators[i] = (BitSet) tempSet.clone();
		}

		// Enqueue all the entry points of the cfg.
		Node [] entryNodes = getEntryNodes();
		for (int i = 0; i < entryNodes.length; i ++) {
			int rootNodeNum = ((Integer)ints.get(entryNodes[i])).intValue();
				if (DEBUG) System.err.println("Dom: enqueuing node" + nodes[rootNodeNum].getNodeNumber());
				nodeQueue.enqueue( rootNodeNum );
		}

		// The main loop of the computation.  Removes from the queue until 
		// it's empty.
		int tempNode;
		while ((tempNode = nodeQueue.dequeue()) != NIL) {
			if (DEBUG) System.err.println("Dom: updating node " + nodes[tempNode].getNodeNumber());

			tempSet = new BitSet(nodes.length);

			// Iterate through successors.  If we have no successors, nothing
			// needs to be done.
			int[] predecessors = getMappedPredecessors(tempNode);
			if (predecessors.length > 0) {
				tempSet.or(dominators[predecessors[0]]);
				
				// Intersect with all the other successor sets.
				for (int i = 1; i < predecessors.length; i++) {
					tempSet.and(dominators[predecessors[i]]);
				}

			}

			// Add itself to its set of dominators.
			tempSet.set(tempNode);

			if (!dominators[tempNode].equals(tempSet)) {

				// If the dominator set has changed, this could potentially
				// affect the dominator sets of our predecessors, so we need
				// to make sure they're in the queue to be processed.
				int[] successors = getMappedSuccessors(tempNode);
				for (int i = 0; i < successors.length; i++) {
					nodeQueue.enqueue(successors[i]);
					if (DEBUG) System.err.println("Dom: enqueuing node" + nodes[successors[i]].getNodeNumber());
				}
			}

			dominators[tempNode] = tempSet;
		}
		
		// We're now done with calculating the dominator sets.

		// Now we're gonig to combine two operations:  mapping the dominator
		// sets from ints back to Nodes and computing the immediate dominators
		// of each node.
		//
		// The algorithm we're going to use to determine immediate
		// dominators is quite simple and was made up by me.  We're simply
		// going to find which dominator of each node has one fewer
		// dominators than that node itself.  This works because we know
		// that no two of its dominators can have the same number of
		// dominators themselves.  We can understand this by thinking about
		// the dominator tree.  The set of dominators for a node is the 
		// set of all nodes between it and the root, and there exists only
		// one path from it to the root (or vice-versa).  Inductively, the
		// corresponding subpaths of the intermediate nodes (the
		// dominators) describe their single paths to the root.
		//
		// Now it's easy to see that a node's immediate dominator its one
		// dominator with one fewer dominators than our original node.

		// Something to store our results.
		int iDomMapSize = dominators.length > 5 ? dominators.length*2-1 : 11;
		immediateDominatorMap = new HashMap(iDomMapSize);

		// Compute the number of dominators for each node.
		int[] domCounts = new int[dominators.length];
		for (int i = 0; i < dominators.length; i++) {
			for (int j = 0; j < dominators[i].length(); j++) {
				if (dominators[i].get(j)) domCounts[i]++;
			}
		}

		// Iterate through the nodes.
		for (int i = 0; i < dominators.length; i++) {
			Node[] dominatorsNode = new Node[domCounts[i]];

			// If this node is it's only dominator (causing the erroneous
			// deduction that it's immediat dominator would have 0 dominators),
			// then we must indicate that this node has no immediate
			// dominator.
			if (domCounts[i] == 1) {
				immediateDominatorMap.put(nodes[i], null);
			}

			int setPosition = -1;

			// Iterate the possible dominator numbers.
			for (int j = 0; j < dominatorsNode.length; j++) {
				// Find the next dominator in the set.
				do { setPosition++; } while (! dominators[i].get(setPosition));
				
				// Store this dominator into the array.
				dominatorsNode[j] = (jaba.graph.Node) nodes[setPosition];

				// If this is the immediate dominator, store it.
				// 
				// We're relying on algorithmic correctness for one (and only
				// one) to be found through the iterations of this loop.
				if (domCounts[setPosition] == (domCounts[i] - 1)) {
					immediateDominatorMap.put(nodes[i],
									dominatorsNode[j]);
				}
			}

			// Mapping stuff.
			dominatorsMap.put(nodes[i], dominatorsNode);
		}
		if (DEBUG) System.err.println("Finishing up... ");
	}

	/**
	 * Return the result of calculating each node's dominators.
	 * <br><br>
	 * @return a <code>Map</code> mapping <code>Node</code>s to <code>Node[]
	 *         </code>s of their dominators
	 */
	Map getDominatorsMap()
	{
		if (dominatorsMap == null) {
			run();
		}
		return dominatorsMap;
	}

	/**
	 * Returns the result of calculating each node's immediate dominator.
	 * <br><br>
	 * @return a <code>Map</code> mapping <code>Node</code>s to <code>Node
	 *         </code>s of thier immediate dominators.
	 */
	Map getImmediateDominatorMap()
	{
		if (immediateDominatorMap == null) {
			run();
		}
		return immediateDominatorMap;
	}
}
