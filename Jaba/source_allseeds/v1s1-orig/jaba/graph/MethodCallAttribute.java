/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Method;

import jaba.du.DefUse;

import java.util.Vector;
import java.util.Enumeration;

/**
 * Attribute that needs to be stored with a method-call (statement) node.
 * @author Jim Jones -- <i>Created.</i>
 * @author S. Sinha -- <i>Revised.</i>
 * @author Huaxing Wu -- <i> Revised. 8/12/02. in addMethod, check to not add
 *                      abstract/interface method.
 * @author Huaxing Wu -- <i> Revised. 12/10/02. Add getSyntaxMethod, in addMethod, allow abstract/interface method. </i>
 * @author Jay Lofstead -- 2003/05/29 updated toString to generate XML and removed commented out code.
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface MethodCallAttribute extends CallAttribute
{
    /** ??? */
    public void setSyntaxMethod(Method m);

    /** ??? */
    public Method getSyntaxMethod();

  /**
   * Adds a Vector of actual parameters to this call site.  This method keeps 
   * track of the order in which the vector of actual parameters were added.  
   * The reason each element must be a vector is demonstrated in a call such as
   * m( i + j ) or m( (i==0)?x:y ).  In the first, the vector would contain
   * {i,j}, and in the second, it would contain {x,y}.
   * @param params  The vector of actual parameters.
   */
  public void addActualParameters( Vector params ) throws IllegalArgumentException;

  /**
   * Returns an array of arrays of actual parameters used at this call site.
   * @return An array of arrays of actual parameters used at this call site.  
   *         The first dimension represents the position of the actual
   *         actual parameters, and the second dimension represents all of the
   *         def use objects used at that position.
   *         Example:  m( i, j+k );
   *         getActualParameters() --> [0][0] => i
   *                                   [1][0] => j
   *                                   [1][1] => k
   */
  public DefUse[][] getActualParameters();

  /**
   * Adds a method that can be called from this call site.
   * @param method  Possible target of this call site.
   */
  public void addMethod( Method method ) throws IllegalArgumentException;

  /**
   * Returns an array of methods that can be called from this call site.
   * @return An array of methods that can be called from this call site.
   */
	public Vector getMethodsVector ();

  /**
   * Returns an array of methods that can be called from this call site.
   * @return An array of methods that can be called from this call site.
   */
	public Method [] getMethods ();

    /**
     * Returns a string representation of a method-call attribute.
     * @return String representation of method-call attribute.
     */
    public String toString();
}
