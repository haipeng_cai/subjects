/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Method;
import jaba.sym.MethodAttribute;

/**
 * Represents a graph created for a method.
 *
 * @author S. Sinha
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface MethodGraph extends Graph, MethodAttribute
{
    /**
     * Returns the method for which the graph is built.
     * @return Method for which graph is built.
     */
    public Method getMethod ();
}
