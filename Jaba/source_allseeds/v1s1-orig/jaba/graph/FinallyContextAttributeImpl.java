/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Class;

import java.util.Vector;
import java.util.Enumeration;

/**
 * Attribute stored with an inedge to a FINALLY_START_NODE or an outedge
 * from a FINALLY_END_NODE in the CFG and the ICFG. Those nodes and
 * therefore, this attribute, are used only in graphs in which finally
 * blocks have been inlined into their containing method's representation.
 *
 * The FinallyContextAttribute stores the list of exception types
 * (<code>jaba.sym.Class</code>) that cause the corresponding edge to be
 * traversed (and the associated finally block to be executed) in an
 * exception context. If there are no exception types stored in the
 * attribute, the corresponding edge is traversed (and the associated
 * finally block is executed) in a normal context. 
 *
 * @author S. Sinha -- <i>Created, Dec. 12, 2002</i>.
 * @author Jay Lofstead 2003/05/29 changed toString to generate XML
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 code merge with main jaba branch
 * @author Jay Lofstead 2003/07/10 changed to implement FinallycontextAttribute
 */
public class FinallyContextAttributeImpl extends EdgeAttributeImpl implements FinallyContextAttribute
{
    /**
     * Exception types that enable the edge that this attribute is
     * attached to, to be traversed.
     */
    private Vector exceptionTypes;

    /**
     * Creates an empty throw-statement attribute.
     */
    public FinallyContextAttributeImpl ()
    {
        exceptionTypes = new Vector();
    }

    /**
     * Adds an exception type to the set of exception types that cause the
     * corresponding edge to be traversed.
     * @param excpType  Exception type that cause the corresponding edge
     *                  to be traversed.
     * @throws IllegalArgumentException if <code>excpType</code> is
     *                                     <code>null</code>.     */
    public void addExceptionType( Class excpType )
        throws IllegalArgumentException {
	if ( excpType == null ) {
	    IllegalArgumentException e = new IllegalArgumentException(
	        "excpType is null" );
	    e.printStackTrace();
	    throw e;
	}
	if ( !exceptionTypes.contains( excpType ) ) {
	    exceptionTypes.add( excpType );
	}
    }

    /**
     * Returns the exception types that cause the
     * corresponding edge to be traversed.
     * @return Array of exception types that cause the corresponding edge
     *         to be traversed; <code>null</code> if there are no exception
     *         types.
     */
	public Vector getExceptionTypesVector ()
	{
		return exceptionTypes;
	}

    /**
     * Returns the exception types that cause the
     * corresponding edge to be traversed.
     * @return Array of exception types that cause the corresponding edge
     *         to be traversed; <code>null</code> if there are no exception
     *         types.
     */
	public Class [] getExceptionTypes ()
	{
		return (Class []) exceptionTypes.toArray (new Class [exceptionTypes.size ()]);
	}

    /**
     * Returns a string representation of a finally-context attribute.
     * @return String representation of finally-context attribute.
     */
    public String toString()
	{
		StringBuffer str = new StringBuffer (1000);

		str.append ("<finallycontextattribute>");

		for (Enumeration i = exceptionTypes.elements (); i.hasMoreElements ();)
		{
			str.append ("<exceptiontype>");
			str.append (((Class) i.nextElement ()).getName ());
			str.append ("</exceptiontype>");
		}

		str.append ("</finallycontextattribute>");

		return str.toString ();
    }
}
