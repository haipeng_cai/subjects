/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Class;

/**
 * Attribute that needs to be stored with a throw or catch site node.
 * @author Jay Lofstead -- 2003/05/29 changed toString to generate XML
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference a member
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface ExceptionAttribute extends StatementNodeAttribute
{
  /**
   * Assigns the exception being thrown or caught at this node.
   * @param exception  The exception being thrown or caught at this node.
   */
  public void setExceptionType( Class exception ) throws IllegalArgumentException;

  /**
   * Returns the exception being thrown or caught at this node.
   * @return  The exception being thrown or caught at this node.
   */
  public Class getExceptionType();

    /**
     * Returns a string representation of an exception attribute.
     * @return String representation of exception attribute.
     */
    public String toString();
}
