package jaba.graph;

/**
 * Provides a Comparator for ordering a list of nodes in numerical order.
 * @author Jay Lofstead 2003/06/06 created
 */
public class NodeSortComparator implements java.util.Comparator
{
	public int compare (Object l, Object r)
	{
		return ((Node) l).getNodeNumber () - ((Node) r).getNodeNumber ();
	}

	public boolean equals (Object o)
	{
		return false;
	}
}
