/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Program;
import jaba.sym.ProgramAttribute;

/**
 * Represents a graph created for a program.
 * @author Jim Jones -- <i>Mar. 1, 1999.  Created.</i>
 * @author Jay Lofstead 2003/07/01 changed to implement ProgramGraph
 */
public abstract class ProgramGraphImpl extends GraphImpl implements ProgramAttribute, ProgramGraph
{
    /** Program for which the graph represents. */
    private Program program;
    
    /**
     * Constructor.
     * @param program  Program for which this graph describes.
     */
    public ProgramGraphImpl ( Program program )
    {
	this.program = program;
    }

    public Program getProgram()
    {
	return program;
    }
    
}
