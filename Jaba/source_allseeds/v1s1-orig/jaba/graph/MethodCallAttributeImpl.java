/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Method;

import jaba.du.DefUse;

import java.util.Vector;
import java.util.Enumeration;

/**
 * Attribute that needs to be stored with a method-call (statement) node.
 * @author Jim Jones -- <i>Created.</i>
 * @author S. Sinha -- <i>Revised.</i>
 * @author Huaxing Wu -- <i> Revised. 8/12/02. in addMethod, check to not add
 *                      abstract/interface method.
 * @author Huaxing Wu -- <i> Revised. 12/10/02. Add getSyntaxMethod, in addMethod, allow abstract/interface method. </i>
 * @author Jay Lofstead -- 2003/05/29 updated toString to generate XML and removed commented out code.
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/07/10 changed to implement MethodCallAttribute
 */
public class MethodCallAttributeImpl extends CallAttributeImpl implements MethodCallAttribute
{
  /** All actual parameters used at this call site. */
  private Vector actualParameters;

  /** All methods that can be called from this call site. */
  private Vector methods;

    /** ??? */
    private Method syntaxMethod;

  /**
   * Constructor
   */
  public MethodCallAttributeImpl ()
    {
      // initialize all fields
      super();
      actualParameters = new Vector();
      methods = new Vector();
    }

    public void setSyntaxMethod(Method m) {
	syntaxMethod = m;
    }

    public Method getSyntaxMethod() { return syntaxMethod; }

  /**
   * Adds a Vector of actual parameters to this call site.  This method keeps 
   * track of the order in which the vector of actual parameters were added.  
   * The reason each element must be a vector is demonstrated in a call such as
   * m( i + j ) or m( (i==0)?x:y ).  In the first, the vector would contain
   * {i,j}, and in the second, it would contain {x,y}.
   * @param params  The vector of actual parameters.
   */
  public void addActualParameters( Vector params ) 
    throws IllegalArgumentException
    {
      if ( params == null )
	throw new IllegalArgumentException( "addActualParameter( null )" );

      actualParameters.add( params );
    }


  /**
   * Returns an array of arrays of actual parameters used at this call site.
   * @return An array of arrays of actual parameters used at this call site.  
   *         The first dimension represents the position of the actual
   *         actual parameters, and the second dimension represents all of the
   *         def use objects used at that position.
   *         Example:  m( i, j+k );
   *         getActualParameters() --> [0][0] => i
   *                                   [1][0] => j
   *                                   [1][1] => k
   */
  public DefUse[][] getActualParameters()
    {
	DefUse[][]actualParametersArr = null;
        if ( actualParametersArr == null )
	{
            actualParametersArr = new DefUse[ actualParameters.size() ][];
	    int i = 0;
            for (Enumeration e1 = actualParameters.elements (); e1.hasMoreElements (); i++)
	    {
		Vector v = (Vector) e1.nextElement ();
	        actualParametersArr[i] = new DefUse[ (v).size() ];
		int j = 0;
	        for (Enumeration e2 = v.elements (); e2.hasMoreElements (); j++)
		{
	            actualParametersArr[i][j] = (DefUse) e2.nextElement ();
                }
 	    }
        }
        return actualParametersArr; 
    }


  /**
   * Adds a method that can be called from this call site.
   * @param method  Possible target of this call site.
   */
  public void addMethod( Method method ) throws IllegalArgumentException
    {
      if ( method == null )
	  throw new IllegalArgumentException( "jaba.graph." +
					      "MethodCallCallAttribute."+
					      "addMethod( null )" );

      if ( methods.contains( method ) ) {
	  return;
      }

      methods.addElement( method );
    }


  /**
   * Returns an array of methods that can be called from this call site.
   * @return An array of methods that can be called from this call site.
   */
	public Vector getMethodsVector ()
	{
		return methods;
	}

  /**
   * Returns an array of methods that can be called from this call site.
   * @return An array of methods that can be called from this call site.
   */
	public Method [] getMethods ()
	{
		return (Method []) methods.toArray (new Method [methods.size ()]);
	}

    /**
     * Returns a string representation of a method-call attribute.
     * @return String representation of method-call attribute.
     */
    public String toString()
	{
		StringBuffer str = new StringBuffer (1000);

		str.append ("<methodcallattribute>");

		Vector methods = getMethodsVector ();
		for (Enumeration i = methods.elements (); i.hasMoreElements ();)
		{
			Method m = (Method) i.nextElement ();
			str.append ("<callablemethod>");
			str.append (jaba.tools.Util.toXMLValid (m.getContainingType ().getName ()) + "." + jaba.tools.Util.toXMLValid (m.getName ()) + " (" + m.getDescriptor () + ") ");
			str.append ("</callablemethod>");
		}

		DefUse [][] params = getActualParameters ();
		for (int i = 0; i < params.length; i++)
		{
			str.append ("<actualparams>");
			for (int j = 0; j < params [i].length; j++)
			{
				str.append ("<param id1=\"" + i + "\" id2=\"" + j + "\">");
				str.append (params [i][j]);
				str.append ("</param>");
			}
			str.append ("</actualparams>");
		}

		str.append ("<syntaxmethod>");
		str.append (jaba.tools.Util.toXMLValid (syntaxMethod.getFullyQualifiedName ()));
		str.append ("</syntaxmethod>");

		str.append ("</methodcallattribute>");

		return str.toString ();
    }
}
