/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Class;

import java.util.Vector;
import java.util.Enumeration;

/**
 * This attribute is placed on all edges from a call node to the entry of a
 * method.
 * @author Jim Jones -- <i>Created, May 28, 1999</i>.
 * @author Jay Lofstead -- 2003/05/29 changed toString to generate XML.
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/07/10 changed to implement MethodCallEdgeAttribute
 */
public class MethodCallEdgeAttributeImpl extends EdgeAttributeImpl implements MethodCallEdgeAttribute
{
  /**
   * All object types that enable the edge that this attribute is attached to,
   *  to be traversed.
   */
  private Vector traversalTypes;

    /**
     * Array copy of the corresponding vector field. The first call to
     * <code>getTraversalTypes()</code> copies the vector to the array,
     * and sets the vector to null.
     */
    private Class[] traversalTypesArr;

  /** Constructor. */
  public MethodCallEdgeAttributeImpl ()
    {
      traversalTypes = new Vector();
    }

  /**
   * Adds an object type that causes this edge to be traversed.
   * @param classType  Object type that causes this edge to be traversed.
   */
  public void addTraversalType( Class classType )
    {
      traversalTypes.add( classType );
    }

  /**
   * Returns all object types that cause this edge to be traversed.
   * @return All object types that cause this edge to be traversed.
   */
  public Class[] getTraversalTypes()
    {
        if ( traversalTypesArr == null ) {
            traversalTypesArr = new Class[ traversalTypes.size() ];
	    int i = 0;
            for (Enumeration e = traversalTypes.elements (); e.hasMoreElements (); i++)
	    {
	        traversalTypesArr [i] = (Class) e.nextElement ();
            }
            traversalTypes = null;
        }
        return traversalTypesArr;
    }

  /**
   * Returns a string representation of this attribute.
   * @return A string representation of this attribute.
   */
  public String toString()
    {
	StringBuffer str = new StringBuffer (1000);

	str.append ("<methodcalledgeattribute>");

	Class [] tt = getTraversalTypes ();
	for (int i = 0; i < tt.length; i++)
	{
		str.append ("<traversaltype>");
		str.append (tt [i].getName ());
		str.append ("</traversaltype>");
	}

	str.append ("</methodcalledgeattribute>");

	return str.toString ();
    }
}
