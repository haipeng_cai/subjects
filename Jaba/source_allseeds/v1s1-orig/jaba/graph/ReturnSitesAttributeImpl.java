/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import java.util.Vector;
import java.util.Enumeration;

/**
 * Attribute to specify the potential return sites for a call site. A call
 * site that has more than one potential return site is a PNRC.

 * @author S. Sinha
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/04 removed private node number in favor of one at Node
 * @author Jay Lofstead 2003/07/10 changed to implement ReturnSitesAttribute
 */
public class ReturnSitesAttributeImpl implements ReturnSitesAttribute, StatementNodeAttribute
{
    /** Statement nodes where control can return following the call. */
    private Vector returnSites;

    /**
     * Array copy of the corresponding vector field. A call to
     * <code>getReturnSites()</code> copies the vector to the array, and
     * sets the vector to null. A call to <code>addReturnSite</code>
     * copies the array back to the vector, sets the array to null, and
     * adds the new element to the vector.
     */
    private StatementNode[] returnSitesArr;

    /**
     * Constructor
     */
    public ReturnSitesAttributeImpl ()
    {
        returnSites = new Vector();
    }


    /**
     * Adds a statement node to the set of potential return sites.
     * @param node statement node to be added.
     * @throws IllegalArgumentException if <code>node</code> is
     *                                  <code>null</code>.
     */
    public void addReturnSite( StatementNode node ) throws IllegalArgumentException
    {
        if ( node == null ) {
	        throw new IllegalArgumentException(
                "ReturnSitesAttribute.addActualParameter( null )" );
        }
        /* if vector of return sites is null, copy elements from array back to
           the vector, set array to null, and add node to the vector of
           return sites. */
        if ( returnSites == null ) {
            returnSites = new Vector( returnSitesArr.length+5 );
            for ( int i=0; i<returnSitesArr.length; i++ ) {
                /* if node already appears in the list of return sites, do not
                   add it again */
                if ( returnSitesArr[i] == node ) {
                    returnSites = null;
                    return;
                }
                returnSites.add( returnSitesArr[i] );
            }
            returnSitesArr = null;
            returnSites.add( node );
            return;
        }
	if ( returnSites.contains( node ) ) return;
        returnSites.add( node );
    }


    /**
     * Returns an array of statement nodes that are potential return sites
     * for the corresponding call site.
     * @return Array of potential return-site nodes.
     */
    public StatementNode[] getReturnSites()
    {
        if ( returnSitesArr == null ) {
            returnSitesArr = new StatementNode[returnSites.size()];
	    int i = 0;
            for (Enumeration e = returnSites.elements (); e.hasMoreElements (); i++)
	    {
                returnSitesArr [i] = (StatementNode) e.nextElement ();
            }
            returnSites = null;
        }
        return returnSitesArr;
    }

    /**
     * Returns a string representation of a return-sites attribute.
     * @return String representation of return-sites attribute.
     */
    public String toString()
	{
		StringBuffer str = new StringBuffer (1000);

		str.append ("<returnsiteattribute>");
		StatementNode [] retSites = getReturnSites ();
		for (int i = 0; i < retSites.length; i++)
		{
			str.append ("<returnsitenodenumber>").append (retSites [i].getNodeNumber ()).append ("</returnsitenodenumber>");
		}
		str.append ("</returnsiteattribute>");

		return str.toString ();
    }
}
