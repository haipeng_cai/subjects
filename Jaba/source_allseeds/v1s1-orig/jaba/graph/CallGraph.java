package jaba.graph;

import jaba.main.DottyOutputSpec;

import java.io.IOException;

/**
 * Represents a call graph.
 * A call graph shows the interrelationship of which methods call which other methods
 * @author Caleb Ho -- <i>Created 5/2001</i>
 * @author Huaxing Wu -- <iRevised 11/20/02, fix bugs in create graph, not 
 * create edge to interface method.Also use more scalable way to generate dotty</i> 
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author jay Lofstead 2003/06/05 cleaned up enumerations
 * @author Jay Lofstead 2003/06/05 changed toString to use getString
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/16 added toStringReduced (), toStringDetailed ()
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface CallGraph extends ProgramGraph
{
	/**
	 * creates the file needed by Dotty to visually display the graph.
	 * @param filename is the file the dotty simulation will be stored in.
	 * @param spec is the DottyOutputSpec representing what customizations the user wants when
	 *  displaying the graph  
	 */
	public void createDottyFile(String filename, DottyOutputSpec spec) throws IOException;

	/**
	 * This allows the the user to only have to pass in the output specs, and then this
	 * method automatically creates the dotty file, and immediately displays the graph after
	 * the dotty file has been created
	 */
	public void displayGraph( DottyOutputSpec spec ) throws java.io.IOException;

	/** returns the call graph in string format */
	public String toString ();

	/** returns the reduced representation of this object */
	public String toStringReduced ();

	/** returns the detailed representation of this object */
	public String toStringDetailed ();
}
