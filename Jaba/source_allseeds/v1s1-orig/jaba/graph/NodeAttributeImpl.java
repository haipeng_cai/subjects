/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Attribute;

/**
 * All node attributes inherit from this class.  Represents any extra
 * information that needs to be stored with a node.
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to implement NodeAttribute
 */
public abstract class NodeAttributeImpl implements NodeAttribute, Attribute, java.io.Serializable
{
}
