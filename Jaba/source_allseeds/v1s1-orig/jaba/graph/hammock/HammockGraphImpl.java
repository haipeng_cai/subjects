package jaba.graph.hammock;

import jaba.graph.Graph;
import jaba.graph.GraphImpl;
import jaba.graph.GraphAttribute;
import jaba.graph.Node;
import jaba.graph.Tree;
import jaba.graph.Edge;
import jaba.graph.EdgeImpl;
import jaba.graph.StatementNode;
import jaba.graph.StatementNodeImpl;

import jaba.graph.dom.DominatorTree;
import jaba.graph.dom.PostDominatorTree;
import jaba.graph.dom.DominatorTreeImpl;
import jaba.graph.dom.PostDominatorTreeImpl;

import jaba.sym.DemandDrivenAttribute;

import java.util.Vector;
import java.util.Stack;
import java.util.HashMap;
import java.util.Enumeration;

//for dotty output stuff
import jaba.main.DottyOutputSpec;

import jaba.sym.Method;

import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;

import java.util.Hashtable;

/**
 * Represents hammocks in a single entry single exit Graph, where a 
 * hammock is a group of nodes where all incoming edges to the hammock
 * go to one node in the hammock and all outgoing edges from the 
 * hammock go to one node outside of the hammock.  The hammock is 
 * represented in the graph by place holder nodes, which means a hammock 
 * entry node is placed directly before a hammock header and a hammock 
 * join node is placed directly before the hammock exit. Then all edges 
 * that enter the header in the Graph now enter the hammock 
 * entry node, and the only edge leaving the entry node goes to the 
 * header.  Also, all the edges entering the hammock exit in the 
 * Graph now enter the hammock join node and the only edge from the 
 * hammock join node goes to the hammock exit.  Throughout the rest of 
 * the documentation header and hammock exit nodes will refer to hammock 
 * entry nodes and hammock join nodes, unless otherwise stated.
 * Note: only the return edge of a call node is traversed.
 * things to do
 * fix up dotty stuff... (make it cleaner so it works for all node types)
 * optimizations
 * merge consecutive mod nodes
 * if there is a bug it is probably in load where I try
 *      to find where to insert the hammock nodes
 * a note on dotty output: sometimes what you
 *      see with regard to subgraphs isn't what the
 *      file says, when in doubt check the file
 * note: The dotty method was copied from Graph and modified
 * @author K. Repine repinekm@rose-hulman.edu
 * @author Jay Lofstead 2003/06/02 replaced elementAt with enumeration for better performance
 * @author Jay Lofstead 2003/06/04 changed to use getNodeNumber
 * @author Jay Lofstead 2003/06/05 removed node number parameter to setUpPair
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 added toString
 * @author Jay Lofstead 2003/06/05 added implementing of DemandDrivenAttribute to allow it to be created.
 * @author Jay Lofstead 2003/06/06 changed to use getStringReduced in toString
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/06/16 added toReducedString (), toDetailedString ()
 * @author Jay Lofstead 2003/07/10 changed to implement HammockGraph
 * @author Jay Lofstead 2003/07/17 removed legacy load ()
 */
public class HammockGraphImpl extends GraphImpl implements GraphAttribute, DemandDrivenAttribute, HammockGraph
{
	/** ??? */
    private static boolean DEBUG = false;

    /** The hammock headers in the <code>HammockGraph</code>. */
    private HammockNode[] headers;

    /** The maximum nesting depth of the hammocks. */
    private int maxNestingDepth;

    /** The <code>Graph</code> for which the HammockGraph was constructed. */
    private Graph parent;

    //constructor
    /** 
     * Creates a new <code>HammockGraph</code> for <code>Graph</code> g.
     * @param g The <code>Graph</code> to construct the 
     * <code>HammockGraph</code> for.
     */
    private HammockGraphImpl (Graph g)
    {
	//initialize fields
	super();
	parent = g;
	headers = null;
	maxNestingDepth = -1;
    }

    //Methods

    /** Returns an array of all the hammock headers in the graph.
     *  @return The hammock header place holder nodes.
     */
    public Node[] getHammockHeaders()
    {
	return headers;
    }

    /** Returns the number of hammocks in the graph.
     *  @return The number of hammocks in the graph.
     */
    public int getNumberHammocks()
    {
	return headers.length;
    }

    /** Returns the maximum nesting of a hammock in the graph.  For example
     *  if there were three hammocks in a graph and the second occurred
     *  inside the first and the third occurred inside the second, then
     *  the maximum nesting of a hammock in the graph would be three.
     *  @return The maximum nesting depth of a hammock in the graph.
     */
    public int getMaxNestingDepth()
    {
	int max = 0;
	int tempInt;
	if (maxNestingDepth == -1){
	    //set the nesting depths
	    for (int j = 0 ; j< headers.length;j++){
		tempInt =  getHammockNestingDepth (headers[j]);
		if (tempInt > max)
		    max = tempInt;
		if (DEBUG)
		    System.out.println(tempInt + "is Nesting Depth of "+
				       headers[j].getNodeNumber());
	    }
	}
	maxNestingDepth = max;
	return maxNestingDepth;
    }

    /**
     * Returns the <code>Graph</code> for which this is the 
     * <code>HammockGraph</code> of.
     * @return The parent MethodGraph.
     */
    public Graph getParentGraph()
    {
	return parent;
    }

    /**
     * Checks if the nesting depth has been calculated, if not it 
     * calculates and sets it (and its other's), then it returns 
     * the nesting depth of node n.
     * @param n A hammock node to find the nesting depth of.
     * @return The nesting depth of n.
     */
    private int getHammockNestingDepth(HammockNode n)
    {
	Node exitNode;
	if (n.getType() == HammockNode.HAMMOCK_HEADER){
	    //perhaps should check that other exists and
	    //that there is only one out edge
	    exitNode =  getOutEdges(n.getOther())[0].getSink();
	}else{
	    //perhaps should check that other exists and
	    //that there is only one out edge, and
	    //that n is an exit node
	    exitNode =  getOutEdges(n)[0].getSink();
	}

	int depth;
	if((exitNode instanceof HammockNode)&&
	   (((HammockNode)exitNode).getType() == HammockNode.HAMMOCK_HEADER)){ 
	    depth = getNestingDepth ((jaba.graph.Node) exitNode);
	}else{
	    depth = getNestingDepth ((jaba.graph.Node) exitNode) + 1;
	}
	n.setNestingDepth(depth);
	n.getOther().setNestingDepth(depth);
	return depth;
    }

    /**
     * Returns the nesting depth of node n.  The nesting
     * depth can be viewed as the number of hammocks a
     * node is in.
     * @param n The node to find the nesting depth of.
     * @return The nesting depth of n.
     */
    public int getNestingDepth(Node n)
    {
	HammockNode temp = getMinHammock(n);
	if (temp == null)
	    return 0;
	if (temp.getNestingDepth() == -1)
	    return getHammockNestingDepth(temp);
	return temp.getNestingDepth();
    }

    /**
     * Returns the exit of the hammock, if header is a hammock header,
     * otherwise, it returns null.
     * @param header The hammock header to find the exit of.
     * @return The exit of the hammock with header, header, or null.
     */
    public Node getHammockExit(Node header)
    {
	if (header instanceof HammockNode){
	    if (((HammockNode)header).getType() == HammockNode.HAMMOCK_HEADER)
		return ((HammockNode)header).getOther();
	}
	return null;
    }

    /**
     * Returns the header of the hammock, if exit is a hammock exit,
     * otherwise, it returns null.
     * @param exit The hammock exit to find the header of.
     * @return The header of the hammock with hammock exit, exit, or null.
     */
    public Node getHammockHeader(Node exit)
    {
	if (exit instanceof HammockNode){
	    if (((HammockNode)exit).getType() == HammockNode.HAMMOCK_EXIT)
		return ((HammockNode)exit).getOther();
	}
	return null;
    }

    /**
     * Returns the child of n, if n is a 1-node, otherwise it returns 
     * <code>null</code>.  A 1-node is a node with exactly one incoming 
     * edge and exactly one outgoing edge.  If n is a hammock header or 
     * exit, then n is treated like a collapsed hammock node, which means 
     * the incoming edges are the in-edges of the header and the outgoing 
     * edges are the out-edges of the exit.
     * @param n The node to find the child of.
     * @return The child of n, if n is a 1-node, otherwise <code>null</code>.
     */
    public Node getOneNodeChild(Node n)
    {
	if (n==null)
	    return null;
	if (n instanceof HammockNode){
	    if (((HammockNode)n).getType() == HammockNode.HAMMOCK_HEADER){
		if ((getInEdges(n).length == 1)&&
		    (getOutEdges(getHammockExit(n)).length == 1)){
		    return (jaba.graph.Node) getOutEdges(getHammockExit(n))[0].getSink();
		}else return null;
	    } else {
		if (((HammockNode)n).getType() == HammockNode.HAMMOCK_EXIT){
		    if ((getOutEdges(n).length == 1)&&
			(getInEdges(getHammockHeader(n)).length == 1)){
			return (jaba.graph.Node) getOutEdges(n)[0].getSink();
		    }else return null;
		}else return null; //improperly formed hammock graph if get here
	    }
	} else {
	    int numIn = getInEdges(n).length;
	    if (numIn == 1){
		Edge [] outEdges = getOutEdges (n);
		if (outEdges.length == 1){
		    return (jaba.graph.Node) outEdges[0].getSink();
		}else {
		    return null;
		}
	    }else return null;
	}
    }

    /**
     * Returns the header of the minimum hammock containing n, or 
     * <code>null</code> if none.
     * @param n The node to find the minimum hammock of.
     * @return The header of the minimum hammock containing n, or 
     * <code>null</code> if none.
     */
    public HammockNode getMinHammock(Node n)
    {
	//check if n is a HammockNode
	//what should happen if n is a hammock node?
	if (n instanceof HammockNode){
	    if (((HammockNode)n).getType() == HammockNode.HAMMOCK_HEADER)
		return (HammockNode) n;
	    else{
		if (((HammockNode)n).getType() == HammockNode.HAMMOCK_EXIT)
		    return ((HammockNode)n).getOther();
		else return null;
	    }
	}else{
	    //idea: do dfs until reach 1st hammock exit or G.exit
	    //then use hammock exit to get header
	    //remember to skip over hammocks including their exits
	    //during the dfs

	    Stack stack = new Stack();
	    stack.push(n);
	    Node tempNode;
	    Edge[] tempOutEdges;
	    //should prob. use hashtable/map instead of vector
	    Vector visited = new Vector();
	    while (!stack.isEmpty()){
		tempNode = (Node) stack.pop();
		if (visited.contains(tempNode))
		    continue;
		visited.addElement(tempNode);
		if (tempNode instanceof HammockNode){
		    if (((HammockNode)tempNode).getType() == 
			HammockNode.HAMMOCK_HEADER){
			tempNode = ((HammockNode)tempNode).getOther();
			if (visited.contains(tempNode))
			    continue;
			visited.addElement(tempNode);
		    }
		    else{
			if (((HammockNode)tempNode).getType() == 
			    HammockNode.HAMMOCK_EXIT)
			    return ((HammockNode)tempNode).getOther();
			else return null;
		    }
		}
		
		tempOutEdges = getOutEdges(tempNode);
		for(int i=0;i<tempOutEdges.length;i++){
		    stack.push(tempOutEdges[i].getSink());
		}
	    }
	    return null;
	}
    }

    /**
     * Returns the set of nodes in the hammock with header, header,
     * not including the place holder nodes of that hammock.  Returns 
     * <code>null</code> if header is not a header.  Note that if there 
     * is a hammock within the hammock lead by header, then the header 
     * node of that hammock will be returned and none of the node 
     * interior nodes will be returned.
     * @param header A hammock header.
     * @return a Vector of Node objects in the hammock, or <code>null</code>.
     */
    public Vector getHammockNodesVector (Node header)
    {
	if (!(header instanceof HammockNode) || !(((HammockNode)header).getType() 
						  == HammockNode.HAMMOCK_HEADER))
	    return null;
	Stack stack = new Stack();
	stack.push(header);
	HammockNode exit = ((HammockNode) header).getOther();
	Node tempNode;
	Vector hammock = new Vector();
	Edge[] outEdges = null;

	//do a depth first search to get all the nodes

	while (!stack.isEmpty()){
	    tempNode = (Node)stack.pop();
	    if (!tempNode.equals(exit)){
		if(!hammock.contains(tempNode)){
		    if(!tempNode.equals(header)){
			hammock.addElement(tempNode);
			if (tempNode instanceof HammockNode){
			    //if tempNode is not a header the graph is malformed
			    outEdges = getOutEdges(((HammockNode)tempNode).
							getOther());
			} else outEdges = getOutEdges(tempNode);
		    } else outEdges = getOutEdges(tempNode);
		    
		    for (int i=0;i<outEdges.length;i++){
			stack.push(outEdges[i].getSink());
		    }
		}
	    }
	}
	
        return hammock;
    }

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getHammockNodesVector</code>
     * @param header A hammock header.
     * @return a Vector of Node objects in the hammock, or <code>null</code>.
     */
    public Node [] getHammockNodes(Node header)
	{
		Vector v = getHammockNodesVector (header);

		return (Node []) v.toArray (new Node [v.size ()]);
	}

    /**
     * Checking whether the node n is inside the hammock
     * whose header is h.
     * @param h The header of the hammock
     * @param n The node to be checked
     * @return true, if inside, if outside or h is not header, return false.
     */
    public boolean isNodeInsideHammock(Node h, Node m)
    {
	if (!(h instanceof HammockNode) || !(((HammockNode)h).getType() 
						  == HammockNode.HAMMOCK_HEADER))
	    return false;
	Stack stack = new Stack();
	stack.push(h);
	HammockNode exit = ((HammockNode) h).getOther();
	Node tempNode;
	Vector nodesInHammock = new Vector();
	Edge[] outEdges = null;

	//do a depth first search to get all the nodes

	while (!stack.isEmpty()){
	    tempNode = (Node)stack.pop();
	    if (!tempNode.equals(exit)){
		if ( !nodesInHammock.contains( tempNode ) ) {
		    nodesInHammock.addElement( tempNode );
		    if (tempNode.equals(m)) {
			return true;
		    }
		    outEdges = getOutEdges(tempNode);		
		    for (int i=0;i<outEdges.length;i++){
			stack.push(outEdges[i].getSink());
		    }
		}
	    }
	}
	
        return false;
    }

    /**
     * Calculates the number of nodes in the hammock
     * whose header is h.
     * @param h The header of the hammock.
     * @return The number of nodes between and including
     * h and h's exit.
     */
    public int getNumNodesInHammock(HammockNode h)
    {
	if (h.getType()!=HammockNode.HAMMOCK_HEADER)
	    return 0;
	Stack stack = new Stack();
	stack.push(h);
	Node temp;
	int numNodes =0;
	Edge[] outEdges;
	HashMap visited = new HashMap();
	while (!stack.isEmpty()){
	    temp = (Node) stack.pop();
	    if (!visited.containsKey(temp)){
		numNodes++;
		visited.put(temp,null);
		if (!temp.equals(h.getOther())){
		    outEdges = getOutEdges(temp);
		    for(int i=0;i<outEdges.length;i++){
			stack.push(outEdges[i].getSink());
		    }
		}
	    }
	}
	//System.out.println("num nodes "+numNodes);
	return numNodes;
    }

    /**
     * Finds the closest common ancestor of nodes a and b in the tree g.
     * @param a A node to find an ancestor of
     * @param b A node to find an ancestor of
     * param g A tree containing nodes a and b.
     * @return The closest common ancestor of a and b or 
     * <code>null</code> on error.
     */
    private Node findClCmnAncst(Node a, Node b, Tree g)
    {    
	    Vector visited = new Vector();
	    //while either aNode or bNode is not the root
	    while((a != null)||(b != null)){
		if (a != null){
		    if (visited.contains(a)){
			return a;
		    }else{
			visited.addElement(a);
			//set aNode equal to its parent in the tree
			a = g.getParentOf(a);
		    }
		}
		if (b != null){
		    if (visited.contains(b)){
			return b;
		    }else{
			visited.addElement(b);
			//set bNode equal to its parent in the postdom graph
			b = g.getParentOf(b);
		    }
		}
	    }
	    //if reach here, we did not find a common ancestor
	    //error, should always have the root as a common ancestor
	    return null;
	
    }

    /**
     * Calculates the header, exit, and nodes of the minimum hammock
     * containing decision node m.  If m is not a decision 
     * node, then <code>null</code> is returned.  It creates the placeholder
     * header and exit nodes, but it does not change any edges.
     * @param m A decision node.
     * @return A <code>Vector</code> of nodes, where the first element is the 
     * place holder hammock header and the remaining elements are the non 
     * place holder nodes in the hammock.
     */
    private Vector calculateMinHammock(Node originalDecisionNode)
    {
	//when comparing with hammock documentation
	//H = hammock
	//T = numIncomingEdges
	//m = currentDecisionNode
	//original m = originalDecisionNode
	//q = currentPostdominator

	//check if originalDecisionNode is not a dec. node
	if (getParentGraph().getOutEdges(originalDecisionNode).length <= 1){
	    return null;
	}

	if (DEBUG){
	    System.err.println("Finding minimal hammock for"+originalDecisionNode.getNodeNumber());
	}

	// General Idea: nodes between the currentDecisionNode 
	// and the currentPostdominator are added to the hammock, 
	// if the resulting group of nodes is not a hammock 
	// (i.e. if it has multiple entry nodes), go to the 
	// next postdominator, if that isn't possible, get the closest 
	// higher dominator, continue until a hammock is found.
	Node currentDecisionNode = originalDecisionNode;

	Node temp;
	int value;//tmp int variable, represents a value for a hashmap
	//vector of nodes with incoming edges (into the hammock)
	Vector hammockEntryNodes = new Vector(); 

	PostDominatorTree postDomTree = (PostDominatorTree)PostDominatorTreeImpl.
	    load(getParentGraph());

	DominatorTree domTree = (DominatorTree)DominatorTreeImpl.load(getParentGraph());

	if (DEBUG){
	    System.err.println("Trees correctly loaded...");
	}

	// During each iteration of this while loop, we try 
	// a new decision node to see if it will generate a hammock 
	// containing the originalDecisionNode.
	while (true){
	    // Since all the edges leaving a hammock must 
	    // go to one node, that node, the exit node, must 
	    // postdominate all the nodes in the hammock.  
	    // Then we can narrow the possible choices of hammock 
	    // exits to postdominators of the currentDecisionNode 
	    // and the originalDecisionNode, since both 
	    // nodes will be in the hammock.  Since we want the 
	    // minimal hammock, we want to try the closest 
	    // postdominator first, and if it isn't an exit to a 
	    // hammock containing currentDecisionNode, then we
	    // will try the next one (its immediate postdominator).

	    Node currentPostdominator = 
	      findClCmnAncst(currentDecisionNode,originalDecisionNode,postDomTree);
	    if (currentPostdominator.equals(currentDecisionNode)){
	      currentPostdominator = postDomTree.getParentOf(currentPostdominator);
	    }
	    if (DEBUG){
		System.err.println("Found postdominator "+
			  currentPostdominator.getNodeNumber());
	    }

	    Vector hammock = new Vector();//The set of nodes in the hammock

	    // Do a DFS (depth first search) to determine all 
	    // the nodes that fall on a path from the currentDecisionNode 
	    // to the curreentPostdominator.  Keep track of which nodes 
	    // have edges entering from outside in numIncomingEdges,
	    // such that it is easy to determine which nodes are the 
	    // entry nodes. We will have found a hammock when there is 
	    // only one entry node. For efficiency, when we first visit 
	    // a node, we add it to numIncomingEdges with the number of 
	    // incoming edges it has.  Then every time we visit it
	    // during the DFS (including the first time), we decrement 
	    // that number.  If we don't traverse along an incoming edge, 
	    // then that edge must come from outside, and the number 
	    // associated with the node will not be zero.

	    //NOTE: for efficiency in the future might want to use the
	    //node number and an array as opposed to a hashmap and
	    //a node object
	    //numIncomingEdges is (re)init to empty

	    //keeps track of the edges entering the hammock
	    HashMap numIncomingEdges = new HashMap();
	    numIncomingEdges.put(currentDecisionNode, new Integer(
		       getParentGraph().getInEdges(currentDecisionNode).length));
	    numIncomingEdges.put(currentPostdominator, new Integer(
                       getParentGraph().getInEdges(currentPostdominator).length));

	    Stack stack = new Stack(); //stack of nodes that need to be visited
	    stack.push(currentDecisionNode);

	    //while there are still more postdominators that might be exits
	    while (currentPostdominator != null){

		//while the DFS for the currentPostdominator is not complete
		while (!stack.empty()){
		    temp = (Node)stack.pop();
		    //if temp is not the currentPostdominator 
		    //and temp is not in the hammock yet
		    if ((!temp.equals(currentPostdominator))&&
			(!hammock.contains(temp))){
			//add temp to the hammock
			hammock.addElement(temp);
			//if temp is the currentDecisionNode and it hasn't been
			//visited (put in the hammock) yet, don't subtract 
			//the incoming edge
			//otherwise initialize the edge count for temp, 
			//and decrement for the 1st visit
			if (!temp.equals(currentDecisionNode)){
			    value = getParentGraph().getInEdges(temp).length
				- 1;
			    numIncomingEdges.put(temp, new Integer(value));
			}
			//push the children of temp on the stack
			Edge[] outEdges = getParentGraph().getOutEdges(temp);
			for(int i=0;i<outEdges.length;i++){
			    stack.push(outEdges[i].getSink());
			}
		    } else {
			//the edge count has already been init
			//decrement the edge count
			value = ((Integer)numIncomingEdges.get(temp)).intValue()
			    - 1;
			numIncomingEdges.put(temp, new Integer(value));
		    }
		    //endwhile
		}

		// The DFS for the currentPostdominator is now complete.
		// check if the set of nodes is a valid hammock, by
		// checking how many entry nodes there are.
		hammockEntryNodes.clear();
		for(Enumeration e = hammock.elements (); e.hasMoreElements ();)
		{
			Node n = (Node) e.nextElement ();
		    if (((Integer) numIncomingEdges.get (n)).intValue () > 0)
		    {
			hammockEntryNodes.addElement (n);
		    }
		}
		if (hammockEntryNodes.size()<=1){
		    //found a valid hammock
		    if (DEBUG){
			System.err.println("Valid hammock found...");
		    }
		    HammockNode headerNode;//the place holder header node for this hammock
		    if (hammockEntryNodes.size() == 1){
			headerNode=HammockNodeImpl.setUpPair((Node)hammockEntryNodes.elementAt(0),currentPostdominator, -1);
		    } else {
			// If there aren't any entry nodes, the
			// graph entry must be the entry node.
			headerNode=HammockNodeImpl.setUpPair ((jaba.graph.Node) getParentGraph ().getEntryNodes () [0],currentPostdominator, -1);
		    }

		    // Put the place holder hammock header first in the array.
		    hammock.insertElementAt(headerNode, 0);
		    return hammock;
		}
		// A valid hammock wasn't found, try the next 
		// postdominator as an exit. Since it would be 
		// redundant to add all the same nodes we just added,
		// just continue the DFS from the currentPostdominator, 
		// but set currentDecisionNode equal to it so that its 
		// edge count doesn't get reinitialized.
		currentDecisionNode = currentPostdominator;
		stack.push(currentDecisionNode);
		// The new postdominator, will be the parent of 
		// the current postdominator. The parent of the 
		// graph exit, is null.  Set up the edge count 
		// for the new postdominator.
		currentPostdominator=postDomTree.getParentOf(currentPostdominator);
		if (currentPostdominator != null)
		    numIncomingEdges.put(currentPostdominator, new Integer(
			 getParentGraph().getInEdges(currentPostdominator).length));
	    }

	    // No hammock was found, try a new currentDecisionNode.

	    // If the hammock contains the entry of the graph, 
	    // then there is no other currentDecisionNode that 
	    // will yield a hammock, an error has occurred
	    // because it the worse case the entry is a header 
	    // and the exit is an exit.
	    if (hammock.contains(getParentGraph().getEntryNodes()[0])){
		// An error has occurred
		return null;
	    }

	    // Calculate the new currentDecisionNode by finding the closest
	    // common dominator of all the entry nodes.
	    // the entry nodes are the nodes with entering arcs from
	    // outside the hammock
	    // note: entry nodes must contain at least two nodes,
	    // otherwise we would have been done above
	    currentDecisionNode = (Node)hammockEntryNodes.elementAt(0);
		Enumeration e = hammockEntryNodes.elements ();
		e.nextElement (); // flush the 0 element so we can start on the second
	    do
	    {
		currentDecisionNode = 
		    findClCmnAncst (currentDecisionNode, (Node) e.nextElement (), domTree);
	    } while (e.hasMoreElements ());
	}
    }

    /**
     * Returns the hammock node parent of n, if it exits otherwise returns null.
     * @param n The node to find the parent hammock node of.
     * @return The hammock node parent of n, or null.
     */
    private HammockNode[] getPrevHammockNodes(Node n)
    {
	Edge[] inEdges = getInEdges(n);
	Vector hammockNodes = new Vector();
	for(int i=0;i<inEdges.length;i++){
	    if(inEdges[i].getSource() instanceof HammockNode){
		hammockNodes.addElement(inEdges[i].getSource());
	    }
	}
	HammockNode[] prevNodes = new HammockNode[hammockNodes.size()];
	hammockNodes.copyInto(prevNodes);
	return prevNodes;
    }

    /**
     * If the HammockGraph for g has been constructed, 
     * returns a reference to the HammockGraph, otherwise
     * it constructs and returns the HammockGraph for the 
     * Graph g, and adds the HammockGraph to the GraphAttributes 
     * of g.  Note that g should be a single entry single 
     * exit graph.
     * @param g A single entry single exit graph to find the HammockGraph of.
     * @return The HammockGraph of g.
     * @throws IllegalArgumentException if g is not single entry, single exit.
     */
    public static GraphAttribute load(Graph g)
	throws IllegalArgumentException
    {
	// Check that g is single entry single exit, if it isn't, throw an exception.
	if (g == null)
	    return null;
	if ((g.getEntryNodes().length > 1)||
	    (g.getExitNodes().length > 1)) {
	    throw(new IllegalArgumentException("\njaba.graph.hammock.HammockGraph.load(Graph) expects a single entry single exit graph"));
	}

	// Check if HammockGraph already exists for g.
	// Do this by looking through
	// g.attributes (the array of attributes) for a HammockGraph.
	GraphAttribute[] gAttributes = g.getAttributes();//g's attributes
	for(int i=0;i<gAttributes.length;i++){
	    //if exists
	    if (gAttributes[i] instanceof HammockGraph){
		//return reference
		if (DEBUG){
		    System.err.println("Existing HammockGraph was found.");
		}
		return (HammockGraph)gAttributes[i];
	    }
	}
	
	if (DEBUG){
	 System.err.println("Existing HammockGraph not found, constructing new one.");
	}

	// The HammockGraph for g hasn't been constructed yet.
	// So we need to construct the HammockGraph,
	// add the HammockGraph to g's attributes,
	// and return the HammockGraph.

	//construct
	//this is the less efficient non hammocks on demand version
	HammockGraphImpl hammockGraph = new HammockGraphImpl (g);

	// Here we are essentially copying g, then later we
	// will modify the edges and add hammock nodes
	// to get the final hammock graph.
	Edge [] gEdges = g.getEdges();//g's edges
	for (int i=0;i<gEdges.length;i++){
	    //the node references are shared among graphs, the
	    //edges are not
	    hammockGraph.addEdge(new EdgeImpl ((jaba.graph.Node) gEdges[i].getSource(), 
					  (jaba.graph.Node) gEdges[i].getSink(), 
					  gEdges[i].getLabel()));
	}

	// Next we need to find where the hammocks are, then add
	// place holder HammockNodes, and move the edges 
	// accordingly.

	// When finding the hammocks we need to keep track of
	// their headers to initialize to global variable, which
	// keeps track of that data.
	Vector headerNodes = new Vector();//a vector of header nodes

	// To find the hammocks we calculate the minimum hammock
	// for every decision node.  Then we see where it fits,
	// for example if one node is the actual header for two
	// hammocks, we have to decide which place holder header
	// to put first.  To find where a place holder node fits
	// we have a successor node (scdNode) that represents
	// the node that would immediately succeed or follow
	// the place holder node.  Initially the scdNode is
	// the actual header or exit, but if that node already has
	// immediately previous hammock node(s), then we have to
	// consider if the new place holder node should go above
	// the old place holder nodes (i.e. should scdNode be
	// changed to one of the old place holder nodes).  Once we
	// figure out the correct scdNode, we move the edges.
	Node [] gNodes = g.getNodesInDepthFirstOrder(); // g's nodes

	if (DEBUG){
	    System.out.println("the nodes in G are...");
	    for(int i=0;i<gNodes.length;i++){
		System.out.print(gNodes[i].getNodeNumber()+" ");
	    }
	    System.out.println("");
	}


	Node tempNode;//a temp node
	for(int i=0;i<gNodes.length;i++){
	    //if the node is a decision node
	    if (DEBUG)
		System.out.println("Examinging node "+
				   gNodes[i].getNodeNumber());
	    if (hammockGraph.getOutEdges(gNodes[i]).length > 1){
		//calculate the nodes min hammock
		if (DEBUG){
		    System.err.println("Attempting to find hammock for node "+
				       gNodes[i].getNodeNumber());
		}
		//the nodes in the current hammock
		Vector nodesInHammock = hammockGraph.calculateMinHammock ((jaba.graph.Node) gNodes[i]);
		if (nodesInHammock == null){
		    //error, no hammock was found
		    if (DEBUG){
			System.err.println("Unable to find hammock for node "+
					gNodes[i].getNodeNumber());
		    }			    
		    return null;
		}
		//a place holder header
		HammockNode headerNode = (HammockNode)nodesInHammock.elementAt(0);

		HammockNode[] prevNodes=hammockGraph.getPrevHammockNodes(headerNode.
				        getActual());//immed. prev. HammockNodes
		//The node that might be the successor of the place holder node
		Node scdNode=headerNode.getActual();
		// Check if scdNode needs to be moved to get a correct
		// HammockGraph, if it does move it up one and repeat.

		//while there are prev header nodes
		while ((prevNodes != null)&&(prevNodes.length!=0)){
		    // When we look at the previous hammock nodes there are 
		    // three things we might want to do
		    // a) Stay - put the place holder before the current scdNode
		    // b) MoveUp - Try the prev. hammock header for a valid scdNode
		    // c) GotoNext - This hammock is already found, go to the next
		    //               decision node.
		    boolean stay = false;
		    boolean moveUp = false;
		    boolean gotoNext = false;
		    // A node can only have one immed. prev. header, so we must keep
		    // track of that.
		    boolean seenHeader = false;

		    for(int j=0;j<prevNodes.length;j++){
			// If the prev node is an exit, not a header, we don't
			// need to worry about this header belonging before it.
			// So, we have found where scdNode should go, so stay.

			// T.Apiwattanapong 6/3/2002
			// if prev node is a hammock exit, we may have to
			// check further that its hammock are inside or
			// outside the current hammock. If outside, then
			// Stay. If inside, nothing is gained, check the others
			
			if(prevNodes[j].getType()!=HammockNode.HAMMOCK_HEADER){
			    // check whether the hammock P of this "hammock exit"
			    // is outside the current hammock by checking if
			    // the "hammock header" of P is NOT in the current hammock
			    if ( !nodesInHammock.contains( prevNodes[j].getOther().
							   getActual() ) ){
				stay = true;
			    }
			    continue;
			}

			if (seenHeader){
			    //we have already seen a header this is an error
			    //the graph is malformed
			    return null;
			}
			seenHeader = true;
			//the stuff after here will only be executed once

			// If any of the prev hammock nodes have the 
			// same exit as scdNode, then the two hammocks 
			// are the same, and we want to
			// move onto the next decison node.
			if (prevNodes[j].getOther().getActual().
			    equals(headerNode.getOther().getActual())){
			    //this hammock already exists, continue with next node
			    gotoNext = true;
			    break;
			}

			// tempNode is the one of the new exit or the 
			// old exit, which postdominates the other.
			// If neither postdominates the other, than 
			// tempNode is some other node in the
			// graph.
			tempNode = hammockGraph.
			    findClCmnAncst(prevNodes[j].getOther().getActual(),
					   headerNode.getOther().getActual(),
					   (Tree)PostDominatorTreeImpl.
					   load(hammockGraph.getParentGraph()));

			//if current exit postdoms prev exit
			if (tempNode.equals(headerNode.getOther().getActual())){
			    // We have not found the right scdNode, try the prev node.
			    //then scdNode = prev node and recalc prev node
			    scdNode = prevNodes[j];
			    //prevNodes = hammockGraph.getPrevHammockNodes(scdNode);
			    moveUp = true;
			} else{
			    if (tempNode.equals(prevNodes[j].getOther().getActual())){
				// We have found the correct scdNode, move the edges.
				stay = true;
			    } else{
				//error
				// malformed graph, one of the nodes should dominate the other.
				return null;
			    }
			}


		    }
		    //if gotonext, scdnode=null, break
		    if (gotoNext){
			scdNode = null;
			break;
		    }
		    //if both stay and move up, then error
		    if (stay && moveUp){
			//can't both stay and move up, error
			return null;
		    }			
		    //if stay, break
		    if (stay){
			//we have found the correct scdNode
			break;
		    }
		    //if moveup reassign scdnode and prevnodes
		    if (moveUp){
			//scdNode has already been assigned to the new scdNode
			prevNodes = hammockGraph.getPrevHammockNodes(scdNode);
		    }

		}
		//if scd not null (if scdNode is null we want 
		//to go to the next decision node)
		if (scdNode != null){
		    // We found the right scdNode, now we want to 
		    // add the header place holder before scdNode 
		    // (i.e. move the edges coming into scdNode 
		    // from the outside to go into the place holder 
		    // node, and set the place holder node to go
		    // to scdNode).
		    if (DEBUG){
			if (scdNode instanceof HammockNode)
			    System.err.println("Found where place holder header node should go, before "+scdNode.getNodeNumber());
			else System.err.println("Found where place holder header node should go, before "+scdNode.getNodeNumber());
		    }
		    Edge[] inEdges = hammockGraph.getInEdges(scdNode);
		    //for each incoming edge to scdNode
		    for(int j=0;j<inEdges.length;j++){
			// If source is not in hammock, then we want to move
			// the edge.
			// The source is not in the hammock if:
			// a) the source is a hammock header
			// b) the source is a hammock exit, whose actual 
			//    header is not in the hammock
			// c) the source is not a hammockNode and not in the hammock
			if (((inEdges[j].getSource() instanceof HammockNode)&&
			     ((((HammockNode)inEdges[j].getSource()).getType() ==
			       HammockNode.HAMMOCK_HEADER)||
			      ((((HammockNode)inEdges[j].getSource()).getType() ==
			       HammockNode.HAMMOCK_EXIT) &&
			       (!nodesInHammock.contains(((HammockNode)inEdges[j].
							  getSource()).getOther().
							 getActual())))))||
			    ((!(inEdges[j].getSource() instanceof HammockNode))&&
			     (!(nodesInHammock.contains(inEdges[j].getSource()))))){
			    // Move an edge by changing the sink from scdNode to header.
			    hammockGraph.addEdge(new EdgeImpl ((jaba.graph.Node) inEdges[j].getSource(),
							  headerNode,
							  inEdges[j].getLabel()));
			    hammockGraph.removeEdge((jaba.graph.Edge) inEdges[j]);
			}
		    }
		    //add edge from header to scdNode
		    hammockGraph.addEdge(new EdgeImpl (headerNode,scdNode));
		    //add header to the vector of header nodes
		    headerNodes.addElement(headerNode);
		}else continue; // scdNode is null, and we want to go to the next decision node.

		//do a similar thing for the exit node
		// Check if scdNode needs to be moved to get a correct
		// HammockGraph, if it does move it up one and repeat.
		prevNodes = hammockGraph.getPrevHammockNodes(headerNode.getOther().
							     getActual());
		scdNode=headerNode.getOther().getActual();
		if (DEBUG){
		    System.err.println("Actual exit node is " + 
				       scdNode.getNodeNumber());
		}
		//while there are prev hammock exit nodes
		here: while ((prevNodes != null)&&(prevNodes.length!=0)){
		    //must handle multiple prev nodes here
		    int j;
		    for(j=0; j<prevNodes.length;j++){
			if (DEBUG){
			    System.err.println("Found prev exit node " + 
				      prevNodes[j].getNodeNumber());
			}
			// For each prev hammock node there are several cases:
			// a) The node is a header node.  It is only possible to move
			//    above the header if the current header doms the 
			//    prev header
			// b) The new/current header equals the old/prev header.
			//    This isn't possible because it would have been
			//    caught above, and we would have moved on to the
			//    next decision node.
			// c) The new/current header dominates the old/prev header.
			//    This means the new/current exit should go after
			//    the old/prev exit, and thus scdNode should not
			//    change.  Further, this relationship or a case e
			//    type should hold for all old/prev nodes or the
			//    graph is malformed.
			// d) The old/prev header dominates the new/current header.
			//    This means scdNode should be changed to the old/prev
			//    header.  Further, this relationship should only
			//    hold for this node and all the rest should be
			//    of case e or the graph is malformed.
			// e) Neither node dominates the other, then if none
			//    of the other cases hold for any node, have 
			//    found scdNode.
			if (prevNodes[j].getType()!=HammockNode.HAMMOCK_EXIT){
			    //this is a case a
			    // tempNode is the dominant of the prev and current 
			    // headers, or another node if neither is dominant.
			    tempNode = hammockGraph.
				findClCmnAncst(prevNodes[j].getActual(),
					       headerNode.getActual(),
					       (Tree)DominatorTreeImpl.
					       load(hammockGraph.getParentGraph()));
			    

			    //T.Apiwattanapong 07/05/02
			    // If this headerNode is inside the hammock whose header is prevNodes[j],
			    // not move up, If not then move up
			    if ( !hammockGraph.isNodeInsideHammock( prevNodes[j], headerNode ) ) {
				scdNode = prevNodes[j];
				prevNodes = hammockGraph.getPrevHammockNodes(scdNode);
				continue here;
			    }else continue;
			}
			// tempNode is the dominant of the prev and current headers, or another node if neither is dominant.
			tempNode = hammockGraph.
			    findClCmnAncst(prevNodes[j].getOther().getActual(),
					   headerNode.getActual(),
					   (Tree)DominatorTreeImpl.
					   load(hammockGraph.getParentGraph()));
			// If the new/current header dominates the prev/old header, it is a case c.
			if (DEBUG){
			    if (tempNode instanceof HammockNode)
				System.err.println(tempNode.getNodeNumber()+" dominates");
			    else System.err.println(tempNode.getNodeNumber()+" dominates");
			}
			if (tempNode.equals(headerNode.getActual())){
			    //should hold for all (except ignored)
			    //might want to check
			    prevNodes = null;
			    if (DEBUG){
				System.err.println("scdNode not moved up");
			    }
			    continue here;
			} else{
			    // If the prev/old header dominates the 
			    // current/new header, it is
			    // a case d.
			    if (tempNode.equals(prevNodes[j].getOther().getActual())){
				//prev header doms this header
				//all others should be ignored (might want to check)
				scdNode = prevNodes[j];
				prevNodes = hammockGraph.getPrevHammockNodes(scdNode);
				if (DEBUG){
				    System.err.println("scdNode moved up ");
				    if ((prevNodes != null)&&(prevNodes.length > 0))
					System.err.println("prev nodes[0] = "+prevNodes[0]);
				}
				continue here;
			    } else{
				// Case e, neither dominates the other, ignore.
				if (DEBUG){
				    System.err.println("neither doms, ignore");
				    System.out.println("neither doms, ignore");
				}
			    }
			}
		    
		    }
		    if ((prevNodes != null) &&
			(j >= prevNodes.length)){
			// Every node is case e, we have found the correct scdNode.
			prevNodes = null;
			if (DEBUG){
			    System.err.println("all prevNodes are ignored...");
			    System.out.println("all prevNodes are ignored...");
			}
		    }
		}
		// We found the correct scdNode, so now add the exit place holder node before scdNode.
		if (DEBUG){
		    if (scdNode instanceof HammockNode)
			System.err.println("Found where place holder exit node should go, before "+scdNode.getNodeNumber());
		    else System.err.println("Found where place holder exit node should go, before "+scdNode.getNodeNumber());
		}
		Edge [] inEdges = hammockGraph.getInEdges(scdNode);
		//for each incoming edge to scdNode
		for(int j=0;j<inEdges.length;j++){
		    // If source is in hammock, then we want to move
		    // the edge.
		    // The source is in the hammock if:
		    // a) the source is a hammock exit, whose actual header is in
		    //    the hammock
		    // b) the source is not a hammockNode and in the hammock
		    if (((inEdges[j].getSource() instanceof HammockNode)&&
			 (((HammockNode)inEdges[j].getSource()).getType() ==
			  HammockNode.HAMMOCK_EXIT)&&
			 (nodesInHammock.contains(((HammockNode)inEdges[j].
					  getSource()).getOther().getActual())))||
			((!(inEdges[j].getSource() instanceof HammockNode))&&
			 (nodesInHammock.contains(inEdges[j].getSource())))){
			//change sink from scdNode to exit node
			hammockGraph.addEdge(new EdgeImpl ((jaba.graph.Node) inEdges[j].getSource(),
						      headerNode.getOther(), 
						      inEdges[j].getLabel()));
			hammockGraph.removeEdge ((jaba.graph.Edge) inEdges[j]);
		    }
		}
		//add edge from exit to scdNode
		hammockGraph.addEdge(new EdgeImpl (headerNode.getOther(),scdNode));
	    }
	}
	//convert headers to array and set headerNodes equal to it
	hammockGraph.headers = new HammockNode[headerNodes.size()];
	headerNodes.copyInto(hammockGraph.headers);

	//add to attributes
	g.addAttribute(hammockGraph);

	//return
	return hammockGraph;
    }


    /**
     * Writes this graph to a file in the format that is employed by dotty, 
     * a graph drawing software from ATT research. 
     * @param filename The name of the file into which the graph has to be 
     *	    written.
     * @param spec ??
     */
    //this should be redone in a more robust fashion
    public void createDottyFile(String filename, DottyOutputSpec spec) 
	throws IOException
    {
      Writer out;
      StatementNode writenode = null;
      Hashtable subgraph = new Hashtable();
      out = new FileWriter(filename);
      // The statements below write some configuration information into the
      // output file like the Font size, Font Name, Font color etc.
      out.write("digraph \"g\" { \n graph [ \n");
      //"size" and "bb" elements have been taken out so that dotty
      // can properly process the file 
      out.write("fontsize = \"14\" \n fontname = \"Times-Roman\" \n");
      out.write("fontcolor = \"black\" \n");
      out.write("lp = \"349,0\" \n");
      out.write("color = \"black\" \n ] \n");
      out.write("node [ \n fontsize = \"14\" \n" +  
		"fontname = \"Times-Roman\" \n");
      out.write("fontcolor = \"black\" \n shape = \"ellipse\" \n");
      out.write("color = \"black\" \n style = \"filled\" \n ] \n");
      // Get all the nodes in the Graph in depth first order 
      Node[] nodes = getNodesInDepthFirstOrder();
      for(int i = 0; i < nodes.length; i++) {
	  
	  if (nodes[i] instanceof StatementNode)
	      writenode = (StatementNode) nodes[i];

	  // If the "subgraph" Hashtable is empty or it doesnt contain a key 
	  // corresponding to the Containing hammock, Create a vector, add this node
	  // to the vector and add the vector to the hashtable
	  // using the containing hammock as the key

	  //System.err.println("adding node "+nodes[i]+" TO: ");

	  Node hammock = null;
	  hammock = getMinHammock ((jaba.graph.Node) nodes[i]);
	  if (hammock == null)
	      hammock = (jaba.graph.Node) getExitNodes()[0];
	  if ((subgraph.isEmpty())||(!subgraph.containsKey(hammock))){
	      Vector V = new Vector();
	      V.addElement(nodes[i]);   
	      subgraph.put(hammock, V);
	      //System.err.println(hammock);
	  }else{
	      ((Vector) subgraph.get(hammock)).addElement(nodes[i]);
	      //System.err.println(hammock);
	  }
	  if ((nodes[i] instanceof HammockNode)&&
	      (((HammockNode)nodes[i]).getType() == HammockNode.HAMMOCK_HEADER)){
	      if (nodes[i].equals(getEntryNodes()[0])){
		  hammock = (jaba.graph.Node) getExitNodes()[0];
	      }else{
		  hammock = (jaba.graph.Node) getInEdges(nodes[i])[0].getSource();
		  while ((hammock instanceof HammockNode)&&
			 (((HammockNode)hammock).getType()==HammockNode.HAMMOCK_EXIT)){
		      hammock = ((HammockNode)hammock).getOther();
		      if (hammock.equals(getEntryNodes()[0])){
			  hammock = (jaba.graph.Node) getExitNodes()[0];
		      }else{
			  hammock = (jaba.graph.Node) getInEdges(hammock)[0].getSource();
		      }
		  }
		  hammock = getMinHammock(hammock);
	      }
	      
	      if (hammock == null)
		  hammock = (jaba.graph.Node) getExitNodes()[0];
	      //System.err.println("adding node "+nodes[i]+" TO: ");
	      if ((subgraph.isEmpty())||(!subgraph.containsKey(hammock))){
		  Vector V = new Vector();
		  V.addElement(nodes[i]);   
		  subgraph.put(hammock, V);
		  //System.err.println(hammock);
	      }else{
		  ((Vector) subgraph.get(hammock)).addElement(nodes[i]);
		  //System.err.println(hammock);
	      }
	  }
	      
	  
	  String nodetypstr;
	  String shape;
	  String color;
	  int getType = -1;
	  if (nodes[i] instanceof StatementNode)
	      getType = ((StatementNode)nodes[i]).getType();
	  if (nodes[i] instanceof HammockNode)
	      //might have conflict btw the ints that rep. the types... this should be more robust
	      getType = ((HammockNode)nodes[i]).getType();

	  // The statements below writes the color and the shape information for 
	  // each type of node
	  switch (getType){
	  case StatementNode.ENTRY_NODE:
	      color = "green";
	      shape = "ellipse";
	      nodetypstr = "EN";
	      break;
	  case StatementNode.EXIT_NODE :
	      color = "red";
	      shape = "ellipse";
	      nodetypstr = "EX";
	      break;
	  case StatementNode.PREDICATE_NODE :
	      color = "yellow";
	      shape = "diamond";
	      nodetypstr = "PR";
	      break;
	  case StatementNode.RETURN_NODE :
	      color = "lightblue";
	      shape = "ellipse";
	      nodetypstr = "RT";
	      break;
	  case StatementNode.RETURN_STATEMENT_NODE :
	      color = "lightblue";
	      shape = "ellipse";
	      nodetypstr = "RS";
	      break;  
	  case StatementNode.NORMAL_FINALLY_CALL_NODE :
	      color = "orange";
	      shape = "ellipse";
	      nodetypstr = "NFC";
	      break;
	  case StatementNode.STATIC_METHOD_CALL_NODE :
	      color = "orange";
	      shape = "ellipse";
	      nodetypstr = "SC";
	      break;
	  case StatementNode.VIRTUAL_METHOD_CALL_NODE :
	      color = "orange";
	      shape = "ellipse";
	      nodetypstr = "VC";
	      break;
	  case StatementNode.EXCEPTIONAL_EXIT_NODE :
	      color = "red";
	      shape = "ellipse";
	      nodetypstr = "EE";
	      break;
	  case StatementNode.CLASS_INSTANTIATION_NODE :
	      color = "yellow";
	      shape = "ellipse";
	      nodetypstr = "CI";
	      break;
	  case StatementNode.ARRAY_INSTANTIATION_NODE :
	      color = "yellow";
	      shape = "ellipse";
	      nodetypstr = "AI";
	      break;
	  case StatementNode.EXCEPTIONAL_FINALLY_CALL_NODE :
	      color = "orange";
	      shape = "ellipse";
	      nodetypstr = "EFC";
	      break;
	  case StatementNode.THROW_NODE :
	      color = "yellow";
	      shape = "ellipse";
	      nodetypstr = "TH";
	      break;
	  case StatementNode.CATCH_NODE :
	      color = "yellow";
	      shape = "ellipse";
	      nodetypstr = "CA";
	      break;
	  case StatementNode.SUPER_EXIT_NODE :
	      color = "red";
	      shape = "ellipse";
	      nodetypstr = "SE";
	      break;
	  case StatementNode.STATEMENT_NODE :
	      color = "yellow";
	      shape = "ellipse";
	      nodetypstr = "ST";
	      break;
	  case StatementNode.HALT_NODE :
	      color = "red";
	      shape = "ellipse";
	      nodetypstr = "HL";
	      break;
	  case StatementNode.TEMP_DEFINITION_NODE :
	      color = "yellow";
	      shape = "ellipse";
	      nodetypstr = "TD";
	      break;
	  case HammockNode.HAMMOCK_HEADER :
	      color = "purple";
	      shape = "box";
	      nodetypstr = "HH";
	      break;
	  case HammockNode.HAMMOCK_EXIT :
	      color = "purple";
	      shape = "box";
	      nodetypstr = "HX";
	      break;
	  default:
	      color = "yellow";
	      shape = "ellipse";
	      nodetypstr = "UN";
	  }//end switch: figures out what type of node it is, and assigns corresponding attributes
	  // The statements below write out the node information to the output file.
	  // The information to be written out includes the shape and the color of 
	  // the node. The text to be included in the node is specified in the 
	  // "spec" parameter
	  int getNodeNumber = nodes[i].getNodeNumber();	  

	  out.write("\"" + getNodeNumber + " " + 
		    getType + "\" [\n");
	  if(spec.getLabelSpec() == DottyOutputSpec.DEFAULT ||
	     spec.labelContains("NODE_NUMBER"))
		    out.write("label = \"Node Number:" +
			      getNodeNumber + "\\n ");
	  if (nodes[i] instanceof StatementNode){
	      if(spec.getLabelSpec() == DottyOutputSpec.DEFAULT || 
		 spec.labelContains("STATEMENT_NUMBER"))
		  out.write("Statement No: " + 
			    ((StatementNode)nodes[i]).getSourceLineNumber() + "\\n ");
	      if(spec.getLabelSpec() == DottyOutputSpec.DEFAULT || 
		 spec.labelContains("BYTECODE_OFFSET"))
		  out.write("Byte Code Offset: " + 
			    writenode.getByteCodeOffset() + "\\n ");
	  }
	  if (nodes[i] instanceof HammockNode){
	      out.write("Other: " + 
			((HammockNode)nodes[i]).getOther().getNodeNumber() + "\\n ");
	  }

	  if(spec.getLabelSpec() == DottyOutputSpec.DEFAULT || 
	     spec.labelContains("NODE_TYPE"))
	      out.write("Node Type :" + 
			nodetypstr);
	  out.write(" \"\nshape = \"" + shape + "\" \n");
	  out.write("color = \"" + color + "\" \n ]\n");
      }//end for loop that loops through all nodes

      int count =0;
      int getNodeNumber, getType;
      Node indexNode, tempNode;
      Vector nextSubgraph;
      Stack stack = new Stack();
      Stack tempStack = new Stack();
      tempStack.push(getExitNodes()[0]);
      stack.push(tempStack);

      while (!stack.empty()){
	  tempStack = (Stack) stack.pop();
	  while (!tempStack.empty()){
	      indexNode = (Node) tempStack.pop();
	      stack.push(tempStack);
	      tempStack = new Stack();
	      nextSubgraph = (Vector) subgraph.get(indexNode);
	      out.write("subgraph" + " " + "cluster_"+ count + "{ \n");
	      out.write("label = Subgraph" + count + ";\n");   
	      count++;
	      for(Enumeration e = nextSubgraph.elements (); e.hasMoreElements ();)
	      {
		  tempNode = (Node) e.nextElement ();
		  if ((tempNode instanceof HammockNode)&&
		      (((HammockNode)tempNode).getType()==HammockNode.HAMMOCK_HEADER)&&
		      (!tempNode.equals(indexNode)))
		      tempStack.push(tempNode);
		  else{
		      //print stuff
		      getNodeNumber = tempNode.getNodeNumber();
		      getType = -1;
			  
		      if (tempNode instanceof StatementNode){
			  getType = ((StatementNode)tempNode).getType();
		      }
		      if (tempNode instanceof HammockNode){
			  getType = ((HammockNode)tempNode).getType();
		      }
		      //System.err.print(getNodeNumber+" ");
		      out.write("\"" + getNodeNumber + " " + getType + 
				"\";\n");
		  }
	      }
	  }
	  if (!stack.empty())
	      out.write("}\n");
      }

      // The statements below write out the edge information into the output file. 

      // loops through each of the edges constructed for this graph.
      //  for each edge, constructs the souce and sink as statementNodes
      //  so that the nodeNumber and type can be obtained
      Edge[] edges = getEdges();
      for(int i = 0; i < edges.length; i++){
	  Node Source = (jaba.graph.Node) edges[i].getSource();
	  Node Sink = (jaba.graph.Node) edges[i].getSink();

	  int sinkGetNodeNumber = Sink.getNodeNumber();;
	  int sinkGetType = -1;
	  Method sinkContainingMethod = null;
	  if (Sink instanceof StatementNode){
	      sinkGetType = ((StatementNode)Sink).getType();
	      sinkContainingMethod = (jaba.sym.Method) ((StatementNode)Sink).getContainingMethod();
	  }
	  if (Sink instanceof HammockNode){
	      sinkGetType = ((HammockNode)Sink).getType();
	      sinkContainingMethod = (jaba.sym.Method) ((StatementNode)((HammockNode)Sink).getActual()).getContainingMethod();
	  }
	  
	  int sourceGetNodeNumber = Source.getNodeNumber();
	  int sourceGetType = -1;
	  Method sourceContainingMethod = null;
	  if (Source instanceof StatementNode){
	      sourceGetType = ((StatementNode)Source).getType();
	      sourceContainingMethod = (jaba.sym.Method) ((StatementNode)Source).getContainingMethod();
	  }
	  if (Source instanceof HammockNode){
	      sourceGetType = ((HammockNode)Source).getType();
	      sourceContainingMethod = (jaba.sym.Method) ((StatementNode)((HammockNode)Source).getActual()).getContainingMethod();
	  }

	  out.write("\"" + sourceGetNodeNumber + " " + 
		    sourceGetType + "\"-> \"" +
		    sinkGetNodeNumber + " " + sinkGetType + "\" ");
	  if(edges[i].getLabel() != null){
	      out.write("[\n label = \"Edge Label:" +
			edges[i].getLabel() +
			"\" \n");
	      if(sourceContainingMethod != sinkContainingMethod)
		  out.write("style = dotted \n ]\n");
	      else
		  out.write("]\n");
	  }
	  else if(sourceContainingMethod != 
		  sinkContainingMethod)
	      out.write("[ \n style = dotted \n ]\n");
	  else 
	      out.write("\n");
      }
      out.write("} \n");
      out.close();
      return;
    }

	/** returns an XML representation of this object */
	public String toString ()
	{
		return "<hammockgraph>" + getString () + "</hammockgraph>";
	}

	/** returns the reduced representation of this object */
	public String toStringReduced ()
	{
		return "<hammockgraph>" + getStringReduced () + "</hammockgraph>";
	}

	/** returns the detailed representation of this object */
	public String toStringDetailed ()
	{
		return "<hammockgraph>" + getStringDetailed () + "</hammockgraph>";
	}
}
