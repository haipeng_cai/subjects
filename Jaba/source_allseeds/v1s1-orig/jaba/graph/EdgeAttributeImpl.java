/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Attribute;

/**
 * This is the top-level interface representing all types of edge attributes.
 *
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to implement EdgeAttribute
 */
public abstract class EdgeAttributeImpl implements EdgeAttribute, Attribute, java.io.Serializable
{
  /** ??? */
  public abstract String toString();
}
