/* Copyright (c) 1999, The Ohio State University */

package jaba.graph;

import jaba.sym.Class;

import java.util.Vector;

/**
 * Attribute stored with an inedge to a FINALLY_START_NODE or an outedge
 * from a FINALLY_END_NODE in the CFG and the ICFG. Those nodes and
 * therefore, this attribute, are used only in graphs in which finally
 * blocks have been inlined into their containing method's representation.
 *
 * The FinallyContextAttribute stores the list of exception types
 * (<code>jaba.sym.Class</code>) that cause the corresponding edge to be
 * traversed (and the associated finally block to be executed) in an
 * exception context. If there are no exception types stored in the
 * attribute, the corresponding edge is traversed (and the associated
 * finally block is executed) in a normal context. 
 *
 * @author S. Sinha -- <i>Created, Dec. 12, 2002</i>.
 * @author Jay Lofstead 2003/05/29 changed toString to generate XML
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 code merge with main jaba branch
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface FinallyContextAttribute extends EdgeAttribute
{
    /**
     * Adds an exception type to the set of exception types that cause the
     * corresponding edge to be traversed.
     * @param excpType  Exception type that cause the corresponding edge
     *                  to be traversed.
     * @throws IllegalArgumentException if <code>excpType</code> is
     *                                     <code>null</code>.     */
    public void addExceptionType( Class excpType ) throws IllegalArgumentException;

    /**
     * Returns the exception types that cause the
     * corresponding edge to be traversed.
     * @return Array of exception types that cause the corresponding edge
     *         to be traversed; <code>null</code> if there are no exception
     *         types.
     */
	public Vector getExceptionTypesVector ();

    /**
     * Returns the exception types that cause the
     * corresponding edge to be traversed.
     * @return Array of exception types that cause the corresponding edge
     *         to be traversed; <code>null</code> if there are no exception
     *         types.
     */
	public Class [] getExceptionTypes ();

    /**
     * Returns a string representation of a finally-context attribute.
     * @return String representation of finally-context attribute.
     */
    public String toString();
}
