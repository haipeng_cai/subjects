package jaba.classfile;

import java.io.IOException;
import java.io.DataInput;

/**
 * Module : ConstantPool.java
 * 
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the ConstantPool class.
 *      The class is responsible for representing the constant pool from
 *      a java .class file.  Current implementation uses Version 1.1.3 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 12/4/96.
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface ConstantPool extends Cloneable, java.io.Serializable
{
    /** ??? */
    public short getConstantPoolCount();

    /** ??? */
    public ConstantPoolInfo getConstantPoolEntry(int index) throws IndexOutOfBoundsException;

    /** Load the constant pool from a stream for the java class file.  Function assumes stream is correctly positioned to read in pool. */
    public boolean LoadConstantPool(DataInput instream) throws IOException;

    /**
     * Utility function - Verify an index maps into the constant pool.
     * Note that the zero index is reserved by the JVM Class file spec incorrectly says index is valid if < (m_ConstantPoolCount - 1)
     */
    public boolean IsThisAConstantPoolIndex(int index);

    /** Utility function - Verify an index refers to a certain constant type */
    public boolean VerifyIndexIsConstantType(int index, byte constantType);

    /** Utility function - Obtain the class name for an index to a field constant */
    public String getClassStringForFieldConstant(int index);

    /** Utility function - Obtain the field string for a field constant index */
    public String getFieldStringForFieldConstant(int index);

    /** Utility function - Obtain the descriptor string for a field constant index */
    public String getDescriptorStringForFieldConstant(int index);

    /** Utility function - Obtain the class name for an index to a methodRef constant */
    public String getClassStringForMethodConstant(int index);

    /** Utility function - Obtain the method string for a method constant index */
    public String getMethodStringForMethodConstant(int index);

    /** Utility function - Obtain the descriptor string for a method constant index */
    public String getDescriptorStringForMethodConstant(int index);

    /** Utility function - Obtain the class name for an index to an interfaceRef constant */
    public String getClassStringForInterfaceConstant(int index);

    /** Utility function - Obtain the string for an interface constant index */
    public String getMethodStringForInterfaceConstant(int index);

    /** Utility function - Obtain the descriptor string for an interface constant index */
    public String getDescriptorStringForInterfaceConstant(int index);

    /** Utility function - Obtain the name string for a name-and-type constant index */
    public String getNameForNameTypeConstant(int index);

    /** Utility function - Obtain the descriptor string for a name-and-type constant index */
    public String getDescriptorForNameTypeConstant(int index);

    /** Utility function - Obtain the class name string for an index to a CONSTANT_Class */
    public String GetStringForClassConstant(int index);

    /** Utility function - Obtain the class name string for an index to a CONSTANT_Class */
    public String GetStringForUtf8Constant(int index);

    /** Output the constant pool information by index */
    public void PrintByIndex();

    /** Output the constant pool information by group (constant type) */
    public void PrintByGroup();
} // ConstantPool
