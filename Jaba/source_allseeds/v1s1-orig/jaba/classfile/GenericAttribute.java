package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;

/**
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the GenericAttribute class.
 *      The class is responsible for representing a generic attribute from
 *      a java .class file.  Current implementation uses Version 1.1.3 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 12/19/96.
 * @author Jay Lofstead 2003/05/29 added a toString to generate XML
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 */
public class GenericAttribute extends AttributeInfoImpl implements Cloneable
{
    ////////////////
    // Attributes //
    ////////////////

    /** Attribute information */
    private byte[]   _info;

    /** Generic attribute name (copy) from constant pool */
    private String   _genericName;

    /////////////
    // Methods //
    /////////////

    // Constructor

    /** ??? */
    public GenericAttribute(int index)
    {
        _attributeNameIndex = (short)index;
    }

    ////////////////////
    // Accessor methods
    ////////////////////

    /** ??? */
    public String getName() { return (_genericName == null) ? null : new String(_genericName); }

    /** Method loads a generic attribute object from an input stream
     *  Method assumes that the attribute name index has already been read;
     *  stream is positioned to read attribute length
     *  Constant pool parameter is not used by this class
     */
    public boolean Load(DataInput instream, ConstantPool pool) throws IOException
    {
        // Verify name index for attribute
        if (pool.IsThisAConstantPoolIndex((int)_attributeNameIndex) == false)
            throw new ClassFormatError("Name index for generic attribute is invalid; value = " + _attributeNameIndex);

        // Verify that the index refers to a Utf8 constant
        if (pool.VerifyIndexIsConstantType((int)_attributeNameIndex,ConstantPoolInfo.CONSTANT_Utf8) == false)
            throw new ClassFormatError("Name index for generic attribute doesn't refer to a CONSTANT_Utf8;" +
                                       " index = " + _attributeNameIndex);

        // Obtain and store a copy of the name for the generic attribute
        ConstantPoolInfo  poolEntry = pool.getConstantPoolEntry((int)_attributeNameIndex);

        if (poolEntry == null)
        {
            System.err.println("Generic attribute index refers to invalid pool entry; index = " + _attributeNameIndex);
            return false;
        }

        _genericName = poolEntry.getString();

        // Load the attribute info length & verify
        int length = instream.readInt();

        if (length < 0)
            throw new ClassFormatError("Invalid length for generic attribute; length = " + length);

        _attributeLength = length;

        // Load the attribute information
        if (length > 0)
        {
            _info = new byte[length];

            for (int k=0; k<length; k++)
                _info[k] = instream.readByte();
        }

        return true;
    } // Load

    /** Output all information about this attribute */
    public boolean print()
    {
        System.out.print("                   GENERIC ATTRIBUTE : ");
        System.out.print("Name index = " + _attributeNameIndex + " (" + _genericName + ") ");
        System.out.println("Attribute data length = " + _attributeLength);

        return true;
    } // print

	/**
	 * returns an XML representation of this object
	 */
	public String toString ()
	{
		return "<genericattribute><name>" + _attributeNameIndex + "</name><datalength>" + _attributeLength + "</datalength></genericattribute>";
	}
} // GenericAttribute
