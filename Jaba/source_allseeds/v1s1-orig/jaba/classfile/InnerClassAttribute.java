/* Copyright (c) 1999, The Ohio State University */

package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;

/**
 * Represents an InnerClasses attribute. An InnerClasses attribute
 * is used in the ClassFile structure of a class file format, and
 * contains information about inner classes and 
 * interfaces declared within a class.
 * @see jaba.classfile.InnerClassEntry
 * @see jaba.classfile.ClassFile
 * @author Jay Lofstead 2003/05/29 added a toString method for generating XML
 */
public class InnerClassAttribute extends AttributeInfoImpl implements Cloneable
{

    // Fields

    /** Number of inner classes and interfaces. */
    private short numberOfClasses;

    /** Information about inner classes. */
    private InnerClassEntry[] innerClasses;


    // Constructor

    /**
     * Creates a new InnerClassAttribute object.
     * @param index Index in the constant pool for the attribute name.
     */
    public InnerClassAttribute( int index ) {
        _attributeNameIndex = (short)index;
        numberOfClasses = 0;
        innerClasses = null;
    }


    // Methods

    /**
     * Loads information about inner classes.
     * @param instream Data input handle to read inner class information
     *                 from a class file
     * @param constantPool Constant pool for the class file.
     * @throws IOException if read from instream fails.
     * @throws ClassFormatError if InnerClassEntry.Load() fails.
     * @see jaba.classfile.InnerClassEntry#Load InnerClassEntry.Load()
     */
    public boolean Load( DataInput instream,
                         ConstantPool constantPool ) throws IOException {
        _attributeLength = instream.readInt();

        numberOfClasses = instream.readShort();
        if ( numberOfClasses > 0 ) {
            innerClasses = new InnerClassEntry[numberOfClasses];
            for ( int i=0; i<numberOfClasses; i++ ) {
                innerClasses[i] = new InnerClassEntry();
                innerClasses[i].Load( instream, constantPool );
            }
        }
        return true;
    }

    /**
     * Returns the number of inner classes.
     * @return Number of inner classes.
     */
    public int getNumberOfInnerClasses() {
        return numberOfClasses;
    }

    /**
     * Returns information about inner classes.
     * @return Array of elements of type InnerClassEntry; each element contains
     *         information about an inner class.
     */
    public InnerClassEntry[] getInnerClasses() {
        return innerClasses;
    }

    /**
     * Prints information in an inner-class attribute to stdout.
     */
    public boolean print() {
        for ( int i=0; i<innerClasses.length; i++ ) {
            innerClasses[i].print();
        }
        return true;
    }

	/**
	 * returns an XML representation of the object
	 */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<innerclassattribute>");
		for (int i = 0; i < innerClasses.length; i++)
		{
			buf.append (innerClasses [i]);
		}
		buf.append ("</innerclassattribute>");

		return buf.toString ();
	}
}
