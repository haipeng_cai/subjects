package jaba.classfile;

/**     This file contains the implementation of the ConstantPoolInfo class.
 *      The class is responsible for representing a constant (all types) from
 *      a java .class file.  Current implementation uses Version 1.1.3 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 12/4/96.
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable; updated JavaDoc
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface ConstantPoolInfo extends Cloneable, java.io.Serializable
{
    ///////////////
    // Constants //
    ///////////////

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Class = 7;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Fieldref = 9;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Methodref = 10;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_InterfaceMethodref = 11;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_String = 8;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Integer = 3;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Float = 4;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Long = 5;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Double = 6;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_NameAndType = 12;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM */
    public static final byte CONSTANT_Utf8 = 1;

    /** Constant pool tag IDs.  These are currently updated to meet Version 1.1.2 of the JVM.
     * This one rarely seen except in old (1.0) class files.  This is not handled properly.
     */
    public static final byte CONSTANT_Unicode = 2;

    /////////////////////////////////////////////////////
    // Accessor methods
    //      Read/Write access to private class members
    /////////////////////////////////////////////////////

    /** ??? */
    public byte   getTagValue();

    /** ??? */
    public int    getNameIndex();

    /** ??? */
    public int    getClassIndex();

    /** ??? */
    public int    getNameTypeIndex();

    /** ??? */
    public int    getStringIndex();

    /** ??? */
    public int    getDescriptorIndex();

    /** ??? */
    public int    getInteger();

    /** ??? */
    public float  getFloat();

    /** ??? */
    public long   getLong();

    /** ??? */
    public double getDouble();

    /** ??? */
    public String getString();

    /** This method validates a given tag value */
    public boolean isValidTag(byte tag);

    /** Load a class constant (via name index) into a ConstantPoolInfo object */
    public void LoadClass(int nameIndex);

    /** Load a field reference constant into a ConstantPoolInfo object */
    public void LoadFieldRef(int classIndex, int nameTypeIndex);

    /** Load a method reference constant into a ConstantPoolInfo object */
    public void LoadMethodRef(int classIndex, int nameTypeIndex);

    /** Load an interface method reference constant into a ConstantPoolInfo object */
    public void LoadInterfaceMethodRef(int classIndex, int nameTypeIndex);

    /** Load a string constant (via string index) into a ConstantPoolInfo object */
    public void LoadString(int stringIndex);

    /** Load an integer constant into a ConstantPoolInfo object */
    public void LoadInteger(int iValue);

    /** Load a float constant into a ConstantPoolInfo object */
    public void LoadFloat(float fValue);

    /** Load a float (IEEE 754 format) constant into a ConstantPoolInfo object */
    public void LoadIEEE754Float(int iValue);

    /** Load a long constant into a ConstantPoolInfo object */
    public void LoadLong(long lValue);

    /** Load a double constant into a ConstantPoolInfo object */
    public void LoadDouble(double dValue);

    /** Load a double (IEEE 754 format) constant into a ConstantPoolInfo object */
    public void LoadIEEE754Double(long lValue);

    /** Load a nametype constant into a ConstantPoolInfo object */
    public void LoadNameType(int nameIndex, int descriptorIndex);

    /** Load a string constant into a ConstantPoolInfo object */
    public void LoadString(String stringConstant);

    /** Load a Utf8 constant into a ConstantPoolInfo object */
    public boolean LoadUtf8(int length, byte[] utf8String);

    /** Output information about the type and data of the constant info object */
    public void print();
} // ConstantPoolInfo
