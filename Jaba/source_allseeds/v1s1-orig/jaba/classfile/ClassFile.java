package jaba.classfile;

import jaba.constants.AccessLevel;
import jaba.constants.Modifier;

import jaba.main.ResourceFileI;

import java.io.IOException;
import java.io.DataInput;
import java.io.DataInputStream;

/**
 * Represents a java class file.
 * Module : ClassFile.java
 * 
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the ClassFile class.
 *      This class is responsible for containing all information stored
 *      in a java .class file.  Several functions (such as Load(), OutputInfo(), ...)
 *      provide the user with the ability to perform various operations on a
 *      ClassFile object.  Current implementation uses Version 1.1.3 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 *      Object of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 1/10/97.
 *
 * @author Don Lance -- <i>Created</i>
 * @author Huaxing Wu -- <i>Revised 3/21/02. Add getMethodInfos(), change getMethod() to return a reference instead of a copy</i>
 * @author Jay Lofstead 2003/05/22 removed redundant code that is legacy from when clone () was called.  Removed commented out code to aid in readability.  Added isClass, isInterface, getAccessLevel. getModifiers methods.
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface ClassFile extends Cloneable, AccessLevel, Modifier,
    jaba.sym.ClassAttribute, jaba.sym.InterfaceAttribute, java.io.Serializable
{
	// Output option flags
	/** Output all information (all bits set) */
	public static final int OUTPUT_ALL              = 0xFFFF;  

	/** Output version information */
	public static final int OUTPUT_VERSION          = 0x0001;  

	/** Output constant pool info by index */
	public static final int OUTPUT_CONSTANTS_INDEX  = 0x0002;  

	/** Output constant pool info by group */
	public static final int OUTPUT_CONSTANTS_GROUPS = 0x0004;

	/** Output access info */
	public static final int OUTPUT_ACCESS           = 0x0008;

	/** Output inheritance info */
	public static final int OUTPUT_INHERITANCE      = 0x0010;

	/** Output interface info */
	public static final int OUTPUT_INTERFACE        = 0x0020;

	/** Output field info */
	public static final int OUTPUT_FIELDS           = 0x0040;

	/** Output method info */
	public static final int OUTPUT_METHODS          = 0x0080;

	/** Output attributes */
	public static final int OUTPUT_ATTRIBUTES       = 0x0100;

	/** Output in class-file format, with bytecode ops */
	public static final int OUTPUT_FILEFORM         = 0x0200;

	////////////////////
	// Accessor methods
	////////////////////

	/** ??? */
	public String getFileName();

	/** ??? */
	public int    getMagic();

	/** ??? */
	public short  getMinorVersion();

	/** ??? */
	public short  getMajorVersion();

	/** ??? */
	public short  getConstantPoolCount();

	/** ??? */
	public short  getAccessFlags();

	/** ??? */
	public short  getClassIndex();

	/** ??? */
	public short  getSuperIndex();

	/** ??? */
	public short  getInterfacesCount();

	/** ??? */
	public short  getFieldsCount();

	/** ??? */
	public short  getMethodsCount();

	/** ??? */
	public short  getAttributeCount();

	/** ??? */
	public ConstantPool getConstantPool();

	/** ??? */
	public ConstantPool getConstantPoolRef();

	/** ??? */
	public short getInterfaceIndex(int index) throws IndexOutOfBoundsException;

	/** ??? */
	public FieldInfo getFieldInfo(int index) throws IndexOutOfBoundsException;

	/** returns if this ClassFile object represents a Java Class */
	public boolean isClass ();

	/** returns if this ClassFile object represents a Java Interface */
	public boolean isInterface ();

	/** returns the access level for this ClassFile object */
	public short getAccessLevel ();

	/** returns the access level for this ClassFile object */
	public short getModifiers ();

	/**
	 * Returns all methods in this class
	 * @return Array of MethodInfos.
	 */
	public MethodInfo[] getMethodInfos();

	/**
	 * Returns a method in this class
	 * @param index  The index of the method in the method array
	 * @return method 
	 */
	public MethodInfo getMethodInfo(int index) throws IndexOutOfBoundsException;

	/** ??? */
	public AttributeInfo getAttribute(int index) throws IndexOutOfBoundsException;

	/** ??? */
	public String GetClassName();

	/** ??? */
	public String GetSuperClassName();

	/** Test to see if a ClassFile object has data from a .class file */
	public boolean IsClassFileLoaded();

	/**
	 * Method Load()
	 *      Responsible for loading the java class file into a ClassFile object.
	 *
	 *      This function could throw an IOException on read errors,
	 *      ClassFormatErrors on improperly organized .class files
	 */
	public boolean Load(ResourceFileI rcFile) throws IOException;

	/**
	 * Method OutputInfo()
	 *      Output information about the contents of a ClassFile object
	 */
	public boolean OutputInfo(int options);

	/** Output information about the contents of a ClassFile object in "Classfile" format */
	public boolean OutputInFileFormat();
}
