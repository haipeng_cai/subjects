package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;

/**
 * Represents an attribut in java class file. It's the base for all attribute classes.
 * Module : AttributeInfo.java
 *
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the AttributeInfo class.
 *      It is the base class of all attribute classes.
 * 
 * Revision History : 
 *      Initial version, released 12/18/96.
 *
 * @author Don Lance -- <i>Created</i>
 * @author Huaxing Wu -- <i>Revised 3/21/02. Remove unused and obsolete methods </i>
 * @author Jay Lofstead 2003/05/29 added a toString method generating XML
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to implement AttributeInfo
 */
public abstract class AttributeInfoImpl implements AttributeInfo, java.io.Serializable
{
    ////////////////
    // Attributes //
    ////////////////

    /** Constant pool index to name of attribute */
    protected short  _attributeNameIndex;

    /** Length of following attribute information */
    protected int    _attributeLength;

    /////////////
    // Methods //
    /////////////

    // Functions that every subclass must have

    /** ??? */
    public abstract boolean Load(DataInput instream, ConstantPool pool) throws IOException;

    /** ??? */
    public abstract boolean print();

    /** generates an XML version of this object */
    public abstract String toString ();
} // AttributeInfo
