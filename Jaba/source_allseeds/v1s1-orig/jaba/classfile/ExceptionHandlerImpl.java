package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;

/**
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the ExceptionHandler class.
 *      The class is responsible for representing an exception handler from
 *      a java .class file.  Current implementation uses Version 1.1.3 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 12/20/96.
 * @author Jay Lofstead added a toStringMethod that returns XML formatted information
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to implement ExceptionHandler
 */
public class ExceptionHandlerImpl implements Cloneable, java.io.Serializable, ExceptionHandler
{
    ////////////////
    // Attributes //
    ////////////////

    /** Starting range in code where handler is active */
    private short   _start_pc;

    /** Ending range in code where handler is active */
    private short   _end_pc;

    /** Start of the handler */
    private short   _handler_pc;

    /** Type that is caught when exception handler invoked */
    private short   _catch_type;

    /** Copy of constant pool entry for catch type, if applies */
    private String  _catchString;

    /////////////
    // Methods //
    /////////////

    // Constructor

    /** ??? */
    public ExceptionHandlerImpl ()
    {
        _start_pc    = (short)0;
        _end_pc      = (short)0;
        _handler_pc  = (short)0;
        _catch_type  = (short)0;
        _catchString = null;
    }

    // Accessor methods

    /** ??? */
    public short  getStartPC()      { return _start_pc;                }

    /** ??? */
    public short  getEndPC()        { return _end_pc;                  }

    /** ??? */
    public short  getHandlerPC()    { return _handler_pc;              }

    /** ??? */
    public short  getCatchType()    { return _catch_type;              }

    /** ??? */
    public String getCatchString()  { return (_catchString == null) ? null : new String(_catchString); }

    /** Load & verify an ExceptionHandler from a DataInput */
    public boolean Load(DataInput instream, ConstantPool pool, int codeLength) throws IOException
    {
        int startpc, endpc, handlerpc, catchtype;


        // Load counters

        startpc   = instream.readUnsignedShort();
        endpc     = instream.readUnsignedShort();
        handlerpc = instream.readUnsignedShort();


        // Verify the counters
        // startpc should be a valid index into the code array
        // endpc should be a valid index into the code array, or equal to the code length
        // handlerpc should be a valid index into the code array, and less than code length

        if (startpc >= codeLength)
            throw new ClassFormatError("Exception handler code beginning range index is invalid;" +
                                       "   index = " + startpc + ", code length = " + codeLength);

        if (endpc > codeLength)
            throw new ClassFormatError("Exception handler code ending range index is invalid;" +
                                       "   index = " + endpc + ", code length = " + codeLength);

        if (handlerpc >= codeLength)
            throw new ClassFormatError("Exception handler code invokation index is invalid;" +
                                       "   index = " + handlerpc + ", code length = " + codeLength);


        // Store the counters

        _start_pc   = (short)startpc;
        _end_pc     = (short)endpc;
        _handler_pc = (short)handlerpc;


        // Load the exception catch type

        catchtype = instream.readUnsignedShort();

        // If the catch type is nonzero, then the type is an index into the constant table
        // It must refer to a CONSTANT_Class constant
        // The CONSTANT_Class must be class Throwable or one of its subclasses
        // If the catch type is zero, then this handler is caught for all exceptions (such as using "finally" keyword)

        if (catchtype != 0)
        {
            if (pool.IsThisAConstantPoolIndex(catchtype) == false)
                throw new ClassFormatError("Invalid catch type index found in exception handler; value = " + catchtype);

            if (pool.VerifyIndexIsConstantType(catchtype,ConstantPoolInfo.CONSTANT_Class) == false)
                throw new ClassFormatError("Catch type index for exception handler is not CONSTANT_Class; index = " + catchtype);

            // Verify that the referred CONSTANT_Class is derived from "Throwable"

        } // if


        _catch_type = (short)catchtype;


        // Now store a copy of the catch string
        //  If zero, then it is caught for all exceptions - use "all"

        if (catchtype == 0)
            _catchString = "all";

        else
        {
            ConstantPoolInfo  poolEntry = pool.getConstantPoolEntry(catchtype);

            if (poolEntry == null)
            {
                System.err.println("Invalid reference to catch type constant pool entry for ExceptionHandler (null)");
                return false;
            }

            _catchString = poolEntry.getString();
        }

        return true;
    } // Load

    /** Output all information about this handler */
    public boolean print()
    {
        System.out.print("                   EXCEPTION HANDLER : ");
        System.out.print("Code start = " + _start_pc + ", code end = " + _end_pc);
        System.out.print(", handler invokation start = " + _handler_pc);
        System.out.println("Catch type = " + _catch_type + " (" + _catchString + ") ");

        return true;
    } // print

	/**
	 * returns an XML format of the object
	 */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<exceptionhandler>");

		buf.append ("<startpc>").append (getStartPC ()).append ("</startpc>");
		buf.append ("<endpc>").append (getEndPC ()).append ("</endpc>");
		buf.append ("<handlerpc>").append (getHandlerPC ()).append ("</handlerpc>");
		buf.append ("<catchtype>").append (getCatchType ()).append ("</catchtype>");
		buf.append ("<catchstring>").append (getCatchString ()).append ("</catchstring>");

		buf.append ("</exceptionhandler>");

		return buf.toString ();
	}
} // ExceptionHandler
