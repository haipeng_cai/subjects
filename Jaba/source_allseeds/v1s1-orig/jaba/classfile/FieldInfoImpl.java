package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;

import jaba.constants.AccessLevel;
import jaba.constants.Modifier;

/**
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the FieldInfo class.
 * 
 * Revision History : 
 *      Initial version, released 12/18/96.
 * @author Jay Lofstead 2003/05/22 simplified the code removing all of the legacy code from the clone calls.
 * 					Removed all of the commented out code to aid in readability.
 * 					Added isPrivate, getAccessLevel, getModifiers, isSynthetic methods.
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to implement FieldInfo
 */
public class FieldInfoImpl implements Cloneable, AccessLevel, Modifier, java.io.Serializable, FieldInfo
{
    ////////////////
    // Attributes //
    ////////////////

    /** Access flags for the field declaration */
    private short               _accessFlags;

    /** Constant pool index to field name */
    private short               _nameIndex;

    /** Constant pool index to field descriptor */
    private short               _descriptorIndex;

    /** Number of additional attributes of this field */
    private short               _attributeCount;

    /** All attributes associated with this field */
    private AttributeInfo[]     _attributes;

    // The following attributes are copies from the constant pool

    /** Copy of field name from constant pool */
    private String              _nameString;

    /** Copy of field descriptor from constant pool */
    private String              _descriptorString;

	/** eliminates the checking of all of the attributes to look for this one thing. */
	private boolean synthetic = false;



    /////////////
    // Methods //
    /////////////

    // Constructor

    /** ??? */
    public FieldInfoImpl ()
    {
        _accessFlags      = (short)0;
        _nameIndex        = (short)0;
        _descriptorIndex  = (short)0;
        _attributeCount   = (short)0;
        _attributes       = null;
        _nameString       = null;
        _descriptorString = null;
    }


    /////////////////////
    // Accessor methods
    /////////////////////

    /** ??? */
    public short  getAccessFlags()      { return _accessFlags;                  }

    /** ??? */
    public short  getNameIndex()        { return _nameIndex;                    }

    /** ??? */
    public short  getDescriptorIndex()  { return _descriptorIndex;              }

    /** ??? */
    public short  getAttributeCount()   { return _attributeCount;               }

    /** ??? */
    public String getNameString()       { return (_nameString == null)       ? null : new String(_nameString); }

    /** ??? */
    public String getDescriptorString() { return (_descriptorString == null) ? null : new String(_descriptorString); }

    /** ??? */
    public AttributeInfo getAttribute(int index) throws IndexOutOfBoundsException
    {
        if ((index < 0) || (index >= _attributeCount))
            throw new IndexOutOfBoundsException("Invalid index for attributes: " + index);
	else
		return _attributes [index];
    } // getAttribute

	/** indicates if this is a private field or not */
	public boolean isPrivate ()
	{
		return (_accessFlags & A_PRIVATE) != 0 ? true : false;
	}

	/** returns the access level for this FieldInfo object */
	public short getAccessLevel ()
	{
		return (short) (_accessFlags & ((short) (A_PUBLIC | A_PRIVATE | A_PROTECTED | A_PACKAGE)));
	}

	/** returns the access level for this ClassFile object */
	public short getModifiers ()
	{
		return (short) (_accessFlags & ((short) (M_ABSTRACT | M_FINAL | M_STATIC | M_TRANSIENT | M_VOLATILE)));
	}

	/** returns if this is a synthetic field or not */
	public boolean isSynthetic ()
	{
		return synthetic;
	}

    /**
     * Load a FieldInfo object from a input stream attached to a .class file
     *  We require the constant pool as a parameter to use in error checking
     */
    public boolean Load(DataInput instream, ConstantPool pool) throws IOException
    {
        // Load & verify the field access flags

        if (LoadAccessFlags(instream) == false)
        {
            System.err.println("Load of field access flags failed.");
            return false;
        }

        // Load & verify the field name index

        if (LoadNameIndex(instream,pool) == false)
        {
            System.err.println("Load of field name index failed.");
            return false;
        }

        // Load & verify the descriptor index

        if (LoadDescriptorIndex(instream,pool) == false)
        {
            System.err.println("Load of descriptor index failed.");
            return false;
        }

        // Load & verify field attributes

        if (LoadAttributes(instream,pool) == false)
        {
            System.err.println("Load of field attributes failed.");
            return false;
        }

        return true;
    } // Load

    /** Load & verify field access flags from an input stream attached to a .class file */
    private boolean LoadAccessFlags(DataInput instream) throws IOException
    {
        ///////////////////////////
        // Load the access flags
        ///////////////////////////

        short allbits = (short)(A_PUBLIC | A_PRIVATE | A_PROTECTED | 
                                M_STATIC | M_FINAL   | M_VOLATILE  | M_TRANSIENT);

        short flags = (short)instream.readUnsignedShort();
        

        // Verify that all unused access bits are zero

        if ((flags & ~allbits) != 0)
            throw new ClassFormatError("Unused access bits for a field are not zeroed out;" +
                                       " improper access bits specified");


        // Verify that no improper field access bits are set

        // Make sure only one type of access is specified

        if ( ( ((flags & A_PUBLIC)  != 0) && ((flags & A_PRIVATE)   != 0) ) ||
             ( ((flags & A_PUBLIC)  != 0) && ((flags & A_PROTECTED) != 0) ) ||
             ( ((flags & A_PRIVATE) != 0) && ((flags & A_PROTECTED) != 0) ) )

            throw new ClassFormatError("Field in class file is set for multiple types of access;" +
                                       " (combinations of public, private, protected) : improper access bits specified");


        // Make sure M_FINAL and M_VOLATILE are not both set; specs say this is impossible

        if ( ((flags & M_FINAL) != 0) && ((flags & M_VOLATILE) != 0) ) 
            throw new ClassFormatError("\nField in class file is both \"final\" and \"volatile\";" +
                                       " improper access bits specified");


        // Note - all fields of interfaces must be marked M_FINAL and M_STATIC
        // Also, every field in an interface must have the A_PUBLIC flag set
        // Don't know know I can verify this at this time

        _accessFlags = flags;

        return true;
    } // LoadAccessFlags

    /** Load & verify field name index from an input stream attached to a .class file */
    private boolean LoadNameIndex(DataInput instream, ConstantPool pool) throws IOException
    {
        // Load & verify the name constant pool index

        int nameIndex = instream.readUnsignedShort();

        if (pool.IsThisAConstantPoolIndex(nameIndex) == false)
            throw new ClassFormatError("Invalid constant pool index for field; value = " + nameIndex);

        if (pool.VerifyIndexIsConstantType(nameIndex,ConstantPoolInfo.CONSTANT_Utf8) == false)
            throw new ClassFormatError("Constant pool index for field is not CONSTANT_Utf8; index = " + nameIndex);

        _nameIndex = (short)nameIndex;


        // Now retrieve a copy of the field name from the constant pool

        ConstantPoolInfo  poolEntry = pool.getConstantPoolEntry(nameIndex);

        if (poolEntry == null)
            throw new ClassFormatError("Name index for field refers to invalid constant pool entry; index = " + nameIndex);

        _nameString = poolEntry.getString();

        return true;
    } // LoadNameIndex

    /** Load & verify field name index from an input stream attached to a .class file */
    private boolean LoadDescriptorIndex(DataInput instream, ConstantPool pool) throws IOException
    {
        // Load & verify the descriptor constant pool index

        int descIndex = instream.readUnsignedShort();

        if (pool.IsThisAConstantPoolIndex(descIndex) == false)
            throw new ClassFormatError("Invalid constant pool index for field; value = " + descIndex);

        if (pool.VerifyIndexIsConstantType(descIndex,ConstantPoolInfo.CONSTANT_Utf8) == false)
            throw new ClassFormatError("Constant pool index for field is not CONSTANT_Utf8; index = " + descIndex);

        _descriptorIndex = (short)descIndex;

        // Now get a copy of the descriptor string from the constant pool

        ConstantPoolInfo  poolEntry = pool.getConstantPoolEntry(descIndex);

        if (poolEntry == null)
            throw new ClassFormatError("Descriptor index for field refers to invalid constant pool entry");

        _descriptorString = poolEntry.getString();

        return true;
    } // LoadDescriptorIndex

    /** Load & verify field attributes from an input stream attached to a .class file */
    private boolean LoadAttributes(DataInput instream, ConstantPool pool) throws IOException
    {
        int     attributeCount;
        int     attributeIndex;
        String  attributeName;

        // Load the number of attributes

        attributeCount = instream.readUnsignedShort();

        if (attributeCount < 0)
            throw new ClassFormatError("Invalid number of field attributes found; value = " + attributeCount);

        _attributeCount = (short)attributeCount;

        // Load the attributes

        if (attributeCount > 0)
        {
            _attributes = new AttributeInfo[attributeCount];

            // LOOP : 
            //  Load and create each attribute from the input stream

            for (int k=0; k<attributeCount; k++)
            {
                // Read the attribute name index to find out type

                attributeIndex = instream.readUnsignedShort();


                // Verify the attribute name index is valid

                if (pool.IsThisAConstantPoolIndex(attributeIndex) == false)
                    throw new ClassFormatError("Invalid attribute name index found in field; value = " + attributeIndex);

                // Verify the attribute name index refers to a CONSTANT_Utf8

                if (pool.VerifyIndexIsConstantType(attributeIndex,ConstantPoolInfo.CONSTANT_Utf8) == false)
                    throw new ClassFormatError("Attribute name index for field is not CONSTANT_Utf8; index = " + attributeIndex);


                // Obtain the name of the attribute
                // Then create the appropiate attribute object

                attributeName = pool.GetStringForUtf8Constant(attributeIndex);

                AttributeInfo  attr=null;

                if (attributeName.compareTo("ConstantValue") == 0)
                    attr = (AttributeInfo) new ConstantValueAttribute(attributeIndex);

                else if (attributeName.compareTo("Synthetic") == 0)
		{
                    attr = (AttributeInfo) new SyntheticAttribute(attributeIndex);
			synthetic = true;
		}
                else
                    attr = (AttributeInfo) new GenericAttribute(attributeIndex);


                if (attr.Load(instream,pool) == false)
                {
                    System.err.println("Attribute load failure occurred");
                    return false;
                }

                _attributes[k] = attr;

            } // for

        } // if

        return true;
    } // LoadAttributes

    /** Output all information about a field */
    public boolean print()
    {
        System.out.println("   FIELD:");


        // Access flags

        System.out.print("      Access flags = ");

        if ((_accessFlags & A_PUBLIC) != 0)    System.out.print("PUBLIC ");
        if ((_accessFlags & A_PRIVATE) != 0)   System.out.print("PRIVATE ");
        if ((_accessFlags & A_PROTECTED) != 0) System.out.print("PROTECTED ");
        if ((_accessFlags & M_STATIC) != 0)    System.out.print("STATIC ");
        if ((_accessFlags & M_FINAL) != 0)     System.out.print("FINAL ");
        if ((_accessFlags & M_VOLATILE) != 0)  System.out.print("VOLATILE ");
        if ((_accessFlags & M_TRANSIENT) != 0) System.out.print("TRANSIENT ");
        System.out.println("");

        // Indices
        System.out.println("      Name index = " + _nameIndex + " (" + _nameString + ")");
        System.out.println("      Descriptor index = " + _descriptorIndex + " (" + _descriptorString + ")");
        
        // Attributes
        System.out.println("\n      Number of attributes = " + _attributeCount);
        System.out.println("      Attributes : ");

        for (int k=0; k<_attributeCount; k++)
            _attributes[k].print();

        return true;
    } // print
} // FieldInfo
