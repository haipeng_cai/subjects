package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;

/**
 * Represents a LineNumberTable attribute from a java .class file.
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the LineNumberTableAttribute class.
 *      The class is responsible for representing a LineNumberTable attribute from
 *      a java .class file.  Current implementation uses Version 1.1.3 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 12/30/96.
 * Current implementation uses Version 1.1.3 of the Java Virtual Machine (JVM) spec.
 * Objects of this class are cloneable.
 *
 * @author Don Lance -- <i>Created 12/30/96</i> 
 * @author Jay Lofstead 2003/05/29 added a toString for generating XML
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 */
public class LineNumberTableAttribute extends AttributeInfoImpl implements Cloneable
{
    ////////////////
    // Attributes //
    ////////////////

    /** Length of the line number table */
    private short              _LineNumberTableLength;  

    /** Line table entries indicating line number changes */
    private LineNumberEntry[]  _LineNumberTable;     
   
    /** For data verification */
    private int                _codeLength;             

    /////////////
    // Methods //
    /////////////

    // Constructor

    /** ??? */
    public LineNumberTableAttribute(int index, int codeLength)
    {
        _attributeNameIndex    = (short)index;
        _LineNumberTableLength = (short)0;
        _LineNumberTable       = null;
        _codeLength            = codeLength;
    }

    /////////////////////
    // Accessor methods
    /////////////////////

    /** ??? */
    public short getLineNumberTableLength()  { return _LineNumberTableLength; }

    /** ??? */
    public int   getCodeLength()             { return _codeLength;            }

    /** ??? */
    public int getLineNumberAtOffset( int offset ) {
        for ( int i=_LineNumberTable.length-1; i>=0; i-- ) {
            if ( _LineNumberTable[i].GetStartPC() == offset ) {
                return _LineNumberTable[i].GetLineNumber();
            }
        }
        return 0;
    }

    /** ??? */
    public LineNumberEntry getLineNumberTableEntry(int index) throws IndexOutOfBoundsException
    {
        if ((index < 0) || (index >= _LineNumberTableLength))
            throw new IndexOutOfBoundsException("Invalid index into line number table: " + index);

        return (LineNumberEntry)_LineNumberTable[index]; //.clone();
    }

    /** Method loads a LineNumberTableAttribute attribute object from an input stream
     *  Method assumes that the attribute name index has already been read;
     *  stream is positioned to read attribute length
     */
    public boolean Load(DataInput instream, ConstantPool pool) throws IOException
    {
        // Load the attribute info length & verify

        int length = instream.readInt();

        if (length < 0)
            throw new ClassFormatError("Invalid length for line number table attribute; length = " + length);

        _attributeLength = length;


        // Load the exceptions information

        _LineNumberTableLength = (short)instream.readUnsignedShort();

        if (_LineNumberTableLength < 0)
            throw new ClassFormatError("Invalid line number table length: " + _LineNumberTableLength);

        if (_LineNumberTableLength > 0)
        {
            _LineNumberTable = new LineNumberEntry[_LineNumberTableLength];

            for (int k=0; k<_LineNumberTableLength; k++)
            {
                _LineNumberTable[k] = new LineNumberEntry();

                if (_LineNumberTable[k].Load(instream,_codeLength) == false)
                {
                    System.err.println("Load of LineNumberEntry failed");
                    return false;
                }
            } // for
        } // if

        return true;
    } // Load

    /** Output all information about this attribute */
    public boolean print()
    {
        System.out.println("                   LINE NUMBER TABLE ATTRIBUTE : ");
        System.out.println("Number of line table entries = " + _LineNumberTableLength);

        for (int k=0; k<_LineNumberTableLength; k++)
        {
            System.out.println("Line table entry # " + k + " : ");
            _LineNumberTable[k].print();
        }

        return true;

    } // print

	/**
	 * returns an XML representation of this object
	 */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<linenumbertableattribute>");
		for (int i = 0; i < _LineNumberTableLength; i++)
		{
			buf.append (_LineNumberTable [i]);
		}
		buf.append ("</linenumbertableattribute>");

		return buf.toString ();
	}
} // LineNumberTableAttribute
