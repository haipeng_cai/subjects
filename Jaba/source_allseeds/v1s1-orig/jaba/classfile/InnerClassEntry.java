/* Copyright (c) 1999, The Ohio State University */

package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;
import jaba.constants.AccessLevel;
import jaba.constants.Modifier;

/**
 * Represents an entry into the table contained within an InnerClasses
 * attribute.
 * Each entry provides information about an inner class or interface.
 * @see jaba.classfile.InnerClassAttribute
 * @author Jay Lofstead 2003/05/29 added a toString for generating XML
 * @author Jay Lofstead 2003/06/09 changed to implement serializable
 */
public class InnerClassEntry implements AccessLevel, Modifier, Cloneable, java.io.Serializable
{
    // Fields

    /** Constant pool index to entry of type CONSTANT_Class_info for
        the inner class */
    private short innerClassInfoIndex;

    /** Constant pool index to entry of type CONSTANT_Class for
        the outer class */
    private short outerClassInfoIndex;

    /** Constant pool index to entry of type CONSTANT_Utf8 for the name of
        the inner class */
    private short innerNameIndex;

    /** Access flag bitmask for the inner class */
    private short innerClassAccessFlags;

    /** String representation of the inner class name. */
    private String innerName;


    // Constructor

    /**
     * Creates a new inner class entry object.
     */
    public InnerClassEntry() {
        innerClassInfoIndex = outerClassInfoIndex = innerNameIndex = 0;
        innerClassAccessFlags = 0;
        innerName = null;
    }


    // Methods

    /**
     * Loads information about an inner class or interface.
     * @param instream Data input handle to read inner class information
     *                 from a class file
     * @param pool Constant pool for the class file.
     * @throws IOException if read from instream fails.
     * @throws ClassFormatError if information read from class file contains
     *                          references to invalid contant pool entries.
     */
    public void Load( DataInput instream,
                      ConstantPool pool ) throws IOException,
                                                 ClassFormatError {

        // read index for inner class info, and verify
        innerClassInfoIndex = instream.readShort();
	if ( innerClassInfoIndex != 0 ) {
	  if ( !pool.IsThisAConstantPoolIndex( (int)innerClassInfoIndex ) ) {
            throw new ClassFormatError( "InnerClassEntry.Load(): "+
					"Bad inner class index: "+ 
					innerClassInfoIndex );
	  }
	  if ( !pool.VerifyIndexIsConstantType( (int)innerClassInfoIndex,
				          ConstantPoolInfo.CONSTANT_Class ) ) {
            throw new ClassFormatError( "InnerClassEntry.Load(): "+
					"Inner class index does not refer to "+
					"a CONSTANT_Class" );
	  }

	}

        // read index for outer class info, and verify
        outerClassInfoIndex = instream.readShort();
	if ( outerClassInfoIndex != 0 ) {
	  if ( !pool.IsThisAConstantPoolIndex( (int)outerClassInfoIndex ) ) {
            throw new ClassFormatError( "InnerClassEntry.Load(): "+
					"Bad outer class index: "+ 
					outerClassInfoIndex );
	  }
	  if ( !pool.VerifyIndexIsConstantType( (int)outerClassInfoIndex,
				          ConstantPoolInfo.CONSTANT_Class ) ) {
            throw new ClassFormatError( "InnerClassEntry.Load(): "+
					"Inner class index does not refer to "+
					"a CONSTANT_Class" );
	  }

	}

        // read index for inner class name, and verify
        innerNameIndex = instream.readShort();
	if ( innerNameIndex != 0 ) {
	  if ( ( innerNameIndex != 0 ) && 
	       ( !pool.IsThisAConstantPoolIndex( (int)innerNameIndex ) ) ) {
            throw new ClassFormatError( "InnerClassEntry.Load(): "+
					"Bad inner class name index: "+ 
					innerNameIndex );
	  }
	  if ( !pool.VerifyIndexIsConstantType( (int)innerNameIndex,
                ConstantPoolInfo.CONSTANT_Utf8 ) ) {
            throw new ClassFormatError( "InnerClassEntry.Load(): "+
					"Inner class name index does not "+
					"refer to a CONSTANT_Utf8" );
	  }

	}

        innerClassAccessFlags = instream.readShort();
	if ( innerNameIndex != 0 )
	  innerName = pool.GetStringForUtf8Constant( innerNameIndex );
	else
	  innerName = null;

    }

    /**
     * Returns constant pool index for inner class information. The entry at
     * that index is of type CONSTANT_Class.
     * @return Constant pool index for inner class information.
     */
    public short getInnerClassInfoIndex() {
        return innerClassInfoIndex;
    }

    /**
     * Returns constant pool index for outer class information. The entry at
     * that index is of type CONSTANT_Class.
     * @return Constant pool index for outer class information.
     */
    public short getOuterClassInfoIndex() {
        return outerClassInfoIndex;
    }

    /**
     * Returns constant pool index for inner class name. The entry at
     * that index is of type CONSTANT_Utf8.
     * @return Constant pool index for inner class name.
     */
    public short getInnerClassNameIndex() {
        return innerNameIndex;
    }

    /**
     * Returns bit-mask for inner class access flags.
     * @return Bit-mask for access flags for inner class.
     */
    public short getInnerClassAccessFlags() {
        return innerClassAccessFlags;
    }

    /**
     * Returns simple name of inner class.
     * @return String name for inner class.
     */
    public String getInnerClassName() {
        return innerName;
    }

  /**
   * Sets the simple name of the inner class.
   * @param String name for inner class.
   */
  private void setInnerClassName( String name ) {
    innerName = name;
  }

    /**
     * Prints an inner class entry to stdout.
     */
    public void print() {
        String str = "Inner Class: "+innerName+"\n";
        str += "\tinner class info index = "+innerClassInfoIndex+"\n";
        str += "\touter class info index = "+outerClassInfoIndex+"\n";
        str += "\tAccess flags = ";
        if ( (innerClassAccessFlags & A_PUBLIC) != 0 ) {
            str += "PUBLIC ";
        }
        if ( (innerClassAccessFlags & A_PRIVATE) != 0 ) {
            str += "PRIVATE ";
        }
        if ( (innerClassAccessFlags & A_PROTECTED) != 0 ) {
            str += "PROTECTED ";
        }
        if ( (innerClassAccessFlags & M_FINAL) != 0 ) {
            str += "FINAL ";
        }
        if ( (innerClassAccessFlags & M_SUPER) != 0 ) {
            str += "SUPER ";
        }
        if ( (innerClassAccessFlags & M_ABSTRACT) != 0 ) {
            str += "ABSTRACT ";
        }
        str += "\n";
        System.out.println( str );
    }

	/**
	 * returns an XML representation of this object
	 */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<innerclassentry>");

		buf.append ("<name>" + innerName + "</name>");
		buf.append ("<innerclassinfoindex>" + innerClassInfoIndex + "</innerclassinfoIndex>");
		buf.append ("<outerclassinfoindex>" + outerClassInfoIndex + "</outerclassinfoindex>");

		if ((innerClassAccessFlags & A_PUBLIC) != 0)
		{
			buf.append ("<accessflag>public</accessflag>");
		}
		if ((innerClassAccessFlags & A_PRIVATE) != 0)
		{
			buf.append ("<accessflag>private</accessflag>");
		}
		if ((innerClassAccessFlags & A_PROTECTED) != 0)
		{
			buf.append ("<accessflag>protected</accessflag>");
		}
		if ((innerClassAccessFlags & M_FINAL) != 0)
		{
			buf.append ("<accessflag>final</accessflag>");
		}
		if ((innerClassAccessFlags & M_SUPER) != 0)
		{
			buf.append ("<accessflag>superc</accessflag>");
		}
		if ((innerClassAccessFlags & M_ABSTRACT) != 0)
		{
			buf.append ("<accessflag>abstract</accessflag>");
		}

		buf.append ("</innerclassentry>");

		return buf.toString ();
	}
}
