package jaba.classfile;

import jaba.constants.AccessLevel;
import jaba.constants.Modifier;

import jaba.main.ResourceFileI;

import java.io.IOException;
import java.io.DataInput;
import java.io.DataInputStream;

/**
 * Represents a java class file.
 * Module : ClassFile.java
 * 
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the ClassFile class.
 *      This class is responsible for containing all information stored
 *      in a java .class file.  Several functions (such as Load(), OutputInfo(), ...)
 *      provide the user with the ability to perform various operations on a
 *      ClassFile object.  Current implementation uses Version 1.1.3 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 *      Object of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 1/10/97.
 *
 * @author Don Lance -- <i>Created</i>
 * @author Huaxing Wu -- <i>Revised 3/21/02. Add getMethodInfos(), change getMethod() to return a reference instead of a copy</i>
 * @author Jay Lofstead 2003/05/22 removed redundant code that is legacy from when clone () was called.  Removed commented out code to aid in readability.  Added isClass, isInterface, getAccessLevel. getModifiers methods.
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to implement ClassFile
 */
public class ClassFileImpl implements Cloneable, AccessLevel, Modifier, ClassFile,
    jaba.sym.ClassAttribute, jaba.sym.InterfaceAttribute, java.io.Serializable
{
    ////////////////
    // Attributes //
    ////////////////

    /** name of the class file */
    private String  _className;

    // The following attributes follow the layout of a class file
    // Note that in some cases there could be redundant copies of class attributes,
    //  but I chose to keep a copy of them here to illustrate the standard class file layout

    /** Magic number (class file format ID) */
    private int              _Magic;

    /** Version number of compiler */
    private short            _MinorVersion;

    /** Version number of compiler */
    private short            _MajorVersion;

    /** Number of entries in constant pool table (redundant copy) */
    private short            _ConstantPoolCount;

    /** Object representing the constant pool */
    private ConstantPool     _ConstantPool;

    /** Modifiers for class/interface declarations */
    private short            _accessFlags;

    /** Constant pool index representing class/interface */
    private short            _thisClass;

    /** Constant pool index representing class's superclass */
    private short            _superClass;

    /** Number of direct superinterfaces of class/interface */
    private short            _interfacesCount;

    /** Array of constant pool indices of all superinterfaces */
    private short[]          _interfaces;

    /** Number of fields of class/interface */
    private short            _fieldsCount;

    /** Array of fields associated with class/interface */
    private FieldInfo[]      _fields;

    /** Number of methods of class/interface */
    private short            _methodsCount;

    /** Array of methods associated with class/interface */
    private MethodInfo[]     _methods;

    /** Number of attributes attached to the class file */
    private short            _attributeCount;

    /** Attributes attached to the class file */
    private AttributeInfo[]  _attributes;
   
    ///////////////
    // Constants //
    ///////////////

    // Output option flags
    /** Output all information (all bits set) */
    public static final int OUTPUT_ALL              = 0xFFFF;  

    /** Output version information */
    public static final int OUTPUT_VERSION          = 0x0001;  

    /** Output constant pool info by index */
    public static final int OUTPUT_CONSTANTS_INDEX  = 0x0002;  

    /** Output constant pool info by group */
    public static final int OUTPUT_CONSTANTS_GROUPS = 0x0004;

    /** Output access info */
    public static final int OUTPUT_ACCESS           = 0x0008;

    /** Output inheritance info */
    public static final int OUTPUT_INHERITANCE      = 0x0010;

    /** Output interface info */
    public static final int OUTPUT_INTERFACE        = 0x0020;

    /** Output field info */
    public static final int OUTPUT_FIELDS           = 0x0040;

    /** Output method info */
    public static final int OUTPUT_METHODS          = 0x0080;

    /** Output attributes */
    public static final int OUTPUT_ATTRIBUTES       = 0x0100;

    /** Output in class-file format, with bytecode ops */
    public static final int OUTPUT_FILEFORM         = 0x0200;



    /////////////
    // Methods //
    /////////////

    // Constructor
    /**
     * Takes the class file name as a parameter
     */
    public  ClassFileImpl (String className)
    {
	_className  = className;
    }
 
    ////////////////////
    // Accessor methods
    ////////////////////

    /** ??? */
    public String getFileName()          { return _className; } 

    /** ??? */
    public int    getMagic()             { return _Magic;             }

    /** ??? */
    public short  getMinorVersion()      { return _MinorVersion;      }

    /** ??? */
    public short  getMajorVersion()      { return _MajorVersion;      }

    /** ??? */
    public short  getConstantPoolCount() { return _ConstantPoolCount; }

    /** ??? */
    public short  getAccessFlags()       { return _accessFlags;       }

    /** ??? */
    public short  getClassIndex()        { return _thisClass;         }

    /** ??? */
    public short  getSuperIndex()        { return _superClass;        }

    /** ??? */
    public short  getInterfacesCount()   { return _interfacesCount;   }

    /** ??? */
    public short  getFieldsCount()       { return _fieldsCount;       }

    /** ??? */
    public short  getMethodsCount()      { return _methodsCount;      }

    /** ??? */
    public short  getAttributeCount()    { return _attributeCount;    }

    /** ??? */
    public ConstantPool getConstantPool()    { return (ConstantPool)_ConstantPool;}

    /** ??? */
    public ConstantPool getConstantPoolRef() { return _ConstantPool; }                        // Actually refer to original

    /** ??? */
    public short getInterfaceIndex(int index) throws IndexOutOfBoundsException
    {
        if ((index < 0) || (index >= _interfacesCount)) 
            throw new IndexOutOfBoundsException("Interface index out of bounds: " + index);

        return _interfaces[index];
    }

    /** ??? */
    public FieldInfo getFieldInfo(int index) throws IndexOutOfBoundsException
    {
        if ((index < 0) || (index >= _fieldsCount))
            throw new IndexOutOfBoundsException("Field index out of bounds: " + index);

        return (FieldInfo)_fields[index]; //.clone();
    }

	/** returns if this ClassFile object represents a Java Class */
	public boolean isClass ()
	{
		return (_accessFlags & M_INTERFACE) == 0 ? true : false;
	}

	/** returns if this ClassFile object represents a Java Interface */
	public boolean isInterface ()
	{
		return (_accessFlags & M_INTERFACE) != 0 ? true : false;
	}

	/** returns the access level for this ClassFile object */
	public short getAccessLevel ()
	{
		return (short) (_accessFlags & ((short) (A_PUBLIC | A_PRIVATE | A_PROTECTED | A_PACKAGE)));
	}

	/** returns the access level for this ClassFile object */
	public short getModifiers ()
	{
		return (short) (_accessFlags & ((short) (M_ABSTRACT | M_FINAL | M_SUPER | M_INTERFACE)));
	}

    /**
     * Returns all methods in this class
     * @return Array of MethodInfos.
     */
    public MethodInfo[] getMethodInfos()
    {
	return _methods;
    }
    
    /**
     * Returns a method in this class
     * @param index  The index of the method in the method array
     * @return method 
     */
    public MethodInfo getMethodInfo(int index) throws IndexOutOfBoundsException
    {
        if ((index < 0) || (index >= _methodsCount))
            throw new IndexOutOfBoundsException("Method index out of bounds: " + index);

        return (MethodInfo)_methods[index];
    }

    /** ??? */
    public AttributeInfo getAttribute(int index) throws IndexOutOfBoundsException
    {
        if ((index < 0) || (index >= _attributeCount))
            throw new IndexOutOfBoundsException("Attribute index out of bounds: " + index);
	else
		return _attributes [index];
    } // getAttribute

    /** ??? */
    public String GetClassName()      
    {
      return _ConstantPool.GetStringForClassConstant(_thisClass);
    }

    /** ??? */
    public String GetSuperClassName() 
    { 
      String superClassName = null;
      if ( _superClass != 0 )
	superClassName = _ConstantPool.GetStringForClassConstant(_superClass);
      return superClassName;

    }

    /** Test to see if a ClassFile object has data from a .class file */
    public boolean IsClassFileLoaded() { return (_Magic != 0XCAFEBABE) ? false : true; }

    /**
     * Method Load()
     *      Responsible for loading the java class file into a ClassFile object.
     *
     *      This function could throw an IOException on read errors,
     *      ClassFormatErrors on improperly organized .class files
     */
    public boolean Load(ResourceFileI rcFile) throws IOException
    {
        // Verify that we have a filename
	if (_className.equals("") == true) return false;

        // Create an input stream for the class file
	DataInputStream instream = null;
	try {
	    instream = new DataInputStream(rcFile.getInputStream(_className));
	}catch (IOException ex) {
	    throw new NoClassDefFoundError(_className);
	}

	// Load magic & version numbers

	_Magic = instream.readInt();
	
	// Per the spec, the magic number should be this
	
	if (_Magic != 0xCAFEBABE)      
	    throw new ClassFormatError("Class file contained bad magic number: " + _Magic);
	
	_MinorVersion = (short)instream.readUnsignedShort();
	_MajorVersion = (short)instream.readUnsignedShort();
	
	// Load constant pool
	
	_ConstantPool = new ConstantPoolImpl ();
	
	if (_ConstantPool.LoadConstantPool(instream) == false){
                System.err.println("\nERROR> LoadConstantPool() failed\n");
                return false;
	}
	
	_ConstantPoolCount = _ConstantPool.getConstantPoolCount();
	
	// Load class-specific information (access, inheritance)
	
	if (LoadAccessInfo(instream) == false){
	    System.err.println("\nERROR> LoadAccessInfo() failed\n");
	    return false;
	}
	
	if (LoadInheritanceInfo(instream) == false){
	    System.err.println("\nERROR> LoadInheritanceInfo() failed\n");
	    return false;
	}
	    
	
	// Load interfaces
	    
	if (LoadInterfaceInfo(instream) == false){
	    System.err.println("\nERROR> LoadInterfaceInfo() failed\n");
	    return false;
	}
	    
	
	// Load fields
	if (LoadFieldInfo(instream) == false){
	    System.err.println("\nERROR> LoadFieldInfo() failed\n");
	    return false;
	}
	    
	// Load methods    
	if (LoadMethodInfo(instream) == false){
	    System.err.println("\nERROR> LoadMethodInfo() failed\n");
	    return false;
	}
	    
	// Load & verify class file attributes    
	if (LoadAttributes(instream,_ConstantPool) == false){
	    System.err.println("Load of class file attributes failed.");
	    return false;
	}
	    
	instream.close();

	return true;
	    
    } // Load


    /**
     * Method LoadAccessInfo()
     *      Internal utility function - load & verify access info from a java class file
     *      Currently this routine will always return true, or will throw an Exception or Error
     */
    private boolean LoadAccessInfo(DataInput instream) throws IOException
    {
        int allbits = A_PUBLIC | M_FINAL | M_SUPER | M_INTERFACE | M_ABSTRACT;
        int flags   = instream.readUnsignedShort();

        // Verify that all unused access bits are zero

        if ((flags & ~allbits) != 0)
            throw new ClassFormatError("Unused access bits for class/interface not zeroed out;" +
                                       " improper access bits specified.  Given flags = " + flags);

        // Verify that no improper class/interface access bits are set
        // Make sure M_FINAL and M_INTERFACE are not both set; specs say this is impossible

        if ( ((flags & M_FINAL) != 0) && ((flags & M_INTERFACE) != 0) ) 
            throw new ClassFormatError("Interface in class file is \"final\" (impossible); improper access bits specified");

        _accessFlags = (short)flags;
        return true;

    } // LoadAccessInfo

    /** load & verify inheritance info from a java class file. Currently this function always returns true or throws an Exception or Error. */
    private boolean LoadInheritanceInfo(DataInput instream) throws IOException
    {
        int thisClass  = instream.readUnsignedShort();
        int superClass = instream.readUnsignedShort();


        // Verify that we have a valid 'this' index into the constant pool 

        if (_ConstantPool.IsThisAConstantPoolIndex(thisClass) == false)
            throw new ClassFormatError("\"this\" class index is invalid in inheritance information; value = " + thisClass);


        // Verify that we have a valid 'super' index - either zero or in the constant pool

        if ((superClass != 0) && _ConstantPool.IsThisAConstantPoolIndex(superClass) == false)
            throw new ClassFormatError("\"super\" class index is invalid in inheritance information; value = " + superClass);


        // Verify that the indices refer to class constants

        if (_ConstantPool.VerifyIndexIsConstantType(thisClass,ConstantPoolInfo.CONSTANT_Class) == false)
            throw new ClassFormatError("\"this\" class index does not refer to a CONSTANT_Class in constant pool;" + 
                                       "value = " + thisClass);

        if (superClass == 0)  
        {
            // Here the class file must represent class java.lang.Object, the only class without a superclass
        }
        else if (_ConstantPool.VerifyIndexIsConstantType(superClass,ConstantPoolInfo.CONSTANT_Class) == false)
        {
            throw new ClassFormatError("\"super\" class index does not refer to a CONSTANT_Class in constant pool;" +
                                       "value = " + superClass);
        }


        // If the class file represents an interface, "super" index MUST be a valid index into constant pool
        // If the index is not zero it was already checked above to verify it is valid

        if ( ((_accessFlags & M_INTERFACE) != 0) && (superClass == 0) )
            throw new ClassFormatError("\"super\" class index for this interface does not refer to a CONSTANT_Class" +
                                       " in the constant pool; value = 0");


        // Another restriction is that the superclass (nor any of its superclasses) may be final
        // Not sure how to verify it at this time
        // This verification code probably should be placed here


        _thisClass  = (short) thisClass;
        _superClass = (short) superClass;

        return true;

    } // LoadInheritanceInfo

    /** load & verify interface info from a java class file.  Currently this function always returns true or throws an Exception or Error. */
    private boolean LoadInterfaceInfo(DataInput instream) throws IOException
    {
        _interfacesCount = (short)instream.readUnsignedShort();

        if (_interfacesCount > 0)
        {
            _interfaces = new short[_interfacesCount];

            for (int k=0; k<_interfacesCount; k++)
            {
                _interfaces[k] = (short)instream.readUnsignedShort();

                if (_ConstantPool.VerifyIndexIsConstantType((int)_interfaces[k],ConstantPoolInfo.CONSTANT_Class) == false)
                    throw new ClassFormatError("Index is not of CONSTANT_Class constant type; value = " + _interfaces[k]);

            } // for

        } // if
        
        return true;

    } // LoadInterfaceInfo

    /** load & verify field info from a java class file.  Currently this function always returns true or throws an Exception or Error. */
    private boolean LoadFieldInfo(DataInput instream) throws IOException
    {
        _fieldsCount = (short)instream.readUnsignedShort();

        if (_fieldsCount > 0)
        {
            _fields = new FieldInfo[_fieldsCount];

            for (int k=0; k<_fieldsCount; k++)
            {
                _fields[k] = new FieldInfoImpl ();

                if (_fields[k].Load(instream,_ConstantPool) == false)
                    throw new ClassFormatError("Load failure occurred on loading FieldInfo from instream stream");

            } // for

        } // if
        
        return true;

    } // LoadFieldInfo

    /** load & verify method info from a java class file */
    private boolean LoadMethodInfo(DataInput instream) throws IOException
    {
        _methodsCount = (short)instream.readUnsignedShort();

        if (_methodsCount > 0)
        {
           _methods = new MethodInfo[_methodsCount];

            for (int k=0; k<_methodsCount; k++)
            {
                _methods[k] = new MethodInfoImpl ();

                if (_methods[k].Load(instream,_ConstantPool) == false)
                    throw new ClassFormatError("Load failure occurred on loading MethodInfo from instream stream");

            } // for

        } // if
        
        return true;

    } // LoadMethodInfo

    /** Load & verify class file attributes from an input stream.
     *  Returns true on success, false on failure. Possible Exceptions or Errors could be thrown.
     */
    private boolean LoadAttributes(DataInput instream, ConstantPool pool) throws IOException
    {
        int     attributeCount;
        int     attributeIndex;
        String  attributeName;

        // Load the number of attributes

        attributeCount = instream.readUnsignedShort();

        if (attributeCount < 0)
            throw new ClassFormatError("Invalid number of class file attributes found; value = " + attributeCount);

        _attributeCount = (short)attributeCount;


        // Load the attributes

        if (attributeCount > 0)
        {
            _attributes = new AttributeInfo[attributeCount];

            // LOOP : 
            //  Load and create each attribute from the input stream

            for (int k=0; k<attributeCount; k++)
            {
                // Read the attribute name index to find out type

                attributeIndex = instream.readUnsignedShort();


                // Verify the attribute name index is valid

                if (pool.IsThisAConstantPoolIndex(attributeIndex) == false)
                    throw new ClassFormatError("Invalid attribute name index found in class file attribute;" +
                                               " value = " + attributeIndex);


                // Verify the attribute name index refers to a CONSTANT_Utf8

                if (pool.VerifyIndexIsConstantType(attributeIndex,ConstantPoolInfo.CONSTANT_Utf8) == false)
                    throw new ClassFormatError("Attribute name index for class file attribute is not CONSTANT_Utf8;" +
                                               " index = " + attributeIndex);


                // Obtain the name of the attribute (predefined attributes are reserved by the class file spec)
                // Then create the appropiate attribute object

                attributeName = pool.GetStringForUtf8Constant(attributeIndex);

                AttributeInfo  attr;

                if (attributeName.compareTo("SourceFile") == 0)
                    attr = (AttributeInfo) new SourceFileAttribute(attributeIndex);

                else if (attributeName.compareTo("InnerClasses") == 0)
                    attr = (AttributeInfo) new InnerClassAttribute(attributeIndex);
                else
                    attr = (AttributeInfo) new GenericAttribute(attributeIndex);


                if (attr.Load(instream,pool) == false)
                {
                    System.err.println("Attribute load failure occurred");
                    return false;
                }

                _attributes[k] = attr;

            } // for

        } // if

        return true;

    } // LoadAttributes

    /**
     * Method OutputInfo()
     *      Output information about the contents of a ClassFile object
     */
    public boolean OutputInfo(int options)
    {
        System.out.println("\n\nINFORMATION ABOUT CLASS FILE \"" + _className + "\"\n" +
                           "=============================================================");


        ////////////////////////////////////////////////
        // If user chose class file format, do it first
        ////////////////////////////////////////////////

        if ((options & OUTPUT_FILEFORM) != 0)
        {
            if (!OutputInFileFormat()) return false;
        }


        ///////////////////////////////
        // Output version information
        ///////////////////////////////

        if ((options & OUTPUT_VERSION) != 0)
        {
            System.out.println("\nVERSION INFORMATION:\n--------------------");
            System.out.println("Magic number  = " + _Magic);
            System.out.println("Minor version = " + _MinorVersion);
            System.out.println("Major version = " + _MajorVersion);
        }


        ////////////////////////////////////
        // Output constant pool information
        ////////////////////////////////////

        if ((options & (OUTPUT_CONSTANTS_INDEX | OUTPUT_CONSTANTS_GROUPS)) != 0)
        {
            // Output constants by index and/or group

            if ((options & OUTPUT_CONSTANTS_INDEX) != 0)  
            {
                System.out.println("\n\nCONSTANT POOL - ITEMS SORTED BY INDEX\n" +
                                   "-------------------------------------");
                _ConstantPool.PrintByIndex();
            }

            if ((options & OUTPUT_CONSTANTS_GROUPS) != 0) 
            {
                System.out.println("\n\nCONSTANT POOL - ITEMS SORTED BY CONSTANT TYPE\n" +
                                   "---------------------------------------------");
                _ConstantPool.PrintByGroup();
            }

        } // if - output constants


        //////////////////////////////
        // Output access information
        //////////////////////////////

        if ((options & OUTPUT_ACCESS) != 0)
        {
            System.out.println("\n\nACCESS FLAGS:\n-------------");

            if ((_accessFlags & A_PUBLIC) != 0)     System.out.print("PUBLIC ");
            if ((_accessFlags & M_FINAL) != 0)      System.out.print("FINAL ");
            if ((_accessFlags & M_SUPER) != 0)      System.out.print("SUPER ");
            if ((_accessFlags & M_INTERFACE) != 0)  System.out.print("INTERFACE ");
            if ((_accessFlags & M_ABSTRACT) != 0)   System.out.print("ABSTRACT ");
            System.out.println("");
        }


        ///////////////////////////////////
        // Output inheritance information
        ///////////////////////////////////

        if ((options & OUTPUT_INHERITANCE) != 0)
        {
            System.out.println("\n\nINHERITANCE INFO:\n-----------------");
            System.out.println("\"This\" class : index " + _thisClass +
                               " (class " + _ConstantPool.GetStringForClassConstant(_thisClass) + ")" );
            System.out.println("\"Super\" class : index " + _superClass +
                               " (class " + _ConstantPool.GetStringForClassConstant(_superClass) + ")" );
        }


        /////////////////////////////////
        // Output interface information
        /////////////////////////////////

        if ((options & OUTPUT_INTERFACE) != 0)
        {
            System.out.println("\n\nINTERFACE INFO:\n---------------");
            System.out.println("Number of interfaces = " + _interfacesCount);

            if (_interfacesCount > 0)
            {
                System.out.println("\nInterfaces by Index :");

                for (int k=0; k<_interfacesCount; k++)
                {
                    System.out.print("Interface # " + k + " - index " + _interfaces[k]);
                    System.out.println(" (interface " + _ConstantPool.GetStringForClassConstant(_interfaces[k]) + ")" );
                }
            }
        }


        /////////////////////////////////
        // Output field information
        /////////////////////////////////

        if ((options & OUTPUT_FIELDS) != 0)
        {
            System.out.println("\n\nFIELD INFO:\n-----------");
            System.out.println("Number of fields = " + _fieldsCount);

            if (_fieldsCount > 0)
            {
                System.out.println("\nFields by Index :");

                for (int k=0; k<_fieldsCount; k++)
                {
                    System.out.println("Field # " + k + " - ");
                    _fields[k].print();
                    System.out.println("");
                }
            }
        }


        /////////////////////////////////
        // Output method information
        /////////////////////////////////

        if ((options & OUTPUT_METHODS) != 0)
        {
            System.out.println("\n\nMETHOD INFO:\n-----------");
            System.out.println("Number of methods = " + _methodsCount);

            if (_methodsCount > 0)
            {
                System.out.println("\nMethods by Index :");

                for (int k=0; k<_methodsCount; k++)
                {
                    System.out.println("Method # " + k + " - ");
                    _methods[k].print();
                    System.out.println("");
                }
            }
        }


        /////////////////////////////////
        // Output method information
        /////////////////////////////////

        if ((options & OUTPUT_ATTRIBUTES) != 0)
        {
            System.out.println("\n\nATTRIBUTES:\n-----------");
            System.out.println("Number of attributes = " + _attributeCount);

            if (_attributeCount > 0)
            {
                System.out.println("\nAttributes by Index :");

                for (int k=0; k<_attributeCount; k++)
                {
                    System.out.println("Attribute # " + k + " - ");
                    _attributes[k].print();
                    System.out.println("");
                }
            }
        }

        return true;

    } // OutputInfo

    /** Output information about the contents of a ClassFile object in "Classfile" format */
    public boolean OutputInFileFormat()
    {
        short  accessFlags;

        // Access flags for class

        System.out.println("");

        if ((_accessFlags & A_PUBLIC) != 0)     System.out.print("public ");
        if ((_accessFlags & M_FINAL) != 0)      System.out.print("final ");
        if ((_accessFlags & M_SUPER) != 0)      System.out.print("super ");
        if ((_accessFlags & M_INTERFACE) != 0)  System.out.print("interface ");
        if ((_accessFlags & M_ABSTRACT) != 0)   System.out.print("abstract ");


        // Class name & base class

        System.out.print(_ConstantPool.GetStringForClassConstant(_thisClass));
        System.out.print(" extends " + _ConstantPool.GetStringForClassConstant(_superClass));
        System.out.println("\n{");


        // Class fields

        System.out.println("    // Fields\n");
        for (int k=0; k<_fieldsCount; k++)
        {
            _fields[k].print();

        }


        // Class methods

        System.out.println("\n    // Methods\n");
        for (int k=0; k<_methodsCount; k++)
        {
            _methods[k].print();
        }

        // Class attributes

        System.out.println("\n    // Attributes\n");
        for ( int k=0; k<_attributeCount; k++ ) {
            _attributes[k].print();
        }

        return true;
    } // OutputInFileFormat
} // ClassFile
