package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;

/**
 * Represents an entry in Local variable table.
 * All file contents Copyright (C) 1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the LocalVariableTableEntry class.
 *      The class is responsible for representing a LocalVariableTableEntry for
 *      a LocalVariableTableAttribute, read froma java .class file.
 *      Current implementation uses Version 1.1.3 of the Java Virtual Machine (JVM) spec.
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 1/1/97.
 *
 * @author Don Lance -- <i>Created</i>
 * @author Huaxing Wu -- <i>Revised 3/21/02. Add new constructor and remove obsolete methods (getNameIndex and getDescriptorIndex) </i>
 * @author Jay Lofstead 2003/05/29 added a toString for generating XML
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 */
public class LocalVariableTableEntry implements Cloneable, java.io.Serializable
{
    ////////////////
    // Attributes //
    ////////////////

    /** Index into code where local var is first defined */
    private short   _start_pc;

    /** Offset from start_pc where local var definition ends */
    private short   _length;

    /** Constant pool index to local var name */
    private short   _name_index;

    /** Constant pool index to local var descriptor */
    private short   _descriptor_index;

    /** Location in its method's local vars */
    private short   _index;

    /** Copy of constant pool string for m_nameString */
    private String  _nameString;

    /** Copy of constant pool string for m_descriptor_index */
    private String  _descriptorString;

    /////////////
    // Methods //
    /////////////

    // Constructor
    /** ??? */
    public LocalVariableTableEntry()
    {
        _start_pc = _length = _name_index = _descriptor_index = _index = 0;
    }

    /** ??? */
    public LocalVariableTableEntry(short startPC, short length, short index,
				   String name, String type)
    {
	_start_pc = startPC;
	_length = length;
	_index = index;
	_nameString = name;
	_descriptorString = type;
    }

    /////////////////////
    // Accessor methods
    /////////////////////

    /** ??? */
    public short  getStartPC()          { return _start_pc;                     }

    /** ??? */
    public short  getLength()           { return _length;                       }

    /** ??? */
    public short  getIndex()            { return _index;                        }

    /** ??? */
    public String getNameString()       { return (_nameString == null)       ? null : new String(_nameString);       }

    /** ??? */
    public String getDescriptorString() { return (_descriptorString == null) ? null : new String(_descriptorString); }

    /** Method loads a LocalVariableTableEntry object from an input stream */
    public boolean Load(DataInput instream, ConstantPool pool, int codeLength) throws IOException
    {
        // Load & verify the starting index for the local var


        int startpc = instream.readUnsignedShort();

        if (startpc >=codeLength)
            throw new ClassFormatError("Starting index for local variable table entry is invalid; value = " + startpc);

        _start_pc = (short)startpc;


        // Load & verify the offset of the local var definition

        int length = instream.readUnsignedShort();

        if ((startpc+length) > codeLength)
            throw new ClassFormatError("Offset for definition of local variable table entry is invalid;" +
                                       "   start_pc = " + startpc + ", value = " + length + ", code length = " + codeLength);

        _length = (short)length;


        // Load & verify the local var name index
        // Index must be a valid index into the constant pool
        // Constant pool entry at the name index should be a CONSTANT_Utf8

        int nameIndex = instream.readUnsignedShort();

        if (pool.IsThisAConstantPoolIndex(nameIndex) == false)
            throw new ClassFormatError("Name index for LocalVariableTable entry is invalid; value = " + nameIndex);

        if (pool.VerifyIndexIsConstantType(nameIndex,ConstantPoolInfo.CONSTANT_Utf8) == false)
            throw new ClassFormatError("Name index for LocalVariableTable entry doesn't refer to a CONSTANT_Utf8;" +
                                       " index = " + nameIndex);

        _name_index = (short)nameIndex;
        _nameString = pool.GetStringForUtf8Constant(nameIndex);


        // Load & verify the local var descriptor index
        // Index must be a valid index into the constant pool
        // Constant pool entry at the descriptor index should be a CONSTANT_Utf8

        int descriptorIndex = instream.readUnsignedShort();

        if (pool.IsThisAConstantPoolIndex(descriptorIndex) == false)
            throw new ClassFormatError("Descriptor index for LocalVariableTable entry is invalid; value = " + descriptorIndex);

        if (pool.VerifyIndexIsConstantType(descriptorIndex,ConstantPoolInfo.CONSTANT_Utf8) == false)
            throw new ClassFormatError("Descriptor index for LocalVariableTable entry doesn't refer to a CONSTANT_Utf8;" +
                                       " index = " + descriptorIndex);

        _descriptor_index = (short)descriptorIndex;
        _descriptorString = pool.GetStringForUtf8Constant(descriptorIndex);


        // Load & store the index

        _index = (short)instream.readUnsignedShort();

        return true;
    } // Load

    /** Output all information about this object */
    public boolean print()
    {
        System.out.println("Local variable table entry : ");
        System.out.println("   StartPC = " + _start_pc + ", Length = " + _length + ", Index = " + _index);
        System.out.println("   Name index = " + _name_index + " (" + _nameString + ") ");
        System.out.println("   Descriptor index = " + _descriptor_index + " (" + _descriptorString + ") ");
        return true;

    } // print

	/**
	 * returns an XML representation of this object
	 */
	public String toString ()
	{
		return "<localvariabletableentry><startpc>" + _start_pc + "</startpc><length>" + _length + "</length>"
			+ "<index>" + _index + "</index><nameindex>" + _name_index + "</nameindex><namestring>" + _nameString + "</namestring>"
			+ "<descriptorindex>" + _descriptor_index + "</descriptorindex><descriptorstring>" + _descriptorString
			+ "</descriptorstring></localvariabletableentry>"
			;
	}
} // LocalVariableTableEntry
