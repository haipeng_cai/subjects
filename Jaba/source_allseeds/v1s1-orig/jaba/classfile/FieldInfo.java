package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;

/**
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the FieldInfo class.
 * 
 * Revision History : 
 *      Initial version, released 12/18/96.
 * @author Jay Lofstead 2003/05/22 simplified the code removing all of the legacy code from the clone calls.
 * 					Removed all of the commented out code to aid in readability.
 * 					Added isPrivate, getAccessLevel, getModifiers, isSynthetic methods.
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/10 changed to an interface
 */
public interface FieldInfo extends Cloneable, java.io.Serializable
{
	/** ??? */
	public short  getAccessFlags();

	/** ??? */
	public short  getNameIndex();

	/** ??? */
	public short  getDescriptorIndex();

	/** ??? */
	public short  getAttributeCount();

	/** ??? */
	public String getNameString();

	/** ??? */
	public String getDescriptorString();

	/** ??? */
	public AttributeInfo getAttribute(int index) throws IndexOutOfBoundsException;

	/** indicates if this is a private field or not */
	public boolean isPrivate ();

	/** returns the access level for this FieldInfo object */
	public short getAccessLevel ();

	/** returns the access level for this ClassFile object */
	public short getModifiers ();

	/** returns if this is a synthetic field or not */
	public boolean isSynthetic ();

	/**
	 * Load a FieldInfo object from a input stream attached to a .class file
	 *  We require the constant pool as a parameter to use in error checking
	 */
	public boolean Load(DataInput instream, ConstantPool pool) throws IOException;

	/** Output all information about a field */
	public boolean print();
} // FieldInfo
