package jaba.classfile;

import java.io.DataInput;
import java.io.IOException;

/**
 * Module : ConstantValueAttribute.java
 * 
 * All file contents Copyright (C) 1996,1997 by Don Lance. All Rights Reserved.
 * 
 * Author : Don Lance, MTSU Computer Science graduate program
 * 
 * Description :
 *      This file contains the implementation of the ConstantValueAttribute class.
 *      The class is responsible for representing a ConstantValue attribute from
 *      a java .class file.  Current implementation uses Version 1.1.3 of
 *      the Java Virtual Machine (JVM) spec.
 * 
 *      Objects of this class are cloneable.
 * 
 * Revision History : 
 *      Initial version, released 12/19/96.
 * @author Jay Lofstead 2003/05/29 added a toString method for generating XML
 * @author Jay Lofstead 2003/06/05 fixed up some of the JavaDoc
 */
public class ConstantValueAttribute extends AttributeInfoImpl implements Cloneable
{
    ////////////////
    // Attributes //
    ////////////////

    /** Constant pool index to a constant value */
    private short   _constantValueIndex;
    
    /** Type of the constant (CONSTANT_Long, CONSTANT_Float, etc.) */
    private byte    _constantType;

    /** For long constants */
    private long    _longValue;

    /** For float constants */
    private float   _floatValue;

    /** For double constants */
    private double  _doubleValue;

    /** For int, short, char, byte, and boolean constants */
    private int     _intValue;

    /** For String constants */
    private String _stringValue;

    /////////////
    // Methods //
    /////////////

    // Constructor

    /** ??? */
    public ConstantValueAttribute(int index)
    {
        _attributeNameIndex = (short)index;
    }

    /** Provide a clone of objects of this class */
    public Object clone()
    {
        try
        {
            return super.clone();
        }

        catch (CloneNotSupportedException e)
        {
            // This shouldn't happen since we're cloneable
            System.err.println("CloneNotSupportedException OCCURRED IN ConstantValueAttribute::Clone - " + e.getMessage());
            return null;
        }

    } // clone

    /** Method loads a ConstantValue attribute object from an input stream
     *  Method assumes that the attribute name index has already been read;
     *  stream is positioned to read attribute length
     *  We need the constant pool as a parameter to verify the constant value index
     */
    public boolean Load(DataInput instream, ConstantPool pool) throws IOException
    {
        // Load the attribute info length & verify

        int length = instream.readInt();

        if (length < 0)
            throw new ClassFormatError("Invalid length for ConstantValue attribute; length = " + length);

        _attributeLength = length;


        // Verify attribute length; a ConstantValue attribute length can only be 2

        if (length != 2)
            throw new ClassFormatError("ConstantValue attribute length in class file not equal to 2; value = " + length);


        // Load the attribute information

        int constantValueIndex = instream.readUnsignedShort();


        // Verify the attribute name index is valid

        if (pool.IsThisAConstantPoolIndex(constantValueIndex) == false)
            throw new ClassFormatError("Invalid constant value index found in field; value = " + constantValueIndex);

        _constantValueIndex = (short)constantValueIndex;


        // Now obtain the actual constant value & store a copy of it

        ConstantPoolInfo  poolEntry = pool.getConstantPoolEntry(constantValueIndex);

        if (poolEntry != null)
        {
            byte tagValue = poolEntry.getTagValue();

            switch(tagValue)
            {
                case ConstantPoolInfo.CONSTANT_Long:
                {
                    _constantType = ConstantPoolInfo.CONSTANT_Long;
                    _longValue    = poolEntry.getLong();
                    break;
                }

                case ConstantPoolInfo.CONSTANT_Float:
                {
                    _constantType = ConstantPoolInfo.CONSTANT_Float;
                    _floatValue   = poolEntry.getFloat();
                    break;
                }

                case ConstantPoolInfo.CONSTANT_Double:
                {
                    _constantType = ConstantPoolInfo.CONSTANT_Double;
                    _doubleValue  = poolEntry.getDouble();
                    break;
                }

                case ConstantPoolInfo.CONSTANT_Integer:
                {
                    _constantType = ConstantPoolInfo.CONSTANT_Integer;
                    _intValue     = poolEntry.getInteger();
                    break;
                }

                // added following case  -- S. Sinha 12-22-98
                case ConstantPoolInfo.CONSTANT_String:
                {
                    _constantType = ConstantPoolInfo.CONSTANT_String;
                    _stringValue = pool.GetStringForUtf8Constant(
                        poolEntry.getStringIndex() );
                    break;
                }

                default:
                {
                    throw new ClassFormatError("Constant value index for ConstantValue attribute doesn't refer to a valid type;" +
                                               "   index = " + constantValueIndex + ", type = " + tagValue);
                }
            } // switch
        } // if - valid pool entry
        else
        {
            System.err.println("Found invalid constant pool entry (null) at constant value index = " + constantValueIndex);
            return false;
        }

        return true;
    } // Load

    /** Output all information about this attribute */
    public boolean print()
    {
        System.out.print("                   CONSTANTVALUE ATTRIBUTE : ");
        System.out.print("Constant value index = " + _constantValueIndex);
        
        switch (_constantType)
        {
            case ConstantPoolInfo.CONSTANT_Long:
            {
                System.out.println(" (Long value = " + _longValue);
                break;
            }

            case ConstantPoolInfo.CONSTANT_Float:
            {
                System.out.println(" (Float value = " + _floatValue);
                break;
            }

            case ConstantPoolInfo.CONSTANT_Double:
            {
                System.out.println(" (Double value = " + _doubleValue);
                break;
            }

            case ConstantPoolInfo.CONSTANT_Integer:
            {
                System.out.println(" (Integer value = " + _intValue);
                break;
            }

            case ConstantPoolInfo.CONSTANT_String:
            {
                System.out.println(" (String value = " + _stringValue);
                break;
            }

            default:
            {
                System.out.println("Unknown type; value = 0");
                break;
            }
        } // switch

        return true;
    } // print

	/**
	 * returns an XML representation of the object
	 */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<constantvalueattribute>");

		buf.append ("<index>").append (_constantValueIndex).append ("</index>");

		switch (_constantType)
		{
			case ConstantPoolInfo.CONSTANT_Long:
				buf.append ("<type>long</type>");
				buf.append ("<value>" + _longValue + "</value>");
				break;

			case ConstantPoolInfo.CONSTANT_Float:
				buf.append ("<type>float</type>");
				buf.append ("<value>" + _floatValue + "</value>");
				break;

			case ConstantPoolInfo.CONSTANT_Double:
				buf.append ("<type>double</type>");
				buf.append ("<value>" + _doubleValue + "</value>");
				break;

			case ConstantPoolInfo.CONSTANT_Integer:
				buf.append ("<type>integer</type>");
				buf.append ("<value>" + _intValue + "</value>");
				break;

			case ConstantPoolInfo.CONSTANT_String:
				buf.append ("<type>string</type>");
				buf.append ("<value>" + _stringValue + "</value>");
				break;

			default:
				buf.append ("<type>unknown</type>");
				buf.append ("<value>0</value>");
				break;
		}

		buf.append ("</constantvalueattribute>");

		return buf.toString ();
	}
} // ConstantValueAttribute
