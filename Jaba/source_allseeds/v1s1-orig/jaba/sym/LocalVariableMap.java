/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import java.util.HashMap;

/**
 * Creates a map from an index in the local-variable table to an entry in the
 * symbol table.
 *
 * @see jaba.classfile.LocalVariableTableAttribute
 * @see jaba.sym.LocalVariable
 *
 * @author S. Sinha -- <i>Created</i>
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 */
public class LocalVariableMap implements MethodAttribute, java.io.Serializable
{

    /** HashMap from an index in the local-variable table to an entry in
        the symbol table. */
    private HashMap map;

    /**
     * Creates an empty <code>LocalVariableMap</code> object.
     */
    public LocalVariableMap() {
        map = new HashMap();
    }

    /**
     * Returns the local variable that maps to a given integer key.
     * Returns <code>null</code> if <code>key</code> is not mapped to
     * a local variable.
     * @param key key to retrieve mapping for.
     * @return variable that maps to <code>key</code>, or <code>null</code> if
     *         if <code>key</code> maps to no variable.
     * @throws IllegalArgumentException if <code>key</code> is
     *                                  <code>null</code>.
     */
    public LocalVariable get( Integer key ) throws IllegalArgumentException {
        if ( key == null ) {
            throw new IllegalArgumentException( "LocalVariableMap.get(): "+
                "key is null" );
        }
        if ( !map.containsKey( key ) ) {
            return null;
        }
        return (LocalVariable)map.get( key );
    }

    /**
     * Maps the given integey key to the given local variable.
     * Replaces any existing mapping for key.
     * @param key key for mapping.
     * @param var variable to map <code>key</code> to.
     * @throws IllegalArgumentException if either <code>key</code> or
     *                                  <code>var</code> is <code>null</code>.
     */
    public void put( Integer key, LocalVariable var )
                throws IllegalArgumentException
    {
        if ( key == null || var == null ) {
            throw new IllegalArgumentException( "LocalVariableMap.get(): "+
                "key or var is null" );
        }
        map.put( key, var );
    }

    /**
     * Returns <code>true</code> if this map contains no key-value mappings.
     * @return <code>true</code> if this map contains no key-value mappings.
     */
    public boolean isEmpty() {
        return map.isEmpty();
    }

}
