/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Represents an array in the symbol table.
 * @author Jim Jones
 * @author Jay Lofstead 2003/06/12 code cleanup
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface Array extends ReferenceType
{
  /**
   * Sets the type of the elements of the array.
   * @param elementType   type of the elements of the array.
   * @exception IllegalArgumentException   thrown if elementType is
   *                                       null
   */
  public void setElementType( TypeEntry elementType ) throws IllegalArgumentException;

  /**
   * Returns the type of the elements of this array.
   * @return   type of the elements of this array
   */
  public TypeEntry getElementType();

  /**
   * Returns the non-array element type. The non-array element type appears
   * at the leaf of the recursive array structure for multi-dimensional
   * arrays.
   * @return Leaf element type of array.
   */
  public TypeEntry getLeafElementType();

  /**
   * Returns the number of dimensions of array.
   * @return number of dimensions of array.
   */
  public int getDimensions();

    /**
     * Sets the superclass of this array (which is always java.lang.Object).
     * @param superClass  The object which represents java.lang.Object.
     */
    public void __setSuperClass( Class superClass );

    /**
     * Returns the superclass of this array (which is always java.lang.Object).
     * @return The superclass of this array (which is always java.lang.Object).
     */
    public Class getSuperClass();

  /**
   * Initializes leaf element type and array dimensions by recursively
   * traversing the recursive array structure. This method is invoked
   * only on the first call to either <code>getLeafElementType()</code> or
   * <code>getDimensions()</code> on an <code>Array</code> object; the
   * method saves the computed values in fields - <code>leafElementType</code>
   * and <code>dimensions</code>. Subsequent calls to either of the
   * methods simply return the field values.  Also sets name of this array
   * and all subarrays.
   */
  public void __initializeLeafTypeAndDimensions();

  /**
   * Returns a string for this object.
   * @return  String representing this object.
   */
  public String toString();
}
