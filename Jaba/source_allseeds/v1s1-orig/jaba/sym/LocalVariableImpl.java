/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import jaba.du.HasValue;
import jaba.constants.Modifier;

/**
 * Class that represents a local variable entry into the symbol table.
 * @author Jay Lofstead 2003/05/22 fixed a possible null pointer exception in toString if type was null
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/30 added implements LocalVariable
 * @author Jay Lofstead 2003/07/01 changed to implement LocalVariable
 */
public class LocalVariableImpl extends SymbolEntryImpl implements HasValue, Modifier, Cloneable, LocalVariable
{
    // Constructor

    /**
     * Creates a new local variable object.
     */
    public LocalVariableImpl ()
    {
        type = null;
        containingMethod = null;
        modifiers = 0;
    }

    // Methods

    /**
     * Sets the type for this variable.
     * @param t Type of variable.
     */
    public void setType( TypeEntry t ) {
        type = t;
    }

    /**
     * Sets the containing method for this variable.
     * @param m Containing method.
     */
    public void setContainingMethod( Method m ) {
        containingMethod = m;
    }

    /**
     * Sets the modifier flags for this variable.
     * @param m Bit-mask for modofier flags for this variable.
     */
    public void setModifiers( int m ) {
        modifiers = m;
    }

    /**
     * Returns the type method for this variable..
     * @return Type of this variable..
     */
    public TypeEntry getType() {
        return type;
    }

    /**
     * Returns the containing method for this variable..
     * @return Containing method of this variable..
     */
    public Method getContainingMethod() {
        return containingMethod;
    }

    /**
     * Returns the bit-mask for the modifiers for this variable.
     * @return Modifiers for this variable.
     */
    public int getModifiers() {
        return modifiers;
    }

  /**
   * Returns a string for this object.
   * @return  String representing this object.
   */
	public String toString ()
	{
		StringBuffer buffer = new StringBuffer ();

		buffer.append ("<localvariable>");

		buffer.append ("<id>" + getID () + "</id>");
		buffer.append ("<name>" + jaba.tools.Util.toXMLValid (getName ()) + "</name>");
		buffer.append ("<containingmethod>" + containingMethod.getID () + "</containingmethod>");
		if ((modifiers & M_FINAL) != 0)
		{
			buffer.append ("<modifiers>final</modifiers>");
		}

		if (type != null)
		{
			buffer.append( "<type>" + type.getID () + "</type>");
		}

		buffer.append ("</localvariable>");

		return buffer.toString ();
	}

  /**
   * Clones this object.
   * @return Clone of this object.
   */
  public Object clone()
    {
      LocalVariable newVar = null;
      try
	{
	  newVar = (LocalVariable)super.clone();
	}catch ( CloneNotSupportedException e){
	    e.printStackTrace();
	    throw new RuntimeException( e );
	}

      return newVar;
    }

    // Fields

    /** Type of variable. */
    private TypeEntry type;

    /** Method which contains this variable. */
    private Method containingMethod;

    /** Modifier flags for variable. */
    private int modifiers;
}
