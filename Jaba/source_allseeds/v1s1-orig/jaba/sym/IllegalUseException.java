package jaba.sym;

public class IllegalUseException extends RuntimeException {

    IllegalUseException(boolean b1, boolean b2, boolean b3) {
	super("Status: " + b1 + ", " + b2 + ", " + b3);
    }
}
