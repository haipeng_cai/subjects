/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import java.util.Vector;
import java.util.Enumeration;
import java.util.Stack;

/**
 * Class that represents a package entry into the symbol table.
 * @author Jay Lofstead 2003/06/05 added a toString method.
 * @author Jay Lofstead 2003/06/05 fixed to work for root packages
 * @author Jay Lofstead 2003/06/30 added implements Package
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface Package extends SymbolEntry
{
    /**
     * Adds a package to the set of packages contained in this package.
     * @param p Package to be added.
     */
    public void addPackage( Package p );

    /**
     * Adds a class to the set of classes contained in this class.
     * @param c Class to be added.
     */
    public void addClass( Class c );

    /**
     * Adds an interface to the set of interfaces contained in this package.
     * @param i Interface to be added.
     */
    public void addInterface( Interface i );

    /**
     * Sets the containing package for this package.
     * @param p Containing package.
     */
    public void setContainingPackage( Package p );

    /**
     * Returns an eumeration of the packages contained within this package.
     * @return Enumeration of contained packages.
     */
    public Enumeration getPackages();

    /**
     * Returns an eumeration of the classes contained within this package.
     * @return Enumeration of contained classes.
     */
    public Enumeration getClasses();

    /**
     * Returns an eumeration of the interfaces contained within this package.
     * @return Enumeration of contained interfaces.
     */
    public Enumeration getInterfaces();

    /**
     * Returns the containing package for this package.
     * @return Containing package of this package.
     */
    public Package getContainingPackage();

	/** returns an XML format of this package */
	public String toString ();
}
