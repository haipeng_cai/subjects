/* Copyright (c) 2002, Georgia Institute of Technology */

package jaba.sym;

import java.util.HashMap;
import java.util.Vector;
import java.util.Enumeration;

import java.io.IOException;
import java.io.File;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Represents a source file.  Can be used if <i>SourceClassPath</i> is
 * defined in the ResourceFile.
 * @author Jim Jones -- <i>Created.  September 9, 2002</i>
 * @author Jay Lofstead 2003/06/02 changed elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/05 fixed some of the JavaDoc
 * @author Jay Lofstead 2003/06/05 changed forName to check the directory structure as well as the default directory
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/16 changed toString to output XML valid text
 * @author Jay Lofstead 2003/07/10 changed to implement SourceFile
 */
public class SourceFileImpl implements SourceFile, java.io.Serializable
{
    /**
     * Constructor.
     */
    private SourceFileImpl (String name, NamedReferenceType type) 
	throws IOException
    {
	InputStream inputStream = 
	    ((jaba.sym.Program) type.getProgram ()).getResourceFile ().getSourceInputStream (name);
	BufferedReader in = 
	    new BufferedReader (new InputStreamReader (inputStream));
	namedReferenceTypes = new Vector ();
	namedReferenceTypes.add (type);
	addNamedReferenceType (type);
	this.name = name;
	lines = new Vector ();
	String inLine;
	do {
	    inLine = in.readLine ();
	    if (inLine == null)
		break;
	    lines.add (inLine);
	} while (inLine != null);
	nameMap.put (name, this);

	attributes = new Vector ();
    }

    /**
     * Returns the specified line from the file.  The first line is
     * line 1.  If a line is requested that is beyond the number of
     * lines in the program, then an ArrayOutOfBounds exception will
     * be thrown.
     * @param line  The number of the line to be returned.
     * @return The specified line from the file.
     */
    public String getLine( int line ) {
	return (String)lines.elementAt( line-1 );
    }

    /**
     * Returns the name of the file using the full package name/class
     * name with File.separatorChar between directory/packages, and
     * without the ".java" extension -- example: jaba/sym/SourceFile.
     * Note, this may not be the same name as the class (a class may
     * be defined in a file named differently).
     * @return The name of the file.
     */
    public String getName() {
	return name;
    }

    /**
     * Returns the number of lines in the source file.  Remember that
     * the lines are numbered 1 to N rather than 0 to N-1.
     * @return The number of lines in the source file.
     */
    public int getNumLines() { 
	return lines.size();
    }
    
    /**
     * Returns the set of classes/interfaces that are defined in this
     * source file.  Note that this may not be an exhaustive list of
     * all classes and interfaces that are defined in this source
     * file.  Classes and interfaces are added to this list as
     * NamedReferenceType.getSourceFile() is called.
     * @return The set of classes/interfaces that are defined in this
     * source file.
     */
    public Vector getNamedReferenceTypesVector ()
    {
	return namedReferenceTypes;
    }

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getNamedReferenceTypesVector</code>
     * @return The set of classes/interfaces that are defined in this
     * source file.
     */
	public NamedReferenceType [] getNamedReferenceTypes ()
	{
		return (NamedReferenceType []) namedReferenceTypes.toArray (new NamedReferenceType [namedReferenceTypes.size ()]);
	}

    /**
     * Returns all of the lines in the source file.  Each element of
     * the array, in ascending order, contains the entire text of the
     * source file (except the newlines between the lines).
     * @return An array of all of the lines in the source file.
     */
    public Vector getLinesVector ()
    {
	return lines;
    }

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getLinesVector</code>
     * @return An array of all of the lines in the source file.
     */
	public String [] getLines ()
	{
		return (String []) lines.toArray (new String [lines.size ()]);
	}

    /**
     * Returns the text of the file as one long string.
     * @return The text of the source file.
     */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();
		int i = 1;

		buf.append ("<sourcefile>");
		buf.append ("<name>").append (jaba.tools.Util.toXMLValid (name)).append ("</name>");
		for (Enumeration e = lines.elements (); e.hasMoreElements (); i++)
		{
			buf.append ("<line number=\"");
			buf.append (i);
			buf.append ("\">");
			buf.append (jaba.tools.Util.toXMLValid ((String) e.nextElement ()));
			buf.append ("</line>");
		}
		buf.append ("</sourcefile>");

		return buf.toString ();
	}

    /**
     * Returns a SourceFile object for the name supplied.  If a
     * SourceFile object already exists matching the source file name,
     * that instance is returned; otherwise a new one is created (this
     * happens when more than one type is defined in the same file).
     * @param type Class or interface for which we are looking for the
     * SourceFile
     * @return A (or <i>the</i>) SourceFile object with a matching
     * name.
     */
	public static SourceFile forName( NamedReferenceType type ) throws IOException
	{
		String name = type.getName ();
		String sourceFileName = type.getSourceFileName ();
		// strip off the package spec and look for that first
		name =   name.substring (0, name.lastIndexOf (File.separatorChar) + 1)
		       + sourceFileName.substring (0, sourceFileName.lastIndexOf (".java"));
		SourceFile srcFile = (SourceFile) nameMap.get (name);
		if (srcFile != null)
		{
			srcFile.addNamedReferenceType (type);
		}
		else
		{
			try
			{
				srcFile = new SourceFileImpl ( name, type );
			}
			catch (IOException ioe)
			{
				// assuming that it may be in the package directories
				// instead of the root directory.  Check there next.
				srcFile = new SourceFileImpl (type.getName (), type);
			}
		}

		return srcFile;
	}

	/** ??? */
    public void addNamedReferenceType( NamedReferenceType type ) {
	namedReferenceTypes.add( type );
    }

   /**
    * Adds an attribute to this SourceFile.
    * @param attribute attribute to add to this SourceFile.
    */
    public void addAttribute( SourceFileAttribute attribute ) {
	if ( attribute == null ) {
	    throw new IllegalArgumentException( "SourceFile.addAttribute(): " +
						"attribute is null" );
	}
	attributes.addElement( attribute );
    }

    /**
     * Returns the attributes that have been associated with this SourceFile.
     * @return The attributes that have been associated with this SourceFile.
     */
    public Vector getAttributesVector ()
    {
	return attributes;
    }

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getAttributesVector</code>
     * @return The attributes that have been associated with this SourceFile.
     */
	public SourceFileAttribute [] getAttributes ()
	{
		return (SourceFileAttribute []) attributes.toArray (new SourceFileAttribute [attributes.size ()]);
	}

    /** Set of classes/interfaces defined in this source file. */
    private Vector namedReferenceTypes;

    /** 
     * Name of this file using the full package name/class name with
     * File.separatorChar between directory/packages, and without the
     * ".java" extension -- example: jaba/sym/SourceFile.  Note, this
     * may not be the same name as the class (a class may be defined
     * in a file named differently).
     */
    private String name;

    /** Set of lines in the file (defined by newlines). */
    private Vector lines;

    /** Mapping from name to instance. */
    private static HashMap nameMap = new HashMap();

    /** Attributes of this SourceFile -- provided for extensibility. */
    private Vector attributes;
}
