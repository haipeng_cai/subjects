/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import jaba.classfile.ClassFile;
import jaba.classfile.ClassFileImpl;
import jaba.classfile.LocalVariableTableAttribute;
import jaba.classfile.ConstantPool;
import jaba.classfile.ConstantPoolInfo;
import jaba.classfile.FieldInfo;
import jaba.classfile.MethodInfo;
import jaba.classfile.AttributeInfo;
import jaba.classfile.ExceptionsAttribute;
import jaba.classfile.CodeAttribute;
import jaba.classfile.LocalVariableTableEntry;
import jaba.classfile.InnerClassAttribute;
import jaba.classfile.InnerClassEntry;

import jaba.debug.Debug;

import jaba.sym.Program;
import jaba.sym.Primitive;

import jaba.main.ResourceFileI;
import jaba.main.Options;

import jaba.constants.Modifier;
import jaba.constants.AccessLevel;

import jode.jvm.CstrLocalVariableTable;

import java.util.Collection;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Vector;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Stack;
import java.util.NoSuchElementException;
import java.util.Arrays;
import java.util.List;

/**
 * Implements a symbol table. A symbol table is a {@link java.util.HashSet
 * HashSet} that contains objects of type
 * {@link jaba.sym.SymbolTableEntry SymbolTableEntry}.
 * @author Huaxing Wu -- <i>Revised 3/21/02 Change methods to use ResouceFile Object
 * 			instead of Classpaths when loading library to take advantage
 * 			of ReousrceFile's supporting methods </i>
 * @author Huaxing Wu -- <i> Revised 8/12/02 Change the way of resolving 
 *          Primitive type to take advantage of the fact that Primitive class 
 *          is immutual and there is only few Primitive object </i>
 * @author Huaxing Wu -- <i> Revised 10/31/02. Add codes to build the package
 *                           trees for the subject classes/interfaces. </i>
 *@author Huaxing Wu -- <i> Revised 10/30/02. Add reference to Program as the
 *                          symbol tables have to belong to a specific 
 *                          program. Functions of loading calsses and 
 *                          Reconstruct LVT have been moved from Program to 
 *                          here as SymbolTable's main functionality is to
 *                          load the program.Corresponding change to methods 
 *                          which used
 *                          to take program as parameter has been changed. Also
 *                          reference to ResourceFileI has been removed as we
 *                          now can get it from Program. </i>
 * @author Huaxing Wu -- <i> 11/20/02. Remove functionality of finding a method/field
 * 			from a class/interface to NamedReferenceType class. Here just
 * 			simply call the corresponding function. </i>
 * @author Jay Lofstead -- 2003/05/19 removed some of the reflection and explicit type
 * 				checking in favor of polymorphism.  Removed commented out code to aid in readability.
 * @author Jay Lofstead -- 2003/05/22 simplified the code by pushing some of the bit twiddling into the ClassFile class where it really belongs.
 * @author Jay Lofstead 2003/06/02 converted elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/16 changed toString to return a deterministic, XML format of the symbol table
 * @author Jay Lofstead 2003/06/16 changed to use auto id generation of SymbolTableEntry objects
 * @author Jay Lofstead 2003/06/26 moved symbol table entry ID into SymbolTable to eliminate static data memeber
 * @author Jay Lofstead 2003/06/30 added implement of SymbolTable
 * @author Jay Lofstead 2003/07/01 changed to implement SymbolTable
 */
public class SymbolTableImpl implements Modifier, AccessLevel, java.io.Serializable, SymbolTable
{
	/**
	 * Constructs a new, empty symbol table object.
	 * @param program -- The program that this symboltable belongs to.
	 */
	public SymbolTableImpl (Program newProgram)
	{
		program = newProgram;

		// create all mapping objects
		symbols = new HashSet ();
		primitiveMap = new HashMap ();
		constants = new Vector ();
		arrayMap = new HashMap ();
		namedReferenceTypeMap = new HashMap ();
		unresolvedReferenceMap = new UnresolvedReferenceMap ();
		packageMap = new HashMap ();
	}

	/**
	 * Adds a symbol to the symbol table.
	 *
	 * @param sym Symbol to be added
	 */
	public void addSymbol (SymbolTableEntry sym)
	{
		sym.setID (nextEntryID++);
		symbols.add (sym);
	}

	/**
	 * Returns a string representation of the symbol table.
	 */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();
		SymbolTableEntry [] symbols = getSymbols ();
		Arrays.sort (symbols, new SymbolTableEntrySortComparator ());

		buf.append ("<symboltable>");
		for (int i = 0; i < symbols.length; i++)
		{
			buf.append ("<symbol>");
			buf.append ("<name>");
			buf.append (jaba.tools.Util.toXMLValid (symbols [i].getName ()));
			buf.append ("</name>");
			buf.append ("<id>");
			buf.append (symbols [i].getID ());
			buf.append ("</id>");
			buf.append ("<type>");
			buf.append (symbols [i].getClass ().getName ());
			buf.append ("</type>");
			buf.append ("</symbol>");
		}
		buf.append ("</symboltable>");

		return buf.toString ();
	}

	/**
	 * Returns all symbols in the symbol table.
	 * @return All of the symbols in the symbol table.
	 */
	public Collection getSymbolsCollection ()
	{
		return symbols;
	}

	/**
	 * Convenience method for converting the Vector to an Array (less efficient) from <code>getSymbolsCollection</code>
	 * @return All of the symbols in the symbol table.
	 */
	public SymbolTableEntry [] getSymbols ()
	{
		return (SymbolTableEntry []) symbols.toArray (new SymbolTableEntry [symbols.size ()]);
	}

	/**
	 * Returns the NamedReferenceType that corresponds to the name given
	 * in the form <i>java.lang.Class</i>.
	 * @param name  Fully qualified class name.
	 * @return The NamedReferenceType with class name <i>name</i>.
	 */
	public NamedReferenceType getNamedReferenceType (String name)
	{
		return (NamedReferenceType) namedReferenceTypeMap.get (name.replace ('.', '/'));
	}

	/**
	 * Returns the Primitive object for the specified primitive type.
	 * @param type  Type of the primitive requested.  This is specified 
	 *              in the form of the constants defined on the Primitive
	 *              class (i.e. Primitive.INTEGER).
	 * @return  The primitive object that corresponds to <code>type</code>.
	 *          Returns <code>null</code> when the specified type is not
	 *          in the symbol table.
	 * @deprecated replaced by {@link PrimitiveImpl#valueOf(int)}
	 */
	public Primitive getPrimitive (int type)
	{
		Integer iType = new Integer( type );
		if (primitiveMap.containsKey (iType))
		{
			return (Primitive) primitiveMap.get (iType);
		}

		return null;
	}

	/**
	 * Loads the symbol table based on the program passed into the constructor.
	 */
	public void load ()
	{
		ResourceFileI rcFile = program.getResourceFile ();

		addSymbol (program);
		addPrimitives();

		// load all classes and interfaces specified
		ClassFile [] classfiles = getSpecifiedClassFiles ();
		loadNamedReferences (classfiles);

		// load library classes (all remaining unresolved references)
		try
		{
			loadLibClasses (rcFile);
		}
		catch (UnresolvedRefNotFound e)
		{
			e.printStackTrace ();
			throw new RuntimeException (e);
		}

		// walk through constant pool and insert any CONSTANT_Class,
		// CONSTANT_Methodref, CONSTANT_Fieldref, or CONSTANT_InterfaceMethodRef
		// constant pool entries into the classfile-constant pool map pair 
		// array
		try
		{
			createMappingForExternalReferences ();
		}
		catch ( UnresolvedRefNotFound e)
		{
			e.printStackTrace ();
			throw new RuntimeException (e);
		}
      
		// resolve all backward references
		// do this after call to createMappingForExternalReferences() because
		// the mapping process may load additional classes
		resolveBackwardReferences ();

		// remove classfile attribute from named-reference types -- they were
		// only required for resolving external references
		Collection namedRefs = program.getLoadedNamedReferenceTypesCollection ();
		Class object = null;
		for (Iterator i = namedRefs.iterator (); i.hasNext ();)
		{
			NamedReferenceType namedRef = (NamedReferenceType) i.next ();
			namedRef.removeAttributeOfType( "jaba.classfile.ClassFile" );

			// while we are already looping through all named reference types,
			// we look for java.lang.Object and when we find it, set it as
			// the superclass for the array types
			if (namedRef.getName ().equals ("java/lang/Object"))
			{
				object = (Class) namedRef;
			}
		}

		if (object == null)
		{
			RuntimeException e = new RuntimeException ("No java.lang.Object class found");
			e.printStackTrace ();
			throw e;
		}

		// for each array variable, initialize its name
		Iterator arrays = arrayMap.values ().iterator ();
		while (arrays.hasNext ())
		{
			Array array = (Array) arrays.next ();
			array.__initializeLeafTypeAndDimensions ();
			array.__setSuperClass (object);
		}
	}

	/**
	 * Returns the fully resolved TypeEntry of the not fully resolved type.
	 * This method is meant for JABA's internal use only. This is part of
	 * the facilities for dynamic loading.
	 * @param incompleteType -- to be fully resolved type
	 * @return the fully resolved type. 
	 */
	public TypeEntry forType (TypeEntry incompleteType)
	{
		if (incompleteType instanceof NamedReferenceType)
		{
			return forNamedReferenceType (incompleteType.getName ());
		}
		else
		{
			if (incompleteType instanceof Array)
			{
				return forArray ((Array) incompleteType);
			}
			else
			{
				assert incompleteType instanceof Primitive : incompleteType.toString ();
				return incompleteType;
			}
		}
	}

	/**
	 * Returns the Array object associated with the incompleted array type. 
	 * The incompleted array type is generated by Descriptor. This method
	 * is meant only for JABA iternal use.This is part of
	 * the facilities for dynamic loading.
	 * @param type -- The incomplete type object constructed by Descriptor
	 * @return the completely resolved array type object
	 */
	public Array forArray (Array type)
	{
		// Check whether this array has already been loaded
		String key = getArrayMapKey (type);
		if (arrayMap.containsKey(key))
		{
			return (Array) arrayMap.get (key);
		}

		// Not loaded yet, load it now.
		// walk down array dimensions until we reach the element type
		int levelsDeep = 0;
		TypeEntry currentType = type;
		while (currentType instanceof Array)
		{
			currentType = ((Array) currentType).getElementType ();
			levelsDeep++;
		}

		TypeEntry elementType = currentType;
		// if the element is a class/interface, resolve it first,
		// as it may not be loaded yet.
		if (elementType instanceof NamedReferenceType)
		{
			elementType = forNamedReferenceType (elementType.getName ());
		}
		currentType = elementType;

		StringBuffer name = new StringBuffer (elementType.getName ());
		// now, walk back the opposite way, from a one dimensional array of
		// elementType up to levelsDeep dimensional array of elementType.
		for (int i = 1; i <= levelsDeep; i++)
		{
			// construct an array-map key with elementType and i levelsDeep
			String strKey = elementType.getName () + String.valueOf (i);
			name.insert (0, '[');

			// if this array type is not already in the symbol table
			if (!arrayMap.containsKey (strKey))
			{
				// create a new array type
				Array array = new ArrayImpl ();
				array.setElementType (currentType);
				array.setName (name.toString ());
				// add this array type to the symbol table and the arrayMap
				addSymbol (array);
				arrayMap.put (strKey, array);

				// set this type as the current type to be assigned as the
				// next higher dimensional array's element type
				currentType = array;
			}
			else // if this array type is already in the symbol table
			{
				// set this type (the entry in the symbol table) as the
				// current type to be assigned as the next higher dimensional
				// array's element type
				currentType = (Array) arrayMap.get (strKey);
			}
		}

		return (Array) currentType;
	}

	/**
	 * Get the NamedReferenceType for the specified name. It will load
	 * the class if it has not been loaded yet. And it will be loaded as a
	 * library class. This method is meant for 
	 * internal use of JABA.This is part of
	 * the facilities for dynamic loading.
	 * Users can use {@link #getNamedReferenceType(String) getNamedReferenceType}
	 * for this functinality. 
	 * @param name -- The name of the NamedReferenceType to be found,
	 * @return NamedReferenceType for the namedreference name.
	 */
	public NamedReferenceType forNamedReferenceType (String name)
	{
		NamedReferenceType namedRef = (NamedReferenceType) namedReferenceTypeMap.get (name);
		if (namedRef == null)
		{
			ClassFile classfile = new ClassFileImpl (name);
			try
			{
				classfile.Load (program.getResourceFile ());
			}
			catch (Exception e)
			{
				throw new RuntimeException (e);
			}

			namedRef = loadNamedReference (classfile, true);
			Debug.println ("Dynamically loaded: " + name, Debug.ST, 1);
		}

		return namedRef;
	}

	/**
	 * Returns a name-package map, including all packages in this program.
	 * This method is for internal use only.
	 * @return A hashmap for name/package.
	 */
	HashMap getPackageMap ()
	{
		return packageMap;
	}

	/** ??? */
	private void addPrimitives ()
	{
		Primitive [] primitives = PrimitiveImpl.getAllPrimitives ();
		for (int i = 0; i < primitives.length; i++)
		{
			addSymbol (primitives [i]);
			Integer iType = new Integer (primitives [i].getType ());
			primitiveMap.put (iType, primitives [i]);
		}
	}

	/**
	 * Create the ClassFile Objects for all classes specified in 
	 * ResourceFile. It will amend the local variable table if user
	 * want to get the absolutely correct lvt.
	 */
	private  ClassFile [] getSpecifiedClassFiles ()
	{
		ResourceFileI rcFile = program.getResourceFile ();
		// get the variables that we need from the resource file
		List classNames = rcFile.getClassFilesList ();

		ClassFile [] classFiles = new ClassFile [classNames.size ()];

		int i = 0;
		for (Iterator e = classNames.iterator (); e.hasNext (); i++)
		{
			classFiles [i] = new ClassFileImpl ((String) e.next ());
			try
			{
				classFiles [i].Load (rcFile);
			}
			catch (Exception e1)
			{
				e1.printStackTrace ();
				throw new RuntimeException (e1.toString ());
			}
		}

		if (program.getOptions ().getCreateLVT ())
		{
			String [] classPaths = (String []) rcFile.getClassPathsList ().toArray (new String [0]);
			CstrLocalVariableTable.rebuildDebugInfos (classFiles, classPaths);
		}

		return classFiles;
	}

	/**
	 * Loads all classes and interfaces specified into the symbol table.
	 * @param classfiles  The set of classfiles to be analyzed.  The classes
	 *                    represented by these classfiles and their dependencies
	 *                    will be loaded into the symbol table.
	 * @param program  Program object for the application being analyzed.
	 */
	private void loadNamedReferences (ClassFile [] classfiles)
	{
		// for each classfile, load this class or interface
		for (int i = 0; i < classfiles.length; i++)
		{
			loadNamedReference (classfiles [i], false);
		}
	}

	/**
	 * Loads one class into the symbol table.
	 * @param classfile  The classfile to be analyzed.
	 * @param isSummarized  A boolean flag indicating whether this class is to be
	 *                      loaded as if it were a library class or is to be
	 *                      fully analyzed.
	 */
	private NamedReferenceType loadNamedReference (ClassFile classfile, boolean isSummarized)
	{
		NamedReferenceType namedRef;
      
		// create the Class or Interface for the classfile, and add attributes
		if (classfile.isInterface ())
		{
			namedRef = new InterfaceImpl ();
			namedRef.setProgram (program);
		}
		else
		{
			namedRef = new ClassImpl ();
			namedRef.setProgram (program);
		}

		program.addNamedReferenceType (namedRef);
		namedReferenceTypeMap.put (classfile.GetClassName (), namedRef);
		addSymbol (namedRef);

		namedRef.setAccessLevel (classfile.getAccessLevel ());
		namedRef.setModifiers (classfile.getModifiers ());
		namedRef.setName (classfile.GetClassName ());
		namedRef.setIsSummarized (isSummarized);
      
		loadFields (classfile, namedRef, isSummarized);
		loadMethods( classfile, namedRef, isSummarized );

		// set the superclass
		// create a named reference for temporary purposes -- it merely holds
		// the name of the super class
		if (namedRef instanceof Class)
		{
			String superClassName = classfile.GetSuperClassName ();
			if (superClassName != null)
			{
				Class tempSuperClass = new ClassImpl ();
				tempSuperClass.setName (superClassName);

				// get and resolve this classes super class
				try
				{
					resolveType (tempSuperClass, namedRef, namedRef.getClass ().getMethod ("setSuperClass", new java.lang.Class[] {jaba.sym.Class.class}));
				}
				catch (Exception e)
				{
					e.printStackTrace ();
					throw new RuntimeException (e);
				}
			}
		}

		// loop through all interfaces implemented
		int numInterfaces = classfile.getInterfacesCount ();
		for (int i = 0; i < numInterfaces; i++)
		{
			// get the index into the constant pool for this interface
			int interfaceIndex = classfile.getInterfaceIndex (i);

			// find the index into the constant pool for the name for this interface
			int interfaceNameIndex = classfile.getConstantPool ().getConstantPoolEntry (interfaceIndex).getNameIndex ();

			// get the name of this interface
			String interfaceName = classfile.getConstantPool ().getConstantPoolEntry (interfaceNameIndex).getString ();

			// create an interface for temporary purposes -- it merely holds the name of the interface
			Interface tempInterface = new InterfaceImpl ();
			tempInterface.setName (interfaceName);

			// get and resolve the interface for this classfile
			// if the classfile that we are parsing represents a class, then we
			// say that this class implements this interface
			if (namedRef instanceof Class)
			{
				try
				{
					resolveType (tempInterface, namedRef, namedRef.getClass ().getMethod ("addInterface", new java.lang.Class [] {jaba.sym.Interface.class}));
				}
				catch (Exception e)
				{
					e.printStackTrace ();
					throw new RuntimeException (e);
				}
			}
			// if the classfile that we are parsing represent an interface, then
			// we say that this interface extends the tempInterface
			else // namedRef instanceof Interface
			{
				try
				{
					resolveType (tempInterface, namedRef, namedRef.getClass ().getMethod ("addSuperInterface", new java.lang.Class [] {jaba.sym.Interface.class}));
				}
				catch (Exception e)
				{
					e.printStackTrace ();
					throw new RuntimeException (e);
				}
			}
		}

		loadInnerClasses( classfile, namedRef );

		// resolve all references to this class that were previously unresolved
		try
		{
			unresolvedReferenceMap.resolve (classfile.GetClassName (), namedRef);
		}
		catch (NoSuchElementException e)
		{
		}
		catch (Exception e)
		{
			e.printStackTrace ();
			throw new RuntimeException (e);
		}

		// add string, int, long, float, and double constants to the
		// constant-pool map---only for non-summarized classes
		if (isSummarized)
		{
			return namedRef;
		}

		// set source file name
		jaba.classfile.SourceFileAttribute srcFileAttr = null;
		for (int i = 0; i < classfile.getAttributeCount (); i++ )
		{
			if (classfile.getAttribute (i) instanceof jaba.classfile.SourceFileAttribute)
			{
				srcFileAttr = (jaba.classfile.SourceFileAttribute) classfile.getAttribute (i);
				break;
			}
		}

		if (srcFileAttr != null)
		namedRef.setSourceFileName (srcFileAttr.getFileString ());

		// create a mapping from constant pool entries to symbol table entries
		ConstantPoolMap constantPoolMap = new ConstantPoolMap ();
		namedRef.addAttribute (constantPoolMap);
		namedRef.addAttribute (classfile);
		ConstantPool constantPool = classfile.getConstantPool ();
		for (int i = 1; i < constantPool.getConstantPoolCount (); i++)
		{
			ConstantPoolInfo entry = constantPool.getConstantPoolEntry (i);
			int tagValue = entry.getTagValue ();
			if (   tagValue == ConstantPoolInfo.CONSTANT_String
			    || tagValue == ConstantPoolInfo.CONSTANT_Integer
			    || tagValue == ConstantPoolInfo.CONSTANT_Long
			    || tagValue == ConstantPoolInfo.CONSTANT_Float
			    || tagValue == ConstantPoolInfo.CONSTANT_Double
			   )
			{
				// constant values stored in the constant pool; a corresponding
				// object is created for each unique constant value and added
				// to the constant-pool map
				Object constVal;
				if (tagValue == ConstantPoolInfo.CONSTANT_String)
				{
					int stringIndex = entry.getStringIndex ();
					ConstantPoolInfo utf8Entry = constantPool.getConstantPoolEntry (stringIndex);
					if (utf8Entry.getTagValue () != ConstantPoolInfo.CONSTANT_Utf8)
					{
						throw new RuntimeException (
							  "SymbolTable.loadNamedReference (): "
							+ " String Index "
							+ stringIndex
							+ " for CONSTANT_String at "
							+ " index "
							+ i
							+ " in the constant pool does not contain entry"
							+ " of type UTF8"
							);
					}
					constVal = utf8Entry.getString ();
					if (constVal == null)
					{
						throw new RuntimeException (
							  "SymbolTable.loadNamedReference ():"
							+ " No String value for UTF8 tag at index "
							+ stringIndex
							+ " in the constant pool."
							);
					}
				}
				else
				{
					if (tagValue == ConstantPoolInfo.CONSTANT_Integer)
					{
						constVal = new Integer (entry.getInteger ());
					}
					else
					{
						if (tagValue == ConstantPoolInfo.CONSTANT_Long)
						{
							constVal = new Long (entry.getLong ());
						}
						else
						{
							if (tagValue == ConstantPoolInfo.CONSTANT_Float)
							{
								constVal = new Float (entry.getFloat ());
							}
							else
							{
								constVal = new Double (entry.getDouble ());
							}
						}
					}
				}

				int index = constants.indexOf (constVal);
				if (index > -1)
				{
					constVal = constants.get (index);
				}
				else
				{
					constants.add (constVal);
				}
				constantPoolMap.putConst (new Integer (i), constVal);
			}
		}

		// Collect the package information for this class, only subject's
		// class's package will be collected since only those classes' control
		// can reach this point
		gatherPackageInfo (namedRef);

		return namedRef;
	}

	/**
	 * Loads all classes that are left on the unresolved named reference list
	 * after all of the specified classes have been loaded.  These classes are
	 * loaded and parsed only for summarized information -- no local variables
	 * for methods are loaded.
	 * @param ResourceFile  The resourceFile object which contains the 
	 *                      knowledge of classpaths. 
	 * @exception UnresolvedRefNotFound  Thrown if an unresolved reference
	 *                                   cannot be found in the directory paths
	 *                                   specified by <code>libraryPaths</code>.
	 */
	private void loadLibClasses (ResourceFileI rcFile) throws UnresolvedRefNotFound
	{
		while (!unresolvedReferenceMap.isEmpty ()) // loop through all unresolved named references
		{
			// get the first name on the unresolved list
			String unresolvedRefName = (String) (unresolvedReferenceMap.unresolvedClassNames ().next ());

			// create a classfile object and try to load it
			ClassFile classfile = null;
			boolean loaded = false;

			// loop through all of the possible library paths, trying each as
			// the classpath for this class
			classfile = new ClassFileImpl (unresolvedRefName);
			try
			{
				classfile.Load (rcFile);
				loaded = true;
			}
			catch (Exception e)
			{
				e.printStackTrace ();
			}

			// if this class wasn't found in any of the library paths, throw an exception
			if (!loaded) 
			{
				throw new UnresolvedRefNotFound (unresolvedRefName);
			}

			// load the library named reference
			loadNamedReference (classfile, true);
		}
	}

	/**
	 * Loads all fields of the classfile into the symbol table.
	 * @param classfile  The classfile to parse.
	 * @param namedRef   The Class or Interface object that is being built and
	 *                   will contain references to the new Field objects.
	 * @param constantPoolMap  Mapping from constant pool entries to symbol
	 *                         table entries.
	 */
	private void loadFields (ClassFile classfile, NamedReferenceType namedRef, boolean isSummarized)
	{
		// loop through all fields of this class
		int numFields = classfile.getFieldsCount ();
		for (int i = 0; i < numFields; i++)
		{
			// get the field info object
			FieldInfo fieldInfo = classfile.getFieldInfo (i);

			if (isSummarized && fieldInfo.isPrivate ())
			{
				continue;
			}

			Field field = new FieldImpl ();
			addSymbol (field);

			field.setContainingType (namedRef);
			field.setAccessLevel (fieldInfo.getAccessLevel ());
			field.setModifiers (fieldInfo.getModifiers ());
			field.setName (fieldInfo.getNameString ());
			field.setDescriptor (fieldInfo.getDescriptorString ());
			field.setSignature (FieldImpl.getSignatureString (fieldInfo.getNameString (), fieldInfo.getDescriptorString ()));

			// add field to the named reference (either a class or interface) that contains it
			namedRef.addField (field);

			// walk through attributes and set the synthetic flag if a synthetic
			// attribute exists for this field
			field.setSynthetic (fieldInfo.isSynthetic ());

			if (isSummarized)
			{
				continue;
			}

			// Only resolve subject classes' field's type. For library classes'
			// field, we will resolve dynamically later.
			// set the field's type
			FieldDescriptor fieldDescriptor = new FieldDescriptor (fieldInfo.getDescriptorString ());

			// resolve the type of the field
			try
			{
				resolveType (fieldDescriptor.getType (), field, field.getClass ().getMethod ("setType", new java.lang.Class [] {jaba.sym.TypeEntry.class}));
			}
			catch (Exception e)
			{
				e.printStackTrace ();
				throw new RuntimeException (e);
			}
		} // end for each field
	}

	/**
	 * Loads all methods of the classfile into the symbol table.
	 * @param classfile  The classfile to parse for methods.
	 * @param namedRef   The Class or Interface object that is being built and
	 *                   will contain references to the new Method objects.
	 * @param constantPoolMap  Mapping from constant pool entries to symbol table
	 *                         entries.
	 * @param isSummarized  Boolean value indicating whether this field should
	 *                      be analyzed for its code attribute.  If this value is
	 *                      <code>true</code> we will not analyze the classfile's
	 *                      local variable table, or any attributes that won't be
	 *                      available in a non-debugged classfile.
	 */
	private void loadMethods (ClassFile classfile, NamedReferenceType namedRef, boolean isSummarized)
	{
		// loop through all methods of this classfile
		int numMethods = classfile.getMethodsCount ();
		for (int i = 0; i < numMethods; i++)
		{
			// get the method info object
			MethodInfo methodInfo = classfile.getMethodInfo (i);

			// Do not load private methods of library classes
			if (isSummarized && methodInfo.isPrivate ())
			{
				continue;
			}

			// create a Method object
			Method method = new MethodImpl (namedRef, methodInfo);

			// add the method to the symbol table
			addSymbol (method);

			// if this is a constructor, add the method to the named reference
			// as a constructor
			if (   (namedRef instanceof Class)
			    && (methodInfo.getNameString ().equals ("<init>"))
			   )
			{
				((Class) namedRef).addConstructor (method);
			}

			// add the method to the named reference (either a class or
			// interface) that contains it
			namedRef.addMethod (method);

			// For library classes' method, its exception types, 
			// return type, paramter types will be resolved dynamically. 
			// Its code attribute is not needed
			if (isSummarized)
			{
				continue;
			}

			// create a local variable mapping from local variable 
			// table index to local variable
			LocalVariableMap localVarMap = new LocalVariableMap ();
			// add local variable map as attributes of the method
			method.addAttribute (localVarMap);

			for (int j = 0; j < methodInfo.getAttributeCount (); j++)
			{
				AttributeInfo attribute = methodInfo.getAttribute (j);

				// if an exception attribute...
				if (attribute instanceof ExceptionsAttribute)
				{
					// we need to register all exceptions that may be thrown by
					// by this method in the method object

					ExceptionsAttribute excAttribute = (ExceptionsAttribute) attribute;
					// loop through each exception that may be thrown
					for (int k = 0; k < excAttribute.getNumberOfExceptions (); k++)
					{
						// create an exception class for temporary purposes --
						// it merely holds the name of the exception
						String exceptionName = excAttribute.getExceptionString (k);
						assert exceptionName != null;
						if (exceptionName != null)
						{
							Class tempException = new ClassImpl ();
							tempException.setName (exceptionName);
							// get and resolve the type of the exception -- add 
							// it to this method's list of exceptions that can be thrown
							try
							{
								resolveType (tempException, method, method.getClass ().getMethod ("addException", new java.lang.Class [] {jaba.sym.Class.class}));
							}
							catch (Exception e)
							{
								throw new RuntimeException (e);
							}
						}
					}
				}
				else
				{
					if (attribute instanceof CodeAttribute) // if a code attribute and we are not summarizing...
					{
						CodeAttribute codeAttr = (CodeAttribute) attribute;
						boolean found = false;

						// loop through all attributes of the code attribute
						for (int k = 0; k < codeAttr.getAttributeCount (); k++)
						{
							AttributeInfo innerAttr = codeAttr.getAttribute (k);
							// if the attribute is a local variable attribute, load
							// the local variables in the symbol table
							if (innerAttr instanceof LocalVariableTableAttribute)
							{
								loadLocalVariables (method, (LocalVariableTableAttribute) innerAttr, methodInfo.getDescriptorString (), localVarMap);
								found = true;
							}
						}

						if (!found && codeAttr.getMaxLocals() > 0)
						{
							System.err.println ("Warning: Missing local variable table for "
									+ classfile.GetClassName ()
									+ "."
									+ methodInfo.getNameString ()
									+ ".\nRecompile with"
									+ " -g option."
									);
						}
					}
				}
			}

			// get and resolve the method's return and parameter types
			MethodDescriptor methodDescriptor =  new MethodDescriptor (methodInfo.getDescriptorString ());
			try
			{
				resolveType (methodDescriptor.getReturnType (), method, method.getClass ().getMethod ("setType", new java.lang.Class [] {jaba.sym.TypeEntry.class}));
				TypeEntry[] paramTypes = methodDescriptor.getParamTypes ();
				// if this method is not static, then its first parameter is
				// the 'this' reference
				if (!method.isStatic ())
				{
					resolveType (namedRef, method, method.getClass ().getMethod ("addFormalParameterType", new java.lang.Class [] {jaba.sym.TypeEntry.class}));
				}
				for (int k = 0; k < paramTypes.length; k++)
				{
					resolveType (paramTypes [k], method, method.getClass ().getMethod ("addFormalParameterType", new java.lang.Class [] {jaba.sym.TypeEntry.class}));
				}
			}
			catch (Exception e)
			{
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * Loads all local variables of a method into the symbol table.
	 * @param method  The method for which the local variables are to be placed
	 *                in the symbol table.
	 * @param localVarAttr  The local variable attribute defined in the code
	 *                      attribute for the method.
	 * @param descriptorString  Descriptor string for the method -- used to
	 *                          determine the number of formal parameters that
	 *                          are to be gotten by looking at the local vars.
	 * @param localVarMap  Mapping from local variable table entries to symbol
	 *                     table entries.
	 */
	private void loadLocalVariables (Method method, LocalVariableTableAttribute localVarAttr, String descriptorString, LocalVariableMap localVarMap)
	{
		// get the method descriptor to see how many formal parameters there
		// are so that we may use the local variables created for the formal
		// parameters of this method
		MethodDescriptor descriptor = new MethodDescriptor (descriptorString);
		TypeEntry [] paramTypes = descriptor.getParamTypes ();

		// iterate through entries in the local variable table attribute, and
		// identify entries for which only one local variable needs to be
		// created; these are entries that have the same name, descriptor,
		// and stack-frame index
		LocalVariableTableEntry [] entries = localVarAttr.getLocalVariableTableEntries ();
		Vector localVars = new Vector ();

outerLoop:
		for (int i = 0; i<entries.length; i++)
		{
			int j = 0;
			for (Iterator e = localVars.iterator (); e.hasNext (); j++)
			{
				Object o = e.next ();
				if (!(o instanceof LocalVariableTableEntry))
				{
					continue;
				}
				LocalVariableTableEntry entry = (LocalVariableTableEntry) o;

				if (   entries [i].getNameString ().equals (entry.getNameString ())
				    && entries [i].getIndex () == entry.getIndex ()
				    && entries [i].getDescriptorString ().equals (entry.getDescriptorString ())
				   )
				{
					localVars.addElement (new Integer (j));
					continue outerLoop;
				}
			}
			localVars.addElement (entries [i]);
		}

		// loop through all entries in the local variable table attribute
		int paramIndex = 0;
		int i = 0;
		for (Iterator e = localVars.iterator (); e.hasNext (); i++)
		{
			// get the local variable entry in the attribute table
			Object o = e.next ();
			if (o instanceof Integer)
			{
				LocalVariable var = (LocalVariable) localVarMap.get ((Integer) o);
				if (var == null)
				{
					RuntimeException re = new RuntimeException ();
					re.printStackTrace ();
					throw re;
				}
				localVarMap.put (new Integer (i), var);
				continue;
			}

			LocalVariableTableEntry localVarEntry = (LocalVariableTableEntry) o;

			// create a LocalVariable object
			LocalVariable localVar;
			// add this local variable as a formal parameter if appropriate --
			// our assumption here is that the first local variable is the 'this'
			// reference (if this method is not static), then if there are n 
			// formal parameters, the next n local variables in the local 
			// variable attribute are the formal parameters, in order
			if (i == 0 && !method.isStatic ())
			{
				localVar = new ThisImpl ();
				method.addFormalParameter (localVar);
			}
			else
			{
				if (paramIndex < paramTypes.length)
				{
					localVar = new FormalParameterImpl ();

					// increment param index
					paramIndex++;

					// add this local var as a formal param
					method.addFormalParameter (localVar);
				}
				else
				{
					localVar = new LocalVariableImpl ();
				}
			}

			// add local variable to the symbol table
			addSymbol (localVar);

			// create a mapping from index of local-variable table to local var
			localVarMap.put (new Integer (i), localVar);

			// set the method to this local variable
			localVar.setContainingMethod (method);

			// add this local variable to the method
			method.addLocalVariable (localVar);

			// set the local variable's name
			localVar.setName (localVarEntry.getNameString ());

			// set and resolve the local variable's type
			FieldDescriptor localVarDescriptor = new FieldDescriptor (localVarEntry.getDescriptorString ());
			try
			{
				resolveType (localVarDescriptor.getType (), localVar, localVar.getClass ().getMethod ("setType", new java.lang.Class [] {jaba.sym.TypeEntry.class}));
			}
			catch (Exception e1)
			{
				e1.printStackTrace ();
				throw new RuntimeException (e1);
			}
		}	      
	}

	/**
	 * Loads inner classes of a classfile into the symbol table.
	 * @param classfile  The classfile to parse for inner classes attributes.
	 * @param namedRef  The Class or Interface object that is being built and
	 *                  may contain references to the inner classes objects.
	 */
	private void loadInnerClasses (ClassFile classfile, NamedReferenceType namedRef)
	{
		// loop through all class attributes looking for inner class attributes
		int numAttr = classfile.getAttributeCount ();
		for (int i = 0; i < numAttr; i++)
		{
			// get the attribute
			AttributeInfo attr = classfile.getAttribute (i);

			// if the attribute is an inner class attribute...
			if (attr instanceof InnerClassAttribute)
			{
				// get the entries in the inner class attribute -- this 
				// represents the inner classes that this class references
				InnerClassEntry [] innerClassEntries = ((InnerClassAttribute) attr).getInnerClasses ();

				// loop through all of the inner class entries
				for (int j = 0; j < innerClassEntries.length; j++)
				{
					// get the name of the inner class in the attribute
					int innerClassIndex = innerClassEntries [j].getInnerClassInfoIndex ();
					int innerClassNameIndex = classfile.getConstantPool ().getConstantPoolEntry (innerClassIndex).getNameIndex ();
					String innerClassName = classfile.getConstantPool ().getConstantPoolEntry (innerClassNameIndex).getString ();

					// get the name of the outer class in the attribute
					int outerClassIndex = innerClassEntries [j].getOuterClassInfoIndex ();
					String outerClassName;
					if (outerClassIndex != 0)
					{
						int outerClassNameIndex = classfile.getConstantPool ().getConstantPoolEntry (outerClassIndex).getNameIndex ();
						outerClassName = classfile.getConstantPool ().getConstantPoolEntry (outerClassNameIndex).getString ();
					}
					else
					{
						outerClassName = parseStringForOuterClassName (innerClassName);
					}

					// if the inner class is "this" class, then we set the outer
					// class as this class's containing type
					if (innerClassName.equals (classfile.GetClassName ()))
					{
						// create a temporary named reference type (class or
						// interface) for the resolution of the containing outer
						// class
						NamedReferenceType tempOuterClass = new NamedReferenceTypeImpl ();
						tempOuterClass.setName (outerClassName);
						try
						{
							resolveType (tempOuterClass, namedRef, namedRef.getClass ().getMethod ("setContainingType", new java.lang.Class [] {jaba.sym.ContainsInnerNamedReferenceTypes.class}));
						}
						catch (Exception e)
						{
							e.printStackTrace ();
							throw new RuntimeException (e);
						}
					}

					// if the outer class is "this" class, then we add the inner
					// class as an inner class of this class
					if (outerClassName.equals (classfile.GetClassName ()))
					{
						// create a temporary named reference type (class or
						// interface) for the resolution of the inner class
						NamedReferenceType tempInnerClass = new NamedReferenceTypeImpl ();
						tempInnerClass.setName (innerClassName);
						try
						{
							resolveType (tempInnerClass, namedRef, namedRef.getClass ().getMethod ("addInnerNamedReferenceType", new java.lang.Class [] {jaba.sym.NamedReferenceType.class}));
						}
						catch (Exception e)
						{
							System.err.println ("Error: Exception occurred during type resolution (10): "
									+ e.getClass ().getName ()
									+ ": "
									+ e.getMessage ()
									);
						}
					}
				}
			}
		}
	}

	/**
	 * Returns the enclosing class name of the an inner class given the inner
	 * class name.
	 * @param innerClassName  The name of the inner class to be parsed.
	 * @return  The name of the deepest enclosing class of 
	 *          <code>innerClassName</code>.
	 */
	private String parseStringForOuterClassName (String innerClassName)
	{
		// create a stack to hold tokens of string
		Stack tokenStack = new Stack ();

		// create a tokenizer that uses the '$' as the delimiter
		StringTokenizer tokenizer = new StringTokenizer (innerClassName, "$");

		// push each token on the stack
		while (tokenizer.hasMoreTokens ())
		{
			try
			{
				tokenStack.push (tokenizer.nextToken ());
			}
			catch (Exception e)
			{
				e.printStackTrace ();
				throw new RuntimeException (e);
			}
		}

		// throw away the top token on the stack -- this is either the simple
		// name of the inner class if inner class is named or a numeric value 
		// if inner class is anonymous
		tokenStack.pop();

		// throw away all numeric tokens on the stack
		char [] topCharString = new char [1];
		((String) tokenStack.peek ()).getChars (0, 1, topCharString, 0);
		while (   (topCharString [0] >= '0')
		       && (topCharString [0] <= '9')
		      )
		{
			try
			{
				tokenStack.pop();
			}
			catch (Exception e)
			{
				e.printStackTrace ();
				throw new RuntimeException (e);
			}

			((String) tokenStack.peek ()).getChars (0, 1, topCharString, 0);
		}

		// now, we have the enclosing class name in the stack, reading from
		// bottom to top -- so, we pop each value off and prepend them to a
		// string buffer
		StringBuffer buffer = new StringBuffer ();
		while (!tokenStack.empty ())
		{
			try
			{
				buffer.insert (0, (String) tokenStack.pop ());
			}
			catch (Exception e)
			{
				e.printStackTrace ();
				throw new RuntimeException (e);
			}

			// if there is another token on the stack then there needs to be a
			// '$' delimiter between the one just inserted and the next one
			if (!tokenStack.empty ())
			{
				buffer.insert (0, "$");
			}
		}

		// return this outer class name
		return buffer.toString ();
	}

	/**
	 * Takes a type returned by the Descriptor classes and resolves it with
	 * the appropriate type entry in the symbol table.
	 * If the type is not in the symbol table yet and it is a primitive or
	 * an array, it is entered into the symbol table.  If the type is not
	 * in the symbol table yet and it is a class or interface,  it is
	 * placed on the unresolved list.  The symbol is assigned the type by
	 * way of its self reference in <code>typedSymbol</code> and the method
	 * to set the type defined by <code>setTypeMethod</code>.
	 * @param type  The type to parse.
	 * @param typedSymbol  The symbol to set the type for.
	 * @param setTypeMethod  The method to call to set the type.
	 */
	private void resolveType (TypeEntry type, SymbolTableEntry typedSymbol, java.lang.reflect.Method setTypeMethod)
	{
		if (type instanceof Primitive) // if the type is a primitive...
		{
			resolvePrimitive ((Primitive) type, typedSymbol, setTypeMethod);
		}
		else
		{
			if (type instanceof Array) // else, if the type is an array...
			{
				resolveArray ((Array) type, typedSymbol, setTypeMethod);
			}
			else // else, if the type is a NamedReference (either an Interface or a Class)
			{
				resolveNamedReferenceType ((NamedReferenceType) type, typedSymbol, setTypeMethod);
			}
		}
	}

	/**
	 * Takes a primitive type that was returned by a Descriptor class and
	 * resolves it with the appropriate type entry in the symbol table.  If the
	 * primitive is not yet in the symbol table, it is entered.  The symbol is
	 * assigned the primitive by way of its self reference in 
	 * <code>typedSymbol</code> and the method to set the type defined by
	 * <code>setTypeMethod</code>.
	 * @param type  The type to parse.
	 * @param typedSymbol  The symbol to set the type for.
	 * @param setTypedMethod  The method to call to set the type.
	 */
	private void resolvePrimitive (Primitive type, SymbolTableEntry typedSymbol, java.lang.reflect.Method setTypeMethod)
	{
		// call the method to set the type of the symbol
		try
		{
			setTypeMethod.invoke (typedSymbol, new Object [] {type});
		}
		catch (Exception e)
		{
			throw new RuntimeException ("Error during type resolution", e);
		}
	}

	/**
	 * Takes an array type that was returned by a Descriptor class and resolves
	 * it with the appropriate type entry in the symbol table.  If the array is
	 * not yet in the symbol table, it is entered.  The symbol is assigned the
	 * array type by way of its self reference in <code>typedSymbol</code> and
	 * the method to set the type defined by <code>setTypeMethod</code>.
	 * @param type  The type to parse.
	 * @param typedSymbol  The symbol to set the type for.
	 * @param setTypedMethod  The method to call to set the type.
	 */
	private void resolveArray (Array type, SymbolTableEntry typedSymbol, java.lang.reflect.Method setTypeMethod)
	{
		// walk down array dimensions until we reach the element type
		int levelsDeep = 0;
		TypeEntry currentType = type;
		while (currentType instanceof Array)
		{
			currentType = ((Array) currentType).getElementType ();
			levelsDeep++;
		}
		TypeEntry elementType = currentType;

		// now, walk back the opposite way, from a one dimensional array of
		// elementType up to levelsDeep dimensional array of elementType.
		StringBuffer name = new StringBuffer (elementType.getName ());
		for (int i = 1; i <= levelsDeep; i++)
		{
			// construct an array-map key with elementType and i levelsDeep
			String strKey = elementType.getName () + String.valueOf (i);
			name.insert (0, '[');

			// if this array type is not already in the symbol table
			if (!arrayMap.containsKey (strKey))
			{
				// create a new array type
				Array array = new ArrayImpl ();

				// set its element type to the last currentType -- this will
				// either be the i-1 dimensional array or the element type
				// if the currentType is not an array, then its type must be
				// resolved, so we recursively call this method
				if (!(currentType instanceof Array))
				{
					// resolve the type of the element type
					try
					{
						resolveType (currentType, array, array.getClass ().getMethod ("setElementType", new java.lang.Class [] { jaba.sym.TypeEntry.class}));
					}
					catch (Exception e)
					{
						e.printStackTrace ();
						throw new RuntimeException (e);
					}
				}
				else
				{
					array.setElementType (currentType);
				}

				// add this array type to the symbol table and the arrayMap
				array.setName (name.toString ());
				addSymbol (array);
				arrayMap.put (strKey, array);

				// set this type as the current type to be assigned as the
				// next higher dimensional array's element type
				currentType = array;
			}
			else // if this array type is already in the symbol table
			{
				// set this type (the entry in the symbol table) as the
				// current type to be assigned as the next higher dimensional
				// array's element type
				currentType = (Array) arrayMap.get (strKey);
			}
		}

		// call the method to set the type of the symbol
		if (setTypeMethod != null)
		{
			try
			{
				setTypeMethod.invoke (typedSymbol, new Object [] {currentType});
			}
			catch (Exception e)
			{
				e.printStackTrace ();
				throw new RuntimeException (e);
			}
		}
	}

	/**
	 * Takes a NamedReferenceType returned by a Descriptor class and resolves it
	 * with the appropriate NamedReferenceType entry in the symbol table.  If
	 * the type is not in the symbol table yet, it is place on the unresolved 
	 * named reference list.  The symbol is assigned the type (either now or
	 * later when the unresolved list is processed) via its self reference in
	 * <code>typedSymbol</code> and the method to set the type defined by
	 * <code>setTypeMethod</code>.
	 * @param type  The type to parse.
	 * @param typedSymbol  The symbol to set the type for.
	 * @param setTypeMethod  The method to call to set the type.
	 */
	private void resolveNamedReferenceType (NamedReferenceType type, SymbolTableEntry typedSymbol, java.lang.reflect.Method setTypeMethod)
	{
		// get the name of the named reference type
		String name = type.getName ();

		// if the named reference type is on the named reference map...
		if (namedReferenceTypeMap.containsKey (name))
		{
			// get the reference to the type
			NamedReferenceType namedReferenceType = (NamedReferenceType) namedReferenceTypeMap.get (name);

			// call the method to set the type of the symbol
			if (setTypeMethod != null)
			{
				try
				{
					setTypeMethod.invoke (typedSymbol, new Object [] {namedReferenceType});
				}
				catch (Exception e)
				{
					e.printStackTrace ();
					throw new RuntimeException (e);
				}
			}
		}
		else // if the named reference type is not yet on the named reference map (and thus, not yet in the symbol table)
		{
			// put the named reference on the unresolved reference map
			try
			{
				unresolvedReferenceMap.add (name, typedSymbol, setTypeMethod);
			}
			catch (Exception e)
			{
				e.printStackTrace ();
				throw new RuntimeException (e);
			}
		}
	}

	/**
	 * Resolves all backward references in the symbol table that were previously
	 * impossible to resolve.
	 */
	private void resolveBackwardReferences ()
	{
		// create an iterator over all classes and interfaces loaded
		Iterator allNamedReferences = namedReferenceTypeMap.values ().iterator ();

		// iterate over all classes and interfaces loaded
		while (allNamedReferences.hasNext ())
		{
			// get the next class or interface loaded
			NamedReferenceType namedRef = (NamedReferenceType) allNamedReferences.next ();

			// if this is a Class...
			if (namedRef instanceof Class)
			{
				// we need to set this Class as its superclass's subclass
				// get this class's super class
				Class superClass = ((Class) namedRef).getSuperClass ();

				// if the superclass is null, this object should be 
				// java.lang.Object, so don't do anything.  Otherwise, set this
				// class as its superclass's subclass
				if (superClass != null)
				{
					superClass.addSubClass ((Class) namedRef);
				}

				// we need to set this Class as a class that implements all of
				// its interfaces
				// get all of the interfaces that this class implements
				Collection interfaces = ((Class) namedRef).getInterfacesCollection ();

				// loop through all interfaces implemented
				for (Iterator i = interfaces.iterator (); i.hasNext ();)
				{
					Interface interfaceImplemented = (Interface) i.next ();

					// in the interface, set this class as a implementing class
					interfaceImplemented.addImplementingClass ((Class) namedRef);
				}
			}
			else // namedRef instanceof Interface
			{
				// we need to set this Interface as extending all of the
				// interfaces that it extends
				// get all of this interface's superinterfaces
				Collection superInterfaces = ((Interface) namedRef).getSuperInterfacesCollection ();

				// loop through all of the superinterfaces of this interface
				for (Iterator i = superInterfaces.iterator (); i.hasNext ();)
				{
					Interface superInterface = (Interface) i.next ();

					// in the superinterface, set this interface as a subinterface
					superInterface.addSubInterface ((Interface) namedRef);
				}
			}
		}
	}

	/**
	 * Inserts new entries into the constant pool maps for all external
	 * references -- CONSTANT_Class, CONSTANT_Methodref, CONSTANT_Fieldref, and
	 * CONSTANT_InterfaceMethodref.  The purpose for this phase is to facilitate
	 * the resolution of external references in the bytecode instructions during
	 * control-flow and data-flow analysis.
	 * @param program program object for the application being analyzed.
	 * @param ResourceFile  ResourceFile object which contains the classpath info
	 *                      
	 */
	private void createMappingForExternalReferences () throws UnresolvedRefNotFound
	{
		ResourceFileI rcFile = program.getResourceFile ();
		// loop through every classfile looking for all CONSTANT_Class 
		// entries -- we need to make sure that all classes are first all
		// resolved
		NamedReferenceType [] namedRefTypes = program.getNamedReferenceTypes ();
      
		for (int i = 0; i < namedRefTypes.length; i++)
		{
			if (namedRefTypes [i].isSummarized ())
			{
				continue;
			}

			// get the constant pool for this classfile
			ClassFile cf = (ClassFile) ((jaba.sym.NamedReferenceType) namedRefTypes [i]).getAttributeOfType ("jaba.classfile.ClassFile");
			ConstantPool constantPool = cf.getConstantPool ();

			// loop through all entries in the constant pool
			for (int j = 1; j < constantPool.getConstantPoolCount (); j++)
			{
				// get this constant pool entry
				ConstantPoolInfo entry = constantPool.getConstantPoolEntry (j);

				if (entry.getTagValue () == ConstantPoolInfo.CONSTANT_Class)
				{
					// find the name of this named reference type
					String name = constantPool.getConstantPoolEntry (entry.getNameIndex ()).getString ();

					// if this is an array, create an array object for it, if required
					if (name.charAt (0) == '[')
					{
						FieldDescriptor fieldDesc = new FieldDescriptor (name);
						resolveType (fieldDesc.getType (), null, null);
						continue;
					}

					// if this class is not already resolved, we need to place
					// it on the unresolved named reference map
					if (!namedReferenceTypeMap.containsKey (name))
					{
						// create a temporary named reference type (class or 
						// interface) -- basically needed to hold the name of the
						// reference type to load
						NamedReferenceType tempNamedReferenceType = new NamedReferenceTypeImpl ();
						tempNamedReferenceType.setName (name);

						try
						{
							// call resolveType with no method to call upon resolution
							resolveType (tempNamedReferenceType, null, null);
						}
						catch (Exception e)
						{
							e.printStackTrace ();
							throw new RuntimeException (e);
						}
					}
				}
			}
		}

		// load all of those classes that are still unresolved and all of their
		// unresolved references
		try
		{
			loadLibClasses (rcFile);
		}
		catch (UnresolvedRefNotFound e)
		{
			e.printStackTrace ();
			throw new RuntimeException (e);
		}

		// okay, now we should be guaranteed that all external named reference
		// types have finally been loaded.  We need to now augment the cCMP with
		// all external references

		// loop through every classfile looking for any external references --
		// CONSTANT_Class, CONSTANT_Methodref, CONSTANT_InterfaceMethodref, and
		// CONSTANT_Fieldref
		for (int i = 0; i < namedRefTypes.length; i++)
		{
			if (namedRefTypes [i].isSummarized ())
			{
				continue;
			}

			// get the constant pool for this classfile
			ClassFile cf = (ClassFile) ((jaba.sym.NamedReferenceType) namedRefTypes [i]).getAttributeOfType ("jaba.classfile.ClassFile");
			ConstantPool constantPool = cf.getConstantPool ();
			ConstantPoolMap constantPoolMap = (ConstantPoolMap) ((jaba.sym.NamedReferenceType) namedRefTypes [i]).getAttributeOfType ("jaba.sym.ConstantPoolMap");

			// loop through all entries in the constant pool
			for (int j = 1; j < constantPool.getConstantPoolCount (); j++)
			{
				// get this constant pool entry
				ConstantPoolInfo entry = constantPool.getConstantPoolEntry (j);

				// if this is a class entry...
				int tagValue = entry.getTagValue ();
				if (tagValue == ConstantPoolInfo.CONSTANT_Class)
				{
					// find the name of this named reference type
					String name = constantPool.getConstantPoolEntry (entry.getNameIndex ()).getString ();

					// if this is an array, get corresponding Array object from the array map
					if (name.charAt (0) == '[')
					{
						FieldDescriptor fieldDesc = new FieldDescriptor (name);
						String key = getArrayMapKey ((Array) fieldDesc.getType ());
						Array array = (Array) arrayMap.get (key);
						constantPoolMap.put (new Integer (j), array);

						continue;
					}

					// get the object to which this reference refers to
					NamedReferenceType namedReferenceType = (NamedReferenceType) namedReferenceTypeMap.get (name);

					// put this value on the constant pool map
					constantPoolMap.put (new Integer (j), namedReferenceType);
				}
				else // if this is a reference to a member...
				{
					if (   (tagValue == ConstantPoolInfo.CONSTANT_Methodref)
					    || (tagValue == ConstantPoolInfo.CONSTANT_Fieldref)
					    || (tagValue == ConstantPoolInfo.CONSTANT_InterfaceMethodref)
					   )
					{
						// find the name of the class for which this member belongs
						int classNameIndex = constantPool.getConstantPoolEntry (entry.getClassIndex ()).getNameIndex ();
						String namedRefName = constantPool.getConstantPoolEntry (classNameIndex).getString ();

						// get the named reference type for which this member belongs
						NamedReferenceType namedReference = (NamedReferenceType) namedReferenceTypeMap.get (namedRefName);

						// get the name and descriptor of the member
						int nameAndTypeIndex = entry.getNameTypeIndex ();
						int nameIndex = constantPool.getConstantPoolEntry (nameAndTypeIndex).getNameIndex ();
						int descriptorIndex = constantPool.getConstantPoolEntry (nameAndTypeIndex).getDescriptorIndex ();
						String memberName = constantPool.getConstantPoolEntry (nameIndex).getString ();
						String memberDescriptor = constantPool.getConstantPoolEntry (descriptorIndex).getString ();

						// loop through all of the members of the named reference
						// type, searching for the member for which this constant
						// pool entry refers to
						Member member = null;
						Member[] members;
						// if this constant pool entry refers to a field...
						if (tagValue == ConstantPoolInfo.CONSTANT_Fieldref)
						{
							member = (jaba.sym.Member) namedReference.findField (FieldImpl.getSignatureString (memberName, memberDescriptor));
						}
						else //if this constant pool entry refers to a method... 
						{
							String signature = MethodImpl.getSignatureString (memberName, memberDescriptor);
							member = (jaba.sym.Member) namedReference.findMethod (MethodImpl.getSignatureString (memberName, memberDescriptor));
						}

						if (member == null)
						{
							throw new UnresolvedRefNotFound (namedRefName
										+ "."
										+ memberName
										+ " "
										+ memberDescriptor
										+ " referenced in class "
										+ namedRefTypes [i]
										);
						}
						// put the found member and its corresponding reference in 
						// the constant pool into the constant pool map
						constantPoolMap.put (new Integer (j), member);
					}
				}
			}
		}
	}

	/**
	 * Returns a key for an array object. An array key is used to query
	 * {@link #arrayMap arrayMap} to determine if a symbol table entry for
	 * that array object has been created.
	 * @param array An empty <code>Array</code> object (that is returned by
	 *              <code>FieldDescriptor.getType()</code>.
	 * @return Array key map for <code>array</code>.
	 */
	private String getArrayMapKey (Array array)
	{
		int levelsDeep = 0;
		TypeEntry currentType = array;
		while (currentType instanceof Array)
		{
			currentType = ((Array) currentType).getElementType ();
			levelsDeep++;
		}
		return currentType.getName () + String.valueOf (levelsDeep);
	}

	/**
	 * Build the package tree information. Create the Package objects for all 
	 * the containing packages of this namedReference. Keep the created 
	 * Package objects in the packageMap to make sure one and only one 
	 * package object is created for a package. Also create the containing and
	 * contained relationship between package and package, and between package
	 * and class. 
	 * @param namedRef -- the namedReferenceType whose package information will
	 *                  be built.
	 */
	private void gatherPackageInfo (NamedReferenceType namedRef)
	{
		String name = namedRef.getName ();
		int packageIndex = name.lastIndexOf ("/");

		// This class may be global, and not belong to any package
		if (packageIndex < 0)
		{
			return;
		}

		String packageName = name.substring (0, packageIndex);
		java.util.StringTokenizer tokenizer = new java.util.StringTokenizer (packageName, "/");

		String previous = "";
		Package parent = null;
		while (tokenizer.hasMoreTokens ())
		{
			String token = previous + tokenizer.nextToken ();
			Package packageObject = (Package) packageMap.get (token);
			// Create package object for this package if necessary 
			if (packageObject == null)
			{
				packageObject = new PackageImpl ();
				packageObject.setName (token);
				addSymbol (packageObject);
				packageMap.put (token, packageObject);

				if (parent != null)
				{
					packageObject.setContainingPackage (parent);
					parent.addPackage (packageObject);
				}
			}
			parent = packageObject;
			previous = token + "/";
		}

		if (parent != null)
		{
			if (namedRef instanceof Class)
			{
				parent.addClass ((Class) namedRef);
			}
			else
			{
				parent.addInterface ((Interface) namedRef);
			}
			namedRef.setContainingPackage( parent );
		}
	}

	/**
	 * The exception type that is thrown when an unresolved reference cannot
	 * be resolved given the library paths supplied by the user.
	 */
	private class UnresolvedRefNotFound extends Exception
	{
		/** The name of the unresolved reference that wasn't found. */
		public String unresolvedRefName;

		/**
		 * Constructor for this class.
		 */
		UnresolvedRefNotFound (String name)
		{
			super (name + " not found in library paths given");
			unresolvedRefName = name;
		}
	}

	/** Symbols in the symbol table. */
	private HashSet symbols;

	/**
	 * Mapping of primitive types in the symbol table. 
	 * Key: an Integer defined in Primitive, Value: Primitive 
	 */
	private HashMap primitiveMap;

	/** Integer, Long, Float, Double, or String objects for constant values */
	private Vector constants;

	/**
	 * Mapping of array types in the symbol table. The key for the hash-map is
	 * a string that is built by concatenating the fully-qualified name of
	 * the array element type and the array dimension.
	 */
	private HashMap arrayMap;

	/** Mapping package name to package object */
	private HashMap packageMap;

	/**
	 * Mapping of named reference types (interfaces and classes) in the symbol
	 * table. 
	 * Key: fully qualified class name, value: NamedReferenceType
	 */
	private HashMap namedReferenceTypeMap;

	/**
	 * Unresolved reference container. used to keep track of classes and
	 * interfaces that are referenced, but have not yet been resolved.
	 */
	private UnresolvedReferenceMap unresolvedReferenceMap;  
  
	/** legacy variable no longer used */
	public static boolean analysisPattern = false;

	/** ??? */
	private Program program;

	/** The highest id given to any SymbolTableEntry. */
	private static int nextEntryID = 0;
}
