package jaba.sym;

/**
 * Represents a formal parameter.  This class extends LocalVariable because
 * formal parameters are treated as local variables.  Thus a quick and easy
 * check for "instanceof FormalParameter" will determine whether a given
 * local variable is a formal parameter.
 * @author Jim Jones -- <i>Created. Apr. 30, 1999</i>.
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface FormalParameter extends LocalVariable
{
}
