/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import java.util.Vector;
import java.util.Collection;
import java.util.Iterator;

/**
 * Represents an interface in the symbol table.
 * @author Jim Jones
 * @author Huaxing Wu: For those variable that those keep a vector and an
 *                     array. Remove vArr, only use vector In this class  v
 *		       are: superInterfaces, subInterfaces and 
 *                     implementingClasses.
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/02 converted elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/26 added implementing Serializable
 * @author Jay Lofstead 2003/06/30 added implements Interface
 * @author Jay Lofstead 2003/07/01 changed to implement Interface
 */
public class InterfaceImpl extends NamedReferenceTypeImpl implements java.io.Serializable, Interface
{
    /**
     * Constructor for Interface.
     */
    public InterfaceImpl ()
    {
	superInterfaces = new Vector();
	subInterfaces = new Vector();
	implementingClasses = new Vector();
    }
    
    /**
     * Adds a superinterface for this interface.
     * @param superInterface  A superinterface to this interface.
     * @exception IllegalArgumentException  Thrown if superinterface is null.
     */
    public void addSuperInterface( Interface superInterface ) 
	throws IllegalArgumentException
    {
	// check if superInterface is a null reference, if so, throw an 
	// exception
	if ( superInterface == null )
	    throw new IllegalArgumentException( "Exception thrown in " +
						"jaba.sym.Interface." + 
						"addSuperInterface()" );
	
	// add the superinterface
	superInterfaces.addElement( (Object) superInterface );
	
    }
    
    /**
     * Returns superinterfaces for this interface.
     * @return Superinterfaces for this interface.
     */
	public Collection getSuperInterfacesCollection ()
	{
		return superInterfaces;
	}

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getSuperInterfacesCollection</code>
     * @return Superinterfaces for this interface.
     */
	public Interface [] getSuperInterfaces ()
	{
		return (Interface []) superInterfaces.toArray (new Interface [superInterfaces.size ()]);
	}

    /**
     * Adds a subinterface for this interface.
     * @param subInterface  A subinterface to this interface.
     * @exception IllegalArgumentException  Thrown if subinterface is null.
     */
    public void addSubInterface (Interface subInterface)
	throws IllegalArgumentException
    {
	// check if subInterface is a null reference, if so, throw an exception
	if ( subInterface == null )
	    throw new IllegalArgumentException( "Exception thrown in " +
						"jaba.sym.Interface." +
						"addSubInterface()" );
	
	// add the subinterface
	subInterfaces.addElement( (Object) subInterface );
	
    }
    
    /**
     * Returns the subinterfaces of this interface.
     * @return Array of subinterfaces for this interface.
     */
	public Collection getDirectSubInterfacesCollection ()
	{
		return subInterfaces;
	}
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getDirectSubInterfacesCollection</code>
     * @return Array of subinterfaces for this interface.
     */
	public Interface [] getDirectSubInterfaces ()
	{
		return (Interface []) subInterfaces.toArray (new Interface [subInterfaces.size ()]);
	}
    
    /**
     * Returns all subinterfaces of this interface.
     * @return Array of all subinterfaces for this interface.
     */
	public Collection getAllSubInterfacesCollection ()
	{
		Vector v = new Vector ();
		traverseSubInterfaces (v);

		return v;
	}

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getAllSubInterfacesCollection</code>
     * @return Array of all subinterfaces for this interface.
     */
	public Interface [] getAllSubInterfaces ()
	{
		Collection v = getAllSubInterfacesCollection ();

		return (Interface []) v.toArray (new Interface [v.size ()]);
	}

    /**
     * Adds a class that implements this interface.
     * @param implementingClass  A class that implements this interface.
     * @exception IllegalArgumentException  Thrown if implementingClass is 
     *                                      null.
     */
    public void addImplementingClass (Class implementingClass) 
	throws IllegalArgumentException
    {
	// check if implementingClass is a null reference, if so, throw an 
	// exception
	if ( implementingClass == null )
	    throw new IllegalArgumentException( "Exception thrown in " +
						"jaba.sym.Interface." +
						"addImplementingClass()" );
	
	// add the implementing class
	implementingClasses.addElement( (Object) implementingClass );
	
    }
    
    /**
     * Returns classes that directly implement this interface.
     * @return Array of classes that directly implement this interface.
     */
	public Collection getDirectImplementingClassesCollection ()
	{
		return implementingClasses;
	}
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getDirectImplementingClassesCollection</code>
     * @return Array of classes that directly implement this interface.
     */
	public Class [] getDirectImplementingClasses ()
	{
		return (Class []) implementingClasses.toArray (new Class [implementingClasses.size ()]);
	}
    
    /**
     * Returns an array of all classes that implement this interface.
     * A class may indirectly implement an interface if (1) it implements
     * a subinterface of this interface, or (2) it is a subclass of a class
     * that implements this interface.
     * @return Array of classes that directly or indirectly implement this
     *         interface.
     */
	public Collection getAllImplementingClassesCollection ()
	{
		Vector v = new Vector ();
		traverseImplementingClasses (v);

		return v;
	}

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getAllImplementingClassesCollection</code>
     * @return Array of classes that directly or indirectly implement this
     *         interface.
     */
	public Class [] getAllImplementingClasses ()
	{
		Collection v = getAllImplementingClassesCollection ();

		return (Class []) v.toArray (new Class [v.size ()]);
	}

    /**
     * Recursively traverses implementing starting at this class, and records
     * all subclasses in the vector <code>v</code>.
     * @param v vector in which to record subclasses.
     */
    private void traverseImplementingClasses (Vector v)
    {
        Collection c = getDirectImplementingClassesCollection ();
        for (Iterator i = c.iterator (); i.hasNext ();)
	{
		Class cls = (Class) i.next ();

            if (!v.contains (cls))
                v.addElement (cls);

            Collection subClasses = cls.getAllSubClassesCollection ();

            for (Iterator j = subClasses.iterator (); j.hasNext ();)
	    {
		Class cls1 = (Class) j.next ();

                if (!v.contains (cls1))
                    v.addElement (cls1);
            }
        }
        Collection subInterfaces = getDirectSubInterfacesCollection ();

        for (Iterator i = subInterfaces.iterator (); i.hasNext ();)
	{
            ((InterfaceImpl) i.next ()).traverseImplementingClasses (v);
        }
    }
    
    /**
     * Adds an attribute to this interface.
     * @param attribute  The attribute to be added to this interface.
     * @throws IllegalArgumentException if <code>attribute</code> is
     *                                  <code>null</code>.
     */
    public void addAttribute( InterfaceAttribute attribute )
	throws IllegalArgumentException
    {
        super.addAttribute( attribute );
    }
    
    /**
     * Returns an array of attributes of this interface.
     * @return All attributes defined for this interface.
     */
    public InterfaceAttribute[] getAttributes()
    {
        Vector attributes = super.getNamedReferenceTypeAttributes();
        InterfaceAttribute[] m = new InterfaceAttribute[attributes.size()];
	int i = 0;
        for (Iterator e = attributes.iterator (); e.hasNext (); i++)
	{
            m [i] = (InterfaceAttribute) e.next ();
        }
        return m;
    }
    
    
    /**
     * Returns a string for this object.
     * @return  String representing this object.
     */
	public String toString ()
	{
		StringBuffer buffer = new StringBuffer ();

		buffer.append ("<interface>");
		buffer.append (super.toString ());

		buffer.append ("<id>" + getID () + "</id>");

		buffer.append ("<name>" + jaba.tools.Util.toXMLValid (getName ()) + "</name>");
		Collection implClasses = getDirectImplementingClassesCollection ();
		for (Iterator i = implClasses.iterator (); i.hasNext ();)
		{
			buffer.append ("<implementingclass>");
			buffer.append (((Class) i.next ()).getID ());
			buffer.append ("</implementingclass>");
		}

		Collection subInterfaces = getDirectSubInterfacesCollection ();
		for (Iterator i = subInterfaces.iterator (); i.hasNext ();)
		{
			buffer.append ("<subinterface>");
			buffer.append (((Interface) i.next ()).getID ());
			buffer.append ("</subinterface>");
		}

		Collection superInterfaces = getSuperInterfacesCollection ();
		for (Iterator i = superInterfaces.iterator (); i.hasNext ();)
		{
			buffer.append ("<superinterface>");
			buffer.append (((Interface) i.next ()).getID ());
			buffer.append ("</superinterface>");
		}

		buffer.append ("</interface>");

		return buffer.toString ();	
	}

	/** ??? */
    protected Collection getParentsCollection ()
    {
	return getSuperInterfacesCollection ();
    }

	/** ??? */
    protected NamedReferenceType [] getParents ()
    {
	return getSuperInterfaces ();
    }

    /**
     * Recursively traverses subinterfaces starting at this interface,
     * and records
     * all subinterfaces in the vector <code>v</code>.
     * @param v vector in which to record subinterfaces.
     */
    private void traverseSubInterfaces( Vector v ) {
        Collection ifc = getDirectSubInterfacesCollection ();
        for (Iterator i = ifc.iterator (); i.hasNext ();)
	{
	    InterfaceImpl intf = (InterfaceImpl) i.next ();

            v.addElement (intf);
            intf.traverseSubInterfaces (v);
        }
    }
    
    /** All superinterfaces of this interface */
    private Vector superInterfaces;
    
    /** All subinterfaces of this interface */
    private Vector subInterfaces;

    /** All classes that implement this interface */
    private Vector implementingClasses;
}
