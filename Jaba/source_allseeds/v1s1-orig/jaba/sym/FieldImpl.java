/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import jaba.du.HasValue;

/**
 * Class that represents a field member.
 * @author Huaxing Wu -- <i> Revised 11/20/02. Add method getSignatureString </i>
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/30 added implements Field
 * @author Jay Lofstead 2003/07/01 changed to implement Field
 */
public class FieldImpl extends MemberImpl implements HasValue, Cloneable, Field
{
    /**
     * Returns the type of member.
     * @return type of member
     */
    public TypeEntry getType()
    {
	if(type == null) {
	    resolveType();
	}
        return type;
    }

  /**
   * Returns a string for this object.
   * @return  String representing this object.
   */
	public String toString()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<field>");
		buf.append (super.toString ());

		buf.append ("<id>" + getID () + "</id>");
		buf.append ("<name>" + jaba.tools.Util.toXMLValid (getName ()) + "</name>");
		buf.append ("</field>");

		return buf.toString ();
	}

  /**
   * Clones this object.
   * @return Clone of this object.
   */
  public Object clone()
    {
      Field newField = null;
      try
	{
	  newField = (Field)super.clone();
	}catch ( CloneNotSupportedException e){
	    e.printStackTrace();
	    throw new RuntimeException( "CloneNotSupportedException: " + e );
	}

      return newField;
    }

    /**
     * Create a Signature string out of a name and a descriptor of a field
     * based on the rules.
     * @param name -- the name of a field
     * @param descriptor -- the descriptor of a field
     * @return the signature string represented by the input
     */
    public static String getSignatureString(String name, String descriptor)
    {
	return name+descriptor;
    }

   /** ??? */
   private void resolveType()
   {
       FieldDescriptor fieldDescriptor = new FieldDescriptor (getDescriptor ());

	type = ((jaba.sym.SymbolTable) getContainingType ().getProgram ().getSymbolTable ()).forType (fieldDescriptor.getType ());
    }
}
