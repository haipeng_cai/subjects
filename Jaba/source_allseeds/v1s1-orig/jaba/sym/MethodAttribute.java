/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Interface for attributes of a method.
 * @author Jim Jones
 */
public interface MethodAttribute extends Attribute
{
}
