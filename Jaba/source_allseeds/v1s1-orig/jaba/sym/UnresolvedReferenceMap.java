/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * Defines a map from a fully-qualified
 * class name to a list of references to that class that are unresolved -
 * pairs of the form (class name, unresolved list).
 * Each element in an unresolved list is a pair: (1) an object of type
 * <code>NamedReferenceType</code>, and (2) a method of type
 * <code>java.lang.reflect.Method</code>.
 * <code>UnresolvedReferenceMap</code> is used during symbol table
 * construction to save all references to class A if class A does not have
 * an entry in the symbol table. When the symbol table entry for A is created,
 * the unresolved list for class A is resolved by walking the elements
 * in the list, and invoking the named method on the named object (with
 * the symbol table entry object for A as the parameter).
 *
 * @see java.lang.reflect.Method
 * @see NamedReferenceType
 * @author S. Sinha
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members.
 */
public class UnresolvedReferenceMap implements java.io.Serializable
{
    // Inner class

    /**
     * Inner class that defines a list element in the unresolved list
     * of references for a class.
     * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
     */
    private class UnresolvedListElement implements java.io.Serializable
    {

        /** Object that references the unresolved class. */
        SymbolTableEntry object;

        /** Method that is used to set the field that references the
            unresolved class. */
        java.lang.reflect.Method method;

    }

    // Fields

    /** Map from fully-qualified class name to an
        <code>UnresolvedListElement</code>. */
    private HashMap unresMap;


    // Constructor

    /**
     * Creates an empty unresolved-reference map.
     */
    public UnresolvedReferenceMap() {
        unresMap = new HashMap();
    }


    // Methods

    /**
     * Adds an entry to the unresolved-reference map.
     * @param key fully-qualified name of class to whose unresolved element
     *            a new element is added.
     * @param object the object field of an element in an unresolved list.
     * @param method method defined in the class of <code>object</code> to be
     *               called when resolving.
     * @exception NoSuchMethodException thrown if <code>methodName</code> and
     *                                  <code>param</code> do not constitute a
     *                                  valid method defined in the class of 
     *                                  <code>object</code>.
     * @exception ClassNotFoundException thrown if the type of 
     *                                   <code>param</code> is not a valid 
     *                                   class or interface name.
     */
    public void add( String key, SymbolTableEntry object,
                     java.lang.reflect.Method method)
                throws NoSuchMethodException, ClassNotFoundException {
        LinkedList list;
        try {
            list = remove( key );
        }
        catch( NoSuchElementException nse ) {
            list = new LinkedList();
        }
	if ( ( object != null ) && ( method != null ) )
	  {
	    UnresolvedListElement elem = new UnresolvedListElement();
	    elem.object = object;
	    elem.method = method;
	    list.add( elem );
	  }
	unresMap.put( key, list );
    }

    /**
     * Returns the unresolved list associated with a class, and removes the
     * (class name, unresolved list) pair from the unresolved-reference map.
     * @param key fully-qualified name of class.
     * @return unresolved list associated with <code>key</code>.
     * @exception NoSuchElementException thrown if <code>key</code> does not
     *                                   have an entry in the unresolved-
     *                                   reference map.
     */
    public LinkedList remove( String key )
                      throws NoSuchElementException {
        if ( !unresMap.containsKey( key ) ) {
            throw new NoSuchElementException(
                "UnresolvedReferenceMap.remove(): "+
                "No entry in unresolved-reference map for "+key );
        }
        return (LinkedList)unresMap.remove( key );
    }

    /**
     * Resolves the unresolved list associated with a class, and removes the
     * (class name, unresolved list) pair from the unresolved-reference map.
     * @param key fully-qualified name of class whose unresolved list is
     *            resolved.
     * @param param parameter to be passed to the method that is invoked
     *              for each element in the unresolved list for
     *              <code>key</code>. Each method named in an unresolved-list
     *              element expects only one parameter.
     * @exception NoSuchElementException thrown if <code>key</code> does not
     *         have an entry in the unresolved-reference map.
     * @exception IllegalAccessException as defined in
     *         {@link java.lang.reflect.Method#invoke( Object, Object[] )
     *                Method.invoke() }
     * @exception IllegalArgumentException as defined in
     *         {@link java.lang.reflect.Method#invoke( Object, Object[] )
     *                Method.invoke() }
     * @exception InvocationTargetException as defined in
     *         {@link java.lang.reflect.Method#invoke( Object, Object[] )
     *                Method.invoke() }
     */
    public void resolve( String key, NamedReferenceType param )
                throws NoSuchElementException,
                       IllegalAccessException,
                       IllegalArgumentException,
                       InvocationTargetException {
        LinkedList list = remove( key );
        java.util.ListIterator iter = list.listIterator();
        UnresolvedListElement elem;
        Object args[] = new Object[1];
        while ( iter.hasNext() ) {
            elem = (UnresolvedListElement)iter.next();
            iter.remove();  // remove the returned element from the list
            args[0] = param;
            elem.method.invoke( elem.object, args );
        }
    }

    /**
     * Returns <code>true</code> if this unresolved-reference map contains
     * no (class name, unresolved list) mappings.
     * @return <code>true</code> if this unresolved-reference map contains
     * no (class name, unresolved list) mappings.
     */
    public boolean isEmpty() {
        return unresMap.isEmpty();
    }

    /**
     * Returns the number of (class name, unresolved list) mappings in this
     * unresolved-reference map.
     * @return number of (class name, unresolved list) mappings in this
     * unresolved-reference map.
     */
    public int size() {
        return unresMap.size();
    }

    /**
     * Returns an iterator for the set of keys (fully-qualified class names)
     * in this unresolved-reference map.
     * @return iterator for the set of keys in this unresolved-reference map.
     */
    public java.util.Iterator unresolvedClassNames() {
        return unresMap.keySet().iterator();
    }

}
