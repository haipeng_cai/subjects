/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import jaba.graph.StatementNode;

import java.util.Collection;

/**
 * Class that represents a method member.
 * @author Huaxing Wu -- <i>Revised 5/16/02. Fixing problems caused by 
 * transfering info back and forward between vector and array. Change to use 
 * vector only. </i>
 * @author Huaxing Wu -- <i>Revised 7/11/02. Several get methods's info are 
 * built when CFG built, so if the call on these methods are before CFG built,
 * these method will return wrong info (empty). The problem is fixed by 
 * building CFG first when these methods are used. These methods are: 
 * get..Methods, get..Noeds(). Totally 8. </i> 
 * @author Huaxing Wu -- <i> Revised 7/17/02. Previous fixing of those get 
 * methods which are depending on CFG built does not work. Because inside of 
 * CFG build,it first gether some information, and use set to add to put it in
 * the class, then later call get method to get them. So if the get method 
 * find the CFG is not built and try to build it, it may run into infinate
 * loop or some other problems. Now it fixed by having a boolean to indicate
 * whether an attribute is set or not. </i>
 * @author Huaxing Wu -- <i> Revised 11/20/02. Add method getSignatureString. </i>
 * @author Jay Lofstead 2003/05/22 added isStatic method.
 * 					fixed toString to work for a symbol table in progress
 * 					(CFG can't be created properly until it is done).
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/02 converted elementAt to enumeration for better performance.
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/06/30 added implements Method
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface Method extends Member, ContainsInnerNamedReferenceTypes
{    
	/** ??? */
    public boolean isConstructor();

	/** returns if this method is static */
	public boolean isStatic ();
    
    /**
     * Adds a formal parameter to the set of formal parameters for the method.
     * @param f Formal parameter to be added.
     */
    public void addFormalParameter( LocalVariable f );
    
    /**
     * Adds a parameter type to the set of formal parameter types for the
     * method.
     * @param t Formal parameter type to be added.
     */
    public void addFormalParameterType( TypeEntry t );
    
    /**
     * Adds an inner class or interface (a named reference type) to the set
     * of inner named reference types contained within the method.
     * @param c Inner class or interface to be added.
     */
    public void addInnerNamedReferenceType( NamedReferenceType c );
    
    /**
     * Adds an exception to the set of exceptions that may be raised by
     * the method.
     * @param e Exception to be added.
     */
    public void addException( Class e );
    
    /**
     * Adds a local variable to the set of variables that are declared in this
     * method.
     * @param v variable to be added.
     */
    public void addLocalVariable( LocalVariable v );
    
    /**
     * Adds a method that is called by this method -- this does not include
     * finally methods.
     * @param method  Method that is called by this method.
     */
    public void addCalledMethod( Method calledMethod );
    
    /**
     * Adds a call site StatementNode -- not including finally call sites.
     * @param callSiteNode  StatementNode that contains a call to a method.
     */
    public void addCallSiteNode( StatementNode callSiteNode );
    
    /**
     * Adds a call site StatementNode that calls a finally method.
     * @param callSiteNode  StatementNode that contains a call to a finally
     *                      method.
     */
    public void addFinallyCallSiteNode( StatementNode callSiteNode );
    
    /**
     * Adds a method that calls this method -- this does not include finally 
     * methods.
     * @param callingMethod  Method that calls this method.
     */
    public void addCallingMethod( Method callingMethod );
    
    /**
     * Adds an attribute to this method.
     * @param attribute  The attribute to be added to this method.
     * @throws IllegalArgumentException if <code>attribute</code> is
     *                                  <code>null</code>.
     */
    public void addAttribute( MethodAttribute attribute )
	throws IllegalArgumentException;
    
    /**
     * Adds an inner (finally) method to this method.
     * @param method  Finally method contained within this method.
     */
    public void addFinallyMethod( Method method );
    
    /**
     * Assigns a parent method.  Calling this method implies that "this" method
     * is a finally method.
     * @param method  Parent method.
     */
    public void setContainingMethod( Method method );

	/** ??? */
    public TypeEntry getType();

    /**
     * Returns Formal parameters for the method.
     * @return Formal parameters.
     */
	public Collection getFormalParametersCollection ();
    

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getFormalParametersCollection</code>
     * @return Formal parameters.
     */
	public LocalVariable [] getFormalParameters ();
    
    /**
     * Returns formal parameter types for the method.
     * @return Formal parameter types.
     */
	public Collection getFormalParameterTypesCollection ();
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getFormalParameterTypesCollection</code>
     * @return Formal parameter types.
     */
	public TypeEntry [] getFormalParameterTypes ();

    /**
     * Returns named reference types contained within
     * the method.
     * @return Contained named reference types.
     */
	public Collection getInnerNamedReferenceTypesCollection ();
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getInnerNamedReferneceTypesCollection</code>
     * @return Contained named reference types.
     */
	public NamedReferenceType [] getInnerNamedReferenceTypes();
    
    /**
     * Returns exception types that may be raised by the
     * method.
     * @return Exceptions.
     */
	public Collection getExceptionsCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getExceptionsVector</code>
     * @return Exceptions.
     */
	public Class [] getExceptions ();

    /**
     * Returns local variables declared within this method.
     * @return Local variables.
     */
	public Collection getLocalVariablesCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getLocalVariablesVector</code>
     * @return Local variables.
     */
	public LocalVariable [] getLocalVariables ();

    /**
     * Returns number of formal parameters for the method.
     * @return number of formal parameters.
     */
    public int getFormalParameterCount ();

    /**
     * Returns all methods called from this one -- not including finally 
     * methods.
     * @return Called Methods.
     */
	public Collection getCalledMethodsCollection ();
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getCalledMethodsCollection</code>
     * @return Called Methods.
     */
	public Method [] getCalledMethods ();
    
    /**
     * Returns all call sites in this method -- not including finally call 
     * sites.
     * @return Call sites.
     */
	public Collection getCallSiteNodesCollection ();
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getCallSiteNodesCollection</code>
     * @return Call sites.
     */
	public StatementNode [] getCallSiteNodes ();

    /**
     * Returns all call sites that call finally methods.
     * @return Finally call sites.
     */
	public Collection getFinallyCallSiteNodesCollection ();
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getFinallyCallSiteNodesCollection</code>
     * @return Finally call sites.
     */
	public StatementNode [] getFinallyCallSiteNodes ();
    
    /**
     * Returns all methods calling this one -- not including finally methods.
     * @return Calling methods.
     */
	public Collection getCallingMethodsCollection ();
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getCallingMethodsCollection</code>
     * @return Calling methods.
     */
	public Method [] getCallingMethods ();
    
    /**
     * Returns all finally methods immediately contained within this method.
     * @return All finally methods immediately contained within this method.
     */
    public Collection getFinallyMethodsCollection ();
    
    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getFinallyMethodsCollection</code>
     * @return All finally methods immediately contained within this method.
     */
    public Method [] getFinallyMethods();
    
    /**
     * Returns all finally methods directly and indirectly contained within
     * this method (returns all recursively contained methods).
     * @return All finally methods directly and indirectly contained within
     *         this method.
     */
	public Collection getTransitiveFinallyMethodsCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getTransitiveFinallyMethodsCollection</code>
     * @return All finally methods directly and indirectly contained within
     *         this method.
     */
	public Method [] getTransitiveFinallyMethods ();

    /**
     * Returns the parent method of this method.  If this is not a finally 
     * method, <code>null</code> is returned.
     * @return The parent method of this method.  If this is not a finally 
     *         method, <code>null</code> is returned.
     */
    public Method getContainingMethod ();
    
    /**
     * Returns the top-level parent method of this method.  If this is not a
     * finally method, <code>this</code> reference is returned. 
     * @return The top-level parent method of this method.  If this is not a
     *         finally method, <code>this</code> reference is returned.
     */
    public Method getTopLevelContainingMethod ();

    /**
     * Returns the name of the method qualified with all containing methods
     * and the containing type.
     * @return Dot-separated names of this method, all containing methods,
     *         and the containing type of the top-level containing method.
     */
    public String getFullyQualifiedName();
    
    /**
     * Returns an array of attributes of this method.
     * @return All attributes defined for this method.
     */
    public MethodAttribute [] getAttributes();
    
    /**
     * Returns the attribute of the specified type that may be associated
     * with a method.
     * @param type type of method attribute.
     * @return Method attribute of the specified type, or
     *         <code>null</code> if method has no attribute of the specified
     *         type.
     */
    public MethodAttribute getAttributeOfType( String typeStr );

    /**
     * Removes the attribute of the specified type that may be associated
     * with a method.
     * @param typeStr type of method attribute. Must be a
     *             subtype of <code>MethodAttribute</code>.
     * @return <code>true</code> if method contained an
     *         attribute of the specified type, or
     *         <code>false</code> if method has no attribute of
     *         the specified type.
     * @throws IllegalArgumentException if <code>type</code> is not a subtype
     *                              of <code>MethodAttribute</code>.
     */
    public boolean removeAttributeOfType( String typeStr )
        throws IllegalArgumentException;

	/** ??? */
    public boolean isMainMethod();

    /**
     * Returns a string for this object.
     * @return  String representing this object.
     */
	public String toString ();
}
