/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import jaba.du.HasValue;

/**
 * Class that represents a field member.
 * @author Huaxing Wu -- <i> Revised 11/20/02. Add method getSignatureString </i>
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/30 added implements Field
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface Field extends Member, HasValue
{
    /**
     * Returns the type of member.
     * @return type of member
     */
    public TypeEntry getType();

  /**
   * Returns a string for this object.
   * @return  String representing this object.
   */
	public String toString();

  /**
   * Clones this object.
   * @return Clone of this object.
   */
  public Object clone();
}
