/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Interface for attributes of a class.
 * @author S. Sinha
 */
public interface ClassAttribute extends NamedReferenceTypeAttribute
{

}
