/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import jaba.sym.Program;
import jaba.sym.Primitive;

import jaba.main.ResourceFileI;
import jaba.main.Options;

import jaba.constants.Modifier;
import jaba.constants.AccessLevel;

import java.util.Collection;

/**
 * Implements a symbol table. A symbol table is a {@link java.util.HashSet
 * HashSet} that contains objects of type
 * {@link jaba.sym.SymbolTableEntry SymbolTableEntry}.
 * @author Huaxing Wu -- <i>Revised 3/21/02 Change methods to use ResouceFile Object
 * 			instead of Classpaths when loading library to take advantage
 * 			of ReousrceFile's supporting methods </i>
 * @author Huaxing Wu -- <i> Revised 8/12/02 Change the way of resolving 
 *          Primitive type to take advantage of the fact that Primitive class 
 *          is immutual and there is only few Primitive object </i>
 * @author Huaxing Wu -- <i> Revised 10/31/02. Add codes to build the package
 *                           trees for the subject classes/interfaces. </i>
 *@author Huaxing Wu -- <i> Revised 10/30/02. Add reference to Program as the
 *                          symbol tables have to belong to a specific 
 *                          program. Functions of loading calsses and 
 *                          Reconstruct LVT have been moved from Program to 
 *                          here as SymbolTable's main functionality is to
 *                          load the program.Corresponding change to methods 
 *                          which used
 *                          to take program as parameter has been changed. Also
 *                          reference to ResourceFileI has been removed as we
 *                          now can get it from Program. </i>
 * @author Huaxing Wu -- <i> 11/20/02. Remove functionality of finding a method/field
 * 			from a class/interface to NamedReferenceType class. Here just
 * 			simply call the corresponding function. </i>
 * @author Jay Lofstead -- 2003/05/19 removed some of the reflection and explicit type
 * 				checking in favor of polymorphism.  Removed commented out code to aid in readability.
 * @author Jay Lofstead -- 2003/05/22 simplified the code by pushing some of the bit twiddling into the ClassFile class where it really belongs.
 * @author Jay Lofstead 2003/06/02 converted elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/16 changed toString to return a deterministic, XML format of the symbol table
 * @author Jay Lofstead 2003/06/16 changed to use auto id generation of SymbolTableEntry objects
 * @author Jay Lofstead 2003/06/26 moved symbol table entry ID into SymbolTable to eliminate static data memeber
 * @author Jay Lofstead 2003/06/30 added implement of SymbolTable
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface SymbolTable
{
	/**
	 * Adds a symbol to the symbol table.
	 *
	 * @param sym Symbol to be added
	 */
	public void addSymbol (SymbolTableEntry sym);

	/**
	 * Returns a string representation of the symbol table.
	 */
	public String toString ();

  /**
   * Returns all symbols in the symbol table.
   * @return All of the symbols in the symbol table.
   */
  public Collection getSymbolsCollection ();

  /**
   * Convenience method for converting the Vector to an Array (less efficient) from <code>getSymbolsCollection</code>
   * @return All of the symbols in the symbol table.
   */
  public SymbolTableEntry [] getSymbols ();

    /**
     * Returns the NamedReferenceType that corresponds to the name given
     * in the form <i>java.lang.Class</i>.
     * @param name  Fully qualified class name.
     * @return The NamedReferenceType with class name <i>name</i>.
     */
    public NamedReferenceType getNamedReferenceType (String name);

    /**
     * Returns the Primitive object for the specified primitive type.
     * @param type  Type of the primitive requested.  This is specified 
     *              in the form of the constants defined on the Primitive
     *              class (i.e. Primitive.INTEGER).
     * @return  The primitive object that corresponds to <code>type</code>.
     *          Returns <code>null</code> when the specified type is not
     *          in the symbol table.
     * @deprecated replaced by static, implementation-only method in {@link Primitive}
     */
    public Primitive getPrimitive (int type);

  /**
   * Loads the symbol table based on the program passed into the constructor.
   */
  public void load();

    /**
     * Returns the fully resolved TypeEntry of the not fully resolved type.
     * This method is meant for JABA's internal use only. This is part of
     * the facilities for dynamic loading.
     * @param incompleteType -- to be fully resolved type
     * @return the fully resolved type. 
     */
    public TypeEntry forType(TypeEntry incompleteType);

    /**
     * Returns the Array object associated with the incompleted array type. 
     * The incompleted array type is generated by Descriptor. This method
     * is meant only for JABA iternal use.This is part of
     * the facilities for dynamic loading.
     * @param type -- The incomplete type object constructed by Descriptor
     * @return the completely resolved array type object
     */
  public Array forArray(Array type);

    /**
     * Get the NamedReferenceType for the specified name. It will load
     * the class if it has not been loaded yet. And it will be loaded as a
     * library class. This method is meant for 
     * internal use of JABA.This is part of
     * the facilities for dynamic loading.
     * Users can use {@link #getNamedReferenceType(String) getNamedReferenceType}
     * for this functinality. 
     * @param name -- The name of the NamedReferenceType to be found,
     * @return NamedReferenceType for the namedreference name.
     */
    public NamedReferenceType forNamedReferenceType (String name);
}
