/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import java.util.Vector;
import java.util.Enumeration;

/**
 * Represents a Java method descriptor.
 *
 * @author S. Sinha
 * @author Jay Lofstead 2003/06/02 converted elementAt to enumeration for better performance
 */

public class MethodDescriptor extends Descriptor {

    // Constants

    /** Marker for a void */
    private final static char D_VOID = 'V';

    /** Marker at end of parameter types in a method descriptor. */
    private final static char D_PARAM_END = ')';


    // Fields

    /** Types of formal parameters of method. */
    private Vector paramTypes;

    /** Array of types of formal parameters of method. */
    private TypeEntry[] paramTypesArr;


    // Constructors

    /**
     * Creates a method descriptor object, and parses the parameter to extract
     * type information about formal parameters and the return type.
     * @param descriptor Java field descriptor.
     * @throws IllegalArgumentException if <code>descriptor</code> is not
     *                                  well-formed.
     */
    public MethodDescriptor( String descriptor ) {
        this.descriptor = new String( descriptor );
        paramTypes = new Vector();
        StringBuffer desc = new StringBuffer( descriptor );
        parseMethodDescriptor( desc );
        if ( desc.length() != 0 ) {
            throw new IllegalArgumentException(
                "FieldDescriptor.FieldDescriptor(): Malformed descriptor: "+
                descriptor );
        }
	paramTypesArr = null;
    }

    // Methods

    /**
     * Parses formal-parameter type and return type information from a
     * method descriptor and creates appropriate <code>TypeEntry</code> objects
     * that correspond to the parsed types.
     * @param desc Java method descriptor.
     */
    private void parseMethodDescriptor( StringBuffer desc ) {
        desc = desc.deleteCharAt( 0 );  // skip leading '('
        while ( desc.charAt( 0 ) != D_PARAM_END ) {
            paramTypes.add( parseType( desc, primitiveFieldTypes ) );
        }
        desc = desc.deleteCharAt( 0 );  // skip D_PARAM_END
        type = parseType( desc, primitiveFieldTypes+D_VOID );
    }

    /**
     * Returns return type for a method descriptor.
     * @return return type for this method descriptor.
     */
    public TypeEntry getReturnType() {
        return type;
    }

    /**
     * Returns types of formal parameters for a method
     * descriptor.
     * @return Formal-parameter types for this method descriptor.
     */
    public TypeEntry[] getParamTypes() {
	if ( paramTypesArr != null ) return paramTypesArr;
	paramTypesArr = new TypeEntry[ paramTypes.size() ];
	int i = 0;
	for (Enumeration e = paramTypes.elements (); e.hasMoreElements (); i++)
	    paramTypesArr[i] = (TypeEntry) e.nextElement ();
	paramTypes = null; // remove the reference to the vector
	return paramTypesArr;
    }

    /**
     * Returns a string representation of this method descriptor.
     * @return string representation of this method descriptor.
     */
    public String toString() {
        String ret = "return type = "+type.toString()+"\nparam types = ";
        TypeEntry[] paramTypes = getParamTypes();
        for ( int i=0; i<paramTypes.length; i++ ) {
            ret += paramTypes[i].toString()+" ";
        }
        return ret;
    }

}
