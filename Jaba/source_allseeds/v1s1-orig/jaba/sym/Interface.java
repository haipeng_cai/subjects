/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import java.util.Collection;

/**
 * Represents an interface in the symbol table.
 * @author Jim Jones
 * @author Huaxing Wu: For those variable that those keep a vector and an
 *                     array. Remove vArr, only use vector In this class  v
 *		       are: superInterfaces, subInterfaces and 
 *                     implementingClasses.
 * @author Jay Lofstead 2003/05/29 converted toString to generate XML
 * @author Jay Lofstead 2003/06/02 converted elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/26 added implementing Serializable
 * @author Jay Lofstead 2003/06/30 added implements Interface
 * @author Jay Lofstead 2003/07/01 changed into an interface
 */
public interface Interface extends NamedReferenceType
{
    /**
     * Adds a superinterface for this interface.
     * @param superInterface  A superinterface to this interface.
     * @exception IllegalArgumentException  Thrown if superinterface is null.
     */
    public void addSuperInterface( Interface superInterface ) throws IllegalArgumentException;

    /**
     * Returns superinterfaces for this interface.
     * @return Superinterfaces for this interface.
     */
	public Collection getSuperInterfacesCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getSuperInterfacesCollection</code>
     * @return Superinterfaces for this interface.
     */
	public Interface [] getSuperInterfaces ();

    /**
     * Adds a subinterface for this interface.
     * @param subInterface  A subinterface to this interface.
     * @exception IllegalArgumentException  Thrown if subinterface is null.
     */
    public void addSubInterface (Interface subInterface) throws IllegalArgumentException;

    /**
     * Returns the subinterfaces of this interface.
     * @return Array of subinterfaces for this interface.
     */
	public Collection getDirectSubInterfacesCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getDirectSubInterfacesCollection</code>
     * @return Array of subinterfaces for this interface.
     */
	public Interface [] getDirectSubInterfaces ();
    
    /**
     * Returns all subinterfaces of this interface.
     * @return Array of all subinterfaces for this interface.
     */
	public Collection getAllSubInterfacesCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getAllSubInterfacesCollection</code>
     * @return Array of all subinterfaces for this interface.
     */
	public Interface [] getAllSubInterfaces ();

    /**
     * Adds a class that implements this interface.
     * @param implementingClass  A class that implements this interface.
     * @exception IllegalArgumentException  Thrown if implementingClass is 
     *                                      null.
     */
    public void addImplementingClass (Class implementingClass) throws IllegalArgumentException;

    /**
     * Returns classes that directly implement this interface.
     * @return Array of classes that directly implement this interface.
     */
	public Collection getDirectImplementingClassesCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getDirectImplementingClassesCollection</code>
     * @return Array of classes that directly implement this interface.
     */
	public Class [] getDirectImplementingClasses ();

    /**
     * Returns an array of all classes that implement this interface.
     * A class may indirectly implement an interface if (1) it implements
     * a subinterface of this interface, or (2) it is a subclass of a class
     * that implements this interface.
     * @return Array of classes that directly or indirectly implement this
     *         interface.
     */
	public Collection getAllImplementingClassesCollection ();

    /**
     * Convenience method for converting the Vector to an Array (less efficient) from <code>getAllImplementingClassesCollection</code>
     * @return Array of classes that directly or indirectly implement this
     *         interface.
     */
	public Class [] getAllImplementingClasses ();

    /**
     * Adds an attribute to this interface.
     * @param attribute  The attribute to be added to this interface.
     * @throws IllegalArgumentException if <code>attribute</code> is
     *                                  <code>null</code>.
     */
    public void addAttribute( InterfaceAttribute attribute ) throws IllegalArgumentException;
    
    /**
     * Returns an array of attributes of this interface.
     * @return All attributes defined for this interface.
     */
    public InterfaceAttribute[] getAttributes();

    /**
     * Returns a string for this object.
     * @return  String representing this object.
     */
	public String toString ();
}
