/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Abstract class that represents an entry of type Symbol into the symbol table.
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface SymbolEntry extends SymbolTableEntry
{
}
