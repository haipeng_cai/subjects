/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Represents a primitive type in the symbol table.
 * @author Jim Jones
 * @author Huaxing Wu -- <i> 8/2/02. Based on the observation that Primitive 
 *          is an immutable class and there is total 11 instance, this class 
 *          has been changed to become immutable, and use private constructor
 *          and factory method to control instances of this class. </i>
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference members
 * @author Jay Lofstead 2003/06/30 adjusted toString to return XML, added implement Primitive
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface Primitive extends TypeEntry
{
	/** ??? */
    public final int VOID = 0;
    
	/** ??? */
    public final int BOOLEAN = 1;
  
	/** ??? */
    public final int CHAR = 2;
    
	/** ??? */
    public final int BYTE = 3;
    
	/** ??? */
    public final int SHORT = 4;
    
	/** ??? */
    public final int INT = 5;
    
	/** ??? */
    public final int LONG = 6;
    
	/** ??? */
    public final int FLOAT = 7;
    
	/** ??? */
    public final int DOUBLE = 8;
    
	/** ??? */
    public final int STRING = 9;

	/** ??? */
    public final int NULL = 10;
  
    /**
     * Returns the type of this primitive type.
     * @return   Type of primitive
     */
    public int getType();
    
    /**
     * Returns a string for this object.
     * @return  String representing this object.
     */
    public String toString ();
    
    /**
     * Clones this object.
     * @return Clone of this object.
     */
    public Object clone();
}
