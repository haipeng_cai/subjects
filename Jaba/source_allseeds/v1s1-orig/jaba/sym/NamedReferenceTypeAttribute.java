/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Interface for attributes of a NamedReferenceType.
 * @author S. Sinha
 */
public interface NamedReferenceTypeAttribute extends Attribute
{

}
