/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import jaba.constants.AccessLevel;
import jaba.constants.Modifier;

/**
 * Abstract class that represents a member entry into the symbol table.
 * @author Huaxing Wu -- <i> Revised 8/12/02 add isAbstract method </i>
 * @author Huaxing Wu -- <i> Revised 11/18/02 remove setSignature(name, descriptor) method, since we now have ways to getsignature from these two strings. </i>
 * @author Jay Lofstead 2003/05/29 added getSignatureXMLValid.  changed toString to return data in an XML format
 * @author Jay Lofstead 2003/06/16 changed to use Util.toXMLValid for string conversion
 * @author Jay Lofstead 2003/06/30 added implements Member
 * @author Jay Lofstead 2003/07/01 changed to implement Member
 */
public abstract class MemberImpl extends SymbolEntryImpl implements AccessLevel, Modifier, Member
{
    // Constructor -- only accessible in subclasses

    /**
     * Initializes fields that are inherited by subclasses. The subclasses
     * invoke this constructor to initialize inherited fields.
     */
    protected MemberImpl () {
        accessLevel = 0;
        type = null;
        containingType = null;
        modifiers = 0;
        descriptor = null;
        signature = null;
        synthetic = false;
    }

    // Methods

    /**
     * Sets the access level of member.
     * @param acc Access level of member
     */
    public void setAccessLevel( int acc ) {
        accessLevel = acc;
    }

    /**
     * Sets the type of member.
     * @param type Type of member
     */
    public void setType( TypeEntry type ) {
        this.type = type;
    }

    /**
     * Sets the containing type of member.
     * @param type Containing type of member
     */
    public void setContainingType( NamedReferenceType type ) {
        containingType = type;
    }

    /**
     * Sets the modifier flags for the member.
     * @param mods Bit-masks representing modifier flags for the member.
     */
    public void setModifiers( int mods ) {
        modifiers = mods;
    }

    /**
     * Sets the descriptor for the member.
     * @param desc String representation of member descriptor.
     */
    public void setDescriptor (String desc)
    {
        descriptor = desc;
    }

    /**
     * Sets the synthetic flag for the member.
     * @param flag Boolean value for the synthetic attribute.
     */
    public void setSynthetic (boolean flag)
    {
        synthetic = flag;
    }

    /**
     * Returns the access level of member.
     * @return access level of member - an integer value defined in
     *  {@link jaba.constants.AccessLevel Accesslevel}.
     */
    public int getAccessLevel ()
    {
        return accessLevel;
    }

    /**
     * Returns the type of member.
     * @return type of member
     */
    public TypeEntry getType ()
    {
        return type;
    }

    /**
     * Returns the containing type of member.
     * @return containing type of member
     */
    public NamedReferenceType getContainingType ()
    {
        return containingType;
    }

    /**
     * Returns the modifier flags of member.
     * @return modifier flags of member
     */
    public int getModifiers() {
        return modifiers;
    }

    /**
     * Returns the member descriptor.
     * @return descriptor for member
     */
    public String getDescriptor() {
        return descriptor;
    }

    /**
     * Returns a boolean value indicating whether the member has its synthetic
     * attribute set.
     * @return true if member has its synthetic attribute set, false otherwise.
     */
    public boolean isSynthetic() {
        return synthetic;
    }

  /**
   * Sets a signature to a specified string.     
   * A method signature is the method name follow
   * by a sequence of formal-parameter types.
   * A field signature is the field name followed by its type descriptor.
   * Both method and field signatures use Java classfile descriptors.
   * @param signature  Specified signature.
   */
  public void setSignature( String signature )
    {
      this.signature = signature;
    }

    /**
     * Returns the signature of this method or field.
     * A method signature is the method name follow
     * by a sequence of formal-parameter types.
     * A field signature is the field name followed by its type descriptor.
     * Both method and field signatures use Java classfile descriptors.
     * @return Signature of method or field.
     */
    public String getSignature() {
        return signature;
    }

    /** Return a boolean to indicate whether this member is abstract.
     *@return true - this member is abtract. false -- member is concrete.
     */
    public boolean isAbstract() {
	return ((modifiers & M_ABSTRACT ) != 0);
    }

  /**
   * Returns a string for this object.
   * @return  String representing this object.
   */
	public String toString ()
	{
		StringBuffer buffer = new StringBuffer ();

		buffer.append ("<member>");
		buffer.append ("<signature>").append (signature).append ("</signature>");
		buffer.append ("<accesslevel>");
		if ((accessLevel & A_PUBLIC) != 0)
		{
			buffer.append ("public");
		}
		else
		{
			if ((accessLevel & A_PRIVATE) != 0) 
			{
				buffer.append ("private");
			}
			else
			{
				if ((accessLevel & A_PROTECTED) != 0)
				{
					buffer.append ("protected");
				}
				else
				{
					buffer.append ("package");
				}
			}
		}
		buffer.append ("</accesslevel>");

		if (type != null)
		{
			buffer.append ("<type>" + type.getID () + "</type>");
		}

		buffer.append ("<containingtype>" + containingType.getID () + "</containingtype>");
		if ((modifiers & M_ABSTRACT) != 0)
		{
			buffer.append ("<modifier>");
			buffer.append ("abstract");
			buffer.append ("</modifier>");
		}
		if ((modifiers & M_FINAL) != 0)
		{
			buffer.append ("<modifier>");
			buffer.append ("final");
			buffer.append ("</modifier>");
		}
		if ((modifiers & M_STATIC ) != 0)
		{
			buffer.append ("<modifier>");
			buffer.append ("static");
			buffer.append ("</modifier>");
		}
		if ((modifiers & M_TRANSIENT ) != 0)
		{
			buffer.append ("<modifier>");
			buffer.append ("transient");
			buffer.append ("</modifier>");
		}
		if ((modifiers & M_VOLATILE ) != 0)
		{
			buffer.append ("<modifier>");
			buffer.append ("volatile");
			buffer.append ("</modifier>");
		}
		if ((modifiers & M_NATIVE ) != 0)
		{
			buffer.append ("<modifier>");
			buffer.append ("native");
			buffer.append ("</modifier>");
		}
		if ((modifiers & M_SYNCHRONIZED ) != 0) 
		{
			buffer.append ("<modifier>");
			buffer.append ("synchronized");
			buffer.append ("</modifier>");
		}

		buffer.append ("<descriptor>" + descriptor + "</descriptor>");
		buffer.append ("<issynthetic>" + (synthetic ? "yes" : "no") + "</issynthetic>");

		buffer.append ("</member>");

		return buffer.toString ();
    }

    // Fields

    /** Access level of member */
    private int accessLevel;

    /** Type of member */
    protected TypeEntry type;

    /** Type of object that contains member */
    private NamedReferenceType containingType;

    /** Modifier flags of member. */
    private int modifiers;

    /** Descriptor for member. */
    private String descriptor;

    /**
     * Flag that indicates if the member has the synthetic attribute. A
     * synthetic member is not declared in the source code, but is
     * added in the class file by the Java compiler. The default value
     * of the flag is false.
     */
    private boolean synthetic;

    /**
     * Signature of the member. A method signature is the method name follow
     * by a sequence of formal-parameter types.
     * A field signature is the field name followed by its type descriptor.
     * The signatures are constructed from the descriptors used in internally
     * in Java classfiles.
     */
    private String signature;
}
