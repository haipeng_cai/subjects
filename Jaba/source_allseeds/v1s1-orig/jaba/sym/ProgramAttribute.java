/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

/**
 * Interface for attributes of a program.
 * @author S. Sinha
 */
public interface ProgramAttribute extends Attribute
{
}
