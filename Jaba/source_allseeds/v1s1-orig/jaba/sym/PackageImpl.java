/* Copyright (c) 1999, The Ohio State University */

package jaba.sym;

import java.util.Vector;
import java.util.Enumeration;
import java.util.Stack;

/**
 * Class that represents a package entry into the symbol table.
 * @author Jay Lofstead 2003/06/05 added a toString method.
 * @author Jay Lofstead 2003/06/05 fixed to work for root packages
 * @author Jay Lofstead 2003/06/30 added implements Package
 * @author Jay Lofstead 2003/07/01 changed to implement Package
 */
public class PackageImpl extends SymbolEntryImpl implements Package
{
    // Constructor
    /**
     * Creates a new package entry object.
     */
    public PackageImpl () {
        packages = new Vector();
        containingPackage = null;
        classes = new Vector();
        interfaces = new Vector();
	setName (""); // for instances when no name is available
    }

    // Methods

    /**
     * Adds a package to the set of packages contained in this package.
     * @param p Package to be added.
     */
    public void addPackage( Package p ) {
        packages.add( p );
    }

    /**
     * Adds a class to the set of classes contained in this class.
     * @param c Class to be added.
     */
    public void addClass( Class c ) {
        classes.add( c );
    }

    /**
     * Adds an interface to the set of interfaces contained in this package.
     * @param i Interface to be added.
     */
    public void addInterface( Interface i ) {
        interfaces.add( i );
    }

    /**
     * Sets the containing package for this package.
     * @param p Containing package.
     */
    public void setContainingPackage( Package p ) {
        containingPackage = p;
    }

    /**
     * Returns an eumeration of the packages contained within this package.
     * @return Enumeration of contained packages.
     */
    public Enumeration getPackages() {
        return packages.elements();
    }

    /**
     * Returns an eumeration of the classes contained within this package.
     * @return Enumeration of contained classes.
     */
    public Enumeration getClasses() {
        return classes.elements();
    }

    /**
     * Returns an eumeration of the interfaces contained within this package.
     * @return Enumeration of contained interfaces.
     */
    public Enumeration getInterfaces() {
        return interfaces.elements();
    }

    /**
     * Returns the containing package for this package.
     * @return Containing package of this package.
     */
    public Package getContainingPackage() {
        return containingPackage;
    }

	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<package>");

		Stack s = new Stack ();

		s.push (this);

		Package p = null;
		if (containingPackage != null)
		{
			p = containingPackage;
			while (p != null)
			{
				s.push (p);
				p = p.getContainingPackage ();
			}
		}

		while (!s.isEmpty ())
		{
			p = (Package) s.pop ();

			buf.append (p.getName ());
			if (!s.isEmpty ())
			{
				buf.append (".");
			}
		}

		buf.append ("</package>");

		return buf.toString ();
	}

    // Fields
    /** Packages contained within this package. */
    private Vector packages;

    /** Package which contains this package. containingPackage is null for
        top-level packages. */
    private Package containingPackage;

    /** Classes contained within this package. */
    private Vector classes;

    /** Interfaces contained within this package. */
    private Vector interfaces;
}
