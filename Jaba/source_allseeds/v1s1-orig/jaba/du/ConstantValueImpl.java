/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.Primitive;
import jaba.sym.TypeEntry;

/**
 * Represents a constant that may appear on the operand stack.
 *
 * @author S. Sinha -- <i>Created</i>
 * @author Jim Jones -- <i>Revised, Feb. 6, 1999, Added value field</i>
 * @author Huaxing Wu -- <i>Revised, 7/8/02, add Method getType() </i>
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/14 changed to implement ConstantValue
 */
public class ConstantValueImpl implements HasValue, java.io.Serializable, ConstantValue
{
  /**
   * The value of this constant if this constant is an int, otherwise, it
   * is not guaranteed to be any particular value.
   */
  private int value;

  /** The type of this constant. */
  private boolean is64Bit;

  /** Indicates whether the <code>value</code> field was set as a valid value. */
  private boolean isValidValue;

    /** Primitive type of this constant value */
    private Primitive primitive;

  /**
   * Constructor.
   */
  public ConstantValueImpl ()
    {
      is64Bit = false;
      isValidValue = false;
    }

  /**
   * Assigns an integer value to this constant.
   * @param value  An integer value for this constant.
   */
  public void setValue( int value )
    {
      this.value = value;
    }

  /**
   * Returns the integer value for this constant if this constant is an,
   * if this constant is an integer, otherwise, it is not guaranteed to be
   * any particular value.
   * @return Integer value for this constant.
   */
  public int getValue()
    {
      return value;
    }

  /**
   * Sets the valid flag for the value.
   * @param isValid  Boolean indicating whether the <code>value</code> field
   *                 is a valid value.
   */
  public void setIsValidValue( boolean isValid )
    {
      isValidValue = isValid;
    }

  /**
   * Returns whether the <code>value</code> attribute is a valid value.
   * @return  Boolean indicating whether the <code>value</code> attribute is 
   *          a valid value.
   */
  public boolean getIsValidValue()
    {
      return isValidValue;
    }

  /**
   * Sets whether the constant is 64 bits wide.
   * @param isIt  Whether the constant is 64 bits wide.
   */
  public void setIs64Bit( boolean isIt )
    {
      is64Bit = isIt;
    }

  /**
   * Returns whether the constant is 64 bits wide.
   * @return  Boolean indicating whether the constant is 64 bits wide.
   */
  public boolean getIs64Bit()
    {
      return is64Bit;
    }

    /**
     * Sets the primitive type of the constant value.
     * @param primitive Primitive type of constant value.
     */
    public void setPrimitive( Primitive primitive )
    {
	this.primitive = primitive;
    }

    /**
     * Returns the primitive type of the constant value.
     * @return The primitive type of the constant value.
     */
    public Primitive getPrimitive() {
	return primitive;
    }

     /**
      * Returns the type of the constant value. Functionally same as 
      * getPrimitive().
      * @return The primitive type of the constant value.
      */
    public TypeEntry getType()
    {
	return primitive;
    }
}
