/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.LocalVariable;
import jaba.sym.Field;
import jaba.sym.TypeEntry;

/**
 * Represents a definition or a use of a reference data member.
 * @author Huaxing Wu -- <i> Revised 7/8/02, Change getType() to directly
 *                           call HasValue's getType() </i>
 * @author Huaxing Wu -- <i> Revised 7/9/02, Add hashCode() as required by
 *                           Object.hashCode() contact : hasCode() must 
 *                           overridden if equals() overridden</i>
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor</i> 
 * @author Jay Lofstead 2003/07/14 changed to an interface
 */
public interface ReferenceDefUse extends LeafDefUse, Cloneable
{
    /**
     * Returns the type of the DefUse object at this level.
     * @return  The type of the DefUse object at this level.
     */
    public TypeEntry getType ();

    /**
     * Indicated whether another ReferenceDefUse is equal to this one.  This
     * compares contents, not DefUse object references.
     * @param du  The DefUse object to compare this object with.
     */
    public boolean equals( Object du );

    /** Returns a hash code for object. The hash code is 
     *  the same as the containing variable
     * @return An int hash code 
     */
    public int hashCode();

    /**
     * Creates a clone of this object.
     * @return A clone of this object.
     */
    public Object clone();
    
    /**
     * Returns a string representation of this reference def/use object.
     * @return A string representation of this reference def/use object.
     */
    public String toString();
}
