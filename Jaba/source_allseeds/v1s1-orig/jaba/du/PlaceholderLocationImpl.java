/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.TypeEntry;

/**
 * Represents a nameless data location in the heap that is statically 
 * undeterminable (without at least further analysis).  Every
 * PlaceholderLocation will be unique, so a test for equality will always
 * return <code>false</code>, when in fact, they <b>may</b> refer to the same
 * memory location.  The intuition is that this is <i>some</i> heap location.
 * This is generally used to represent a reference of an element of an array.
 * @author Jim Jones -- <i>Created, May 3, 1999</i>.
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/14 changed to implement PlaceholderLocation
 */
public class PlaceholderLocationImpl implements HasValue, java.io.Serializable, PlaceholderLocation
{
    /**
     * The type of the location(s) that this represents.
     */
    private TypeEntry type;

    /**
     * Sets the type of the location(s) that this represents.
     * @param type  The type of the location(s) that this represents.
     */
    public void setType( TypeEntry type ) {
	this.type = type;
    }

    /**
     * Returns the type of the location(s) that this represents.
     * @return  The type of the location(s) that this represents.
     */
    public TypeEntry getType() {
	return type;
    }
}
