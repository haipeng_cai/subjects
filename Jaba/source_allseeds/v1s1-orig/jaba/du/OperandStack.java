/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.du.DefUse;
import jaba.du.ConstantValue;
import jaba.du.PrimitiveDefUse;

import java.util.Stack;
import java.util.Vector;
import java.util.EmptyStackException;
import java.util.Enumeration;

/**
 * Represents the Java operand stack. The CFG construction uses <code>
 * OperandStack </code> to determine DU for those bytecode instructions
 * that take their operands from the Java operand stack at run-time.
 * Each element on the stack represents a list of variable uses.
 *
 * @author S. Sinha -- <i>Created</i>
 * @author Jim Jones -- <i>Revised. Feb. 6, 1999. Changed 
 *                      stack elements from HasStack[] to DefUse[]</i>
 * @author Huaxing Wu -- <i> Revised. 7/15/02. Change System.exit to
 *                       throw RuntimeException in toString() </i>
 * @author Jay Lofstead 2003/06/02 converted use of elementAt to enumeration for better performance
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference methods.
 */
public class OperandStack implements Cloneable, java.io.Serializable
{
    // Fields

    /** Stack with elements of type Vector of <code>DefUse</code>. */
    private Stack opStack;

    // Constructor

    /**
     * Creates a new, empty <code>OperandStack</code> object.
     */
    public OperandStack() {
        opStack = new Stack();
    }

    // Methods

    /**
     * Pushes a new element onto the operand stack.
     * @param item Element to push onto stack.
     */
    public void push( Vector item ) {
        opStack.push( item );
    }

    /**
     * Pops the top element off the stack. Returns the popped element.
     * @return Top element from the operand stack.
     * @throws EmptyStackException if operand stack is empty.
     */
    public Vector pop() throws EmptyStackException {
        return (Vector)opStack.pop();
    }

    /**
     * Returns the top element, without removing it from the stack.
     * @return Top element from the operand stack.
     * @throws EmptyStackException if operand stack is empty.
     */
    public Vector peek() throws EmptyStackException {
        return (Vector)opStack.peek();
    }

    /**
     * Checks if the operand stack is empty.
     * @return <code>true</code> if the operand stack is empty; <code>
     *         false</code> otherwise.
     */
    public boolean isEmpty() {
        return opStack.empty();
    }

  /**
   * Returns the depth of the operand stack.  Added for debugging purposes.
   * @return Depth of the operand stack.
   */
  public int getDepth()
    {
      return opStack.size();
    }

  /**
   * Pops top <code>n</code> elements from the operand stack, merges them,
   * and pushes the merged element onto the stack.
   * @param n number of stack elements to merge.
   * @return the merged element.
   * @throws EmptyStackException if operand stack contains less than
   *                             <code>n</code> elements.
   */
  public Vector merge( int n ) {
    Vector items = new Vector();
    for ( int i=0; i<n; i++ ) {
      items.addAll( pop() );  // preserve order of pushes
    }
    boolean containsNonConst = false;
    PrimitiveDefUse wideDU = null;
    for (Enumeration e = items.elements (); e.hasMoreElements ();)
      {
	DefUse item = (DefUse) e.nextElement ();
	if ( !(item instanceof PrimitiveDefUse ) ||
	     !( ((PrimitiveDefUse)item).getVariable() instanceof
		ConstantValue ) )
	  {
	    containsNonConst = true;
	    break;
	  } 
	else 
	  {
	    if ( ((ConstantValue)((PrimitiveDefUse)item).getVariable()).
		 getIs64Bit() )
	      wideDU = (PrimitiveDefUse)item;
	  }
      }
    if ( containsNonConst )
      push( purge( items ) );
    else
      {
	if ( wideDU != null )
	  {
	    items = new Vector();
	    items.add( wideDU );
	  }
	else
	  {
	    PrimitiveDefUse tempDU = (PrimitiveDefUse)items.elementAt(0);
	    items = new Vector();
	    items.add( tempDU );
	  } 
	push( items );
      }
    return peek();
  }

  /**
   * Removes elements that are not relevant for DU from a list of 
   * <code>DefUse</code> objects.
   * @param v  Vector of <code>DefUse</code> objects to remove irrelevant
   *           elements from.
   * @return  Updated Vector of <code>DefUse</code>.
   */
  private Vector purge( Vector v )
    {
      Vector returnVec = new Vector();
      DefUse element = null;
      for (Enumeration e = v.elements (); e.hasMoreElements ();)
	{
	  element = (DefUse) e.nextElement ();
	  if ( !( element instanceof PrimitiveDefUse ) ||
	       !( ((PrimitiveDefUse)element).getVariable() instanceof 
	       ConstantValue ) )
	    {
	      returnVec.add( element );
	    }
	}
      return returnVec;
    }

  /**
   * Returns a clone of this object.
   * @return A clone of this object.
   */
  public Object clone()
    {
      OperandStack newOperandStack = null;
      try
	{
	  newOperandStack = (OperandStack)super.clone();
	  newOperandStack.opStack = (Stack)opStack.clone();
	}catch ( CloneNotSupportedException e){
	    e.printStackTrace();
	    throw new RuntimeException( "CloneNotSupported Exception: " + e );
	}

      return newOperandStack;
      
    }

  /**
   * Returns a string representation of this operand stack.
   * @return A string representation of this operand stack.
   */
  public String toString()
    {
      StringBuffer buf = new StringBuffer();
      Enumeration stackElements = opStack.elements();
      if ( !stackElements.hasMoreElements() )
	buf.append( "empty" );
      while ( stackElements.hasMoreElements() )
	{
	  Vector stackElement = (Vector)stackElements.nextElement();
	  Enumeration vecElements = stackElement.elements();
	  if ( !vecElements.hasMoreElements() ) {
	      throw new RuntimeException("Position on operand stack that is empty"); 
	  }
	  DefUse vecElement = (DefUse)vecElements.nextElement();
	  buf.append( vecElement.toString() );
	  while ( vecElements.hasMoreElements() )
	    {
	      buf.append( ", " );
	      vecElement = (DefUse)vecElements.nextElement();
	      buf.append( vecElement.toString() );
	    }
	  if ( stackElements.hasMoreElements() )
	    buf.append( " | " );

	}
      return buf.toString();
    }

}
