package jaba.du;

import jaba.sym.TypeEntry;

/**
 * Represents a non-leaf defintion/use object in a definition/use tree.
 * @author Huaxing Wu -- <i> Revise 7/3/02 Add a constructor, taking parameter: element </i>
 * @author Jay Lofstead 2003/07/01 changed to extend DefUseImpl
 * @author Jay Lofstead 2003/07/14 changed to implement RecursiveDefUse
 */
public abstract class RecursiveDefUseImpl extends DefUseImpl implements RecursiveDefUse
{
  /**
   * The element of this def/use.  This field gives rise to the recursive
   * nature of this class.
   */
    private DefUse element;

    /** ??? */
    public RecursiveDefUseImpl (DefUse element)
    {
	this.element = element;
    }

  /**
   * Returns the element of this def/use.
   * @return The element of this def/use.
   */
  public DefUse getElement()
    {
      return element;
    }


  /**
   * Assigns the element of this def/use.
   * @param element  The element of this def/use.
   */
  public void setElement( DefUse element )
    {
      this.element = element;
    }


  /**
   * Returns the leaf element of this def-use tree or subtree.
   * @return The leaf element of this def-use tree or subtree.
   */
  public LeafDefUse getLeafElement()
    {
      DefUse element = getElement();
      if ( element instanceof LeafDefUse )
	return (LeafDefUse)element;
      else
	return ((RecursiveDefUse)element).getLeafElement();
    }


  /**
   * Returns the def/use element just above the leaf.
   * @return The def/use element just above the leaf element.
   */
  public RecursiveDefUse getLastRecursiveElement()
    {
      DefUse element = getElement();
      if ( element instanceof LeafDefUse )
	return this;
      else
	return ((RecursiveDefUse)element).getLastRecursiveElement();
    }

  /**
   * Creates a clone of this object.
   * @return A clone of this object.
   */
  public Object clone()
    {
      RecursiveDefUseImpl newDU = null;
      try
	{
	  newDU = (RecursiveDefUseImpl) super.clone();
	  if ( element instanceof PrimitiveDefUse )
	    newDU.element = (DefUse)((PrimitiveDefUse)element).clone();
	  else if ( element instanceof ReferenceDefUse )
	    newDU.element = (DefUse)((ReferenceDefUse)element).clone();
	  else // if ( element instanceof RecursiveDefUse )
	    newDU.element = (DefUse)((RecursiveDefUse)element).clone();
	}catch ( CloneNotSupportedException e){
	    e.printStackTrace();
	    throw new RuntimeException( "CloneNotSupportedException: " + e );
	}

      return newDU;

    }

  /**
   * Returns an array of DefUse object that are parent DefUse trees of this.
   * @return An array of DefUse object that are parent DefUse trees of this.
   */
  public abstract DefUse[] getParentDefUses();

    /**
     * Returns the type of the entire DefUse tree using this as the
     * root.  The type of the entire tree is the type of the leaf --
     * this method returns the type of the leaf.
     * @return The type of the entire DefUse tree beneath the DefUse
     *         object on which this method is called.
     */
    public TypeEntry getTreeType() {
	return getLeafElement().getType();
    }
}
