package jaba.du;

import jaba.sym.TypeEntry;
import jaba.sym.ReferenceType;
import jaba.sym.LocalVariable;
import jaba.sym.Field;
import jaba.du.HasValue;
import jaba.du.TempVariable;

/**
 * Represents a casted reference element definition or use.
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor</i>
 * @author Jay Lofstead 2003/07/14 changed to implement CastedReferenceElementDefUse
 */
public class CastedReferenceElementDefUseImpl extends ReferenceElementDefUseImpl implements CastedReferenceElementDefUse
{
    /** The casted type of this level of the DefUse tree. */
    ReferenceType type;

    /**
     * Construct a CastedReferenceElementDefUse. E.g Expression (A)b.c,
     * type = A, object = b, element = c;
     * @param type the casted ReferenceType
     * @param object The reference
     * @param element the field
     */
    public CastedReferenceElementDefUseImpl (ReferenceType type, HasValue object, 
					DefUse element) {
	super(object, element);
	this.type = type;
    }

    /**
     * Sets the casted type of this level of the DefUse tree.
     * @param type  The casted type of this level of the DefUse tree.
     */
    public void setType( ReferenceType type ) {
	this.type = type;
    }


    /**
     * Returns the casted type of this level of the DefUse tree.
     * @return The casted type of this level of the DefUse tree.
     */
    public TypeEntry getType ()
    {
	return type;
    }
    
    
    /**
     * Returns a string representation of this casted reference element 
     * def/use object.
     * @return A string representation of this casted reference element 
     *         def/use object.
     */
    public String toString()
    {
	StringBuffer buf = new StringBuffer();

	buf.append ("<castedreferenceelementdefuse>");
	buf.append( "( o=" );
	HasValue object = getObject();
	String objType = object.getClass().getName();
	buf.append( objType.substring( objType.lastIndexOf(".") + 1, 
				       objType.length() ) );
	if ( !( object instanceof TempVariable ) &&
	     ( object instanceof LocalVariable ) )
	    buf.append( "( " + ((LocalVariable)object).getName() + " )" );
	else if ( object instanceof Field )
	    buf.append( "( " + ((Field)object).getName() + " )" );
	buf.append( ", t=" );
	buf.append( type.getName() + " ), " );
	buf.append( "e=" + getElement().toString() + " )" );

	buf.append ("</castedreferenceelementdefuse>");

	return buf.toString();
    }
    
}
