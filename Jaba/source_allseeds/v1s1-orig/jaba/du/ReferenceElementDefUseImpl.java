/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.LocalVariable;
import jaba.sym.Field;
import jaba.sym.TypeEntry;

/**
 * Represents a definition or a use to a named reference element.
 * @author Huaxing Wu -- <i> Revised 7/8/02, Change getType() to directly
 *                           call HasValue's getType() 
 * @author Huaxing Wu -- <i> Revised 7/9/02, Add hashCode() as required by
 *                           Object.hashCode() contact: hasCode() must 
 *                           overridden if equals() overridden</i>
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor, bug fixing in
 *                           getParentDefUses()</i>
 * @author Jay Lofstead 2003/07/14 changed to implement ReferenceElementDefUse
 */
public class ReferenceElementDefUseImpl extends RecursiveDefUseImpl implements ReferenceElementDefUse
{
  /** The object that is being dereferenced to reach the element. */
  private HasValue object;

    /**
     * Construct a ReferenceElementDefuse with this object and element. 
     * e.g, For expression a.b, object = a, Element = b
     * @param object The reference
     * @param element the field
     */
    public ReferenceElementDefUseImpl (HasValue object, DefUse element) {
	super(element);
	this.object = object;
    }

    /**
     * Returns the type of the DefUse object at this level.
     * @return  The type of the DefUse object at this level.
     */
    public TypeEntry getType() {
	return object.getType();
    }


  /**
   * Returns the object that is being dereferenced to reach the element.
   * @return The object that is being dereferenced to reach the element.
   */
  public HasValue getObject()
    {
      return object;
    }


  /**
   * Assigns the object that is being dereferenced to reach the element.
   * @param object  The object that is being dereferenced to reach the element.
   */
  public void setObject( HasValue object )
    {
      this.object = object;
    }

  
  /**
   * Returns an array of DefUse object that are parent DefUse trees of this.
   * @return An array of DefUse object that are parent DefUse trees of this.
   */
  public DefUse[] getParentDefUses()
    {
      DefUse[] returnVal = null;
      // if my element is a recursive def use tree, call getParentDefUses on it
      DefUse element = getElement();
      if ( element instanceof RecursiveDefUse )
	{
	  DefUse[] elementParents = 
	    ((RecursiveDefUse)element).getParentDefUses();

	  // allocate the array to return the result -- the size of the 
	  // parents of the element plus one (for the parent of this RE)
	  returnVal = new DefUse[ elementParents.length + 1 ];

	  // loop through each of the parents of the element and create a
	  // ReferenceElementDefUse with each of these items as elements (and
	  // all other attributes of this RE
	  for ( int i = 0; i < elementParents.length; i++ )
	    {
		DefUse elementParent = elementParents[i];
		ReferenceElementDefUse re = 
		    new ReferenceElementDefUseImpl (object, elementParent);
		returnVal[i] = re;
	      // Constructor change
	//       ReferenceElementDefUse re = new ReferenceElementDefUse();
// 	      re.setObject( object );
// 	      re.setElement( elementParent );
	    }

	}
      else
	returnVal = new DefUse[1];
	 
      // create reference def use for the parent of this RE
      ReferenceDefUse ref = new ReferenceDefUseImpl (object);
      //  ref.setVariable( object );
      returnVal[ returnVal.length - 1 ] = ref;

      return returnVal;

    }
  
  /**
   * Indicated whether another ReferenceElementDefUse is equal to this one.  
   * This compares contents, not DefUse object references.
   * @param du  The DefUse object to compare this object with.
   */
  public boolean equals( Object du )
    {
      if ( !(du instanceof ReferenceElementDefUse ) ) return false;

      ReferenceElementDefUse redu = (ReferenceElementDefUse)du;

      return object.equals(redu.getObject()) &&
	  getElement().equals( redu.getElement());
	  //      if ( !object.equals( redu.getObject() ) ) return false;

	  //      return getElement().equals( redu.getElement() );

    }

    /**
     * Returns a hash code for this object. 
     * The hash code = 31 * object.hashCode() + element.hashCode() 
     * @return An int hash code 
     */
    public int hashCode() {
	return 31 * object.hashCode() + getElement().hashCode();
    }

  /**
   * Returns a string representation of this reference element def/use object.
   * @return A string representation of this reference element def/use object.
   */
  public String toString()
    {
      StringBuffer buf = new StringBuffer();

	buf.append ("<referenceelementdefuse>");
      buf.append( "( " );
      buf.append( "o=" );
      String objType = object.getClass().getName();
      buf.append( objType.substring( objType.lastIndexOf(".") + 1, 
				     objType.length() ) );
      if ( !( object instanceof TempVariable ) &&
	   ( object instanceof LocalVariable ) )
	buf.append( "( " + ((LocalVariable)object).getName() + " )" );
      else if ( object instanceof Field )
	buf.append( "( " + ((Field)object).getName() + " )" );
      buf.append( ", " );
      buf.append( "e=" + getElement().toString() + " )" );

	buf.append ("</referenceelementdefuse>");

      return buf.toString();
    }
}
