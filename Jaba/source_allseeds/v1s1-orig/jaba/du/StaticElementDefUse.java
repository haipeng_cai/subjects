/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.NamedReferenceType;
import jaba.sym.TypeEntry;

/**
 * Represents a definition or use of a static (class) element.
 * @author Huaxing Wu -- <i> Revised 7/9/02, Add hashCode() as required by
 *                       Object.hashCode() contact: hasCode() must overridden 
 *                       if equals() overridden</i>
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor, bug fixing in
 *                           getParentDefUses()</i>
 * @author Jay Lofstead 2003/07/14 changed to an interface
 */
public interface StaticElementDefUse extends RecursiveDefUse
{
  /**
   * Returns the type of the class or interface that contains the static 
   * element being referenced or defined.
   * @return The type of the class or interface that contains the static 
   *         element being referenced or defined.
   */
  public TypeEntry getType();

  /**
   * Assigns the typeof the class or interface that contains the static 
   * element being referenced or defined.
   * @param type  The type of the class or interface that contains the static 
   *              element being referenced or defined.
   */
  public void setType( NamedReferenceType type );

  /**
   * Returns an array of DefUse object that are parent DefUse trees of this.
   * @return An array of DefUse object that are parent DefUse trees of this.
   */
  public DefUse[] getParentDefUses();

  /**
   * Indicated whether another StaticElementDefUse is equal to this one.  This
   * compares contents, not DefUse object references.
   * @param du  The DefUse object to compare this object with.
   */
  public boolean equals( Object du );

    /**
     * Returns a hash code for this object. 
     * The hash code = 31 * type.hashCode() + element.hashCode() 
     * @return An int hash code 
     */
    public int hashCode();

  /**
   * Returns a string representation of this static element def/use object.
   * @return A string representation of this static element def/use object.
   */
  public String toString();
}
