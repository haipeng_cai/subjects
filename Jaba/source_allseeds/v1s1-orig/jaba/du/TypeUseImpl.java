package jaba.du;

import jaba.sym.TypeEntry;
import jaba.sym.ReferenceType;

/**
 * Represents a use of the type of a named reference element in an
 * <code>instanceof</code>.  This type of use is differentiated from others
 * because the object of the <code>instanceof</code> is not used, but its
 * <i>type</i> is.
 * @author Jay Lofstead 2003/06/12 removed redundant use of this to reference a member
 * @author Jay Lofstead 2003/07/14 changed to implement TypeUse
 */
public class TypeUseImpl extends RecursiveDefUseImpl implements TypeUse
{
    /** The type used in the the <code>instanceof</code> instruction. */
    private ReferenceType checkedType;

    /**
     * Construct a TypeUse with the type and the element.
     * E.G: a instanceof String, type =String, element = a
     * @param type -- the type that has been used, can be a Class or interface.
     * @param element -- The object whose type is being Checking.
     */
    public TypeUseImpl (ReferenceType type, DefUse element)
    {
	super(element);
	checkedType = type;
    }

    /**
     * Sets the type used in the <code>instanceof</code> instruction.
     * @param type  The type used in the <code>instanceof</code> instruction.
     */
    public void __setCheckedType( ReferenceType type )
    {
	checkedType = type;
    }


    /**
     * Returns the type used in the <code>instanceof</code> instruction.
     * @return The type used in the <code>instanceof</code> instruction.
     */
    public ReferenceType getCheckedType()
    {
	return checkedType;
    }


    /**
     * Returns the type of this level of the DefUse tree.  Because a
     * TypeUse really doesn't have a "type", this returns the type of
     * its element.
     * @return The type of this level of the DefUse tree.
     */
    public TypeEntry getType()
    {
	return getElement().getType();
    }


    /**
     * Returns an array of DefUse object that are parent DefUse trees of this.
     * @return An array of DefUse object that are parent DefUse trees of this.
     */
    public DefUse[] getParentDefUses()
    {
	DefUse element = getElement();
	if ( element instanceof RecursiveDefUse )
	    return ((RecursiveDefUse)element).getParentDefUses();
	else
	    return new DefUse[] { element };
    }


    /**
     * Returns a string representation of this type use object.
     * @return A string representation of this type use object.
     */
    public String toString()
    {
	return "<typeuse>" + "ct=" + checkedType.getName() + ", e=" + getElement() + "</typeuse>";
    }

    /** ??? */
    public boolean equals( Object du )
    {
	if ( !(du instanceof TypeUse ) )
		return false;

	TypeUse tu = (TypeUse) du;

	if ( checkedType != tu.getCheckedType() )
		return false;

	return getElement().equals( tu.getElement() );
    }
}
