/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.LocalVariable;
import jaba.sym.Field;
import jaba.sym.TypeEntry;

/**
 * Represents a definition or a use to a named reference element.
 * @author Huaxing Wu -- <i> Revised 7/8/02, Change getType() to directly
 *                           call HasValue's getType() 
 * @author Huaxing Wu -- <i> Revised 7/9/02, Add hashCode() as required by
 *                           Object.hashCode() contact: hasCode() must 
 *                           overridden if equals() overridden</i>
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor, bug fixing in
 *                           getParentDefUses()</i>
 * @author Jay Lofstead 2003/07/14 changed to an interface
 */
public interface ReferenceElementDefUse extends RecursiveDefUse
{
    /**
     * Returns the type of the DefUse object at this level.
     * @return  The type of the DefUse object at this level.
     */
    public TypeEntry getType();

  /**
   * Returns the object that is being dereferenced to reach the element.
   * @return The object that is being dereferenced to reach the element.
   */
  public HasValue getObject();

  /**
   * Assigns the object that is being dereferenced to reach the element.
   * @param object  The object that is being dereferenced to reach the element.
   */
  public void setObject( HasValue object );
  
  /**
   * Returns an array of DefUse object that are parent DefUse trees of this.
   * @return An array of DefUse object that are parent DefUse trees of this.
   */
  public DefUse[] getParentDefUses();
  
  /**
   * Indicated whether another ReferenceElementDefUse is equal to this one.  
   * This compares contents, not DefUse object references.
   * @param du  The DefUse object to compare this object with.
   */
  public boolean equals( Object du );

    /**
     * Returns a hash code for this object. 
     * The hash code = 31 * object.hashCode() + element.hashCode() 
     * @return An int hash code 
     */
    public int hashCode();

  /**
   * Returns a string representation of this reference element def/use object.
   * @return A string representation of this reference element def/use object.
   */
  public String toString();
}
