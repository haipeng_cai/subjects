package jaba.du;

import jaba.sym.TypeEntry;

/**
 * This class represents the bottom-most level of any def-use tree.  This
 * class is either a primitive or a reference def-use.
 * @author Jim Jones -- <i>Created, March 1999</i>.
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor</i> 
 * @author Jay Lofstead 2003/07/01 changed to extend DefUseImpl
 * @author Jay Lofstead 2003/07/14 changed to an interface
 */
public interface LeafDefUse extends DefUse
{
    /**
     * Returns the data element that is being defined or used.
     * @return The data element that is being defined or used.
     */
    public HasValue getVariable();
    
    /**
     * Returns the type of the entire DefUse tree using this as the
     * root.  The type of the entire tree is the type of the leaf --
     * this method returns the type of the leaf.
     * @return The type of the entire DefUse tree beneath the DefUse
     *         object on which this method is called.
     */
    public TypeEntry getTreeType ();
    
    /**
     * Assigns the data element that is being defined or used.
     * @param variable  The data element that is being defined or used.
     */
    public void setVariable( HasValue variable );
}
