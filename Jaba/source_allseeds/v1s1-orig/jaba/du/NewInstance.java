/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.TypeEntry;

/**
 * Represents a nameless data location in the heap created by a 
 * <code>new</code> instruction.
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/07/14 changed to an interface
 */
public interface NewInstance extends HasValue
{
  /**
   * Sets the type for this heap location.
   * @param t Type of this heap location.
   */
  public void setType( TypeEntry t );
  
  /**
   * Returns the type of this heap location.
   * @return Type of this heap location.
   */
  public TypeEntry getType();
}
