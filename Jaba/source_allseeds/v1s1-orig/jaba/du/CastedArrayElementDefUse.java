package jaba.du;

import jaba.sym.TypeEntry;
import jaba.sym.ReferenceType;

/**
 * Represents a casted array element definition or use.
 * @author Huaxing Wu -- <i> Revised 7/10/02, Add Constructor</i>
 * @author Jay Lofstead 2003/07/14 changed to an interface
 */
public interface CastedArrayElementDefUse extends ArrayElementDefUse
{
    /**
     * Sets the casted type of this level of the DefUse tree.
     * @param type  The casted type of this level of the DefUse tree.
     */
    public void setType( ReferenceType type );

    /**
     * Returns the casted type of this level of the DefUse tree.
     * @return The casted type of this level of the DefUse tree.
     */
    public TypeEntry getType ();

    /**
     * Returns a string representation of this casted array element def/use 
     * object.
     * @return A string representation of this casted array element def/use 
     *         object.
     */
    public String toString();
}
