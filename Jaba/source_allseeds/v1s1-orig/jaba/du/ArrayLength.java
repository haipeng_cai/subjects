/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import jaba.sym.Primitive;
import jaba.sym.PrimitiveImpl;
import jaba.sym.TypeEntry;

/**
 * Represents the length of an array
 * @author Huaxing Wu -- <i>Created 7/23/02 </i>
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 */
public class ArrayLength implements HasValue, java.io.Serializable
{

    /** The Array variable */
    private HasValue array;

    /** The type will always be int */
    private static final Primitive type = PrimitiveImpl.valueOf(Primitive.INT); 

    /** Construct an ArrayLength Object for the specified array.
     *@param array -- an Array
     */
    public ArrayLength(HasValue array) {
	this.array = array;
    }

    /** Return the type, which should always be Primitive(INT).
     *@return type -- the type of ArrayLength, will be Primitive(INT)
     */
    public TypeEntry getType() {
	return type;
    }

    /** return the array of this arraylength object.
     *@return the array
     */
    public HasValue getArray() { return array;}
}
