/* Copyright (c) 1999, The Ohio State University */

package jaba.du;

import java.util.Vector;

/**
 * A DU association maps a variable definition to variables that contribute
 * to the value of that definition. A DU association is created for a
 * <code>SymbolicInstruction</code>, and only contains information local
 * to a CFG node.
 *
 * @see jaba.instruction.SymbolicInstruction
 * @author S. Sinha
 * @author Jay Lofstead 2003/06/02 replaced use of elementAt with enumeration for better performance
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 changed to not have array/vector mismatch bugs
 */
public class DUAssociation implements java.io.Serializable
{
    // Fields

    /** Variable that is defined. */
    private HasValue definition;

    /** Variables that contribute to the definition. */
    private Vector uses;

    // Constructor

    /**
     * Creates an empty DUAssociation object.
     */
    public DUAssociation() {
        uses = new Vector();
    }

    // Methods

    /**
     * Sets the variable being defined in a DU association.
     * @param def variable being defined.
     */
    public void setDefinition( HasValue def ) {
        definition = def;
    }

    /**
     * Adds a use to the set of uses in a DU association.
     * @param use Variable that is used to compute the definition.
     */
    public void addUse( HasValue use ) {
        uses.add( use );
    }

    /**
     * Returns the variable that is defined.
     * @return Variable being defined.
     */
    public HasValue getDefinition() {
        return definition;
    }

    /**
     * Returns uses for this <code>DUAssociation</code>.
     * @return Uses for this <code>DUAssociation</code>.
     */
    public DefUse [] getUses()
    {
	return (DefUse []) uses.toArray (new DefUse [uses.size ()]);
    }
}
