package jaba.main;

import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;

import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.File;

/**
 * A Resources object that pre-loads all of the .class files allowing transmission to the server
 * for remote processing.
 *
 * @author Jay Lofstead 2003/07/01
 */
public class ResourceBundle extends ResourceFile
{
	/** create a new ResourceBundle without reading from a file */
	public ResourceBundle () throws IllegalArgumentException
	{
		super ();
	}

	/** create a new ResourceBundle from a resource file */
	public ResourceBundle (String filename) throws IllegalArgumentException
	{
		super (filename);

		loadInitialBinaries ();
	}

	/** Adds an entry to the ClassPath resource list. */
	public void addClassPath (String path)
	{
		int dirCount = dirs.size ();

		super.addClassPath (path);

		if (dirs.size () > dirCount)
		{
			loadBinaryDir ();
		}
		else
		{
			loadBinaryZip ();
		}
	}

	/**
	 * Searches for a class in the class paths.
	 * @param className the name of the class
	 * @return An inputstream for the file
	 */ 
	public InputStream getInputStream (String className) throws IOException
	{
		className = className.replace ('.', File.separatorChar) + ".class";
		if (dirs == null)
		{
			dirs = new LinkedList ();
			zips = new LinkedList ();

			initPathFiles (classPaths, dirs, zips);
		}

		return createStream (className);
	}

	/** remove any temporary objects to shrink memory usage.  Generally called after symbol table built. */
	public void cleanup ()
	{
		super.cleanup ();

		dirImplementations = null;
		zipImplementations = null;
	}

	/** load the binaries specified into the internal data structure */
	private void loadInitialBinaries () throws IllegalArgumentException
	{
		dirs = new LinkedList ();
		zips = new LinkedList ();

		initPathFiles (classPaths, dirs, zips);

// TODO: need to treat dirs like zip files
		for (Iterator i = dirs.iterator (); i.hasNext ();)
		{
			File pdf = new File (i.next ().toString ());
			if (pdf != null)
			{
				try
				{
					FileInputStream fis = new FileInputStream (pdf);

					byte [] b = new byte [(int) pdf.length ()];
					fis.read (b, 0, b.length);
					fis.close ();

					dirImplementations.add (b);
				}
				catch (IOException e)
				{
					throw (new IllegalArgumentException (e.getMessage ()));
				}
			}
		}

		for (Iterator i = zips.iterator (); i.hasNext ();)
		{
			String s = null;
			File pzf = new File ((s = i.next ().toString ()));
			if (pzf != null)
			{
				try
				{
					FileInputStream fis = new FileInputStream (pzf);

					byte [] b = new byte [(int) pzf.length ()];
					fis.read (b, 0, b.length);
					fis.close ();

					zipImplementations.add (b);
				}
				catch (IOException e)
				{
					throw (new IllegalArgumentException (e.getMessage ()));
				}
			}
		}
	}

	/** load the binary specified into the LinkedList */
	private void loadBinaryDir ()
	{
// TODO: need to treat dirs like zip files
		File pdf = new File ((String) dirs.get (dirs.size ()));
		if (pdf != null)
		{
			try
			{
				FileInputStream fis = new FileInputStream (pdf);

				byte [] b = new byte [(int) pdf.length ()];
				fis.read (b, 0, b.length);
				fis.close ();

				dirImplementations.add (b);
			}
			catch (IOException e)
			{
				throw (new IllegalArgumentException (e.getMessage ()));
			}
		}
	}

	/** load the binary specified into the LinkedList */
	private void loadBinaryZip ()
	{
		File pzf = new File ((String) zips.get (dirs.size ()));
		if (pzf != null)
		{
			try
			{
				FileInputStream fis = new FileInputStream (pzf);

				byte [] b = new byte [(int) pzf.length ()];
				fis.read (b, 0, b.length);
				fis.close ();

				zipImplementations.add (b);
			}
			catch (IOException e)
			{
				throw (new IllegalArgumentException (e.getMessage ()));
			}
		}
	}

	/** create an input stream from the internally stored */
	private InputStream createStream (String className) throws IOException
	{
// TODO: need to treat dirs like zip files
		Iterator di = dirImplementations.iterator ();
		for (Iterator pd = dirs.iterator (); pd.hasNext ();)
		{
			byte [] b = (byte []) di.next ();
			if (pd.next ().toString ().equals (className))
			{
				return new ByteArrayInputStream (b);
			}
		}

		Iterator dz = zipImplementations.iterator ();
		for (Iterator pz = zips.iterator (); pz.hasNext ();)
		{
			pz.next ();
			byte [] b1 = (byte []) dz.next ();
			ZipInputStream pzf = new ZipInputStream (new ByteArrayInputStream (b1));
			if (pzf != null)
			{
				ZipEntry ze = null;
				while ((ze = pzf.getNextEntry ()) != null)
				{
					if (ze.getName ().equals (className))
					{
						// we can't rely on the size of the entry so must read and assemble
						byte [] b = readStream (pzf);
						pzf.close ();
						return new ByteArrayInputStream (b);
					}
					pzf.closeEntry ();
				}
			}
		}

		throw new FileNotFoundException (className);
	}

	/** helper method to read a byte [] from an InputStream */
	private byte [] readStream (InputStream is)
	{
		int READ_SIZE = 4000;
		boolean done = false;
		byte [] b = null;
		byte [] temp = new byte [READ_SIZE];

		while (!done)
		{
			int bytes_read = -1;

			try
			{
				bytes_read = is.read (temp, 0, temp.length);
			}
			catch (java.io.IOException ioe)
			{
				ioe.printStackTrace ();
			}

			if (bytes_read != -1)
			{
				if (b == null)
				{
					b = new byte [bytes_read];

					for (int j = 0; j < bytes_read; j++)
					{
						 b [j] = temp [j];
					}
				}
				else
				{
					int new_size = b.length + bytes_read;
					byte [] t = new byte [new_size];

					int k = 0;
					for (; k < b.length; k++)
					{
						t [k] = b [k];
					}
					for (int j = 0; j < bytes_read; j++)
					{
						t [k] = temp [j];
						k++;
					}

					b = t;
				}

				if (bytes_read < READ_SIZE)
				{
					//done = true;
				}
			}
			else
			{
				done = true;
			}
		}

		return b;
	}

	/** a list of byte [] representing each of the resources specified */
	List dirImplementations = new LinkedList ();

	/** a list of byte [] representing each of the resources specified */
	List zipImplementations = new LinkedList ();
}