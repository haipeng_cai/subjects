package jaba.main;

import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.StringTokenizer;

import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;

/**
 * ResourceInterface.java
 *
 * Created on September 10, 2001, 8:03 PM
 *
 * @author  MANSOUR
 * @author Huaxing Wu -- <i> Revised 3/21/02, add method
 * 			getInputStream() and related fields. Remove obsolete fields
 * 			and methods, change the implementation of getClasspaths() and
 * 			getClassNames() to take advantage of vector's toArray() method.
 * @author Jay Lofstead 2003/06/05 changed to use enumerations
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/09 code cleanup
 * @author Jay Lofstead 2003/06/11 merged with main branch
 * @author Jay Lofstead 2003/06/23 added new required methods from ResourceFileI, code cleanup
 */
public class GenericResource implements ResourceFileI, java.io.Serializable
{
	/** Constructor */
	protected GenericResource ()
	{
		init ();
	}

	/** Constructor */
	protected GenericResource (String progName)
	{
		init ();
		programName = progName;
	}

	/** shared initialization method for all constructors */
	private void init ()
	{
		classPaths = new LinkedList ();
		classFiles = new LinkedList ();
		sourceClassPaths = new LinkedList ();
		dottyOutputParams = new DottyOutputSpec ();
	}

	/**
	 * Searches for a class in the class paths.
	 * @param className the name of the class
	 * @return An inputstream for the file
	 */ 
	public InputStream getInputStream (String className) throws IOException
	{
		className = className.replace ('.', File.separatorChar) + ".class";
		if (dirs == null)
		{
			dirs = new LinkedList ();
			zips = new LinkedList ();

			initPathFiles (classPaths, dirs, zips);
		}

		return createInputStream (className, dirs, zips);
	}

	/**
	 * Searches for a source file in the source class path.
	 * @param sourceName the name of the source file
	 * @return An inputstream for the file
	 */ 
	public InputStream getSourceInputStream (String sourceName) throws IOException
	{
		sourceName = sourceName.replace ('.', File.separatorChar) + ".java";
		if (sourceDirs == null)
		{
			sourceDirs = new LinkedList ();
			sourceZips = new LinkedList ();

			initPathFiles (sourceClassPaths, sourceDirs, sourceZips);
		}

		return createInputStream (sourceName, sourceDirs, sourceZips);
	}

	/**
	 * Returns the program name specified in the resource file.
	 * @return  The program name specified in the resource file.
	 */
	public String getProgramName ()
	{
		return programName;
	}
    
	/**
	 * Returns an array of strings, each representing a path to be used to find
	 * class files to be analyzed.
	 * @return  An array of strings, each representing a path to be used to find
	 *          class files to be analyzed.
	 */
	public List getClassPathsList ()
	{
		return classPaths;
	}

	/**
	 * Convenience method for converting the List to an Array (less efficient) from <code>getClassPathsList</code>
	 * @return  An array of strings, each representing a path to be used to find
	 *          class files to be analyzed.
	 */
	public String [] getClassPaths ()
	{
		return (String []) classPaths.toArray (new String [classPaths.size ()]);
	}

	/**
	 * Returns an array of strings, each representing a path to be
	 * used to find source files of the class files that are analyzed.
	 * @return An array of strings, each representing a path to be
	 * used to find source files of the class files that are analyzed.
	 */
	public List getSourceClassPathsList ()
	{
		return sourceClassPaths;
	}
    
	/**
	 * Convenience method for converting the List to an Array (less efficient) from <code>getSourceClassPathsList</code>
	 * @return An array of strings, each representing a path to be
	 * used to find source files of the class files that are analyzed.
	 */
	public String [] getSourceClassPaths ()
	{
		return (String []) sourceClassPaths.toArray (new String [sourceClassPaths.size ()]);
	}
    
	/**
	 * Returns an array of strings specified in the resource file in the
	 * "package.classname" format to be analyzed.
	 * @return  Array of class files.
	 */
	public List getClassFilesList ()
	{
		return classFiles;
	}
    
	/**
	 * Convenience method for converting the List to an Array (less efficient) from <code>getClassFilesList</code>
	 * @return  Array of class files.
	 */
	public String [] getClassFiles ()
	{
		return (String []) classFiles.toArray (new String [classFiles.size ()]);
	}
    
	/** Returns the object containing the graph output parameters */
	public DottyOutputSpec getDottyOutputSpec ()
	{
		return dottyOutputParams;
	}

	/** gets the base directory from which relative paths are judged */
	public String getBaseDir ()
	{
		return baseDir;
	}

	/** sets the base directory from which relative paths are judged.  Defaults to nothing.  Returns old value. */
	public String setBaseDir (String newBaseDir)
	{
		String old = baseDir;

		baseDir = newBaseDir;

		return old;
	}

	/** ??? */
	public void save (String file)
	{
		try
		{
			String newLine = System.getProperty ("line.separator");
			String pathSep = System.getProperty ("path.separator");
			String fileSep = System.getProperty ("file.separator");

			java.io.FileWriter writer = new java.io.FileWriter (file);

			writer.write ("# JABA resource file" + newLine);
			writer.write ("ProgramName = " + getProgramName () + newLine);
			writer.write ("ClassPath = ");
			writePath (writer, getClassPaths ());
			writer.write ("SourceClassPath = ");
			writePath (writer, getSourceClassPaths ());
			writer.write ("ClassFiles = ");
			List file_paths = getClassFilesList ();
			for (Iterator i = file_paths.iterator (); i.hasNext ();)
			{
				writer.write (" \\" + newLine + "\t" + i.next ());
			}

			writer.flush();
		}
		catch (Exception e)
		{
			e.printStackTrace ();
		}
	}

	/** Adds an entry to the ClassPath resource list.*/
	public void addClassPath (String path)
	{
		classPaths.add (path);
	}

	/** Adds an entry to the SourceClassPath resource list. */
	public void addSourceClassPath (String path)
	{
		sourceClassPaths.add (path);
	}

	/** Adds an entry to the ClassFile resource list. */
	public void addClassFile (String file)
	{
		int index = file.lastIndexOf (".class");
		if (index != -1)
		{
			file = file.substring (0, index);
		}

		// chop off the class path prefix
		boolean found = false;
		for (Iterator e = classPaths.iterator (); !found && e.hasNext ();)
		{
			String aPath = (String) e.next ();
			if (file.startsWith (aPath))
			{
				file = file.substring (aPath.length ());
				found = true;
			}
		}

		if (!found)
		{
			System.out.println ("warning: file" + file + "'s classpath is not in the specified classpaths");
		}

		// replace file separator with '.' and add
		classFiles.add (file.replaceAll (java.io.File.separator, "."));
	}

	/** Adds an entry to the DottyOutputParam resource list. */
	public void addDottyOutputParam (String param)
	{
		dottyOutputParams.load (param);
	}

	/** remove any temporary objects to shrink memory usage.  Generally called after symbol table built. */
	public void cleanup ()
	{
		dirs = null;
		zips = null;
		sourceDirs = null;
		sourceZips = null;
	}

	/** set the program name.  Returns the old name. */
	public String setProgramName (String newName)
	{
		String old = programName;

		programName = newName;

		return old;
	}

	/** outputs an XML valid format of this object */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<resourcefile>");

		if (baseDir != null)
		{
			buf.append ("<basedir>").append (jaba.tools.Util.toXMLValid (baseDir)).append ("</basedir>");
		}

		buf.append ("<classpath>");
		for (Iterator i = classPaths.iterator (); i.hasNext ();)
		{
			buf.append ("<entry>");
			buf.append (i.next ());
			buf.append ("</entry>");
		}
		buf.append ("</classpath>");

		buf.append ("<classfile>");
		for (Iterator i = classFiles.iterator (); i.hasNext ();)
		{
			buf.append ("<entry>");
			buf.append (i.next ());
			buf.append ("</entry>");
		}
		buf.append ("</classfile>");

		buf.append ("<sourceclasspath>");
		for (Iterator i = sourceClassPaths.iterator (); i.hasNext ();)
		{
			buf.append ("<entry>");
			buf.append (i.next ());
			buf.append ("</entry>");
		}
		buf.append ("</sourceclasspath>");

		buf.append ("<dottyoutputparams>");
		for (Iterator i = dottyOutputParams.getOutputParams ().iterator (); i.hasNext ();)
		{
			buf.append ("<param>");
			buf.append (i.next ());
			buf.append ("</param>");
		}
		buf.append ("</dottyoutputparams>");

		buf.append ("</resourcefile>");

		return buf.toString ();
	}

	/** adds an OS file path separator delimited string of entries into the classpath.  Returns the number added. */
	protected int loadClassPaths (String classPathString)
	{
		int count = 0;
		StringTokenizer tokenizer = new StringTokenizer (classPathString, File.pathSeparator);
        	while (tokenizer.hasMoreTokens ())
		{
			String classPath = tokenizer.nextToken ().trim ();
			if (classPath.equals (""))
				continue;

			count++;
			classPaths.add (relative2absolute (classPath));
		}

		dirs = null;
		zips = null;

		return count;
	}

	/** adds an OS file path separator delimited string of entries into the classpath.  Returns the number added. */
	protected int loadSourceClassPaths (String sourceClassPathString)
	{
		int count = 0;
		StringTokenizer tokenizer = new StringTokenizer (sourceClassPathString, File.pathSeparator);
		while (tokenizer.hasMoreTokens ())
		{
			String sourceClassPath = tokenizer.nextToken ().trim ();
			if (sourceClassPath.equals (""))
				continue;

			count++;
			sourceClassPaths.add (relative2absolute (sourceClassPath));
		}

		sourceDirs = null;
		sourceZips = null;

		return count;
	}

	/** adds an OS file path separator delimited string of entries into the class file path.  Returns the number added. */
	protected int loadClassFiles (String classFilesString)
	{
		int count = 0;
		StringTokenizer tokenizer = new StringTokenizer (classFilesString, "\t:, ");
		while (tokenizer.hasMoreTokens ())
		{
			String classFile = tokenizer.nextToken ();
			if (classFile.equals (""))
				continue;

			count++;
			classFiles.add (classFile);
		}

		return count;
	}

	/** ??? */
	protected static void initPathFiles (List paths, List pathDirs, List pathZips) 
	{
		for (Iterator e = paths.iterator (); e.hasNext ();)
		{
			String p = (String) e.next ();
			File f = new File (p);
			pathDirs.add (p);

			if (!f.isDirectory ())
			{
				try
				{
					ZipFile zf = new ZipFile (f);
					pathZips.add (p);
				}
				catch (Exception e1)
				{
					throw new IllegalArgumentException ("Path " + p + " is neither a directory nor zip/jar file");
				}
			}
		}
	}

	/** ??? */
	protected static InputStream createInputStream (String fileName, List pathDirs, List pathZips) throws IOException
	{
		for (Iterator pd = pathDirs.iterator (); pd.hasNext ();)
		{
			File pdf = new File (pd.next ().toString ());
			if (pdf != null)
			{
				File f = new File (pdf, fileName);
				if (f.exists ())
					return new FileInputStream (f);
			}
		}

		for (Iterator pz = pathZips.iterator (); pz.hasNext ();)
		{
			ZipFile pzf = new ZipFile (pz.next ().toString ());
			if (pzf != null)
			{
				ZipEntry ze = pzf.getEntry (fileName);
				if (ze != null)
					return pzf.getInputStream (ze);
			}
		}

		throw new FileNotFoundException(fileName);
	}

	/** ??? */
	protected void writePath (java.io.FileWriter writer, String[] paths) throws IOException
	{
		String sep = "";
		String pathSep = System.getProperty ("path.separator");
		for (int i = 0; i < paths.length; i++)
		{
			String path = new String ();
			StringTokenizer tokenizer = new StringTokenizer (paths [i], "\\");
			String tempSep = "";
			while (tokenizer.hasMoreTokens ())
			{
				path += tempSep + tokenizer.nextToken ();
				tempSep = "\\\\";
			}
			writer.write (sep + path);
			sep = pathSep;
		}
		writer.write (System.getProperty ("line.separator"));
	}

	/** ??? */
	protected void writePath (java.io.FileWriter writer, List paths) throws IOException
	{
		String sep = "";
		String pathSep = System.getProperty ("path.separator");
		for (Iterator i = paths.iterator (); i.hasNext ();)
		{
			String path = new String ();
			StringTokenizer tokenizer = new StringTokenizer ((String) i.next (), "\\");
			String tempSep = "";
			while (tokenizer.hasMoreTokens ())
			{
				path += tempSep + tokenizer.nextToken ();
				tempSep = "\\\\";
			}
			writer.write (sep + path);
			sep = pathSep;
		}

		writer.write (System.getProperty ("line.separator"));
	}

	/**
	 * Returns an absolute path for a relative one -- using this
	 * absolute path of the resource file as a starting point.
	 * Created by Manas: To account for paths relative from the
	 * location of the resource file
	 * @param relPath  A potentially relative path
	 * @return  A path that is equivalent to relPath, but is absolute.
	 */
	protected String relative2absolute (String relPath)
	{
		String absPath = relPath;

		if (!new File (absPath).isAbsolute ()) // This is a relative path.
		{
			if (baseDir != null)
			{
				absPath = baseDir + File.separator + absPath;
			}

			try
			{
				File tmpFile = new File (absPath);
				absPath = tmpFile.getCanonicalPath ();
			}
			catch (Exception e)
			{
				throw new IllegalArgumentException ("Cannot resolve path " + relPath);
			}
		}

		return absPath;
	}

	/** Name of the program to be analyzed. */
	protected String programName = "UnnamedProgram";

	/** base directory from which relative paths are judged */
	protected String baseDir = null;
    
	/**
	 * List of strings, each representing a path to be used to find class
	 * files to be analyzed.
	 */
	protected List classPaths;
    
	/** List of strings in the "package.classname" format to be analyzed. */
	protected List classFiles;

	/**
	 * List of strings, each representing a path to be used to find
	 * source files corresponding to the class files listed in the
	 * classFiles field.
	 */
	protected List sourceClassPaths;
    
	/** Object containing the parameters for the dotty output routine. */
	protected DottyOutputSpec dottyOutputParams;
    
	/** ClassPaths which are directory.  This is an optimization of the class path. */
	protected List dirs = null;

	/** ClassPaths which are zip files.  This is an optimization of the class path. */
	protected List zips = null;

	/** SourceClassPaths which are directory.  This is an optimization of the source class path. */
	protected List sourceDirs = null;

	/** SourceClassPaths which are zip files.  This is an optimization of the source class path. */
	protected List sourceZips = null;
}
