package jaba.main;

/**
 * This class provides a holder for the various processing options available during processing.
 * Originally this class provided strictly static members for holding the options, but is migrating
 * to an instance with private data members approach to facilitate multi-use with JABA on a shared
 * server.
 * 
 * The values of the parameters have been guessed from the code due to the complete lack of original
 * documentation.  The documentation should be close, if not cryptic descriptions of what the options
 * changes in JABA's behavior.
 * 
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/20 changed into a class with instances, added all methods and JavaDoc
 * @author Jay Lofstead 2003/06/23 added implement of Options, added toString
 * @author Jay Lofstead 2003/07/01 changed to implement Options
 */
public class OptionsImpl implements java.io.Serializable, Options
{
	/**
	 * @deprecated replaced by {@link #setCreateLVT(boolean)}
	 */
	public static boolean createLVT = false;

	/**
	 * @deprecated replaced by {@link #setAnalyzeRuntimeException(boolean)}
	 */
	public static boolean analyzeRunTimeException = false;

	/**
	 * @deprecated replaced by {@link #setSerialize(boolean)}
	 */
	public static boolean serialize = false;

	/**
	 * @deprecated replaced by {@link #setDumpFile(String)}
	 */
	public static final String dumpfile = "dump.bin";

	/**
	 * @deprecated replaced by {@link #setRemoveSymbolicInstruction(boolean)}
	 */
	public static boolean removeSymbolicInstruction = false;

	/**
	 * @deprecated replaced by {@link #setInlineFinally(boolean)}
	 */
	public static boolean inlineFinally = false;

	/**
	 * create a new, default Options object.  Uses the static members as default values.
	 */
	public OptionsImpl ()
	{
		setCreateLVT (createLVT);
		setAnalyzeRuntimeException (analyzeRunTimeException);
		setSerialize (serialize);
		setDumpFile (dumpfile);
		setRemoveSymbolicInstruction (removeSymbolicInstruction);
		setInlineFinally (inlineFinally);
	}

	/**
	 * Create a new Options object using the parameter for the initial values
	 */
	public OptionsImpl (Options opt)
	{
		setCreateLVT (opt.getCreateLVT ());
		setAnalyzeRuntimeException (opt.getAnalyzeRuntimeException ());
		setSerialize (opt.getSerialize ());
		setDumpFile (opt.getDumpFile ());
		setRemoveSymbolicInstruction (opt.getRemoveSymbolicInstruction ());
		setInlineFinally (opt.getInlineFinally ());
	}

	/** accessor method */
	public boolean getCreateLVT ()
	{
		return createlvt;
	}

	/** accessor method */
	public boolean getAnalyzeRuntimeException ()
	{
		return analyzeRuntimeException;
	}

	/** accessor method */
	public String getDumpFile ()
	{
		return dumpFile;
	}

	/** accessor method */
	public boolean getSerialize ()
	{
		return serializeOutput;
	}

	/** accessor method */
	public boolean getRemoveSymbolicInstruction ()
	{
		return removeSymbolicInstructions;
	}

	/** accessor method */
	public boolean getInlineFinally ()
	{
		return inlineFinallyBlocks;
	}

	/**
	 * in the build of the SymbolTable, JODE should be called to recreate debug info. (t/f).
	 * returns the old value.  Default value is false.
	 */
	public boolean setCreateLVT (boolean newValue)
	{
		boolean old = createlvt;

		createlvt = newValue;

		return old;
	}

	/**
	 * Should runtime exceptions be propogated beyond their first appearance to all possible affected code.
	 * returns the old value.  Default value is false.
	 */
	public boolean setAnalyzeRuntimeException (boolean newValue)
	{
		boolean old = analyzeRuntimeException;

		analyzeRuntimeException = newValue;

		return old;
	}

	/**
	 * The name of the file to optionally write a copy of the program object.
	 * returns the old value.  Default value is "dump.bin".
	 */
	public String setDumpFile (String newValue)
	{
		String old = dumpFile;

		dumpFile = newValue;

		return old;
	}

	/**
	 * Once the Program is created, it should be written in binary form to the dumpfile (t/f).
	 * returns the old value.  Default value is false.
	 */
	public boolean setSerialize (boolean newValue)
	{
		boolean old = serializeOutput;

		serializeOutput = newValue;

		return old;
	}

	/**
	 * When creating a CFG, all of the symbolic instructions be cleared once it is created (t/f).
	 * returns the old value.  Default value is false.
	 */
	public boolean setRemoveSymbolicInstruction (boolean newValue)
	{
		boolean old = removeSymbolicInstructions;

		removeSymbolicInstructions = newValue;

		return old;
	}

	/**
	 * Should finally blocks be inlined into the current CFG (true) or in separate graphs (false).
	 * returns the old value.  Default value is false.
	 */
	public boolean setInlineFinally (boolean newValue)
	{
		boolean old = inlineFinallyBlocks;

		inlineFinallyBlocks = newValue;

		return old;
	}

	/**
	 * returns a String representation of this object
	 */
	public String toString ()
	{
		StringBuffer buf = new StringBuffer ();

		buf.append ("<options>");
		buf.append ("<createlvt>").append (createlvt).append ("</createlvt>");
		buf.append ("<analyszeruntimeexception>").append (analyzeRuntimeException).append ("</analyszeruntimeexception>");
		buf.append ("<dumpfile>").append (dumpFile).append ("</dumpfile>");
		buf.append ("<serializeoutput>").append (serializeOutput).append ("</serializeoutput>");
		buf.append ("<removesymbolicinstructions>").append (removeSymbolicInstructions).append ("</removesymbolicinstructions>");
		buf.append ("<inlinefinallyblocks>").append (inlineFinallyBlocks).append ("</inlinefinallyblocks>");
		buf.append ("</options>");

		return buf.toString ();
	}

	/** instance storage of value */
	private boolean createlvt = false;

	/** instance storage of value */
	private boolean analyzeRuntimeException = false;

	/** instance storage of value */
	private String dumpFile = "dump.bin";

	/** instance storage of value */
	private boolean serializeOutput = false;

	/** instance storage of value */
	private boolean removeSymbolicInstructions = false;

	/** instance storage of value */
	private boolean inlineFinallyBlocks = false;
}
