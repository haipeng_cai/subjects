package jaba.main;

import java.util.List;
import java.io.InputStream;
import java.io.IOException;

/**
 * Represents the set of resources available from which to create analysis objects.
 * The resources consist of a set of name/value pairs each providing a different piece
 * of the necessary data.  The table below lists each of the possible property values,
 * the format of the entry, and the use of the resource.
 *
 * <table border="1">
 * <tr><th>Property</th><th>Use</th><th>Required?</th></tr>
 * <tr><td>ClassPath</td><td>The path on which to search for the various .class files.
 * this obeys the normal OS specific Java classpath rules</td><td>Yes</td></tr>
 * <tr><td>ProgramName</td><td>The name to be associated with the Program being analyzed</td><td>No</td></tr>
 * <tr><td>ClassFiles</td><td>The class files to analyze as part of the program.  All other classes will be
 * summarized (few details and no further exploration).  The format for the entry is the full package
 * specification for the classes with a '/' separator between packages and the also the classname.  Multiple
 * entries can be specified with a comma separator (with optional spaces).</td><td>Yes</td></tr>
 * <tr><td>DottyOutputParameters</td><td>Customized Dotty output.  This is in the process of being removed
 * from the core JABA system in favor of a non-intrusive, client-side only implementation.  The integration of
 * such an output formatting mechanism unnecessarily clouds the implementation and couples it with a particular
 * output mechanism.</td><td>No</td></tr>
 * <tr><td>SourceClassPath</td><td>The path on which to search for source files for the creation of a SourceFile
 * object object.  If a SourceFile is never requested, this is not required.  This classpath obeys the same rules
 * as the Java OS specific classpath.  The location of a .java file can either be in the directory specified, in
 * a .jar file specified (using the packages for directory layout) or in a package directory layout from a
 * directory specified in the path.</td><td>No*</td></tr>
 * </table>
 * 
 * @author  MANSOUR -- <i>Created, Sep 10, 2001</i>
 * @author Huaxing Wu -- <i>Revised 3/21/02, remove getClasspathNames(0, replaced with getInputStream().
 * 			To Encapture and centure the knowledge of finding a class in classpaths no
 * 			matter it's a libaray class or to be analyzed class  </i>
 * @author Jay Lofstead 2003/06/06 changed to mostly use collections rather than arrays
 * @author Jay Lofstead 2003/06/23 added JavaDoc to define what properties are allowed and what the
 *					values of each should be.  Also added in alternative methods
 * 					for loading the various parameters required.
 * @author Jay Lofstead 2003/07/01 added cleanup method to remove temporary optimization data structures
 * 					once they are no longer needed.
 */
public interface ResourceFileI
{
	/** Returns the name to be associated with the Program */
	public String getProgramName ();

	/**
	 * Returns an array of strings, each representing a path to be used to find
	 * class files to be analyzed.
	 * @return  An array of strings, each representing a path to be used to find
	 *          class files to be analyzed.
	 */
	public List getClassPathsList ();

	/**
	 * Returns an array of strings, each representing a path to be used to find
	 * class files to be analyzed.
	 * @return  An array of strings, each representing a path to be used to find
	 *          class files to be analyzed.
	 */
	public String [] getClassPaths ();

	/**
	 * Returns an array of strings specified in the resource file in the
	 * "package.classname" format to be analyzed.
	 * @return  Array of class files.
	 */
	public List getClassFilesList ();

	/**
	 * Returns an array of strings specified in the resource file in the
	 * "package.classname" format to be analyzed.
	 * @return  Array of class files.
	 */
	public String [] getClassFiles ();

	/**
	 * Get an inputstream of a class.
	 * @param className name of the Class
	 * @return The inputstream of the class
	 */
	public InputStream getInputStream (String className) throws IOException;

	/**
	 * Searches for a source file in the source class path.
	 * @param sourceName the name of the source file
	 * @return An inputstream for the file
	 */ 
	public InputStream getSourceInputStream (String sourceName) throws IOException;

	/** Adds an entry to the ClassPath resource list. */
	public void addClassPath (String path);

	/** Adds an entry to the SourceClassPath resource list. */
	public void addSourceClassPath (String path);

	/** Adds an entry to the ClassFile resource list. */
	public void addClassFile (String file);

	/** set the program name */
	public String setProgramName (String newName);

	/** set the base directory from which relative paths will be judged */
	public String setBaseDir (String newBaseDir);

	/** get the base directory from which relative paths will be judged */
	public String getBaseDir ();
	/** Returns the object containing the graph output parameters */
	public DottyOutputSpec getDottyOutputSpec ();

	/** Adds an entry to the DottyOutputParam resource list. */
	public void addDottyOutputParam (String param);

	/** remove any temporary objects to shrink memory usage.  Generally called after symbol table built. */
	public void cleanup ();
}
