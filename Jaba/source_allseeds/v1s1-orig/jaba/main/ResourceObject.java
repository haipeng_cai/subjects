/* Copyright (c) 1999, The Ohio State University */

package jaba.main;

import java.util.StringTokenizer;
import java.util.Iterator;

/**
 * Represents a resource object specifying the program name, class path to use
 * for analysis, and class files to analyze.  This class reads and parses a
 * resource file and provides accessor methods to this data.
 * 
 * @author Mohamed Mansour -- <i>Created.  September 12, 2001</i>
 * @author Huaxing Wu -- <i>Revised. Remove the usage of classpathnames </i>
 * @author Jay Lofstead 2003/06/23 merged into GenericResource.
 */
public class ResourceObject extends GenericResource
{
    /** Constructor. */
    public ResourceObject (String _programName)
    {
        super (_programName);
    }

    /** use parent class version */
    public String toString ()
    {
	return super.toString ();
    }
}
