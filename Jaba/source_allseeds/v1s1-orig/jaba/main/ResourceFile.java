/* Copyright (c) 2000-2002 Georgia Institute of Technology */

package jaba.main;

import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;

import java.util.zip.ZipFile;

import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;

/**
 * Represents a resource file specifying the program name, class path
 * to use for analysis, and class files to analyze.  This class reads
 * and parses a resource file and provides accessor methods to this
 * data.
 * @author Jim Jones -- <i>Created.  April 1, 1999</i>
 * @author Lakshmish Ramaswamy -- <i> Revised January 2, 2001 added
 * loadDottyOutputParamters() and getDottyOutputVector() functions
 * </i>
 * @author Caleb Ho -- <i> Revised. Redisgned to be more object
 * oriented, by having the ResourceFile use the DottyOutputSpec class
 * for storing and managing output specs rather than going indirectly
 * through the ResourceFile. </i>
 * @author Huaxing Wu -- <i> Revised. 3/21/02 Replace method
 * loadClassFilePathNames() with loadClassFiles() to just load the
 * class files. Change method loadClassPaths() to validate each
 * classpath and initialize relative fields </i>
 * @author Huaxing Wu -- <i> 8/28/02, Revised loadClassPaths. </i>
 * @author Jim Jones -- <i> Revised. Sep. 6, 2002 Added
 * loadSourceClassPaths()</i>
 * @author Jay Lofstead 2003/06/05 changed to use enumerations
 * @author Jay Lofstead 2003/06/16 added a toString method
 * @author Jay Lofstead 2003/06/23 code cleanup and merge work with parent and child classes
 */
public class ResourceFile extends GenericResource
{
    /**
     * Constructor.  Loads the resource file to initialize this object.
     * @param resourceFileName Full path name (relative or absolute)
     * of the resource file to be loaded.
     * @exception IllegalArgumentException This is thrown when the
     * resource file is in an invalid format or variables defined
     * within are invalid or absent.
     */
    public ResourceFile (String resourceFileName) throws IllegalArgumentException
    {
        super ();
	Properties rcProperties = null;
	try
	{
	    File rcFile = new File (resourceFileName);

	    resourceFileName = rcFile.getAbsolutePath ();

	    // open resource file for reading
            FileInputStream resourceIn = new FileInputStream (rcFile);

            rcProperties = new Properties ();
	    rcProperties.load (resourceIn);
	    resourceIn.close ();

		int index = resourceFileName.lastIndexOf (File.separator);
		if (index != -1)
		{
			setBaseDir (resourceFileName.substring (0, index));
		}
        }
	catch (IOException e)
	{
	    throw (new IllegalArgumentException (e.getMessage ()));
	}

	// load each of the variables from the Properties into my fields
        loadProgramName (rcProperties, resourceFileName);
	loadClassPaths (rcProperties, resourceFileName);
	loadClassFiles (rcProperties, resourceFileName);
	loadSourceClassPaths (rcProperties, resourceFileName);
	loadDottyOutputParameters (rcProperties, resourceFileName);
    }

	/** Create a new, empty object without using a resource file on disk */
	public ResourceFile () throws IllegalArgumentException
	{
	}

    /** use parent class version */
    public String toString ()
    {
	return super.toString ();
    }
       
    /** Loads the program name from the resource file. */
    private void loadProgramName (Properties rcProperties, String resourceFileName)
    {
        String newName = rcProperties.getProperty ("ProgramName");

        if (newName != null && !newName.equals (""))
	    setProgramName (newName);
    }

    /** Loads the class paths from the resource file. */
    private void loadClassPaths (Properties rcProperties, String resourceFileName)
    {
	// parse the ClassPath variable
        String classPathString = rcProperties.getProperty ("ClassPath");

        if (classPathString == null || classPathString.equals (""))
	{
            throw new IllegalArgumentException ("ClassPath variable must be set in resource file: " + resourceFileName);
	}

	if (loadClassPaths (classPathString) == 0)
	{
            throw new IllegalArgumentException ("ClassPath variable must include at least one path: " + classPathString);
	}
    }

    /** Loads the source class paths from the resource file. */
    private void loadSourceClassPaths (Properties rcProperties, String resourceFileName)
    {
	String sourceClassPathString = rcProperties.getProperty ("SourceClassPath");
	if (sourceClassPathString == null || sourceClassPathString.equals (""))
		return;

	loadSourceClassPaths (sourceClassPathString);
    }

    /** Loads the class files from the resource file. */
    private void loadClassFiles (Properties rcProperties, String resourceFileName)
    {
	// parse the ClassFiles variable 
        String classFilesString = rcProperties.getProperty ("ClassFiles");

	if (classFilesString == null || classFilesString.equals (""))
	{
            throw new IllegalArgumentException ("ClassFiles variable must be set in the resource file: " + resourceFileName);
	}
	
	if (loadClassFiles (classFilesString) == 0)
	{
            throw new IllegalArgumentException ("ClassFile variable must include at least one class: " + classFilesString);
	}
    }

    /** Loads the Output Parameters for the dotty graph output */
    private void loadDottyOutputParameters (Properties rcProperties, String resourceFileName)
    {
        // parse the output parameter variable
        String outputString = rcProperties.getProperty ("DottyOutputParameters");

        // If the output parameters are empty, then return
        if (outputString == null || outputString.equals (""))
	{
            return;
        }

	dottyOutputParams.load (outputString);
    }   
}
