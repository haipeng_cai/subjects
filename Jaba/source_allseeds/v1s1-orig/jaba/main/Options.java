package jaba.main;

/**
 * This class provides a holder for the various processing options available during processing.
 * Originally this class provided strictly static members for holding the options, but is migrating
 * to an instance with private data members approach to facilitate multi-use with JABA on a shared
 * server.
 * 
 * The values of the parameters have been guessed from the code due to the complete lack of original
 * documentation.  The documentation should be close, if not cryptic descriptions of what the options
 * changes in JABA's behavior.
 * 
 * @author Jay Lofstead 2003/06/09 changed to implement java.io.serializable
 * @author Jay Lofstead 2003/06/20 changed into a class with instances, added all methods and JavaDoc
 * @author Jay Lofstead 2003/06/23 added implement of Options, added toString
 * @author Jay Lofstead 2003/07/01 changed to an interface
 */
public interface Options
{
	/** accessor method */
	public boolean getCreateLVT ();

	/** accessor method */
	public boolean getAnalyzeRuntimeException ();

	/** accessor method */
	public String getDumpFile ();

	/** accessor method */
	public boolean getSerialize ();

	/** accessor method */
	public boolean getRemoveSymbolicInstruction ();

	/** accessor method */
	public boolean getInlineFinally ();

	/**
	 * in the build of the SymbolTable, JODE should be called to recreate debug info. (t/f).
	 * returns the old value.  Default value is false.
	 */
	public boolean setCreateLVT (boolean newValue);

	/**
	 * Should runtime exceptions be propogated beyond their first appearance to all possible affected code.
	 * returns the old value.  Default value is false.
	 */
	public boolean setAnalyzeRuntimeException (boolean newValue);

	/**
	 * The name of the file to optionally write a copy of the program object.
	 * returns the old value.  Default value is "dump.bin".
	 */
	public String setDumpFile (String newValue);

	/**
	 * Once the Program is created, it should be written in binary form to the dumpfile (t/f).
	 * returns the old value.  Default value is false.
	 */
	public boolean setSerialize (boolean newValue);

	/**
	 * When creating a CFG, all of the symbolic instructions be cleared once it is created (t/f).
	 * returns the old value.  Default value is false.
	 */
	public boolean setRemoveSymbolicInstruction (boolean newValue);

	/**
	 * Should finally blocks be inlined into the current CFG (true) or in separate graphs (false).
	 * returns the old value.  Default value is false.
	 */
	public boolean setInlineFinally (boolean newValue);

	/**
	 * returns a String representation of this object
	 */
	public String toString ();
}
