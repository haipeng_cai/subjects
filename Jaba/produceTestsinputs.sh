#!/bin/bash
source ./jaba_global.sh

if [ -s ./RegressionTestSuite.tar.gz -a ! -d ./RegressionTestSuite ];then
	tar zxvf ./RegressionTestSuite.tar.gz
fi

mkdir -p ${subjectloc}/inputs/
fninput=${subjectloc}/inputs/testinputs.txt
> $fninput
tn=0
for jrc in ${subjectloc}/RegressionTestSuite/jaba/inputs/*.jrc;
do
	for dt in ACDG ACFG CDG CFG DefUse ICFG SymbolTable;
	do
		echo "jaba.main.AllDriver -l $jrc -- $dt" >> $fninput
		((tn++))
	done
	replace "/Users/jjones/Desktop/JABA" "$subjectloc" "C:/ARG/Soot/workspace/Subjects/jaba" "$subjectloc" -- $jrc
	replace "/opt/jdk1.4.0/jre/lib/rt.jar" "$subjectloc/lib/rt.jar" -- $jrc
	replace "JABA/lib/jdk1.1/rt.jar" "jaba/RegressionTestSuite/lib/jdk1.1/rt.jar" -- $jrc
	replace "JABA/lib/jdk1.2/rt.jar" "jaba/RegressionTestSuite/lib/jdk1.2/rt.jar" -- $jrc
	replace "JABA/lib/jdk1.3/rt.jar" "jaba/RegressionTestSuite/lib/jdk1.3/rt.jar" -- $jrc
	replace "JABA/lib/jdk1.4/rt.jar" "jaba/RegressionTestSuite/lib/jdk1.4/rt.jar" -- $jrc
done
replace ";" ":" -- ${subjectloc}/RegressionTestSuite/jaba/inputs/JMK.jrc
echo "totally $tn tests found and written into $fninput"
exit 0

# hcai vim :set ts=4 tw=4 tws=4
