#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi
source ./jaba_global.sh

ver=$1
seed=$2

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

INDIR=$subjectloc/DynAliasInstrumented-$ver$seed

SOOTCP=.:$ROOT/software/j2re1.4.2_18/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$subjectloc/bin/${ver}${seed}:$subjectloc/RegressionTestSuite/jaba/testdrivers/AllDriver/lib:$INDIR

java -Xmx1600m -ea -cp ${MAINCP} soot.Main -f J -cp ${SOOTCP} \
	jode.bytecode.BytecodeInfo"$"InstructionList"$"1 \
	-d `pwd`

exit 0


# hcai vim :set ts=4 tw=4 tws=4

