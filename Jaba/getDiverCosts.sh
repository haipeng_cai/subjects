#!/bin/bash
echo "EA Instr costs"
cat timing.EAInstr | awk '{if(NF==6) print $(NF-1)*1.0/1000}'

echo "Diver Instr costs"
cat timing.DiverInstr | awk '{if(NF==6) print $(NF-1)*1.0/1000}'

echo "EA Run costs"
cat timing.EARun | grep -a -E "RunTime" | awk '{if(NF==6) print $(NF-1)*1.0/1000}'

echo "Diver Run costs"
cat timing.DiverRun | grep -a -E "RunTime" | awk '{if(NF==6) print $(NF-1)*1.0/1000}'


# hcai vim :set ts=4 tw=4 tws=4

