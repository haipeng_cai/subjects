#!/bin/bash
export base_dir=/Users/jjones/Desktop/JABA
export JAVA=/System/Library/Frameworks/JavaVM.framework/Versions/1.4/Home/bin/java

mkdir ${base_dir}/outputs/$1
for i in ${base_dir}/RegressionTestSuite/jaba/inputs/*.jrc
do
export base_name=`basename $i`;
echo $base_name

# ACDG
#${JAVA} -mx1000m -ea -cp ${base_dir}/jaba.$1/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/ACDGDriver/lib jaba.main.JABADriver ACDGDriver -l $i > ${base_dir}/outputs/$1/ACDG-${base_name} 2>&1
${JAVA} -mx1000m -ea -cp ${base_dir}/jaba.$1/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/AllDriver/lib jaba.main.JABADriver jaba.main.AllDriver -l $i -- ACDG > ${base_dir}/outputs/$1/ACDG-${base_name} 2>&1

# ACFG
#${JAVA} -mx1000m -ea -cp ${base_dir}/jaba.$1/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/ACFGDriver/lib jaba.main.JABADriver ACFGDriver -l $i > ${base_dir}/outputs/$1/ACFG-${base_name} 2>&1
${JAVA} -mx1000m -ea -cp ${base_dir}/jaba.$1/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/AllDriver/lib jaba.main.JABADriver jaba.main.AllDriver -l $i -- ACFG > ${base_dir}/outputs/$1/ACFG-${base_name} 2>&1

# CDG
#${JAVA} -mx1000m -ea -cp ${base_dir}/jaba.$1/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/CDGDriver/lib jaba.main.JABADriver CDGDriver -l $i > ${base_dir}/outputs/$1/CDG-${base_name} 2>&1
${JAVA} -mx1000m -ea -cp ${base_dir}/jaba.$1/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/AllDriver/lib jaba.main.JABADriver jaba.main.AllDriver -l $i -- CDG > ${base_dir}/outputs/$1/CDG-${base_name} 2>&1

# CFG
#${JAVA} -mx1000m -ea -cp ${base_dir}/jaba.$1/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/CFGDriver/lib jaba.main.JABADriver CFGDriver -l $i > ${base_dir}/outputs/$1/CFG-${base_name} 2>&1
${JAVA} -mx1000m -ea -cp ${base_dir}/jaba.$1/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/AllDriver/lib jaba.main.JABADriver jaba.main.AllDriver -l $i -- CFG > ${base_dir}/outputs/$1/CFG-${base_name} 2>&1

# Definition/Use
#${JAVA} -mx1000m -ea -cp ${base_dir}/jaba.$1/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/DefUseDriver/lib jaba.main.JABADriver DefUseDriver -l $i > ${base_dir}/outputs/$1/DefUse-${base_name} 2>&1
${JAVA} -mx1000m -ea -cp ${base_dir}/jaba.$1/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/AllDriver/lib jaba.main.JABADriver jaba.main.AllDriver -l $i -- DefUse > ${base_dir}/outputs/$1/DefUse-${base_name} 2>&1

# ICFG
#${JAVA} -mx1000m -ea -cp ${base_dir}/jaba.$1/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/ICFGDriver/lib jaba.main.JABADriver ICFGDriver -l $i > ${base_dir}/outputs/$1/ICFG-${base_name} 2>&1
${JAVA} -mx1000m -ea -cp ${base_dir}/jaba.$1/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/AllDriver/lib jaba.main.JABADriver jaba.main.AllDriver -l $i -- ICFG > ${base_dir}/outputs/$1/ICFG-${base_name} 2>&1

# Symbol Table
#${JAVA} -mx1000m -ea -cp ${base_dir}/jaba.$1/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/SymbolTableDriver/lib jaba.main.JABADriver SymbolTableDriver -l $i > ${base_dir}/outputs/$1/SymbolTable-${base_name} 2>&1
${JAVA} -mx1000m -ea -cp ${base_dir}/jaba.$1/lib/rlvt.jar:${base_dir}/RegressionTestSuite/jaba/testdrivers/AllDriver/lib jaba.main.JABADriver jaba.main.AllDriver -l $i -- SymbolTable > ${base_dir}/outputs/$1/SymbolTable-${base_name} 2>&1

done




