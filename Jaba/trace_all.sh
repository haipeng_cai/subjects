#!/bin/bash
export base_dir=/home/jjones/research/fault_local_exp/jaba
export ins_dir=/home/jjones/.Instrumented
export ins_jar=/home/jjones/software/insect/lib/insect.jar


mkdir ${base_dir}/outputs.ins/$1
mkdir ${base_dir}/traces/$1
for i in ${base_dir}/RegressionTestSuite/jaba/inputs/*.jrc
do
export base_name=`basename $i`;
echo $base_name

# ACDG
rm -Rf ${ins_dir}/jaba.${1}/*.xd
#java -mx750m -ea -cp ${ins_dir}/jaba.${1}:${base_dir}/RegressionTestSuite/jaba/testdrivers/ACDGDriver/lib:${ins_jar}:${base_dir}/jaba.${1}/lib/rlvt.jar jaba.main.JABADriver ACDGDriver -l $i > ${base_dir}/outputs.ins/$1/ACDG-${base_name} 2>&1
java -mx750m -ea -cp ${ins_dir}/jaba.${1}:${base_dir}/RegressionTestSuite/jaba/testdrivers/AllDriver/lib:${ins_jar}:${base_dir}/jaba.${1}/lib/rlvt.jar jaba.main.JABADriver jaba.main.AllDriver -l $i -- ACDG > ${base_dir}/outputs.ins/$1/ACDG-${base_name} 2>&1
mv ${ins_dir}/jaba.${1}/*.xd ${base_dir}/traces/$1/ACDG-${base_name}.xd

# ACFG
rm -Rf ${ins_dir}/jaba.${1}/*.xd
#java -mx750m -ea -cp ${ins_dir}/jaba.${1}:${base_dir}/RegressionTestSuite/jaba/testdrivers/ACFGDriver/lib:${ins_jar}:${base_dir}/jaba.${1}/lib/rlvt.jar jaba.main.JABADriver ACFGDriver -l $i > ${base_dir}/outputs.ins/$1/ACFG-${base_name} 2>&1
java -mx750m -ea -cp ${ins_dir}/jaba.${1}:${base_dir}/RegressionTestSuite/jaba/testdrivers/AllDriver/lib:${ins_jar}:${base_dir}/jaba.${1}/lib/rlvt.jar jaba.main.JABADriver jaba.main.AllDriver -l $i -- ACFG > ${base_dir}/outputs.ins/$1/ACFG-${base_name} 2>&1
mv ${ins_dir}/jaba.${1}/*.xd ${base_dir}/traces/$1/ACFG-${base_name}.xd

# CDG
rm -Rf ${ins_dir}/jaba.${1}/*.xd
#java -mx750m -ea -cp ${ins_dir}/jaba.${1}:${base_dir}/RegressionTestSuite/jaba/testdrivers/CDGDriver/lib:${ins_jar}:${base_dir}/jaba.${1}/lib/rlvt.jar jaba.main.JABADriver CDGDriver -l $i > ${base_dir}/outputs.ins/$1/CDG-${base_name} 2>&1
java -mx750m -ea -cp ${ins_dir}/jaba.${1}:${base_dir}/RegressionTestSuite/jaba/testdrivers/AllDriver/lib:${ins_jar}:${base_dir}/jaba.${1}/lib/rlvt.jar jaba.main.JABADriver jaba.main.AllDriver -l $i -- CDG > ${base_dir}/outputs.ins/$1/CDG-${base_name} 2>&1
mv ${ins_dir}/jaba.${1}/*.xd ${base_dir}/traces/$1/CDG-${base_name}.xd

# CFG
rm -Rf ${ins_dir}/jaba.${1}/*.xd
#java -mx750m -ea -cp ${ins_dir}/jaba.${1}:${base_dir}/RegressionTestSuite/jaba/testdrivers/CFGDriver/lib:${ins_jar}:${base_dir}/jaba.${1}/lib/rlvt.jar jaba.main.JABADriver CFGDriver -l $i > ${base_dir}/outputs.ins/$1/CFG-${base_name} 2>&1
java -mx750m -ea -cp ${ins_dir}/jaba.${1}:${base_dir}/RegressionTestSuite/jaba/testdrivers/AllDriver/lib:${ins_jar}:${base_dir}/jaba.${1}/lib/rlvt.jar jaba.main.JABADriver jaba.main.AllDriver -l $i -- CFG > ${base_dir}/outputs.ins/$1/CFG-${base_name} 2>&1
mv ${ins_dir}/jaba.${1}/*.xd ${base_dir}/traces/$1/CFG-${base_name}.xd

# Definition/Use
rm -Rf ${ins_dir}/jaba.${1}/*.xd
#java -mx750m -ea -cp ${ins_dir}/jaba.${1}:${base_dir}/RegressionTestSuite/jaba/testdrivers/DefUseDriver/lib:${ins_jar}:${base_dir}/jaba.${1}/lib/rlvt.jar jaba.main.JABADriver DefUseDriver -l $i > ${base_dir}/outputs.ins/$1/DefUse-${base_name} 2>&1
java -mx750m -ea -cp ${ins_dir}/jaba.${1}:${base_dir}/RegressionTestSuite/jaba/testdrivers/AllDriver/lib:${ins_jar}:${base_dir}/jaba.${1}/lib/rlvt.jar jaba.main.JABADriver jaba.main.AllDriver -l $i -- DefUse > ${base_dir}/outputs.ins/$1/DefUse-${base_name} 2>&1
mv ${ins_dir}/jaba.${1}/*.xd ${base_dir}/traces/$1/DefUse-${base_name}.xd

# ICFG
rm -Rf ${ins_dir}/jaba.${1}/*.xd
#java -mx750m -ea -cp ${ins_dir}/jaba.${1}:${base_dir}/RegressionTestSuite/jaba/testdrivers/ICFGDriver/lib:${ins_jar}:${base_dir}/jaba.${1}/lib/rlvt.jar jaba.main.JABADriver ICFGDriver -l $i > ${base_dir}/outputs.ins/$1/ICFG-${base_name} 2>&1
java -mx750m -ea -cp ${ins_dir}/jaba.${1}:${base_dir}/RegressionTestSuite/jaba/testdrivers/AllDriver/lib:${ins_jar}:${base_dir}/jaba.${1}/lib/rlvt.jar jaba.main.JABADriver jaba.main.AllDriver -l $i -- ICFG > ${base_dir}/outputs.ins/$1/ICFG-${base_name} 2>&1
mv ${ins_dir}/jaba.${1}/*.xd ${base_dir}/traces/$1/ICFG-${base_name}.xd

# Symbol Table
rm -Rf ${ins_dir}/jaba.${1}/*.xd
#java -mx750m -ea -cp ${ins_dir}/jaba.${1}:${base_dir}/RegressionTestSuite/jaba/testdrivers/SymbolTableDriver/lib:${ins_jar}:${base_dir}/jaba.${1}/lib/rlvt.jar jaba.main.JABADriver SymbolTableDriver -l $i > ${base_dir}/outputs.ins/$1/SymbolTable-${base_name} 2>&1
java -mx750m -ea -cp ${ins_dir}/jaba.${1}:${base_dir}/RegressionTestSuite/jaba/testdrivers/AllDriver/lib:${ins_jar}:${base_dir}/jaba.${1}/lib/rlvt.jar jaba.main.JABADriver jaba.main.AllDriver -l $i -- SymbolTable > ${base_dir}/outputs.ins/$1/SymbolTable-${base_name} 2>&1
mv ${ins_dir}/jaba.${1}/*.xd ${base_dir}/traces/$1/SymbolTable-${base_name}.xd

done




