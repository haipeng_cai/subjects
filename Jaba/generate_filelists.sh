#!/bin/bash

BASEDIR=source_allseeds
for N in 1;
do
	#find $BASEDIR/v2s$N -name "*.java" 1>v2s${N}files.lst
	#find $BASEDIR/v2s$N-orig -name "*.java" 1>v2s${N}-origfiles.lst

	> v1s${N}files.lst
	> v1s${N}-origfiles.lst
	#for subdir in jorphan core components functions protocol/ftp protocol/http protocol/jdbc protocol/java;
	for subdir in "";
	do
		find $BASEDIR/v1s$N/$subdir -name "*.java" 1>>v1s${N}files.lst
		find $BASEDIR/v1s$N-orig/$subdir -name "*.java" 1>>v1s${N}-origfiles.lst
	done
done

exit 0

# hcai vim :set ts=4 tw=4 tws=4
