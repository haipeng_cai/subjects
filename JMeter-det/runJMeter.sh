#!/bin/bash
source ./jmeter-det_global.sh
#if [ $# -lt 3 ];then
#	echo "Usage: $0 version seed testplan"
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed "
	exit 1
fi

ver=$1
seed=$2
tp=$3

INDIR=$subjectloc/tracerInstrumented-$ver$seed

MAINCP="$subjectloc:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/Tracer/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$INDIR"

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

starttime=`date +%s%N | cut -b1-13`

cd $INDIR

#java -Xmx10000m -ea -cp ${MAINCP} -Djava.class.path=$subjectloc:$MAINCP org.apache.jmeter.NewDriver -n -t $tp
#$subjectloc/SJD/jdk1.6.0_45/bin/
java -Xmx10000m -ea -Dlog4j.configuration=file:${subjectloc}/log4j.conf -cp ${MAINCP} \
	org.apache.jmeter.NewDriver \
    -n -t ${subjectloc}/xdocs/demos/InterleaveTestPlan.jmx \
    -p ${subjectloc}/jmeter.properties -l ${subjectloc}/InterleaveTestPlan.log 

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

