#!/bin/bash

BASEDIR=source_allseeds
for N in 1 2 5 6 11 13 19;
do
	#find $BASEDIR/v2s$N -name "*.java" 1>v2s${N}files.lst
	#find $BASEDIR/v2s$N-orig -name "*.java" 1>v2s${N}-origfiles.lst

	> v2s${N}files.lst
	> v2s${N}-origfiles.lst
	for subdir in jorphan core components functions protocol/ftp protocol/http protocol/jdbc protocol/java;
	do
		# we omit the PKCS12KeyStore.java code because of the lack of the IAIK package, which is not freely available
		find $BASEDIR/v2s$N/$subdir -name "*.java" | grep -v "PKCS12KeyStore" 1>>v2s${N}files.lst
		find $BASEDIR/v2s$N-orig/$subdir -name "*.java" | grep -v "PKCS12KeyStore" 1>>v2s${N}-origfiles.lst
	done
done

exit 0

# hcai vim :set ts=4 tw=4 tws=4

