#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

source ./jmeter-det_global.sh

INDIR=$subjectloc/execHistInstrumented-$ver-$seed-orig-$change
MODINDIR=$subjectloc/execHistInstrumented-$ver-$seed-$change

MAINCP=.:$ROOT/tools/j2re1.4.2_18/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin:$subjectloc/../JMeter/build/core

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

OUTDIR=exechist_outdyn-${ver}${seed}-$change-orig
mkdir -p $OUTDIR

MODOUTDIR=exechist_outdyn-${ver}${seed}-$change-mod
mkdir -p $MODOUTDIR

#cp $MODINDIR/org/apache/xml/security/test/AllTestsSelect.class $MODINDIR/org/apache/xml/security/test/AllTestsSelectMod.class

java -Xmx10000m -ea -cp ${MAINCP} -DOutputScope=2 -DForActualImpact=true Sensa.SensaRun \
	org.apache.jorphan.test.AllTestsSelect \
	"$subjectloc" \
	"$INDIR" \
	"$change" \
	"$OUTDIR" \
	"$MODINDIR" \
	"$MODOUTDIR"

exit 0


# hcai vim :set ts=4 tw=4 tws=4

