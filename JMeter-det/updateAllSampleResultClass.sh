#!/bin/bash
if [ ! -s ./SampleResult-det.java ];then
	echo "the source of substitution, ./SampleResult-det.java,  has not found, bail out now"
	exit 1
fi

for N in 1 2 5 6 11 13 19; # all 7 changes selected after s9 removed (change has no impacts!) and change s11 added
do
	cp ./SampleResult-det.java source_allseeds/v2s$N/core/org/apache/jmeter/samplers/SampleResult.java
	echo "source_allseeds/v2s$N/core/org/apache/jmeter/samplers/SampleResult.java has been replaced."
	cp ./SampleResult-det.java source_allseeds/v2s$N-orig/core/org/apache/jmeter/samplers/SampleResult.java
	echo "source_allseeds/v2s$N-orig/core/org/apache/jmeter/samplers/SampleResult.java has been replaced."
done

exit 0


# hcai vim :set ts=4 tw=4 tws=4

