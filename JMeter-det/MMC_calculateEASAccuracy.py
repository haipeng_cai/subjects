#!/usr/bin/env python
''' hcai: for the MCIA accuracy study data analysis: for multi-method changes'''
import os
import sys
import string
import subprocess
import random
import time
import re

# executables
g_mutRunBoth = os.getcwd() + "/MutRunAnalysis.sh"

# data source locations
g_VerPrefix="v2"
#g_Seeds=[1,2,3,4,5,6,8]
g_Seeds=[1]

# optional inputs
#g_fnQueryFile= os.getcwd()+"/functionList.out"
g_fnQueryFile="" 
g_nTest=79

# number of queries as samples
g_nQueries=-1

# size of randomly formed query group
g_groupSize=2

def Usage():
	print >> sys.stdout, "%s [versionId] [seedId,..] [file of queries] [number of traces] " % sys.argv[0]
	return

def ParseCommandLine():
	global g_VerPrefix
	global g_Seeds
	global g_fnQueryFile
	global g_nTest
	global g_groupSize
	argc = len(sys.argv)
	if argc >= 4:
		g_groupSize = int(sys.argv[1])
		g_VerPrefix = sys.argv[2]
		g_Seeds=list()
		for s in string.split(sys.argv[3],','):
			g_Seeds.append( s )
		if argc >= 5:
			g_fnQueryFile = sys.argv[4]
			if not os.path.exists( g_fnQueryFile ):
				raise IOError, "the file of queries [%s] does not exist, bailed out now." % g_fnQueryFile
		if argc >= 6:
			g_nTest = int(sys.argv[5])
	else:
		g_Seeds = ['s'+str(v) for v in g_Seeds]
	
	'''check existence of binaries'''
	global g_mutRunBoth
	for binary in (g_mutRunBoth,):
		if not os.path.exists( binary ):
			raise IOError, "required executable [%s] does not exist, bailed out now." % binary 
	print >> sys.stderr, "command line: %s %s %s %s %d" % (sys.argv[0], g_VerPrefix, g_Seeds, g_fnQueryFile, g_nTest)

def collectData():
	global g_dataTable
	global g_fnQueryFile
	global g_nTest
	global g_nQueries
	global g_groupSize

	bUsingSingleQueryList=True
	AllQueries=list()
	if len(g_fnQueryFile) >= 3:
		'''open the file of queries, namely input function list, fetch each one as a single query at a time'''
		qfh = file(g_fnQueryFile, "r")
		if None == qfh:
			raise Exception, "Failed to open query file named %s\n" % g_fnQueryFile 
		AllQueries = qfh.readlines()
		qfh.close()
	else:
		bUsingSingleQueryList=False

	for seed in g_Seeds:
		change = "%s%s" % (g_VerPrefix, seed)
		if bUsingSingleQueryList != True:
			g_fnQueryFile= "%s/MutEHInstrumented-%s/MutPoints.out" % (os.getcwd(), change)
			if not os.path.exists( g_fnQueryFile ):
				print >> sys.stderr,  "the file of queries [%s] does not exist, bailed out now." % g_fnQueryFile
				continue
			'''open the file of queries, namely input function list, fetch each one as a single query at a time'''
			qfh = file(g_fnQueryFile, "r")
			if None == qfh:
				print >> sys.stderr, "%s skipped because: failed to open query file named %s\n" % (change, g_fnQueryFile)
				continue
			AllQueries = qfh.readlines()
			qfh.close()

		bRandomSampling=True
		
		if g_nQueries<1:
			g_nQueries=len(AllQueries)
			bRandomSampling=False
		else:
			if len(AllQueries) < g_nQueries:
				g_nQueries = len(AllQueries)

		query2pnts={}
		## query = qfh.readline()
		## while query:
		for k in range(0, g_nQueries):
			qidx=k
			if bRandomSampling:
				qidx=int(random.random()*(len(AllQueries)-1))
			queryline = AllQueries[ qidx ]
			queryline = queryline.lstrip().rstrip(' \r\n')

			''' retrieve the query method and selected mutation points in it '''
			mres = re.match( r'(<.*>) ([0-9].*)', queryline )
			query = mres.group(1)
			qcontent = string.split(mres.group(2), ' ')
			assert len(qcontent)>=1 
			mutpnts = [ n for n in qcontent[0:len(qcontent)] ]

			query2pnts[query] = mutpnts


		i = len(query2pnts)
		maxTries=20
		while len(query2pnts) >= 1:
			queryset=[]
			mpset=[]
			n=0
			while n<g_groupSize and len(query2pnts)>=1:
				q=query2pnts.keys()[ int(random.random()* (i-1)) ]
				itry=0
				while q in queryset and itry<maxTries:
					q=query2pnts.keys()[ int(random.random()* (i-1)) ]
					itry=itry+1
				queryset.append(q)
				mpset.append( query2pnts[q][0] )
				del query2pnts[q][0]
				if len( query2pnts[q] ) < 1:
					del query2pnts[q]
					i=i-1
				n=n+1

			queries=';'.join(queryset)
			mps=','.join(mpset)

			#print >> sys.stderr, "Queries to deliver: %s + %s" % (queries,mps)
			#continue

			''' run the mutated versions on MutEAS and MutEH, each characterized with a set of mutation point'''
			print >> sys.stderr,"With query [%s] and mutation point[%s]..." % (queries,mps)
			print >> sys.stderr,"Running the base version with MutEAS and MutEH ..."
			os.system("rm -rf MutEAoutdyn-%s%s" % (g_VerPrefix,seed))
			os.system("rm -rf MutEHoutdyn-%s%s" % (g_VerPrefix,seed))
			try:
				cmd = [str(g_mutRunBoth), g_VerPrefix, seed, queries, mps, str(g_nTest)]
				#rcMutBoth = subprocess.check_call( cmd, stderr=subprocess.STDOUT )
				rcMutBoth = subprocess.check_call( cmd )
			except subprocess.CalledProcessError,e:
				print >> sys.stderr, "call %s failed, skipped [ret code=%d]." % \
					( str(cmd).strip(','), e.returncode )
				## query = qfh.readline()
				continue
			except Exception,e:
				print >> sys.stderr, "Unexpected error when calling %s, halt now with error=%s" % \
					( str(cmd).strip(','), e )
				sys.exit(-1)

		print >> sys.stderr,"Done ALL with %s-%s." % (g_VerPrefix, seed)
		## qfh.seek(0,0)
	# // all seeds
	## qfh.close()

######################################
# the boost
if __name__ == "__main__":
	try:
		ParseCommandLine()
	except Exception,e:
		print >> sys.stderr, e
		sys.exit(1)

	#time.sleep(28800)
	import time
	current_milli_time = lambda: int(round(time.time() * 1000))
	random.seed( current_milli_time() )

	collectData()

	sys.exit(0)

# set ts=4 tw=100 sts=4 sw=4


