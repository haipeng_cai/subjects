#!/bin/bash
source ./jmeter-det_global.sh
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [number of traces]"
	exit 1
fi

ver=$1
seed=$2
NT=${3:-"79"}

INDIR=$subjectloc/tracerOutdyn-${ver}${seed}

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/Tracer/bin:$ROOT/workspace/SimplyDep/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Deam/bin:$INDIR"

starttime=`date +%s%N | cut -b1-13`
	#"main" \
	#$subjectloc/sc.txt \
$JAVA -Xmx40g -ea -cp ${MAINCP} sli.SliceDiff \
	"$INDIR" \
	"" \
	$NT \
	-inspect \
	#-debug

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

