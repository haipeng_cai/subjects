#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./jmeter-det_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin"

SOOTCP=".:$ROOT/software/j2re1.4.2_18/lib/rt.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin/:$ROOT/workspace/Sensa/bin:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$subjectloc/bin/$ver$seed:$ROOT/workspace/mcia/bin:$ROOT/workspace/Deam/bin"
for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

suffix=${ver}${seed}

LOGDIR=out-MutEHInstr
mkdir -p $LOGDIR
logout=$LOGDIR/instr-$suffix.out
logerr=$LOGDIR/instr-$suffix.err

OUTDIR=$subjectloc/Step1MutEHInstrumented-$suffix 
#OUTDIR=$subjectloc/MutEHInstrumented-$suffix
mkdir -p $OUTDIR 
starttime=`date +%s%N | cut -b1-13` 

echo "stage 1: mutation instrumentation..."

	#-duaverbose \
	#-instrExecHist \
java -Xmx6600m -ea -cp ${MAINCP} mut.MutInst \
	-w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
	-allowphantom \
	-slicectxinsens \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	1> $logout 2> $logerr

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/Sensa/bin:$ROOT/workspace/ProbSli/bin:$OUTDIR
for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR1=$OUTDIR
OUTDIR=$subjectloc/MutEHInstrumented-$suffix

echo "stage 2: exec history instrumentation..."

change="-2147483648"
	#-duaverbose \
	#blacktypes "ru.novosoft.uml.foundation.data_types.MMultiplicity,java.lang.ref.WeakReference" \
java -Xmx6600m -ea -cp ${MAINCP} deam.DeamInst \
	-w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	blacktypes "org.apache.xpath.objects.XObject,org.apache.xpath.objects.XNodeSet,org.apache.jorphan.collections.Data" \
	-brinstr:off -duainstr:off \
	-allowphantom \
	-slicectxinsens \
	-start:$change \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $OUTDIR1 \
	1>> $logout 2>> $logerr

mv $OUTDIR1/MutPoints.out $OUTDIR/
rm -rf $OUTDIR1

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for $suffix elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Instrumentation done, now copying resources required for running."
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/resources $OUTDIR/org/apache/jmeter/
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/help.txt $OUTDIR/org/apache/jmeter/
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/images $OUTDIR/org/apache/jmeter/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

