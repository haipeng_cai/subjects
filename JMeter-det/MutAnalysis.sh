#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [number of traces]"
	exit 1
fi

ver=$1
seed=$2
NT=${3:-"92"}
query=${4:-"main"}
change=${5:-"-2147483648"}

source ./jmeter-det_global.sh

suffix=${ver}${seed}
[ $change -ne -2147483648 ] && suffix=$change-${ver}${seed}

INDIR=$subjectloc/MutEHInstrumented-$suffix
[ $change -ne -2147483648 ] && INDIR=$INDIR-$change

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/TestAdequacy/bin"

OUTDIR1=NonMutEHoutdyn-${suffix}
[ $change -ne -2147483648 ] && OUTDIR1=$OUTDIR-$change
OUTDIR2=MutEHoutdyn-${suffix}
[ $change -ne -2147483648 ] && OUTDIR2=$OUTDIR-$change

starttime=`date +%s%N | cut -b1-13`

java -ea -Xmx20000m -cp ${MAINCP} mut.MutAnalysis \
	"$query" \
	$OUTDIR1 \
	$OUTDIR2 \
	$INDIR/stmtids.out \
	$NT \
	"-debug" \
	$change

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for $suffix elapsed: " `expr $stoptime - $starttime` milliseconds
exit 0

# hcai vim :set ts=4 tw=4 tws=4
