#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi
source ./jmeter-det_global.sh

ver=$1
seed=$2

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Tracer/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

mkdir -p out-TracerInstr

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Tracer/bin:$subjectloc/bin/${ver}${seed}

for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/tracerInstrumented-$ver$seed
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-debug \
	#-dumpJimple \
	#-wrapTryCatch \
   	#-duaverbose \
	#-dumpFunctionList \
	#-notracingusedval \
$JAVA -Xmx6600m -ea -cp ${MAINCP} profile.EventInstrumenter \
	-w -cp ${SOOTCP} \
	-p cg verbose:false,implicit-entry:false -p cg.spark verbose:false,on-fly-cg:true,rta:true \
	-f c -d "$OUTDIR" -brinstr:off -duainstr:off \
	-defblacktypes "org.apache.xpath.objects.XObject,org.apache.xpath.objects.XNodeSet,org.apache.jorphan.collections.Data" \
	-useblacktypes "org.apache.xpath.objects.XObject,org.apache.xpath.objects.XNodeSet,org.apache.jorphan.collections.Data" \
	-wrapTryCatch \
	-notracingval \
	-slicectxinsens \
	-allowphantom \
	-staticCDInfo \
	-main-class ${DRIVERCLASS} -entry:${DRIVERCLASS} \
	-process-dir $subjectloc/bin/${ver}${seed}  \
	1>out-TracerInstr/instr-${ver}${seed}.out 2>out-TracerInstr/instr-${ver}${seed}.err
stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Copying data to prepare for running the instrumented subject..."
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/resources $OUTDIR/org/apache/jmeter/
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/help.txt $OUTDIR/org/apache/jmeter/
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/images $OUTDIR/org/apache/jmeter/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

