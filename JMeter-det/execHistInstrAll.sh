#!/bin/bash
#C=(34326 18083 34452 34445 3255 27992 19081)  # full set of changes covered at least by one of the 80 tests - s9->s11
# after inserting the mock system clock for determinizing, shift 12 lines
# the inserted code takes 27276-27421, so original location after the start needs be shifted
#C=(34338 18083 34464 34457 3255 28004 19081)

# (if ignoring the "Sensa.Modify.__link()" line, only shifting 11 lines is needed)
#C=(34337 18083 34463 34456 3255 28003 19081) // when using the in-place system clock mock

C=(34326 18083 34452 34445 3255 27992 19081)  # 
i=0
for N in 1 2 5 6 11 13 19; # all 7 changes selected after s9 removed (change has no impacts!) and change s11 added
do
	echo "Now execHistInstrumenting s$N at ${C[$i]} ..."
	sh execHistInstr.sh ${C[$i]} v2 s$N 

	echo "Now execHistInstrumenting s$N-orig at ${C[$i]} ..."
	sh execHistInstr.sh ${C[$i]} v2 s$N-orig

	let i=i+1
done

exit 0


# hcai vim :set ts=4 tw=4 tws=4

