#!/bin/bash
title=${1:-"1"}
tag=${2:-"All"}
#allsubjects="Schedule1 Nano XML-security JMeter-det Ant-det ArgoUML"
#root=/home/hcai/SVNRepos/star-lab/trunk/Subjects/
#allsubjects="schedule nano ant jmeter"
allsubjects="argo"
root=`pwd`
for subject in ${allsubjects};
do
	if [ $title -eq 1 ];
	then
		echo -e "\t================================"
		echo -e "\t===== for Subject $subject ====="
		echo -e "\t================================"
	fi
	if [ $title -eq 1 ];
	then
		echo -e "\t----- query group size=1 -----"
		echo -e "\t0"
	fi
	for s in 2 3 4 5 6 7 8 9 10;
	do
		if [ $title -eq 1 ];
		then
			echo -e "\t----- query group size=$s -----"
		fi
		for fn in $root/$subject/result*EASAccuracy_s$s;
		do
			if [ ! -s $fn ];then
				echo "$fn not found"
				continue
			fi
			grep -a -E "\[All\]" $fn | awk '{if($(NF-2)!=0 || $(NF-3)!=0) print $(NF-1),$(NF-2),$(NF-3);}' | \
				python statDeamMMCResults_summaryOnly.py "-" "" 1

			total=`grep -a -E "\[C\]" $fn | wc -l`
			nc=`grep -a -E "\[C\]" $fn | awk '{for(i=0;i<=7;i++) printf("%.4f ", $(NF-i)); printf("\n");}' | awk '{if ($NF<1) print $0}' | wc -l`
			cr=`echo "nothing" | awk '{printf("%.2f%%", (1-'$nc'*1.0/'$total')*100);}'`
			ncr=`echo "nothing" | awk '{printf("%.2f%%", ('$nc'*1.0/'$total')*100);}'`

			grep -a -E "\[NC\]" $fn | awk '{if($(NF-2)!=0 || $(NF-3)!=0) print $(NF-1),$(NF-2),$(NF-3)}' | \
				python statDeamMMCResults_summaryOnly.py "-" "$ncr" 1
			grep -a -E "\[C\]" $fn | awk '{if($(NF-2)!=0 || $(NF-3)!=0) print $(NF-1),$(NF-2),$(NF-3)}' | \
				python statDeamMMCResults_summaryOnly.py "-" "$cr" 1
			echo
		done
	done
	echo
done
