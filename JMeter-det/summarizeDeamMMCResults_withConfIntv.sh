#!/bin/bash
title=${1:-"1"}
resdir=${2:-"MMC_results"}
#allsubjects="Schedule1 Nano XML-security JMeter-det Ant-det ArgoUML"
#root=/home/hcai/SVNRepos/star-lab/trunk/Subjects/
#allsubjects="schedule nano ant jmeter"
allsubjects="schedule"
root=`pwd`
for subject in ${allsubjects};
do
	if [ $title -eq 1 ];
	then
		echo -e "\t================================"
		echo -e "\t===== for Subject $subject ====="
		echo -e "\t================================"
	fi
	for s in 2 3 4 5 6 7 8 9 10;
	do
		if [ $title -eq 1 ];
		then
			echo -e "\t----- query group size=$s -----"
		fi
		for fn in $root/$subject/result*EASAccuracy_s$s;
		do
			if [ ! -s $fn ];then
				echo "$fn not found"
				continue
			fi
			grep -a -E "\[All\]" $fn | awk '{for(i=0;i<=7;i++) printf("%.4f ", $(NF-i)); printf("\n");}' | \
				python statDeamMMCResults_withConfIntv.py "-" "All"

			total=`grep -a -E "\[C\]" $fn | wc -l`
			nc=`grep -a -E "\[C\]" $fn | awk '{for(i=0;i<=7;i++) printf("%.4f ", $(NF-i)); printf("\n");}' | awk '{if ($NF<1) print $0}' | wc -l`
			cr=`echo "nothing" | awk '{printf("%.2f%%", (1-'$nc'*1.0/'$total')*100);}'`
			ncr=`echo "nothing" | awk '{printf("%.2f%%", ('$nc'*1.0/'$total')*100);}'`

			grep -a -E "\[NC\]" $fn | awk '{for(i=0;i<=7;i++) printf("%.4f ", $(NF-i)); printf("\n");}' | \
				python statDeamMMCResults_withConfIntv.py "-" "Non-crashing ($ncr)"
			grep -a -E "\[C\]" $fn | awk '{for(i=0;i<=7;i++) printf("%.4f ", $(NF-i)); printf("\n");}' | \
				python statDeamMMCResults_withConfIntv.py "-" "Crashing ($cr)"
		done
	done
done
