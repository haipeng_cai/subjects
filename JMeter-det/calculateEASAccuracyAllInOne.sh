#!/bin/bash

#for gsz in 2 3 4 5 8;
for gsz in 6 7 9 10;
do
	python ./MMC_calculateEASAccuracy.py $gsz v2 s1-orig 1>result_jmeter_EASAccuracy_s$gsz 2>log_jmeter_EASAccuracy_s$gsz
done

# hcai vim :set ts=4 tw=4 tws=4

