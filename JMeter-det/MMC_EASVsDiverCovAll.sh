#!/bin/bash

mkdir ./MMC_Covresults/
for s in 2 3 4 5 6 7 8 9 10;
do
	sh MMC_EASVsDiver.sh \
		DiverCovResults/jmeterCov_EASVsDiverLog \
		EAInstrumented-v2s1-orig/functionList.out \
		$s 1000 \
		1>./MMC_Covresults/result_s_$s 2>/dev/null #./MMC_Covresults/log_s_$s
done

echo "all finished."
exit 0

# hcai vim :set ts=4 tw=4 tws=4
