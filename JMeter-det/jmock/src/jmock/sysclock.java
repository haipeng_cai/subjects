package jmock;

/**
 *  This is a mock that mimics Java System clock with a fixed starting time, which 
 *  supports valid time elapses so that program semantics relies on timing measurement
 *  but not on the absolute starting time can be kept, but which helps eliminate 
 *  non-determinism (w.r.t to, for instance, execution history differentiations) due to
 *  varying real System clock starting values upon the start of the program executions;
 *
 *  We created this mock as a separate library, instead of integrating it into the subject
 *  programs themselves for (1) better reusability; and more importantly, (2) avoiding 
 *  spurious difference in execution histories due to the use of actual system clock utilities
 *  with this mock itself. 
 *
 *@author     $Author: hcai $
 *@created    $Date: 2013/08/26 20:06:08 $
 *@version    $Revision: 1.0.0.0 $
 */
public class sysclock
{
	// added by hcai for determinization: just use a "constant" starting time, but keep valid time lapses
	// this will keep the original program semantics where the timing info is involved
	// what is done here is simply to mimic setting a "fixed" system time when the program starts
	// -- hcai addition starts --
	private static long currentMillis = 1377469450888L;
	private static long lastMillis = System.currentTimeMillis();
	public static long getCurrentMillis() {
		//return currentMillis++;
		long nowMillis = System.currentTimeMillis();
		currentMillis = currentMillis + (nowMillis - lastMillis);
		lastMillis = nowMillis;
		return currentMillis;
	}
	// -- hcai addition ends --

	/**
	 *  Returns the display name
	 *
	 *@return    display name of this sample result
	 */
	public String toString()
	{
		return "*Java system clock mock*";
	}
}

