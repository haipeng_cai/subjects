#!/bin/sh
if [ $# -lt 1 ];then
	echo "Usage: $0 verDir"
	exit 1
fi

verDir=$1

source ./jmeter-det_global.sh

mkdir -p bin/${verDir}
MAINCP=.:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/Sensa/bin:$subjectloc/source_allseeds/${verDir}:$subjectloc/source_allseeds/${verDir}/jorphan:$subjectloc/source_allseeds/${verDir}/core/:$subjectloc/source_allseeds/${verDir}/components:$subjectloc/source_allseeds/${verDir}/functions:$subjectloc/source_allseeds/${verDir}/protocol:$subjectloc/source_allseeds/${verDir}/protocol/http:$subjectloc/source_allseeds/${verDir}/protocol/ftp:$subjectloc/source_allseeds/${verDir}/protocol/java:$subjectloc/source_allseeds/${verDir}/protocol/jdbc:$subjectloc/lib/:$ROOT/workspace/Tracer/bin

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

#-source 1.4 -target 1.4
sed s'/\\/\//g' ${verDir}files.lst > /tmp/${verDir}files.lst
$ROOT/tools/jdk160/bin/javac -g:source -source 1.4 -cp ${MAINCP} -d bin/$verDir @/tmp/${verDir}files.lst  #1>err 2>&1

function compileOneByOne()
{
	LOGFILE=${verDir}-compile.out
	#> $LOGFILE
	cat ${verDir}files.lst | dos2unix | sed 's/\\/\//g' | while read fn;
	do
		echo "compiling $fn ....."
		#javac -g:source -source 1.4 -cp ${MAINCP} -d bin/$verDir $fn 1>/dev/null 2>&1
		javac -g:source -source 1.4 -cp ${MAINCP} -d bin/$verDir $fn 1>/dev/null 2>&1
		#javac -cp ${MAINCP} -d bin/$verDir $fn 1>>$LOGFILE 2>&1
		if [ $? -ne 0 ];
		then
			echo "Failed to compile $fn, bail out."
			exit 1
		fi
	done
}

echo "now copying resources required for running."
cp -fr $subjectloc/source_allseeds/${verDir}/core/org/apache/jmeter/resources bin/${verDir}/org/apache/jmeter/
cp -fr $subjectloc/source_allseeds/${verDir}/core/org/apache/jmeter/help.txt bin/${verDir}/org/apache/jmeter/
cp -fr $subjectloc/source_allseeds/${verDir}/core/org/apache/jmeter/images bin/${verDir}/org/apache/jmeter/

if [ -d bin/${verDir}/profile ];
then
	rm -rf bin/${verDir}/profile
fi

echo "Compilation all done." 
exit 0


# hcai vim :set ts=4 tw=4 tws=4

