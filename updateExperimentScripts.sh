#!/bin/bash

subjectloc=/home/hcai/SVNRepos/star-lab/trunk/Subjects/
srcs=(Schedule1 Nano XML-security JMeter Jaba Ant Ant-det JMeter-det ArgoUML)
tgts=(Schedule1 Nano XML-security JMeter Jaba Ant Ant-det JMeter-det ArgoUML)
#tgts=(Schedule Nano XMLSecurity JMeter Jaba Ant)
for ((i=0;i<${#srcs[@]};++i));
do
	cp ${subjectloc}/${srcs[$i]}/*.sh ./${tgts[$i]}/
	cp ${subjectloc}/${srcs[$i]}/*.py ./${tgts[$i]}/
	echo "Done updating scripts for ${srcs[$i]}"
	svn add ./${tgts[$i]}/*.sh
	svn add ./${tgts[$i]}/*.py
done

echo "All got done."
exit 0
