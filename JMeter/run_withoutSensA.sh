#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

source ./jmeter_global.sh
INDIR=$subjectloc/instrumented-$ver-$seed-$change

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/ProbSli/bin:$INDIR"
#$subjectloc/bin/$ver$seed/jorphan:$subjectloc/bin/$ver$seed/core:$subjectloc/bin/$ver$seed/protocol:$subjectloc/bin/$ver$seed/components:$subjectloc/bin/$ver$seed/functions:$subjectloc/bin/$ver$seed/protocol/ftp:$subjectloc/bin/$ver$seed/protocol/http:$subjectloc/bin/$ver$seed/protocol/java:$subjectloc/bin/$ver$seed/protocol/jdbc:

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

: '
for i in $subjectloc/lib/ext/*.jar;
do
	MAINCP=$MAINCP:$i
done
'

OUTDIR=Runout-${ver}${seed}
mkdir -p $OUTDIR

MAINCLASS=org.apache.jorphan.test.AllTests

function RunAllInOne()
{
	# to run all test in one
	#java -Xmx1600m -ea -cp $MAINCP org.apache.jorphan.test.AllTests \
	java -Xmx1600m -ea -cp $MAINCP org.apache.jorphan.test.AllTestsSelect
	#$subjectloc/bin/$ver$seed/protocol/http/org/apache/jmeter/junit/protocol/http/config/,$subjectloc #1> $OUTDIR/1.out 2> $OUTDIR/1.err
}

function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	cat $subjectloc/test_names.txt | dos2unix | \
	while read testname;
	do
		let i=i+1

		echo "Run Test #$i....."
		java -Xmx4000m -ea -cp $MAINCP  org.apache.jorphan.test.AllTestsSelect $testname 1> $OUTDIR/$i.out 2> $OUTDIR/$i.err
	done
}

starttime=`date +%s%N | cut -b1-13`
RunOneByOne
#RunAllInOne
stoptime=`date +%s%N | cut -b1-13`
echo "Normal RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0



# hcai vim :set ts=4 tw=4 tws=4

