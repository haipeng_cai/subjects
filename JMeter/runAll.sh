#!/bin/bash
#C=(34326 34361 34452 9825 9615 27992 19081)  # original seven chosen 

# full set of changes covered at least by one of the 80 tests - but s7-3173 would cause infinite run during SensARun..
#C=(34326 18083 34452 3173 5534 27992 19081)  

#C=(34326 18083 34452 34445 5534 27992 19081)  # full set of changes covered at least by one of the 80 tests 
C=(34326 18083 34452 34445 3255 27992 19081)  # full set of changes covered at least by one of the 80 tests - s9->s11
i=0

for algo in rand inc observed;
do
	cp sensa_${algo}.cfg sensa.cfg
	echo "Now using the $algo strategies for all tests ---"
	i=0
	#for N in 1 3 5 8 10 13 19; # original seven chosen
	#for N in 1 2 5 6 9 13 19; # all 7 changes selected 
	for N in 1 2 5 6 11 13 19; # all 7 changes selected after s9 removed (change has no impacts!) and change s11 added
	do
		#echo "Now Running instrumented s$N at ${C[$i]} ..."
		#sh run.sh ${C[$i]} v2 s$N $algo 

		echo "Now Running instrumented s$N-orig at ${C[$i]} ..."
		sh run.sh ${C[$i]} v2 s$N-orig $algo

		let i=i+1
	done
done

echo "Running phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

