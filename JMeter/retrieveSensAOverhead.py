#!/usr/bin/env python
'''
read a run log of the Whole SensA Experimentation script ./startSensaExperiment.sh, and retrieve
the per-phase overhead of SensA for each change of the given subject

Haipeng Cai @Oct. 29th, 2012
'''

import os
import sys
import string
import re
import resource
from odict import odict

#resource.setrlimit(resource.RLIMIT_NOFILE, (1000,-1))

# name of file to parse
g_fns=[]

# hold all grep results
g_allimfiles=[]
g_bKeepImfile=False

'''data structure:
	{change:[staticAnalysisTime, {strategy:(runTime, rankingTime),...},normalRunTime], ...}
'''
g_alltimeinfo=odict()

split=string.split

PREPROCESS_FILTER="strategies|Normal RunTime|StaticAnalysisTime|RunTime|RankingTime|ALL STATIC ANALYSIS TIME|ALL RUN TIME|ALL RANKING TIME"
# keywords for retrieving feature lines
FORSTRATEGY="strategies"
FORSA="StaticAnalysisTime"
FORRUN="RunTime"
FORRANKING="RankingTime"
FORNR="Normal RunTime"

FORASA="ALL STATIC ANALYSIS TIME"
FORARUN="ALL RUN TIME"
FORARANKING="ALL RANKING TIME"

## @brief print usage
## @param none
## @retrun none
def Usage():
	print >> sys.stdout, "%s <SensaExperimentLog> [NormalRunsLog] \n" % \
			sys.argv[0]
	return

## @brief parse command line to get user's input for source file name and target
## @param none
## @retrun none
def ParseCommandLine():
	global g_fns
	argc = len(sys.argv)
	if argc >= 2:
		for i in range(1, argc):
			g_fns.append( sys.argv[i] )
			if not os.path.exists( g_fns[i-1] ):
				raise IOError, "source file given [%s] does not exist, bailed out now." \
					% g_fns[i-1]
	else:
		Usage()
		raise Exception, "too few arguments, aborted."

## @brief preprocess the raw log and filter out only relevant information 
##	for the statistics
## @param srcfns a list of raw log files
## @return none
def preprocess(srcfns):
	global g_allimfiles
	for rfn in srcfns:
		imfn = "%s_grepout" % rfn
		os.system("cat %s | grep -E \"%s\" > %s" % (rfn, PREPROCESS_FILTER, imfn) )
		g_allimfiles.append( imfn )
		print >> sys.stderr, "%s processed and procuded %s" % (rfn, imfn)

## @brief load the run log concerning SensA Experimentation
## @param srcfn the run log file 
## @return none
def loadSensaExpLog(srcfn):
	def pickStrategy( curline ):
		strategy = None
		curline = curline.lstrip().rstrip(' \r\n')
		__segs = split( curline )
		if len(__segs) >= 8:
			strategy = __segs[3]
		return strategy

	def pickChange( curline ) :
		change = None
		stg = None # in the run phase, strategy adopted may also be indicated in the change, like "v0s1-orig-rand"
		curline = curline.lstrip().rstrip(' \r\n')
		__segs = split( curline )
		if len(__segs) >= 6 and __segs[-1] == "milliseconds":
			i=0
			while i < len(__segs) and __segs[i] != "for":
				i+=1
			__chsegs = split( __segs[i+1], '-')
			stidx = 0
			if len(__chsegs) >= 3:
				stidx = 1
			change="%s-%s" % (__chsegs[stidx], __chsegs[stidx+1])

			if len(__chsegs) >= 3:
				stg = __chsegs[-1]
		return (change,stg)

	def pickTime( curline ) :
		time = None
		curline = curline.lstrip().rstrip(' \r\n')
		__segs = split( curline )
		if len(__segs) >= 6 and __segs[-1] == "milliseconds":
			time = int ( __segs[-2] )
		return time 

	def pickAllTime( curline ) :
		alltime = None
		curline = curline.lstrip().rstrip(' \r\n')
		__segs = split( curline )
		if len(__segs) >= 4 and __segs[-1] == "milliseconds":
			alltime = int ( __segs[-2] )
		return alltime

	'''===================================================='''
	global g_alltimeinfo

	sfh = file(srcfn,"r")
	if None == sfh:
		raise Exception, "Failed to open file - %s." % (srcfn)

	lnCnt = 0
			
	''' read one strategy by another '''
	strategy = None
	change = None

	curline = sfh.readline()
	# overlook lines before the first FORSA tag
	while string.find( curline, FORSA) == -1:
		lnCnt += 1
		curline = sfh.readline()

	while curline:
		lnCnt += 1
		if string.find( curline, FORSA ) != -1:
			(change,unused) = pickChange( curline )
			satime = pickTime( curline )
			if None == change:
				raise ValueError, "parsing %s failed at line No. %d - %s." % (srcfn,lnCnt,curline)

			if change not in g_alltimeinfo.keys():
				g_alltimeinfo[change] = [None, None, None]
			g_alltimeinfo[change][0] = satime
			curline = sfh.readline()
			continue

		if string.find( curline, FORSTRATEGY ) != -1:
			strategy = pickStrategy( curline )
			if None == strategy:
				raise ValueError, "parsing %s failed at line No. %d - %s." % (srcfn,lnCnt,curline)
			curline = sfh.readline()
			continue

		if string.find( curline, FORRUN) != -1:
			(change,stg) = pickChange( curline )
			runtime = pickTime( curline )

			'''
			if stg: strategy = stg
			'''
			if change not in g_alltimeinfo.keys():
				g_alltimeinfo[change] = [None, None, None]

			if len(g_alltimeinfo[change]) >= 2 and isinstance(g_alltimeinfo[change][1], dict):
				if strategy not in g_alltimeinfo[change][1].keys():
					g_alltimeinfo[change][1][strategy] = [None, None]
				g_alltimeinfo[change][1][strategy][0] = runtime 
			else:
				g_alltimeinfo[change][1] = dict()
				g_alltimeinfo[change][1][strategy] = [runtime, None]

			curline = sfh.readline()
			continue

		if string.find( curline, FORRANKING) != -1:
			(change,stg) = pickChange( curline )
			ranktime = pickTime( curline )

			if stg: strategy = stg
			change = split(change,'-')[0] + "-orig"

			if change not in g_alltimeinfo.keys():
				g_alltimeinfo[change] = [None, None, None]

			if len(g_alltimeinfo[change]) >= 2 and isinstance(g_alltimeinfo[change][1], dict):
				runtimeinfo = g_alltimeinfo[change][1]
				if strategy not in runtimeinfo.keys():
					runtimeinfo[strategy] = [None, None]
				runtimeinfo[strategy][1] = ranktime 
			else:
				runtimeinfo = dict()
				runtimeinfo[strategy] = [None, None]
				runtimeinfo[strategy][1] = ranktime
				g_alltimeinfo[change][1] = runtimeinfo

			curline = sfh.readline()
			continue

		print >> sys.stderr, "line skipped - %s" % curline
		curline = sfh.readline()
			
	sfh.close()
	print >> sys.stderr, "%d lines loaded in file %s" % (lnCnt, srcfn)

## @brief load the Normal run log
## @param srcfn the run log file 
## @return none
def loadNormalRunLog(srcfn):
	def pickChange( curline ) :
		change = None
		stg = None # in the run phase, strategy adopted may also be indicated in the change, like "v0s1-orig-rand"
		curline = curline.lstrip().rstrip(' \r\n')
		__segs = split( curline )
		if len(__segs) > 6 and __segs[-1] == "milliseconds":
			i=0
			while i < len(__segs) and __segs[i] != "for":
				i+=1
			__chsegs = split( __segs[i+1], '-')
			stidx = 0
			if len(__chsegs) >= 3:
				stidx = 1
			change="%s-%s" % (__chsegs[stidx], __chsegs[stidx+1])

			if len(__chsegs) >= 4:
				stg = __chsegs[-1]
		return (change,stg)

	def pickTime( curline ) :
		time = None
		curline = curline.lstrip().rstrip(' \r\n')
		__segs = split( curline )
		if len(__segs) > 6 and __segs[-1] == "milliseconds":
			time = int ( __segs[-2] )
		return time 


	'''===================================================='''
	global g_alltimeinfo

	sfh = file(srcfn,"r")
	if None == sfh:
		raise Exception, "Failed to open file - %s." % (srcfn)

	lnCnt = 0
			
	curline = sfh.readline()
	# overlook lines before the first FORNR tag
	while string.find( curline, FORNR) == -1:
		lnCnt += 1
		curline = sfh.readline()

	while curline:
		lnCnt += 1
		if string.find( curline, FORNR) != -1:
			(change,unused) = pickChange( curline )
			nruntime = pickTime( curline )

			if change not in g_alltimeinfo.keys():
				g_alltimeinfo[change] = [None, None, None]

			g_alltimeinfo[change][2] = nruntime

			curline = sfh.readline()
			continue

		print >> sys.stderr, "line skipped - %s" % curline
		curline = sfh.readline()
			
	sfh.close()
	print >> sys.stderr, "%d lines loaded in file %s" % (lnCnt, srcfn)

## @brief read the run logs for all participants
## @param imfns the list of immediate files
def loadall(srcfns):
	loadSensaExpLog( srcfns[0] )
	if len(srcfns) >= 2:
		loadNormalRunLog( srcfns[1] )

## @brief serialize pariticipant info to the stdout file
def dump():
	global g_alltimeinfo
	# table title
	print >> sys.stdout, "change instrCost RunCost_rand RunCost_inc RunCost_obs RankCost_rand RankCost_inc RankCost_obs NormalRunCost"
	for change in g_alltimeinfo.keys():
		print >> sys.stdout, change, g_alltimeinfo[change][0],
		for algo in ("rand", "inc", "observed"):
			print >> sys.stdout, g_alltimeinfo[change][1][algo][0],
		for algo in ("rand", "inc", "observed"):
			print >> sys.stdout, g_alltimeinfo[change][1][algo][1],
		print >> sys.stdout, g_alltimeinfo[change][2]

def dumpInSeconds():
	global g_alltimeinfo
	# table title
	print >> sys.stdout, "change instrCost RunCost_rand RunCost_inc RunCost_obs RankCost_rand RankCost_inc RankCost_obs NormalRunCost"
	for change in g_alltimeinfo.keys():
		print >> sys.stdout, change, float(g_alltimeinfo[change][0])/1000.0,
		for algo in ("rand", "inc", "observed"):
			print >> sys.stdout, float(g_alltimeinfo[change][1][algo][0])/1000.0,
		for algo in ("rand", "inc", "observed"):
			print >> sys.stdout, float(g_alltimeinfo[change][1][algo][1])/1000.0,
		print >> sys.stdout, float(g_alltimeinfo[change][2])/1000.0

## @brief remove all immediate files
def removeImfiles():
	global g_allimfiles
	for imfn in g_allimfiles:
		os.system(" rm -rf %s " % (imfn) )

######################################
# the boost
if __name__ == "__main__":
	try:
		ParseCommandLine()
	except Exception,e:
		print >> sys.stderr, e
		sys.exit(1)

	try:
		preprocess( g_fns )
		loadall( g_allimfiles )
		#dump()
		dumpInSeconds()
	finally:
		if not g_bKeepImfile:
			removeImfiles()
		
# set ts=4 tw=100 sts=4 sw=4
 
