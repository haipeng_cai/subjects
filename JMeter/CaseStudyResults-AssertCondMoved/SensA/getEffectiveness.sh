#!/bin/bash
# *** PARAMS   1=change 2=seed
# *** EXAMPLE    25      s1
if [ $# -lt 4 ];then
	echo "Usage: $0 changeLoc version seed algo"
	exit 1
fi

change=$1
ver=$2
seed=$3
algo=$4

ROOT=/home/hcai/
ARANKFILE=actualImpactRanking-${ver}${seed}-$change
SRANKFILE=predictedImpactRanking-${ver}${seed}-orig-$change-$algo
ACCUMEFFECTFILE=accumulatedEffectiveness-${ver}${seed}-$change-$algo
AVGEFFECTFILE=averageCosts-${ver}${seed}-$change-$algo
CURVEFILE=curveEffectiveness-${ver}${seed}-$change-$algo

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/TestAdequacy/bin:$ROOT/tools/java_cup.jar"

COSTEFFECTDIR=`pwd`/costEffectiveness
mkdir -p $COSTEFFECTDIR

java -Xmx1600m -ea -cp $MAINCP \
	experiment.sensa.getCostOfEffectiveness \
	"$SRANKFILE" \
	"$ARANKFILE" \
	"$COSTEFFECTDIR/$ACCUMEFFECTFILE" \
	"$COSTEFFECTDIR/$AVGEFFECTFILE" \
	"$COSTEFFECTDIR/$CURVEFILE"

exit 0
