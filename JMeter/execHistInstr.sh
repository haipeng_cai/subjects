#!/bin/bash

if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3
source ./jmeter_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin"

mkdir -p out-execHistInstr

SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin/:$ROOT/workspace/Sensa/bin:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$subjectloc/bin/$ver$seed"

for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/execHistInstrumented-$ver-$seed-$change
mkdir -p $OUTDIR

java -Xmx6000m -ea -cp ${MAINCP} sli.ProbSli \
	-w -cp $SOOTCP -p cg verbose:true,implicit-entry:false \
	-p cg.spark verbose:true,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
	-nopslice \
   	-duaverbose \
	-slicectxinsens \
	-start:$change \
	-main-class org.apache.jorphan.test.AllTestsSelect \
	-entry:org.apache.jorphan.test.AllTestsSelect \
	-process-dir $subjectloc/bin/${ver}${seed} \
	1>out-execHistInstr/execHistInstr-$change-${ver}${seed}.out 2>out-execHistInstr/execHistInstr-$change-${ver}${seed}.err

echo "Instrumentation done, now copying resources required for running."
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/resources $OUTDIR/org/apache/jmeter/
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/help.txt $OUTDIR/org/apache/jmeter/
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/images $OUTDIR/org/apache/jmeter/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

