#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3
source ./jmeter_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin"

INDIR=$subjectloc/execHistInstrumented-$ver-$seed-$change

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin/:$ROOT/workspace/Sensa/bin:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$INDIR

for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

java -Xmx1600m -ea -cp ${MAINCP} soot.Main -f J -cp ${SOOTCP} \
	"org.apache.jmeter.protocol.http.proxy.HttpRequestHdr\$Test" \
	-d `pwd`

exit 0


# hcai vim :set ts=4 tw=4 tws=4

