#!/bin/bash
for N in 1 2 5 6 11 13 19; # all 7 changes selected after s9 removed (change has no impacts!) and change s11 added
do
	echo "Now instrumenting s$N ..."
	sh DiverInstr.sh v2 s$N 

done

exit 0


# hcai vim :set ts=4 tw=4 tws=4

