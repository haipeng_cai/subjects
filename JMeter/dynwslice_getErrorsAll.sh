#!/bin/bash
# these are the real change locations to instrument at
C=(34326 18083 34452 34445 3255 27992 19081)  # full set of changes covered at least by one of the 80 tests - s9->s11

RESDIR=`pwd`/errorMetrics
mkdir -p $RESDIR

i=0
for N in 1 2 5 6 11 13 19; # all 7 changes selected after s9 removed (change has no impacts!) and change s11 added
do
	echo -n "Now calculating errors for v2 s$N at ${C[$i]} ... "
	sh ./dynwslice_getErrors.sh ${C[$i]} v2 s$N > $RESDIR/Error-v2s$N-${C[$i]}

	let i=i+1
	echo " done."
done

echo "Ranking phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

