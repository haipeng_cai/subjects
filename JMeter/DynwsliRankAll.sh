#!/bin/bash
C=(34326 18083 34452 34445 3255 27992 19081)  # full set of changes covered at least by one of the 80 tests - s9->s11
i=0

for N in 1 2 5 6 11 13 19; # all 7 changes selected after s9 removed (change has no impacts!) and change s11 added
do
	echo "Now Ranking instrumented s$N-orig at ${C[$i]} ..."
	sh DynwsliRank.sh ${C[$i]} v2 s$N-orig

	let i=i+1
done

echo "Running phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

