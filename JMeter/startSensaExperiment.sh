#!/bin/bash
: '
stime=`date +%s%N | cut -b1-13`
sh ./compileAll.sh
etime=`date +%s%N | cut -b1-13`
echo "ALL COMPILATION TIME: " `expr $etime - $stime` milliseconds
wait
'

###################################################
# 1. instrumentation / static analysis
stime=`date +%s%N | cut -b1-13`
sh ./instrAll.sh 
etime=`date +%s%N | cut -b1-13`
echo "ALL STATIC ANALYSIS TIME: " `expr $etime - $stime` milliseconds
wait

# 2. Run phase
stime=`date +%s%N | cut -b1-13`
sh ./runAll.sh 
etime=`date +%s%N | cut -b1-13`
echo "ALL RUN TIME: " `expr $etime - $stime` milliseconds
wait

# 3. Ranking phase
stime=`date +%s%N | cut -b1-13`
sh ./sensaRankingForRawoutAll.sh 
etime=`date +%s%N | cut -b1-13`
echo "SENSA RANKING TIME: " `expr $etime - $stime` milliseconds
wait
###################################################

stime=`date +%s%N | cut -b1-13`
sh ./execHistInstrAll.sh
etime=`date +%s%N | cut -b1-13`
echo "execHistory instrumentation TIME: " `expr $etime - $stime` milliseconds
wait

stime=`date +%s%N | cut -b1-13`
sh ./execHistRun_withoutSensaRunAll.sh
etime=`date +%s%N | cut -b1-13`
echo "execHistory run TIME: " `expr $etime - $stime` milliseconds
wait

stime=`date +%s%N | cut -b1-13`
sh ./actualRankingForIndependentExechistRunAll.sh
etime=`date +%s%N | cut -b1-13`
echo "ACTUAL RANKING TIME: " `expr $etime - $stime` milliseconds
wait

exit 0

# hcai vim :set ts=4 tw=4 tws=4

