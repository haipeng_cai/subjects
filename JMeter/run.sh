#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3
source ./jmeter_global.sh

INDIR=$subjectloc/instrumented-$ver-$seed-$change

MAINCP=".:$ROOT/tools/j2re1.4.2_18/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/ProbSli/bin:$INDIR"

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

: '
for i in $subjectloc/lib/ext/*.jar;
do
	MAINCP=$MAINCP:$i
done
'


OUTDIR=outdyn-${ver}${seed}-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`

java -Xmx10000m -ea -cp ${MAINCP} -DOutputScope=0 -DnotRunWithNotHit=true Sensa.SensaRun \
	org.apache.jorphan.test.AllTestsSelect \
	"$subjectloc" \
	"$INDIR" \
	$change \
	$OUTDIR

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for $change-${ver}${seed}-$4 elapsed: " `expr $stoptime - $starttime` milliseconds

if [ $# -ge 4 ];
then
	# save these for possible later trouble shooting
	algo=$4
	OUTDIR=outdyn-${ver}${seed}-$change
	mkdir -p $OUTDIR/${algo}_ValuesUsed
	mv $subjectloc/valuetried $subjectloc/observed_value.out $OUTDIR/${algo}_ValuesUsed
fi

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

