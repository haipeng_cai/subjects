#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

source ./jmeter_global.sh

INDIR=$subjectloc/DynwsliInstrumented-$ver-$seed-$change
#
#$ROOT/tools/j2re1.4.2_18/lib/rt.jar

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DynWSlicing/bin:$ROOT/workspace/TestAdequacy/bin:$INDIR"
for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

OUTDIR=$subjectloc/dynwsli_outdyn-${ver}${seed}-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	cat $subjectloc/inputs/testinputs.txt | while read test;
	do
		let i=i+1
		echo "Test No. $i: $test "
		java -Xmx8000m -ea -cp ${MAINCP} \
			org.apache.jorphan.test.AllTestsSelect $test \
			1> $OUTDIR/$i.out \
			2> $OUTDIR/$i.err
	done
}

pushd . 1>/dev/null 2>&1
cd ${INDIR}
#cp $INDIR/slice.out ./
RunOneByOne
popd
#rm -f ./slice.out

stoptime=`date +%s%N | cut -b1-13`
echo "Dynamic W-Slicing Run Time for $change-${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

exit 0


# hcai vim :set ts=4 tw=4 tws=4

