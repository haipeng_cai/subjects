#!/bin/bash
LOGDIR=`pwd`/complog
#mkdir -p $LOGDIR

#for N in 1 3 5 8 10 13 19; # the original 7 seeds
#for N in 1 2 3 4 5 6 7 8 9 10 13 19 20; # the full set of seeds found as yet

#for N in 1 2 5 6 7 9 13 19; # changes really covered by at least one of the 80 available tests

#for N in 1 2 5 7 9 13 19; # all 7 changes selected - but s7-3173 does not work with sensa
#for N in 1 2 5 6 9 13 19; # all 7 changes selected 

for N in 1 2 5 6 11 13 19; # all 7 changes selected after s9 removed (change has no impacts!) and change s11 added
do
	echo "compiling v2s$N......"
	sh compile.sh  v2s$N 
	#1>$LOGDIR/v2s$N.log 2>&1 
	if [ $? -ne 0 ];then
		echo "compiling v2s$N failed, aborted."
		exit 1
	fi

	echo "compiling v2s$N-orig......"
	sh compile.sh  v2s$N-orig 
	#1>$LOGDIR/v2s$N-org.log 2>&1
	if [ $? -ne 0 ];then
		echo "compiling v2s$N-orig failed, aborted."
		exit 1
	fi
done

echo "All finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

