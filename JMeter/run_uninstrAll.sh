#!/bin/bash

for N in 1 2 5 6 11 13 19; # all 7 changes selected after s9 removed (change has no impacts!) and change s11 added
do
	echo "Now Running Uninstrumented v2 s$N ..."
	sh run_uninstr.sh v2 s$N-orig
done

echo "Normal Running now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

