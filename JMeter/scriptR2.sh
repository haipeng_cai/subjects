#!/bin/sh
# The following is an R script type
#: <<'END'
#unset CLASSPATH

CLASSPATH=".:${experiment_root}/jakarta-jmeter/source:${experiment_root}/jakarta-jmeter/source/JMeter/bin:/nfs/spectre/a4/solaris8/common/j2sdk1.4.2/lib/tools.jar:../lib/Tidy.jar:../lib/ant-1.5-optional.jar:../lib/ant-1.5.jar:../lib/avalon-excalibur-4.1.jar:../lib/avalon-framework.jar:../lib/jakarta-oro-2.0.1.jar:../lib/jorphan.jar:../lib/junit.jar:../lib/logkit-1.0.1.jar:../lib/xalan.jar:../lib/xerces.jar:../lib/xml-apis.jar:../lib/ext/ApacheJMeter_components.jar:../lib/ext/ApacheJMeter_core.jar:../lib/ext/ApacheJMeter_ftp.jar:../lib/ext/ApacheJMeter_functions.jar:../lib/ext/ApacheJMeter_http.jar:../lib/ext/ApacheJMeter_java.jar:../lib/ext/ApacheJMeter_jdbc.jar:../build/core:../build/jorphan:../build"
export CLASSPATH
#END

echo ">>>>>>>>running test 1"
$JAVA_HOME/bin/java -cp ${CLASSPATH} junit.textui.TestRunner org.apache.jmeter.gui.action.Load$Test  > ${experiment_root}/jakarta-jmeter/outputs/t1 2>&1
sh ${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 1

echo ">>>>>>>>running test 2"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.gui.action.Save$Test  > ${experiment_root}/jakarta-jmeter/outputs/t2 2>&1
sh ${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 2

echo ">>>>>>>>running test 3"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.engine.TreeCloner$Test  > ${experiment_root}/jakarta-jmeter/outputs/t3 2>&1
sh ${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 3

exit 0

echo ">>>>>>>>running test 4"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.functions.ValueReplacer$Test  > ${experiment_root}/jakarta-jmeter/outputs/t4 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 4

echo ">>>>>>>>running test 5"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.functions.CompoundFunction$Test  > ${experiment_root}/jakarta-jmeter/outputs/t5 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 5

echo ">>>>>>>>running test 6"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.threads.TestCompiler$Test  > ${experiment_root}/jakarta-jmeter/outputs/t6 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 6

echo ">>>>>>>>running test 7"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.junit.JMeterTest  > ${experiment_root}/jakarta-jmeter/outputs/t7 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 7

echo ">>>>>>>>running test 8"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.save.SaveService$Test  > ${experiment_root}/jakarta-jmeter/outputs/t8 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 8

echo ">>>>>>>>running test 9"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.util.StringUtilities$Test  > ${experiment_root}/jakarta-jmeter/outputs/t9 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 9

echo ">>>>>>>>running test 10"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.config.gui.ArgumentsPanel$Test  > ${experiment_root}/jakarta-jmeter/outputs/t10 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 10

echo ">>>>>>>>running test 11"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.control.OnceOnlyController$Test  > ${experiment_root}/jakarta-jmeter/outputs/t11 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 11

echo ">>>>>>>>running test 12"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.control.GenericController$Test  > ${experiment_root}/jakarta-jmeter/outputs/t12 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 12

echo ">>>>>>>>running test 13"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.control.InterleaveControl$Test  > ${experiment_root}/jakarta-jmeter/outputs/t13 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 13

echo ">>>>>>>>running test 14"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.control.LoopController$Test  > ${experiment_root}/jakarta-jmeter/outputs/t14 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 14

echo ">>>>>>>>running test 15"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.functions.RegexFunction$Test  > ${experiment_root}/jakarta-jmeter/outputs/t15 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 15

echo ">>>>>>>>running test 16"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.junit.protocol.http.config.UrlConfigTest  > ${experiment_root}/jakarta-jmeter/outputs/t16 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 16

echo ">>>>>>>>running test 17"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.junit.protocol.http.parser.HtmlParserTester  > ${experiment_root}/jakarta-jmeter/outputs/t17 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 17

echo ">>>>>>>>running test 18"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.protocol.http.sampler.HTTPSampler$Test  > ${experiment_root}/jakarta-jmeter/outputs/t18 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 18

echo ">>>>>>>>running test 19"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.protocol.http.sampler.HTTPSamplerFull$Test  > ${experiment_root}/jakarta-jmeter/outputs/t19 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 19

echo ">>>>>>>>running test 20"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.protocol.http.modifier.URLRewritingModifier$Test  > ${experiment_root}/jakarta-jmeter/outputs/t20 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 20

echo ">>>>>>>>running test 21"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.protocol.http.parser.HtmlParser$Test  > ${experiment_root}/jakarta-jmeter/outputs/t21 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 21

echo ">>>>>>>>running test 22"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.protocol.http.proxy.ProxyControl$Test  > ${experiment_root}/jakarta-jmeter/outputs/t22 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 22

echo ">>>>>>>>running test 23"
$JAVA_HOME/bin/java junit.textui.TestRunner org.apache.jmeter.protocol.http.util.HTTPArgument$Test  > ${experiment_root}/jakarta-jmeter/outputs/t23 2>&1
${experiment_root}/jakarta-jmeter/testplans.alt/testscripts/RemoveTime.sh 23




# hcai vim :set ts=4 tw=4 tws=4

