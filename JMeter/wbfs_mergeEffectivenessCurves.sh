#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 srcdir dstfile"
	exit 1
fi
srcdir=$1
dstfile=$2

source ./jmeter_global.sh
CURVEFILE=curveEffectiveness-${ver}${seed}-$change

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/TestAdequacy/bin:$ROOT/tools/java_cup.jar"

C=(34326 18083 34452 34445 3255 27992 19081)  # full set of changes covered at least by one of the 80 tests - s9->s11
N=(1 2 5 6 11 13 19)
for algo in inc rand observed;
do
	inputfiles=""
	for ((i=0;i<7;i++));
	do
		inputfiles=$inputfiles" "${srcdir}/curveEffectiveness-v2s${N[$i]}-${C[$i]}-$algo
	done

	java -Xmx1600m -ea -cp $MAINCP \
		experiment.sensa.getCostOfEffectiveness \
		${inputfiles} \
		"$dstfile-$algo"
done
exit 0

# hcai vim :set ts=4 tw=4 tws=4

