#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./jmeter_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

mkdir -p out-DiverBrInstr

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$subjectloc/bin/${ver}${seed}:$ROOT/workspace/mcia/bin:$ROOT/workspace/Deam/bin

for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/DiverBrInstrumented-$ver-$seed
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-allowphantom \
	#-debug \
	#-dumpJimple \
   	#-duaverbose \
java -Xss4096m -Xmx6000m -ea -cp ${MAINCP} Diver.DiverBranchInst \
	-w -cp ${SOOTCP} \
	-p cg verbose:false,implicit-entry:false -p cg.spark verbose:false,on-fly-cg:true,rta:true \
	-f c -d "$OUTDIR" -brinstr:off -duainstr:off \
	-slicectxinsens \
	-allowphantom \
	-dumpJimple \
	-main-class org.apache.jorphan.test.AllTestsSelect \
	-entry:org.apache.jorphan.test.AllTestsSelect \
	-process-dir $subjectloc/bin/${ver}${seed} \
	1>out-DiverBrInstr/instr-${ver}${seed}.out 2>out-DiverBrInstr/instr-${ver}${seed}.err
stoptime=`date +%s%N | cut -b1-13`

echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds
cp -f $OUTDIR/entitystmt.out.branch $subjectloc/

echo "Instrumentation done, now copying resources required for running."
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/resources $OUTDIR/org/apache/jmeter/
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/help.txt $OUTDIR/org/apache/jmeter/
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/images $OUTDIR/org/apache/jmeter/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

