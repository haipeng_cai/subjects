#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./jmeter_global.sh

INDIR=$subjectloc/DiverBrInstrumented-$ver-$seed
#source ./jmeter_global.sh

MAINCP=".:$ROOT/tools/j2re1.4.2_18/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$INDIR:$ROOT/workspace/mcia/bin"

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

: '
for i in $subjectloc/lib/ext/*.jar;
do
	MAINCP=$MAINCP:$i
done
'

OUTDIR=$subjectloc/DiverBroutdyn-${ver}${seed}
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#"-fullseq" 
java -Xmx2800m -ea -cp ${MAINCP} Diver.DiverRun \
	"$DRIVERCLASS" \
	"$subjectloc" \
	"$INDIR" \
	${ver}${seed} \
	$OUTDIR 

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

