#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

source ./jmeter_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/ProbSli/bin"

mkdir -p out-instr
INDIR=$subjectloc/instrumented-$ver-$seed-$change

#$ROOT/tools/j2re1.4.2_18/lib/rt.jar:
#/etc/alternatives/java_sdk/jre/lib/rt.jar:
#SOOTCP=".:$ROOT/tools/j2re1.4.2_18/lib/rt.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/ProbSli/bin:$subjectloc/bin/${ver}${seed}:$subjectloc/bin/$ver$seed/jorphan:$subjectloc/bin/$ver$seed/core:$subjectloc/bin/$ver$seed/protocol:$subjectloc/bin/$ver$seed/components:$subjectloc/bin/$ver$seed/functions:$subjectloc/bin/$ver$seed/protocol/ftp:$subjectloc/bin/$ver$seed/protocol/http:$subjectloc/bin/$ver$seed/protocol/java:$subjectloc/bin/$ver$seed/protocol/jdbc:$ROOT/workspace/Sensa/bin"

#SOOTCP=".:$ROOT/tools/j2re1.4.2_18/lib/rt.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/tools/DUAForensics-bins-code/InstrReporters:$ROOT/tools/DUAForensics-bins-code/DUAForensics:$ROOT/workspace/ProbSli/bin:$subjectloc/bin/$ver$seed/jorphan:$subjectloc/bin/$ver$seed/core:$ROOT/workspace/Sensa/bin"
SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin/:$ROOT/workspace/Sensa/bin:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$INDIR"

for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/instr-instrumented-$ver-$seed-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`

	#-allowphantom \
java -Xmx1600m -ea -cp ${MAINCP} Sensa.SensaInst \
	-w -cp $SOOTCP -p cg verbose:true,implicit-entry:false \
	-p cg.spark verbose:true,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
   	-duaverbose \
	-slicectxinsens \
	-exechist \
	-start:$change \
	-main-class org.apache.jorphan.test.AllTestsSelect \
	-entry:org.apache.jorphan.test.AllTestsSelect \
	-process-dir $INDIR \
	1>out-instr/instrInstrumented-$change-${ver}${seed}.out 2>out-instr/instrInstrumented-$change-${ver}${seed}.err

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for $change-${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Instrumentation done, now copying resources required for running."
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/resources $OUTDIR/org/apache/jmeter/
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/help.txt $OUTDIR/org/apache/jmeter/
cp -fr $subjectloc/source_allseeds/${ver}${seed}/core/org/apache/jmeter/images $OUTDIR/org/apache/jmeter/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

