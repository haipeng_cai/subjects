#!/bin/sh
##!/usr/local/bin/bash

if [ $# -lt 1 ]; then
    echo "Must give version number to install"
    exit 1
fi

if [ ! -d ${experiment_root}/jakarta-jmeter/versions.alt/seeded/v${1} ]; then
    echo "Invalid version number"
    exit 1
fi

if [ -d ${experiment_root}/jakarta-jmeter/source/JMeter ]; then
    rm -rf ${experiment_root}/jakarta-jmeter/source/JMeter
fi

rm -rf ${experiment_root}/source/*

echo copying...
cp -rf ${experiment_root}/jakarta-jmeter/versions.alt/seeded/v${1}/JMeter ${experiment_root}/jakarta-jmeter/source

CLASSPATH="$CLASSPATH"
export CLASSPATH
current_dir=`pwd`
cp ${experiment_root}/jakarta-jmeter/scripts/EqualizeLineNumbers*.class ${experiment_root}/jakarta-jmeter/source/.
cd ${experiment_root}/jakarta-jmeter/source/JMeter
# turn off previous fault
find ./ -name "*.cpp" >  __tmpfile
while read LINE
do
        java EqualizeLineNumbers $LINE 0 `echo $LINE | sed "s/\.cpp/\.java/"`
done < __tmpfile
rm __tmpfile

sh build.sh test

# hcai vim :set ts=4 tw=4 tws=4

