#!/bin/bash

VERSION=v0
SEEDS="1 2 3 4 5 6 7"
ROOT=/home/hcai
DRIVERCLASS=org.apache.zookeeper.test.AllTestsSelect
RUNALLCLASS=org.apache.zookeeper.test.AllTestsRun
RUNPERCLASS=org.apache.zookeeper.test.TestSelector
subjectloc=$ROOT/abc/zookeeper-3.4.6/
C=(22034) # after insert the __link invocations

JAVA=$ROOT/tools/jdk160/bin/java

# hcai vim :set ts=4 tw=4 tws=4
