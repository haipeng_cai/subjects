#!/bin/bash
if [ $# -lt 0 ];then
	echo "Usage: $0 "
	exit 1
fi

source ./zk_global.sh

MAINCP=".:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/mcia/bin:$subjectloc/build/classes:$subjectloc/build/test/classes:$subjectloc/build/contrib/fatjar/classes:$subjectloc/lib/netty-3.7.0.Final.jar"

for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

suffix="zk"

LOGDIR=out-distEAInstr
mkdir -p $LOGDIR
logout=$LOGDIR/instr-$suffix.out
logerr=$LOGDIR/instr-$suffix.err

OUTDIR=$subjectloc/distEAInstrumented
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`

	#-allowphantom \
   	#-duaverbose \
	#-wrapTryCatch \
	#-dumpJimple \
	#-statUncaught \
	#-perthread \
	#-syncnio \
	#-main-class $DRIVERCLASS \
	#-entry:$DRIVERCLASS \
	#-syncnio \
	#-syncnio \
	#-main-class org.apache.zookeeper.util.FatJarMain \
	#-entry:org.apache.zookeeper.util.FatJarMain \
	#-process-dir $subjectloc/build/contrib/fatjar/classes \
java -Xmx40600m -ea -cp ${MAINCP} distEA.distEAInst \
	-w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
	-allowphantom \
	-nio \
	-socket \
	-wrapTryCatch \
	-dumpFunctionList \
	-slicectxinsens \
	-process-dir $subjectloc/build/classes \
	-process-dir $subjectloc/build/test/classes \
	1> $logout 2> $logerr

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for $suffix elapsed: " `expr $stoptime - $starttime` milliseconds

#echo "Instrumentation done, now copying resources required for running."
#cp -rf $subjectloc/build/classes/org/apache/fop/cli/image2fo.xsl $OUTDIR/org/apache/fop/cli/
#cp -rf $subjectloc/build/classes/org/apache/fop/pdf/*.icm* $OUTDIR/org/apache/fop/pdf/
#cp -rf $subjectloc/build/classes/org/apache/fop/render/awt/viewer/{images,resources} $OUTDIR/org/apache/fop/render/awt/viewer/
#cp -rf $subjectloc/build/classes/META-INF $OUTDIR/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

