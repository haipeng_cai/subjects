package org.apache.zookeeper.test;
/**
 * Similar to AllTestsSelect but run all test methods in a test class as a single test 
 *
 *@author     $Author: hcai $
 *@created    $Date: 2015/02/05 $
 *@version    $Revision: 1.0.0.0 $
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Properties;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.LinkedHashMap;
import java.util.Comparator;
//import java.util.logging.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import junit.framework.*;
import org.junit.runner.*;
import org.junit.runner.notification.Failure;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runner.JUnitCore;
import org.junit.runners.model.InitializationError;
import org.junit.internal.TextListener;

import profile.ExecHistReporter;
import change.DynSliceReporter;
import profile.BranchReporter;
import profile.DUAReporter;

public class TestSelector
{
	static void __link() {
		/*
		BranchReporter.__link(); 
		DUAReporter.__link(); 
		DynSliceReporter.__link(); 
		ExecHistReporter.__link(); 
		*/
		distEA.distMonitor.__link(); 
		/*
		EAS.Monitor.__link(); 
		Diver.EAMonitor.__link();
		mut.Modify.__link();
		Sensa.Modify.__link();
		*/
	}

	/** {@link org.apache.log4j} logging facility */
	static org.apache.log4j.Category cat = org.apache.log4j.Category
			.getInstance(TestSelector.class.getName());

	public static void main(String[] args) {
		org.apache.log4j.BasicConfigurator.configure();
		//Logger.getRootLogger().setLevel(Level.WARN);

		Map<String, Class> tsSorted = sortTestSuite(createSuite());
		Map<Class, String> tsSelect = new LinkedHashMap<Class, String>();
		// System.out.println("==================== all individual test cases ===================== ");
		for (String tn : tsSorted.keySet()) {
			// hcai: to dump the complete test case list
			// System.out.println(tn);
			if (args.length == 0 || args[0].trim().equals(tn.trim())) {
				tsSelect.put( tsSorted.get( tn ), tn );
			}
		}

		/*
		int a = 1;
		if (a != 2) return;
		*/
		
		if (tsSelect.size() == 0)
			throw new RuntimeException("ABORTING: NO TEST CASES TO EXECUTE");
		
		JUnitCore ju = new JUnitCore();
		ju.addListener(new TextListener(System.out));
		for (Class cls : tsSelect.keySet()) {
			Result res = ju.runClasses( cls );
			// System.err.println("1 test [" + tsSelect.get(r) + "] :" + (res.wasSuccessful()? "passed" : "failed"));
			System.out.println(".");
			System.out.println("Time: " + res.getRunTime() + "\n");
			if (res.wasSuccessful()) {
				// System.out.println("OK (1 tests) \n");
				System.out.println("OK (" + res.getRunCount() + " tests) \n");
			}
			else {
				System.out.println("FAILED (" + res.getFailureCount() + "/" + res.getRunCount() + " tests) \n");
				for (Failure fl : res.getFailures()) {
					System.err.println("\nFailure:");
					System.err.println(fl);
					System.err.println("\nMessage:");
					System.err.println(fl.getMessage());
					System.err.println("\nTrace:");
					System.err.println(fl.getTrace());
					System.err.println("\nDescription:");
					System.err.println(fl.getDescription()+"\n");
				}
			}
		}
	}

	/* collect all test classes; for now the list of classes is filled manually, but instead an automatic search could replace this laborious manner */
	public static List<Class> createSuite() {

		List<Class> testClses = new ArrayList();

		try
		{
			testClses.add (org.apache.zookeeper.ClientReconnectTest.class);
			testClses.add (org.apache.zookeeper.MultiResponseTest.class);
			testClses.add (org.apache.zookeeper.MultiTransactionRecordTest.class);
			testClses.add (org.apache.zookeeper.server.CRCTest.class);
			testClses.add (org.apache.zookeeper.server.DatadirCleanupManagerTest.class);
			testClses.add (org.apache.zookeeper.server.DataTreeTest.class);
			testClses.add (org.apache.zookeeper.server.DataTreeUnitTest.class);
			testClses.add (org.apache.zookeeper.server.DeserializationPerfTest.class);
			testClses.add (org.apache.zookeeper.server.InvalidSnapCountTest.class);
			testClses.add (org.apache.zookeeper.server.InvalidSnapshotTest.class);
			testClses.add (org.apache.zookeeper.server.NettyServerCnxnTest.class);
			testClses.add (org.apache.zookeeper.server.NIOServerCnxnTest.class);
			testClses.add (org.apache.zookeeper.server.PrepRequestProcessorTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.CnxManagerTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.FLEBackwardElectionRoundTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.FLECompatibilityTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.FLEDontCareTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.FLELostMessageTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.LearnerTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.QuorumPeerMainTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.WatchLeakTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.Zab1_0Test.class);
			testClses.add (org.apache.zookeeper.server.SerializationPerfTest.class);
			testClses.add (org.apache.zookeeper.server.SessionTrackerTest.class);
			testClses.add (org.apache.zookeeper.server.ToStringTest.class);
			testClses.add (org.apache.zookeeper.server.ZooKeeperServerMainTest.class);
			testClses.add (org.apache.zookeeper.server.ZooKeeperServerTest.class);
			testClses.add (org.apache.zookeeper.server.ZxidRolloverTest.class);
			testClses.add (org.apache.zookeeper.test.ACLCountTest.class);
			testClses.add (org.apache.zookeeper.test.ACLRootTest.class);
			testClses.add (org.apache.zookeeper.test.ACLTest.class);
			testClses.add (org.apache.zookeeper.test.AsyncHammerTest.class);
			testClses.add (org.apache.zookeeper.test.AsyncOpsTest.class);
			testClses.add (org.apache.zookeeper.test.AsyncTest.class);
			testClses.add (org.apache.zookeeper.test.AtomicFileOutputStreamTest.class);
			testClses.add (org.apache.zookeeper.test.AuthTest.class);
			testClses.add (org.apache.zookeeper.test.BufferSizeTest.class);
			testClses.add (org.apache.zookeeper.test.ChrootAsyncTest.class);
			testClses.add (org.apache.zookeeper.test.ChrootClientTest.class);
			testClses.add (org.apache.zookeeper.test.ChrootTest.class);
			testClses.add (org.apache.zookeeper.test.ClientHammerTest.class);
			testClses.add (org.apache.zookeeper.test.ClientPortBindTest.class);
			testClses.add (org.apache.zookeeper.test.ClientTest.class);
			testClses.add (org.apache.zookeeper.test.ConnectStringParserTest.class);
			testClses.add (org.apache.zookeeper.test.CreateModeTest.class);
			testClses.add (org.apache.zookeeper.test.DisconnectedWatcherTest.class);
			testClses.add (org.apache.zookeeper.test.EventTypeTest.class);
			testClses.add (org.apache.zookeeper.test.FLENewEpochTest.class);
			testClses.add (org.apache.zookeeper.test.FLEPredicateTest.class);
			testClses.add (org.apache.zookeeper.test.FLERestartTest.class);
			testClses.add (org.apache.zookeeper.test.FLETest.class);
			testClses.add (org.apache.zookeeper.test.FLEZeroWeightTest.class);
			testClses.add (org.apache.zookeeper.test.FollowerResyncConcurrencyTest.class);
			testClses.add (org.apache.zookeeper.test.FollowerTest.class);
			testClses.add (org.apache.zookeeper.test.FourLetterWordsQuorumTest.class);
			testClses.add (org.apache.zookeeper.test.FourLetterWordsTest.class);
			testClses.add (org.apache.zookeeper.test.GetChildren2Test.class);
			testClses.add (org.apache.zookeeper.test.HierarchicalQuorumTest.class);
			testClses.add (org.apache.zookeeper.test.InvalidSnapshotTest.class);
			testClses.add (org.apache.zookeeper.test.KeeperStateTest.class);
			testClses.add (org.apache.zookeeper.test.LENonTerminateTest.class);
			testClses.add (org.apache.zookeeper.test.LETest.class);
			testClses.add (org.apache.zookeeper.test.LoadFromLogTest.class);
			testClses.add (org.apache.zookeeper.test.MaxCnxnsTest.class);
			testClses.add (org.apache.zookeeper.test.MultiTransactionTest.class);
			testClses.add (org.apache.zookeeper.test.NioNettySuiteHammerTest.class);
			testClses.add (org.apache.zookeeper.test.NioNettySuiteTest.class);
			testClses.add (org.apache.zookeeper.test.NullDataTest.class);
			testClses.add (org.apache.zookeeper.test.ObserverHierarchicalQuorumTest.class);
			testClses.add (org.apache.zookeeper.test.ObserverLETest.class);
			testClses.add (org.apache.zookeeper.test.ObserverQuorumHammerTest.class);
			testClses.add (org.apache.zookeeper.test.ObserverTest.class);
			testClses.add (org.apache.zookeeper.test.OOMTest.class);
			testClses.add (org.apache.zookeeper.test.OSMXBeanTest.class);
			testClses.add (org.apache.zookeeper.test.PurgeTxnTest.class);
			testClses.add (org.apache.zookeeper.test.QuorumHammerTest.class);
			testClses.add (org.apache.zookeeper.test.QuorumQuotaTest.class);
			testClses.add (org.apache.zookeeper.test.QuorumTest.class);
			testClses.add (org.apache.zookeeper.test.QuorumZxidSyncTest.class);
			testClses.add (org.apache.zookeeper.test.ReadOnlyModeTest.class);
			testClses.add (org.apache.zookeeper.test.RecoveryTest.class);
			testClses.add (org.apache.zookeeper.test.RepeatStartupTest.class);
			testClses.add (org.apache.zookeeper.test.RestoreCommittedLogTest.class);
			testClses.add (org.apache.zookeeper.test.SaslAuthDesignatedClientTest.class);
			testClses.add (org.apache.zookeeper.test.SaslAuthDesignatedServerTest.class);
			testClses.add (org.apache.zookeeper.test.SaslAuthFailDesignatedClientTest.class);
			testClses.add (org.apache.zookeeper.test.SaslAuthFailNotifyTest.class);
			testClses.add (org.apache.zookeeper.test.SaslAuthFailTest.class);
			testClses.add (org.apache.zookeeper.test.SaslAuthMissingClientConfigTest.class);
			testClses.add (org.apache.zookeeper.test.SaslAuthTest.class);
			testClses.add (org.apache.zookeeper.test.SaslClientTest.class);
			testClses.add (org.apache.zookeeper.test.SessionInvalidationTest.class);
			testClses.add (org.apache.zookeeper.test.SessionTest.class);
			testClses.add (org.apache.zookeeper.test.StandaloneTest.class);
			testClses.add (org.apache.zookeeper.test.StaticHostProviderTest.class);
			testClses.add (org.apache.zookeeper.test.StatTest.class);
			testClses.add (org.apache.zookeeper.test.SyncCallTest.class);
			testClses.add (org.apache.zookeeper.test.TruncateTest.class);
			testClses.add (org.apache.zookeeper.test.UpgradeTest.class);
			testClses.add (org.apache.zookeeper.test.WatchedEventTest.class);
			testClses.add (org.apache.zookeeper.test.WatcherFuncTest.class);
			testClses.add (org.apache.zookeeper.test.WatcherTest.class);
			testClses.add (org.apache.zookeeper.test.ZkDatabaseCorruptionTest.class);
			testClses.add (org.apache.zookeeper.test.ZooKeeperQuotaTest.class);
			testClses.add (org.apache.zookeeper.VerGenTest.class);
			testClses.add (org.apache.zookeeper.ZooKeeperTest.class);
		}
		catch (Exception ex)
		{
			System.err.println("error adding test :"+ex);
		}

		return testClses;
	}

	/* retrieve test cases from test classes following the standard convention (test prefix, for instance); and 
	 * then sort all test cases by their names
	 */ 
	private static Map<String,Class> sortTestSuite(List<Class> testClses) {
		Map<String, Class> allTests = new TreeMap<String, Class>(StringBasedComparator.inst);
		
		for (Class cls : testClses) {
			String testName = cls.getName();
			allTests.put(testName, cls);
		}

		return allTests;

		/*
		Collections.sort(allTests, StringBasedComparator.inst);

		TestSuite tsSorted = new TestSuite("entire unit test suite");
		for (Iterator it = allTests.iterator();it.hasNext();) {
			tsSorted.addTest((Test) it.next());
		}

		return tsSorted;
		*/
	}
}

/* hcai vim :set ts=4 tw=4 tws=4 */

