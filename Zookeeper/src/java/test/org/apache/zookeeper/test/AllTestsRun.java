package org.apache.zookeeper.test;

import java.io.*;
import java.util.Iterator;
import java.util.Properties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Comparator;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runner.JUnitCore;

import profile.ExecHistReporter;
import change.DynSliceReporter;
import profile.BranchReporter;
import profile.DUAReporter;

public class AllTestsRun
{
	/** Generic comparator for objects based on the result of toString. Null is the "minimum". */
	/*
	public static class StringBasedComparator implements Comparator {
		private StringBasedComparator() {}
		
		public int compare(Object o1, Object o2) {
			if (o1 == null)
				return (o2 == null)? 0 : -1; // o1 is equal to or less than o2
			if (o2 == null)
				return 1; // o1 is greater than o2
			return o1.toString().compareTo(o2.toString());
		}
	}
	*/

	static void __link() { 
		BranchReporter.__link(); DUAReporter.__link(); DynSliceReporter.__link(); ExecHistReporter.__link(); 
		EAS.Monitor.__link(); 
		//mut.Modify.__link();
		Diver.EAMonitor.__link();
		//Sensa.Modify.__link();
	}

	public static void main(String[] args) {
		// added by hcai to hold off the log4j erros complaining about the lack of appenders for 
		org.apache.log4j.BasicConfigurator.configure();
		// - 

		TestSuite tsSorted = sortTestSuite((TestSuite) suite());
		TestSuite tsSelect = new TestSuite();
		for (Enumeration e = tsSorted.tests(); e.hasMoreElements(); ) {
			TestCase t = (TestCase) e.nextElement();
			// debug
			// System.out.println("[TEST]----" + t);
			if (args.length == 0 || args[0].equals(t.toString())) 
				tsSelect.addTest(t);
		}
		if (tsSelect.testCount() == 0)
			throw new RuntimeException("ABORTING: NO TEST CASES TO EXECUTE");
		
		junit.textui.TestRunner.run(tsSelect);
	}

	public static Test suite() {
		// hcai: add test classes directly without relying on the system class loader in DoAllTests.suite();
		//		which is required by the mutation-based mcia study framework that has to run EAS and mDEA subjects 
		//		using app-level class-loader; this is also useful for other applications which need load classes
		//		using app-level class-loader too.
		// return DoAllTests.suite();

		TestSuite suite = new TestSuite("All org.apache.zookeeper.test JUnit Tests");

		try
		{
			TestSuiteAll alltests = new TestSuiteAll();

			JUnitCore.runClasses( TestSuiteAll.class );
			/*
			BlockJUnit4ClassRunner runner = new BlockJUnit4ClassRunner(org.apache.zookeeper.test.TestSuiteAll.class) {
				@Override
				public java.util.List<org.junit.runners.model.FrameworkMethod> getChildren() {
					return super.getChildren();
				}

				@Override
				public java.lang.String testName(org.junit.runners.model.FrameworkMethod method) {
					return super.testName(method);
				}
			};
			System.out.println("test count=" + runner.testCount());
			*/
			//System.out.println("tests =%s\n", runner.getChildren());

		}
		catch (Exception ex)
		{
			System.err.println("error adding test :"+ex);
		}

		return suite;
	}

	private static TestSuite sortTestSuite(TestSuite ts) {
		List tests = new ArrayList();
		getTestsRecursive(ts, tests);

		Collections.sort(tests, StringBasedComparator.inst);

		TestSuite tsSorted = new TestSuite();
		for (Iterator it = tests.iterator(); it.hasNext();)
			tsSorted.addTest((TestCase) it.next());

		return tsSorted;
	}

	private static void getTestsRecursive(Test t, List tests) {
		if (t instanceof TestCase)
			tests.add(t);
		else
			for (Enumeration e = ((TestSuite) t).tests(); e.hasMoreElements();)
				getTestsRecursive((Test) e.nextElement(), tests);
	}
}


