package org.apache.zookeeper.test;
/**
 *  This is a test driver that collects all test cases from the given list of test classes that are written
 *  in the framework of Junit4 instead of Junit3;
 *
 *  Without inputing any argument, this driver will run all test cases garnered as in a single holistic test suite, 
 *  otherwise, it will run only the single test case matching the given name provided as the input argument;
 *  
 *  This test driver is intended to serve a test driver template that supports test separation for unit tests written in the 
 *  paradigm of JUnit4
 *
 *@author     $Author: hcai $
 *@created    $Date: 2015/02/04 $
 *@version    $Revision: 1.0.0.0 $
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Properties;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.LinkedHashMap;
import java.util.Comparator;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import junit.framework.*;
import org.junit.runner.*;
import org.junit.runner.notification.Failure;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runner.JUnitCore;
import org.junit.runners.model.InitializationError;
import org.junit.internal.TextListener;

import org.junit.runner.*;
import org.junit.runner.notification.Failure;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runner.notification.RunNotifier;

import java.util.Arrays;

import profile.ExecHistReporter;
import change.DynSliceReporter;
import profile.BranchReporter;
import profile.DUAReporter;

public class AllTestsSelect
{
	static void __link() {
		/*
		BranchReporter.__link(); 
		DUAReporter.__link(); 
		DynSliceReporter.__link(); 
		ExecHistReporter.__link(); 
		*/

		/*
		EAS.Monitor.__link(); 
		Diver.EAMonitor.__link();
		mut.Modify.__link();
		Sensa.Modify.__link();
		*/

		//distEA.distThreadMonitor.__link(); 
		distEA.distMonitor.__link(); 
	}

	/** {@link org.apache.log4j} logging facility */
	static org.apache.log4j.Category cat = org.apache.log4j.Category
			.getInstance(AllTestsSelect.class.getName());

	 // private static Result runTest(final Class<?> testClazz, final String methodName)
	 private static Runner runTest(final Class<?> testClazz, final String methodName)
		    throws InitializationError {
		BlockJUnit4ClassRunner runner = new BlockJUnit4ClassRunner(testClazz) {
		    @Override
		    protected List<FrameworkMethod> computeTestMethods() {
		        try {
		            Method method = testClazz.getMethod(methodName);
		            return Arrays.asList(new FrameworkMethod(method));

		        } catch (Exception e) {
		            throw new RuntimeException(e);
		        }
		    }
		};
		return runner;
		/*
		Result res = new Result();
		runner.run(res);
		return res;
		*/
	}

	/*
	public static void main(String[] args) {
		org.apache.log4j.BasicConfigurator.configure();

		Map<String, Request> tsSorted = sortTestSuite(createSuite());
		Map<Request, String> tsSelect = new LinkedHashMap<Request, String>();
		// System.out.println("==================== all individual test cases ===================== ");
		for (String tn : tsSorted.keySet()) {
			// hcai: to dump the complete test case list
			// System.out.println(tn);
			if (args.length == 0 || args[0].trim().equals(tn.trim())) {
				tsSelect.put( tsSorted.get( tn ), tn );
			}
		}

		if (tsSelect.size() == 0)
			throw new RuntimeException("ABORTING: NO TEST CASES TO EXECUTE");
		
		JUnitCore ju = new JUnitCore();
		ju.addListener(new TextListener(System.out));
		for (Request r : tsSelect.keySet()) {
			if (tsSelect.get(r).contains("org.apache.zookeeper.VerGenTest")) {
				JUnitCore.runClasses(org.apache.zookeeper.VerGenTest.class);
				continue;
			}
			Result res = ju.run( r );
			// System.err.println("1 test [" + tsSelect.get(r) + "] :" + (res.wasSuccessful()? "passed" : "failed"));
			System.out.println(".");
			System.out.println("Time: " + res.getRunTime() + "\n");
			if (res.wasSuccessful()) {
				System.out.println("OK (1 tests) \n");
			}
			else {
				for (Failure fl : res.getFailures()) {
					System.err.println("\nFailure:");
					System.err.println(fl);
					System.err.println("\nMessage:");
					System.err.println(fl.getMessage());
					System.err.println("\nTrace:");
					System.err.println(fl.getTrace());
					System.err.println("\nDescription:");
					System.err.println(fl.getDescription()+"\n");
				}
			}
		}
	}
	*/

	public static void main(String[] args) {
		Map<String, test_pair> tsSorted = sortTestSuite(createSuite());
		List<test_pair> tsSelect = new ArrayList<test_pair>();

		org.apache.log4j.BasicConfigurator.configure();

		for (String tn : tsSorted.keySet()) {
			// hcai: to dump the complete test case list
			// System.out.println(tn);
			//
			// String tn = tsSorted.get(cls)+"("+cls.getName()+")";
			if (args.length == 0 || args[0].trim().equals(tn.trim())) {
				tsSelect.add( tsSorted.get(tn) );
			}
		}
		
		if (tsSelect.size() == 0)
			throw new RuntimeException("ABORTING: NO TEST CASES TO EXECUTE matching " + args[0]);
		
		for (test_pair tp : tsSelect) {
			// Result res = new JUnitCore().run( r );
			Result res = null;

			try {
				res = new JUnitCore().run(runTest(tp.cls, tp.mname));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

			// System.err.println("1 test [" + tsSelect.get(r) + "] :" + (res.wasSuccessful()? "passed" : "failed"));
			System.out.println(".");
			System.out.println("Time: " + res.getRunTime() + "\n");
			if (res.wasSuccessful()) {
				System.out.println("OK (1 tests) \n");
			}
			else {
				for (Failure fl : res.getFailures()) {
					System.err.println("\nFailure:");
					System.err.println(fl);
					System.err.println("\nMessage:");
					System.err.println(fl.getMessage());
					System.err.println("\nTrace:");
					System.err.println(fl.getTrace());
					System.err.println("\nDescription:");
					System.err.println(fl.getDescription()+"\n");
				}
			}
		}
	}

	/* collect all test classes; for now the list of classes is filled manually, but instead an automatic search could replace this laborious manner */
	public static List<Class> createSuite() {

		List<Class> testClses = new ArrayList();

		try
		{
			testClses.add (org.apache.zookeeper.ClientReconnectTest.class);
			testClses.add (org.apache.zookeeper.MultiResponseTest.class);
			testClses.add (org.apache.zookeeper.MultiTransactionRecordTest.class);
			testClses.add (org.apache.zookeeper.server.CRCTest.class);
			testClses.add (org.apache.zookeeper.server.DatadirCleanupManagerTest.class);
			testClses.add (org.apache.zookeeper.server.DataTreeTest.class);
			testClses.add (org.apache.zookeeper.server.DataTreeUnitTest.class);
			testClses.add (org.apache.zookeeper.server.DeserializationPerfTest.class);
			testClses.add (org.apache.zookeeper.server.InvalidSnapCountTest.class);
			testClses.add (org.apache.zookeeper.server.InvalidSnapshotTest.class);
			testClses.add (org.apache.zookeeper.server.NettyServerCnxnTest.class);
			testClses.add (org.apache.zookeeper.server.NIOServerCnxnTest.class);
			testClses.add (org.apache.zookeeper.server.PrepRequestProcessorTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.CnxManagerTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.FLEBackwardElectionRoundTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.FLECompatibilityTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.FLEDontCareTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.FLELostMessageTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.LearnerTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.QuorumPeerMainTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.WatchLeakTest.class);
			testClses.add (org.apache.zookeeper.server.quorum.Zab1_0Test.class);
			testClses.add (org.apache.zookeeper.server.SerializationPerfTest.class);
			testClses.add (org.apache.zookeeper.server.SessionTrackerTest.class);
			testClses.add (org.apache.zookeeper.server.ToStringTest.class);
			testClses.add (org.apache.zookeeper.server.ZooKeeperServerMainTest.class);
			testClses.add (org.apache.zookeeper.server.ZooKeeperServerTest.class);
			testClses.add (org.apache.zookeeper.server.ZxidRolloverTest.class);
			testClses.add (org.apache.zookeeper.test.ACLCountTest.class);
			testClses.add (org.apache.zookeeper.test.ACLRootTest.class);
			testClses.add (org.apache.zookeeper.test.ACLTest.class);
			testClses.add (org.apache.zookeeper.test.AsyncHammerTest.class);
			testClses.add (org.apache.zookeeper.test.AsyncOpsTest.class);
			testClses.add (org.apache.zookeeper.test.AsyncTest.class);
			testClses.add (org.apache.zookeeper.test.AtomicFileOutputStreamTest.class);
			testClses.add (org.apache.zookeeper.test.AuthTest.class);
			testClses.add (org.apache.zookeeper.test.BufferSizeTest.class);
			testClses.add (org.apache.zookeeper.test.ChrootAsyncTest.class);
			testClses.add (org.apache.zookeeper.test.ChrootClientTest.class);
			testClses.add (org.apache.zookeeper.test.ChrootTest.class);
			testClses.add (org.apache.zookeeper.test.ClientHammerTest.class);
			testClses.add (org.apache.zookeeper.test.ClientPortBindTest.class);
			testClses.add (org.apache.zookeeper.test.ClientTest.class);
			testClses.add (org.apache.zookeeper.test.ConnectStringParserTest.class);
			testClses.add (org.apache.zookeeper.test.CreateModeTest.class);
			testClses.add (org.apache.zookeeper.test.DisconnectedWatcherTest.class);
			testClses.add (org.apache.zookeeper.test.EventTypeTest.class);
			testClses.add (org.apache.zookeeper.test.FLENewEpochTest.class);
			testClses.add (org.apache.zookeeper.test.FLEPredicateTest.class);
			testClses.add (org.apache.zookeeper.test.FLERestartTest.class);
			testClses.add (org.apache.zookeeper.test.FLETest.class);
			testClses.add (org.apache.zookeeper.test.FLEZeroWeightTest.class);
			testClses.add (org.apache.zookeeper.test.FollowerResyncConcurrencyTest.class);
			testClses.add (org.apache.zookeeper.test.FollowerTest.class);
			testClses.add (org.apache.zookeeper.test.FourLetterWordsQuorumTest.class);
			testClses.add (org.apache.zookeeper.test.FourLetterWordsTest.class);
			testClses.add (org.apache.zookeeper.test.GetChildren2Test.class);
			testClses.add (org.apache.zookeeper.test.HierarchicalQuorumTest.class);
			testClses.add (org.apache.zookeeper.test.InvalidSnapshotTest.class);
			testClses.add (org.apache.zookeeper.test.KeeperStateTest.class);
			testClses.add (org.apache.zookeeper.test.LENonTerminateTest.class);
			testClses.add (org.apache.zookeeper.test.LETest.class);
			testClses.add (org.apache.zookeeper.test.LoadFromLogTest.class);
			testClses.add (org.apache.zookeeper.test.MaxCnxnsTest.class);
			testClses.add (org.apache.zookeeper.test.MultiTransactionTest.class);
			testClses.add (org.apache.zookeeper.test.NioNettySuiteHammerTest.class);
			testClses.add (org.apache.zookeeper.test.NioNettySuiteTest.class);
			testClses.add (org.apache.zookeeper.test.NullDataTest.class);
			testClses.add (org.apache.zookeeper.test.ObserverHierarchicalQuorumTest.class);
			testClses.add (org.apache.zookeeper.test.ObserverLETest.class);
			testClses.add (org.apache.zookeeper.test.ObserverQuorumHammerTest.class);
			testClses.add (org.apache.zookeeper.test.ObserverTest.class);
			testClses.add (org.apache.zookeeper.test.OOMTest.class);
			testClses.add (org.apache.zookeeper.test.OSMXBeanTest.class);
			testClses.add (org.apache.zookeeper.test.PurgeTxnTest.class);
			testClses.add (org.apache.zookeeper.test.QuorumHammerTest.class);
			testClses.add (org.apache.zookeeper.test.QuorumQuotaTest.class);
			testClses.add (org.apache.zookeeper.test.QuorumTest.class);
			testClses.add (org.apache.zookeeper.test.QuorumZxidSyncTest.class);
			testClses.add (org.apache.zookeeper.test.ReadOnlyModeTest.class);
			testClses.add (org.apache.zookeeper.test.RecoveryTest.class);
			testClses.add (org.apache.zookeeper.test.RepeatStartupTest.class);
			testClses.add (org.apache.zookeeper.test.RestoreCommittedLogTest.class);
			testClses.add (org.apache.zookeeper.test.SaslAuthDesignatedClientTest.class);
			testClses.add (org.apache.zookeeper.test.SaslAuthDesignatedServerTest.class);
			testClses.add (org.apache.zookeeper.test.SaslAuthFailDesignatedClientTest.class);
			testClses.add (org.apache.zookeeper.test.SaslAuthFailNotifyTest.class);
			testClses.add (org.apache.zookeeper.test.SaslAuthFailTest.class);
			testClses.add (org.apache.zookeeper.test.SaslAuthMissingClientConfigTest.class);
			testClses.add (org.apache.zookeeper.test.SaslAuthTest.class);
			testClses.add (org.apache.zookeeper.test.SaslClientTest.class);
			testClses.add (org.apache.zookeeper.test.SessionInvalidationTest.class);
			testClses.add (org.apache.zookeeper.test.SessionTest.class);
			testClses.add (org.apache.zookeeper.test.StandaloneTest.class);
			testClses.add (org.apache.zookeeper.test.StaticHostProviderTest.class);
			testClses.add (org.apache.zookeeper.test.StatTest.class);
			testClses.add (org.apache.zookeeper.test.SyncCallTest.class);
			testClses.add (org.apache.zookeeper.test.TruncateTest.class);
			testClses.add (org.apache.zookeeper.test.UpgradeTest.class);
			testClses.add (org.apache.zookeeper.test.WatchedEventTest.class);
			testClses.add (org.apache.zookeeper.test.WatcherFuncTest.class);
			testClses.add (org.apache.zookeeper.test.WatcherTest.class);
			testClses.add (org.apache.zookeeper.test.ZkDatabaseCorruptionTest.class);
			testClses.add (org.apache.zookeeper.test.ZooKeeperQuotaTest.class);
			testClses.add (org.apache.zookeeper.VerGenTest.class);
			testClses.add (org.apache.zookeeper.ZooKeeperTest.class);
		}
		catch (Exception ex)
		{
			System.err.println("error adding test :"+ex);
		}

		return testClses;
	}

	/* retrieve test cases from test classes following the standard convention (test prefix, for instance); and 
	 * then sort all test cases by their names
	 */ 
	/*
	private static Map<String,Request> sortTestSuite(List<Class> testClses) {
		Map<String, Request> allTests = new TreeMap<String,Request>(StringBasedComparator.inst);
		
		for (Class cls : testClses) {
			for (Method m : cls.getDeclaredMethods() ) {
				if (!isPublicTestMethod(m)) {
					// skip non-test methods
					continue;
				}
				// create a test case for each public (accessible from outside of the class then) test method
				String testName = m.getName()+"("+cls.getName()+")"; // cls.getName()+": " + m.getName();
				allTests.put(testName, Request.method(cls, m.getName()));
			}
		}

		// testClses.remove (org.apache.zookeeper.VerGenTest.class);
		sortTestSuiteEx(testClses, allTests);

		return allTests;
	}
	*/
	static class test_pair {
		public Class cls;
		public String mname;
		public test_pair(Class cls, String mname) {
			this.cls = cls;
			this.mname = mname;
		}
	}

	private static Map<String,test_pair> sortTestSuite(List<Class> testClses) {
		Map<String, test_pair> allTests = new TreeMap<String,test_pair>(StringBasedComparator.inst);
		
		for (Class cls : testClses) {
			for (Method m : cls.getDeclaredMethods() ) {
				if (!isPublicTestMethod(m)) {
					// skip non-test methods
					continue;
				}
				// create a test case for each public (accessible from outside of the class then) test method
				String testName = m.getName()+"("+cls.getName()+")"; // cls.getName()+": " + m.getName();
				// allTests.put(testName, Request.method(cls, m.getName()));
				allTests.put(testName, new test_pair(cls, m.getName()));
			}
		}

		return allTests;

		/*
		Collections.sort(allTests, StringBasedComparator.inst);

		TestSuite tsSorted = new TestSuite("OpenNLP 1.5.3 entire unit test suite");
		for (Iterator it = allTests.iterator();it.hasNext();) {
			tsSorted.addTest((Test) it.next());
		}

		return tsSorted;
		*/
	}

	/*
	static class hcJUnit4ClassRunner extends BlockJUnit4ClassRunner {
		public hcJUnit4ClassRunner(Class<?> kcls) throws InitializationError {
			super(kcls);
		}
		//@Override
		public java.util.List<org.junit.runners.model.FrameworkMethod> getChildren() {
			return super.getChildren();
		}

		//@Override
		public java.lang.String testName(org.junit.runners.model.FrameworkMethod method) {
			return super.testName(method);
		}
	}

	private static Map<String,Request> sortTestSuiteEx(List<Class> testClses, Map<String, Request> allTests) {
		for (Class cls : testClses) {
			// System.out.print("In class: " + cls.getName() + " -> test count="); 
			hcJUnit4ClassRunner runner = null;
			try {
				runner = new hcJUnit4ClassRunner(cls);
			}
			catch(Exception e) {
				// e.printStackTrace();
				continue;
			}
			// System.out.println(runner.testCount());
			for (org.junit.runners.model.FrameworkMethod m : runner.getChildren() ) {
				// create a test case for each public (accessible from outside of the class then) test method
				// String testName = runner.testName(m); 
				String testName = m.getName()+"("+cls.getName()+")"; // cls.getName()+": " + m.getName();
				if (!allTests.containsKey(testName)) {
					allTests.put(testName, Request.method(cls, m.getName()));
				}
			}
		}

		return allTests;
	}
	*/
	
	private static boolean isPublicTestMethod(Method m) {
        return isTestMethod(m) && Modifier.isPublic(m.getModifiers());
    }

    private static boolean isTestMethod(Method m) {
        return m.getParameterTypes().length == 0 &&
                m.getName().startsWith("test") &&
                m.getReturnType().equals(Void.TYPE);
    }
}

/* hcai vim :set ts=4 tw=4 tws=4 */

