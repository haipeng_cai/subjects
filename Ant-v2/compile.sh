#!/bin/sh
if [ $# -lt 1 ];then
	echo "Usage: $0 verDir"
	exit 1
fi

verDir=$1

source ./ant_global.sh

mkdir -p bin/${verDir}

#$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:
MAINCP=".:/usr/lib/jvm/java-1.6.0-openjdk-1.6.0.0.x86_64/lib/tools.jar:/usr/lib/jvm/java-1.6.0-openjdk-1.6.0.0.x86_64/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/InstrReporters/bin:$ROOT/source_allseeds/$verDir"

for i in $subjectloc/lib/*.jar; #$subjectloc/lib/optional/*.jar $subjectloc/lib/xalan/*.jar;
do
	MAINCP=$MAINCP:$i
done
javac -g:source -source 1.4 -cp ${MAINCP} -d bin/$verDir @${verDir}files.lst  #1>err 2>&1

cp $subjectloc/source_allseeds/$verDir/main/org/apache/tools/ant/taskdefs/defaults.properties bin/$verDir/org/apache/tools/ant/taskdefs/
cp $subjectloc/source_allseeds/$verDir/main/org/apache/tools/ant/types/defaults.properties bin/$verDir/org/apache/tools/ant/types/
cp $subjectloc/source_allseeds/$verDir/main/org/apache/tools/ant/defaultManifest.mf bin/$verDir/org/apache/tools/ant/

if [ -d bin/${verDir}/profile ];
then
	rm -rf bin/${verDir}/profile
fi

echo "Compilation all done." 
exit 0

# hcai vim :set ts=4 tw=4 tws=4
