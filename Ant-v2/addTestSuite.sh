#!/bin/bash
cat test_names.txt | awk -F"[(,)]" '{print $2}' | sort | uniq | \
	awk '{printf("suite.addTestSuite(Class.forName(\"%s\"));\n", $1);}'

# hcai vim :set ts=4 tw=4 tws=4

