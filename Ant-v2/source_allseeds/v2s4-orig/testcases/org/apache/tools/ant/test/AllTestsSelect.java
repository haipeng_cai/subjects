/*
 * ====================================================================
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 2001 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in
 * the documentation and/or other materials provided with the
 * distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 * if any, must include the following acknowledgment:
 * "This product includes software developed by the
 * Apache Software Foundation (http://www.apache.org/)."
 * Alternately, this acknowledgment may appear in the software itself,
 * if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "Apache" and "Apache Software Foundation" and
 * "Apache JMeter" must not be used to endorse or promote products
 * derived from this software without prior written permission. For
 * written permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache",
 * "Apache JMeter", nor may "Apache" appear in their name, without
 * prior written permission of the Apache Software Foundation.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 */
 
package org.apache.tools.ant.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Properties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import profile.ExecHistReporter;
import profile.TimeReporter;
import change.DynSliceReporter;
import profile.BranchReporter;
import profile.DUAReporter;

/************************************************************
 *  Provides a quick and easy way to run all <a href="http://junit.sourceforge.net">junit</a> unit tests in your java project.  It will 
 * find all unit test classes and run all their test methods.  There is no need to configure
 * it in any way to find these classes except to give it a path to search.
 * <p>
 * Here is an example Ant target (See Ant at <a href="http://jakarta.apache.org/ant">Apache</a>) that
 * runs all your unit tests:
 * <pre>
 *		&lt;target name="test" depends="compile"&gt;
 *			&lt;java classname="org.apache.jorphan.test.AllTestsSelect" fork="yes"&gt; 
 *				&lt;classpath&gt;
 *					&lt;path refid="YOUR_CLASSPATH"/&gt;
 *					&lt;pathelement location="ROOT_DIR_OF_YOUR_COMPILED_CLASSES"/&gt;
 *				&lt;/classpath&gt;
 *				&lt;arg value="SEARCH_PATH/"/&gt;
 *				&lt;arg value="PROPERTY_FILE"/&gt;
 *				&lt;arg value="NAME_OF_UNITTESTMANAGER_CLASS"/&gt;
 *			&lt;/java&gt;
 *		&lt;/target&gt;
 * </pre>
 * 
 * <DL><dt>YOUR_CLASSPATH
 * 	<DD>Refers to the classpath that includes all jars and libraries need to run your unit tests
 *</dd>
 * </dt><dt>ROOT_DIR_OF_YOUR_COMPILED_CLASSES
 * <dd>The classpath should include the directory where all your project's classes are compiled
 * to, if it doesn't already.  </dd></dt>
 * <dt>SEARCH_PATH
 * <dd>The first argument tells AllTestsSelect where to 
 * look for unit test classes to execute.  In most cases, it is identical to 
 * ROOT_DIR_OF_YOUR_COMPILED_CLASSES.  You can specify multiple directories or jars to 
 * search by providing a comma-delimited list.</dd></dt>
 * <dt>PROPERTY_FILE
 * <dd>A simple property file that sets logging parameters.  It is optional and is only relevant
 * if you use the same logging packages that JOrphan uses.</dd></dt>
 * <dt>NAME_OF_UNITTESTMANAGER_CLASS
 * <dd>If your system requires some configuration to run correctly, you can implement the
 * {@link UnitTestManager} interface and be given an opportunity to initialize your system from a configuration 
 * file.</dd></dt></dl>
 *
 *@author     Michael Stover (mstover1 at apache.org)
 *@see UnitTestManager
 ***********************************************************/
import java.util.Comparator;

// *** ported from XML-sec for Ant test separation by hcai - 09/2013
public class AllTestsSelect
{
	/** Generic comparator for objects based on the result of toString. Null is the "minimum". */
	/*
	public static class StringBasedComparator implements Comparator {
		private StringBasedComparator() {}
		
		public int compare(Object o1, Object o2) {
			if (o1 == null)
				return (o2 == null)? 0 : -1; // o1 is equal to or less than o2
			if (o2 == null)
				return 1; // o1 is greater than o2
			return o1.toString().compareTo(o2.toString());
		}
	}
	*/
	// ***
	//
	// added by hcai for Sensa Instrumentation
	static void __link() { 
		BranchReporter.__link(); 
		DUAReporter.__link(); 
		DynSliceReporter.__link();
		ExecHistReporter.__link(); 
		//EAS.Monitor.__link();
		//Diver.EAMonitor.__link(); 
		//mut.Modify.__link();
	}

	/************************************************************
	 *  Constructor for the AllTestsSelect object
	 ***********************************************************/
	public AllTestsSelect()
	{
	}

	/************************************************************
	 *  Starts a run through all unit tests found in the specified classpaths.
	 * The first argument should be a list of paths to search.
	 * The second argument is optional and specifies a properties file used to
	 * initialize logging.
	 * The third argument is also optional, and specifies a class that implements the 
	 * UnitTestManager interface.  This provides a means of initializing your application
	 * with a configuration file prior to the start of any unit tests.
	 *
	 *@param  args  The command line arguments
	 ***********************************************************/
	/* commented by hcai - 12/12
	public static void main(String[] args)
	{
		if(args.length < 1)
		{
			System.out.println("You must specify a comma-delimited list of paths to search for unit tests");
			System.exit(0);
		}
		initializeLogging(args);
		initializeManager(args);
		// end : added - 11 July 2001
		try
		{
			TestSuite suite = suite(args[0]);
			PrintStream out = new PrintStream(new FileOutputStream("testresults.txt"));
			junit.textui.TestRunner runner = new junit.textui.TestRunner(out);
			runner.run(suite);
			out.close();
		}
		catch (FileNotFoundException e)
		{
			log.error("",e);
		}
		System.exit(0);
	}
	*/

	private static int dummy = 0;

	public static void main(String[] args)
	{
		/*
		TestSuite tsSorted = sortTestSuite((TestSuite) suite(args[0]));
		junit.textui.TestRunner.run(tsSorted);
		*/
		TestSuite tsSorted = sortTestSuite((TestSuite) suite());
		TestSuite tsSelect = new TestSuite();
		for (Enumeration e = tsSorted.tests(); e.hasMoreElements(); ) {
			//System.out.println(e.nextElement()); // DEBUG
			TestCase t = (TestCase) e.nextElement();
			if (args.length == 0 || args[0].equals(t.toString())) {
				tsSelect.addTest(t);
				//System.out.println(t);
				/** hcai: in the EchoTest cases, EH ouptuts can interfere the test logic */
				if (t.getName().contains("EchoTest")) {
					ExecHistReporter.outStream = System.err;
				}
			}
		}
		if (tsSelect.testCount() == 0)
			throw new RuntimeException("ABORTING: NO TEST CASES TO EXECUTE");
		
		junit.textui.TestRunner.run(tsSelect);
        if (dummy != 0) {
		  try {
							
			(new org.apache.tools.ant.IncludeTest("")).test1();
			(new org.apache.tools.ant.taskdefs.AntStructureTest("")).test1();
			(new org.apache.tools.ant.taskdefs.AntTest("")).test1();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test1();
			(new org.apache.tools.ant.taskdefs.CopyTest("")).test1();
			(new org.apache.tools.ant.taskdefs.CopydirTest("")).test1();
			(new org.apache.tools.ant.taskdefs.CopyfileTest("")).test1();
			(new org.apache.tools.ant.taskdefs.DeleteTest("")).test1();
			(new org.apache.tools.ant.taskdefs.DeltreeTest("")).test1();
			(new org.apache.tools.ant.taskdefs.DependSetTest("")).test1();
			(new org.apache.tools.ant.taskdefs.EchoTest("")).test1();
			(new org.apache.tools.ant.taskdefs.FailTest("")).test1();
			(new org.apache.tools.ant.taskdefs.FilterTest("")).test1();
			(new org.apache.tools.ant.taskdefs.FixCrLfTest("")).test1();
			(new org.apache.tools.ant.taskdefs.GUnzipTest("")).test1();
			(new org.apache.tools.ant.taskdefs.GetTest("")).test1();
			(new org.apache.tools.ant.taskdefs.GzipTest("")).test1();
			(new org.apache.tools.ant.taskdefs.JarTest("")).test1();
			(new org.apache.tools.ant.taskdefs.MkdirTest("")).test1();
			(new org.apache.tools.ant.taskdefs.PropertyTest("")).test1();
			(new org.apache.tools.ant.taskdefs.RenameTest("")).test1();
			(new org.apache.tools.ant.taskdefs.ReplaceTest("")).test1();
			(new org.apache.tools.ant.taskdefs.SleepTest("")).test1();
			(new org.apache.tools.ant.taskdefs.TarTest("")).test1();
			(new org.apache.tools.ant.taskdefs.TaskdefTest("")).test1();
			(new org.apache.tools.ant.taskdefs.UnzipTest("")).test1();
			(new org.apache.tools.ant.taskdefs.ZipTest("")).test1();
			(new org.apache.tools.ant.types.DescriptionTest("")).test1();
			(new org.apache.tools.ant.types.FilterSetTest("")).test1();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test10();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test11();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test12();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test13();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test13b();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test15();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test16();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test17();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test18();
			(new org.apache.tools.ant.IncludeTest("")).test2();
			(new org.apache.tools.ant.taskdefs.AntTest("")).test2();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test2();
			(new org.apache.tools.ant.taskdefs.CopyTest("")).test2();
			(new org.apache.tools.ant.taskdefs.CopydirTest("")).test2();
			(new org.apache.tools.ant.taskdefs.CopyfileTest("")).test2();
			(new org.apache.tools.ant.taskdefs.DeleteTest("")).test2();
			(new org.apache.tools.ant.taskdefs.DeltreeTest("")).test2();
			(new org.apache.tools.ant.taskdefs.DependSetTest("")).test2();
			(new org.apache.tools.ant.taskdefs.EchoTest("")).test2();
			(new org.apache.tools.ant.taskdefs.FailTest("")).test2();
			(new org.apache.tools.ant.taskdefs.FilterTest("")).test2();
			(new org.apache.tools.ant.taskdefs.FixCrLfTest("")).test2();
			(new org.apache.tools.ant.taskdefs.GUnzipTest("")).test2();
			(new org.apache.tools.ant.taskdefs.GetTest("")).test2();
			(new org.apache.tools.ant.taskdefs.GzipTest("")).test2();
			(new org.apache.tools.ant.taskdefs.JarTest("")).test2();
			(new org.apache.tools.ant.taskdefs.MkdirTest("")).test2();
			(new org.apache.tools.ant.taskdefs.PropertyTest("")).test2();
			(new org.apache.tools.ant.taskdefs.RenameTest("")).test2();
			(new org.apache.tools.ant.taskdefs.ReplaceTest("")).test2();
			(new org.apache.tools.ant.taskdefs.SleepTest("")).test2();
			(new org.apache.tools.ant.taskdefs.TarTest("")).test2();
			(new org.apache.tools.ant.taskdefs.TaskdefTest("")).test2();
			(new org.apache.tools.ant.taskdefs.UnzipTest("")).test2();
			(new org.apache.tools.ant.taskdefs.ZipTest("")).test2();
			(new org.apache.tools.ant.types.DescriptionTest("")).test2();
			(new org.apache.tools.ant.types.FilterSetTest("")).test2();
			(new org.apache.tools.ant.IncludeTest("")).test3();
			(new org.apache.tools.ant.taskdefs.AntTest("")).test3();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test3();
			(new org.apache.tools.ant.taskdefs.CopydirTest("")).test3();
			(new org.apache.tools.ant.taskdefs.CopyfileTest("")).test3();
			(new org.apache.tools.ant.taskdefs.DependSetTest("")).test3();
			(new org.apache.tools.ant.taskdefs.EchoTest("")).test3();
			(new org.apache.tools.ant.taskdefs.FilterTest("")).test3();
			(new org.apache.tools.ant.taskdefs.FixCrLfTest("")).test3();
			(new org.apache.tools.ant.taskdefs.GUnzipTest("")).test3();
			(new org.apache.tools.ant.taskdefs.GetTest("")).test3();
			(new org.apache.tools.ant.taskdefs.GzipTest("")).test3();
			(new org.apache.tools.ant.taskdefs.JarTest("")).test3();
			(new org.apache.tools.ant.taskdefs.MkdirTest("")).test3();
			(new org.apache.tools.ant.taskdefs.PropertyTest("")).test3();
			(new org.apache.tools.ant.taskdefs.RenameTest("")).test3();
			(new org.apache.tools.ant.taskdefs.ReplaceTest("")).test3();
			(new org.apache.tools.ant.taskdefs.SleepTest("")).test3();
			(new org.apache.tools.ant.taskdefs.TarTest("")).test3();
			(new org.apache.tools.ant.taskdefs.TaskdefTest("")).test3();
			(new org.apache.tools.ant.taskdefs.UnzipTest("")).test3();
			(new org.apache.tools.ant.taskdefs.ZipTest("")).test3();
			(new org.apache.tools.ant.types.DescriptionTest("")).test3();
			(new org.apache.tools.ant.types.FilterSetTest("")).test3();
			(new org.apache.tools.ant.IncludeTest("")).test4();
			(new org.apache.tools.ant.taskdefs.AntTest("")).test4();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test4();
			(new org.apache.tools.ant.taskdefs.CopydirTest("")).test4();
			(new org.apache.tools.ant.taskdefs.CopyfileTest("")).test4();
			(new org.apache.tools.ant.taskdefs.DeleteTest("")).test4();
			(new org.apache.tools.ant.taskdefs.DependSetTest("")).test4();
			(new org.apache.tools.ant.taskdefs.FilterTest("")).test4();
			(new org.apache.tools.ant.taskdefs.FixCrLfTest("")).test4();
			(new org.apache.tools.ant.taskdefs.GetTest("")).test4();
			(new org.apache.tools.ant.taskdefs.GzipTest("")).test4();
			(new org.apache.tools.ant.taskdefs.JarTest("")).test4();
			(new org.apache.tools.ant.taskdefs.PropertyTest("")).test4();
			(new org.apache.tools.ant.taskdefs.ReplaceTest("")).test4();
			(new org.apache.tools.ant.taskdefs.SleepTest("")).test4();
			(new org.apache.tools.ant.taskdefs.TarTest("")).test4();
			(new org.apache.tools.ant.taskdefs.TaskdefTest("")).test4();
			(new org.apache.tools.ant.taskdefs.ZipTest("")).test4();
			(new org.apache.tools.ant.types.DescriptionTest("")).test4();
			(new org.apache.tools.ant.IncludeTest("")).test5();
			(new org.apache.tools.ant.taskdefs.AntTest("")).test5();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test5();
			(new org.apache.tools.ant.taskdefs.CopydirTest("")).test5();
			(new org.apache.tools.ant.taskdefs.CopyfileTest("")).test5();
			(new org.apache.tools.ant.taskdefs.DependSetTest("")).test5();
			(new org.apache.tools.ant.taskdefs.FilterTest("")).test5();
			(new org.apache.tools.ant.taskdefs.FixCrLfTest("")).test5();
			(new org.apache.tools.ant.taskdefs.GetTest("")).test5();
			(new org.apache.tools.ant.taskdefs.GzipTest("")).test5();
			(new org.apache.tools.ant.taskdefs.ReplaceTest("")).test5();
			(new org.apache.tools.ant.taskdefs.SleepTest("")).test5();
			(new org.apache.tools.ant.taskdefs.TarTest("")).test5();
			(new org.apache.tools.ant.taskdefs.TaskdefTest("")).test5();
			(new org.apache.tools.ant.taskdefs.ZipTest("")).test5();
			(new org.apache.tools.ant.taskdefs.AntTest("")).test6();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test6();
			(new org.apache.tools.ant.taskdefs.CopydirTest("")).test6();
			(new org.apache.tools.ant.taskdefs.CopyfileTest("")).test6();
			(new org.apache.tools.ant.taskdefs.FilterTest("")).test6();
			(new org.apache.tools.ant.taskdefs.FixCrLfTest("")).test6();
			(new org.apache.tools.ant.taskdefs.GetTest("")).test6();
			(new org.apache.tools.ant.taskdefs.RenameTest("")).test6();
			(new org.apache.tools.ant.taskdefs.ReplaceTest("")).test6();
			(new org.apache.tools.ant.taskdefs.SleepTest("")).test6();
			(new org.apache.tools.ant.taskdefs.ZipTest("")).test6();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test7();
			(new org.apache.tools.ant.taskdefs.FilterTest("")).test7();
			(new org.apache.tools.ant.taskdefs.FixCrLfTest("")).test7();
			(new org.apache.tools.ant.taskdefs.ReplaceTest("")).test7();
			(new org.apache.tools.ant.taskdefs.ZipTest("")).test7();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test8();
			(new org.apache.tools.ant.taskdefs.FilterTest("")).test8();
			(new org.apache.tools.ant.taskdefs.FixCrLfTest("")).test8();
			(new org.apache.tools.ant.taskdefs.ReplaceTest("")).test8();
			(new org.apache.tools.ant.taskdefs.AvailableTest("")).test9();
			(new org.apache.tools.ant.taskdefs.FixCrLfTest("")).test9();
			(new org.apache.tools.ant.IntrospectionHelperTest("")).testAddText();
			(new org.apache.tools.ant.types.PathTest("")).testAppending();
			(new org.apache.tools.ant.IntrospectionHelperTest("")).testAttributeSetters();
			(new org.apache.tools.ant.DirectoryScannerTest("")).testChildrenOfExcludedDirectory();
			(new org.apache.tools.ant.types.FileListTest("")).testCircularReferenceCheck();
			(new org.apache.tools.ant.types.FileSetTest("")).testCircularReferenceCheck();
			(new org.apache.tools.ant.types.MapperTest("")).testCircularReferenceCheck();
			(new org.apache.tools.ant.types.PathTest("")).testCircularReferenceCheck();
			(new org.apache.tools.ant.types.PatternSetTest("")).testCircularReferenceCheck();
			(new org.apache.tools.ant.taskdefs.JavaTest("")).testClassname();
			(new org.apache.tools.ant.taskdefs.JavaTest("")).testClassnameAndJar();
			(new org.apache.tools.ant.types.PathTest("")).testConstructor();
			(new org.apache.tools.ant.types.EnumeratedAttributeTest("")).testContains();
			(new org.apache.tools.zip.AsiExtraFieldTest("")).testContent();
			(new org.apache.tools.ant.types.MapperTest("")).testCopyTaskWithTwoFilesets();
			(new org.apache.tools.ant.ProjectTest("")).testDataTypes();
			(new org.apache.tools.ant.IntrospectionHelperTest("")).testElementCreators();
			(new org.apache.tools.ant.types.FileListTest("")).testEmptyElementIfIsReference();
			(new org.apache.tools.ant.types.FileSetTest("")).testEmptyElementIfIsReference();
			(new org.apache.tools.ant.types.MapperTest("")).testEmptyElementIfIsReference();
			(new org.apache.tools.ant.types.PathTest("")).testEmptyElementIfIsReference();
			(new org.apache.tools.ant.types.PatternSetTest("")).testEmptyElementIfIsReference();
			(new org.apache.tools.ant.types.PathTest("")).testEmpyPath();
			(new org.apache.tools.ant.util.DOMElementWriterTest("")).testEncode();
			(new org.apache.tools.ant.types.EnumeratedAttributeTest("")).testExceptions();
			(new org.apache.tools.ant.taskdefs.ExecuteWatchdogTest("")).testFailed();
			(new org.apache.tools.ant.IntrospectionHelperTest("")).testGetAttributes();
			(new org.apache.tools.ant.types.CommandlineJavaTest("")).testGetCommandline();
			(new org.apache.tools.ant.IntrospectionHelperTest("")).testGetNestedElements();
			(new org.apache.tools.ant.util.DOMElementWriterTest("")).testIsReference();
			(new org.apache.tools.ant.taskdefs.JavaTest("")).testJarAndClassName();
			(new org.apache.tools.ant.taskdefs.JavaTest("")).testJarNoFork();
			(new org.apache.tools.ant.taskdefs.ExecuteWatchdogTest("")).testManualStop();
			(new org.apache.tools.zip.ExtraFieldUtilsTest("")).testMerge();
			(new org.apache.tools.zip.AsiExtraFieldTest("")).testModes();
			(new org.apache.tools.ant.taskdefs.CVSPassTest("")).testNoCVSRoot();
			(new org.apache.tools.ant.taskdefs.JavaTest("")).testNoJarNoClassname();
			(new org.apache.tools.ant.taskdefs.FixCrLfTest("")).testNoOverwrite();
			(new org.apache.tools.ant.taskdefs.CVSPassTest("")).testNoPassword();
			(new org.apache.tools.ant.util.GlobPatternMapperTest("")).testNoPatternAtAll();
			(new org.apache.tools.ant.taskdefs.ExecuteWatchdogTest("")).testNoTimeOut();
			(new org.apache.tools.ant.util.FileUtilsTest("")).testNormalize();
			(new org.apache.tools.zip.ExtraFieldUtilsTest("")).testParse();
			(new org.apache.tools.ant.taskdefs.CVSPassTest("")).testPassFile();
			(new org.apache.tools.ant.taskdefs.CVSPassTest("")).testPassFileDuplicateEntry();
			(new org.apache.tools.ant.taskdefs.CVSPassTest("")).testPassFileMultipleEntry();
			(new org.apache.tools.ant.util.GlobPatternMapperTest("")).testPostfixOnly();
			(new org.apache.tools.ant.util.GlobPatternMapperTest("")).testPreAndPostfix();
			(new org.apache.tools.ant.util.GlobPatternMapperTest("")).testPrefixOnly();
			(new org.apache.tools.zip.AsiExtraFieldTest("")).testReparse();
			(new org.apache.tools.ant.ProjectTest("")).testResolveFile();
			(new org.apache.tools.ant.util.FileUtilsTest("")).testResolveFile();
			(new org.apache.tools.ant.util.FileUtilsTest("")).testSetLastModified();
			(new org.apache.tools.ant.types.PathTest("")).testSetLocation();
			(new org.apache.tools.ant.IntrospectionHelperTest("")).testSupportsCharacters();
			(new org.apache.tools.ant.taskdefs.ExecuteWatchdogTest("")).testTimeOut();
			(new org.apache.tools.ant.types.CommandlineTest("")).testToString();
			(new org.apache.tools.ant.types.CommandlineTest("")).testTokenizer();
			(new org.apache.tools.ant.types.PathTest("")).testUnique();

		  }
		  catch (Throwable t) {
		  }
		}

	}

	/************************************************************
	 *  A unit test suite for JUnit
	 *
	 *@return    The test suite
	 ***********************************************************/
	public static Test suite(String testName) {

		//TestSuite suite = new TestSuite("All org.apache.jorphan.test JUnit Tests");
		TestSuite suite = new TestSuite();

		try
		{
			suite.addTest(new TestSuite(Class.forName(testName)));
		}
		catch (Exception ex)
		{
			System.err.println("error adding test :"+ex);
		}

		return suite;
	}

	// *** ported for Ant-v2 by hcai - 12/2013
	/************************************************************
	 *  A unit test suite for JUnit
	 *
	 *@return    The test suite
	 ***********************************************************/
	public static Test suite() {
		TestSuite suite = new TestSuite("All Ant-V2 JUnit Tests");

		try
		{
			suite.addTestSuite(org.apache.tools.ant.taskdefs.GzipTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.GUnzipTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.AntStructureTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.AntTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.AvailableTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.CopydirTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.CopyfileTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.DeleteTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.DeltreeTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.EchoTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.FailTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.FilterTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.GetTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.MkdirTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.RenameTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.ReplaceTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.TarTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.TaskdefTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.UnzipTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.ZipTest.class);
			suite.addTestSuite(org.apache.tools.ant.types.CommandlineJavaTest.class);
			suite.addTestSuite(org.apache.tools.ant.types.CommandlineTest.class);
			suite.addTestSuite(org.apache.tools.ant.types.EnumeratedAttributeTest.class);
			suite.addTestSuite(org.apache.tools.ant.types.FileSetTest.class);
			suite.addTestSuite(org.apache.tools.ant.types.PathTest.class);
			suite.addTestSuite(org.apache.tools.ant.types.PatternSetTest.class);
			suite.addTestSuite(org.apache.tools.ant.IntrospectionHelperTest.class);
			suite.addTestSuite(org.apache.tools.ant.ProjectTest.class);

			// hcai: new tests for v1?
			suite.addTestSuite(org.apache.tools.ant.taskdefs.ExecuteWatchdogTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.PropertyTest.class);
			suite.addTestSuite(org.apache.tools.ant.types.MapperTest.class);
			suite.addTestSuite(org.apache.tools.ant.util.DOMElementWriterTest.class);
			suite.addTestSuite(org.apache.tools.ant.util.GlobPatternMapperTest.class);
			suite.addTestSuite(org.apache.tools.ant.IncludeTest.class);
			// new test for v2
			// suite.addTestSuite(org.apache.tools.ant.taskdefs.optional.junit.JUnitTestRunnerTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.CopyTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.CVSPassTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.DependSetTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.FixCrLfTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.JarTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.JavaTest.class);
			suite.addTestSuite(org.apache.tools.ant.taskdefs.SleepTest.class);
			suite.addTestSuite(org.apache.tools.ant.types.DescriptionTest.class);
			suite.addTestSuite(org.apache.tools.ant.types.FileListTest.class);
			suite.addTestSuite(org.apache.tools.ant.types.FilterSetTest.class);
			suite.addTestSuite(org.apache.tools.ant.util.FileUtilsTest.class);
			suite.addTestSuite(org.apache.tools.ant.DirectoryScannerTest.class);
			suite.addTestSuite(org.apache.tools.zip.AsiExtraFieldTest.class);
			suite.addTestSuite(org.apache.tools.zip.ExtraFieldUtilsTest.class);
		}
		catch (Exception ex)
		{
			System.err.println("error adding test :"+ex);
		}

		return suite;
	}

	/************************************************************
	 *  A unit test suite for JUnit
	 *
	 *@return    The test suite
	 ***********************************************************/
	/* commented by hcai - 12/12
	private static TestSuite suite(String searchPaths)
	{
		TestSuite suite = new TestSuite();
		try
		{
			Iterator classes = ClassFinder.findClassesThatExtend(
			    		JOrphanUtils.split(searchPaths, ","),
					new Class[]{TestCase.class},true).iterator();
			while (classes.hasNext())
			{
				String name = (String)classes.next();
				try
				{
					suite.addTest(new TestSuite(Class.forName(name)));
				}
				catch (Exception ex)
				{
					log.error("error adding test :"+ex);
				}
			}
		}
		catch (IOException e)
		{
			log.error("",e);
		}
		catch (ClassNotFoundException e)
		{
			log.error("",e);
		}
		return suite;
	}
	*/

	// *** ported from Ant by hcai - 09/2013
	/*
	private static TestSuite sortTestSuite(TestSuite ts) {
		List tests = new ArrayList();
		getTestsRecursive(ts, tests);

		Collections.sort(tests, new StringBasedComparator());

		TestSuite tsSorted = new TestSuite();
		for (Iterator it = tests.iterator(); it.hasNext();)
			tsSorted.addTest((TestCase) it.next());

		return tsSorted;
	}
	*/
	// hcai: use standalone StringBasedComparator to avoid non-determinism - 12/2013
	private static TestSuite sortTestSuite(TestSuite ts) {
		List tests = new ArrayList();
		getTestsRecursive(ts, tests);

		Collections.sort(tests, StringBasedComparator.inst);

		TestSuite tsSorted = new TestSuite();
		for (Iterator it = tests.iterator(); it.hasNext();)
			tsSorted.addTest((TestCase) it.next());

		return tsSorted;
	}

	// *** ported from Ant by hcai - 09/2013
	private static void getTestsRecursive(Test t, List tests) {
		if (t instanceof TestCase)
			tests.add(t);
		else
			for (Enumeration e = ((TestSuite) t).tests(); e.hasMoreElements();)
				getTestsRecursive((Test) e.nextElement(), tests);
	}
}


