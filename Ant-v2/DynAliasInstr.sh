#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./ant_global.sh

MAINCP=".:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar"

mkdir -p out-DynAliasInstr

#/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/lib/tools.jar
#SOOTCP=.:$ROOT/tools/j2sdk1.4.2_18/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$subjectloc/bin/${ver}${seed}:$ROOT/workspace/mcia/bin

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics/bin:$subjectloc/bin/${ver}${seed}:$ROOT/workspace/mcia/bin

for i in $subjectloc/lib/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/DynAliasInstrumented-$ver$seed
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-allowphantom \
	#-debug \
	#-dumpJimple \
	#-wrapTryCatch \
	#-dumpFunctionList \
	#-statUncaught \
	#-intraCD \
	#-interCD \
	#-exInterCD \
	#-ignoreRTECD \
   	#-duaverbose \
	#-serializeVTG \
java -Xmx1600m -ea -cp ${MAINCP} Diver.DynAliasInst \
	-w -cp ${SOOTCP} \
	-p cg verbose:true,implicit-entry:false -p cg.spark verbose:true,on-fly-cg:true,rta:true \
	-f c -d "$OUTDIR" -brinstr:off -duainstr:off \
	-allowphantom \
	-dumpJimple \
	-cachingOIDs \
	-slicectxinsens \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	1>out-DynAliasInstr/instr-${ver}${seed}.out 2>out-DynAliasInstr/instr-${ver}${seed}.err
stoptime=`date +%s%N | cut -b1-13`

echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Instrumentation done, now copying resources required for running."
cp -fr $subjectloc/source_allseeds/${ver}${seed}/main/org/apache/tools/ant/taskdefs/defaults.properties $OUTDIR/org/apache/tools/ant/taskdefs/
cp -fr $subjectloc/source_allseeds/${ver}${seed}/main/org/apache/tools/ant/types/defaults.properties $OUTDIR/org/apache/tools/ant/types/


echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

