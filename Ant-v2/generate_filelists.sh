#!/bin/bash

source ./ant_global.sh
BASEDIR=source_allseeds

for N in ${SEEDS}
do
	#find $BASEDIR/v2s$N -name "*.java" 1>v2s${N}files.lst
	#find $BASEDIR/v2s$N-orig -name "*.java" 1>v2s${N}-origfiles.lst

	> ${VERSION}s${N}files.lst
	> ${VERSION}s${N}-origfiles.lst
	#for subdir in jorphan core components functions protocol/ftp protocol/http protocol/jdbc protocol/java;
	for subdir in main testcases;
	do
		find $BASEDIR/${VERSION}s$N-orig/$subdir -name "*.java" | \
			grep -v -E "FTP.java|NetRexxC.java|AntStarTeamCheckOut.java|XalanLiaison.java|XslpLiaison.java|XalanLiaisonTest.java|XslpLiaisonTest.java" | \
			grep -v -E "Script.java|EjbcHelper.java|DDCreatorHelper.java|Javah.java" | grep -v -E "optional\/ide" 1>>${VERSION}s${N}-origfiles.lst
			#| grep -v -E "optional"
			#
		find $BASEDIR/${VERSION}s$N/$subdir -name "*.java" | \
			grep -v -E "FTP.java|NetRexxC.java|AntStarTeamCheckOut.java|XalanLiaison.java|XslpLiaison.java|XalanLiaisonTest.java|XslpLiaisonTest.java" | \
			grep -v -E "Script.java|EjbcHelper.java|DDCreatorHelper.java|Javah.java" | grep -v -E "optional\/ide" 1>>${VERSION}s${N}files.lst
	done
done

exit 0

# hcai vim :set ts=4 tw=4 tws=4
