#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [number of traces]"
	exit 1
fi

ver=$1
seed=$2
NT=${3:-"112"}
#query=${4:-"execute"}
query=${4:-"<org.apache.tools.ant.taskdefs.Copy: void execute()>"}

source ./ant_global.sh

INDIR=$subjectloc/Diveroutdyn-${ver}${seed}
BINDIR=$subjectloc/DiverInstrumented-$ver$seed

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$INDIR"

starttime=`date +%s%N | cut -b1-13`
	#"main" \
	#-debug
	#-stmtcov
	#-dynalias \
	#-postprune \
	#-debug
java -Xmx2800m -ea -cp ${MAINCP} Diver.DiverAnalysis \
	"$query" \
	"$INDIR" \
	"$BINDIR" \
	$NT

stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

