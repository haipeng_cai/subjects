#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed"
	exit 1
fi

ver=$1
seed=$2

source ./ant_global.sh

INDIR=$subjectloc/DynAliasInstrumented-$ver$seed

#$ROOT/tools/j2re1.4.2_18/lib/rt.jar
MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$INDIR:$ROOT/workspace/mcia/bin"

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

OUTDIR=$subjectloc/DynAliasoutdyn-${ver}${seed}
mkdir -p $OUTDIR

function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	cat $subjectloc/inputs/testinputs.txt | dos2unix | \
	while read testname;
	do
		let i=i+1

		echo "Run Test #$i" # [" $testname "] ....."
		java -Xmx4000m -ea -cp $MAINCP  $DRIVERCLASS $testname 1> $OUTDIR/test$i.out 2> $OUTDIR/test$i.err
		if [ -s $OUTDIR/$i.err ];then
			echo "$testname failed"
		else
			echo "$testname passed"
		fi
	done
}

function RunbyRunner()
{
		#"-fullseq" 
	java -Xmx2800m -ea -cp ${MAINCP} Diver.DiverRun \
		$DRIVERCLASS \
		"$subjectloc" \
		"$INDIR" \
		${ver}${seed} \
		$OUTDIR \
		-cachingOIDs \
		-ninstanceLevel
}

starttime=`date +%s%N | cut -b1-13`
#RunOneByOne
RunbyRunner
stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Running finished."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

