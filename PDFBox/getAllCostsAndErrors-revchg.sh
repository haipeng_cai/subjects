#!/bash/bin
sensaData=${1:-`pwd`/Results-revchg/}
#sensaData=${1:-`pwd`/ResultWBFS/}
wslicingData=${2:-`pwd`/ResultWslicing-revchg/}
dynwslicingData=${3:-`pwd`/DynWslicingResults-revchg/}

python ./tabulateCostAndError-revchg.py \
	$sensaData/IdealCostEffectiveness \
	$wslicingData/costEffectiveness \
	$dynwslicingData/costEffectiveness \
	$sensaData/costEffectiveness \
	$sensaData/IdealErrorMetrics \
	$wslicingData/errorMetrics \
	$dynwslicingData/errorMetrics \
	$sensaData/errorMetrics 

# hcai vim :set ts=4 tw=4 tws=4

