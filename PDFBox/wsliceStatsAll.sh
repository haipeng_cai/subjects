#!/bin/bash
source ../pdfbox_global.sh
i=0

for N in ${SEEDS};
do
	echo -n "Now post-processing ${VERSION} s$N at ${C[$i]} ... "
	sh ./wsliceStats.sh ${C[$i]} ${VERSION} s$N 2>werr.${VERSION}s$N-${C[$i]} 1>wout.${VERSION}s$N-${C[$i]}

	echo " done"
	let i=i+1
done

echo "Ranking phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

