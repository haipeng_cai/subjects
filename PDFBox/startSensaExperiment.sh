#!/bin/bash

# 1. instrumentation / static analysis
function sensa_phase1()
{
	stime=`date +%s%N | cut -b1-13`
	sh ./SensaInstrAll.sh 1> timing.SensaInst 2>&1
	etime=`date +%s%N | cut -b1-13`
	echo "ALL STATIC ANALYSIS TIME: " `expr $etime - $stime` milliseconds
}

# 2. Run phase
function sensa_phase2()
{
	stime=`date +%s%N | cut -b1-13`
	sh ./SensaRunAll.sh 1>> timing.SensaRun 2>&1
	etime=`date +%s%N | cut -b1-13`
	echo "ALL RUN TIME: " `expr $etime - $stime` milliseconds
}

# 3. Ranking phase
function sensa_phase3()
{
	stime=`date +%s%N | cut -b1-13`
	sh ./SensaRankAll.sh 1>>timing.SensaRank 2>&1
	etime=`date +%s%N | cut -b1-13`
	echo "ALL RANKING TIME: " `expr $etime - $stime` milliseconds
}

# 1. instrumentation / static analysis
function eh_phase1()
{
	stime=`date +%s%N | cut -b1-13`
	sh ./execHistInstrAll.sh 1> timing.ehInst 2>&1
	etime=`date +%s%N | cut -b1-13`
	echo "ALL STATIC ANALYSIS TIME: " `expr $etime - $stime` milliseconds
}

# 2. Run phase
function eh_phase2()
{
	stime=`date +%s%N | cut -b1-13`
	sh ./execHistRunAll.sh 1> timing.ehRun 2>&1
	etime=`date +%s%N | cut -b1-13`
	echo "ALL RUN TIME: " `expr $etime - $stime` milliseconds
}

# 3. Ranking phase
function eh_phase3()
{
	stime=`date +%s%N | cut -b1-13`
	sh ./execHistRankAll.sh 1> timing.ehRank 2>&1
	etime=`date +%s%N | cut -b1-13`
	echo "ALL RANKING TIME: " `expr $etime - $stime` milliseconds
}

#sensa_phase1
sensa_phase2
sensa_phase3

wait 

#eh_phase1
#eh_phase2
#eh_phase3

exit 0

# hcai vim :set ts=4 tw=4 tws=4

