#!/bin/bash

#for gsz in 2 3 4 5 6 7 8 9 10;
for gsz in 3 4;
do
	python ./MMC_calculateEASAccuracy.py $gsz v0 s1-orig 1>result_pdfbox_EASAccuracy_s$gsz 2>log_pdfbox_EASAccuracy_s$gsz
done

# hcai vim :set ts=4 tw=4 tws=4

