#!/bin/bash
source ../pdfbox_global.sh

RESDIR=`pwd`/errorMetrics
mkdir -p $RESDIR

i=0
for N in ${SEEDS};
do
	echo -n "Now calculating errors for ${VERSION} s$N at ${C[$i]} ... "
	sh ./dynwslice_getErrors.sh ${C[$i]} ${VERSION} s$N > $RESDIR/Error-${VERSION}s$N-${C[$i]}

	let i=i+1
	echo " done."
done

echo "Ranking phases now got ALL done."

exit 0


# hcai vim :set ts=4 tw=4 tws=4

