#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

source ./pdfbox_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/Sensa/bin:$ROOT/workspace/ProbSli/bin"

mkdir -p out-execHistInstr

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$subjectloc/libs/xercesImpl-fixed:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Deam/bin:$ROOT/workspace/Sensa/bin:$subjectloc/bin/${ver}${seed}:$ROOT/workspace/ProbSli

for i in $subjectloc/libs/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/execHistInstrumented-$ver$seed-$change
mkdir -p $OUTDIR

   	#-duaverbose \
java -Xmx6000m -ea -cp ${MAINCP} sli.ProbSli \
	-w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
	-allowphantom \
	-nopslice \
	-slicectxinsens \
	-start:$change \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	1>out-execHistInstr/execHistInstr-$change-${ver}${seed}.out 2>out-execHistInstr/execHistInstr-$change-${ver}${seed}.err

echo "Copying data to prepare for running the instrumented subject..."
cp -fr source_allseeds/${ver}${seed}/pdfbox/src/test/resources/org/apache/pdfbox/pdmodel/test_pagelabels.pdf $OUTDIR/org/apache/pdfbox/pdmodel/
cp -fr source_allseeds/${ver}${seed}/pdfbox/src/test/resources/org/apache/pdfbox/encryption/* $OUTDIR/org/apache/pdfbox/encryption/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

