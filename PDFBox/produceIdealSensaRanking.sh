#!/bin/bash
source ../pdfbox_global.sh
i=0
for N in ${SEEDS};
do
	echo -n "Now processing ${VERSION} s$N at ${C[$i]} ... "
	cat actualImpactRanking-${VERSION}s$N-${C[$i]} > IdealSensaRanking-${VERSION}s$N-${C[$i]}
	sh ./addStaticSlices.sh actualImpactRanking-${VERSION}s$N-${C[$i]} \
		../SensaInstrumented-${VERSION}s$N-${C[$i]}/fwdslice.out \
		1>>IdealSensaRanking-${VERSION}s$N-${C[$i]}

	let i=i+1
	echo " done."
done

echo "got ALL done."

exit 0

# hcai vim :set ts=4 tw=4 tws=4
