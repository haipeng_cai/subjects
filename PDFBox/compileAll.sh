#!/bin/bash

source ./pdfbox_global.sh

for N in ${SEEDS};
do
	echo "compiling ${VERSION}s$N-orig......"
	sh compile.sh  ${VERSION}s$N-orig 

	echo "compiling ${VERSION}s$N......"
	sh compile.sh  ${VERSION}s$N
done

echo "All finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

