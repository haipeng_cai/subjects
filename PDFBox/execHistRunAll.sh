#!/bin/bash
source ./pdfbox_global.sh
i=0
for N in ${SEEDS};
do
	echo "Now execHistRunning s$N at ${C[$i]} ..."
	sh execHistRun.sh ${C[$i]} ${VERSION} s$N 

	echo "Now execHistRunning s$N-orig at ${C[$i]} ..."
	sh execHistRun.sh ${C[$i]} ${VERSION} s$N-orig

	let i=i+1
done

exit 0


# hcai vim :set ts=4 tw=4 tws=4

