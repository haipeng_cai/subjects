#!/bin/bash

if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3

source ./pdfbox_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/InstrReporters/bin/:$ROOT/workspace/DynWSlicing/bin:$ROOT/workspace/ProbSli/bin"

SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin/:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$subjectloc/bin/$ver$seed:$ROOT/workspace/Sensa/bin:$ROOT/workspace/DynWSlicing/bin"

for i in $subjectloc/libs/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

suffix=${ver}${seed}-$change

LOGDIR=out-DynwsliInstr
mkdir -p $LOGDIR
logout=$LOGDIR/instr-$suffix.out
logerr=$LOGDIR/instr-$suffix.err

OUTDIR=$subjectloc/DynwsliInstrumented-$ver$seed-$change
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`

	#-debug \
	#-allowphantom \
	#-eventLimit 2000 \
   	#-duaverbose \
	#-reportvarsdynslice \
java -Xmx8000m -ea -cp ${MAINCP} sli.DynWSlicing \
	-w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-allowphantom \
	-brinstr:off -duainstr:off \
   	-start:$change \
	-fdynslice \
	-slicectxinsens \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/${ver}${seed} \
	1> $logout 2> $logerr

stoptime=`date +%s%N | cut -b1-13`
echo "Dynamic W-Slicing Instrumentation Time for $change-${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Instrumentation done, now copying resources required for running."
cp -fr source_allseeds/${ver}${seed}/pdfbox/src/test/resources/org/apache/pdfbox/pdmodel/test_pagelabels.pdf $OUTDIR/org/apache/pdfbox/pdmodel/
cp -fr source_allseeds/${ver}${seed}/pdfbox/src/test/resources/org/apache/pdfbox/encryption/* $OUTDIR/org/apache/pdfbox/encryption/

exit 0

# hcai vim :set ts=4 tw=4 tws=4

