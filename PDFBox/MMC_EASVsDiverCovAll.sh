#!/bin/bash

mkdir -p ./MMC_Covresults/
for s in 2 3 4 5 6 7 8 9 10;
do
	sh MMC_EASVsDiver.sh \
		DiverResults/DiverStmtCov/pdfbox_EASVsDiverStmtCovLog \
		EAInstrumented-v0s1-orig/functionList.out \
		$s 1000 \
		1>./MMC_Covresults/result_s_$s 2>/dev/null #2>./MMC_Covresults/log_s_$s
done

echo "all finished."
exit 0
