#!/bin/bash
if [ $# -lt 1 ];then
	echo "Usage: $0 revision"
	exit 1
fi
Rev=$1

source ./pdfbox_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar"

LOGDIR=$subjectloc/out-DiverInstr
mkdir -p $LOGDIR
logout=$LOGDIR/instr-$Rev.out
logerr=$LOGDIR/instr-$Rev.err

SOOTCP=.:/etc/alternatives/java_sdk/jre/lib/rt.jar:/etc/alternatives/java_sdk/jre/lib/jce.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Deam/bin:$subjectloc/bin/$Rev

for i in $subjectloc/libs/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

OUTDIR=$subjectloc/DiverInstrumented-$Rev
mkdir -p $OUTDIR

starttime=`date +%s%N | cut -b1-13`
	#-debug \
	#-dumpJimple \
	#-wrapTryCatch \
	#-dumpFunctionList \
	#-reachability \
	#-debug \
   	#-duaverbose \
	#-ignoreRTECD \
java -Xmx14g -ea -cp ${MAINCP} Diver.DiverInst \
	-w -cp $SOOTCP -p cg verbose:false,implicit-entry:false \
	-p cg.spark verbose:false,on-fly-cg:true,rta:true -f c \
	-d $OUTDIR \
	-brinstr:off -duainstr:off \
	-wrapTryCatch \
	-intraCD \
	-interCD \
	-exInterCD \
	-ignoreRTECD \
	-serializeVTG \
	-slicectxinsens \
	-allowphantom \
	-main-class $DRIVERCLASS \
	-entry:$DRIVERCLASS \
	-process-dir $subjectloc/bin/$Rev \
	1> $logout 2> $logerr

stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for r$Rev elapsed: " `expr $stoptime - $starttime` milliseconds

echo "Copying data to prepare for running the instrumented subject..."
cp -fr $subjectloc/source_allseeds/$Rev/pdfbox/src/test/resources/org/apache/pdfbox/pdmodel/test_pagelabels.pdf $OUTDIR/org/apache/pdfbox/pdmodel/
cp -fr $subjectloc/source_allseeds/$Rev/pdfbox/src/test/resources/org/apache/pdfbox/encryption/* $OUTDIR/org/apache/pdfbox/encryption/

echo "Running finished."
exit 0


# hcai vim :set ts=4 tw=4 tws=4

