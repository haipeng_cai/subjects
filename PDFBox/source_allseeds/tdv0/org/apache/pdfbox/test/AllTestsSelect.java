/**
 *  This is a test driver that collects all test cases from the given list of test classes 
 *  Without inputing any argument, this driver will run all test cases garnered as in a single holistic test suite, 
 *  otherwise, it will run only the single test case matching the given name provided as the input argument;
 *
 *@author     $Author: hcai $
 *@created    $Date: 2013/12/06 $
 *@version    $Revision: 1.0.0.0 $
 */
package org.apache.pdfbox.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Properties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Comparator;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


import profile.ExecHistReporter;
import change.DynSliceReporter;
import profile.BranchReporter;
import profile.DUAReporter;

public class AllTestsSelect
{
	static void __link() { 
		BranchReporter.__link(); DUAReporter.__link(); DynSliceReporter.__link(); ExecHistReporter.__link(); 
		/*
		EAS.Monitor.__link(); 
		mut.Modify.__link();
		Diver.EAMonitor.__link();
		*/
		Sensa.Modify.__link();
	}

	public static void main(String[] args) {
		TestSuite tsSorted = sortTestSuite((TestSuite) suite());
		TestSuite tsSelect = new TestSuite();
		for (Enumeration e = tsSorted.tests(); e.hasMoreElements(); ) {
			TestCase t = (TestCase) e.nextElement();
			// debug
			// System.out.println(t);
			if (args.length == 0 || args[0].equals(t.toString())) 
				tsSelect.addTest(t);
		}
		if (tsSelect.testCount() == 0)
			throw new RuntimeException("ABORTING: NO TEST CASES TO EXECUTE");
		
		junit.textui.TestRunner.run(tsSelect);
	}

    /**
     * This will get the suite of test that this class holds.
     *
     * @return All of the tests that this class holds.
     */
    public static Test suite()
    {
		TestSuite suite = new TestSuite("All org.apache.pdfbox.test JUnit Tests");
		try
		{
			// submodule tests
			suite.addTestSuite(org.apache.jempbox.xmp.XMPSchemaTest.class);
			suite.addTestSuite(org.apache.fontbox.cmap.TestCMap.class);
			suite.addTestSuite(org.apache.fontbox.cff.Type1FontUtilTest.class);
			suite.addTestSuite(org.apache.fontbox.cff.Type1CharStringTest.class);

			// pdfbox tests
			suite.addTest( org.apache.pdfbox.util.TestDateUtil.suite() );
			suite.addTestSuite( org.apache.pdfbox.filter.TestFilters.class );
			suite.addTest( org.apache.pdfbox.pdmodel.TestFDF.suite() );
			suite.addTest( org.apache.pdfbox.pdmodel.interactive.form.TestFields.suite() );
			suite.addTest( org.apache.pdfbox.cos.TestCOSString.suite() );
			suite.addTestSuite( org.apache.pdfbox.pdmodel.TestPDDocumentCatalog.class );

			// pdfbox tests besides those listed in its bound AllTests.java
			suite.addTest( org.apache.pdfbox.util.TestTextStripper.suite() );
			suite.addTest( org.apache.pdfbox.util.TestTextStripperPerformance.suite() );
			suite.addTest( org.apache.pdfbox.util.TestPDFToImage.suite() );
			suite.addTest( org.apache.pdfbox.pdfparser.TestPDFParser.suite() );
			suite.addTestSuite( org.apache.pdfbox.cos.TestCOSInteger.class );
			suite.addTestSuite( org.apache.pdfbox.pdmodel.graphics.color.PDColorStateTest.class );
			suite.addTestSuite( org.apache.pdfbox.encryption.TestPublicKeyEncryption.class );
		}
		catch (Exception ex)
		{
			System.err.println("error adding test :"+ex);
		}
        return suite;
    }

	private static TestSuite sortTestSuite(TestSuite ts) {
		List tests = new ArrayList();
		getTestsRecursive(ts, tests);

		Collections.sort(tests, StringBasedComparator.inst);

		TestSuite tsSorted = new TestSuite();
		for (Iterator it = tests.iterator(); it.hasNext();)
			tsSorted.addTest((TestCase) it.next());

		return tsSorted;
	}

	private static void getTestsRecursive(Test t, List tests) {
		if (t instanceof TestCase)
			tests.add(t);
		else
			for (Enumeration e = ((TestSuite) t).tests(); e.hasMoreElements();)
				getTestsRecursive((Test) e.nextElement(), tests);
	}
}


