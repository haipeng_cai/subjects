#!/bin/bash

source ../pdfbox_global.sh

RESDIR=`pwd`/IdealErrorMetrics
mkdir -p $RESDIR

i=0
for N in ${SEEDS};
do
	echo -n "Now calculating errors for ${VERSION} s$N at ${C[$i]} ... "
	sh ./getIdealErrors.sh ${C[$i]} ${VERSION} s$N $algo > $RESDIR/Error-${VERSION}s$N-${C[$i]}

	let i=i+1
	echo " done."
done

echo "Ranking phases now got ALL done."

exit 0

