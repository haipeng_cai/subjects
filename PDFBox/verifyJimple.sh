#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3
source ./pdfbox_global.sh
MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/tools/java_cup.jar:$ROOT/workspace/InstrReporters/bin/:$ROOT/workspace/DynWSlicing/bin:$ROOT/workspace/ProbSli/bin"

suffix=${ver}${seed}
INDIR=$subjectloc/execHistInstrumented-$suffix-$change

SOOTCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DUAForensics2.5/bin:$ROOT/workspace/ProbSli/bin:$ROOT/workspace/TestAdequacy/bin/:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.5.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/java_cup.jar:$subjectloc/bin/$ver$seed:$ROOT/workspace/Sensa/bin:$ROOT/workspace/DynWSlicing/bin:$INDIR"

for i in $subjectloc/libs/*.jar;
do
	SOOTCP=$SOOTCP:$i
done

	#"org.apache.pdfbox.pdfparser.TestPDFParser" \
java -Xmx1600m -ea -cp ${MAINCP} soot.Main -f J -cp ${SOOTCP} \
	"org.apache.pdfbox.test.AllTestsSelect" \
	-d `pwd`
exit 0


# hcai vim :set ts=4 tw=4 tws=4

