#!/bin/bash

mkdir -p ./MMC_results/
for s in 2 3 4 5 6 7 8 9 10;
do
	sh MMC_EASVsDiver.sh \
		DiverResults/BasicDiver/pdfbox_EASVsDiverLog \
		EAInstrumented-v0s1-orig/functionList.out \
		$s 1000 \
		1>./MMC_results/result_s_$s 2>/dev/null #2>./MMC_results/log_s_$s
done

echo "all finished."
exit 0
