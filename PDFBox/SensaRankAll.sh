#!/bin/bash
source ./pdfbox_global.sh
i=0

#for algo in rand inc observed;
for algo in inc observed;
do
	echo "Now for the $algo strategy ---"
	i=0
	for N in ${SEEDS};
	do
		#echo -n "Now Ranking impact for s$N at ${C[$i]} ... "
		#sh SensaRank.sh ${C[$i]} ${VERSION} s$N $algo 

		echo -n "Now Ranking impact for s$N-orig at ${C[$i]} ... "
		sh SensaRank.sh ${C[$i]} ${VERSION} s$N-orig $algo 

		let i=i+1
	done
done

echo "Ranking phases now got ALL done."

exit 0

# hcai vim :set ts=4 tw=4 tws=4

