#!/bin/bash
if [ $# -lt 3 ];then
	echo "Usage: $0 changeLoc version seed"
	exit 1
fi

change=$1
ver=$2
seed=$3
source ./pdfbox_global.sh

INDIR=$subjectloc/wsliceExecHistInstrumented-$ver$seed-$change

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/Sensa/bin:$ROOT/tools/java_cup.jar:$ROOT/tools/DUAForensics-bins-code/LocalsBox:$ROOT/workspace/InstrReporters/bin:$ROOT/workspace/DynWSlicing/bin:$ROOT/workspace/TestAdequacy/bin:$INDIR"
for i in $subjectloc/libs/*.jar;
do
	MAINCP=$MAINCP:$i
done

#OUTDIR=$subjectloc/execHist_outdyn-${ver}${seed}-$change
OUTDIR=$subjectloc/ResultWslicing/outdyn-${ver}${seed}-$change
mkdir -p $OUTDIR

i=0
cat inputs/testinputs.txt | while read test;
do
	let i=i+1
	echo "Test No. $i: $test "
	java -Xmx10000m -ea -cp ${MAINCP} \
		$DRIVERCLASS $test \
		1> $OUTDIR/$i.out \
		2> $OUTDIR/$i.err
done

exit 0

# hcai vim :set ts=4 tw=4 tws=4
