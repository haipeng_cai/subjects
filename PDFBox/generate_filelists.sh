#!/bin/bash

source ./pdfbox_global.sh
BASEDIR=source_allseeds
for N in $SEEDS;
do
	#find $BASEDIR/v2s$N -name "*.java" 1>v2s${N}files.lst
	#find $BASEDIR/v2s$N-orig -name "*.java" 1>v2s${N}-origfiles.lst

	> ${VERSION}s${N}files.lst
	> ${VERSION}s${N}-origfiles.lst
	for subdir in fontbox jempbox pdfbox;
	do
		find $BASEDIR/${VERSION}s$N/$subdir -name "*.java" 1>>${VERSION}s${N}files.lst
		find $BASEDIR/${VERSION}s$N-orig/$subdir -name "*.java" 1>>${VERSION}s${N}-origfiles.lst
	done
	find $BASEDIR/td${VERSION}/ -name "*.java" 1>>${VERSION}s${N}files.lst
	find $BASEDIR/td${VERSION}/ -name "*.java" 1>>${VERSION}s${N}-origfiles.lst
done

exit 0

# hcai vim :set ts=4 tw=4 tws=4

: << "comment"
47532   pdfbox          java=47532
8839    fontbox         java=8839
3109    jempbox         java=3109
comment
# hcai vim :set ts=4 tw=4 tws=4
