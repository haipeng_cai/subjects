#!/bin/bash

if [ $# -lt 1 ]; then
   echo "Usage: $0 source_file"
   exit 0
fi

#shift;
for src in "$@";
do
	n=`grep -a -E -c "hcai|Haipeng Cai|Hcai|haipeng" $src`
	if [ $n -ge 1 ]; then
		# already tagged, skip it
		echo "$src already tagged."
		continue
	fi

	n=`grep -a -E -c "vim set" $src`
	if [ $n -ge 1 ]; then
		replace "vim set" "hcai vim set" -- $src
		continue
	fi

	n=`grep -a -E -c "set ts=4" $src`
	if [ $n -ge 1 ]; then
		replace "set ts=4" "hcai vim set ts=4" -- $src
		continue
	fi

	echo -e "\n# hcai vim :set ts=4 tw=4 tws=4\n" >> $src

	echo "$src has been tagged."

done

exit 0

# hcai vim :set ts=4 tw=4 tws=4

