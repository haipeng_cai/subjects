#!/bin/bash
if [ $# -lt 1 ];then
	echo "too few arguments"
	exit 0
fi

find . -name $1 -exec rm -rf {} \;
