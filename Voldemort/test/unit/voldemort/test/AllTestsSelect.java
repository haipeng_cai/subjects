package voldemort.test;
/**
 *  This is a test driver that collects all test cases from the given list of test classes that are written
 *  in the framework of Junit4 instead of Junit3;
 *
 *  Without inputing any argument, this driver will run all test cases garnered as in a single holistic test suite, 
 *  otherwise, it will run only the single test case matching the given name provided as the input argument;
 *  
 *  This test driver is intended to serve a test driver template that supports test separation for unit tests written in the 
 *  paradigm of JUnit4
 *
 *@author     $Author: hcai $
 *@created    $Date: 2015/03/18 $
 *@version    $Revision: 1.0.0.0 $
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Properties;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.LinkedHashMap;
import java.util.Comparator;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import junit.framework.*;
import org.junit.runner.*;
import org.junit.runner.notification.Failure;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runner.JUnitCore;
import org.junit.runners.model.InitializationError;
import org.junit.internal.TextListener;

import org.junit.runner.*;
import org.junit.runner.notification.Failure;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runner.notification.RunNotifier;

import java.util.Arrays;

//import profile.ExecHistReporter;
//import change.DynSliceReporter;
//import profile.BranchReporter;
//import profile.DUAReporter;

public class AllTestsSelect
{
	static void __link() {
		/*
		BranchReporter.__link(); 
		DUAReporter.__link(); 
		DynSliceReporter.__link(); 
		ExecHistReporter.__link(); 
		*/

		/*
		EAS.Monitor.__link(); 
		Diver.EAMonitor.__link();
		mut.Modify.__link();
		Sensa.Modify.__link();
		*/

		//distEA.distThreadMonitor.__link(); 
		distEA.distMonitor.__link(); 
	}

	/** {@link org.apache.log4j} logging facility */
	static org.apache.log4j.Category cat = org.apache.log4j.Category
			.getInstance(AllTestsSelect.class.getName());

	 // private static Result runTest(final Class<?> testClazz, final String methodName)
	 private static Runner runTest(final Class<?> testClazz, final String methodName)
		    throws InitializationError {
		BlockJUnit4ClassRunner runner = new BlockJUnit4ClassRunner(testClazz) {
		    @Override
		    protected List<FrameworkMethod> computeTestMethods() {
		        try {
		            Method method = testClazz.getMethod(methodName);
		            return Arrays.asList(new FrameworkMethod(method));

		        } catch (Exception e) {
		            throw new RuntimeException(e);
		        }
		    }
		};
		return runner;
		/*
		Result res = new Result();
		runner.run(res);
		return res;
		*/
	}

	/*
	public static void main(String[] args) {
		org.apache.log4j.BasicConfigurator.configure();

		Map<String, Request> tsSorted = sortTestSuite(createSuite());
		Map<Request, String> tsSelect = new LinkedHashMap<Request, String>();
		// System.out.println("==================== all individual test cases ===================== ");
		for (String tn : tsSorted.keySet()) {
			// hcai: to dump the complete test case list
			// System.out.println(tn);
			if (args.length == 0 || args[0].trim().equals(tn.trim())) {
				tsSelect.put( tsSorted.get( tn ), tn );
			}
		}

		if (tsSelect.size() == 0)
			throw new RuntimeException("ABORTING: NO TEST CASES TO EXECUTE");
		
		JUnitCore ju = new JUnitCore();
		ju.addListener(new TextListener(System.out));
		for (Request r : tsSelect.keySet()) {
			if (tsSelect.get(r).contains("org.apache.zookeeper.VerGenTest")) {
				JUnitCore.runClasses(org.apache.zookeeper.VerGenTest.class);
				continue;
			}
			Result res = ju.run( r );
			// System.err.println("1 test [" + tsSelect.get(r) + "] :" + (res.wasSuccessful()? "passed" : "failed"));
			System.out.println(".");
			System.out.println("Time: " + res.getRunTime() + "\n");
			if (res.wasSuccessful()) {
				System.out.println("OK (1 tests) \n");
			}
			else {
				for (Failure fl : res.getFailures()) {
					System.err.println("\nFailure:");
					System.err.println(fl);
					System.err.println("\nMessage:");
					System.err.println(fl.getMessage());
					System.err.println("\nTrace:");
					System.err.println(fl.getTrace());
					System.err.println("\nDescription:");
					System.err.println(fl.getDescription()+"\n");
				}
			}
		}
	}
	*/

	public static void main(String[] args) {
		Map<String, test_pair> tsSorted = sortTestSuite(createSuite());
		List<test_pair> tsSelect = new ArrayList<test_pair>();

		org.apache.log4j.BasicConfigurator.configure();

		for (String tn : tsSorted.keySet()) {
			// hcai: to dump the complete test case list
			// System.out.println(tn);
			//
			// String tn = tsSorted.get(cls)+"("+cls.getName()+")";
			if (args.length == 0 || args[0].trim().equals(tn.trim())) {
				tsSelect.add( tsSorted.get(tn) );
			}
		}
		
		if (tsSelect.size() == 0)
			throw new RuntimeException("ABORTING: NO TEST CASES TO EXECUTE matching " + args[0]);
		
		for (test_pair tp : tsSelect) {
			// Result res = new JUnitCore().run( r );
			Result res = null;

			try {
				res = new JUnitCore().run(runTest(tp.cls, tp.mname));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

			// System.err.println("1 test [" + tsSelect.get(r) + "] :" + (res.wasSuccessful()? "passed" : "failed"));
			System.out.println(".");
			System.out.println("Time: " + res.getRunTime() + "\n");
			if (res.wasSuccessful()) {
				System.out.println("OK (1 tests) \n");
			}
			else {
				for (Failure fl : res.getFailures()) {
					System.err.println("\nFailure:");
					System.err.println(fl);
					System.err.println("\nMessage:");
					System.err.println(fl.getMessage());
					System.err.println("\nTrace:");
					System.err.println(fl.getTrace());
					System.err.println("\nDescription:");
					System.err.println(fl.getDescription()+"\n");
				}
			}
		}
	}

	/* collect all test classes; for now the list of classes is filled manually, but instead an automatic search could replace this laborious manner */
	public static List<Class> createSuite() {

		List<Class> testClses = new ArrayList();

		try
		{
			testClses.add (voldemort.cluster.failuredetector.FailureDetectorPerformanceTest.class);
			testClses.add (voldemort.cluster.failuredetector.FlappingTest.class);
			testClses.add (voldemort.cluster.failuredetector.TimedUnavailabilityTest.class);
			testClses.add (voldemort.InMemoryMultiThreadedTest.class);
			testClses.add (voldemort.MultithreadedStressTest.class);
			testClses.add (voldemort.nonblocking.E2ENonblockingCheckoutTest.class);
			testClses.add (voldemort.performance.AdminTest.class);
			testClses.add (voldemort.performance.CacheStorageEnginePerformanceTest.class);
			testClses.add (voldemort.performance.ClientConnectionStressTest.class);
			testClses.add (voldemort.performance.MetadataStressTest.class);
			testClses.add (voldemort.performance.PerformanceTest.class);
			testClses.add (voldemort.performance.ReadOnlyStorePerformanceTest.class);
			testClses.add (voldemort.performance.RemoteStoreComparisonTest.class);
			testClses.add (voldemort.performance.ResourcePoolPerfTest.class);
			testClses.add (voldemort.performance.RoutedStoreParallelismTest.class);
			testClses.add (voldemort.performance.StorageEnginePerformanceTest.class);
			testClses.add (voldemort.socketpool.AbstractSocketPoolTest.class);
			testClses.add (voldemort.socketpool.SimpleSocketPoolTest.class);
			testClses.add (voldemort.store.slop.LongHintedHandoffTest.class);
			testClses.add (voldemort.store.slop.SlopStreamingTest.class);
			testClses.add (voldemort.client.rebalance.NonZonedRebalanceLongTest.class);
			testClses.add (voldemort.client.rebalance.ZonedRebalanceLongTest.class);
			testClses.add (voldemort.socketpool.E2EClientRequestExecutorPoolAndFailureDetectorTest.class);
			testClses.add (voldemort.client.AbstractAdminServiceFilterTest.class);
			testClses.add (voldemort.client.AbstractStoreClientFactoryTest.class);
			testClses.add (voldemort.client.AdminFetchTest.class);
			testClses.add (voldemort.client.AdminServiceBasicTest.class);
			testClses.add (voldemort.client.AdminServiceFailureTest.class);
			testClses.add (voldemort.client.AdminServiceFilterTest.class);
			testClses.add (voldemort.client.AdminServiceMultiJVMTest.class);
			testClses.add (voldemort.client.AtomicSetMetadataPairTest.class);
			testClses.add (voldemort.client.CachingStoreClientFactoryTest.class);
			testClses.add (voldemort.client.ClientJmxTest.class);
			testClses.add (voldemort.client.ClientRegistryTest.class);
			testClses.add (voldemort.client.DefaultStoreClientTest.class);
			testClses.add (voldemort.client.EndToEndRebootstrapTest.class);
			testClses.add (voldemort.client.HttpStoreClientFactoryTest.class);
			testClses.add (voldemort.client.OfflineStateTest.class);
			testClses.add (voldemort.client.protocol.admin.QueryKeyResultTest.class);
			testClses.add (voldemort.client.protocol.admin.StreamingClientTest.class);
			testClses.add (voldemort.client.rebalance.AbstractNonZonedRebalanceTest.class);
			testClses.add (voldemort.client.rebalance.AbstractRebalanceTest.class);
			testClses.add (voldemort.client.rebalance.AbstractZonedRebalanceTest.class);
			testClses.add (voldemort.client.rebalance.AdminRebalanceTest.class);
			testClses.add (voldemort.client.rebalance.NonZonedRebalanceBatchPlanTest.class);
			testClses.add (voldemort.client.rebalance.NonZonedRebalanceTest.class);
			testClses.add (voldemort.client.rebalance.RebalanceMetadataConsistencyTest.class);
			testClses.add (voldemort.client.rebalance.RebalancePlanTest.class);
			testClses.add (voldemort.client.rebalance.RebalanceRebootstrapConsistencyTest.class);
			testClses.add (voldemort.client.rebalance.RebalanceSchedulerTest.class);
			testClses.add (voldemort.client.rebalance.ZonedRebalanceBatchPlanTest.class);
			testClses.add (voldemort.client.rebalance.ZonedRebalanceNonContiguousZonesTest.class);
			testClses.add (voldemort.client.rebalance.ZonedRebalanceTest.class);
			testClses.add (voldemort.client.rebalance.ZoneShrinkageClientTest.class);
			testClses.add (voldemort.client.rebalance.ZoneShrinkageEndToEndTest.class);
			testClses.add (voldemort.client.ReplaceNodeTest.class);
			testClses.add (voldemort.client.SocketStoreClientFactoryMbeanTest.class);
			testClses.add (voldemort.client.SocketStoreClientFactoryTest.class);
			testClses.add (voldemort.cluster.failuredetector.AbstractFailureDetectorTest.class);
			testClses.add (voldemort.cluster.failuredetector.BannagePeriodFailureDetectorTest.class);
			testClses.add (voldemort.cluster.failuredetector.ServerStoreConnectionVerifierTest.class);
			testClses.add (voldemort.cluster.failuredetector.ThresholdFailureDetectorTest.class);
			testClses.add (voldemort.cluster.TestCluster.class);
			testClses.add (voldemort.coordinator.CoordinatorRestAPITest.class);
			testClses.add (voldemort.coordinator.DynamicTimeoutStoreClientTest.class);
			testClses.add (voldemort.protocol.AbstractRequestFormatTest.class);
			testClses.add (voldemort.protocol.pb.ProtocolBuffersRequestFormatTest.class);
			testClses.add (voldemort.protocol.vold.VoldemortNativeRequestFormatTest.class);
			testClses.add (voldemort.routing.ConsistentRoutingStrategyTest.class);
			testClses.add (voldemort.routing.StoreRoutingPlanTest.class);
			testClses.add (voldemort.routing.ZoneRoutingStrategyTest.class);
			testClses.add (voldemort.scheduled.BlockingSlopPusherTest.class);
			testClses.add (voldemort.scheduled.DataCleanupJobTest.class);
			testClses.add (voldemort.scheduled.SlopPurgeTest.class);
			testClses.add (voldemort.scheduled.SlopPusherDeadSlopTest.class);
			testClses.add (voldemort.scheduled.StreamingSlopPusherTest.class);
			testClses.add (voldemort.serialization.avro.AvroGenericSerializerTest.class);
			testClses.add (voldemort.serialization.avro.AvroReflectiveSerializerTest.class);
			testClses.add (voldemort.serialization.avro.AvroSpecificSerializerTest.class);
			testClses.add (voldemort.serialization.avro.versioned.AvroBackwardsCompatibilityTest.class);
			testClses.add (voldemort.serialization.json.JsonBackwardsCompatibilityTest.class);
			testClses.add (voldemort.serialization.json.JsonReaderTest.class);
			testClses.add (voldemort.serialization.json.JsonTypeSerializerTest.class);
			testClses.add (voldemort.serialization.protobuf.ProtoBufSerializerTest.class);
			testClses.add (voldemort.serialization.SlopSerializerTest.class);
			testClses.add (voldemort.serialization.thrift.ThriftSerializerTest.class);
			testClses.add (voldemort.serialization.VersionedSerializerTest.class);
			testClses.add (voldemort.server.EndToEndTest.class);
			testClses.add (voldemort.server.gossip.GossiperTest.class);
			testClses.add (voldemort.server.protocol.admin.AsyncOperationTest.class);
			testClses.add (voldemort.server.protocol.hadoop.RestHadoopFetcherTest.class);
			testClses.add (voldemort.server.quota.ExceededQuotaSlopTest.class);
			testClses.add (voldemort.server.quota.QuotaLimitingStoreTest.class);
			testClses.add (voldemort.server.rebalance.RebalancerStateTest.class);
			testClses.add (voldemort.server.ServiceTest.class);
			testClses.add (voldemort.server.socket.ClientRequestExecutorPoolTest.class);
			testClses.add (voldemort.server.socket.NioStatsJmxTest.class);
			testClses.add (voldemort.server.socket.SocketPoolTest.class);
			testClses.add (voldemort.server.socket.SocketServerTest.class);
			testClses.add (voldemort.server.storage.RepairJobTest.class);
			testClses.add (voldemort.server.storage.ScanPermitWrapperTest.class);
			testClses.add (voldemort.server.storage.StorageServiceTest.class);
			testClses.add (voldemort.server.storage.VersionedPutPruneJobTest.class);
			testClses.add (voldemort.server.storage.VersionedPutPruningTest.class);
			testClses.add (voldemort.store.AbstractByteArrayStoreTest.class);
			testClses.add (voldemort.store.AbstractStorageEngineTest.class);
			testClses.add (voldemort.store.AbstractStoreTest.class);
			testClses.add (voldemort.store.bdb.BdbCachePartitioningTest.class);
			testClses.add (voldemort.store.bdb.BdbPartitionListIteratorTest.class);
			testClses.add (voldemort.store.bdb.BdbSplitStorageEngineTest.class);
			testClses.add (voldemort.store.bdb.BdbStorageEngineTest.class);
			testClses.add (voldemort.store.bdb.PartitionPrefixedBdbStorageEngineTest.class);
			testClses.add (voldemort.store.compress.CompressingStoreTest.class);
			testClses.add (voldemort.store.compress.lzf.TestLZF.class);
			testClses.add (voldemort.store.configuration.ConfigurationStorageEngineTest.class);
			testClses.add (voldemort.store.configuration.FileBackedCachingStorageEngineTest.class);
			testClses.add (voldemort.store.http.HttpStoreTest.class);
			testClses.add (voldemort.store.invalidmetadata.InvalidMetadataCheckingStoreTest.class);
			testClses.add (voldemort.store.invalidmetadata.ServerSideRoutingTest.class);
			testClses.add (voldemort.store.logging.LoggingStoreTest.class);
			testClses.add (voldemort.store.memory.CacheStorageEngineTest.class);
			testClses.add (voldemort.store.memory.InMemoryStorageEngineTest.class);
			testClses.add (voldemort.store.memory.SlowStorageEngineTest.class);
			testClses.add (voldemort.store.metadata.MetadataStoreTest.class);
			testClses.add (voldemort.store.mysql.MysqlStorageEngineTest.class);
			testClses.add (voldemort.store.readonly.chunk.DataFileChunkSetIteratorTest.class);
			testClses.add (voldemort.store.readonly.ExternalSorterTest.class);
			testClses.add (voldemort.store.readonly.ReadOnlyStorageEngineTest.class);
			testClses.add (voldemort.store.readonly.ReadOnlyStorageMetadataTest.class);
			testClses.add (voldemort.store.readonly.ReadOnlyUtilsTest.class);
			testClses.add (voldemort.store.readonly.SearchStrategyTest.class);
			testClses.add (voldemort.store.readonly.swapper.StoreSwapperTest.class);
			testClses.add (voldemort.store.rebalancing.RebootstrappingStoreTest.class);
			testClses.add (voldemort.store.rebalancing.RedirectingStoreTest.class);
			testClses.add (voldemort.store.routed.AbstractZoneAffinityTest.class);
			testClses.add (voldemort.store.routed.action.AbstractActionTest.class);
			testClses.add (voldemort.store.routed.action.ConfigureNodesLocalHostTest.class);
			testClses.add (voldemort.store.routed.action.ConfigureNodesTest.class);
			testClses.add (voldemort.store.routed.action.GetAllConfigureNodesTest.class);
			testClses.add (voldemort.store.routed.GetallNodeReachTest.class);
			testClses.add (voldemort.store.routed.HintedHandoffFailureTest.class);
			testClses.add (voldemort.store.routed.HintedHandoffSendHintTest.class);
			testClses.add (voldemort.store.routed.NodeValueTest.class);
			testClses.add (voldemort.store.routed.ReadRepairerTest.class);
			testClses.add (voldemort.store.routed.RoutedStoreTest.class);
			testClses.add (voldemort.store.routed.ZoneAffinityGetAllTest.class);
			testClses.add (voldemort.store.routed.ZoneAffinityGetTest.class);
			testClses.add (voldemort.store.routed.ZoneAffinityGetVersionsTest.class);
			testClses.add (voldemort.store.routed.ZoneCountWriteTest.class);
			testClses.add (voldemort.store.serialized.SerializingStoreTest.class);
			testClses.add (voldemort.store.slop.SlopTest.class);
			testClses.add (voldemort.store.slop.strategy.ConsistentHandoffStrategyTest.class);
			testClses.add (voldemort.store.slop.strategy.HandoffToAnyStrategyTest.class);
			testClses.add (voldemort.store.slop.strategy.ProximityHandoffStrategyTest.class);
			testClses.add (voldemort.store.socket.AbstractSocketStoreTest.class);
			testClses.add (voldemort.store.socket.ProtocolBuffersSocketStoreTest.class);
			testClses.add (voldemort.store.socket.VoldemortNativeSocketStoreTest.class);
			testClses.add (voldemort.store.stats.ClientSocketStatsTest.class);
			testClses.add (voldemort.store.stats.HistogramTest.class);
			testClses.add (voldemort.store.stats.RequestCounterTest.class);
			testClses.add (voldemort.store.stats.SimpleCounterTest.class);
			testClses.add (voldemort.store.stats.StatsTest.class);
			testClses.add (voldemort.store.stats.StoreStatsJmxTest.class);
			testClses.add (voldemort.store.system.AsyncMetadataVersionManagerTest.class);
			testClses.add (voldemort.store.system.SystemStoreTest.class);
			testClses.add (voldemort.store.views.ViewStorageEngineTest.class);
			testClses.add (voldemort.store.views.ViewTransformsTest.class);
			testClses.add (voldemort.tools.admin.AvroAddStoreTest.class);
			testClses.add (voldemort.tools.admin.MetaOperationsTest.class);
			testClses.add (voldemort.tools.admin.QuotaOperationsTest.class);
			testClses.add (voldemort.tools.admin.SetMetadataTest.class);
			testClses.add (voldemort.tools.admin.StoreOperationsTest.class);
			testClses.add (voldemort.tools.PartitionBalanceTest.class);
			testClses.add (voldemort.tools.ReadOnlyReplicationHelperTest.class);
			testClses.add (voldemort.tools.RepartitionerTest.class);
			testClses.add (voldemort.tools.ZoneCLipperTest.class);
			testClses.add (voldemort.tools.ZoneShrinkageCLITest.class);
			testClses.add (voldemort.utils.ByteUtilsTest.class);
			testClses.add (voldemort.utils.CachedCallableTest.class);
			testClses.add (voldemort.utils.ClusterForkLiftToolTest.class);
			testClses.add (voldemort.utils.ClusterUtilsTest.class);
			testClses.add (voldemort.utils.ConsistencyCheckTest.class);
			testClses.add (voldemort.utils.ConsistencyFixTest.class);
			testClses.add (voldemort.utils.ConsistencyFixWorkerTest.class);
			testClses.add (voldemort.utils.IoThrottlerTest.class);
			testClses.add (voldemort.utils.MoveMapTest.class);
			testClses.add (voldemort.utils.NetworkClassLoaderTest.class);
			testClses.add (voldemort.utils.PairTest.class);
			testClses.add (voldemort.utils.pool.KeyedResourcePoolContentionTest.class);
			testClses.add (voldemort.utils.pool.KeyedResourcePoolRaceTest.class);
			testClses.add (voldemort.utils.pool.KeyedResourcePoolTest.class);
			testClses.add (voldemort.utils.pool.QueuedKeyedResourcePoolContentionTest.class);
			testClses.add (voldemort.utils.pool.QueuedKeyedResourcePoolTest.class);
			testClses.add (voldemort.utils.PropsTest.class);
			testClses.add (voldemort.utils.RebalanceUtilsTest.class);
			testClses.add (voldemort.utils.ReflectUtilsTest.class);
			testClses.add (voldemort.utils.ServerTestUtilsTest.class);
			testClses.add (voldemort.utils.UtilsTest.class);
			testClses.add (voldemort.utils.VoldemortIOUtilsTest.class);
			testClses.add (voldemort.versioning.ChainedInconsistencyResolverTest.class);
			testClses.add (voldemort.versioning.ClockEntryTest.class);
			testClses.add (voldemort.versioning.InconsistentDataExceptionTest.class);
			testClses.add (voldemort.versioning.VectorClockInconsistencyResolverTest.class);
			testClses.add (voldemort.versioning.VectorClockTest.class);
			testClses.add (voldemort.versioning.VersionedTest.class);
			testClses.add (voldemort.xml.ClusterMapperTest.class);
			testClses.add (voldemort.xml.StoreDefinitionMapperTest.class);
			/*
			testClses.add (voldemort.cluster.failuredetector.FailureDetectorPerformanceTest.class);
			testClses.add (voldemort.cluster.failuredetector.FlappingTest.class);
			testClses.add (voldemort.cluster.failuredetector.TimedUnavailabilityTest.class);
			testClses.add (voldemort.InMemoryMultiThreadedTest.class);
			testClses.add (voldemort.MultithreadedStressTest.class);
			testClses.add (voldemort.nonblocking.E2ENonblockingCheckoutTest.class);
			testClses.add (voldemort.performance.AdminTest.class);
			testClses.add (voldemort.performance.CacheStorageEnginePerformanceTest.class);
			testClses.add (voldemort.performance.ClientConnectionStressTest.class);
			testClses.add (voldemort.performance.MetadataStressTest.class);
			testClses.add (voldemort.performance.PerformanceTest.class);
			testClses.add (voldemort.performance.ReadOnlyStorePerformanceTest.class);
			testClses.add (voldemort.performance.RemoteStoreComparisonTest.class);
			testClses.add (voldemort.performance.ResourcePoolPerfTest.class);
			testClses.add (voldemort.performance.RoutedStoreParallelismTest.class);
			testClses.add (voldemort.performance.StorageEnginePerformanceTest.class);
			testClses.add (voldemort.socketpool.AbstractSocketPoolTest.class);
			testClses.add (voldemort.socketpool.SimpleSocketPoolTest.class);
			testClses.add (voldemort.store.slop.LongHintedHandoffTest.class);
			testClses.add (voldemort.store.slop.SlopStreamingTest.class);
			*/
		}
		catch (Exception ex)
		{
			System.err.println("error adding test :"+ex);
		}

		return testClses;
	}

	/* retrieve test cases from test classes following the standard convention (test prefix, for instance); and 
	 * then sort all test cases by their names
	 */ 
	/*
	private static Map<String,Request> sortTestSuite(List<Class> testClses) {
		Map<String, Request> allTests = new TreeMap<String,Request>(StringBasedComparator.inst);
		
		for (Class cls : testClses) {
			for (Method m : cls.getDeclaredMethods() ) {
				if (!isPublicTestMethod(m)) {
					// skip non-test methods
					continue;
				}
				// create a test case for each public (accessible from outside of the class then) test method
				String testName = m.getName()+"("+cls.getName()+")"; // cls.getName()+": " + m.getName();
				allTests.put(testName, Request.method(cls, m.getName()));
			}
		}

		// testClses.remove (org.apache.zookeeper.VerGenTest.class);
		sortTestSuiteEx(testClses, allTests);

		return allTests;
	}
	*/
	static class test_pair {
		public Class cls;
		public String mname;
		public test_pair(Class cls, String mname) {
			this.cls = cls;
			this.mname = mname;
		}
	}

	private static Map<String,test_pair> sortTestSuite(List<Class> testClses) {
		Map<String, test_pair> allTests = new TreeMap<String,test_pair>(StringBasedComparator.inst);
		
		for (Class cls : testClses) {
			for (Method m : cls.getDeclaredMethods() ) {
				if (!isPublicTestMethod(m)) {
					// skip non-test methods
					continue;
				}
				// create a test case for each public (accessible from outside of the class then) test method
				String testName = m.getName()+"("+cls.getName()+")"; // cls.getName()+": " + m.getName();
				// allTests.put(testName, Request.method(cls, m.getName()));
				allTests.put(testName, new test_pair(cls, m.getName()));
			}
		}

		return allTests;

		/*
		Collections.sort(allTests, StringBasedComparator.inst);

		TestSuite tsSorted = new TestSuite("OpenNLP 1.5.3 entire unit test suite");
		for (Iterator it = allTests.iterator();it.hasNext();) {
			tsSorted.addTest((Test) it.next());
		}

		return tsSorted;
		*/
	}

	/*
	static class hcJUnit4ClassRunner extends BlockJUnit4ClassRunner {
		public hcJUnit4ClassRunner(Class<?> kcls) throws InitializationError {
			super(kcls);
		}
		//@Override
		public java.util.List<org.junit.runners.model.FrameworkMethod> getChildren() {
			return super.getChildren();
		}

		//@Override
		public java.lang.String testName(org.junit.runners.model.FrameworkMethod method) {
			return super.testName(method);
		}
	}

	private static Map<String,Request> sortTestSuiteEx(List<Class> testClses, Map<String, Request> allTests) {
		for (Class cls : testClses) {
			// System.out.print("In class: " + cls.getName() + " -> test count="); 
			hcJUnit4ClassRunner runner = null;
			try {
				runner = new hcJUnit4ClassRunner(cls);
			}
			catch(Exception e) {
				// e.printStackTrace();
				continue;
			}
			// System.out.println(runner.testCount());
			for (org.junit.runners.model.FrameworkMethod m : runner.getChildren() ) {
				// create a test case for each public (accessible from outside of the class then) test method
				// String testName = runner.testName(m); 
				String testName = m.getName()+"("+cls.getName()+")"; // cls.getName()+": " + m.getName();
				if (!allTests.containsKey(testName)) {
					allTests.put(testName, Request.method(cls, m.getName()));
				}
			}
		}

		return allTests;
	}
	*/
	
	private static boolean isPublicTestMethod(Method m) {
        return isTestMethod(m) && Modifier.isPublic(m.getModifiers());
    }

    private static boolean isTestMethod(Method m) {
        return m.getParameterTypes().length == 0 &&
                m.getName().startsWith("test") &&
                m.getReturnType().equals(Void.TYPE);
    }
}

/* hcai vim :set ts=4 tw=4 tws=4 */

