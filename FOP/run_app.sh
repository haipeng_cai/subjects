#!/bin/bash
scale=${1:-"default"}

source ./fop_global.sh

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/mcia/bin:$ROOT/workspace/Tracer/bin:$ROOT/tools/java_cup.jar:$subjectloc/tracerInstrumented-fop/"

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

OUTDIR=Runout-app
#mkdir -p $OUTDIR

args="-q $subjectloc/dat/test.fo -ps test.ps"
if [ ${scale} = "small" ];
then
	args="-q $subjectloc/dat/readme.fo -pdf readme.pdf"
fi

function RunAllInOne()
{
	java -Xmx4000m -ea -cp $MAINCP  $DRIVERCLASS ${args} #2>/dev/null
	#java -Xmx4000m -ea -cp $MAINCP -jar $subjectloc/dacapo-9.12-bach.jar $DRIVERCLASS "$args"
}

starttime=`date +%s%N | cut -b1-13`
RunAllInOne
stoptime=`date +%s%N | cut -b1-13`
echo "Normal RunTime elapsed: " `expr $stoptime - $starttime` milliseconds
exit 0

# hcai vim :set ts=4 tw=4 tws=4

