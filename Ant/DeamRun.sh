#!/bin/bash
if [ $# -lt 2 ];then
	echo "Usage: $0 version seed [changeloc]"
	exit 1
fi

ver=$1
seed=$2
change=${3:-"-2147483648"}

source ./ant_global.sh

INDIR=$subjectloc/DeamInstrumented-$ver-$seed
[ $change -ne -2147483648 ] && INDIR=$INDIR-$change

MAINCP=".:/etc/alternatives/java_sdk/jre/lib/rt.jar:$ROOT/tools/j2sdk1.4.2_18/lib/tools.jar:$ROOT/tools/polyglot-1.3.5/lib/polyglot.jar:$ROOT/tools/soot-2.3.0/lib/sootclasses-2.3.0.jar:$ROOT/tools/jasmin-2.3.0/lib/jasminclasses-2.3.0.jar:$ROOT/tools/java_cup.jar:$ROOT/workspace/DUAForensics/bin:$ROOT/workspace/LocalsBox/bin:$ROOT/workspace/InstrReporters/bin:$INDIR"

for i in $subjectloc/lib/*.jar;
do
	MAINCP=$MAINCP:$i
done

suffix=${ver}${seed}
[ $change -ne -2147483648 ] && suffix=$change-${ver}${seed}

OUTDIR=Deamoutdyn-${ver}${seed}
[ $change -ne -2147483648 ] && OUTDIR=$OUTDIR-$change
mkdir -p $OUTDIR

FAILLIST=./errorTestName-$suffix.txt
PASSLIST=./normalTestName-$suffix.txt
> $FAILLIST
> $PASSLIST

function RunAllInOne()
{
	java -Xmx4000m -ea -cp $MAINCP  $DRIVERCLASS 
}

function RunOneByOne()
{
	# to run a single test at a time
	local i=0
	cat $subjectloc/inputs/testinputs.txt | dos2unix | \
	while read testname;
	do
		let i=i+1

		echo "Run Test #$i" # [" $testname "] ....."
		java -Xmx4000m -ea -cp $MAINCP  $DRIVERCLASS $testname 1> $OUTDIR/$i.out 2> $OUTDIR/$i.err
		if [ -s $OUTDIR/$i.err ];then
			echo "$testname" >> $FAILLIST
		else
			echo "$testname" >> $PASSLIST
		fi
	done
}

starttime=`date +%s%N | cut -b1-13`
#RunAllInOne
RunOneByOne
stoptime=`date +%s%N | cut -b1-13`
echo "RunTime for $suffix elapsed: " `expr $stoptime - $starttime` milliseconds
exit 0

# hcai vim :set ts=4 tw=4 tws=4
